package com.touchtechnologies.log;

interface LogDatabaseMetadata {
	int VERSION = 1;
	String DBNAME = "logdb";
	
	public enum TABLE{	
		LOG("log");
			
		TABLE(String name){
			this.name = name;
		}
		private String name;
		
		/**
		 * Get table name
		 * @return table name
		 */
		public String getName(){
			return name;
		}
		
		/**
		 * Get SQLite command to create a database.<br/>
		 * Currently it is not use foreign key constraint as it need to support Android version 2.2 and less  
		 * @return
		 */
		public String getCreateSQL(){			
				return "CREATE TABLE IF NOT EXISTS " + name + " (" +
						"_id integer primary key autoincrement," +
						"text text not null," +
						"throwable text not null," +
						"created_date date" +
						")";			
		}
		
	}
}
