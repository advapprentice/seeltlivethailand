package com.touchtechnologies.log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.os.Environment;

import com.touchtechnologies.lib.BuildConfig;

public class Log {
	public static boolean verbose = BuildConfig.DEBUG;
	public static boolean fileLog = false;
	public static String tag = "touch";
	public static boolean logToDatabase;

	private static SimpleDateFormat logFotmat = new SimpleDateFormat("MMdd_HHmmss", Locale.US);
	public static String ROOT_PATH = Environment.getExternalStorageDirectory().getAbsolutePath()+"/touchtechnologies.co.th";
	
	/**
	 * @deprecated use Android log instead
	 * @param message
	 */
	public static void log(String message){
		log(message, null);
	}
	
	/**
	 * @deprecated use Android log instead
	 * @param message
	 * @param e
	 */
	public static void log(String message, Exception e){		
		if(verbose){
			android.util.Log.d(tag, (message == null?"":message) + (e == null?"":e.getMessage()) );
			
			if(e != null){
				e.printStackTrace();			
			}	
		}
		
		logToFile(ROOT_PATH, message, e);
	}
	
	/**
	 * @deprecated use Android log instead
	 * @param message
	 */
	public static void println(String message){
		if(verbose){
			System.out.println(message == null?"null" : message);
		}
		
		logToFile(ROOT_PATH, message);
	}
	
	public static void logToFile(final String path, final String message){
		logToFile(path, message, null);
	}
	
	public static void logToFile(final String path, final String message, final Exception e){
		if(!fileLog) return;
		
		Thread fileThread = new Thread(){
			@Override
			public void run() {				
				FileOutputStream fos = null;
				PrintWriter pw = null;
				try{
					File file = new File(path + File.separator);
					file.mkdirs();
					
					file = new File(file.getPath()+ File.separator + logFotmat.format(new Date(System.currentTimeMillis())) + ".log");
					file.createNewFile();
					
					fos = new FileOutputStream(file);
					pw = new PrintWriter(fos);
					if(e != null){
						e.printStackTrace(pw);
						pw.flush();			
					}
					
					pw.println(message);
					pw.flush();
					
				}catch(Exception ex){
					log("print log to file\t", ex);
					
				}finally{
					if(fos != null){
						try{
							fos.close();
						}catch(Exception ee){							
						}
					}
					if(pw != null){
						pw.close();
					}
				}
			}			
		};
		fileThread.start();
		
	}
}
