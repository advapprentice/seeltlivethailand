package com.touchtechnologies.log;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.touchtechnologies.log.LogDatabaseMetadata.TABLE;
import com.touchtechnologies.persistent.DatabaseHelper;

class LogDatabaseHelper extends DatabaseHelper{
	public LogDatabaseHelper(Context context) {
		super(context, LogDatabaseMetadata.DBNAME, LogDatabaseMetadata.VERSION);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onCreate(db);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		TABLE[] tables = TABLE.values();
		for (int i = 0; i < tables.length; i++) {
			db.execSQL("DROP TABLE IF EXISTS " + tables[i].getName());
		}
		
		for (int i = 0; i < tables.length; i++) {
			try {
				db.execSQL(tables[i].getCreateSQL());
			} catch (Exception e) {
				Log.log("DatabaseHelper", e);
			}
		}
	}
}
