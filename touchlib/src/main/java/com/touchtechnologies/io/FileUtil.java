package com.touchtechnologies.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.touchtechnologies.app.LibConfig;

import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

public class FileUtil {

	/**
	 * Get file path from a given uri
	 * @param context
	 * @param uri
	 * @return file path
	 */
	public static String getFilePath(Context context, Uri uri){
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor cursor = new CursorLoader(context, uri, proj, null, null, null).loadInBackground();
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		String path = cursor.getString(column_index);
		
		return path;
	}
	
	/**
	 * Write bitmap to a give path, using jpg compression format with 90% quality.
	 * @param bitmap
	 * @param dir
	 * @param name file name
	 * @return
	 * @throws IOException
	 */
	public static boolean writeToFile(Bitmap bitmap, String dir, String name) throws IOException{
		boolean success = false;
		
		FileOutputStream fos = null;
		try{
			//create a directory if not exist
			File file = new File(dir);
			if(!file.exists()){
				file.mkdirs();
			}
			
			//write bitmap to file
			fos = new FileOutputStream(dir + File.separator + name);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
			fos.flush();		
			
			success = true;			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "save bitmap to file failed " + dir + File.separator + name, e);
			new IOException(e);
		}finally{
			if(fos != null){
				try{
					fos.close();
				}catch(Exception e){					
				}
			}
		}
		
		return success;
		
	}
}
