package com.touchtechnologies.telephony;

import android.content.Context;
import android.telephony.TelephonyManager;

public class TelephonyInfo {
	public enum TELEPHONY_INFO{
		IMSI, IMEI, MSISDN, ICCID, OPERATOR_CODE, OPERATOR_NAME ,OPERATOR_TYPE,OPERATOR_COUNTRY
	}	
		
	public static String getInfo(Context context, TELEPHONY_INFO tELEPHONY_INFO){
		String info = null;
		
		try{
			TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
			
			switch(tELEPHONY_INFO){
			case IMSI:
				info = tm.getSubscriberId();
				break;
			case IMEI:
				info = tm.getDeviceId();
				break;
			case MSISDN:
				info = tm.getLine1Number();
				break;
			case OPERATOR_NAME:
				info = tm.getNetworkOperatorName();
				break;
			case OPERATOR_CODE:
				info = tm.getNetworkOperator();
				break;
			case ICCID:
				info = tm.getSimSerialNumber();
				break;
				
			case OPERATOR_COUNTRY:
				info = tm.getSimCountryIso();
				break;
			}
		}catch(Exception e){	
			com.touchtechnologies.log.Log.log("Telephony", e);
		}
		return info;
	}
}
