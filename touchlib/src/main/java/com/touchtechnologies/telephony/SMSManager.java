package com.touchtechnologies.telephony;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import com.touchtechnologies.log.Log;

public class SMSManager {
	private static final String SERVICE_URL = "http://203.170.193.74:9080/TouchSMSWeb/TouchSMSEnqueueSMSServlet";
	private String gwUsername;
	private String gwPassword;
	private String sourceID;
	
	/**
	 * Set SMS gateway password.
	 * @param gwPassword
	 */
	public void setGwPassword(String gwPassword) {
		this.gwPassword = gwPassword;
	}
	
	/**
	 * Set SMS gateway username.
	 * @param gwUsername
	 */
	public void setGwUsername(String gwUsername) {
		this.gwUsername = gwUsername;
	}
	
	/**
	 *  Set SMS gateway source ID
	 * @param sourceID
	 */
	public void setSourceID(String sourceID) {
		this.sourceID = sourceID;
	}
	
	public void sendMessage(String src, String u, String p, final String address, final String content){
		setGwPassword(p);
		setGwUsername(u);
		setSourceID(src);
		
		sendMessage(address, content);
	}
	
	public void sendMessage(final String address, final String content){
		Thread sendThread = new Thread() {

            public void run() {
                HttpURLConnection httpConn = null;
                DataOutputStream dos = null;
                DataInputStream input = null;
                URL url = null;

                        try {
                            url = new URL(SERVICE_URL);
                            httpConn = (HttpURLConnection) url.openConnection();
                            httpConn.setRequestMethod("POST");
                            httpConn.setDoOutput(true);

                            StringBuffer query = new StringBuffer("CMD=ENQUEUESMS&");
                            query.append("TO=");
                            query.append(address);
                            query.append("&");
                            query.append("SENDER=TOUCHSMS&");
                            query.append("CTYPE=UNICODE&");
                            query.append("SMSMSG=");
                            query.append(URLEncoder.encode(content, "UTF-8"));
                            //query.append(message.getText());
                            query.append("&");
                            query.append("USERNAME=");
                            query.append(gwUsername);
                            query.append("&");
                            query.append("PASSWORD=");
                            query.append(gwPassword);
                            query.append("&");
                            query.append("SOURCEID=");
                            query.append(sourceID);

                            dos = new DataOutputStream(httpConn.getOutputStream());
                            dos.write(query.toString().getBytes());

                            input = new DataInputStream(httpConn.getInputStream());

                            // read in each character until end-of-stream is detected
                            StringBuffer buff = new StringBuffer();
                            for (int c = input.read(); c != -1; c = input.read()) {
                                buff.append((char) c);
                            }
                            input.close();

                            Log.println("Send SMS response:" + buff.toString());
                        } catch (Exception e) {
                            Log.log("Send alert message error", e);
                            if (url != null) {
                            	Log.println("To URL: " + url.getHost() + ":" + url.getPort() + url.getPath() + "?" + url.getQuery());
                            }
                        } finally {
                            if (dos != null) {
                                try {
                                    dos.close();
                                } catch (Exception e) {
                                }
                            }
                            if (input != null) {
                                try {
                                    input.close();
                                } catch (Exception e) {
                                }
                            }
                            if (httpConn != null) {
                                httpConn.disconnect();
                            }
                        }
                    } 
           
        };

        sendThread.start();
	}
}
