package com.touchtechnologies.graphics;

import java.io.Serializable;
import java.util.Locale;
import java.util.Random;

import org.json.JSONObject;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.io.FileUtil;
import com.touchtechnologies.json.JSONUtil;

public class Media implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4135110009432295260L;
	public static final String TYPE_VIDEO = "video";
	public static final String TYPE_IMAGE = "image/jpeg";
	public static final String TYPE_AUDIO = "audio";	
	
	String mediaID;
	String mediaThumbUri;
	String mediaType;
	String mediaLatitude;
	String mediaLongitude;
	String parsedUri;
	
	/**
	 * File path of local media
	 */
	String filePath;
	/**
	 * File path of local thumb image 
	 */
	String filePathThumb;
	/**
	 * Media file name
	 */
	String name;
	/**
	 * Media thumb nail file name
	 */
	String nameThumbnail;
	
	/**
	 * Full image width
	 */
	public int width;
	/**
	 * Full image height
	 */
	public int height;
	/**
	 * Thumb image width
	 */
	public int widthThumb;
	/**
	 * Thumb image height
	 */
	public int heightThumb;
			
	public Media(){		
	}
	
	/**
	 * Create a Media object of a "local" media file.
	 * @param context
	 * @param uri
	 */
	public Media(Context context, Uri uri){
		filePath = FileUtil.getFilePath(context, uri);
		initName(filePath);
		
		setUri(uri);
	}
	
	public Media(JSONObject json){
		if(!json.isNull("mediaID")){
			setID(JSONUtil.getString(json, "mediaID"));
		}
		
		if(!json.isNull("mediaThumbURL")){
			setThumbUri(JSONUtil.getString(json, "mediaThumbURL"));
		}

		if(!json.isNull("mediaURL")){
			setUri(JSONUtil.getString(json, "mediaURL"));
		}

		if(!json.isNull("mediaType")){
			setType(JSONUtil.getString(json, "mediaType"));
		}

		if(!json.isNull("mediaLatitude")){
			setLatitude(JSONUtil.getString(json, "mediaLatitude"));
		}

		if(!json.isNull("mediaLongitude")){
			setLongitude(JSONUtil.getString(json, "mediaLongitude"));			
		}
		
		if(!json.isNull("uri")){
			setUri(JSONUtil.getString(json, "uri"));
		}

		if(!json.isNull("filePath")){
			filePath = JSONUtil.getString(json, "filePath");
			initName(filePath);
		}
		
		if(!json.isNull("filePathThumb")){
			filePathThumb = JSONUtil.getString(json, "filePathThumb");
		}
		
		if(!json.isNull("mediaThumbDimension")){
			try{
				JSONObject tmp = json.getJSONObject("mediaThumbDimension");
				widthThumb = JSONUtil.getInt(tmp, "width");
				heightThumb = JSONUtil.getInt(tmp, "height");
			}catch(Exception e){
				
			}
		}
		
		if(!json.isNull("mediaDimension")){
			try{
				JSONObject tmp = json.getJSONObject("mediaDimension");
				width = JSONUtil.getInt(tmp, "width");
				height = JSONUtil.getInt(tmp, "height");			
			}catch(Exception e){
				
			}
		}
		
	
//		{
//            "mediaID": "9",
//            "mediaThumbURL": "http://192.168.9.90/app/resources/news/91/comment/70/thumbnail/1402459680830_4132.jpg",
//            "mediaThumbDimension": {
//              "filesize": 84518,
//              "width": 714,
//              "height": 400,
//              "type": 2,
//              "extension": "jpg",
//              "attr": "width=\"714\" height=\"400\"",
//              "bits": 8,
//              "channels": 3,
//              "mime": "image/jpeg"
//            },
//            "mediaURL": "http://192.168.9.90/app/resources/news/91/comment/70/image/1402459680830_4127.jpg",
//            "mediaDimension": {
//              "filesize": 9728,
//              "width": 300,
//              "height": 168,
//              "type": 2,
//              "extension": "jpg",
//              "attr": "width=\"300\" height=\"168\"",
//              "bits": 8,
//              "channels": 3,
//              "mime": "image/jpeg"
//            },
//            "mediaType": "image/jpeg",
//            "mediaLatitude": "0",
//            "mediaLongitude": "0"
//          }
	}
	
	public void setID(String mediaID) {
		this.mediaID = mediaID;
	}
	
	public String getID() {
		return mediaID;
	}
	
	public void setLatitude(String mediaLatitude) {
		this.mediaLatitude = mediaLatitude;
	}
	
	public String getLatitude() {
		return mediaLatitude;
	}
	
	public void setLongitude(String mediaLongitude) {
		this.mediaLongitude = mediaLongitude;
	}
	
	public String getLongitude() {
		return mediaLongitude;
	}
	
	public void setThumbUri(String mediaThumbURL) {
		this.mediaThumbUri = mediaThumbURL;
	}
	
	public String getThumbURL() {
		return mediaThumbUri;
	}
	
	public void setType(String mediaType) {
		this.mediaType = mediaType;
	}
	
	public String getType() {
		return mediaType;
	}
	
	public String getUriString() {
		return parsedUri;
	}
	
	public void setUri(Uri uri) {
		this.parsedUri = uri.toString();
		if(name == null & nameThumbnail == null){
			initName(uri.toString());
		}
	}
	
	/**
	 * Init file name, thumb nail from a given path.
	 * @param path
	 */
	private void initName(String path){
		int random = 1000 + new Random().nextInt(8990);
		name = System.currentTimeMillis() + "_" + random;
		nameThumbnail = System.currentTimeMillis() + "_" + (random + 5);
		
		int extensionIndex = path.lastIndexOf(".");
		if(extensionIndex != -1){
			String extString = path.substring(extensionIndex );
			if(".jpeg".equalsIgnoreCase(extString)){
				extString = ".jpg";
			}
			name += extString.toLowerCase(Locale.US);
			nameThumbnail += extString.toLowerCase(Locale.US);
		}
	}
	
	
	public String getFilePath() {
		return filePath;
	}
	
	public String getName() {
		return name;
	}
	
	public String getNameThumbnail() {
		return nameThumbnail;
	}
	
	public void setUri(String uriString){
		setUri(Uri.parse(uriString));
	}
	
	public Uri getUri() {
		return parsedUri == null?null:Uri.parse(parsedUri);
	}
	
	public void setFilePathThumb(String filePathThumb) {
		this.filePathThumb = filePathThumb;
	}
	
	public String getFilePathThumb() {
		return filePathThumb;
	}
	
	public JSONObject toJson(){
		JSONObject json =  new JSONObject();
		
		try{
			if(mediaID != null){
				json.put("mediaID", mediaID);
			}
			
			if(mediaThumbUri != null){
				json.put("mediaThumbURL", mediaThumbUri);
			}
	
			if(parsedUri != null){
				json.put("mediaURL", parsedUri);
			}
	
			if(mediaType != null){
				json.put("mediaType", mediaType);
			}
	
			if(mediaLatitude != null){
				json.put("mediaLatitude", mediaLatitude);
			}
	
			if(mediaLongitude != null){
				json.put("mediaLongitude", mediaLongitude);			
			}
			
			if(parsedUri != null){
				json.put("uri", parsedUri);
			}
			
			if(filePath != null){
				json.put("filePath", filePath);
			}
			
			if(filePathThumb != null){
				json.put("filePathThumb", filePathThumb);
			}
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "convert Media to json error", e);
		}
		
		return json;
	}
}
