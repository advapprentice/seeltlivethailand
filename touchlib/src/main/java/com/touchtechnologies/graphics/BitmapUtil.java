package com.touchtechnologies.graphics;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.io.FileUtil;
import com.touchtechnologies.json.JSONUtil;

public class BitmapUtil {
	
	/**
	 * Get Bitmap information without decode a real image.
	 * {
	 *  "width":,
	 *  "height":,
	 *  "mime":
	 * }
	 * 
	 * @param file
	 * @return json object containing bitmap information
	 */
	public static JSONObject getBitmapInfo(String file,Context context,Uri uri){
		JSONObject json = new JSONObject();
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;

		//Returns null, sizes are in the options variable
		try {
			InputStream in = context.getApplicationContext().getContentResolver().openInputStream(uri);
			BitmapFactory.decodeStream(in, null, options);
		} catch (Exception e) {
			Log.e("", "getBitmapInfo: ",e );
			// do something
		}
		//BitmapFactory.decodeFile(file, options);
		int width = options.outWidth;
		int height = options.outHeight;
		
		//MIME type
		String type = options.outMimeType;		
		try{
			json.put("width", width);
			json.put("height", height);
			json.put("mime", type);
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, e.getMessage(), e);
		}
		
		return json;
	}
	
	
	/**
	 * @deprecated
	 * @param context
	 * @param uri
	 * @param options
	 * @return
	 * @throws IOException
	 */
	public static Bitmap getBitmap_(Context context, Uri uri, BitmapFactory.Options options) throws IOException{
		int reqWidth = options.outWidth;
		int reqHeight = options.outHeight;
		options.inPreferredConfig = Config.RGB_565;
		
		Log.d(LibConfig.LOGTAG, "requested bitmap width " + reqWidth + " height " + reqHeight);
		
		
		Bitmap bitmap = null;
		InputStream ins = null;		
		try{
			ins = context.getApplicationContext().getContentResolver().openInputStream(uri);
			bitmap = BitmapFactory.decodeStream(ins, null, options);			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "decode stream bitmap error", e);
		}finally{
			if(ins != null){
				ins.close();
			}
		}		
		
		if(bitmap.getWidth() != reqWidth
				|| bitmap.getHeight() != reqHeight){
			bitmap = Bitmap.createScaledBitmap(bitmap, reqWidth, reqHeight, false);
		}
		
		//detect image orientation
		ExifInterface exifInterface = new ExifInterface(FileUtil.getFilePath(context, uri));
		int orientation = Integer.parseInt(exifInterface.getAttribute(ExifInterface.TAG_ORIENTATION));
		Matrix matrix = null;
		switch (orientation) {
//		case ExifInterface.ORIENTATION_NORMAL:
//			matrix.postRotate(90);
//
//			break;
		case ExifInterface.ORIENTATION_ROTATE_180:
			matrix = new Matrix();
			matrix.postRotate(180);

			break;
		case ExifInterface.ORIENTATION_ROTATE_270:
			matrix = new Matrix();
			matrix.postRotate(270);

			break;
		case ExifInterface.ORIENTATION_ROTATE_90:
			matrix = new Matrix();
			matrix.postRotate(90);
	       
			break;	
		}
		
		if(matrix != null){
			bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
		}
		
		System.gc();
		
		
		return bitmap;
	}
	
	public static Bitmap getBitmap(Context context, String filePath, BitmapFactory.Options options) throws IOException{
		Log.d(LibConfig.LOGTAG, "requested bitmap width " + options.outWidth + " height " + options.outHeight);
				
		Bitmap bitmap = null;
		InputStream ins = null;		
		try{
			bitmap  = BitmapFactory.decodeFile(filePath, options);
					
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "decode stream bitmap error", e);
		}finally{
			if(ins != null){
				ins.close();
			}
		}		
		
		if(bitmap.getWidth() != options.outWidth
				|| bitmap.getHeight() != options.outHeight){
			bitmap = Bitmap.createScaledBitmap(bitmap, options.outWidth, options.outHeight, false);
		}
		
		//detect image orientation
		ExifInterface exifInterface = new ExifInterface(filePath);
		int orientation = Integer.parseInt(exifInterface.getAttribute(ExifInterface.TAG_ORIENTATION));
		Matrix matrix = null;
		switch (orientation) {
		case ExifInterface.ORIENTATION_ROTATE_180:
			matrix = new Matrix();
			matrix.postRotate(180);

			break;
		case ExifInterface.ORIENTATION_ROTATE_270:
			matrix = new Matrix();
			matrix.postRotate(270);

			break;
		case ExifInterface.ORIENTATION_ROTATE_90:
			matrix = new Matrix();
			matrix.postRotate(90);
	       
			break;	
		}
		
		if(matrix != null){
			bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
		}
		
		System.gc();
		
		
		return bitmap;
	}
	
	public static Bitmap getBitmap(Context context, Uri uri, int requestWidth, int requestHeight) throws IOException{
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.outWidth = requestWidth;
		options.outHeight = requestHeight;
		options.inSampleSize = calculateInSampleSize(context, FileUtil.getFilePath(context, uri), requestWidth, requestHeight);
		options.inPreferredConfig = Config.RGB_565;
				
		Log.d(LibConfig.LOGTAG, "requested bitmap width " + requestWidth + " height " + requestHeight);
				
		Bitmap bitmap = null;
		InputStream ins = null;		
		try{
			ins = context.getApplicationContext().getContentResolver().openInputStream(uri);
			bitmap = BitmapFactory.decodeStream(ins, null, options);			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "decode stream bitmap error", e);
		}finally{
			if(ins != null){
				ins.close();
			}
		}		
		
		if(bitmap.getWidth() != requestWidth
				|| bitmap.getHeight() != requestHeight){
			bitmap = Bitmap.createScaledBitmap(bitmap, requestWidth, requestHeight, false);
		}
		
		//detect image orientation
		ExifInterface exifInterface = new ExifInterface(FileUtil.getFilePath(context, uri));
		int orientation = Integer.parseInt(exifInterface.getAttribute(ExifInterface.TAG_ORIENTATION));
		Matrix matrix = null;
		switch (orientation) {
		case ExifInterface.ORIENTATION_ROTATE_180:
			matrix = new Matrix();
			matrix.postRotate(180);

			break;
		case ExifInterface.ORIENTATION_ROTATE_270:
			matrix = new Matrix();
			matrix.postRotate(270);

			break;
		case ExifInterface.ORIENTATION_ROTATE_90:
			matrix = new Matrix();
			matrix.postRotate(90);
	       
			break;	
		}
		
		if(matrix != null){
			bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
		}
		
		System.gc();
		
		
		return bitmap;
	}
	
	/**
	 * Calculate aspect ratio width/height
	 * @param context
	 * @param uri
	 * @return
	 */
	public static double getAspectRatio(Context context, Uri uri){
		double ratio = 1.0;
		
		String filePath = FileUtil.getFilePath(context, uri);		
		JSONObject imgInfo = BitmapUtil.getBitmapInfo(filePath,context,uri);
		try{
			float width = imgInfo.getInt("width");
			float height = imgInfo.getInt("height");
	
			ratio = height / width;
			if(ratio == 0){
				ratio =1.0;
			}
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "get aspect ratio error", e);
		}		
		
		return ratio;
	}
	
	public static int calculateInSampleSize(Context context, String file, int reqWidth, int reqHeight) {
		// Raw height and width of image
		JSONObject jsonObject = getBitmapInfo(file,context,Uri.parse(file));
		
		int height = JSONUtil.getInt(jsonObject, "width");
		int width  = JSONUtil.getInt(jsonObject, "height");
		
		int inSampleSize = 1;
		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

    return inSampleSize;
}

	/**
	 * @deprecated
	 * @param context
	 * @param uri
	 * @return
	 * @throws IOException
	 */
	private static Bitmap getBitmap(Context context, Uri uri) throws IOException{
		return MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
	}
	
	/**
	 * @deprecated
	 * @param context
	 * @param options
	 * @return
	 */
	private Bitmap getBitmapThumbnail(Context context, Options options){
		return MediaStore.Images.Thumbnails.getThumbnail(context.getContentResolver(), 0, 0, options);
	}
	
	/**
	 * Get bitmap from URL
	 * @param context
	 * @param _url
	 * @return bitmap or null
	 * @throws IOException
	 */
	public static Bitmap getBitmap(Context context, URL url, Options option) throws IOException{
		Bitmap bitmap = null;
		InputStream ins = null;
		
		try{		
			HttpURLConnection urlCon = (HttpURLConnection)url.openConnection();
			urlCon.setDoInput(true);
			urlCon.connect();
			if(urlCon.getResponseCode() == 200){
				ins = urlCon.getInputStream();
				bitmap = BitmapFactory.decodeStream(ins, null, option);
			}
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "get bitmap from url error");
		}finally{
			if(ins != null){
				ins.close();
			}
		}
		
		return bitmap;
	}
	
	public static int sizeOf(Bitmap bitmap) {
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//	        return bitmap.getAllocationByteCount();
//	    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
//	        return bitmap.getByteCount();
//	    } else {
	        return bitmap.getRowBytes() * bitmap.getHeight();
//	    }
    }
}
