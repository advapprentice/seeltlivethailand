package com.touchtechnologies.graphics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.Serializable;


public class MediaHolder implements Serializable{
	private Context context;
	public Media media;
	public BitmapFactory.Options options;
	
//	/** @see Media.TYPE_ */
// 	public String mediaType;
	
//	/**
//	 * In cases of selected from gallery or take a picture using camera, a picture is in an uri form.
//	 */
//	private Uri uri;
	/**
	 * In a case of video preview, a bitmap was generated in memory
	 */
	
	public Bitmap bitmap;
	
//	/**
//	 * A media file name.
//	 */
//	private String name;
//	private String nameThumbnail;
//	private String path;
	
	public MediaHolder(Context context) {
		this.context = context;
	}
	
//	/**
//	 * Get Uri path 
//	 * @return
//	 */
//	public String getFilePath(){
//		return path;
//	}
	
//	public Uri getUri(){
//		return uri;
//	}
	
//	public String getUriScheme(){
//		Uri uri = media.getUri();
//		
//		if(uri == null){
//			return "";
//			
//		}else if(uri.getScheme().indexOf("http") != -1){
//			return uri.toString();
//			
//		} else{
//			
//			String path = uri.getPath();
//			
//			if("content".equals(uri.getScheme())){
//				path = "content://media" + File.separator + uri.getPath();
//			}
//			Log.d(LibConfig.LOGTAG, path);
//			
//			return path;
//		}
//	}
	
//	/**
//	 * Set Uri.
//	 * File path, name and thumbnail will be set using a give uri path.
//	 * @param uri
//	 */
//	public void setUri(Uri uri) {
//		this.uri = uri;
//		
//		if(uri.getScheme().indexOf("http") != -1){
//			return;
//		}
//		
//		path = com.touchtechnologies.io.FileUtil.getFilePath(context, uri);
//		
//		int random = 1000 + new Random().nextInt(8990);
//		name = System.currentTimeMillis() + "_" + random;
//		nameThumbnail = System.currentTimeMillis() + "_" + (random + 5);
//		
//		int extensionIndex = path.lastIndexOf(".");
//		if(extensionIndex != -1){
//			String extString = path.substring(extensionIndex );
//			if(".jpeg".equalsIgnoreCase(extString)){
//				extString = ".jpg";
//			}
//			name += extString.toLowerCase();
//			nameThumbnail += extString.toLowerCase();
//		}
//	}
	
//	public String getThumbFilename(){
//		return nameThumbnail;
//	}
	
//	/**
//	 * Get file name from a MediaHolder's uri
//	 * @return
//	 */
//	public String getFilename(){
//		return name;
//	}
}