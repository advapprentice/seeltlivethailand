package com.touchtechnologies.graphics;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore.Video.Thumbnails;
import android.util.Log;
import android.widget.Toast;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.io.FileUtil;

public class MediaPicked {
	public static final int ACTION_IMAGE_CAPTURE = 4;
	public static final int ACTION_VIDEO_CAPTURE = 6;
	public static final int ACTION_AUDIO_CAPTURE = 7;
		
	public static MediaHolder getMediaHolder(Context context, Uri uri, String mime){
		if(uri.getScheme().indexOf("http") != -1){
			MediaHolder mediaHolder = new MediaHolder(context);
			Media media = new Media(context, uri);
			mediaHolder.media = media;
			
			if(Media.TYPE_IMAGE.equals(mime)){
				media.mediaType = Media.TYPE_IMAGE;
				
			}else if(Media.TYPE_VIDEO.equals(mime)){
				media.mediaType = Media.TYPE_VIDEO;
				
			}else if(Media.TYPE_AUDIO.equals(mime)){
				media.mediaType = Media.TYPE_AUDIO;
			}
			return mediaHolder;
			
			
		}else{		
			if(Media.TYPE_IMAGE.equals(mime)){
				return getResult(context, uri, ACTION_IMAGE_CAPTURE);
				
			}else if(Media.TYPE_VIDEO.equals(mime)){
				return getResult(context, uri, ACTION_VIDEO_CAPTURE);
				
			}else if(Media.TYPE_AUDIO.equals(mime)){
				return getResult(context, uri, ACTION_AUDIO_CAPTURE);
			}
		}
		
		return null;
	}
	
	public static MediaHolder getResult(Context context, Uri uri, int action){
		MediaHolder mediaHolder = new MediaHolder(context);
		Media media = new Media(context, uri);
		
		String fileName;		
		Bitmap bitmap;
		switch(action){
		case ACTION_IMAGE_CAPTURE:
			Bitmap bmpThumb = null;
			try {
				// get image info
				//get image info
				int outHeigth = 400;
				int outWidth  = (int)( outHeigth / BitmapUtil.getAspectRatio(context, uri));

				bmpThumb = BitmapUtil.getBitmap(context, uri, outWidth, outHeigth);				
			} catch (Exception e) {
				Log.e(LibConfig.LOGTAG, "get bitmap error", e);
			}

			Log.d(LibConfig.LOGTAG, "Bitmap width " + bmpThumb.getWidth() + " height " + bmpThumb.getHeight());

			mediaHolder.bitmap = bmpThumb;
			media.mediaType = Media.TYPE_IMAGE;
			mediaHolder.media = media;
			
			break;
		case ACTION_VIDEO_CAPTURE:
			String path = FileUtil.getFilePath(context, uri);
			//
			bitmap = ThumbnailUtils.createVideoThumbnail(path, Thumbnails.MINI_KIND);
			// android.provider.MediaStore.Video.Media.
			mediaHolder.bitmap = bitmap;
			media.mediaType = Media.TYPE_VIDEO;
			mediaHolder.media = media;
			
			fileName = mediaHolder.media.nameThumbnail;
			if(fileName != null && fileName.lastIndexOf('.') != -1){
				fileName = fileName.substring(0, fileName.lastIndexOf('.'));
				mediaHolder.media.nameThumbnail = fileName + ".jpg";
			}
			break;
		case ACTION_AUDIO_CAPTURE:
			media.mediaType = Media.TYPE_AUDIO;
			mediaHolder.media = media;
			
//			Toast.makeText(context, "Saved: " + uri.getPath(), Toast.LENGTH_LONG).show();
			break;
		}
		
		return mediaHolder;
	}
}
