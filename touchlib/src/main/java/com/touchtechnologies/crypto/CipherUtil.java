package com.touchtechnologies.crypto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Hashtable;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

import org.apache.http.HttpResponse;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONObject;

import android.util.Base64;

import com.touchtechnologies.command.Command.PARAMS;
import com.touchtechnologies.json.JSONUtil;
import com.touchtechnologies.log.Log;

public final class CipherUtil {
	private static CipherUtil cipherUtil;
	private static final int SALT_LENGTH = 8;
	private SecretKey KEY;
	private byte[] iv;
	private static Hashtable<String, CipherUtil> hCipher = new Hashtable<String, CipherUtil>();

	private javax.crypto.Cipher cipherEncryptor, cipherDecryptor;

	private CipherUtil(String key) throws GeneralSecurityException{
		byte[] byteKey = key.getBytes();

		DESedeKeySpec keySpec = new DESedeKeySpec(byteKey);
		SecretKeyFactory skeyFactory = SecretKeyFactory.getInstance("DESede");
		KEY = skeyFactory.generateSecret(keySpec);

		iv = getSalt();
		cipherEncryptor = javax.crypto.Cipher
				.getInstance("DESede/CBC/PKCS5Padding");
		cipherEncryptor.init(Cipher.ENCRYPT_MODE, KEY, new IvParameterSpec(iv));

		cipherDecryptor = javax.crypto.Cipher
				.getInstance("DESede/CBC/PKCS5Padding");

		cipherDecryptor.init(Cipher.DECRYPT_MODE, KEY, new IvParameterSpec(
				cipherEncryptor.getIV()));
	}
	
	private CipherUtil(String key, String base64Salt) throws GeneralSecurityException{
		byte[] byteKey = key.getBytes();

		DESedeKeySpec keySpec = new DESedeKeySpec(byteKey);
		SecretKeyFactory skeyFactory = SecretKeyFactory.getInstance("DESede");
		KEY = skeyFactory.generateSecret(keySpec);
		
		String b64 = new String(Base64.decode(base64Salt, Base64.DEFAULT));
		
		iv = b64.getBytes();
		cipherEncryptor = javax.crypto.Cipher
				.getInstance("DESede/CBC/PKCS5Padding");
		cipherEncryptor.init(Cipher.ENCRYPT_MODE, KEY, new IvParameterSpec(iv));

		cipherDecryptor = javax.crypto.Cipher
				.getInstance("DESede/CBC/PKCS5Padding");

		cipherDecryptor.init(Cipher.DECRYPT_MODE, KEY, new IvParameterSpec(
				cipherEncryptor.getIV()));
		
	}
	
	
	public static CipherUtil getInstance(String key) throws GeneralSecurityException {
		cipherUtil = hCipher.get(key);
		
		if (cipherUtil == null) {
			cipherUtil = new CipherUtil(key);
			
			hCipher.put(key, cipherUtil);
		}

		return cipherUtil;
	}
	
	public static CipherUtil getInstance(String key, String base64Salt) throws GeneralSecurityException {
		cipherUtil = new CipherUtil(key, base64Salt);
		
		return cipherUtil;
	}


	public String encrypt(JSONObject json) throws GeneralSecurityException, IOException {
		String encryptReqMsg = null;
		try {
			JSONObject jsonMessage = json.getJSONObject(PARAMS.data.toString());
			
			String jsonMsgString = jsonMessage.toString();
			byte[] encrypted = cipherEncryptor.doFinal(jsonMsgString.getBytes());
			byte[] data = Base64.encode(encrypted, Base64.DEFAULT);
			jsonMsgString = new String(data);			
			json.put(PARAMS.data.toString(), jsonMsgString.trim());
			json.put(PARAMS.mask.toString(), new String(Base64.encode(iv, Base64.DEFAULT)).trim());			
			encryptReqMsg = new String(json.toString());

		} catch (Exception e) {
			throw new IOException(e.getMessage());
		}

		return encryptReqMsg;
	}

	public String decrypt(String msg) throws GeneralSecurityException, IOException {
		String decryptMsg = null;
		
		try {
			
			byte[] decode = Base64.decode(msg, Base64.DEFAULT);
			byte[] data = cipherDecryptor.doFinal(decode);

			decryptMsg = new String(data);
			
//			android.util.Log.d(LibConfig.LOGTAG, decryptMsg);

		} catch (Exception e) {
			throw new IOException(e.getMessage());
		}

		return decryptMsg;
	}

	public JSONObject decrypt(InputStream is) throws GeneralSecurityException,
			IOException {
		JSONObject json = null;
		try {
			ByteArrayBuffer buff = new ByteArrayBuffer(Byte.MAX_VALUE);
			int c;
			while ((c = is.read()) != -1) {
				buff.append(c);
			}

			byte[] data = buff.toByteArray();
			String tmp = new String(data);
			tmp = decrypt(tmp);
			json = new JSONObject(tmp);
		} catch (Exception e) {
			throw new IOException(e.getMessage());
		}

		return json;
	}
	
	/**
	 * Decrypting an encrypted text from a give InputStream. 
	 * @param in InputStream to read
	 * @return decrypted text in JSONObject format.
	 */
	public JSONObject getJSONResponse(InputStream in){
		JSONObject jsonResponse = null;
		
		try {
			// Start the query
			StringBuilder sb = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}			
			
			String responseText = sb.toString();
			Log.println("Response <--" + responseText);			
			
			if(responseText.startsWith("{")){//only a JSON "data" was encrypted
				JSONObject tmpJson = new JSONObject(responseText);
				if("0".equals(JSONUtil.getString(tmpJson, "status"))){
					
					String data = JSONUtil.getString(tmpJson, "data");				
					if(data != null && data.length() > 0){
						JSONObject dataJson = new JSONObject(decrypt(data));
						tmpJson.put("data", dataJson);
					}	
				}
				
				jsonResponse = tmpJson;
				
			}else{//a whole response was encrypted
				responseText = decrypt(responseText);
				jsonResponse = new JSONObject(responseText);
				
			}			
			
			Log.println("to JSON -> " + jsonResponse);
			Log.logToFile(Log.ROOT_PATH, "to json:" + jsonResponse);
			
		} catch (Exception e) {
			// handle the exception !
			Log.log("getJSONResponse from InputStream: " + in, e);
		}finally{
			if(in != null){
				try{
					in.close();
				}catch(Exception ee){					
				}
			}			
		}
		
		return jsonResponse;		
	}
	
	/**
	 * Decrypting an encrypted text from a given HttpResponse. 
	 * @param response HttpResponse to read
	 * @return decrypted text in JSONObject format.
	 */
	public JSONObject getJSONResponse(HttpResponse response) {
		JSONObject jsonResponse = null;
		try{
			jsonResponse = getJSONResponse(response.getEntity().getContent());
		}catch (Exception e) {
			// handle the exception !
			Log.log("getJSONResponse from HttpResponse: " + response, e);
		}
		
		return jsonResponse;
	}

	private static byte[] getSalt() {
		byte salt[] = new byte[SALT_LENGTH];
		for (int i = 0; i < SALT_LENGTH; i++) {
			byte ascii = (byte) (97 + (i + Math.random() + "").hashCode() % 25);
			salt[i] = (byte) (ascii);
		}

		return salt;
	}
	
	public void destroy(){
		cipherUtil = null;
		KEY = null;
		iv = null;		
	}
}
