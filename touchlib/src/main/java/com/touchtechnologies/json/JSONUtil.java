package com.touchtechnologies.json;

import org.json.JSONObject;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.log.Log;

public class JSONUtil {
	public static boolean trim;	
	
	public static boolean getBoolean(JSONObject json, String key){
		boolean val = false;
		if(!json.isNull(key)){
			try{
				val = json.getBoolean(key);
			}catch(Exception e){
				Log.log(null, e);
			}
		}
		
		return val;
	}
	
	public static int getInt(JSONObject json, String key){
		int val = -1;
		if(!json.isNull(key)){
			try{
				val = json.getInt(key);
			}catch(Exception e){
				Log.log(null, e);
			}
		}
		
		return val;
	}
	
	public static double getDouble(JSONObject json, String key){
		double val = -1;
		if(!json.isNull(key)){
			try{
				val = json.getDouble(key);
			}catch(Exception e){
				Log.log(null, e);
			}
		}
		
		return val;
	}	
	
	public static long getLong(JSONObject json, String key){
		long val = -1;
		if(!json.isNull(key)){
			try{
				val = json.getLong(key);
			}catch(Exception e){
				Log.log(null, e);
			}
		}
		
		return val;
	}
	
	public static String getString(JSONObject json, String key){
		String val = null;
		if(!json.isNull(key)){
			try{
				val = json.getString(key);
				if(trim){
					val = val.trim();
				}
			}catch(Exception e){
				Log.log(null, e);
			}
		}
		
		return val;
	}
	
	/**
	 * Put json string to a given json if not null, nothing change if a given string is null or empty
	 * @param json
	 * @param key
	 * @param value
	 */
	public static void setStringNotNull(JSONObject json, String key, String value){
		if(value != null && !value.isEmpty()){
			try{
				json.put(key, value);
			}catch(Exception e){
				android.util.Log.e(LibConfig.LOGTAG, e.getMessage());
			}
		}
	}
}
