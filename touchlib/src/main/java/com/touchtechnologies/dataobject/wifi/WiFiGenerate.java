package com.touchtechnologies.dataobject.wifi;

import com.touchtechnologies.json.JSONUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Touch on 10/4/2016.
 */

public class WiFiGenerate extends JSONObject implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4214103737327482642L;

    private String posUsername;
    private String ip;
    private String posPassword;
    private String command;
    private String packageID;
    private String interfaces;
    private String channel;
    private String message;
    public WiFiGenerate(){

    }

    public WiFiGenerate(JSONObject json) throws JSONException {

        if (!json.isNull("PosUsername")) {
            setPosUsername(JSONUtil.getString(json, "PosUsername"));
        }

        if (!json.isNull("PosPassword")) {
            setPosPassword(JSONUtil.getString(json, "PosPassword"));
        }

        if (!json.isNull("IP")) {
            setIp(JSONUtil.getString(json, "IP"));
        }
        if (!json.isNull("PackageID")) {
            setPackageID(JSONUtil.getString(json, "PackageID"));
        }
        if (!json.isNull("Command")) {
            setCommand(JSONUtil.getString(json, "Command"));
        }
        if (!json.isNull("Interface")) {
            setInterfaces(JSONUtil.getString(json, "Interface"));
        }
        if (!json.isNull("Channel")) {
            setChannel(JSONUtil.getString(json, "Channel"));
        }
        if (!json.isNull("Message")) {
            setMessage(JSONUtil.getString(json, "Message"));
        }

    }

    public void setPosUsername(String posUsername) {
        this.posUsername = posUsername;
    }

    public String getPosUsername() {
        return posUsername;
    }

    public void setPosPassword(String posPassword) {
        this.posPassword = posPassword;
    }

    public String getPosPassword() {
        return posPassword;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getIp() {
        return ip;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

    public void setPackageID(String packageID) {
        this.packageID = packageID;
    }

    public String getPackageID() {
        return packageID;
    }

    public void setInterfaces(String interfaces) {
        this.interfaces = interfaces;
    }

    public String getInterfaces() {
        return interfaces;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getChannel() {
        return channel;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public JSONObject toJson(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("PosUsername", getPosUsername());
            jsonObject.put("PosPassword", getPosPassword());
            jsonObject.put("IP", getIp());
            jsonObject.put("PackageID",getPackageID());
            jsonObject.put("Command", getCommand());
            jsonObject.put("Interface",getInterfaces());
            jsonObject.put("Channel", getChannel());
            jsonObject.put("Message",getMessage());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
    @Override
    public String toString() {
        return toJson().toString();
    }
}