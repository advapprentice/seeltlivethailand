package com.touchtechnologies.dataobject;

import java.io.Serializable;

import org.json.JSONObject;

import android.util.Log;

import com.touchtechnologies.json.JSONUtil;

public class Facility implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 0xb8b88964c7877856L;
	private String id;
	private String iconUri;
	private String iconUriInactive;
	private String name;
	private String keyName;
	private int type;
	private final static int type_room = 0 , type_hotel = 1; 
	private boolean attached;
	
	public Facility(JSONObject json){
		//set id
		if(!json.isNull("facility_id")){
			setId(JSONUtil.getString(json, "facility_id"));
			setType(Facility.type_hotel);
		}else if (!json.isNull("room_type_facility_id")) {
			setId(JSONUtil.getString(json, "room_type_facility_id"));
			setType(Facility.type_room);
		}
		//set key name
		setKeyName(JSONUtil.getString(json, "facility_keyname"));
		
		//facility_name_en
		setName(JSONUtil.getString(json, "facility_name_en"));
		
		//is attached to the provider
		setAttached(JSONUtil.getBoolean(json, "attached"));
		
		//icon_image
		if(!json.isNull("images")){
			try{
				JSONObject jsonImage = json.getJSONObject("images").getJSONObject("icons");
				
				setIconUri(JSONUtil.getString(jsonImage, "darken"));
				setIconUriInactive(JSONUtil.getString(jsonImage, "lighten"));
			}catch(Exception e){
				
			}
		}
		
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}
	
	public String getKeyName() {
		return keyName;
	}
	
	public String getIconUri() {
		return iconUri;
	}
	
	public void setIconUri(String iconUri) {
		this.iconUri = iconUri;
	}
	
	public void setIconUriInactive(String iconUriInactive) {
		this.iconUriInactive = iconUriInactive;
	}
	
	public String getIconUriInactive() {
		return iconUriInactive;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public void setAttached(boolean attached) {
		this.attached = attached;
	}
	
	public boolean isAttached() {
		return attached;
	}
	
	public JSONObject toJson(){
		JSONObject jsonObject = new JSONObject();
		
		try{
			if(getType() == type_hotel){
				jsonObject.put("facility_id", getId());
			}else {
				jsonObject.put("room_type_facility_id", getId());
			}
			
			
			jsonObject.put("facility_keyname", getKeyName());
			jsonObject.put("facility_name_en", getName());
			
			
			JSONObject jsonIcon = new JSONObject();
			jsonIcon.put("darken", getIconUri());
			jsonIcon.put("lighten", getIconUriInactive());
			
			JSONObject jsonImage = new JSONObject();
			jsonImage.put("icons", jsonIcon);
			
			jsonObject.put("images", jsonImage);
			
			jsonObject.put("attached", attached);
		}catch(Exception e){
			Log.e("ERR", "Facility", e);
		}
		
		return jsonObject;
	}
	
	@Override
	public boolean equals(Object o) {
		return o instanceof Facility 
				&& ((Facility)o).getKeyName().equals(getKeyName());
	}
}
