package com.touchtechnologies.dataobject;

import org.json.JSONException;
import org.json.JSONObject;

public final class DataItemFactory {
	public static final String PROVIDER_TYPE_KEYNAME = "provider_type_keyname";
	
	public static DataItem newDataItem(JSONObject json) throws JSONException{
		DataItem dataItem = null;
		
		if(json.isNull(PROVIDER_TYPE_KEYNAME)) return null;
		
		String providerKeyName = json.getString(PROVIDER_TYPE_KEYNAME);
		
		if(Restaurant.PROVIDER_TYPE_KEYNAME.equals(providerKeyName)){
			dataItem = new Restaurant(json);
			
		}else if(Hotel.PROVIDER_TYPE_KEYNAME.equals(providerKeyName)){
			dataItem = new Hotel(json);
		}
		
		
		return dataItem;
	}
}
