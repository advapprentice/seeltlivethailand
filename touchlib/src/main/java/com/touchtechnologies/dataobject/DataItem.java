package com.touchtechnologies.dataobject;

import java.io.Serializable;

import com.google.android.gms.maps.model.LatLng;


import android.location.Address;

public class DataItem implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 0xd9999d44e9e73d67L;
	
	private String id;	
	
	/**
	 * Latitude and longitude
	 */
	private double latitude;
	/**
	 * Latitude and longitude
	 */
	private double longitude;
	
	/**
	 * A name of place, point of interest
	 */
	private String name;
	
	/**
	 * Image of this place, point of interest
	 */
	private Image image;

	/**
	 * Post address
	 */
	private Address address;
	
	private String description;
	
	private String descriptionLong;
	
	private String displayAddress;
	private String distance;
	/**
	 * @return the id
	 */
	
	public String getDistance() {
		return distance;
	}
	
	public void setDistance(String distance) {
		this.distance = distance;
	}
	
	
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	
	public void setDescriptionLong(String descriptionLong) {
		this.descriptionLong = descriptionLong;
	}
	
	public String getDescriptionLong() {
		return descriptionLong;
	}
	
	/**
	 * Get latitude and longitude
	 * @return the location
	 */
	public LatLng getLocation() {
		return new LatLng(latitude, longitude);
	}
	

	/**
	 * Set latitude and longitude
	 * @param location
	 */
	public void setLocation(double lat, double lon) {
		this.latitude = lat;
		this.latitude = lon;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setImage(Image image) {
		this.image = image;
	}
	
	public Image getImage() {
		return image;
	}
	
	/**
	 * Set post address
	 * @param address
	 */
	public void setAddress(Address address) {
		this.address = address;
	}
	
	/**
	 * Get post address
	 * @return
	 */
	public Address getAddress() {
		return address;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDisplayAddress(String displayAddress) {
		this.displayAddress = displayAddress;
	}
	
	public String getDisplayAddress() {
		return displayAddress;
	}
}
