package com.touchtechnologies.dataobject;

import java.io.Serializable;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.json.JSONUtil;

public class Restaurant extends Hospitality{
	private static final long serialVersionUID = 0xff06f9d32f84abaL;
	public static final String PROVIDER_TYPE_KEYNAME = "restaurant";
	
	
	
	public Restaurant() {

	}

	public Restaurant(JSONObject json) throws JSONException {
		super(json);
		
	// restaurant gallery	
		String galleryKey ="restaurant_gallery";
		if(!json.isNull(galleryKey)){
			JSONArray galleryArray = json.getJSONArray(galleryKey);
			for(int i =0;i<galleryArray.length(); i++){
				
				Image imageTmp = new Image();
				addImageToGallery(imageTmp);
				
				JSONObject roomJson = galleryArray.getJSONObject(i);
				//last_modified
				
				//hotel_room_type_avg_price
				imageTmp.setThumbnail(JSONUtil.getString(roomJson, "thumbnail"));
				
			}
			
		}
	
		
	}
	
	
	private ArrayList<MenuCategory> arrMenus;
	
	public int getMenuCategoryCount(){
		return arrMenus == null?0:arrMenus.size();
	}
	
	public ArrayList<MenuCategory> getMenuCategories(){
		return arrMenus;
	}
	
	public void addMenus(MenuCategory menus){
		if(arrMenus == null){
			arrMenus = new ArrayList<Restaurant.MenuCategory>();
		}
		
		arrMenus.add(menus);
	}
	
	
	/**
	 * ----------------------------- Menu Category------------------------------------
	 * @author MEe
	 *
	 */
	public static final class MenuCategory implements Serializable{		
		private static final long serialVersionUID = 0x21585c69fc26bc9fL;
		
		ArrayList<FoodMenu> arrMenus;
		
		String name;
		JSONObject jsonObject;
		public MenuCategory(String name) {
			this.name = name;
		}
		
		
		public MenuCategory(JSONObject jsonObject) throws JSONException {
			this.jsonObject = jsonObject;
			JSONArray array = jsonObject.getJSONArray("menus");
			if(array != null){
			for (int i = 0; i < array.length(); i++) {
				JSONObject json = array.getJSONObject(i);
				if(!json.isNull("menu_name_en")){
					FoodMenu menu = new FoodMenu(json.getString("menu_name_en"));
					if(!json.isNull("menu_name_en")){
						menu.setPrice(JSONUtil.getDouble(json, "menu_price"));
					}
					if(!json.isNull("menu_name_en")){
						menu.setImage(new Image(JSONUtil.getString(json, "cover_image")));
					}
					if(!json.isNull("menu_id")){
						menu.setId(JSONUtil.getString(json, "menu_id"));
					}
					if(!json.isNull("spicy_level")){
						menu.setSpicy(JSONUtil.getInt(json, "spicy_level"));
					}
					if(!json.isNull("logo_image")){
						menu.setImage(new Image(json.getString("logo_image")));
								
					}
					if(!json.isNull("menu_description_en")){
						menu.setDescription(JSONUtil.getString(json, "menu_description_en"));
								
					}
						add(menu);
				}
				
				
				
				}
			}else {
				return;
			}
			
		}


		/**
		 * Get number of menu in this category
		 * @return
		 */
		public int getMenuCount(){
			return arrMenus == null?0:arrMenus.size();
		}
		
		public ArrayList<FoodMenu> getMenus(){
			return arrMenus;
		}
		
		public void add(FoodMenu menu){
			if(arrMenus == null){
				arrMenus = new ArrayList<Restaurant.FoodMenu>();				
			}	
			arrMenus.add(menu);
		}
		public FoodMenu getMenuAt(int position){
			return arrMenus.get(position);

		}
	}
	
	
	/**
	 * ----------------------------- Menu ------------------------------------
	 * @author MEe
	 *
	 */
	public static final class FoodMenu implements Serializable{		
		private static final long serialVersionUID = 0x62f3d07907255fb5L;
	
		public enum Hour{
			Breakfast, Lunch, Dinner
		}
		
		public enum Ethic{
			Italian, French, Japanese, Chinese, American, Thai, Indian		
		}
		int spicy = 0;
	public static final int mild =0,middle =1,hot=2;
		String name;
		double price;
		Image image;
		ImageGallery imageGallery;
		String description;
		String id;
				
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		/**
		 * Italian, French, Japanese, Chinese, American, Thai, and Indian
		 */
		Ethic ethnic;
		Hour hour;
		
		
		public FoodMenu(String name) {
			this.name = name;
		}
		
		
		public int getSpicy() {
			return spicy;
		}

		public void setSpicy(int spicy) {
			this.spicy = spicy;
		}

		public void setPrice(double price) {
			this.price = price;
		}
		
		public double getPrice() {
			return price;
		}
		
		public String getName() {
			return name;
		}
		
		public Hour getHour() {
			return hour;
		}
		
		public void setHour(Hour hour) {
			this.hour = hour;
		}
		
		public Ethic getEthnic() {
			return ethnic;
		}
		
		public void setEthnic(Ethic ethnic) {
			this.ethnic = ethnic;
		}
		
		
		public void setImage(Image image) {
			this.image = image;
		}
		
		public Image getImage() {
			return image;
		}
		
		public void setImageGallery(ImageGallery imageGallery) {
			this.imageGallery = imageGallery;
		}
		
		public ImageGallery getImageGallery() {
			return imageGallery;
		}
		
		/**
		 * Get number of image in the gallery
		 * @return
		 */
		public int getImageGalleryCount(){
			return imageGallery == null?0:imageGallery.getImageCount();
		}
		
		public void setDescription(String description) {
			this.description = description;
		}
		
		public String getDescription() {
			return description;
		}
	}
}
