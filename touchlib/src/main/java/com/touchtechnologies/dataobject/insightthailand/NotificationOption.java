package com.touchtechnologies.dataobject.insightthailand;

/**
 * Created by TouchICS on 6/10/2016.
 */
import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.json.JSONUtil;


/**
 * Created by TouchICS on 5/31/2016.
 */
public class NotificationOption implements Serializable {

    private int id;
    private int notifiableId;
    private String notifiableType;
    private String isRead;
    private String type;


    public NotificationOption(){

    }

    public NotificationOption(JSONObject json) throws JSONException {

        if (!json.isNull("id")) {

            setId(json.getInt("id"));
        }
        if (!json.isNull("notifiable_id")) {

            setNotifiableId(json.getInt("notifiable_id"));
        }

        if (!json.isNull("notifiable_type")) {

            setNotifiableType(json.getString("notifiable_type"));
        }
        if (!json.isNull("is_read")) {

            setIsRead(json.getString("is_read"));
        }
        if (!json.isNull("type")) {

            setType(json.getString("type"));
        }



    }


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setNotifiableId(int notifiableId) {
        this.notifiableId = notifiableId;
    }

    public int getNotifiableId() {
        return notifiableId;
    }

    public void setNotifiableType(String notifiableType) {
        this.notifiableType = notifiableType;
    }

    public String getNotifiableType() {
        return notifiableType;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public JSONObject toJson(){
        JSONObject jUserProperties = new JSONObject();
        try{
            jUserProperties = new JSONObject();

            if(id != 0){
                jUserProperties.put("id", getId());
            }

        }catch(Exception e){
            Log.e(LibConfig.LOGTAG, "Report toJson error " + e.getMessage(), e);
        }

        return jUserProperties;
    }

    @Override
    public String toString() {
        return toJson().toString();
    }

}