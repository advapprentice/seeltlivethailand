package com.touchtechnologies.dataobject.insightthailand;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.json.JSONUtil;

public class Address implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4869379510150242689L;
	private String provinceID;
	private String provinceName;
	
	public Address(){
	}
	
	public Address(String json) throws JSONException{
		this(new JSONObject(json));
	}
	
	public Address(JSONObject json) throws JSONException{
		if(!json.isNull("provinceID")){
			setProvinceID(JSONUtil.getString(json, "provinceID"));
		}
		
		if(!json.isNull("provinceName")){
			setProvinceName(JSONUtil.getString(json, "provinceName"));
		}
	}
	
	public JSONObject toJson() throws JSONException{
		JSONObject json = new JSONObject();
		
		if(provinceID != null){
			json.put("provinceID", provinceID);
		}
		
		if(provinceName != null){
			json.put("provinceName", provinceName);
		}
		
		return json;
	}
	
	public void setProvinceID(String provinceID) {
		this.provinceID = provinceID;
	}
	
	public String getProvinceID() {
		return provinceID;
	}
	
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	
	public String getProvinceName() {
		return provinceName;
	}
}
