package com.touchtechnologies.dataobject.insightthailand;

import com.touchtechnologies.json.JSONUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by TouchICS on 9/2/2016.
 */
public class PoiGallery  implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 749548868907640918L;


    private String thumbnail;

    public PoiGallery(JSONObject json) throws JSONException {

        if (!json.isNull("thumbnail")) {
            setThumbnail(JSONUtil.getString(json, "thumbnail"));

        }

    }



    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }
}

