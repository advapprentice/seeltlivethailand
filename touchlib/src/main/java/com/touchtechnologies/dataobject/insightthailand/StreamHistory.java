package com.touchtechnologies.dataobject.insightthailand;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.json.JSONUtil;

public class StreamHistory implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 749548868907640918L;

	/**
	 * id_provider_information_tag_keyname
	 */
	private String id;
	/**
	 * provider_information_tag_keyname
	 */
	private String streamingID;
	/**
	 * provider_information_tag_name
	 */
	private String urls;
	
	/**
	 * ROI's category
	 */
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
	static SimpleDateFormat sdfOut = new SimpleDateFormat("d MMM yyyy  H:mm", new Locale("en_EN"));
	static Calendar calendar = Calendar.getInstance(new Locale("en_EN"));
	
	private String channelID;
	private String title;
	private String note;
	private String startStreamingDatetime;
	private String finishStreamingDatetime;
	private String icon_category;
	private Double lat;
	private Double lng;
	private int Category_id;
	private long created;
	private long updated;
	private long time_duration;
	private String rtsp;
	private String snapshotsmedium;
	private String weburl;
	private String http;
	private String categoryName;
	private String commentcount;
	private int love;
	private int ispublic;
	private boolean isLove;
	private ArrayList<Comment> comments;

	private String watchedCount;
	private final String KEY_ID = "id_stream";
	private final String KEY_STREAMINGID = "streamingID";
	private final String KEY_CHANNELID = "channelID";
	private final String KEY_TITLE = "title";
	private final String KEY_NOTE = "note";
	private final String KEY_STARTSTREAMINGDATE = "startStreamingDatetime";
	private final String KEY_FINISHSTREAMINGDATE = "finishStreamingDatetime";
	private final String KEY_DURATION = "streamDurationAsSecond";
	private final String KEY_RTSP = "rtsp";
	private final String KEY_HTTP = "http";
	private final String KEY_SNAPSHOTS = "snapshots";
	private final String KEY_SNAPSHOTSMEDIUM = "640x480";
	private final String KEY_LIST_SNAPSHOTSMEDIUM ="list_snapshot";
	private final String KEY_LATITUDE = "latitude";
	private final String KEY_LONGITUDE ="longitude";
	private final String KEY_URLS = "urls";
	private final String KEY_LIST_URLS = "list_url";
	private final String KEY_CREATEDATE = "createdate";
	private final String KEY_UPDATEDATE = "updatedate";
	private final String KEY_USER = "user";
	private final String KEY_WATCHEDCOUNT = "watchedCount";
	private final String KEY_LOVECOUNT = "loves_count";
	private final String KEY_ISLOVE = "is_loved";
	private final String KEY_ISPUBLIC = "public";
	private final String KEY_CATEGORY = "category_stream";
	private final String KEY_CATEGORY_NAME= "category_name_en";
	private final String KEY_CATEGORY_ICON = "icon_category";
	private final String KEY_CATEGORY_ID = "category_id";
	private final String KEY_COMMENTCOUNT = "count_comment";


	private User user;
	public StreamHistory(JSONObject json) throws JSONException{
		if(!json.isNull(KEY_ID)){
			setId(JSONUtil.getString(json, KEY_ID));
		}
		
		if(!json.isNull(KEY_STREAMINGID)){
			setStreamingID(JSONUtil.getString(json, KEY_STREAMINGID));
		}
		
		if(!json.isNull(KEY_CHANNELID)){
			setChannelID(JSONUtil.getString(json, KEY_CHANNELID));
		}
		
		if(!json.isNull(KEY_TITLE)){
			setTitle(JSONUtil.getString(json, KEY_TITLE));
		}
		if(!json.isNull("web_url")){
			setWeburl(JSONUtil.getString(json, "web_url"));
		}
		
		if(!json.isNull(KEY_NOTE)){
			setNote(JSONUtil.getString(json, KEY_NOTE));
		}
		
		if(!json.isNull(KEY_STARTSTREAMINGDATE)){
			setStartStreamingDatetime(JSONUtil.getString(json, KEY_STARTSTREAMINGDATE));
		}
		
		if(!json.isNull(KEY_FINISHSTREAMINGDATE)){
			setFinishStreamingDatetime(JSONUtil.getString(json, KEY_FINISHSTREAMINGDATE));
		}
		if(!json.isNull(KEY_WATCHEDCOUNT)){
			setWatchedCount(JSONUtil.getString(json, KEY_WATCHEDCOUNT));
		}
		if(!json.isNull(KEY_LOVECOUNT)){
			setLove(JSONUtil.getInt(json, KEY_LOVECOUNT));
		}

		if(!json.isNull(KEY_ISLOVE)){
			setIsLove(JSONUtil.getBoolean(json, KEY_ISLOVE));
		}

		if(!json.isNull(KEY_ISPUBLIC)){
			setIspublic(JSONUtil.getInt(json,KEY_ISPUBLIC));
		}
		if(!json.isNull(KEY_CATEGORY_ID)){
			// TODO: 25/4/2559
			setCategory_id(JSONUtil.getInt(json,KEY_CATEGORY_ID));
		}

		if(!json.isNull(KEY_CATEGORY)){
				try{
					JSONObject jsonCategory = json.getJSONObject(KEY_CATEGORY);
					//"Form"
					setCategoryName(JSONUtil.getString(jsonCategory,KEY_CATEGORY_NAME));
					setIcon_category(JSONUtil.getString(jsonCategory,KEY_CATEGORY_ICON));
			
				}catch(Exception e){
					
				}
			}

		if(!json.isNull(KEY_LIST_SNAPSHOTSMEDIUM)){
			try{
				JSONObject jsonUrls = json.getJSONObject(KEY_LIST_SNAPSHOTSMEDIUM);
				//"Form"  
				setSnapshots(JSONUtil.getString(jsonUrls,KEY_SNAPSHOTSMEDIUM));
		
			}catch(Exception e){
				
			}
		}
		if(!json.isNull(KEY_SNAPSHOTS)){
			try{
				JSONObject jsonUrls = json.getJSONObject(KEY_SNAPSHOTS);
				//"Form"
				setSnapshots(JSONUtil.getString(jsonUrls,KEY_SNAPSHOTSMEDIUM));

			}catch(Exception e){

			}
		}

		
		if(!json.isNull(KEY_URLS)){
			try{
				JSONObject jsonUrl = json.getJSONObject(KEY_URLS);
				//"Form"
				if (!jsonUrl.isNull(KEY_RTSP)) {
					setRtsp(JSONUtil.getString(jsonUrl,KEY_RTSP));
				}
				if (!jsonUrl.isNull(KEY_HTTP)) {
					setHttp(JSONUtil.getString(jsonUrl,KEY_HTTP));
				}

			}catch(Exception e){
				
			}
		}
		
		if(!json.isNull(KEY_LIST_URLS)){
			try{
				JSONObject jsonUrl = json.getJSONObject(KEY_LIST_URLS);
				//"Form"  
				setRtsp(JSONUtil.getString(jsonUrl,KEY_RTSP));
				setHttp(JSONUtil.getString(jsonUrl,KEY_HTTP));
		
			}catch(Exception e){
				
			}
		}
	

		if(!json.isNull(KEY_USER)){
				try{
					if(json.getJSONObject("user").length() > 0){
						setUser(new User(json.getJSONObject("user")));
					}
					
				}catch(Exception e){
					Log.e("user", e.getMessage(), e);
				}
			}
		if(!json.isNull(KEY_LATITUDE) && !json.isNull(KEY_LONGITUDE)){
			if(JSONUtil.getDouble(json,KEY_LONGITUDE)==0 && JSONUtil.getDouble(json,KEY_LATITUDE)==0){
				//setLocation(null);
				setLat(null);
				setLng(null);
			}else {
				//setLocation(new LatLng(JSONUtil.getDouble(json,KEY_LATITUDE),JSONUtil.getDouble(json,KEY_LONGITUDE)));
				setLng(JSONUtil.getDouble(json,KEY_LONGITUDE));
				setLat(JSONUtil.getDouble(json,KEY_LATITUDE));
			}

		}
		

		if(!json.isNull(KEY_CREATEDATE)){
			try{
				String dateString = JSONUtil.getString(json, KEY_CREATEDATE);
				if(dateString.indexOf(":") != -1){
					setCreatedtDates(sdf.parse(dateString).getTime());
				}else{
					setCreatedtDates(Long.parseLong(dateString));
				}
			}catch(Exception e){				
			}
		}		
		
		if(!json.isNull(KEY_UPDATEDATE)){
			try{
				String dateString = JSONUtil.getString(json, KEY_UPDATEDATE);
				if(dateString.indexOf(":") != -1){
					setUpDates(sdf.parse(dateString).getTime());
				}else{
					setUpDates(Long.parseLong(dateString));
				}
			}catch(Exception e){				
			}
		}
		if(!json.isNull(KEY_DURATION)){
			setTime_duration(JSONUtil.getLong(json,KEY_DURATION));
		}
		if(!json.isNull(KEY_COMMENTCOUNT)){
			setCommentcount(JSONUtil.getString(json,KEY_COMMENTCOUNT));
		}
		
		
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

	public void setStreamingID(String streamingID) {
		this.streamingID = streamingID;
	}
	public String getStreamingID() {
		return streamingID;
	}
	
	
	public void setChannelID(String channelID) {
		this.channelID = channelID;
	}
	public String getChannelID() {
		return channelID;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle() {
		return title;
	}
	
	public void setNote(String note) {
		this.note = note;
	}
	public String getNote() {
		return note;
	}
	
	public void setStartStreamingDatetime(String startStreamingDatetime) {
		this.startStreamingDatetime = startStreamingDatetime;
	}
	public String getStartStreamingDatetime() {
		return startStreamingDatetime;
	}
	public void setFinishStreamingDatetime(String finishStreamingDatetime) {
		this.finishStreamingDatetime = finishStreamingDatetime;
	}
	public String getFinishStreamingDatetime() {
		return finishStreamingDatetime;
	}
	
	
	public void setRtsp(String rtsp) {
		this.rtsp = rtsp;
	}
	public String getRtsp() {
		return rtsp;
	}
	
	public void setSnapshots(String snapshots) {
		this.snapshotsmedium = snapshots;
	}
	public String getSnapshots() {
		return snapshotsmedium;
	}
	
	
	
	/**
	 *
	 */

	
	public void setUrls(String urls) {
		this.urls = urls;
	}
	public String getUrls() {
		return urls;
	}
	

	public void setUser(User user) {
		this.user = user;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setCreatedtDates(String date){
//		this.createdDateString = date;
		try{
			setCreatedtDates(sdf.parse(date).getTime());
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "parsed date error", e);
		}
	}
	public void setUpDates(String date){
//		this.createdDateString = date;
		try{
			setUpDates(sdf.parse(date).getTime());
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "parsed date error", e);
		}
	}
	
	
	public void setCreatedtDates(long createdtDate) {
		this.created = createdtDate;
	}
	public long getCreatedtDates() {
		return created;
	}
	
	public void setUpDates(long updated) {
		this.updated = updated;
	}
	public long getUpDates() {
		return updated;
	}
	
	public String getCreatedDateStrings(){
		calendar.setTime(new Date(created));
		if(calendar.get(Calendar.YEAR) < 2500){
			calendar.add(Calendar.YEAR, 543);
		}
		
		return sdfOut.format(calendar.getTime());
	}
	
	public String getUpdatedDateStrings(){
		calendar.setTime(new Date(updated));
		if(calendar.get(Calendar.YEAR) < 2500){
			calendar.add(Calendar.YEAR, 543);
		}
		
		return sdfOut.format(calendar.getTime());
	}

	public String getWeburl() {
		return weburl;
	}

	public void setWeburl(String weburl) {
		this.weburl = weburl;
	}
	
	public void setWatchedCount(String watchedCount) {
		this.watchedCount = watchedCount;
	}
	public String getWatchedCount() {
		return watchedCount;
	}


	public void setLove(int love) {
		this.love = love;
		if(this.love < 0){
			this.love = 0;
		}
	}

	public int getLove() {
		return love;
	}

	public void setIsLove(boolean isLove) {
		this.isLove = isLove;
	}

	public boolean isLove() {
		return isLove;
	}

	public ArrayList<Comment> getComments(){
		return comments;
	}

	public void setComments(ArrayList<Comment> comments){
		this.comments = comments;
	}

	public int getIspublic() {
		return ispublic;
	}

	public void setIspublic(int ispublic) {
		this.ispublic = ispublic;
	}

	public String getHttp() {
		return http;
	}

	public void setHttp(String http) {
		this.http = http;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public long getTime_duration() {
		return time_duration;
	}

	public void setTime_duration(long time_duration) {
		this.time_duration = time_duration;
	}

	public String getIcon_category() {
		return icon_category;
	}

	public void setIcon_category(String icon_category) {
		this.icon_category = icon_category;
	}

	public int getCategory_id() {
		return Category_id;
	}

	public void setCategory_id(int category_id) {
		Category_id = category_id;
	}

	public String getCommentcount() {
		return commentcount;
	}

	public void setCommentcount(String commentcount) {
		this.commentcount = commentcount;
	}
}
