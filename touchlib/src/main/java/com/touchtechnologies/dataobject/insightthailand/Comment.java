package com.touchtechnologies.dataobject.insightthailand;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.dataobject.reporter.LiveStreamConnection;
import com.touchtechnologies.json.JSONUtil;

import android.util.Log;

public class Comment implements Serializable {
	
	private String id;
	private String comment_content;
	private String created_by;
	private String updated_by;
	

	
	private double latitude;
	private double longitude;
	private long CreatedDatetime;
	private long UpdatedDatetime;
	private User user;
	
	public static final String SET_PROTOCOL = "RTSP";	

	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
	static SimpleDateFormat sdfOut = new SimpleDateFormat("d MMM yyyy  H:mm", new Locale("en_EN"));
	static Calendar calendar = Calendar.getInstance(new Locale("th_TH"));
	
	
	public Comment(){
		id = "LOCAL" +  System.currentTimeMillis();
		CreatedDatetime = System.currentTimeMillis();
		UpdatedDatetime = System.currentTimeMillis();
	}
	
	
	public Comment(JSONObject json) throws JSONException{		
		if(!json.isNull("id")){
			setId(json.getString("id"));
		}
		if(!json.isNull("comment_content")){
			setCommentContent(json.getString("comment_content"));
		}
		if(!json.isNull("created_by")){
			setCreatedBy(json.getString("created_by"));
		}
	 		
		if(!json.isNull("updated_by")){
			setUpdatedBy(json.getString("updated_by"));
		}
		
		if(!json.isNull("commentator")){
			try{
				if(json.getJSONObject("commentator").length() > 0){
					setUser(new User(json.getJSONObject("commentator")));
				}
				
			}catch(Exception e){
				Log.e("commentator", e.getMessage(), e);
			}
		}
		
		
	
		
		if(!json.isNull("created_at")){
			try{
				String dateString = JSONUtil.getString(json, "created_at");
				if(dateString.indexOf(":") != -1){
					setCreatedDatetime(sdf.parse(dateString).getTime());
				}else{
					setCreatedDatetime(Long.parseLong(dateString));
				}
			}catch(Exception e){				
			}
			
		}

		if(!json.isNull("updated_at")){
			try{
				String dateString = JSONUtil.getString(json, "updated_at");
				if(dateString.indexOf(":") != -1){
					setUpdatedDatetime(sdf.parse(dateString).getTime());
				}else{
					setUpdatedDatetime(Long.parseLong(dateString));
				}
			}catch(Exception e){				
			}
		}
	
		
		
	}
	
	
	
	public Comment(String _json) throws JSONException{
		this(new JSONObject(_json));
	}	
	
	
	
	//////////// date
	
	
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}
	public void setCommentContent(String commentContent) {
		this.comment_content = commentContent;
	}
	public String getCommentContent() {
		return comment_content;
	}
	
	public void setCreatedBy(String createdBy) {
		this.created_by = createdBy;
	}
	
	public String getCreatedBy() {
		return created_by;
	}


	public void setUpdatedBy(String updatedBy) {
		this.updated_by = updatedBy;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public User getUser() {
		return user;
	}
	
	
	public void setCreatedDatetime(String date){
//		this.createdDateString = date;
		try{
			setCreatedDatetime(sdf.parse(date).getTime());
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "parsed date error", e);
		}
	}

	public void setCreatedDatetime(long connectionDatetime) {
		this.CreatedDatetime = connectionDatetime;
	}
	
	/**
	 * Get post_date
	 * @return
	 */
	public long getCreatedDatetime() {
		return CreatedDatetime;
	}
	
	public String getCreateDateString(){
		calendar.setTime(new Date(CreatedDatetime));
//		if(calendar.get(Calendar.YEAR) < 2500){
//			calendar.add(Calendar.YEAR, 543);
//		}
		
		return sdfOut.format(calendar.getTime());
	}
	
//// date end


	public void setUpdatedDatetime(String date){
//		this.createdDateString = date;
		try{
			setUpdatedDatetime(sdf.parse(date).getTime());
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "parsed date error", e);
		}
	}

	public void setUpdatedDatetime(long connectingTimeoutDatetime) {
		this.UpdatedDatetime = connectingTimeoutDatetime;
	}
	
	/**
	 * Get post_date
	 * @return
	 */
	public long getUpdatedDatetime() {
		return UpdatedDatetime;
	}
	
	public String getUpdateString(){
		calendar.setTime(new Date(UpdatedDatetime));
		if(calendar.get(Calendar.YEAR) < 2500){
			calendar.add(Calendar.YEAR, 543);
		}
		
		return sdfOut.format(calendar.getTime());
	}
	

}
	