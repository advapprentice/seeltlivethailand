package com.touchtechnologies.dataobject.insightthailand;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.json.JSONUtil;


/**
 * Created by TouchICS on 5/31/2016.
 */
public class Report implements Serializable {

    static String KEY_ID = "id";
    static String KEY_EXPLANATION = "reason";


    private int id;
    private String explanation;

    public Report(){

    }

    public Report(JSONObject json) throws JSONException {

        if (!json.isNull(KEY_ID)) {

            setId(json.getInt(KEY_ID));
        }
        if (!json.isNull(KEY_EXPLANATION)) {
            setExplanation(json.getString(KEY_EXPLANATION));
        }

    }


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public String getExplanation() {
        return explanation;
    }

    public JSONObject toJson(){
        JSONObject jUserProperties = new JSONObject();
        try{
            jUserProperties = new JSONObject();

            if(id != 0){
                jUserProperties.put(KEY_ID, getId());
            }
            if(explanation != null){
                jUserProperties.put(KEY_EXPLANATION, getExplanation());
            }

        }catch(Exception e){
            Log.e(LibConfig.LOGTAG, "Report toJson error " + e.getMessage(), e);
        }

        return jUserProperties;
    }

    @Override
    public String toString() {
        return toJson().toString();
    }

}