package com.touchtechnologies.dataobject.insightthailand;

/**
 * Created by TouchICS on 3/4/2016.
 */

import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;


public class CategoryLiveStream implements Serializable {
    static String key_name = "category_name_en";
    static String key_id = "id";
    static String KEY_ICON = "icon_category";
    static String KEY_VIDEO_SIZE = "count_stream";
    private int id;
    private String categoryName;
    private String iconCoverCategory;
    private String countStream;

    public CategoryLiveStream(JSONObject json)  throws JSONException {

        if (!json.isNull("id")) {
            setId(json.getInt("id"));
        }
        if (!json.isNull(key_name)) {
           setCategoryName(json.getString(key_name));
       }
        if (!json.isNull(KEY_ICON)) {
            setIconCoverCategory(json.getString(KEY_ICON));
        }
        if (!json.isNull(KEY_VIDEO_SIZE)) {
            setCountStream(json.getString(KEY_VIDEO_SIZE));
        }


    }

//    public CategoryLiveStream(String _json) throws JSONException{
//        this(new JSONObject(_json));
//    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setIconCoverCategory(String iconCoverCategory) {
        this.iconCoverCategory = iconCoverCategory;
    }

    public String getIconCoverCategory() {
        return iconCoverCategory;
    }

    public void setCountStream(String countStream) {
        this.countStream = countStream;
    }

    public String getCountStream() {
        return countStream;
    }

    public JSONObject toJson() throws JSONException{
        JSONObject jsonObject =   new JSONObject();
        jsonObject.put(key_id,getId());
        jsonObject.put(key_name,getCategoryName());
        jsonObject.put(KEY_ICON,getIconCoverCategory());
        jsonObject.put(KEY_VIDEO_SIZE,getCountStream());
        return jsonObject;
    }

}