package com.touchtechnologies.dataobject.insightthailand;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.json.JSONUtil;

public class User implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3092041761840662828L;
	public static String GROUP_USER = "User";
	public static String GROUP_ADMINISTRATOR = "Administrator";
	public static final String GENDER_MALE = "M";
	public static final String GENDER_FEMALE = "F";
	
	public static final String SIGNUP_SOURCE_SYSTEM = "TIT";
	public static final String SIGNUP_SOURCE_FACEBOOK = "Facebook";
	public static final String SIGNUP_SOURCE_GOOGLEPLUS ="Google";
	public static final String SIGNUP_SOURCE_TWITTER = "twitter";
	
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
	static SimpleDateFormat sdfOut = new SimpleDateFormat("d MMM yyyy  H:mm", new Locale("en_EN"));
	static Calendar calendar = Calendar.getInstance(new Locale("en_EN"));

	private String id;
	
	private String email;
	private String password;
	private String passwordconfrim;
	private String firstName;
	private String lastName;
	private long dateOfBirth;
	private String token;
	private String user_group;
	private String BirthDate;
	private String status;
	private String gender;
	private String avatar;
	private String country;
	private String address;
	private String zipcode;
	private int follower;
	private int following;


	private double latitude = Double.MIN_VALUE;
	private double longitude = Double.MIN_VALUE;	

	private String signupSource;
	/**
	 * Is user logged in. 
	 */

	private boolean isFollowed;
	private boolean loggedIn;
	
	private boolean assigned;
	
	public User(){
		dateOfBirth = System.currentTimeMillis();
	}


	public User(JSONObject json) throws JSONException{		
		if(!json.isNull("id")){
			setId(json.getString("id"));
		}		
		if(!json.isNull("access_token")){
			setToken(json.getString("access_token"));
		}

		if(!json.isNull("email")){
			setEmail(json.getString("email"));
		}
		
		if(!json.isNull("password")){
			setPassword(json.getString("password"));
		}
		
		if(!json.isNull("password_confirmation")){
			setPasswordconfrim(json.getString("password_confirmation"));
		}
		
		if(!json.isNull("loggedIn")){
			setLoggedIn(json.getBoolean("loggedIn"));
		}
	
		if(!json.isNull("first_name")){
			setFirstName(json.getString("first_name"));
		}
		
		
		if(!json.isNull("last_name")){
			setLastName(json.getString("last_name"));
		}
		
		if(!json.isNull("latitude")){
			setLatitude(JSONUtil.getDouble(json, "latitude"));
		}

		if(!json.isNull("longitude")){
			setLongitude(JSONUtil.getDouble(json, "longitude"));
		}
		
		if(!json.isNull("signupSource")){
			setSignupSource(JSONUtil.getString(json, "signupSource"));
		}
		if(!json.isNull("user_group")){
			setGroup(JSONUtil.getString(json, "user_group"));
		}
		if(!json.isNull("status")){
			setStatus(JSONUtil.getString(json, "status"));
		}
		if(!json.isNull("birth_date")){
			setBirthDate(JSONUtil.getString(json, "birth_date"));
		}
		if(!json.isNull("gender")){
			setGender(JSONUtil.getString(json, "gender"));
		}
		if(!json.isNull("profile_picture")){
			setAvatar(JSONUtil.getString(json, "profile_picture"));
		}	
		if(!json.isNull("country")){
			setCountry(JSONUtil.getString(json, "country"));
		}
		if(!json.isNull("address")){
			setAddress(JSONUtil.getString(json, "address"));
		}

		if(!json.isNull("zipcode")){
			setZipcode(JSONUtil.getString(json, "zipcode"));

		}
		
		if(!json.isNull("activated_at")){
			try{
				String dateString = JSONUtil.getString(json, "activated_at");
				if(dateString.indexOf(":") != -1){
					setCreatedtDates(sdf.parse(dateString).getTime());
				}else{
					setCreatedtDates(Long.parseLong(dateString));
				}
			}catch(Exception e){				
			}
		}
		if(!json.isNull("count_follower")){
			setFollower(JSONUtil.getInt(json, "count_follower"));

		}
		if(!json.isNull("count_following")){
			setFollowing(JSONUtil.getInt(json, "count_following"));

		}

		if(!json.isNull("is_followed")){
			setIsFollowed(JSONUtil.getBoolean(json, "is_followed"));
		}

	}
	
	public void setBirthDate(String birth) {
		this.BirthDate = birth;
	}
	public String getBirthDate() {
		return BirthDate;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus() {
		return status;
	}
	public void setGroup(String user_group) {
		this.user_group = user_group;
	}
	public String getGroup() {
		return user_group;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public User(String _json) throws JSONException{
		this(new JSONObject(_json));
	}
	
	public void setSignupSource(String signupSource) {
		this.signupSource = signupSource;
	}
	
	public String getSignupSource() {
		return signupSource;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setPasswordconfrim(String passwordconfrim) {
		this.passwordconfrim = passwordconfrim;
	}
	public String getPasswordconfrim() {
		return passwordconfrim;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void setCreatedtDates(String date){
//		this.createdDateString = date;
		try{
			setCreatedtDates(sdf.parse(date).getTime());
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "parsed date error", e);
		}
	}
	
	public void setCreatedtDates(long createdtDate) {
		this.dateOfBirth = createdtDate;
	}
	public long getCreatedtDates() {
		return dateOfBirth;
	}
	
	
	public String getCreatedDateStrings(){
		calendar.setTime(new Date(dateOfBirth));
		if(calendar.get(Calendar.YEAR) < 2500){
			calendar.add(Calendar.YEAR, 543);
		}
		
		return sdfOut.format(calendar.getTime());
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public String getToken() {
		return token;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getGender() {
		return gender;
	}
	
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getAvatar() {
		return avatar;
	}
	
	public JSONObject toJson(){
		JSONObject jUserProperties = new JSONObject();
		try{
			jUserProperties = new JSONObject();
			
			if(id != null){
				jUserProperties.put("id", id);
			}			
			if(token != null){
				jUserProperties.put("access_token", token);
			}
			
			if(signupSource != null){
				jUserProperties.put("signupSource", signupSource);
			}
			
			if(email != null){
				jUserProperties.put("email", email);		
			}
			
			if(password != null){
				jUserProperties.put("password", password);		
			}
			
			if(passwordconfrim != null){
				jUserProperties.put("passwordconfrim", passwordconfrim);		
			}
			
			jUserProperties.put("loggedIn", loggedIn);
						
			if(firstName != null){
				jUserProperties.put("first_name", firstName);
			}
		
			if(lastName != null){
				jUserProperties.put("last_name", lastName);
			}
				
			if(dateOfBirth != 0){
				jUserProperties.put("activated_at", dateOfBirth);
			}
			if(user_group !=null){
				jUserProperties.put("user_group", user_group);
			}
			
			if(BirthDate !=null){
				jUserProperties.put("bdate", BirthDate);
			}
			if(gender !=null){
				jUserProperties.put("gender", gender);
			}
			
			if(avatar !=null){
				jUserProperties.put("profile_picture", avatar);
			}
			if(Math.abs(latitude) > 0.1){//fix floating conversion error
				jUserProperties.put("latitude", latitude);
			}
			if(Math.abs(longitude) > 0.1){//fix floating conversion error
				jUserProperties.put("longitude", longitude);
			}
			
						
			
//			jUser = new JSONObject();
//			jUser.put("user", jUserProperties);
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "User toJson error " + e.getMessage(), e);
		}
		
		return jUserProperties;
	}
	
	public void setAssigned(boolean assigned) {
		this.assigned = assigned;
	}
	
	public boolean isAssigned() {
		return assigned;
	}
	
	/**
	 * Equal if an user ID is identical to the another
	 */
	@Override	
	public boolean equals(Object o) {
		return o instanceof User 
				&& email != null && email.equals(((User)o).getEmail());
	}
	
	@Override
	public String toString() {
		return toJson().toString();
	}
	
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public int setFollower(int follower) {
		this.follower = follower;
		return follower;
	}

	public int getFollower() {
		return follower;
	}

	public int setFollowing(int following) {
		this.following = following;
		return following;
	}

	public int getFollowing() {
		return following;
	}

	public void setIsFollowed(boolean isFollowed) {
		this.isFollowed = isFollowed;
	}

	public boolean isFollowed() {
		return isFollowed;
	}

}
