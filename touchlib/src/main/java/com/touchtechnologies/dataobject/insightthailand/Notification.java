package com.touchtechnologies.dataobject.insightthailand;

/**
 * Created by TouchICS on 6/9/2016.
 */


import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.json.JSONUtil;


/**
 * Created by TouchICS on 5/31/2016.
 */
public class Notification implements Serializable {

    static String KEY_ID = "id";
    static String KEY_TEXT = "text";
    static String KEY_USERID = "user_id";
    static String KEY_UIQUE = "unique_device_id";
    static String KEY_PLATFORM = "platform";
    static String KEY_OS = "os";
    static String KEY_IS_CONNECTED = "is_connected_socket";
    static String KEY_LANGUAGE = "language";

    private int id;
    private String text;
    private String userId;
    private String uniqueDeviceId;
    private String platform;
    private String os;
    private String ConnectedSocket;
    private String language;

    private NotificationOption notificationOption;

    public Notification(){

    }

    public Notification(JSONObject json) throws JSONException {

        if (!json.isNull(KEY_ID)) {

            setId(json.getInt(KEY_ID));
        }
        if (!json.isNull(KEY_TEXT)) {
            setText(json.getString(KEY_TEXT));
        }
        if (!json.isNull(KEY_USERID)) {
            setUserId(json.getString(KEY_USERID));
        }
        if (!json.isNull(KEY_UIQUE)) {
            setUniqueDeviceId(json.getString(KEY_UIQUE));
        }
        if (!json.isNull(KEY_PLATFORM)) {
            setPlatform(json.getString(KEY_PLATFORM));
        }
        if (!json.isNull(KEY_OS)) {
            setOs(json.getString(KEY_OS));
        }
        if (!json.isNull(KEY_IS_CONNECTED)) {
            setConnectedSocket(json.getString(KEY_IS_CONNECTED));
        }

        if (!json.isNull(KEY_LANGUAGE)) {
            setLanguage(json.getString(KEY_LANGUAGE));
        }

        if (!json.isNull("options")) {
            try{
                if(json.getJSONObject("options").length() > 0){
                    setNotificationOption(new NotificationOption(json.getJSONObject("options")));
                }

            }catch(Exception e){
                Log.e("user", e.getMessage(), e);
            }
        }
    }


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUniqueDeviceId(String uniqueDeviceId) {
        this.uniqueDeviceId = uniqueDeviceId;
    }

    public String getUniqueDeviceId() {
        return uniqueDeviceId;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getPlatform() {
        return platform;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getOs() {
        return os;
    }

    public void setConnectedSocket(String connectedSocket) {
        ConnectedSocket = connectedSocket;
    }

    public String getConnectedSocket() {
        return ConnectedSocket;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public void setNotificationOption(NotificationOption notificationOption) {
        this.notificationOption = notificationOption;
    }

    public NotificationOption getNotificationOption() {
        return notificationOption;
    }

    public JSONObject toJson(){
        JSONObject jUserProperties = new JSONObject();
        try{
            jUserProperties = new JSONObject();

            if(id != 0){
                jUserProperties.put(KEY_ID, getId());
            }
            if(text != null){
                jUserProperties.put(KEY_TEXT, getText());
            }

        }catch(Exception e){
            Log.e(LibConfig.LOGTAG, "Report toJson error " + e.getMessage(), e);
        }

        return jUserProperties;
    }

    @Override
    public String toString() {
        return toJson().toString();
    }

}