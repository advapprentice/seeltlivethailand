package com.touchtechnologies.dataobject.insightthailand;

import com.touchtechnologies.json.JSONUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Touch on 10/6/2016.
 */

public class UserFacebook implements Serializable {
    private String name;
    private String urlPicture;



    public UserFacebook(){

    }
    public UserFacebook(JSONObject json) throws JSONException {
        if(!json.isNull("name")){
            setName(json.getString("name"));
        }
        if(!json.isNull("picture")){
            try{
                JSONObject jsonUrl = json.getJSONObject("picture").getJSONObject("data");
                //"Form"
                setUrlPicture(jsonUrl.getString("url"));

            }catch(Exception e){

            }
        }


    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setUrlPicture(String urlPicture) {
        this.urlPicture = urlPicture;
    }

    public String getUrlPicture() {
        return urlPicture;
    }
}
