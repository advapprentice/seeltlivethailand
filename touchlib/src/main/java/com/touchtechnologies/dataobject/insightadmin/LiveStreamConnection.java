package com.touchtechnologies.dataobject.insightadmin;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.json.JSONUtil;

import android.util.Log;

public class LiveStreamConnection implements Serializable {
	
	private String streamingID;
	private String channelID;
	private String UrlForm;
	private String title;
	private String note;
	private String snapshotMedium;
	private String connectionDatetime;
	private String connectingTimeoutDatetime;
	private String status;
	private String lastName;
	private String email;
	private String mobile;
	private int watchingCount ;
	private double latitude;
	private double longitude;
	private long ConnectionDatetime;
	private long ConnectingTimeoutDatetime;
	
	public static final String SET_PROTOCOL = "RTSP";	

	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
	static SimpleDateFormat sdfOut = new SimpleDateFormat("d MMM yyyy  H:mm", new Locale("th_TH"));
	static Calendar calendar = Calendar.getInstance(new Locale("th_TH"));
	
	
	public LiveStreamConnection(){
		streamingID = "LOCAL" +  System.currentTimeMillis();
		ConnectionDatetime = System.currentTimeMillis();
		ConnectingTimeoutDatetime = System.currentTimeMillis();
	}
	
	
	public LiveStreamConnection(JSONObject json) throws JSONException{		
		if(!json.isNull("streamingID")){
			setStreamId(json.getString("streamingID"));
		}
		if(!json.isNull("channelID")){
			setChannelId(json.getString("channelID"));
		}
		if(!json.isNull("title")){
			setTitle(json.getString("title"));
		}
		
		if(!json.isNull("note")){
			setNote(json.getString("note"));
		}
		if(!json.isNull("watchingCount")){
			setWatchingCount(json.getInt("watchingCount"));
		}
		
		if(!json.isNull("urls")){
				try{
					JSONObject jsonUrl = json.getJSONObject("urls");
					//"Form"
					setUrlForm(jsonUrl.getString("rtsp"));
			
				}catch(Exception e){
					
				}
			}
		if(!json.isNull("snapshots")){
			try{
				JSONObject jsonUrl = json.getJSONObject("snapshots");
				//"Form"
				setSnapshotMedium(jsonUrl.getString("640x480"));
		
			}catch(Exception e){
				
			}
		}	
		
	
		
		if(!json.isNull("getConnectionDatetime")){
			try{
				String dateString = JSONUtil.getString(json, "getConnectionDatetime");
				if(dateString.indexOf(":") != -1){
					setConnectionDatetime(sdf.parse(dateString).getTime());
				}else{
					setConnectionDatetime(Long.parseLong(dateString));
				}
			}catch(Exception e){				
			}
			
		}

		if(!json.isNull("connectingTimeoutDatetime")){
			try{
				String dateString = JSONUtil.getString(json, "connectingTimeoutDatetime");
				if(dateString.indexOf(":") != -1){
					setConnectingTimeoutDatetime(sdf.parse(dateString).getTime());
				}else{
					setConnectingTimeoutDatetime(Long.parseLong(dateString));
				}
			}catch(Exception e){				
			}
		}
		
		
		if(!json.isNull("status")){
			setStatus(json.getString("status"));
		}
		
		if(!json.isNull("geolocation")){
			try{
				JSONObject jsonUrl = json.getJSONObject("geolocation");
				//"Form"
				setLatitude(jsonUrl.getDouble("latitude"));
		        setLongitude(jsonUrl.getDouble("longitude"));
		        
			}catch(Exception e){
				
			}
		}
		
		
		
	}
	
	
	
	public LiveStreamConnection(String _json) throws JSONException{
		this(new JSONObject(_json));
	}	
	
	
	public void setStreamId(String streamingID) {
		this.streamingID = streamingID;
	}
	public String getStreamId() {
		return streamingID;
	}
	public void setChannelId(String channelID) {
		this.channelID = channelID;
	}
	public String getChannelId() {
		return channelID;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle() {
		return title;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getNote() {
		return note;
	}
	public void setUrlForm(String urlForm) {
		UrlForm = urlForm;
	}
	public String getUrlForm() {
		return UrlForm;
	}
	public void setSnapshotMedium(String snapshotmedium) {
		this.snapshotMedium = snapshotmedium;
	}
	public String getSnapshotMedium() {
		return snapshotMedium;
	}
	
	public void setWatchingCount(int watchingCount) {
		this.watchingCount = watchingCount;
	}
	public int getWatchingCount() {
		return watchingCount;
	}
	
	
	
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus() {
		return status;
	}
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLongitude() {
		return longitude;
	}
	
	
	//////////// date
	

	public void setConnectionDatetime(String date){
//		this.createdDateString = date;
		try{
			setConnectionDatetime(sdf.parse(date).getTime());
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "parsed date error", e);
		}
	}

	public void setConnectionDatetime(long connectionDatetime) {
		this.ConnectionDatetime = connectionDatetime;
	}
	
	/**
	 * Get post_date
	 * @return
	 */
	public long getConnectionDatetime() {
		return ConnectionDatetime;
	}
	
	public String getStartDateString(){
		calendar.setTime(new Date(ConnectionDatetime));
		if(calendar.get(Calendar.YEAR) < 2500){
			calendar.add(Calendar.YEAR, 543);
		}
		
		return sdfOut.format(calendar.getTime());
	}
	
//// date end


	public void setConnectingTimeoutDatetime(String date){
//		this.createdDateString = date;
		try{
			setConnectingTimeoutDatetime(sdf.parse(date).getTime());
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "parsed date error", e);
		}
	}

	public void setConnectingTimeoutDatetime(long connectingTimeoutDatetime) {
		this.ConnectingTimeoutDatetime = connectingTimeoutDatetime;
	}
	
	/**
	 * Get post_date
	 * @return
	 */
	public long getConnectingTimeoutDatetime() {
		return ConnectingTimeoutDatetime;
	}
	
	public String getEndDateString(){
		calendar.setTime(new Date(ConnectingTimeoutDatetime));
		if(calendar.get(Calendar.YEAR) < 2500){
			calendar.add(Calendar.YEAR, 543);
		}
		
		return sdfOut.format(calendar.getTime());
	}
	

}
	