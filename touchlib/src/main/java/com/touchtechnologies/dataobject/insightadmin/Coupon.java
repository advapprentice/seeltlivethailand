package com.touchtechnologies.dataobject.insightadmin;

import java.io.Serializable;

/**
 * Created by surachet on 28/7/2559.
 */
public class Coupon implements Serializable{
    /*{
        "id": "2",
            "coupongroupId": "2",
            "couponNo": "THVP-00001",
            "couponCode": "SIT40P",
            "referenceId": "2",
            "gotDate": "2016-07-06 00:00:00",
            "usedTime": "0",
            "status": "Enabled",
            "createdAt": "2016-07-06 18:50:39",
            "createdBy": "0",
            "updatedAt": "2016-07-06 18:50:39",
            "updatedBy": "0",
            "deletedAt": null,
            "applicationKeyname": "silfrontend"
    }*/
    String id;
    String coupongroupId;
    String cuponNo;
    String couponCode;
    String referenceId;
    String gotDate;
    String usedTime;
    String status;
    String createdAt;
    String createdBy;
    String updatedBy;
    String updatedAt;
    String deletedAt;
    String applicationKeyname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCoupongroupId() {
        return coupongroupId;
    }

    public void setCoupongroupId(String coupongroupId) {
        this.coupongroupId = coupongroupId;
    }

    public String getCuponNo() {
        return cuponNo;
    }

    public void setCuponNo(String cuponNo) {
        this.cuponNo = cuponNo;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getGotDate() {
        return gotDate;
    }

    public void setGotDate(String gotDate) {
        this.gotDate = gotDate;
    }

    public String getUsedTime() {
        return usedTime;
    }

    public void setUsedTime(String usedTime) {
        this.usedTime = usedTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getApplicationKeyname() {
        return applicationKeyname;
    }

    public void setApplicationKeyname(String applicationKeyname) {
        this.applicationKeyname = applicationKeyname;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }
}
