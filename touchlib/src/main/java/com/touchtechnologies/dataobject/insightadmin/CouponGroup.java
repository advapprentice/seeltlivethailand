package com.touchtechnologies.dataobject.insightadmin;

/**
 * Created by TouchICS on 7/22/2016.
 */

import java.io.Serializable;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.provider.ContactsContract.CommonDataKinds.Phone;

import com.touchtechnologies.dataobject.DataItem;
import com.touchtechnologies.dataobject.Image;
import com.touchtechnologies.dataobject.ImageGallery;
import com.touchtechnologies.dataobject.PhoneNumber;
import com.touchtechnologies.json.JSONUtil;
import com.touchtechnologies.log.Log;

/**
 * Base object of business type in hospitality industry.
 * @author MEe
 *
 */
public class CouponGroup implements Serializable {

    private static final long serialVersionUID = 0xc6629f9800003748L;
    private String id;
    private String providerId;
    private String coupongroupNameEn;
    private String descriptionEn ;
    private String shortDescriptionEn;
    private String conditionEn;
    private int coupongroupValue;
    private String coupongroupType;
    private int limitUseTime;
    private String coupongroupLimit;
    private String coupongroupPrefix;
    private String contactemail;
    private String contactPhone;
    private String status;
    private String price;
    private String BannerUrl;
    private String userId;
    private String startdate;
    private String enddate;
    private String startpromotiondate;
    private String endpromotiondate;
    private String Businessname;
    public enum Businesstype{
        hotel,restaurant,attraction
    }
    private Businesstype businesstype;

    public CouponGroup(){
    }

    public CouponGroup(JSONObject json) {

        if(!json.isNull("id")){
            setId(JSONUtil.getString(json, "id"));
        }

        if(!json.isNull("providerId")){
            setProviderId(JSONUtil.getString(json, "providerId"));
        }
        if(!json.isNull("coupongroupNameEn")){
            setCoupongroupNameEn(JSONUtil.getString(json, "coupongroupNameEn"));
        }
        if(!json.isNull("descriptionEn")){
            setDescriptionEn(JSONUtil.getString(json, "descriptionEn"));
        }
        if(!json.isNull("shortDescriptionEn")){
            setShortDescriptionEn(JSONUtil.getString(json, "shortDescriptionEn"));
        }
        if(!json.isNull("conditionEn")){
            setConditionEn(JSONUtil.getString(json, "conditionEn"));
        }
        if(!json.isNull("price")){
            setPrice(JSONUtil.getString(json, "price"));
        }
        if(!json.isNull("coupongroupType")){
            setCoupongroupType(JSONUtil.getString(json, "coupongroupType"));
        }

        if(!json.isNull("coupongroupValue")){
            setCoupongroupValue(JSONUtil.getInt(json, "coupongroupValue"));
        }
        if(!json.isNull("coupongroupLimit")){
            setCoupongroupLimit(JSONUtil.getString(json, "coupongroupLimit"));
        }
        if(!json.isNull("limitUseTime")){
            setLimitUseTime(JSONUtil.getInt(json, "limitUseTime"));
        }
        if(!json.isNull("coupongroupPrefix")){
            setCoupongroupPrefix(JSONUtil.getString(json, "coupongroupPrefix"));
        }
        if(!json.isNull("contactPhone")){
            setContactPhone(JSONUtil.getString(json, "contactPhone"));
        }

        if(!json.isNull("contactEmail")){
            setContactEmail(JSONUtil.getString(json, "contactEmail"));
        }
        if(!json.isNull("status")){
            setStatus(JSONUtil.getString(json, "status"));
        }
        if(!json.isNull("bannerUrl")){

            setBannerUrl(JSONUtil.getString(json,"bannerUrl"));
        }
        if(!json.isNull("endDate")){
            String[] part = JSONUtil.getString(json,"endDate").split(" ");
            setEnddate(part[0]);
        }
        if(!json.isNull("startDate")){
            String[] part = JSONUtil.getString(json,"startDate").split(" ");
            setStartdate(part[0]);
        }
        if(!json.isNull("startPromotionDate")){
            String[] part = JSONUtil.getString(json,"startPromotionDate").split(" ");
            setStartpromotiondate(part[0]);
        }
        if(!json.isNull("endPromotionDate")){
            String[] part = JSONUtil.getString(json,"endPromotionDate").split(" ");
            setEndpromotiondate(part[0]);

        }
        if(!json.isNull("providerTypeId")){
            if(JSONUtil.getInt(json,"providerTypeId") == 3){
                setBusinesstype(Businesstype.hotel);
            }else if(JSONUtil.getInt(json,"providerTypeId") == 2){
                setBusinesstype(Businesstype.restaurant);
            }else {
                setBusinesstype(Businesstype.attraction);
            }
        }


    }



    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public String getBusinessname() {
        return Businessname;
    }

    public void setBusinessname(String businessname) {
        Businessname = businessname;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setCoupongroupNameEn(String coupongroupNameEn) {
        this.coupongroupNameEn = coupongroupNameEn;
    }

    public String getCoupongroupNameEn() {
        return coupongroupNameEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setShortDescriptionEn(String shortDescriptionEn) {
        this.shortDescriptionEn = shortDescriptionEn;
    }

    public String getShortDescriptionEn() {
        return shortDescriptionEn;
    }

    public void setConditionEn(String conditionEn) {
        this.conditionEn = conditionEn;
    }

    public String getConditionEn() {
        return conditionEn;
    }

    public void setCoupongroupValue(int coupongroupValue) {
        this.coupongroupValue = coupongroupValue;
    }

    public int getCoupongroupValue() {
        return coupongroupValue;
    }

    public void setCoupongroupType(String coupongroupType) {
        this.coupongroupType = coupongroupType;
    }

    public String getCoupongroupType() {
        return coupongroupType;
    }

    public void setLimitUseTime(int limitUseTime) {
        this.limitUseTime = limitUseTime;
    }

    public int getLimitUseTime() {
        return limitUseTime;
    }

    public void setCoupongroupLimit(String coupongroupLimit) {
        this.coupongroupLimit = coupongroupLimit;
    }

    public String getCoupongroupLimit() {
        return coupongroupLimit;
    }

    public void setCoupongroupPrefix(String coupongroupPrefix) {
        this.coupongroupPrefix = coupongroupPrefix;
    }

    public String getCoupongroupPrefix() {
        return coupongroupPrefix;
    }

    public void setContactEmail(String email) {
        this.contactemail = email;
    }

    public String getContactEmail() {
        return contactemail;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public String getBannerUrl() {
        return BannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        BannerUrl = bannerUrl;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getStartpromotiondate() {
        return startpromotiondate;
    }

    public void setStartpromotiondate(String startpromotiondate) {
        this.startpromotiondate = startpromotiondate;
    }

    public String getEndpromotiondate() {
        return endpromotiondate;
    }

    public void setEndpromotiondate(String endpromotiondate) {
        this.endpromotiondate = endpromotiondate;
    }

    public Businesstype getBusinesstype() {
        return businesstype;
    }

    public void setBusinesstype(Businesstype businesstype) {
        this.businesstype = businesstype;
    }

    public JSONObject toJson(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("providerId", getProviderId());
            jsonObject.put("coupongroupNameEn", getCoupongroupNameEn());
            jsonObject.put("coupongroupNameTh", getCoupongroupNameEn());
            jsonObject.put("descriptionEn",getDescriptionEn());
            jsonObject.put("descriptionTh",getDescriptionEn());
            jsonObject.put("shortDescriptionEn",getShortDescriptionEn());
            jsonObject.put("shortDescriptionTh",getShortDescriptionEn());
            jsonObject.put("conditionEn",getConditionEn());
            jsonObject.put("conditionTh",getConditionEn());
            jsonObject.put("price",getPrice());
            jsonObject.put("coupongroupType",getCoupongroupType());
            jsonObject.put("coupongroupValue",getCoupongroupValue());
            jsonObject.put("startDate",getStartdate());
            jsonObject.put("endDate",getEnddate());
            jsonObject.put("startPromotionDate",getStartpromotiondate());
            jsonObject.put("endPromotionDate", getEndpromotiondate());
            jsonObject.put("coupongroupLimit",getCoupongroupLimit());
            jsonObject.put("limitUseTime",getLimitUseTime());
            jsonObject.put("coupongroupPrefix",getCoupongroupPrefix());
            jsonObject.put("email",getContactEmail());
            jsonObject.put("contactPhone","0"+getContactPhone());
            jsonObject.put("status",getStatus());
            jsonObject.put("userId",getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObject;
    }

}
