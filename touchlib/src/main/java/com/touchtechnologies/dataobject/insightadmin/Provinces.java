package com.touchtechnologies.dataobject.insightadmin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.json.JSONUtil;

public class Provinces implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 749548868907640918L;

	private String id;
	private String nameEn;
	private String nameTh;

	
	public Provinces(JSONObject json) throws JSONException{
		
		if(!json.isNull("id_province")){
			setId(JSONUtil.getString(json,"id_province"));
		}
		if(!json.isNull("province_name_en")){
			setNameEn(JSONUtil.getString(json,"province_name_en"));
		}
		if(!json.isNull("province_name_th")){
			setNameTh(JSONUtil.getString(json,"province_name_th"));
		}
	
	
	
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}
	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}
	public String getNameEn() {
		return nameEn;
	}
	public void setNameTh(String nameTh) {
		this.nameTh = nameTh;
	}
	public String getNameTh() {
		return nameTh;
	}
	
	
	public JSONObject toJson() throws JSONException{
		JSONObject json = new JSONObject();
		
		if(getId() != null){
			json.put("id_province", getId());
		}
		
		if(getNameEn() != null){
			json.put("province_name_en", getNameEn());
		}
		
		if(getNameTh() != null){
			json.put("province_name_th", getNameTh());
		}


		return json;
	}
	
}