package com.touchtechnologies.dataobject.insightadmin;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.json.JSONUtil;

import android.util.Log;


public class User extends JSONObject implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3092041761840662828L;
	public static String GROUP_USER = "User";
	public static String GROUP_ADMINISTRATOR = "Administrator";
	public static final String GENDER_MALE = "M";
	public static final String GENDER_FEMALE = "F";
	
	public static final String SIGNUP_SOURCE_SYSTEM = "TIT";
	public static final String SIGNUP_SOURCE_FACEBOOK = "facebook";
	public static final String SIGNUP_SOURCE_GOOGLEPLUS ="Google";
	public static final String SIGNUP_SOURCE_TWITTER = "twitter";
	
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
	static SimpleDateFormat sdfOut = new SimpleDateFormat("d MMM yyyy  H:mm", new Locale("en_EN"));
	static Calendar calendar = Calendar.getInstance(new Locale("en_EN"));

	private String userId;
	private String userName;
	private String email;
	private String password;
	private String passwordConfrim;
	private String displayName;
	private String permissions;
	private String accessToken;
	private long activatedAt;
	private String activated;
	private String lastLogin;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private String avatar;
	private String activeStatus;
	private String createdBy;
	private String tokenIdentifier;
	private String social;
	private String identifier;
	private String accesstokenid;
	
	
	private double latitude = Double.MIN_VALUE;
	private double longitude = Double.MIN_VALUE;	
	private String signupSource;
	/**
	 * Is user logged in. 
	 */
	private boolean loggedIn;
	
	private boolean assigned;
	
	public User(){
		activatedAt = System.currentTimeMillis();
	}
	
	public User(JSONObject json) throws JSONException{		
		

		if(!json.isNull("accessToken")){
			setAccessToken(JSONUtil.getString(json, "accessToken"));
		}
		if(!json.isNull("userId")){
			setUserId(JSONUtil.getString(json, "userId"));
		}else if(!json.isNull("id")){
			setUserId(JSONUtil.getString(json, "id"));
		}
		if(!json.isNull("username")){
			setUserName(JSONUtil.getString(json, "username"));
		}
		if(!json.isNull("email")){
			setEmail(JSONUtil.getString(json, "email"));
		}
		
		if(!json.isNull("displayName")){
			setDisplayName(JSONUtil.getString(json, "displayName"));
		}
		
		if(!json.isNull("permissions")){
			setPermissions(JSONUtil.getString(json, "permissions"));
		}
		
		if(!json.isNull("activated")){
			setActivated(JSONUtil.getString(json, "activated"));
		}
		
		if(!json.isNull("lastLogin")){
			setLastLogin(JSONUtil.getString(json, "lastLogin"));
		}
		
		if(!json.isNull("firstName")){
			setFirstName(JSONUtil.getString(json, "firstName"));
		}
		
		if(!json.isNull("lastName")){
			setLastName(JSONUtil.getString(json, "lastName"));
		}
		
		if(!json.isNull("phoneNumber")){
			setPhoneNumber(JSONUtil.getString(json, "phoneNumber"));
		}
		
		if(!json.isNull("avatar")){
			setAvatar(JSONUtil.getString(json, "avatar"));
		}
		
		if(!json.isNull("activeStatus")){
			setActiveStatus(JSONUtil.getString(json, "activeStatus"));
		}
		
		if(!json.isNull("createdBy")){
			setCreatedBy(JSONUtil.getString(json, "createdBy"));
		}
		if(!json.isNull("social")){
			setSocial(JSONUtil.getString(json, "social"));
		}
		if(!json.isNull("identifier")){
			setIdentifier(JSONUtil.getString(json, "identifier"));
		}
	
		if(!json.isNull("tokenIdentifier")){
			setTokenIdentifier(JSONUtil.getString(json, "tokenIdentifier"));
		}
		

		if(!json.isNull("activatedAt")){
			try{
				String dateString = JSONUtil.getString(json, "activatedAt");
				if(dateString.indexOf(":") != -1){
					setCreatedtDates(sdf.parse(dateString).getTime());
				}else{
					setCreatedtDates(Long.parseLong(dateString));
				}
			}catch(Exception e){				
			}
		}		
		
		
	}
	
	
	public User(String _json) throws JSONException{
		this(new JSONObject(_json));
		
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setTokenIdentifier(String tokenIdentifier) {
		this.tokenIdentifier = tokenIdentifier;
	}
	
	public String getTokenIdentifier() {
		return tokenIdentifier;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserName() {
		return userName;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail() {
		return email;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPassword() {
		return password;
	}
	
	public void setPasswordConfrim(String passwordConfrim) {
		this.passwordConfrim = passwordConfrim;
	}
	public String getPasswordConfrim() {
		return passwordConfrim;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getDisplayName() {
        if(getLastName() != null && getFirstName() != null)
            setDisplayName(getFirstName() +"  "+getLastName());
		return displayName;
	}
	public void setPermissions(String permissions) {
		this.permissions = permissions;
	}
	public String getPermissions() {
		return permissions;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setActivated(String activated) {
		this.activated = activated;
	}
	public String getActivated() {
		return activated;
	}
	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}
	public String getLastLogin() {
		return lastLogin;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;

	}
	public String getFirstName() {
		return firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}
	public String getActiveStatus() {
		return activeStatus;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getAvatar() {
		return avatar;
	}
	
	public void setAssigned(boolean assigned) {
		this.assigned = assigned;
	}
	
	public boolean isAssigned() {
		return assigned;
	}

	
	/**
	 * Equal if an user ID is identical to the another
	 */
	@Override	
	public boolean equals(Object o) {
		return o instanceof User 
				&& email != null && email.equals(((User)o).getEmail());
	}
	
	
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	public boolean isLoggedIn() {
		return loggedIn;
	}
	
	public void setSignupSource(String signupSource) {
		this.signupSource = signupSource;
	}
	
	public String getSignupSource() {
		return signupSource;
	}
	
	public void setSocial(String social) {
		this.social = social;
	}
	public String getSocial() {
		return social;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getIdentifier() {
		return identifier;
	}

	
	public void setCreatedtDates(String date){
//		this.createdDateString = date;
		try{
			setCreatedtDates(sdf.parse(date).getTime());
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "parsed date error", e);
		}
	}
	
	public void setCreatedtDates(long createdtDate) {
		this.activatedAt = createdtDate;
	}
	public long getCreatedtDates() {
		return activatedAt;
	}
	
	
	public String getCreatedDateStrings(){
		calendar.setTime(new Date(activatedAt));
		if(calendar.get(Calendar.YEAR) < 2500){
			calendar.add(Calendar.YEAR, 543);
		}
		
		return sdfOut.format(calendar.getTime());
	}
	
	public JSONObject toJson(){
		JSONObject jUserProperties = new JSONObject();
		try{
			jUserProperties = new JSONObject();
			
			if(userId!= null){
				jUserProperties.put("userId", userId);
			}
			if(accessToken != null){
				jUserProperties.put("accessToken", accessToken);
			}
			
			if(userName != null){
				jUserProperties.put("username", userName);
			}
			
			if(email != null){
				jUserProperties.put("email", email);		
			}
			
			if(password != null){
				jUserProperties.put("password", password);		
			}
			
			if(passwordConfrim != null){
				jUserProperties.put("passwordconfrim", passwordConfrim);		
			}
			jUserProperties.put("loggedIn", loggedIn);			
			if(displayName != null){
				jUserProperties.put("displayName", displayName);
			}
			
			if(permissions != null){
				jUserProperties.put("permissions", permissions);
			}
			if(activated != null){
				jUserProperties.put("activated", activated);
			}
			if(lastLogin != null){
				jUserProperties.put("lastLogin", lastLogin);
			}
			if(firstName != null){
				jUserProperties.put("firstName", firstName);
			}
			if(lastName != null){
				jUserProperties.put("lastName", lastName);
			}
			if(lastName != null){
				jUserProperties.put("lastName", lastName);
			}
			if(phoneNumber!= null){
				jUserProperties.put("phoneNumber", phoneNumber);
			}
			if(avatar != null){
				jUserProperties.put("avatar", avatar);
			}
			if(activeStatus !=null){
				jUserProperties.put("activeStatus", activeStatus);
			}
			if(createdBy !=null){
				jUserProperties.put("createdBy", createdBy);
			}
			if(social !=null){
				jUserProperties.put("social", social);
			}
			if(identifier !=null){
				jUserProperties.put("identifier", identifier);
			}
			if(tokenIdentifier !=null){
				jUserProperties.put("tokenIdentifier", tokenIdentifier);
			}
			
			
			if(Math.abs(latitude) > 0.1){//fix floating conversion error
				jUserProperties.put("latitude", latitude);
			}
			if(Math.abs(longitude) > 0.1){//fix floating conversion error
				jUserProperties.put("longitude", longitude);
			}
			

		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "User toJson error " + e.getMessage(), e);
		}
		
		return jUserProperties;
	}
	
	@Override
	public String toString() {
		return toJson().toString();
	}

	public String getAccesstokenid() {
		return accesstokenid;
	}

	public void setAccesstokenid(String accesstokenid) {
		this.accesstokenid = accesstokenid;
	}


}
