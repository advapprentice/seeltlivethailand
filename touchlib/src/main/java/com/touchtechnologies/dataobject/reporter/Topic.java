package com.touchtechnologies.dataobject.reporter;

import java.io.Serializable;

import org.json.JSONObject;

import android.util.Log;

import com.touchtechnologies.json.JSONUtil;

public class Topic implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5162972438933895008L;

	private String id;
	private String title;
	private String note;
	private User guru;
	
	public Topic() {
	}
	
	public Topic(JSONObject json){
		if(!json.isNull("topicID")){
			setId(JSONUtil.getString(json, "topicID"));
		}
		
		
		if(!json.isNull("topicTitle")){
			setTitle(JSONUtil.getString(json, "topicTitle"));
		}
		
		if(!json.isNull("note")){
			setNote(JSONUtil.getString(json, "note"));
		}
		
		if(!json.isNull("gurus")){
			try{
				setGuru(new Guru(json.getJSONArray("gurus").getJSONObject(0)));
				
			}catch(Exception e){
				Log.e("Topic", e.getMessage(), e);
			}
		}
		
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	
	public User getGuru() {
		return guru;
	}
	
	public void setGuru(User guru) {
		this.guru = guru;
	}
	
	public void setNote(String note) {
		this.note = note;
	}
	
	public String getNote() {
		return note;
	}
}
