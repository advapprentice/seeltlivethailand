package com.touchtechnologies.dataobject.reporter;

import java.io.Serializable;
import java.text.SimpleDateFormat;

import org.json.JSONObject;

import com.touchtechnologies.json.JSONUtil;

public class Questionnaire implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3185016797727006289L;
	private String id;
	private String title;
	private String description;
	private long createdDate;
	private long startDate;
	private long endDate;
	private int numAnswered;
	private int numViews;
	private boolean answered;
	private boolean available;
	private boolean allowAnswer;
	private String checkAnswer;
	private String urlForm;
	private String urlReport;
	
	private static SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public Questionnaire() {
	}
	
	public Questionnaire(JSONObject json){
		if(!json.isNull("questionnaireID")){
			setId(JSONUtil.getString(json, "questionnaireID"));
		}
		
		if(!json.isNull("questionnaireName")){
			setTitle(JSONUtil.getString(json, "questionnaireName"));
		}
	    
		if(!json.isNull("questionnaireDescription")){
			setDescription(JSONUtil.getString(json, "questionnaireDescription"));
		}
		
		if(!json.isNull("isAnswered")){
			setCheckAnswer(JSONUtil.getString(json, "isAnswered"));
		}
		
		if(!json.isNull("createdDatetime")){
			try{
				long dateTime = sdfDateTime.parse(JSONUtil.getString(json, "createdDatetime")).getTime(); 
				setCreatedDate(dateTime);
			}catch(Exception e){
				
			}
			
		}

		if(!json.isNull("startDatetime")){
			try{
				long dateTime = sdfDate.parse(JSONUtil.getString(json, "startDatetime")).getTime(); 
				setStartDate(dateTime);
			}catch(Exception e){
				
			}
		}
		
		if(!json.isNull("endDatetime")){
			try{
				long dateTime = sdfDate.parse(JSONUtil.getString(json, "endDatetime")).getTime(); 
				setEndDate(dateTime);
			}catch(Exception e){
				
			}
		}

		if(!json.isNull("answered")){
			setAnswered("Y".equalsIgnoreCase(JSONUtil.getString(json, "answered")));
		}

		if(!json.isNull("views")){
			setNumViews(JSONUtil.getInt(json, "views"));
		}

		if(!json.isNull("isAvailable")){
			setAvailable("Y".equalsIgnoreCase(JSONUtil.getString(json, "isAvailable")));
		}

		if(!json.isNull("isAllowAnswer")){
			setAllowAnswer("Y".equalsIgnoreCase(JSONUtil.getString(json, "isAllowAnswer")));
		}

		if(!json.isNull("isAllowViewReport")){
			setAllowAnswer("Y".equalsIgnoreCase(JSONUtil.getString(json, "isAllowViewReport")));
		}

		if(!json.isNull("URLs")){
			try{
				JSONObject jsonUrl = json.getJSONObject("URLs");
				//"Form"
				setUrlForm(jsonUrl.getString("Form"));
			
				//"Report"
				setUrlReport(jsonUrl.getString("Report"));
			}catch(Exception e){
				
			}
		}	    
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}
	
	public long getCreatedDate() {
		return createdDate;
	}
	
	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}
	
	public long getStartDate() {
		return startDate;
	}
	
	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}
	
	public long getEndDate() {
		return endDate;
	}
	
	public void setNumAnswered(int numAnswered) {
		this.numAnswered = numAnswered;
	}
	
	public int getNumAnswered() {
		return numAnswered;
	}
	
	public void setNumViews(int numViews) {
		this.numViews = numViews;
	}
	
	public int getNumViews() {
		return numViews;
	}
	
	public void setAnswered(boolean answered) {
		this.answered = answered;
	}
	
	public boolean isAnswered() {
		return answered;
	}
	
	public void setAllowAnswer(boolean allowAnswer) {
		this.allowAnswer = allowAnswer;
	}

	
	public boolean isAllowAnswer() {
		return allowAnswer;
	}
	
	
	public void setCheckAnswer(String checkAnswer) {
		this.checkAnswer = checkAnswer;
	}
	public String getCheckAnswer() {
		return checkAnswer;
	}
	
	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	public boolean isAvailable() {
		return available;
	}
	
	public void setUrlForm(String urlForm) {
		this.urlForm = urlForm;
	}
	
	public String getUrlForm() {
		return urlForm;
	}
	
	public void setUrlReport(String urlReport) {
		this.urlReport = urlReport;
	}
	
	public String getUrlReport() {
		return urlReport;
	}	
}
