package com.touchtechnologies.dataobject.reporter;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.json.JSONUtil;

import android.util.Log;

public class LiveStreamConnection implements Serializable {
	
	private String streamingID;
	private String channelID;
	private String url;
	private String title;
	private String note;
	private String startDate;
	private String connectionDatetime;
	private String status;
	private String lastName;
	private String email;
	private String mobile;
	private User user;
	public static final String SET_PROTOCOL = "RTSP";	
	public LiveStreamConnection(){
	}
	
	public LiveStreamConnection(JSONObject json) throws JSONException{		
		if(!json.isNull("streamingID")){
			setStreamId(json.getString("streamingID"));
		}
		
		if(!json.isNull("channelID")){
			setChannelId(json.getString("channelID"));
		}
		
		if(!json.isNull("url")){
			setUrl(json.getString("url"));
		}
		
		if(!json.isNull("title")){
			setTitle(json.getString("title"));
		}
		
		if(!json.isNull("note")){
			setNote(json.getString("note"));
		}
		
		if(!json.isNull("startDate")){
			setStartDate(json.getString("startDate"));
		}
		
		if(!json.isNull("getConnectionDatetime")){
			setConnectionDatetime(json.getString("getConnectionDatetime"));
		}
		if(!json.isNull("status")){
			setStatus(json.getString("status"));
		}
		
		if(!json.isNull("reporter")){
			try{
				setReporter(new User(json.getJSONObject("reporter").getJSONObject("user")));
				
			}catch(Exception e){
				Log.e("reporter", e.getMessage(), e);
			}
		}
		
		
	}
	
	
	
	public LiveStreamConnection(String _json) throws JSONException{
		this(new JSONObject(_json));
	}	
	
	
	public void setStreamId(String streamingID) {
		this.streamingID = streamingID;
	}
	public String getStreamId() {
		return streamingID;
	}
	public void setChannelId(String channelID) {
		this.channelID = channelID;
	}
	public String getChannelId() {
		return channelID;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUrl() {
		return url;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle() {
		return title;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getNote() {
		return note;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setConnectionDatetime(String connectionDatetime) {
		this.connectionDatetime = connectionDatetime;
	}
	public String getConnectionDatetime() {
		return connectionDatetime;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus() {
		return status;
	}
	public void setReporter(User reporter) {
		this.user = reporter;
	}
	public User getReporter() {
		return user;
	}
	
}
	