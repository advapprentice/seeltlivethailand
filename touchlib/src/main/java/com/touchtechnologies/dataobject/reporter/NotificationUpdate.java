package com.touchtechnologies.dataobject.reporter;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.json.JSONUtil;

public class NotificationUpdate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5275312054061973264L;
	
	
	private String id;
	private String command;
	private String message;
	
	private String notificationID;
	private String notifyStatus;
	private Notification notification;

	public NotificationUpdate(){
	}

	public NotificationUpdate(JSONObject json) throws JSONException{		
		if(!json.isNull("notificationID")){
			setNotificationID(json.getString("notificationID"));
		}
		
		if(!json.isNull("notifyStatus")){
			setNotifyStatus(json.getString("notifyStatus"));
		}
		
		

		JSONObject jsonNotification = null;
		if(!json.isNull("notification")){
			jsonNotification = json.getJSONObject("notification");
		}
	
		
	}

	public NotificationUpdate(String _json) throws JSONException{
		this(new JSONObject(_json));
	}
	
	public void setNotificationID(String notificationID) {
		this.notificationID = notificationID;
	}
	
	public String getNotificationID() {
		return notificationID;
	}
	
	
	
	public void setNotifyStatus(String notifyStatus) {
		this.notifyStatus = notifyStatus;
	}

	public String getNotifyStatus() {
		return notifyStatus;
	}


	
	public void setNotification(Notification notification) {
		this.notification = notification;
	}
	
	public Notification getNotification() {
		return notification;
	}
	

	public JSONObject toJson(){
		JSONObject jUserProperties = new JSONObject();
		try{
			jUserProperties = new JSONObject();
			if(notificationID != null){
				jUserProperties.put("notificationID", notificationID);
			}			
		
			if(notifyStatus != null){
				jUserProperties.put("notifyStatus", notifyStatus);
			}
	
			if(notification != null){
				jUserProperties.put("notification", notification);

			}
		
//			jUser = new JSONObject();
//			jUser.put("user", jUserProperties);
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "User toJson error " + e.getMessage(), e);
		}
		
		return jUserProperties;
	}
	
	@Override
	public String toString() {
		return toJson().toString();
	}
	
	
}
