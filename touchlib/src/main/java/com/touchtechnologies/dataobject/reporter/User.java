package com.touchtechnologies.dataobject.reporter;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.json.JSONUtil;

import android.util.Log;

public class User implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3092041761840662828L;
	public static String GROUP_USER = "User";
	public static String GROUP_ADMINISTRATOR = "Administrator";
	public static final String GENDER_MALE = "M";
	public static final String GENDER_FEMALE = "F";
	
	private String session;
	private String userID;
	private String username;
	private String fullName;
	private String password;
	private String gender;
	private String citizenID;
	private String firstName;
	private String lastName;
	private String email;
	private String mobile;
	private String userGroup;
	private String avatar;
	private String dateOfBirth;
	private Address address;
	private double latitude = Double.MIN_VALUE;
	private double longitude = Double.MIN_VALUE;	
	
	private boolean assigned;
	
	public User(){
	}
	
	public User(JSONObject json) throws JSONException{		
		if(!json.isNull("session")){
			setSession(json.getString("session"));
		}
		
		if(!json.isNull("userID")){
			setUserID(json.getString("userID"));
		}
		
		if(!json.isNull("username")){
			setUsername(json.getString("username"));
		}
		
		if(!json.isNull("password")){
			setPassword(json.getString("password"));
		}
		
		if(!json.isNull("gender")){
			setGender(json.getString("gender"));
		}
		
		if(!json.isNull("citizenID")){
			setCitizenID(json.getString("citizenID"));
		}
				
		if(!json.isNull("fullName")){
			setFullName(json.getString("fullName"));
		}
		
		
		if(!json.isNull("firstName")){
			setFirstName(json.getString("firstName"));
		}
		
		
		if(!json.isNull("lastName")){
			setLastName(json.getString("lastName"));
		}
		
		if(!json.isNull("email")){
			setEmail(json.getString("email"));
		}
		
		if(!json.isNull("mobile")){
			setMobile(json.getString("mobile"));
		}
		
		if(!json.isNull("userType")){
			setUserGroup(json.getString("userType"));
		}
		
		if(!json.isNull("avatar")){
			setAvatar(json.getString("avatar"));
		}
		
		if(!json.isNull("dateOfBirth")){
			setDateOfBirth(json.getString("dateOfBirth"));
		}
		
		if(!json.isNull("address")){
			setAddress(new Address(json.getString("address")));
		}
		
		if(!json.isNull("latitude")){
			setLatitude(JSONUtil.getDouble(json, "latitude"));
		}

		if(!json.isNull("longitude")){
			setLongitude(JSONUtil.getDouble(json, "longitude"));
		}
		
		
		
	}
	
	public User(String _json) throws JSONException{
		this(new JSONObject(_json));
	}
	
	public void setSession(String session) {
		this.session = session;
	}
	
	public String getSession() {
		return session;
	}
	
	public void setUserID(String userID) {
		this.userID = userID;
	}
	
	public String getUserID() {
		return userID;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public String getFullName() {
		
		if(firstName!=null&&lastName!=null){
			
			
		}
		
		return firstName+"  "+lastName;
		
	}
	
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setCitizenID(String citizenID) {
		this.citizenID = citizenID;
	}
	
	public String getCitizenID() {
		return citizenID;
	}
	
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	public String getMobile() {
		return mobile;
	}
	
	public void setUserGroup(String userGroup) {
		this.userGroup = userGroup;
	}
	
	public String getUserGroup() {
		return userGroup;
	}
	
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	public String getAvatar() {
		return avatar;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}
	
	public Address getAddress() {
		return address;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	public JSONObject toJson(){
		JSONObject jUserProperties = new JSONObject();
		try{
			jUserProperties = new JSONObject();
			if(session != null){
				jUserProperties.put("session", session);
			}			
			
			if(userID != null){
				jUserProperties.put("userID", userID);
			}
			
			if(username != null){
				jUserProperties.put("username", username);
			}
			
			if(fullName != null){
				jUserProperties.put("fullName", fullName);
			}
			
			if(firstName != null){
				jUserProperties.put("firstName", firstName);
			}
		
			if(lastName != null){
				jUserProperties.put("lastName", lastName);
			}
			
			if(gender != null){
				jUserProperties.put("gender", gender);
			}
			
			if(citizenID != null){
				jUserProperties.put("citizenID", citizenID);
			}
			
			if(password != null){
				jUserProperties.put("password", password);
			}
			
			if(userGroup != null){
				jUserProperties.put("userType", userGroup);
			}
			
			if(email != null){
				jUserProperties.put("email", email);		
			}
			
			if(avatar != null){
				jUserProperties.put("avatar", avatar);				
			}
			
			if(dateOfBirth != null){
				jUserProperties.put("dateOfBirth", dateOfBirth);
			}
			
			if(mobile != null){
				jUserProperties.put("mobile", mobile);
			}

			
			if(address != null){
				jUserProperties.put("address", address.toJson());
			}
			if(Math.abs(latitude) > 0.1){//fix floating conversion error
				jUserProperties.put("latitude", latitude);
			}
			if(Math.abs(longitude) > 0.1){//fix floating conversion error
				jUserProperties.put("longitude", longitude);
			}
			
						
			
//			jUser = new JSONObject();
//			jUser.put("user", jUserProperties);
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "User toJson error " + e.getMessage(), e);
		}
		
		return jUserProperties;
	}
	
	public void setAssigned(boolean assigned) {
		this.assigned = assigned;
	}
	
	public boolean isAssigned() {
		return assigned;
	}
	
	/**
	 * Equal if an user ID is identical to the another
	 */
	@Override	
	public boolean equals(Object o) {
		return o instanceof User 
				&& username != null && username.equals(((User)o).getUsername());
	}
	
	@Override
	public String toString() {
		return toJson().toString();
	}
}
