package com.touchtechnologies.dataobject.reporter;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.graphics.Media;
import com.touchtechnologies.json.JSONUtil;

public class Comment implements Serializable{
	private String commentID;
	private String commentContent;
	private User userAuthor;
	private long date;
	public ArrayList<Media> medias;
	
	private static final SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US );
	private static final SimpleDateFormat sdfOutDate = new SimpleDateFormat("d MMM yyyy  H:mm", new Locale("th_TH") );
	static Calendar calendar = Calendar.getInstance(new Locale("th_TH"));
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4282894352079262710L;

	public Comment() {
	}
	
	public Comment(JSONObject json) throws JSONException{
		if(!json.isNull("commentID")){
			setCommentID(JSONUtil.getString(json, "commentID"));
		}
		
		if(!json.isNull("commentContent")){
			setCommentContent(JSONUtil.getString(json, "commentContent"));
		}
		
		if(!json.isNull("auther")){
			userAuthor = new User(json.getJSONObject("auther"));
		}
	
		if(!json.isNull("post_date")){
			try{
				setDate(sdfDate.parse(JSONUtil.getString(json, "post_date")).getTime());
			}catch(Exception e){
				Log.e(LibConfig.LOGTAG, e.getMessage());
			}
		}
		
		if(!json.isNull("media")){
			medias = new ArrayList<Media>();
			
			try{
			JSONArray jMedias = json.getJSONArray("media");
			for(int i=0; i<jMedias.length(); i++){
				medias.add(new Media(jMedias.getJSONObject(i)));
			}
			}catch(Exception e){
				Log.e(LibConfig.LOGTAG, e.getMessage());
			}
		}
	}
	
	public void setDate(long date) {
		this.date = date;
	}
	
	public long getDate() {
		return date;
	}
	
	public String getDateString(){
		calendar.setTime(new Date(date));
		if(calendar.get(Calendar.YEAR) < 2500){
			calendar.add(Calendar.YEAR, 543);
		}
		
		return sdfOutDate.format(calendar.getTime());
	}
	
	public void setCommentID(String commentID) {
		this.commentID = commentID;
	}
	
	public String getCommentID() {
		return commentID;
	}
	
	public User getUserAuthor() {
		return userAuthor;
	}
	
	public void setUserAuthor(User userAuthor) {
		this.userAuthor = userAuthor;
	}
	
	public void setCommentContent(String commentContent) {
		this.commentContent = commentContent;
	}

	public String getCommentContent() {
		return commentContent;
	}
	
	public JSONObject toJson(){
		JSONObject json = new JSONObject();
		try{
			if(commentContent != null){
				json.put("commentContent", commentContent);
			}
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "convert comment to json error", e);
		}
		
		
		return json;
	}
}
