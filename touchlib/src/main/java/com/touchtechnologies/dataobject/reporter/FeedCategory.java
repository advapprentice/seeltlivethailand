package com.touchtechnologies.dataobject.reporter;

import java.io.Serializable;

import org.json.JSONObject;

import com.touchtechnologies.json.JSONUtil;

public class FeedCategory implements Serializable{
	private String ID;
	private String name;
	private String type;

	/**
	 * 
	 */
	private static final long serialVersionUID = 4811883060360092130L;

	public FeedCategory() {
	}
	
	public FeedCategory(JSONObject json) {
		if(!json.isNull("feedName")){
			setName(JSONUtil.getString(json, "feedName"));
		}
		
		if(!json.isNull("feedCategory")){
			setID(JSONUtil.getString(json, "feedCategory"));
		}
		
		if(!json.isNull("feedType")){
			setType(JSONUtil.getString(json, "feedType"));
		}
	}
	
	public void setID(String iD) {
		ID = iD;
	}
	
	public String getID() {
		return ID;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getType() {
		return type;
	}
}
