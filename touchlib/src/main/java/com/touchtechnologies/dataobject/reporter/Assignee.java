package com.touchtechnologies.dataobject.reporter;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.touchtechnologies.app.LibConfig;

public class Assignee extends TargetGroup{
	private static final long serialVersionUID = -1096275165525433992L;
	
	
	//private String user;
	private User user;
	public ArrayList<User> users;
	
	public Assignee() {
	}
	
	public Assignee(JSONObject json)throws JSONException {
		super(json);
		
		if(!json.isNull("assignees")){
			setMemberUsers(json.getJSONArray("assignees"));
		}
	}
	
	
	@Override
	public void setMemberUsers(JSONArray jsonUsers){
		//set user array
		if(memberUsers == null){
			memberUsers = new ArrayList<User>();
		}
				
		for(int i=0; i<jsonUsers.length(); i++){		
			try{
				JSONObject tmp = jsonUsers.getJSONObject(i);				
				memberUsers.add(new User(tmp.getJSONObject("user")));
				
			}catch(Exception e){
				Log.e("assignees", e.getMessage());
			}
		}		
	}

		
	
	public JSONObject toJson(){
		JSONObject json = new JSONObject();
		
		try{
			if(groupName != null){
				json.put("groupName", groupName);
			}
			
			if(groupTypeID != null){
				json.put("groupType", groupTypeID);
			}
//			
//			if(asSignees!= null){
//				
//				JSONArray jArrayUsers = new JSONArray();
//				
//				JSONObject objusers = new JSONObject();
//				objusers.put("user", user.toJson());
//				jArrayUsers.put(objusers);
//		
//				
//				json.put("assignees", jArrayUsers );
//			}
			
		
			if(user!= null){
				json.put("user",user.toJson());
			}
				
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "convert TargetGroup to json error", e);
		}
		
		return json;
	}

	
	@Override
	public int hashCode() {
		return groupTypeID == null?super.hashCode():groupTypeID.hashCode();
	}
}
