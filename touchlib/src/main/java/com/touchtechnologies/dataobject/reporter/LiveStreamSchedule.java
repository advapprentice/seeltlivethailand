package com.touchtechnologies.dataobject.reporter;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.json.JSONUtil;

import android.util.Log;

public class LiveStreamSchedule implements Serializable {
	
	private String streamingScheduleID;
	private String channelID;
	private String channelName;
	private String title;
	private String startDate;
	private String endDate;
	private String note;
	private String isOnSchedulePeriod;
	private String isHasStreaming;
	private String isHasStreamConnecting;
	private String isStreamable;
	
	
	private long ScheduleDatetime;
	private long ScheduleEndDatetime;
	private LiveStreamChannel livestreamchannel;
	public static final String SET_DISPLAYSTATUS = "Display";	
	public static final String SET_HASSTREAMING = "Y";	
	public static final String SET_YES = "Y";	
	public static final String SET_NO = "N";	
	
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
	static SimpleDateFormat sdfOut = new SimpleDateFormat("d MMM yyyy  H:mm", new Locale("th_TH"));
	static Calendar calendar = Calendar.getInstance(new Locale("th_TH"));
	
	
	public LiveStreamSchedule(){
		streamingScheduleID = "LOCAL" +  System.currentTimeMillis();
		ScheduleDatetime = System.currentTimeMillis();
		ScheduleEndDatetime = System.currentTimeMillis();
	}
	
	public LiveStreamSchedule(JSONObject json) throws JSONException{		
		if(!json.isNull("streamingScheduleID")){
			setStreamingScheduleID(json.getString("streamingScheduleID"));
		}
		
		if(!json.isNull("channelID")){
			setChannelID(json.getString("channelID"));
		}
		
		if(!json.isNull("channelName")){
			setChannelName(json.getString("channelName"));
		}
		
		if(!json.isNull("title")){
			setTitle(json.getString("title"));
		}
		
		if(!json.isNull("startDate")){
			try{
				String dateString = JSONUtil.getString(json, "startDate");
				if(dateString.indexOf(":") != -1){
					setStartDate(sdf.parse(dateString).getTime());
				}else{
					setStartDate(Long.parseLong(dateString));
				}
			}catch(Exception e){				
			}
			
		}

		if(!json.isNull("endDate")){
			try{
				String dateString = JSONUtil.getString(json, "endDate");
				if(dateString.indexOf(":") != -1){
					setEndDate(sdf.parse(dateString).getTime());
				}else{
					setEndDate(Long.parseLong(dateString));
				}
			}catch(Exception e){				
			}
		}

		if(!json.isNull("note")){
			setNote(json.getString("note"));
		}
		
		if(!json.isNull("isOnSchedulePeriod")){
			setIsOnSchedulePeriod(json.getString("isOnSchedulePeriod"));
		}
		if(!json.isNull("isHasStreaming")){
			setIsHasStreaming(json.getString("isHasStreaming"));
		}
		if(!json.isNull("isHasStreamConnecting")){
			setIsHasStreamConnecting(json.getString("isHasStreamConnecting"));
		}
		if(!json.isNull("isStreamable")){
			setIsStreamable(json.getString("isStreamable"));
		}
	
		
		if(!json.isNull("streamingChannel")){
			try{
				setLiveStreamChannel(new LiveStreamChannel(json.getJSONObject("streamingChannel")));
				
			}catch(Exception e){
				Log.e("streamingChannel", e.getMessage(), e);
			}
		}
		

	
	}
	
	
	
	public LiveStreamSchedule(String _json) throws JSONException{
		this(new JSONObject(_json));
	}	
	
	public void setStreamingScheduleID(String streamingScheduleID) {
		this.streamingScheduleID = streamingScheduleID;
	}
	public String getStreamingScheduleID() {
		return streamingScheduleID;
	}

	public void setChannelID(String channelID) {
		this.channelID = channelID;
	}
	public String getChannelID() {
		return channelID;
	}
	
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public String getChannelName() {
		return channelName;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle() {
		return title;
	}
	
		
	//////////// date
	

	public void setStartDate(String date){
//		this.createdDateString = date;
		try{
			setStartDate(sdf.parse(date).getTime());
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "parsed date error", e);
		}
	}

	public void setStartDate(long scheduledatetime) {
		this.ScheduleDatetime = scheduledatetime;
	}
	
	/**
	 * Get post_date
	 * @return
	 */
	public long getStartDate() {
		return ScheduleDatetime;
	}
	
	public String getStartDateString(){
		calendar.setTime(new Date(ScheduleDatetime));
		if(calendar.get(Calendar.YEAR) < 2500){
			calendar.add(Calendar.YEAR, 543);
		}
		
		return sdfOut.format(calendar.getTime());
	}
	
//// date end


	public void setEndDate(String date){
//		this.createdDateString = date;
		try{
			setEndDate(sdf.parse(date).getTime());
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "parsed date error", e);
		}
	}

	public void setEndDate(long scheduleenddatetime) {
		this.ScheduleEndDatetime = scheduleenddatetime;
	}
	
	/**
	 * Get post_date
	 * @return
	 */
	public long getEndDate() {
		return ScheduleEndDatetime;
	}
	
	public String getEndDateString(){
		calendar.setTime(new Date(ScheduleEndDatetime));
		if(calendar.get(Calendar.YEAR) < 2500){
			calendar.add(Calendar.YEAR, 543);
		}
		
		return sdfOut.format(calendar.getTime());
	}
	

	
	
	
	////
	public void setNote(String note) {
		this.note = note;
	}
	public String getNote() {
		return note;
	}
	
	public void setIsOnSchedulePeriod(String isOnSchedulePeriod) {
		this.isOnSchedulePeriod = isOnSchedulePeriod;
	}
	public String getIsOnSchedulePeriod() {
		return isOnSchedulePeriod;
	}
	
	public void setIsHasStreaming(String isHasStreaming) {
		this.isHasStreaming = isHasStreaming;
	}
	public String getIsHasStreaming() {
		return isHasStreaming;
	}
	
	public void setIsHasStreamConnecting(String isHasStreamConnecting) {
		this.isHasStreamConnecting = isHasStreamConnecting;
	}
	public String getIsHasStreamConnecting() {
		return isHasStreamConnecting;
	}
	public void setIsStreamable(String isStreamable) {
		this.isStreamable = isStreamable;
	}
	public String getIsStreamable() {
		return isStreamable;
	}
	

	public void setLiveStreamChannel(LiveStreamChannel livestream) {
	this.livestreamchannel = livestream;
	}
	public LiveStreamChannel getLiveStreamChannel() {
		return livestreamchannel;
	}
	
	
}

	