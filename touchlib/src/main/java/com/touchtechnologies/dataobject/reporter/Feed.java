package com.touchtechnologies.dataobject.reporter;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.graphics.Media;
import com.touchtechnologies.graphics.MediaHolder;
import com.touchtechnologies.json.JSONUtil;

public class Feed implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 9221883028215562790L;
	public static final String STATUS_DRAFT = "draft";
	public static final String STATUS_PENDING = "Pending";
	public static final String STATUS_APPROVED = "Approved";
	public static final String STATUS_REJECT = "Rejected";
	public static final String STATUS_DELETED = "Deleted";	

	public static final String STATUS_NEW = "New";
	public static final String STATUS_ACCEPTED = "Accepted";
	public static final String STATUS_HANDLE = "Handle";
	public static final String STATUS_COMPLETED = "Completed";
	
	public static final String TYPE_REPORTNEWS = "2";
	public static final String TYPE_COMPLAINT = "1";
	
	
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
	static SimpleDateFormat sdfOut = new SimpleDateFormat("d MMM yyyy  H:mm", new Locale("th_TH"));
	static Calendar calendar = Calendar.getInstance(new Locale("th_TH"));
	
	/**
	 * Media contains in this feed  
	 * *Google android pattern , I hate this public pattern - - " */;
	public ArrayList<Media> medias;
	/**
	 * Comment contains in this feed
	 * *Google android pattern , I hate this public pattern - - " 
	 */
	public ArrayList<Comment> comments;
	
	private String feedID;
	private String feedType;
	private String feedName;
	private String feedTypeIcon;
	private String feedTitle;
	private String feedContent;
	private String feedStatus;
	private String feedCategoryIcon;
	private String manageable;
	private FeedCategory feedCategory;
	public ArrayList<TargetGroup> targetGroups;
	private long createdtDate;
//	String createdDateString;
	private int numView;
	private int numLike;
	private int numDislike;
	private int numFollow;
	private boolean isLike;
	private boolean isDislike;
	private boolean isFollow;
	private int commentCount;
	private int deleteCount;
	private double latitude = Double.MIN_VALUE;
	private double longitude = Double.MIN_VALUE;	
	private User author;
	private User actor;
	private double distance;
	private int anonymous = -1;
	private int public_ = -1;
	
	/**
	 * Create a new Feed object with auto generate LOCALE id 
	 */
	public Feed(){
		feedID = "LOCAL" +  System.currentTimeMillis();
		createdtDate = System.currentTimeMillis();
//		createdDateString = sdf.format(new Date(createdtDate));
	}
	
	public Feed(JSONObject json) throws JSONException{
		if(!json.isNull("feedID")){
			setId(JSONUtil.getString(json, "feedID"));
		}
		
		if(!json.isNull("feedType")){
			setTypeKey(JSONUtil.getString(json, "feedType"));
		}
		
		if(!json.isNull("feedName")){
			setTypeName(JSONUtil.getString(json, "feedName"));
		}
		
		if(!json.isNull("feedTypeIcon")){
			setTypeIcon(JSONUtil.getString(json, "feedTypeIcon"));
		}
		
		if(!json.isNull("feedTitle")){
			setTitle(JSONUtil.getString(json, "feedTitle"));
		}
		
		
		if(!json.isNull("feedContent")){
			setContent(JSONUtil.getString(json, "feedContent"));
		}
		
		if(!json.isNull("feedStatus")){
			setStatus(JSONUtil.getString(json, "feedStatus"));
		}
		if(!json.isNull("manageable")){
			setManageable(JSONUtil.getString(json, "manageable"));
		}

		
		
		
		if(!json.isNull("feedCategoryID")){
			feedCategory = new FeedCategory();
			
			feedCategory.setID(JSONUtil.getString(json, "feedCategoryID"));
			feedCategory.setName(JSONUtil.getString(json, "feedCategoryName"));
		}
		
		if(!json.isNull("feedCategoryIcon")){
			setCategoryIcon(JSONUtil.getString(json,"feedCategoryIcon"));
		}
		
		if(!json.isNull("post_date")){
			try{
				String dateString = JSONUtil.getString(json, "post_date");
				if(dateString.indexOf(":") != -1){
					setCreatedtDate(sdf.parse(dateString).getTime());
				}else{
					setCreatedtDate(Long.parseLong(dateString));
				}
			}catch(Exception e){				
			}
		}		

		if(!json.isNull("follow")){
			setNumFollow(JSONUtil.getInt(json, "follow"));
		}
		
		if(!json.isNull("view")){
			setNumView(JSONUtil.getInt(json, "view"));
		}
		
		if(!json.isNull("like")){
			setNumLike(JSONUtil.getInt(json, "like"));
		}
		
		if(!json.isNull("dislike")){
			setNumDislike(JSONUtil.getInt(json, "dislike"));
		}
		
		if(!json.isNull("isFollow")){
			setFollow(JSONUtil.getInt(json, "isFollow") == 1);
		}
		
		if(!json.isNull("isLike")){
			setLike(JSONUtil.getInt(json, "isLike") == 1);
		}
		
		if(!json.isNull("isDislike")){
			setDislike(JSONUtil.getInt(json, "isDislike") == 1);
		}
		
		if(!json.isNull("deleteCount")){
			setDeleteCount(JSONUtil.getInt(json, "deleteCount"));
		}
		
		if(!json.isNull("commentCount")){
			setCommentCount(JSONUtil.getInt(json, "commentCount"));
		}
		
		if(!json.isNull("latitude")){
			setLatitude(JSONUtil.getDouble(json, "latitude"));
		}

		if(!json.isNull("longitude")){
			setLongitude(JSONUtil.getDouble(json, "longitude"));
		}

		if(!json.isNull("distance")){
			setDistance(JSONUtil.getDouble(json, "distance"));
		}
		
		if(!json.isNull("auther")){
			setAuthor(new User(json.getJSONObject("auther")));
		}
		
		if(!json.isNull("manageable")){
			setAuthor(new User(json.getJSONObject("manageable")));
		}


		JSONObject jsonActor = null;
		if(!json.isNull("actor")){
			jsonActor = json.getJSONObject("actor");
		}
		
		if(jsonActor == null && !json.isNull("feedAction")){
			jsonActor = json.getJSONObject("feedAction").getJSONObject("actor");
		}
		
		if(jsonActor != null){
			setActor(new User(jsonActor));
		}
		
		if(!json.isNull("media")){
			JSONArray jarrayMedia = json.getJSONArray("media");
			if(jarrayMedia.length() > 0){
				medias = new ArrayList<Media>();
				for(int i=0; i<jarrayMedia.length(); i++){
					medias.add(new Media(jarrayMedia.getJSONObject(i)));
				}
			}
		}
		
		if(!json.isNull("comments")){
			comments = new ArrayList<Comment>();
			JSONArray jarrayComment = json.getJSONArray("comments");
			for(int i=0; i<jarrayComment.length(); i++){
				comments.add(new Comment(jarrayComment.getJSONObject(i)));
			}
			commentCount = comments.size();
		}
	}
	
	/**
	 * Set feedID
	 * @param id
	 */
	public void setId(String id) {
		this.feedID = id;
	}
	
	/**
	 * Get feedID
	 * @return
	 */
	public String getId() {
		return feedID;
	}
	
	/**
	 * Add media objects from MediaHolder array to this Feed
	 * @param arrHolders
	 */
	public void addMedias(ArrayList<MediaHolder> arrHolders){
		for(MediaHolder mdHolder: arrHolders){
			addMedia(mdHolder);
		}
	}
	
	/**
	 * Create a new media object from MediaHolder then add to this Feed
	 * @param mdHolder
	 */
	public void addMedia(MediaHolder mdHolder){
		if(medias == null){
			medias = new ArrayList<Media>();
		}
		
		if(mdHolder.media != null){
			medias.add(mdHolder.media);
		}		
	}
	
	public void remove(Comment comment){
		comments.remove(comment);
	}
	
	public void addComment(Comment comment){
		if(comments == null){
			comments = new ArrayList<Comment>();
		}
		
		comments.add(comment);
	}

	
	/**
	 * Set feedType (key type)
	 * @param typeKey
	 */
	public void setTypeKey(String typeKey) {
		this.feedType = typeKey;
	}
	
	/**
	 * Get feedType (key type)
	 * @return
	 */
	public String getTypeKey() {
		return feedType;
	}
	
	/**
	 * Set feedName
	 * @param typeName feed display name
	 */
	public void setTypeName(String typeName) {
		this.feedName = typeName;
	}
		
	/**
	 * Get feedName
	 * @return feed display name
	 */
	public String getTypeName() {
		return feedName == null?"":feedName;
	}
	
	public void setFeedCategory(FeedCategory feedCategory) {
		this.feedCategory = feedCategory;
	}
	
	public FeedCategory getFeedCategory() {
		return feedCategory;
	}
	
	/**
	 * Set feedTypeIcon
	 * @param typeIcon URL of feed icon.
	 */
	public void setTypeIcon(String typeIcon) {
		this.feedTypeIcon = typeIcon;
	}
	
	/**
	 * Get feedTypeIcon, URL of feed icon. 
	 * @return URL of feed icon.
	 */
	public String getTypeIcon() {
		return feedTypeIcon;
	}
	/**
	 * Set feedTypeIcon
	 * @param typeIcon URL of feed icon.
	 */
	
	
	public void setCategoryIcon(String categoryIcon) {
		this.feedCategoryIcon = categoryIcon;
	}
	
	/**
	 * Get feedTypeIcon, URL of feed icon. 
	 * @return URL of feed icon.
	 */
	public String getCategoryIcon() {
		return feedCategoryIcon;
	}
	
	
	
	
	
	
	
	
	/**
	 * Set feedTitle
	 * @param title title, if any, such as news title
	 */
	public void setTitle(String title) {
		this.feedTitle = title;
	}
	
	/**
	 * Get feedTitle
	 * @return title, if any, such as news title
	 */
	public String getTitle() {
		return feedTitle == null?"":feedTitle;
	}
	
	/**
	 * Set feedContent
	 * @param content content, if any, such news content
	 */
	public void setContent(String content) {
		this.feedContent = content;
	}
	
	/**
	 * Get feedContent
	 * @return content, if any, such news content
	 */
	public String getContent() {
		return feedContent==null?"":feedContent;
	}
	
	/**
	 * Set post_date
	 * @param date
	 */
	public void setCreatedtDate(String date){
//		this.createdDateString = date;
		try{
			setCreatedtDate(sdf.parse(date).getTime());
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "parsed date error", e);
		}
	}
	
//	/**
//	 * Get post_date
//	 * @return
//	 */
//	public String getCreatedDateString(){
//		return createdDateString;
//	}
	
	/**
	 * Set post_date
	 * @param createdtDate
	 */
	public void setCreatedtDate(long createdtDate) {
		this.createdtDate = createdtDate;
	}
	
	/**
	 * Get post_date
	 * @return
	 */
	public long getCreatedtDate() {
		return createdtDate;
	}
	
	public String getCreatedDateString(){
		calendar.setTime(new Date(createdtDate));
		if(calendar.get(Calendar.YEAR) < 2500){
			calendar.add(Calendar.YEAR, 543);
		}
		
		return sdfOut.format(calendar.getTime());
	}
	
	public void setNumFollow(int numFollow) {
		this.numFollow = numFollow<0?0:numFollow;
	}
	
	public int getNumFollow() {
		return numFollow;
	}

	public void setNumView(int numView) {
		this.numView = numView<0?0:numView;
	}
	
	public int getNumView() {
		return numView;
	}
	
	public void setNumLike(int numLike) {
		this.numLike = numLike<0?0:numLike;
	}
	
	public int getNumLike() {
		return numLike;
	}
	
	public void setNumDislike(int numDislike) {
		this.numDislike = numDislike<0?0:numDislike;
		
	}
	
	public int getNumDislike() {
		return numDislike;
	}
	
	public void setLike(boolean isLike) {
		this.isLike = isLike;
	}
	
	public void setDislike(boolean isDislike) {
		this.isDislike = isDislike;
	}

	public void setFollow(boolean isFollow) {
		this.isFollow = isFollow;
	}
	public boolean isLike() {
		return isLike;
	}
	
	public boolean isDislike() {
		return isDislike;
	}
	public boolean isFollow() {
		return isFollow;
	}
	
	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}
	
	public int getCommentCount() {
		return commentCount;
	}
	
	public void setDeleteCount(int deleteCount) {
		this.deleteCount = deleteCount;
	}
	
	public int getDeleteCount() {
		return deleteCount;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	public void setManageable(String manageable) {
		this.manageable = manageable;
	}
	
	public String getManageable() {
		return manageable;
	}
	/**
	 * Set feedStatus; Approved | Rejected | Pending
	 * @param feedStatus
	 */
	public void setStatus(String feedStatus){
		this.feedStatus = feedStatus;
	}
	
	/**
	 * Get feedStatus; Approved | Rejected | Pending
	 * @return
	 */
	public String getStatus(){
		return feedStatus;
	}
	
	public void setAuthor(User author) {
		this.author = author;
	}
	
	public User getAuthor() {
		return author;
	}
	/////
	public void setActor(User actor) {
		this.actor = actor;
	}
	
	public User getActor() {
		return actor;
	}
	////
	public void addTargetGroup(TargetGroup targetGroup) {
		if(targetGroups == null){
			targetGroups = new ArrayList<TargetGroup>();
		}
		
		targetGroups.add(targetGroup);
		
	}
	
	public ArrayList<TargetGroup> getTargetGroups() {
		return targetGroups;
	}
	
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	public double getDistance() {
		return distance;
	}
	
	public void setAnonymous(int anonymous) {
		this.anonymous = anonymous;
	}
	
	public int getAnonymous() {
		return anonymous;
	}
	
	public void setPublic(int public_) {
		this.public_ = public_;
	}
	
	public int getPublic() {
		return public_;
	}
	
	public JSONObject toJson(){
		JSONObject json = new JSONObject();
		
		try{
			if(feedID != null){
				json.put("feedID", feedID);
			}
			
			if(feedType != null){
				json.put("feedType", feedType);
			}
			
			if(feedName != null){
				json.put("feedName", feedName);
			}
			
			if(feedTypeIcon  != null){
				json.put("feedTypeIcon", feedTypeIcon);
			}
			
			if(feedCategoryIcon != null){
				json.put("feedCategoryIcon", feedCategoryIcon);
			}
			
			if(feedTitle != null){
				json.put("feedTitle", feedTitle);
			}			
			
			if(feedContent != null){
				json.put("feedContent", feedContent);
			}
			
			if(feedStatus != null){
				json.put("feedStatus", feedStatus);
			}
			if(manageable != null){
				json.put("manageable", manageable);
			}
						
			if(feedCategory != null){
				json.put("feedCategoryID", feedCategory.getID());
				json.put("feedCategoryName", feedCategory.getName());
			}
			
			if(targetGroups != null){
				JSONArray jsonArray = new JSONArray();
				for(TargetGroup tg: targetGroups){
					jsonArray.put(tg.toJson());
				}
				
				json.put("targetGroup", jsonArray);
			}
			
			json.put("post_date", createdtDate);
			json.put("view", numView);
			json.put("like", numLike);
			json.put("dislike", numDislike);
			json.put("follow", numFollow);
			json.put("isLike", isLike?"1":"0");
			json.put("isDislike", isDislike?"1":"0");
			json.put("isFollow", isFollow?"1":"0");
			json.put("deleteCount", deleteCount);
			json.put("commentCount", commentCount);
			json.put("latitude", latitude);
			json.put("longitude", longitude);
						
			if(author!= null){
				json.put("auther", author.toJson());
			}
			
			if(actor!= null){
				json.put("actor", actor.toJson());
			}
			
			
			if(medias != null){
				JSONArray jsonMedias = new JSONArray();
				for(int i=0; i<medias.size(); i++){
					jsonMedias.put(medias.get(i).toJson());
				}
				
				json.put("media", jsonMedias);
			}
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "convert Feed to json error", e);
		}
		
		return json;
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof Feed)
				&& getId().equals(((Feed)o).getId());
	}
	
	@Override
	public String toString() {
		return toJson().toString();
	}
}
