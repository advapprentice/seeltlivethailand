package com.touchtechnologies.dataobject.reporter;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.json.JSONUtil;

public class Notification implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5275312054061973264L;
	public static final String STATUS_INCOMINGNEWS = "IncomingNews";
	public static final String STATUS_REPORTINAPPROPRIATENEWS = "ReportInappropriateNews";
	public static final String STATUS_COMMENTNEWS = "CommentNews";
	public static final String STATUS_LIKENEWS = "LikeNews";
	public static final String STATUS_UPDATENEWS = "UpdateNewsStatus";
	public static final String STATUS_UPDATEASSIGNEE = "UpdateNewsAssignee";
	public static final String STATUS_ALERTSTREAMING = "AlertStreamingAutoStop";
	public static final String STATUS_READ = "Read";
	public static final String STATUS_UNREAD = "Unread";
	public static final String STATUS_DELETE = "Delete";
	public static final String STATUS_CELAR_ALL = "ClearAllUnread";
	
	public static final String TYPE_REPORTNEWS = "2";
	public static final String TYPE_COMPLAINT = "1";

	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
	static SimpleDateFormat sdfOut = new SimpleDateFormat("d MMM yyyy  H:mm", new Locale("th_TH"));
	static Calendar calendar = Calendar.getInstance(new Locale("th_TH"));
	
	private String id;
	private String command;
	private String message;
	
	private String notificationID;
	private String notifiyEvent;
//	private String notifiyDatetime;
	private String notifyStatus;
	private String updateNotifyStatusDatetime;
	private String notifiyContent;
	private String notificationCount;
	
	private String feedTitle;
	private FeedCategory feedCategory;
	private Feed feed;
	private User actor;
	private long notifiyDatetime;
	private boolean read;
	public Notification(){
		notificationID = "LOCAL" +  System.currentTimeMillis();
		notifiyDatetime = System.currentTimeMillis();
	}

	public Notification(JSONObject json) throws JSONException{		
		if(!json.isNull("notificationID")){
			setNotificationID(json.getString("notificationID"));
		}
		
		if(!json.isNull("notifiyEvent")){
			setNotifiyEvent(json.getString("notifiyEvent"));
		}
		
		if(!json.isNull("notifiyDatetime")){
			try{
				String dateString = JSONUtil.getString(json, "notifiyDatetime");
				if(dateString.indexOf(":") != -1){
					setNotifiyDatetime(sdf.parse(dateString).getTime());
				}else{
					setNotifiyDatetime(Long.parseLong(dateString));
				}
			}catch(Exception e){				
			}
		}		
	
		
		if(!json.isNull("notifyStatus")){
			setNotifyStatus(json.getString("notifyStatus"));
		}
		
		if(!json.isNull("updateNotifyStatusDatetime")){
			setUpdateNotifyStatusDatetime(json.getString("updateNotifyStatusDatetime"));
		}
		
		if(!json.isNull("notifiyContent")){
			setNotifiyContent(json.getString("notifiyContent"));
		}
		if(!json.isNull("notificationCount")){
			setNotificationCount(json.getString("notificationCount"));
		}

		JSONObject jsonFeed = null;
		if(!json.isNull("feed")){
			jsonFeed = json.getJSONObject("feed");
		}
		JSONObject jsonActor= null;
		if(!json.isNull("actor")){
			jsonActor = json.getJSONObject("actor");
		}
		
		
		
		
	}

	public Notification(String _json) throws JSONException{
		this(new JSONObject(_json));
	}
	
	public void setNotificationID(String notificationID) {
		this.notificationID = notificationID;
	}
	
	public String getNotificationID() {
		return notificationID;
	}
	
	public void setNotifiyEvent(String notifiyEvent) {
		this.notifiyEvent = notifiyEvent;
	}
	
	public String getNotifiyEvent() {
		return notifiyEvent;
	}
	
	
	public void setNotifiyDatetime(String date){
//		this.createdDateString = date;
		try{
			setNotifiyDatetime(sdf.parse(date).getTime());
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "parsed date error", e);
		}
	}

	public void setNotifiyDatetime(long notifiyDatetime) {
		this.notifiyDatetime = notifiyDatetime;
	}
	
	/**
	 * Get post_date
	 * @return
	 */
	public long getNotifiyDatetime() {
		return notifiyDatetime;
	}
	
	public String getNotifiyDatetimeString(){
		calendar.setTime(new Date(notifiyDatetime));
		if(calendar.get(Calendar.YEAR) < 2500){
			calendar.add(Calendar.YEAR, 543);
		}
		
		return sdfOut.format(calendar.getTime());
	}
	
	
	public void setNotifyStatus(String notifyStatus) {
		this.notifyStatus = notifyStatus;
	}

	public String getNotifyStatus() {
		return notifyStatus;
	}

	public void setUpdateNotifyStatusDatetime(String updateNotifyStatusDatetime) {
		this.updateNotifyStatusDatetime = updateNotifyStatusDatetime;
	}

	public String getUpdateNotifyStatusDatetime() {
		return updateNotifyStatusDatetime;
	}
	public void setNotifiyContent(String notifiyContent) {
		this.notifiyContent = notifiyContent;
	}

	public String getNotifiyContent() {
		return notifiyContent;
	}
	
	public void setNotificationCount(String notificationCount) {
		this.notificationCount = notificationCount;
	}

	public String getNotificationCount() {
		return notificationCount;
	}
	
	
	
	
	public void setFeed(Feed feed) {
		this.feed = feed;
	}
	
	public Feed getFeed() {
		return feed;
	}
	public void setActor(User actor) {
		this.actor = actor;
	}
	
	public User getActor() {
		return actor;
	}

	public JSONObject toJson(){
		JSONObject jUserProperties = new JSONObject();
		try{
			jUserProperties = new JSONObject();
			if(notificationID != null){
				jUserProperties.put("notificationID", notificationID);
			}			
			
			if(notifiyEvent != null){
				jUserProperties.put("notifiyEvent", notifiyEvent);
			}
		
			if(notifyStatus != null){
				jUserProperties.put("notifyStatus", notifyStatus);
			}
			
			if(updateNotifyStatusDatetime != null){
				jUserProperties.put("updateNotifyStatusDatetime", updateNotifyStatusDatetime);
			}
		
			if(notifiyContent != null){
				jUserProperties.put("notifiyContent", notifiyContent);
			}
			if(notificationCount != null){
				jUserProperties.put("notificationCount", notificationCount);
			}
			jUserProperties.put("notifiyDatetime", notifiyDatetime);
			
			if(feed != null){
				jUserProperties.put("feed", feed);
				
				
//				jUserProperties.put("feedID", feed.getId());
//				jUserProperties.put("feedType", feed.getTypeName());
//				jUserProperties.put("feedTitle", feed.getTitle());
//				jUserProperties.put("feedStatus", feed.getStatus());
			}
		
			if(actor!= null){
				jUserProperties.put("actor", actor.toJson());
			}
			
		
//			jUser = new JSONObject();
//			jUser.put("user", jUserProperties);
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "User toJson error " + e.getMessage(), e);
		}
		
		return jUserProperties;
	}
	
	@Override
	public String toString() {
		return toJson().toString();
	}
	
	
	
	
	
	/*//// first code
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setCommand(String command) {
		this.command = command;
	}
	
	public String getCommand() {
		return command;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}
	
	public long getCreatedDate() {
		return createdDate;
	}
	
	public boolean isRead() {
		return read;
	}
	
	public void setRead(boolean read) {
		this.read = read;
	}
*/
}
