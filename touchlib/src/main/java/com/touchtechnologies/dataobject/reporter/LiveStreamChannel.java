package com.touchtechnologies.dataobject.reporter;


import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.json.JSONUtil;

import android.util.Log;

public class LiveStreamChannel implements Serializable {
	
	private String channelID;
	private String channelName;
	private String hasStreaming;
	private String hasConnecting;
	private String hasAllowConnecting;
	private LiveStreamConnection livestream;
	public static final String SET_DISPLAYSTATUS = "Display";	
	public static final String SET_HASSTREAMING = "Y";	
	
	public LiveStreamChannel(){
	}
	
	public LiveStreamChannel(JSONObject json) throws JSONException{		
		if(!json.isNull("channelID")){
			setChannelID(json.getString("channelID"));
		}
		
		if(!json.isNull("channelName")){
			setChannelName(json.getString("channelName"));
		}
		
		if(!json.isNull("hasStreaming")){
			setHasStreaming(json.getString("hasStreaming"));
		}
		
		if(!json.isNull("hasConnecting")){
			setHasConnecting(json.getString("hasConnecting"));
		}
		
		if(!json.isNull("hasAllowConnecting")){
			setHasAllowConnecting(json.getString("hasAllowConnecting"));
		}
		
		if(!json.isNull("streamings")){
			try{
				if(json.getJSONArray("streamings").length() > 0){
					setLiveStream(new LiveStreamConnection(json.getJSONArray("streamings").getJSONObject(0)));
				}
				
			}catch(Exception e){
				Log.e("streamings", e.getMessage(), e);
			}
		}
		

	
	}
	
	
	
	public LiveStreamChannel(String _json) throws JSONException{
		this(new JSONObject(_json));
	}	
	
	
	public void setChannelID(String channelID) {
		this.channelID = channelID;
	}
	public String getChannelID() {
		return channelID;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setHasStreaming(String hasStreaming) {
		this.hasStreaming = hasStreaming;
	}
	public String getHasStreaming() {
		return hasStreaming;
	}
	public void setHasConnecting(String hasConnecting) {
		this.hasConnecting = hasConnecting;
	}
	public String getHasConnecting() {
		return hasConnecting;
	}
	public void setHasAllowConnecting(String hasAllowConnecting) {
		this.hasAllowConnecting = hasAllowConnecting;
	}
	public String getHasAllowConnecting() {
		return hasAllowConnecting;
	}
	
	public void setLiveStream(LiveStreamConnection livesm) {
		this.livestream = livesm;
	}
	
	public LiveStreamConnection getLiveStream() {
		return livestream;
	}
	
}

	