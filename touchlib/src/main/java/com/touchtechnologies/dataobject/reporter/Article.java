package com.touchtechnologies.dataobject.reporter;

import java.io.Serializable;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.touchtechnologies.graphics.Media;
import com.touchtechnologies.json.JSONUtil;

public class Article implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6545847745920413912L;
	
	private String articleID;
	private String topicID;
	private String topictitle;
	private String articleTitle;
	private String view;
	private String views;
	private String medies;
	public Article() {
	}
	
	public Article(JSONObject json){
		if(!json.isNull("articleID")){
			setArticleID(JSONUtil.getString(json,"articleID"));
		}
		if(!json.isNull("topicID")){
			setTopicId(JSONUtil.getString(json,"topicID"));
		}
				
		if(!json.isNull("topicTitle")){
			settopicTitle(JSONUtil.getString(json,"topicTitle"));
		}
		if(!json.isNull("articleTitle")){
			setArticleTitle(JSONUtil.getString(json,"articleTitle"));
		}
		if(!json.isNull("views")){
			setViews(JSONUtil.getString(json,"views"));
		}
		if(!json.isNull("URLs")){
				try{
					JSONObject jsonUrl = json.getJSONObject("URLs");
					//"Viewurl"
					setURLsView(jsonUrl.getString("view"));
			
				}catch(Exception e){
				}	
			}  
		if(!json.isNull("medias")){
			try{
				JSONArray jsonUrl = json.getJSONArray("medias");
				for(int i = 0;i<jsonUrl.length(); i++){

				            JSONObject myObj = jsonUrl.getJSONObject(i);
				            setMediaThumbURL(myObj.getString("mediaThumbURL"));
				}
			
			}catch(Exception e){
			}	
		}
	}

	
	public void setArticleID(String articleID) {
		this.articleID = articleID;
	}
	
	public String getArticleID() {
		return articleID;
	}
	
	public void setTopicId(String topicID) {
		this.topicID = topicID;
	}
	
	public String getTopicId() {
		return topicID;
	}
	
	public void settopicTitle(String topictitle) {
		this.topictitle = topictitle;
	}
	
	public String gettopicTitle() {
		return topictitle;
	}
	public void setArticleTitle(String articleTitle) {
		this.articleTitle = articleTitle;
	}
	public String getArticleTitle() {
		return articleTitle;
	}
	
	public void setURLsView(String uRLs) {
		view = uRLs;
	}
	public String getURLsView() {
		return view;
	}
	public void setViews(String views) {
		this.views = views;
	}
	public String getViews() {
		return views;
	}
	public void setMediaThumbURL(String medie) {
		this.medies = medie;
	}
	public String getMediaThumbURL() {
		return medies;
	}
	
}
