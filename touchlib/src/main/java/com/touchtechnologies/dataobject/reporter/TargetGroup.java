package com.touchtechnologies.dataobject.reporter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.json.JSONUtil;

public class TargetGroup implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1096275165525433992L;
	protected String groupTypeID;
	protected String groupName;
	//private String user;
	private User users;
	protected List<User> memberUsers;
	public TargetGroup() {
	}
	
	public TargetGroup(JSONObject json)throws JSONException {
		if(!json.isNull("groupType")){
			setGroupTypeID(JSONUtil.getString(json, "groupType"));
		}
		
		if(!json.isNull("groupName")){
			setGroupName(JSONUtil.getString(json, "groupName"));
		}
		
		if(!json.isNull("user")){
			setUsers(new User(json.getJSONObject("user")));
		}
		
		if(!json.isNull("users")){
			setMemberUsers(json.getJSONArray("users"));
		}
				
	}
	
	public void setMemberUsers(JSONArray jsonUsers){
		//set user array
		if(memberUsers == null){
				
			memberUsers = new ArrayList<User>();
		}
				
		for(int i=0; i<jsonUsers.length(); i++){		
			try{
				memberUsers.add(new User(jsonUsers.getJSONObject(i)));
				
			}catch(Exception e){
				Log.e("TargetGroup", e.getMessage());
			}
		}		
	}
	
	public boolean contains(User user){
		boolean contain = false;
		
		if(memberUsers != null){
			contain = memberUsers.contains(user);
		}
		
		return contain;
	}
	
	public void addMemberUser(User user){
		if(memberUsers == null){
			memberUsers = new ArrayList<User>();
		}
		
		if(!memberUsers.contains(user)){
			memberUsers.add(user);
		}
	}
	
	public void removeMemberUser(User user){
		if(memberUsers != null){
			memberUsers.remove(user);
		}
	}
	
	public List<User> getMemberUsers() {
		return memberUsers;
	}
	
	public int getMemberCount(){
		return memberUsers == null?0:memberUsers.size();
	}
	
	public User getMemberAt(int position){
		return memberUsers.get(position);
	}
	
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public String getGroupName() {
		return groupName;
	}
	
	public void setGroupTypeID(String groupType) {
		this.groupTypeID = groupType;
	}
	
	public String getGroupTypeID() {
		return groupTypeID;
	}
		
	public void setUsers(User users) {
		this.users = users;
	}
	
	public User getUsers() {
		return users;
	}

	public void updateAssignee(User user, boolean assigned){
		if(memberUsers != null){
			user = memberUsers.get(memberUsers.indexOf(user));
			user.setAssigned(assigned);
		}
	}

	public JSONObject toJson(){
		JSONObject json = new JSONObject();
		
		try{
			if(groupName != null){
				json.put("groupName", groupName);
			}
			
			if(groupTypeID != null){
				json.put("groupType", groupTypeID);
			}
			
			if(users!= null){
				json.put("users", users.toJson());
			}
			
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "convert TargetGroup to json error", e);
		}
		
		return json;
	}
	
	@Override
	public int hashCode() {
		return groupTypeID == null?super.hashCode():groupTypeID.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof TargetGroup) && (((TargetGroup)o).groupTypeID.equals(groupTypeID));
	}
}
