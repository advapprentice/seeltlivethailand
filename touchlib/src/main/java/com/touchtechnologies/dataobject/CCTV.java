package com.touchtechnologies.dataobject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.dataobject.insightthailand.Comment;
import com.touchtechnologies.json.JSONUtil;

public class CCTV implements Serializable,Comparable<CCTV>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 749548868907640918L;

	/**
	 * id_provider_information_tag_keyname
	 */
	private String id;
	/**
	 * provider_information_tag_keyname
	 */
	private String keyName;
	/**
	 * provider_information_tag_name
	 */
	private String url;
	private String idcctv;
	private String urlcctv;
	private String phoneNumber;
	private String webSite;
	private String facebook;
	private int online_status;
	private int view;
	private ArrayList<Comment> comments;
	/**
	 * ROI's category
	 */
	private List<Category> categories;
	private List<CCTVHighlight> cctvHighlight;
	private Double distance;
	private String created;
	private String updated;
	private String descriptionen;
	private	double	latitude;
	private	double	longitude;
	private String  label_en;
	private final String KEY_ID = "id_cctv";
	private final String KEY_KEYNAME = "name";
	private final String KEY_URL = "live_url";
	private final String KEY_SHARE_URL = "url";
	private final String KEY_DESCRIPTION_EN = "description_en";
	private final String KEY_CREATEDATE = "created";
	private final String KEY_UPDATEDATE = "updated";
	private final String KEY_LATITUDE = "latitude";
	private final String KEY_LONGITUDE = "longitude";
	private final String KEY_LABEL_EN= "label_en";
	private final String KEY_HIGHLIGH= "cctv_highlights";
	private final String KEY_PHONE = "phone_number";
	private final String KEY_WEBSITE = "website";
	private final String KEY_FACEBOOK = "facebook";
	private final String KEY_VIEW = "view";
	private final String KEY_ONLINE_STATUS = "online_status";
	 
	public CCTV(JSONObject json) throws JSONException{
		if(!json.isNull(KEY_ID)){
			setId(JSONUtil.getString(json, KEY_ID));
		}
		if(!json.isNull(KEY_PHONE)){
			setPhoneNumber(JSONUtil.getString(json, KEY_PHONE));
		}
		if(!json.isNull(KEY_WEBSITE)){
			setWebSite(JSONUtil.getString(json, KEY_WEBSITE));
		}
		if(!json.isNull(KEY_FACEBOOK)){
			setFacebook(JSONUtil.getString(json, KEY_FACEBOOK));
		}
		if(!json.isNull(KEY_VIEW)){
			setView(JSONUtil.getInt(json, KEY_VIEW));
		}
		
		if(!json.isNull(KEY_KEYNAME)){
			setKeyName(JSONUtil.getString(json, KEY_KEYNAME));
		}
		if(!json.isNull(KEY_URL)){
			setUrl(JSONUtil.getString(json, KEY_URL));
		}
		if(!json.isNull(KEY_DESCRIPTION_EN)){
			setDescription(JSONUtil.getString(json, KEY_DESCRIPTION_EN));
		}
		if(!json.isNull(KEY_LATITUDE)){
			setLatitude(JSONUtil.getDouble(json, KEY_LATITUDE));
		}
		if(!json.isNull(KEY_LONGITUDE)){
			setLongitude(JSONUtil.getDouble(json, KEY_LONGITUDE));
		}
		if(!json.isNull(KEY_LABEL_EN)){
			setLabel_en(JSONUtil.getString(json, KEY_LABEL_EN));
		}
		if(!json.isNull(KEY_ONLINE_STATUS)){
			setOnlineStatus(JSONUtil.getInt(json, KEY_ONLINE_STATUS));
		}
		if(!json.isNull(KEY_SHARE_URL)){
			
				try{
					JSONArray jsonUrl = json.getJSONArray(KEY_SHARE_URL);
				
					  
					  for (int i = 0; i < jsonUrl.length(); i++) {
					    
					    JSONObject objcctv = jsonUrl.getJSONObject(i); 
				
					//"Form"
					    setIDCCTV(objcctv.getString("id"));
				
					//"Report"
					    setUrlCCTV(objcctv.getString("url"));
					  }
				}catch(Exception e){
					
				}
			}
		if(!json.isNull(KEY_HIGHLIGH)){
			JSONArray cctvs = json.getJSONArray(KEY_HIGHLIGH);
	
			//TODO add "ALL" categpry filter
			
			for(int i=0; i<cctvs.length(); i++){
				
				
				addCCTVHighlight(new CCTVHighlight(cctvs.getJSONObject(i)));
			
//				addCategory(this.getCategoryAt(c));
			}
		}
		
		
		
	}
	

	public void setCCTVHighlight(List<CCTVHighlight> cctv){
		this.cctvHighlight = cctv;
	}
	
	public void addCCTVHighlight(CCTVHighlight cctv){
		if(cctvHighlight == null){
			this.cctvHighlight = new ArrayList<CCTVHighlight>();
		}
		
		cctvHighlight.add(cctv);
	}
	
	public CCTVHighlight getCCTVHighlightAt(int position){
		return cctvHighlight.get(position);
	}
	
	public List<CCTVHighlight> getCCTVHighlights(){
		return cctvHighlight;
	}
	
	/**
	 * Number of categories
	 * @return
	 */
	public int sizeCCTV(){
		return cctvHighlight == null?0:cctvHighlight.size();
	}
	
	
	
	
	
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}
	
	public String getKeyName() {
		return keyName;
	}
	
	/**
	 * 
	 * @param categories
	 */
	public void setCategories(List<Category> categories){
		this.categories = categories;
	}
	
	public void addCategory(Category category){
		if(categories == null){
			this.categories = new ArrayList<Category>();
		}
		
		categories.add(category);
	}
	
	public Category getCategoryAt(int position){
		return categories.get(position);
	}
	
	/**
	 * Number of categories
	 * @return
	 */
	public int sizeCategory(){
		return categories == null?0:categories.size();
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setCreated(String created) {
		this.created = created;
	}
	
	public String getCreated() {
		return created;
	}
	
	public void setUpdated(String updated) {
		this.updated = updated;
	}
	
	public String getUpdated() {
		return updated;
	}
	
	public void setDescription(String descriptionen) {
		this.descriptionen = descriptionen;
	}
	public String getDescription() {
		return descriptionen;
	}
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLongitude() {
		return longitude;
	}
	
	public void setLabel_en(String label_en) {
		this.label_en = label_en;
	}
	public String getLabel_en() {
		return label_en;
	}
	
	public void setIDCCTV(String urlid) {
		this.idcctv = urlid;
	}
	public String getsetIDCCTV() {
		return idcctv;
	}
	public void setUrlCCTV(String urlcctv) {
		this.urlcctv = urlcctv;
	}
	
	public String getUrlCCTV() {
		return urlcctv;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}
	public String getWebSite() {
		return webSite;
	}
	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}
	public String getFacebook() {
		return facebook;
	}
	public void setView(int view) {
		this.view = view;
	}
	public int getView() {
		return view;
	}

	public void  addComment(Comment comment){
			if(comments == null){
				comments = new ArrayList<Comment>();
				comments.add(comment);
				return;
			}else {
				comments.add(comment);
			}
	}

	public void setComments(ArrayList<Comment> commentArrayList){
		this.comments = commentArrayList;
	}
	public ArrayList<Comment> getComments(){
		return this.comments;
	}

	@Override
	public boolean equals(Object o) {
		return (o instanceof CCTV) && ((CCTV)o).getKeyName().equals(getKeyName());
	}
	
	 public void setOnlineStatus(int online_status) {
		this.online_status = online_status;
	}
	 public int getOnlineStatus() {
		return online_status;
	}
	 
	@Override
	public String toString() {
		String string = null;
		
		try{
			string = toJson().toString();
		}catch(Exception e){
		}
		
		return string;
	}
	
	
	public JSONObject toJson() throws JSONException{
		JSONObject json = new JSONObject();
		
		if(getId() != null){
			json.put(KEY_ID, getId());
		}
		
		if(getKeyName() != null){
			json.put(KEY_KEYNAME, getKeyName());
		}
		
		if(getUrl() != null){
			json.put(KEY_URL, getUrl());
		}

		if(getDescription() != null){
			json.put(KEY_DESCRIPTION_EN, getDescription());
		}
		if(getLatitude()!= 0){
			json.put(KEY_LATITUDE, getLatitude());
		}
		if(getLongitude()!= 0){
			json.put(KEY_LONGITUDE, getLongitude());
		}
		if(getPhoneNumber()!= null){
			json.put(KEY_PHONE, getPhoneNumber());
		}
		if(getWebSite()!= null){
			json.put(KEY_WEBSITE, getWebSite());
		}
		if(getFacebook()!= null){
			json.put(KEY_FACEBOOK, getFacebook());
		}
		if(getView()!= 0){
			json.put(KEY_VIEW, getView());
		}
		if(getLabel_en() != null){
			json.put(KEY_LABEL_EN, getLabel_en());
		}
		
		if(getsetIDCCTV()!= null){
			json.put("id", getsetIDCCTV());
		}
		if(getUrlCCTV()!= null){
			json.put("url", getUrlCCTV());
		}
		if(getOnlineStatus()!= 0){
			json.put("online_status", getOnlineStatus());
		}

		return json;
	}


	
	public Double getDistance() {
		return distance;
	}


	
	public void setDistance(Double distance) {
		this.distance = distance;
	}


	@Override
	public int compareTo(CCTV another) {

		return distance.compareTo(another.getDistance());
	}




	
	
	
	
}
