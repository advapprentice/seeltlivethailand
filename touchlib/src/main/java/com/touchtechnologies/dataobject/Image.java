package com.touchtechnologies.dataobject;

import java.io.Serializable;

/**
 * Image uri nd it thumb nail
 * @author MEe
 *
 */
public class Image implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 0xadd0d6f68b53610dL;
	
	private String thumbnail;
	private String uri;
	private String id;
	private int state;
	private int icon;
	
	public Image(){
	}
	
	public Image(String uri){
		this.uri = uri;
	}
	
	public Image(String uri, String thumbUri){
		this.uri = uri;
		this.thumbnail = thumbUri;
	}
	public Image(String uri, String thumbUri,String id){
		this.uri = uri;
		this.thumbnail = thumbUri;
		this.id = id;
	}
	
	
	public String getThumbnail() {
		return thumbnail;
	}
	
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	
	public String getUri() {
		return uri;
	}
	
	public void setUri(String uri) {
		this.uri = uri;
	}
	
	public void setIcon(int icon){
		this.icon = icon;
	}
	
	public int getIcon(){
		return icon;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}
	
	
	
}
