package com.touchtechnologies.dataobject;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.provider.ContactsContract.CommonDataKinds.Phone;

import com.touchtechnologies.dataobject.insightthailand.PoiGallery;
import com.touchtechnologies.json.JSONUtil;
import com.touchtechnologies.log.Log;

/**
 * Base object of business type in hospitality industry.
 * @author MEe
 *
 */
public class Hospitality extends DataItem{
	
	private static final long serialVersionUID = 0xc6629f9800003748L;
	private final String[] KEY_POI = {"hotel_gallery", "attraction_gallery", "restaurant_gallery"};


	private String providerType;
	private String providerTypeKeyname;
	
	/**
	 * Phone numbers
	 */
	private ArrayList<PhoneNumber> arrPhoneNumbers;
	private List<PoiGallery> arrgall;
	/**
	 * Image gallery of this hospitality
	 */
	private ImageGallery imageGallery;
	
	
	/**
	 * Web site url 
	 */
	private String webUrl;
	
	private String logo;
	private String logoCover;
	/**
	 * Contact email address
	 */
	private String email;
	private String idHotel;
	//restaurant
	private String paycredit ;
	private String wongnaiurl;
	private String opentime;
	private String closetime;
	private String openDaily;
	private double distanceFromCity;
	private double distanceFromAirport;
	private int ratingInt;
	private String provinceId;
	private double rating;
	private double latitude = Double.MIN_VALUE;
	private double longitude = Double.MIN_VALUE;
	private String provinceNameEn;
	private String phone;
	private String checkInTime;
	private String checkOutTime;
	private String wifiAvailable;
	private	String parkingAvailable;
	private String nonSmokingZone;
	private String timetoairport;
	private int totalRoom;
	private int totalFloor;
	private int totalRestaurant;
	private int totalBar;

	public Hospitality(){
	}
	
	public Hospitality(JSONObject json) {
	
		if(!json.isNull("providerId")){
			setId(JSONUtil.getString(json, "providerId"));
		}
		
		if(!json.isNull("provider_id")){
			setId(JSONUtil.getString(json, "provider_id"));
		}
		
		setProviderType(JSONUtil.getString(json, "providerType"));
		
		if(!json.isNull("provider_type_keyname")){
			setProviderTypeKeyname(JSONUtil.getString(json, "provider_type_keyname"));
		
		}else if(json.isNull("providerTypeKeyname")){
			setProviderTypeKeyname(JSONUtil.getString(json, "providerTypeKeyname"));
		}
		
		//setdistance
		if(!json.isNull("distance")){
			setDistance(JSONUtil.getString(json, "distance"));
		}
		
		if(!json.isNull("distance_from_city_center")){
			setDistanceFromCity(JSONUtil.getDouble(json, "distance_from_city_center"));
		}
		
		if(!json.isNull("distance_to_airport")){
			setDistanceFromAirport(JSONUtil.getDouble(json, "distance_to_airport"));
		}
		setProvinceNameEn(JSONUtil.getString(json, "province_name_en"));
		//id_poi
		setIdHotel(JSONUtil.getString(json, "id_poi"));
				
		//name
		setName(JSONUtil.getString(json, "name_en"));
		
		// email
		setEmail(JSONUtil.getString(json, "email"));
		

		if(!json.isNull("total_room")){
			setTotalRoom(JSONUtil.getInt(json,"total_room"));
		}
		if(!json.isNull("number_of_floors")){
			setTotalFloor(JSONUtil.getInt(json,"number_of_floors"));
		}
		if(!json.isNull("number_of_restaurants")){
			setTotalRestaurant(JSONUtil.getInt(json,"number_of_restaurants"));
		}
		if(!json.isNull("number_of_bar")){
			setTotalBar(JSONUtil.getInt(json,"number_of_bar"));
		}
//		"number_of_bar": null,
//				"number_of_floors": null,
//				"number_of_restaurants": null,
		if(!json.isNull("number_of_bar")){
			setTotalBar(JSONUtil.getInt(json,"number_of_bar"));
		}
		if(!json.isNull("number_of_floors")){
			setTotalFloor(JSONUtil.getInt(json,"number_of_floors"));
		}
		if(!json.isNull("number_of_restaurants")){
			setTotalRestaurant(JSONUtil.getInt(json,"number_of_restaurants"));
		}
		if(!json.isNull("time_to_airport")){
			setTimetoairport(JSONUtil.getString(json,"time_to_airport"));
		}
		if(!json.isNull("restaurant_gallery")){
			JSONArray poi = null;
			try {

					poi = json.getJSONArray("restaurant_gallery");
					for(int t=0; t<poi.length(); t++){

						addPoiGallery(new PoiGallery(poi.getJSONObject(t)));


				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		if(!json.isNull("hotel_gallery")){
			JSONArray poi = null;
			try {
				poi = json.getJSONArray("hotel_gallery");
				for(int t=0; t<poi.length(); t++){

					addPoiGallery(new PoiGallery(poi.getJSONObject(t)));


				}


			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		if(!json.isNull("attraction_gallery")){
			JSONArray poi = null;
			try {
				poi = json.getJSONArray("attraction_gallery");
				for(int i=0; i<poi.length(); i++){

					addPoiGallery(new PoiGallery(poi.getJSONObject(i)));

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		
		// address_en
		setDisplayAddress(JSONUtil.getString(json, "address_en"));

		// address_th

		// cover_image
		Image image = new Image(JSONUtil.getString(json, "logo_image"), JSONUtil.getString(json, "cover_image"));
		setImage(image);

		
		// set logo
		setLogo(JSONUtil.getString(json, "logo_image"));

		// set logo
		setLogoCover(JSONUtil.getString(json, "cover_image"));

		// short_description_en
		setDescription(JSONUtil.getString(json, "short_description_en"));

		// short_description_th

		// website
		setWebUrl(JSONUtil.getString(json, "website"));

		// rating
		setRating(JSONUtil.getDouble(json, "rating"));
		
		setRatingInt(JSONUtil.getInt(json, "rating"));
		// phone
		addPhoneNumber(Phone.TYPE_MOBILE, JSONUtil.getString(json, "phone"));

		//set province
		setProvinceId(JSONUtil.getString(json, "province_id"));
		
		setPhone(JSONUtil.getString(json, "phone"));
		
		// latitude, longitude
		setLocation(JSONUtil.getDouble(json, "latitude"), JSONUtil.getDouble(json, "longitude"));

		setLatitude(JSONUtil.getDouble(json, "latitude"));
		setLongitude(JSONUtil.getDouble(json, "longitude"));
		
///<<<<<<<< Restaurant>>>>>>>>>>>>	
		
		//check_in_time
		setCheckInTime(JSONUtil.getString(json, "check_in_time"));
		//check_out_time
		setCheckOutTime(JSONUtil.getString(json, "check_out_time"));
		// Description restaurant
		setPaycredit(JSONUtil.getString(json, "pay_by_credit_card"));
			
		// Description restaurant
		setWongnaiurl(JSONUtil.getString(json, "wongnai_url"));
		
		setOpentime(JSONUtil.getString(json, "restaurant_weekday_opentime"));
		
		setClosetime(JSONUtil.getString(json, "restaurant_weekday_closetime"));
		
		setOpenDaily(JSONUtil.getString(json, "open_daily"));

		setWifiAvailable(JSONUtil.getString(json, "wifi_available"));
		setParkingAvailable(JSONUtil.getString(json, "parking_available"));
		setNonSmokingZone(JSONUtil.getString(json, "non_smoking_zone"));




//		// last_modified
//		String dateString = JSONUtil.getString(json, "last_modified");
//		if (dateString != null && dateString.length() > 1) {
//			try {
//				SimpleDateFormat sdf = new SimpleDateFormat(
//						"yyyy-MM-dd HH:mm:ss");// //"2013-03-07 13:40:47"
//				setLastModified(sdf.parse(dateString).getTime());
//			} catch (Exception e) {
//				Log.log("parse date " + dateString, e);
//			}
//		}

		// gallery
		if (!json.isNull("gallery")) {
			try {
				JSONArray imgArray = json.getJSONArray("gallery");
				for (int img = 0; img < imgArray.length(); img++) {
					JSONObject tmp = imgArray.getJSONObject(img);
					addImageToGallery(new Image(JSONUtil.getString(tmp, "cover_image"), JSONUtil.getString(tmp, "logo_image")));
				}
			} catch (JSONException je) {
				Log.log("parsing gallery", je);
			}
		}
	}


	public void setPoiGallery(List<PoiGallery> poi){
		this.arrgall = poi;
	}

	public void addPoiGallery(PoiGallery poi){
		if(arrgall == null){
			this.arrgall = new ArrayList<PoiGallery>();
		}

		arrgall.add(poi);
	}

	public PoiGallery getPoiGalleryAt(int position){
		return arrgall.get(position);
	}

	public List<PoiGallery> getPoiGallery(){
		return arrgall;
	}

	/**
	 * Number of categories
	 * @return
	 */
	public int sizeGallery(){
		return arrgall == null?0: arrgall.size();
	}



	public void setIdHotel(String idHotel) {
		this.idHotel = idHotel;
	}
	public String getIdHotel() {
		return idHotel;
	}
	
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}
	
	public String getProviderType() {
		return providerType;
	}
	
	public void setProviderTypeKeyname(String providerTypeKeyname) {
		this.providerTypeKeyname = providerTypeKeyname;
	}
	
	public String getProviderTypeKeyname() {
		return providerTypeKeyname;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setWebUrl(String webUrl) {
		this.webUrl = webUrl;
	}
	
	public String getWebUrl() {
		return webUrl;
	}
	
	/**
	 * Get number of image in the gallery
	 * @return
	 */
	public int getImageGallerySize(){
		return imageGallery == null?0:imageGallery.getImageCount();
	}
	
	public ImageGallery getImageGallery(){
		return imageGallery;
	}
	
	public void setImageGallery(ImageGallery imageGallery){
		this.imageGallery = imageGallery;
	}
	
	public void addImageToGallery(Image image){
		if(imageGallery == null){
			imageGallery = new ImageGallery();
		}
		
		imageGallery.add(image);
	}
	
	public int getPhoneNumberCount(){
		return arrPhoneNumbers == null?0:arrPhoneNumbers.size();
	}
	
	
	public PhoneNumber getPhoneNumberAt(int index){
		return arrPhoneNumbers.get(index);
	}
	
	public void addPhoneNumber(int type, String number){
		addPhoneNumber(new PhoneNumber(type, number));
	}
	
	public void addPhoneNumber(PhoneNumber phonNumber){
		if(arrPhoneNumbers == null){
			arrPhoneNumbers = new ArrayList<PhoneNumber>();
		}
		
		arrPhoneNumbers.add(phonNumber);
	}
	
	public void setRating(double rating) {
		this.rating = rating;
	}
	
	public double getRating() {
		return rating;
	}
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLongitude() {
		return longitude;
	}
	
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getLogo() {
		return logo;
	}
	/// Restaurant

	public void setProvinceNameEn(String province_name_en) {
		this.provinceNameEn = province_name_en;
	}
	public String getProvinceNameEn() {
		return provinceNameEn;
	}
	
	public void setDistanceFromAirport(double distanceFromAirport) {
		this.distanceFromAirport = distanceFromAirport;
	}
	
	public void setDistanceFromCity(double distanceFromCity) {
		this.distanceFromCity = distanceFromCity;
	}
	
	public double getDistanceFromAirport() {
		return distanceFromAirport;
	}
	
	public double getDistanceFromCity() {
		return distanceFromCity;
	}
	
	public void setPaycredit(String paycredit) {
		this.paycredit = paycredit;
	}
	public String getPaycredit() {
		return paycredit;
	}
	public void setWongnaiurl(String wongnaiurl) {
		this.wongnaiurl = wongnaiurl;
	}
	public String getWongnaiurl() {
		return wongnaiurl;
	}
	public void setRatingInt(int ratingInt) {
		this.ratingInt = ratingInt;
	}
	public int getRatingInt() {
		return ratingInt;
	}
	public void setOpentime(String opentime) {
		this.opentime = opentime;
	}
	public String getOpentime() {
		return opentime;
	}
	public void setClosetime(String closetime) {
		this.closetime = closetime;
	}
	public String getClosetime() {
		return closetime;
	}
	
	public void setOpenDaily(String openDaily) {
		this.openDaily = openDaily;
	}
	public String getOpenDaily() {
		return openDaily;
	}

	public void setWifiAvailable(String wifiAvailable) {
		this.wifiAvailable = wifiAvailable;
	}

	public String getWifiAvailable() {
		return wifiAvailable;
	}

	public void setParkingAvailable(String parkingAvailable) {
		this.parkingAvailable = parkingAvailable;
	}

	public String getParkingAvailable() {
		return parkingAvailable;
	}


	public void setNonSmokingZone(String nonSmokingZone) {
		this.nonSmokingZone = nonSmokingZone;
	}

	public String getNonSmokingZone() {
		return nonSmokingZone;
	}

	public void setTotalRoom(int totalRoom) {
		this.totalRoom = totalRoom;
	}


	/*
        public void setTotalRoom(int totalRoom) {
            if(totalRoom < 0){
                totalRoom = 0;
            }
            this.totalRoom = totalRoom;
        }
        */
	public int getTotalRoom() {
		return totalRoom;
	}

	public int getTotalBar() {
		return totalBar;
	}

	public void setTotalBar(int totalBar) {
		this.totalBar = totalBar;
	}

	public int getTotalRestaurant() {
		return totalRestaurant;
	}

	public void setTotalRestaurant(int totalRestaurant) {
		this.totalRestaurant = totalRestaurant;
	}

	public int getTotalFloor() {
		return totalFloor;
	}

	public void setTotalFloor(int totalFloor) {
		this.totalFloor = totalFloor;
	}

	public String getTimetoairport() {
		return timetoairport;
	}

	public void setTimetoairport(String timetoairport) {
		this.timetoairport = timetoairport;
	}

	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}
	public String getProvinceId() {
		return provinceId;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPhone() {
		return phone;
	}
	

	
	public void setCheckInTime(String checkInTime) {
		this.checkInTime = checkInTime;
	}

	public String getCheckInTime() {
		return checkInTime;
	}

	public String getCheckOutTime() {
		return checkOutTime;
	}
	
	public void setCheckOutTime(String checkOutTime) {
		this.checkOutTime = checkOutTime;
	}

	public void setLogoCover(String logoCover) {
		this.logoCover = logoCover;
	}

	public String getLogoCover() {
		return logoCover;
	}

	public JSONObject toJson(){
		JSONObject json = new JSONObject();
		
		return json;
	}

}
