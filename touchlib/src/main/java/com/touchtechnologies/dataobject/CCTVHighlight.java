package com.touchtechnologies.dataobject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.json.JSONUtil;

public class CCTVHighlight implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 749548868907640918L;

	private String urlhighlight;
	private String id;
	private String highlighttime;
	private String url;

	private final String KEY_ID = "id_cctv_highlight";
	private final String KEY_HIGHLIGHT = "highlight_time";
	private final String KEY_SHARE_URL = "url";
	private final String KEY_HIGHLIGHT_URL = "highlight_url";
	public CCTVHighlight(JSONObject json) throws JSONException{
		if(!json.isNull(KEY_ID)){
			setId(JSONUtil.getString(json, KEY_ID));
		}
		
		if(!json.isNull(KEY_HIGHLIGHT)){
			setHighlightTime(JSONUtil.getString(json, KEY_HIGHLIGHT));
		}
		if(!json.isNull(KEY_HIGHLIGHT_URL)){
			setHighlightUrl(JSONUtil.getString(json, KEY_HIGHLIGHT_URL));
		}
		
		if(!json.isNull(KEY_SHARE_URL)){
			
			try{
				JSONArray jsonUrl = json.getJSONArray(KEY_SHARE_URL);
			
				  
				  for (int i = 0; i < jsonUrl.length(); i++) {
				    
				    JSONObject objcctv = jsonUrl.getJSONObject(i); 
						
				//"Report"
				    setShareCCTVHighlight(objcctv.getString("url"));
				  }
			}catch(Exception e){
				
			}
		}
		
		
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}
	public void setHighlightTime(String highlighttime) {
		this.highlighttime = highlighttime;
	}
	public String getHighlighttime() {
		return highlighttime;
	}
	
	public void setShareCCTVHighlight(String urlhighlight) {
		this.urlhighlight = urlhighlight;
	}
	public String getShareCCTVHighlight() {
		return urlhighlight;
	}
	
	public void setHighlightUrl(String url) {
		this.url = url;
	}
	public String getHighlightUrl() {
		return url;
	}
	
}
