package com.touchtechnologies.dataobject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.json.JSONUtil;

public class ROI implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 749548868907640918L;

	/**
	 * id_provider_information_tag_keyname
	 */
	private String id;
	/**
	 * provider_information_tag_keyname
	 */
	private String keyName;
	/**
	 * provider_information_tag_name
	 */
	private String name;
	
	/**
	 * ROI's category
	 */
	private List<Category> categories;
	private List<CCTV> cctvs;
	private String imageUrlLogo;
	private String imageUrlCover;
	private String imageUrlBanner;
	private String imageUrlCoverStatistic;
	private Double distance;
	private int viewCount;
	
	private final String KEY_ID = "id_provider_information_tag_keyname";
	private final String KEY_KEYNAME = "provider_information_tag_keyname";
	private final String KEY_TYPE = "provider_information_tag_type";
	private final String KEY_NAME = "provider_information_tag_name";
	private final String KEY_URL_LOGO = "logo_image";
	private final String KEY_URL_COVER = "cover_image";
	private final String KEY_URL_COVER_STAT = "cover_url";
	private final String KEY_URL_BANNER = "banner_image";
	private final String KEY_CATEGORIES = "categories";
	private final String KEY_CCTV = "cctvs";
	private final String KEY_VIEW = "total_view";
	
	public ROI(JSONObject json) throws JSONException{
		if(!json.isNull(KEY_ID)){
			setId(JSONUtil.getString(json, KEY_ID));
		}
		
		if(!json.isNull(KEY_KEYNAME)){
			setKeyName(JSONUtil.getString(json, KEY_KEYNAME));
		}
		
		if(!json.isNull(KEY_NAME)){
			setName(JSONUtil.getString(json, KEY_NAME));
		}
		
		if(!json.isNull(KEY_VIEW)){
			setViewCount(JSONUtil.getInt(json, KEY_VIEW));
		}
		
		if(!json.isNull(KEY_URL_COVER)){
			setImageUrlCover(JSONUtil.getString(json, KEY_URL_COVER));
		}
		
		if(!json.isNull(KEY_URL_COVER_STAT)){
			setImageUrlCoverStatistic(JSONUtil.getString(json, KEY_URL_COVER_STAT));
		}

		if(!json.isNull(KEY_URL_LOGO)){
			setImageUrlLogo(JSONUtil.getString(json, KEY_URL_LOGO));
		}
		if(!json.isNull(KEY_CATEGORIES)){
			JSONArray jCategories = json.getJSONArray(KEY_CATEGORIES);
	
			//TODO add "ALL" categpry filter
			
			Category catTmp = new Category(json);
			catTmp.setId("0");
			addCategory(catTmp);
			
			for(int c=0; c<jCategories.length(); c++){
				
				
				addCategory(new Category(jCategories.getJSONObject(c)));
			
//				addCategory(this.getCategoryAt(c));
			}
		}
		
		if(!json.isNull(KEY_CCTV)){
			JSONArray cctvs = json.getJSONArray(KEY_CCTV);
	
			//TODO add "ALL" categpry filter
			
			for(int i=0; i<cctvs.length(); i++){
				
				
				addCCTV(new CCTV(cctvs.getJSONObject(i)));
			
//				addCategory(this.getCategoryAt(c));
			}
		}
	}
	
	public void setImageUrlCoverStatistic(String imageUrlCoverStatistic) {
		this.imageUrlCoverStatistic = imageUrlCoverStatistic;
	}
	
	public String getImageUrlCoverStatistic() {
		return imageUrlCoverStatistic;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}
	
	public String getKeyName() {
		return keyName;
	}
	
	/**
	 * 
	 * @param categories
	 */
	public void setCategories(List<Category> categories){
		this.categories = categories;
	}
	
	public void addCategory(Category category){
		if(categories == null){
			this.categories = new ArrayList<Category>();
		}
		
		categories.add(category);
	}
	
	public Category getCategoryAt(int position){
		return categories.get(position);
	}
	
	/**
	 * Number of categories
	 * @return
	 */
	public int sizeCategory(){
		return categories == null?0:categories.size();
	}
	
	
	
	public void setCCTV(List<CCTV> cctv){
		this.cctvs = cctv;
	}
	
	public void addCCTV(CCTV cctv){
		if(cctvs == null){
			this.cctvs = new ArrayList<CCTV>();
		}
		
		cctvs.add(cctv);
	}
	
	public CCTV getCCTVAt(int position){
		return cctvs.get(position);
	}
	
	public List<CCTV> getCCTVs(){
		return cctvs;
	}
	
	/**
	 * Number of categories
	 * @return
	 */
	public int sizeCCTV(){
		return cctvs == null?0:cctvs.size();
	}
	

	
	public void setImageUrlBanner(String imageUrlBanner) {
		this.imageUrlBanner = imageUrlBanner;
	}
	
	public String getImageUrlBanner() {
		return imageUrlBanner;
	}
	
	public void setImageUrlCover(String imageUrlCover) {
		this.imageUrlCover = imageUrlCover;
	}
	
	public String getImageUrlCover() {
		return imageUrlCover;
	}
	
	public void setImageUrlLogo(String imageUrlLogo) {
		this.imageUrlLogo = imageUrlLogo;
	}
	
	public String getImageUrlLogo() {
		return imageUrlLogo;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name == null?"Unknown":name;
	}
	
	/**
	 * Get total number of all cctv view counts.
	 * @return
	 */
	public int getCCTVViewCount(){
		int sum = 0;
		if(cctvs != null){
			for(CCTV cctv: cctvs){
				sum += cctv.getView();
			}
		}
		return sum;
	}
	
	/**
	 * @TODO
	 * @return
	 */
	public JSONObject toJsonObject() throws JSONException{
		JSONObject json = new JSONObject();
		
		if(getId() != null){
			json.put(KEY_ID, getId());
		}
		
		if(getKeyName() != null){
			json.put(KEY_KEYNAME, getKeyName());
		}
		
		if(getName() != null){
			json.put(KEY_NAME, getName());
		}
		
		if(getImageUrlCover() != null){
			json.put(KEY_URL_COVER, getImageUrlCover());
		}
		
		if(getImageUrlCoverStatistic() != null){
			json.put(KEY_URL_COVER_STAT, getImageUrlCoverStatistic());
		}

		if(getImageUrlLogo() != null){
			json.put(KEY_URL_LOGO, getImageUrlLogo());
		}
		if(getViewCount() != 0){
			json.put(KEY_VIEW,getViewCount());
		}
		
		//category
		if(sizeCategory() > 0){
			JSONArray jCatArray = new JSONArray();
			for(Category cat: categories){
				jCatArray.put(cat.toJson());
			}
			
			json.put(KEY_CATEGORIES, jCatArray);
		}
		
		
		if(sizeCCTV() > 0){
			JSONArray jCCTVs = new JSONArray();
			for(CCTV cctv: cctvs){
				jCCTVs.put(cctv.toJson());
			}
			
			json.put(KEY_CCTV, jCCTVs);
		}
		
		return json;
	}



	public Double getDistance() {
		return distance;
	}



	public void setDistance() {
		this.distance = getCCTVAt(0).getDistance();
	}



	public int getViewCount() {
		return viewCount;
	}



	public void setViewCount(int view) {
		this.viewCount = view;
	}
	
	public List<CCTV> getcctvonline(){
		List<CCTV> cctvs = getCCTVs();
		List<CCTV> online = new ArrayList<CCTV>();
		
		if(!cctvs.isEmpty()){
			for (int i = 0; i < sizeCCTV(); i++) {
				if(getCCTVAt(i).getOnlineStatus()==1){
					online.add(getCCTVAt(i));
				}
			}
		}
			
		return online;
	}
	
	
}
