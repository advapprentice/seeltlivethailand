package com.touchtechnologies.dataobject;

import android.support.annotation.NonNull;
import android.util.Log;

import com.touchtechnologies.json.JSONUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Jannrong on 13/7/2559.
 *             {
 "providerInformation": {
 "providerId": "int require",
 "referenceId": "int require if providerId is unset",
 "providerTypeKeyname": "string require if providerId is unset"
 },
 "info": {
 "infoTypeKeyname": "",
 "titleEn": "",
 "titleTh": "",
 "queue": "0",
 "infoContents": [
 {
 "contentEn": "",
 "contentTh": "",
 "queue": "0"
 }
 ]
 },
 "user": {
 "accessToken": "string require"
 }
 */
public class ProviderContent implements Serializable{
    private String titel,titelth;
    private String keyname_value = "hightlight_content";

    public ProviderContent() {

    }

    public  enum keyname  {highlgiht,about_content,general}
    private String id;
    private ArrayList<Content> contents = new ArrayList<>();

    public ProviderContent(@NonNull keyname keyname,String Providertype) {
        switch (keyname) {
            case highlgiht:
             keyname_value ="highlight_content";
                break;
            case about_content:
                keyname_value ="about_"+Providertype+"_content";
                break;
            case general:
                keyname_value ="general_content";
                break;
            default:
                keyname_value ="highlight_content";
                break;
        }
    }

    public ProviderContent(@NonNull keyname keyname,String Providertype,@NonNull JSONObject jsonObject){
    if(jsonObject != null){
        switch (keyname) {
            case highlgiht:
                keyname_value ="highlight_content";
                break;
            case about_content:
                keyname_value ="about_"+Providertype+"_content";
                break;
            case general:
                keyname_value ="general_content";
                break;
            default:
                keyname_value ="highlight_content";
                break;
        }
        // TODO
        setId(jsonObject.isNull("info_id")? "":JSONUtil.getString(jsonObject,"info_id"));
        setTitel(jsonObject.isNull("title_en")? "":JSONUtil.getString(jsonObject,"title_en"));
        setTitelth(jsonObject.isNull("title_th")? "":JSONUtil.getString(jsonObject,"title_th"));
        //setContent(new Content(jsonObject.getJSONArray()));
        try {
            JSONArray jsonArray = jsonObject.getJSONArray("info_contents");
            if(jsonArray.length() >= 0) {
                for (int i = 0; jsonArray.length() > i; i++) {
                    addContent(new Content(jsonArray.getJSONObject(i)));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }else {
        Log.e(ProviderContent.class.getSimpleName(), "ProviderContent: Json is null");
    }


    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void  addContent(Content content){
        if (contents == null){
            contents = new ArrayList<>();
            contents.add(content);
        }else {
            contents.add(content);
        }

    }

    public String getTitelth() {
        return titelth;
    }

    public void setTitelth(String titelth) {
        this.titelth = titelth;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public Content getContent(){
        return contents.get(0);
    }

    public String toString(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("infoTypeKeyname",keyname_value);
            jsonObject.put("titleTh",getTitelth());
            jsonObject.put("titleEn",getTitel());
            jsonObject.put("infoContents",new JSONArray().put(new JSONObject()
                    .put("contentTh",getContent().getDescriptionth()).put("contentEn",getContent().getDescription())));
        }catch (JSONException e){
            e.printStackTrace();
        }
        return jsonObject.toString();
    }



    public JSONObject toJson(){
        JSONObject jsonObject = new JSONObject();
        try {
            if(getId()!= null){
                jsonObject.put("infoId",getId());
            }
            jsonObject.put("infoTypeKeyname",keyname_value);
            jsonObject.put("titleTh",getTitelth());
            jsonObject.put("titleEn",getTitel());
            jsonObject.put("infoContents",new JSONArray().put(new JSONObject()
                    .put("contentTh",getContent().getDescriptionth()).put("contentEn",getContent().getDescription())));
        }catch (JSONException e){
            e.printStackTrace();
        }
        return jsonObject;
    }

  public   class Content implements Serializable{
        private String id;
        private String description,descriptionth;

        public Content(){


        }

      public Content(JSONObject jsonObject) {
            setDescription(jsonObject.isNull("content_en")? "":JSONUtil.getString(jsonObject,"content_en"));
            setDescriptionth(jsonObject.isNull("content_th")? "":JSONUtil.getString(jsonObject,"content_th"));
        }

        public String getDescriptionth() {
            return descriptionth;
        }

        public void setDescriptionth(String descriptionth) {
            this.descriptionth = descriptionth;
        }



        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

    }

}
