package com.touchtechnologies.dataobject;

import java.io.Serializable;
import java.util.ArrayList;

import android.provider.ContactsContract.CommonDataKinds.Im;

public class ImageGallery implements Serializable {

	private static final long serialVersionUID = 0x644c0537048d5c4cL;

	private ArrayList<Image> arrImages;

	public ImageGallery() {
	}

	public int size() {
		return getImageCount();
	}

	public int getImageCount() {
		return arrImages == null ? 0 : arrImages.size();
	}

	public ArrayList<Image> getImages() {
		return arrImages;
	}

	public Image getImageAt(int index) {
		return arrImages.get(index);
	}

	public void add(Image image) {
		if (arrImages == null) {

			arrImages = new ArrayList<Image>();
		}

		arrImages.add(image);
	}

	public void add(String uri) {
		add(new Image(uri));
	}

	public void add(String uri, String thumbUri) {
		add(new Image(uri, thumbUri));
	}
	public void addall(ArrayList<Image> arrayList){
		for(Image image: arrayList){
			add(image);
		}
	}
}
