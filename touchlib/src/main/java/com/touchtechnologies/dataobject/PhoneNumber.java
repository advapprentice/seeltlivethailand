package com.touchtechnologies.dataobject;

import java.io.Serializable;

public class PhoneNumber implements Serializable{

	
	private static final long serialVersionUID = 0x7b3d8ff7d76815feL;
	private int type;
	private String number;
	private String alias;
	
	public PhoneNumber(int type, String number) {
		this.type = type;
		this.number = number;
	}
	
	
	public int getType() {
		return type;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public String getAlias() {
		return alias;
	}
	
	public void setAlias(String alias) {
		this.alias = alias;
	}
}
