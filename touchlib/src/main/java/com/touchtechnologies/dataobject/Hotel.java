package com.touchtechnologies.dataobject;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.json.JSONUtil;

public class Hotel extends Hospitality{
	
	private static final long serialVersionUID = 0x86b5d6850d80b7eeL;
	public static final String PROVIDER_TYPE_KEYNAME = "hotel";
	

	private ArrayList<RoomType> arrRoomTypes;
	private ArrayList<Facility> arrFacilities;
	private ArrayList<Image> arrImageGallery;
	private ArrayList<ProviderContent> highlightcontentArrayList;

	private String policy;

	private String coverImage;
	
	private double price;
	private int Bookable;
	
	
	Hotel(){		
	}
	
	public Hotel(JSONObject json) throws JSONException{
		super(json);
		
		
		setCoverImage(JSONUtil.getString(json, "cover_image"));
		setBookable(JSONUtil.getInt(json,"is_bookable"));
	
		//TODO
		//hotel_policy_th
		//hotel_policy_en
		
		//TODO
		//facility
		
		//room_type
		String roomTypeKey = !json.isNull("room_type")?"room_type":(!json.isNull("hotel_room_type")?"hotel_room_type":null);
		if(roomTypeKey != null){
			JSONArray roomArray = json.getJSONArray(roomTypeKey);
			
			for(int i=0; i<roomArray.length(); i++){
				RoomType roomType = new RoomType();
				addRoomType(roomType);
				
				JSONObject roomJson = roomArray.getJSONObject(i);
				//last_modified
				
				//hotel_room_type_avg_price
				roomType.setPrice(JSONUtil.getInt(roomJson, "hotel_room_type_avg_price"));
				
				//hotel_room_type_current_price
			
				//hotel_room_type_description_th
				//hotel_room_type_description_en
				roomType.setDescription(JSONUtil.getString(roomJson, "hotel_room_type_description_en"));
				
				//hotel_room_type_name_th
				//hotel_room_type_name_en
				roomType.setName(JSONUtil.getString(roomJson,"hotel_room_type_name_en"));
								
				//id_hotel_room_type
				//logo_image
				Image image = new Image(JSONUtil.getString(roomJson,"cover_image"), JSONUtil.getString(roomJson,"logo_image"));
				roomType.setImage(image);
				
				//room_type_facility
				if(!roomJson.isNull("room_type_facility")){
					JSONArray facilityArray = roomJson.getJSONArray("room_type_facility");
					for(int f=0; f<facilityArray.length(); f++){
						roomType.addFacility(new Facility(facilityArray.getJSONObject(f)));
					}
				}
				
				//room_type_gallery
				if(!roomJson.isNull("room_type_gallery")){
					JSONArray imageArray = roomJson.getJSONArray("room_type_gallery");
					for(int img=0; img<imageArray.length(); img++){
						JSONObject tmp = imageArray.getJSONObject(img);
						
						Image imageTmp = new Image(JSONUtil.getString(tmp, "cover_image"), JSONUtil.getString(tmp, "logo_image"));
						roomType.addImageToGallery(imageTmp);
					}
				}
						
			}
		}		
		
		
		//set image gallery restaurant_gallery
		String galleryKey = !json.isNull("hotel_gallery")?"hotel_gallery":(!json.isNull("restaurant_gallery")?"restaurant_gallery":null);
		if(!json.isNull(galleryKey)){
			JSONArray galleryArray = json.getJSONArray(galleryKey);
			for(int i =0;i<galleryArray.length(); i++){
				
				Image imageTmp = new Image();
				addImageToGallery(imageTmp);
				
				JSONObject roomJson = galleryArray.getJSONObject(i);
				//last_modified
				
				//hotel_room_type_avg_price
				imageTmp.setThumbnail(JSONUtil.getString(roomJson, "thumbnail"));
				
			}
			
		}
		if(!json.isNull("hotel_policy_en")){
			setPolicy(JSONUtil.getString(json,"hotel_policy_en"));
		}
	}
	
	public ArrayList<RoomType> getRoomType() {
		return arrRoomTypes;
	} 
	
	
	public ArrayList<Image> getGallery() {
		return arrImageGallery;
	} 
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public double getPrice() {
		return price;
	} 

	
	public void setTotalRoom(String totalRoom){
		try{
			setTotalRoom(Integer.parseInt(totalRoom));
		}catch(Exception e){
			
		}
	}

	public void setTotalbar(String totalbar){
		try{
			setTotalBar(Integer.parseInt(totalbar));
		}catch (Exception e){

		}
	}

	public void setTotalfloor(String totalfloor){
		try{
			setTotalFloor(Integer.parseInt(totalfloor));
		}catch (Exception e){

		}
	}

	public void setTotalRestaurant(String totalRestaurant){
		try{
			setTotalRestaurant(Integer.parseInt(totalRestaurant));
		}catch (Exception e){

		}
	}
	

	public void addFacility(Facility facility){
		if(arrFacilities == null){
			arrFacilities = new ArrayList<Facility>();
		}
		
		arrFacilities.add(facility);
	}
	
	public int getFacilitySize(){
		return arrFacilities == null?0:arrFacilities.size();
	}
	
	public Facility getFacilityAt(int index){
		return arrFacilities == null?null:arrFacilities.get(index);
	}
	
	public ArrayList<Facility> getFacilites(){
		return arrFacilities;
	}
	
	public int getRoomTypeCount(){
		return arrRoomTypes == null?0:arrRoomTypes.size();
	}
	
	public void setPolicy(String policy) {
		this.policy = policy;
	}
	
	public String getPolicy() {
		return policy;
	}
	
	public void setCoverImage(String coverImage) {
		this.coverImage = coverImage;
	}
	public String getCoverImage() {
		return coverImage;
	}

	public void removeAllRoomType(){
		if(arrRoomTypes != null){
			arrRoomTypes.clear();
		}
	}
	
	public void addRoomType(RoomType roomType){
		if(arrRoomTypes == null){
			arrRoomTypes = new ArrayList<Hotel.RoomType>();
		}
		
		arrRoomTypes.add(roomType);
	}

	public void addHlightcontent(ProviderContent highlightcontent){
		if(highlightcontentArrayList == null){
			highlightcontentArrayList = new ArrayList<>();
		}
		highlightcontentArrayList.add(highlightcontent);
	}

	public int getHighlightcontentscount(){
		return highlightcontentArrayList == null?0:highlightcontentArrayList.size();
	}

	public ArrayList<ProviderContent> getHighlightcontentArrayList (){
		return highlightcontentArrayList;
	}

	public ProviderContent getHighlightcontent(int position){
		return highlightcontentArrayList.get(position);
	}

	public RoomType getRoomTypeAt(int index){
		return arrRoomTypes.get(index);
	}

	public int getBookable() {
		return Bookable;
	}

	public void setBookable(int bookable) {
		Bookable = bookable;
	}

	/**
	 *  ----------------------------------Room Type -----------------------------
	 * @author MEe
	 *
	 */
	public static final class RoomType extends Hotel{
		
		private static final long serialVersionUID = 0x8d6372f1b7d91233L;
		private int maxOccupancy = 2;
		
		public RoomType() {
		}
		
		public RoomType(JSONObject json){
			setId(JSONUtil.getString(json, "room_type_id"));
			setName(JSONUtil.getString(json, "room_type_name_en"));
			setDescription(JSONUtil.getString(json, "room_type_description_en"));
			setPrice(JSONUtil.getInt(json, "room_type_current_price"));
			setLogo(JSONUtil.getString(json, "logo_image"));
			setCoverImage(JSONUtil.getString(json, "cover_image"));
			setTotalRoom(JSONUtil.getString(json, "quantity"));
			setMaxOccupancy(JSONUtil.getInt(json, "maximum_person"));
		}
		
		public void setMaxOccupancy(int maxOccupancy) {
			this.maxOccupancy = maxOccupancy;
		}
		
		public int getMaxOccupancy() {
			return maxOccupancy;
		}
	}
}


