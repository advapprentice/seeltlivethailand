package com.touchtechnologies.dataobject;

import java.io.Serializable;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.json.JSONUtil;

public class Attraction extends Hospitality{
	private static final long serialVersionUID = 0xff06f9d32f84abaL;
	public static final String PROVIDER_TYPE_KEYNAME = "attraction";
	private String attractionNavigate;
	private String weekdayOpentime;
	private String weekdayClosetime;
	public Attraction(JSONObject json) throws JSONException {
		super(json);
		
	// restaurant gallery	
		String galleryKey ="attraction_gallery";
		if(!json.isNull(galleryKey)){
			JSONArray galleryArray = json.getJSONArray(galleryKey);
			for(int i =0;i<galleryArray.length(); i++){
				
				Image imageTmp = new Image();
				addImageToGallery(imageTmp);
				
				JSONObject roomJson = galleryArray.getJSONObject(i);
				//last_modified
				
				//hotel_room_type_avg_price
				imageTmp.setThumbnail(JSONUtil.getString(roomJson, "thumbnail"));
				
			}
			
		}
		if(!json.isNull("navigate_en")){
			setAttractionNavigate(JSONUtil.getString(json, "navigate_en"));
		}
		if(!json.isNull("weekday_opentime")){
			setWeekdayOpentime(JSONUtil.getString(json, "weekday_opentime"));
		}
		if(!json.isNull("weekday_closetime")){
			setWeekdayClosetime(JSONUtil.getString(json, "weekday_closetime"));
		}
		
	}
	
	public void setWeekdayOpentime(String weekdayOpentime) {
		this.weekdayOpentime = weekdayOpentime;
	}
	public String getWeekdayOpentime() {
		return weekdayOpentime;
	}
	public void setWeekdayClosetime(String weekdayClosetime) {
		this.weekdayClosetime = weekdayClosetime;
	}
	public String getWeekdayClosetime() {
		return weekdayClosetime;
	}
	
	
	public void setAttractionNavigate(String attractionNavigate) {
		this.attractionNavigate = attractionNavigate;
	}
	public String getAttractionNavigate() {
		return attractionNavigate;
	}
	
	
	
	private ArrayList<MenuCategory> arrMenus;
	
	public int getMenuCategoryCount(){
		return arrMenus == null?0:arrMenus.size();
	}
	
	public ArrayList<MenuCategory> getMenuCategories(){
		return arrMenus;
	}
	
	public void addMenus(MenuCategory menus){
		if(arrMenus == null){
			arrMenus = new ArrayList<Attraction.MenuCategory>();
		}
		
		arrMenus.add(menus);
	}
	
	
	/**
	 * ----------------------------- Menu Category------------------------------------
	 * @author MEe
	 *
	 */
	public static final class MenuCategory implements Serializable{		
		private static final long serialVersionUID = 0x21585c69fc26bc9fL;
		
		ArrayList<Menu> arrMenus;
		
		String name;
		public MenuCategory(String name) {
			this.name = name;
		}
		
		
		/**
		 * Get number of menu in this category
		 * @return
		 */
		public int getMenuCount(){
			return arrMenus == null?0:arrMenus.size();
		}
		
		public ArrayList<Menu> getMenus(){
			return arrMenus;
		}
		
		public void add(Menu menu){
			if(arrMenus == null){
				arrMenus = new ArrayList<Attraction.Menu>();				
			}
			
			arrMenus.add(menu);
		}
	}
	
	
	/**
	 * ----------------------------- Menu ------------------------------------
	 * @author MEe
	 *
	 */
	public static final class Menu implements Serializable{		
		private static final long serialVersionUID = 0x62f3d07907255fb5L;
	
		public enum Hour{
			Breakfast, Lunch, Dinner
		}
		
		public enum Ethic{
			Italian, French, Japanese, Chinese, American, Thai, Indian		
		}
		
		String name;
		double price;
		Image image;
		ImageGallery imageGallery;
		String description;
				
		/**
		 * Italian, French, Japanese, Chinese, American, Thai, and Indian
		 */
		Ethic ethnic;
		Hour hour;
		
		
		public Menu(String name) {
			this.name = name;
		}
		
		public void setPrice(double price) {
			this.price = price;
		}
		
		public double getPrice() {
			return price;
		}
		
		public String getName() {
			return name;
		}
		
		public Hour getHour() {
			return hour;
		}
		
		public void setHour(Hour hour) {
			this.hour = hour;
		}
		
		public Ethic getEthnic() {
			return ethnic;
		}
		
		public void setEthnic(Ethic ethnic) {
			this.ethnic = ethnic;
		}
		
		
		public void setImage(Image image) {
			this.image = image;
		}
		
		public Image getImage() {
			return image;
		}
		
		public void setImageGallery(ImageGallery imageGallery) {
			this.imageGallery = imageGallery;
		}
		
		public ImageGallery getImageGallery() {
			return imageGallery;
		}
		
		/**
		 * Get number of image in the gallery
		 * @return
		 */
		public int getImageGalleryCount(){
			return imageGallery == null?0:imageGallery.getImageCount();
		}
		
		public void setDescription(String description) {
			this.description = description;
		}
		
		public String getDescription() {
			return description;
		}
	}
}
