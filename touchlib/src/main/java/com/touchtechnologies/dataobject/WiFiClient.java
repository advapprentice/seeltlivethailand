package com.touchtechnologies.dataobject;


public class WiFiClient{

	private WiFiAccount account;
	private String ip;
	private String mac;
	
	public void setAccount(WiFiAccount account) {
		this.account = account;
	}
	
	public WiFiAccount getAccount() {
		return account;
	}
	
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public String getIp() {
		return ip;
	}
	
	public void setMac(String mac) {
		this.mac = mac;
	}
	
	public String getMac() {
		return mac;
	}
}
