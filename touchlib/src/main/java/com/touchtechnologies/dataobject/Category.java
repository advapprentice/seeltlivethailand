package com.touchtechnologies.dataobject;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.json.JSONUtil;

public class Category implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4214103737327482642L;
	
	private String id;
	private String keyname;
	private String categoryname;
	private String tagkeyname;
	

	private final String KEY_ID = "id_category";
	private final String KEY_KEYNAME = "category_keyname";
	private final String KEY_CATEGORYNAME = "category_name_en";
	private final String KEY_TAGKEYNAME = "id_provider_information_tag_keyname";
	
	
	
	public Category(JSONObject json) throws JSONException{
	
			if(!json.isNull(KEY_ID)){
				setId(JSONUtil.getString(json, KEY_ID));
			}
			
			if(!json.isNull(KEY_KEYNAME)){
				setKeyname(JSONUtil.getString(json, KEY_KEYNAME));
			}
			
			if(!json.isNull(KEY_CATEGORYNAME)){
				setCategoryname(JSONUtil.getString(json, KEY_CATEGORYNAME));
			}
		
		}	
	
	
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setKeyname(String keyname) {
		this.keyname = keyname;
	}
	public String getKeyname() {
		return keyname;
	}
	
	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}
	
	public String getCategoryname() {
		return categoryname;
	}
	
	public JSONObject toJson() throws JSONException{
		JSONObject json = new JSONObject();
		
		if(getId() != null){
			json.put(KEY_ID, getId());
		}
		
		if(getKeyname() != null){
			json.put(KEY_KEYNAME, getKeyname());
		}
		
		if(getCategoryname() != null){
			json.put(KEY_CATEGORYNAME, getCategoryname());
		}
		
		return json;
	}
	
	@Override
	public String toString() {
		return "id:" +id +" keyname=" + keyname + ", category keyname=" + categoryname;
	}
}
