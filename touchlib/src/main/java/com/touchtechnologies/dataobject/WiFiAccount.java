package com.touchtechnologies.dataobject;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class WiFiAccount implements Serializable{
	private static final long serialVersionUID = 461673321550393907L;
	private String username;
	private String password;
	
	public WiFiAccount() {
	}
	
	public WiFiAccount(JSONObject json) throws JSONException{
		if(!json.isNull("username")){
			setUsername(json.getString("username"));
		}
		
		if(!json.isNull("password")){
			setPassword(json.getString("password"));
		}
		
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPassword() {
		return password;
	}
	
	public JSONObject toJson(){
		JSONObject json = new JSONObject();
		
		try{
			json.put("username", username);
			json.put("password", password);
			
		}catch(Exception e){
			Log.e(getClass().getSimpleName(), e.getMessage());
		}
		
		return json;
	}
	
}
