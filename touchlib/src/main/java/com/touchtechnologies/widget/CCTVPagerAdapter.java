package com.touchtechnologies.widget;

import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.Config;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.graphics.BitmapUtil;
import com.touchtechnologies.io.FileUtil;
import com.touchtechnologies.lib.R;

public class CCTVPagerAdapter extends PagerAdapter {
	/** LrcCache bitmap cache */
	private static LruCache<String, Bitmap> mMemoryCache;

	protected Context context;
	/**
	 * CCTVs in this adapter
	 */
	protected List<CCTV> listCCTVs;

	public CCTVPagerAdapter(Context context) {
		this.context = context;

		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		final int cacheSize = maxMemory / 2;

		if(mMemoryCache == null){
			mMemoryCache = new LruCache<String, Bitmap>(cacheSize);
		}
	}

	/* ============ MEMORY CACHE ============ */
	public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
		mMemoryCache.put(key, bitmap);
	}

	public Bitmap getBitmapFromMemCache(String key) {
		return mMemoryCache.get(key);
	}

	/* ============ END OF MEMORY ============ */
	public void setData(List<CCTV> listCCTVs) {
		this.listCCTVs = listCCTVs;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return listCCTVs == null ? 0 : listCCTVs.size();
	}

	public CCTV getItem(int position) {
		return listCCTVs.get(position);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		View view = LayoutInflater.from(context).inflate(R.layout.item_cctv, container, false);		
		
		ImageView imageView = (ImageView)view.findViewById(R.id.imageView);
		
		loadBitmap(listCCTVs.get(position).getUrl(), imageView);
		container.setTag(view);
		
		container.addView(view, 0);
		return view;
	}

	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

	@Override
	public void destroyItem(View collection, int position, Object view) {
		((ViewPager) collection).removeView((View) view);
	}

	public void loadBitmap(String resId, ImageView imageView) {
		try{
			// set bitmap using cache
			Bitmap bitmap = mMemoryCache.get(resId);
			if (bitmap != null) {
				imageView.setImageBitmap(bitmap);
			}
	
			// load bitmap from server
//			if (cancelPotentialWork(resId, imageView)) {
				BitmapWorkerTask task = new BitmapWorkerTask(imageView);
//				final AsyncDrawable asyncDrawable = new AsyncDrawable(
//						context.getResources(), null, task);
//				imageView.setImageDrawable(asyncDrawable);
				task.execute(resId);
//			}
		}catch(Exception e){
			Log.e("LoadBitmap", e.getMessage(), e);
		}
	}

//	private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
//		if (imageView != null) {
//			final Drawable drawable = imageView.getDrawable();
//			if (drawable instanceof AsyncDrawable) {
//				final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
//				return asyncDrawable.getBitmapWorkerTask();
//			}
//		}
//		return null;
//	}

//	public static boolean cancelPotentialWork(String data, ImageView imageView) {
//		final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);
//
//		if (bitmapWorkerTask != null) {
//			final String bitmapData = bitmapWorkerTask.data;
//			if (bitmapData != null && !(bitmapData.equals(data))) {
//				// Cancel previous task
//				bitmapWorkerTask.cancel(true);
//			} else {
//				// The same work is already in progress
//				return false;
//			}
//		}
//		// No task associated with the ImageView, or an existing task was
//		// cancelled
//		return true;
//	}

	@Override
	protected void finalize() throws Throwable {
		if (mMemoryCache != null) {
			mMemoryCache.evictAll();
		}
		super.finalize();
	}

	/**
	 * Download bitmap task
	 * 
	 * @author Touch
	 */
	class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
		private final WeakReference<ImageView> imageViewReference;
		private final Timer timer; 
		private String data;

		public BitmapWorkerTask(ImageView imageView) {
			// Use a WeakReference to ensure the ImageView can be garbage
			// collected
			imageViewReference = new WeakReference<ImageView>(imageView);
			timer = new Timer();
		}

		// Decode image in background.
		@Override
		protected Bitmap doInBackground(String... params) {
			data = params[0];
			Bitmap bmp = null;

			try {
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 2;
				options.inPreferredConfig = Config.RGB_565;
				
				bmp = BitmapUtil.getBitmap(context, new URL(data), options);
//				bmp = BitmapUtil.

				// caching
				mMemoryCache.put(data, bmp);

			} catch (Exception e) {
				Log.e("CCTVPager", "Load cctv image e:" + e.getMessage(), e);
			}

			return bmp;
		}

		// Once complete, see if ImageView is still around and set bitmap.
		@Override
		protected void onPostExecute(Bitmap bitmap) {
			if (imageViewReference != null && bitmap != null) {
				final ImageView imageView = imageViewReference.get();
//				final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);
				if (imageView != null) {
					imageView.setImageBitmap(bitmap);
					notifyDataSetChanged();
					
					loadBitmap(data, imageView);
					Log.e("CCTVPager", "update BITMAP!");
				}
				
			}else{
				Log.e("CCTVPager", "imageViewReference is NULL");
			}
		}
	}
}
