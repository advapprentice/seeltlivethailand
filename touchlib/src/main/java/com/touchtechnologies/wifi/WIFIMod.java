package com.touchtechnologies.wifi;

import com.touchtechnologies.log.Log;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;


public class WIFIMod {
	public static final int REQUEST_WIFI_SETTING = 4554;

	/**
	 * Connected SSID or empty string, never null
	 * @param ctx
	 * @param nameonly
	 * @return
	 */
	private static String getConnectedImpl(Context ctx, boolean nameonly){
		String info = "";
		
		WifiManager wifiMgr = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
		if(wifiMgr != null){
			WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
			if(wifiInfo != null){
				StringBuilder builder = new StringBuilder();
				if(wifiInfo.getSSID() != null){
					builder.append(wifiInfo.getSSID());
				}
				
				if(!nameonly){
					if(wifiInfo.getBSSID() != null){
						builder.append(" ");
						builder.append(wifiInfo.getBSSID());
						builder.append("\r\n");
						builder.append(wifiInfo.getRssi());					
					}
				}
				
				info = builder.length() > 0?builder.toString():"";
			}
		}
		
		info = info.replaceAll("\"", "");
		
		return info;
	}
	
	public static String getWiFiMACAddress(Context context){
		String info = null;
		
		WifiManager wifiMgr = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		if(wifiMgr != null){
			WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
			if(wifiInfo != null){
				StringBuilder builder = new StringBuilder();
				if(wifiInfo.getBSSID() != null){
					builder.append(wifiInfo.getBSSID());
				}
				
				info = builder.length() > 0?builder.toString():null;
			}
		}
		
		return info;
	}
	
	public static String getMACAddress(Context ctx) {
		String info = null;
		
		WifiManager wifiMgr = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
		if(wifiMgr != null){
			WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
			if(wifiInfo != null){
				info = wifiInfo.getMacAddress();
			}
		}
		
		return info;
	}
	
	
	public static String getConnectedSSID(Context ctx) {
		return getConnectedImpl(ctx, true);
	}
	
	public static String getConnectedWiFiInfo(Context ctx) {
		return getConnectedImpl(ctx, false);
	}
	
	
	public static boolean isNetworkConnected(Context ctx) {
		ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;

	}
	
	public static boolean isConnectedToSSIDs(Context context, String[] ssids){
		String ssid = getConnectedSSID(context).toLowerCase();
		for(String tmp: ssids){
			tmp = tmp.toLowerCase();
			if(ssid.equals(tmp)){
				return true;
			}
		}
		
		return false;
	}
	
	public static String getGWAddress(Context context) {
		String ip = getLocalAddress(context);
		if(ip != null){
			ip = ip.substring(0, ip.lastIndexOf('.')) + ".1";
		}
		
		return ip;
	}

	/**
	 * Get IP address assigned to the device's WiFi interface
	 * @param context
	 * @return ip address
	 */
	public static String getLocalAddress(Context context) {
		String ip = null;
		try {
			WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
			if(wifiManager != null){
				WifiInfo wifiInfo = wifiManager.getConnectionInfo();
				int ipAddress = wifiInfo.getIpAddress();

				ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff),
						(ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff),
						(ipAddress >> 24 & 0xff));
			}else{
				ip = "0.0.0.0";
			}
		} catch (Exception ex) {
			Log.log("getLocalAddress", ex);
		}

		return ip;
	}	
}
