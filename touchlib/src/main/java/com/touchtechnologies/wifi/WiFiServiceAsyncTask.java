package com.touchtechnologies.wifi;

import android.os.AsyncTask;

import com.touchtechnologies.command.wifi.WiFiCommand;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;

public class WiFiServiceAsyncTask extends AsyncTask<WiFiCommand, Void, ServiceResponse>{
	private String url;
	
	public WiFiServiceAsyncTask(String url) {
		this.url = url;
	}
	
	@Override
	protected ServiceResponse doInBackground(WiFiCommand... command) {
		
		ServiceConnector sc = new ServiceConnector(url);
		ServiceResponse response = sc.doAsynPost(command[0], true);
		
		return response;
	}

	
}
