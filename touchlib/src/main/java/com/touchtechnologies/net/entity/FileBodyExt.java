package com.touchtechnologies.net.entity;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.content.FileBody;

/**
 * Extending FileBody class and has a capability of a number of byte write listener. 
 * @author MEe
 *
 */
public class FileBodyExt extends FileBody{
	private int totalWrite;
	private WriterListener listener;
	private long length;


	public FileBodyExt(File file, ContentType contentType, String filename) {
		super(file, contentType, filename);
		
		length = file.length();
	}
	
	@Override
	public void writeTo(OutputStream outstream) throws IOException {
		InputStream fis = getInputStream();
		
		byte[] fileBufferred = new byte[1024 * 8];
		int count;
		while( (count = fis.read(fileBufferred)) != -1){
			outstream.write(fileBufferred, 0, count);
			outstream.flush();
			
			totalWrite += count;
			if(listener != null){
				listener.byteWrite(count, totalWrite);				
			}
		} 
		
		fis.close();
	}
	
	public void setListener(WriterListener listener) {
		this.listener = listener;
	}
	
	public long getLength(){
		return length;
	}
}
