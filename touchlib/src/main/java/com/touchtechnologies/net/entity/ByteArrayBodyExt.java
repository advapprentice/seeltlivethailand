package com.touchtechnologies.net.entity;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.http.entity.mime.content.ByteArrayBody;

/**
 * Extending ByteArrayBody class and has a capability of a number of byte write listener. 
 * @author MEe
 *
 */
public class ByteArrayBodyExt extends ByteArrayBody{
	private int totalWrite;
	private WriterListener listener;
	
	public ByteArrayBodyExt(byte[] b, String filename) {
		super(b, filename);
		
		totalWrite = b.length;
	}
	
	@Override
	public void writeTo(OutputStream out) throws IOException {
		super.writeTo(out);
		if(listener != null){
			listener.byteWrite(totalWrite, totalWrite);
		}
	}
	
	public int getLength() {
		return totalWrite;
	}
	
	public void setListener(WriterListener listener) {
		this.listener = listener;
	}
}
