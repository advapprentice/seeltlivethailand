package com.touchtechnologies.net;

import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.command.Command;


public class ServiceResponse {
	public static final int SUCCESS = Command.SUCCESS;	
	
	private int code;
	private String message;
	private Object content;
	
	public void setCode(int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
	
	public void setMessage(String message) {
		Log.i(LibConfig.LOGTAG, " resason phrase[" + message + "]");
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setContent(Object content) {
		Log.i(LibConfig.LOGTAG, "content set[" + content + "]");
		this.content = content;
	}
	
	public Object getContent() {
		return content;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("code(");
		builder.append(code);
		builder.append(")");
		builder.append(message == null?"":message);
		builder.append('\t');
		builder.append(content == null?"":content);
		
		return builder.toString();
	}
	
}
