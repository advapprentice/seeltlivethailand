package com.touchtechnologies.net;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.graphics.BitmapUtil;
import com.touchtechnologies.graphics.MediaHolder;
import com.touchtechnologies.net.entity.ByteArrayBodyExt;
import com.touchtechnologies.net.entity.FileBodyExt;
import com.touchtechnologies.net.entity.WriterListener;

public class ContentUploader implements WriterListener{
	private String uri;
	/**
	 * A WriterLisenter to listen to bytes write when request to uploading media
	 */
	private WriterListener writerListener;
	/**
	 * Number of write byte count
	 */
	private int writerCount;
	/**
	 * Total number of bytes of all medias to write per upload request.
	 */
	private int writerTotalToWrite;
	
	
	public ContentUploader(String uri) {
		Log.d(LibConfig.LOGTAG, "#### UPLOADER: " + uri);
		this.uri = uri;
	}
	
	/**
	 * Upload a given image/video/file to uri.
	 * @param arrayListMediaHolder arraylist contains MediaHolder 
	 * @param key media uploader key
	 */
	public ArrayList<ServiceResponse> upload(String key, ArrayList<MediaHolder> arrayListMediaHolder, List<NameValuePair> nvPairs){
		ArrayList<ServiceResponse> arrResponses = new ArrayList<ServiceResponse>();
		
		/*
		 * Get a total byte to write
		 */
		for(MediaHolder mHolder: arrayListMediaHolder){
			//calculate size from bitmap
			if(mHolder.bitmap != null){			
				writerTotalToWrite += BitmapUtil.sizeOf(mHolder.bitmap);
			}
			
			//calculate size from file
			File fileTmp = new File(mHolder.media.getFilePath());
			writerTotalToWrite += fileTmp.length();
		}		
				
		/*
		 * Upload process 
		 */
		for(int i=0; i<arrayListMediaHolder.size(); i++){
			MediaHolder mHolder = arrayListMediaHolder.get(i);
			
			List<NameValuePair> tmpNvPairs;
			if(mHolder.bitmap != null){			
				tmpNvPairs = new ArrayList<NameValuePair>();
				tmpNvPairs.addAll(nvPairs);
				tmpNvPairs.add(new BasicNameValuePair("name", mHolder.media.getNameThumbnail()));
				
				//request upload
				arrResponses.add(upload(key, mHolder.bitmap, tmpNvPairs));
			}
						
			//image, video, audio
			tmpNvPairs = new ArrayList<NameValuePair>();
			tmpNvPairs.addAll(nvPairs);
			//**create from file				
			tmpNvPairs.add(new BasicNameValuePair("name", mHolder.media.getName()));
				
			//request upload
			arrResponses.add(upload(key, mHolder.media.getFilePath(), tmpNvPairs));
		}
		
		if(writerListener != null){
			writerListener.byteWrite(writerTotalToWrite, writerTotalToWrite);
		}
		
		return arrResponses;
	}
	
	
//	/**
//	 * Upload a given image/video/file to uri.
//	 * @param progressBar a progress update
//	 * @param arrayListMediaHolder arraylist contains MediaHolder 
//	 * @param key media uploader key
//	 */
//	public ArrayList<ServiceResponse> upload(ProgressBar progressBar, String key, ArrayList<MediaHolder> arrayListMediaHolder, List<NameValuePair> nvPairs){
//		this.progressBar = progressBar;
//		return upload(key, arrayListMediaHolder, nvPairs);
//	}
	
	public void setWriterListener(WriterListener writerListener) {
		this.writerListener = writerListener;
	}
	
	private ServiceResponse getResponsePhrase(InputStream inputStream) throws IOException, JSONException{
		ServiceResponse serviceResponse = new ServiceResponse();
		
		JSONObject json = null;
		BufferedReader reader = null;
		StringBuilder sb = new StringBuilder();
		// Start the query
		reader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"));
		String line = null;
		while ((line = reader.readLine()) != null) {
			sb.append(line);
		}
		
		inputStream.close();
		
		Log.i(LibConfig.LOGTAG, "upload response:'" + sb.toString() + "'");
		json = new JSONObject(sb.toString());
		serviceResponse.setCode(json.getInt("status"));
		serviceResponse.setMessage(json.getString("message"));

		return serviceResponse;
	}
	
	public ServiceResponse upload(Bitmap bitmap, List<NameValuePair> nvPairs){
		return upload("", bitmap, nvPairs);
	}
	
	public ServiceResponse upload(String key, Bitmap bitmap, List<NameValuePair> nvPairs){
		Log.i(LibConfig.LOGTAG, ">>>> Upload load content using bitmap " + key);
		
		ByteArrayOutputStream bos = null;
		ServiceResponse serviceResponse = new ServiceResponse();
		
		try{
			
			String filename = null;
			for(NameValuePair nv: nvPairs){
				if(nv.getName().equals("name")){
					filename = nv.getValue();
				}
			}
			
			//set content entity to post
			bos = new ByteArrayOutputStream();
			bitmap.compress(CompressFormat.JPEG, 90, bos);
			ByteArrayBody fileBody = new ByteArrayBodyExt(bos.toByteArray(), filename);
			((ByteArrayBodyExt)fileBody).setListener(this);
			
			bitmap = null;
						
			/* post */
			HttpClient httpClient = new DefaultHttpClient();			
			
			//an entity to post
			HttpEntity entity = getHttpEntityImpl(key, nvPairs, fileBody);
			
			//HTTP post; set usi and set entity to post 
			HttpPost httpPost = new HttpPost(uri);			
			httpPost.setEntity(entity);
			
			//do posting
			HttpResponse response = httpClient.execute(httpPost, getHttpContextImpl(key, nvPairs));
				
			//read status and response message
			int statusCode = response.getStatusLine().getStatusCode();
			if(HttpStatus.SC_OK == statusCode){
				serviceResponse = getResponsePhrase(response.getEntity().getContent());
				
			}else{
				serviceResponse.setCode(statusCode);
				serviceResponse.setMessage(response.getStatusLine().getReasonPhrase());
			}			
			
			Log.i(LibConfig.LOGTAG, "Upload response(" + serviceResponse.getCode() + "):" + serviceResponse.getMessage());
		
		}catch(Exception e){
			serviceResponse.setCode(500);
			serviceResponse.setMessage(e.getMessage());
			Log.e(LibConfig.LOGTAG, "upload content error", e);
		}finally{
			if(bos != null){
				try{
					bos.close();
				}catch(Exception e){
				}
			}
		}
		
		return serviceResponse;
	}
	
	/**
	 * 
	 * @param key
	 * @param nvPairs
	 * @return
	 */
	HttpContext getHttpContextImpl(String key, List<NameValuePair> nvPairs){
		HttpContext httpContext = new BasicHttpContext();
		httpContext.setAttribute(key, key);
		for(NameValuePair nv: nvPairs){
			httpContext.setAttribute(nv.getName(), nv.getValue());
		}
		
		return httpContext;
	}
	
	/**
	 * 
	 * @param key
	 * @param nvPairs
	 * @param content
	 * @return
	 */
	HttpEntity getHttpEntityImpl(String key, List<NameValuePair> nvPairs, ContentBody content){
		MultipartEntityBuilder meBuilder  = MultipartEntityBuilder.create();
		meBuilder.addPart(key, content);
		
		for(NameValuePair nv: nvPairs){
			meBuilder.addPart(nv.getName(), new StringBody(nv.getValue(), ContentType.TEXT_PLAIN));
		}
		
		return meBuilder.build();
	}
	
	
	
	
	/**
	 * Upload a given image/video/file to uri.
	 * @param filePath
	 * @param key media uploader key
	 */
	public ServiceResponse upload(String key, String filePath, List<NameValuePair> nvPairs){
		Log.i(LibConfig.LOGTAG, ">>>> Upload load content "+ key + " using a file path " + filePath);
		
		ServiceResponse serviceResponse = new ServiceResponse();
		try{
			String filename = null;
			for(NameValuePair nv: nvPairs){
				if(nv.getName().equals("name")){
					filename = nv.getValue();
				}
			}
			
			//set content entity to post
			FileBody fileBody = new FileBodyExt(new File(filePath), ContentType.MULTIPART_FORM_DATA, filename);
			((FileBodyExt)fileBody).setListener(this);
						
			/* post */
			HttpClient httpClient = new DefaultHttpClient();
			
			//an entity to post
			HttpEntity entity = getHttpEntityImpl(key, nvPairs, fileBody);
			
			//HTTP post; set uri and set entity to post 
			HttpPost httpPost = new HttpPost(uri);			
			httpPost.setEntity(entity);
			
			//do post
			HttpResponse response = httpClient.execute(httpPost, getHttpContextImpl(key, nvPairs));
		
			//read status and response message
			int statusCode = response.getStatusLine().getStatusCode();
			if(HttpStatus.SC_OK == statusCode){
				serviceResponse = getResponsePhrase(response.getEntity().getContent());
				
			}else{
				serviceResponse.setCode(statusCode);
				serviceResponse.setMessage(response.getStatusLine().getReasonPhrase());
			}
			
			Log.i(LibConfig.LOGTAG, "Upload response(" + serviceResponse.getCode() + "):" + serviceResponse.getMessage());
		
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "upload content error", e);
			
		}
		
		return serviceResponse;
	}
	
	@Override
	public void byteWrite(int write, int total) {
		writerCount += write;
		if(writerListener != null){
			writerListener.byteWrite(writerCount, writerTotalToWrite);
		}
	}
}
