package com.touchtechnologies.net.dataobject;

import java.io.Serializable;

import org.json.JSONObject;

import com.touchtechnologies.json.JSONUtil;

public class WiFiPackage implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 0xbd520ae5db610383L;
	private String name;
	private String id;
	private String description;
	private int price;
	
	public WiFiPackage(JSONObject json) {
		setName(JSONUtil.getString(json, "Name"));
		setDescription(JSONUtil.getString(json, "Description"));
		setPrice(JSONUtil.getInt(json, "Price"));
		setId(JSONUtil.getString(json, "ID"));
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public int getPrice() {
		return price;
	}
}
