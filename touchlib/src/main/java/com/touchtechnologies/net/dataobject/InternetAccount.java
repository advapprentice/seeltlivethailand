package com.touchtechnologies.net.dataobject;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.json.JSONUtil;

public class InternetAccount implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 0x7fe13f7797dcb7feL;
	private String username;
	private String password;

	public InternetAccount(JSONObject json) {
		setUsername(JSONUtil.getString(json, "Username"));
		setPassword(JSONUtil.getString(json, "Password"));
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPassword() {
		return password;
	}
	public JSONObject toJson(){
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("Username", getUsername());
			jsonObject.put("Password", getPassword());

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}
	@Override
	public String toString() {
		return toJson().toString();
	}
}
