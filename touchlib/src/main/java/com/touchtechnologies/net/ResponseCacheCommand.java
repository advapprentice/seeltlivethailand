package com.touchtechnologies.net;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.CacheRequest;
import java.net.CacheResponse;
import java.net.ResponseCache;
import java.net.URI;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.touchtechnologies.log.Log;

public class ResponseCacheCommand extends ResponseCache {
	private File directory;
	
	/**
	 * HashMap containing cached file name and its path (to reduce IO access) 
	 */
	private HashMap<String, CacheResponse> mapReq;
	
	private ResponseCacheCommand(File _directory) {
		this.directory = _directory;
		mapReq = new HashMap<String, CacheResponse>();
		
		File files[] = _directory.listFiles();
		for(File tmp: files){
			mapReq.put(tmp.getName(), null);
		}
	}

	@Override
	public CacheResponse get(URI uri, String requestMethod, final Map<String, List<String>> requestHeaders) {
		Log.println("Somebody call CacheResponse get(URI uri, String requestMethod, Map<String, List<String>> requestHeaders)...");

//		if (requestHeaders.containsKey("Hash-Command")) {
//			//get CacheResponse in a HashMap, if any.
//			String hashKey = requestHeaders.get("Hash-Command").get(0);
//			CacheResponse response = mapReq.get(hashKey);
//			
//			if(response == null){
//				response = new CacheResponse() {
//					@Override
//					public Map<String, List<String>> getHeaders() throws IOException {
//						return requestHeaders;
//					}
//
//					@Override
//					public InputStream getBody() throws IOException {
//						InputStream inStream = null;
//						String path = directory + File.separator + requestHeaders.get("Hash-Command").get(0).hashCode();
//						try {
//							File file = new File(path);
//							if (file.exists() && file.length() > 0) {
//								inStream = new FileInputStream(file);
//							}
//						} catch (Exception e) {
//							Log.log("create cache input ", e);
//						}
//
//						return inStream;
//					}
//				};
//				
//				mapReq.put(hashKey, response);
//			}
//
//			return response;
//			
//		} else {
			return null;
//		}
	}

	@Override
	public CacheRequest put(URI uri, final URLConnection connection) throws IOException {
		Log.println("Somebody call CacheRequest put(URI uri, URLConnection connection)...");
		
//		Map<String, List<String>> requestHeaders = connection.getRequestProperties();
//		if(requestHeaders.containsKey("Hash-Command")){
//			final String path = directory + File.separator + requestHeaders.get("Hash-Command").get(0);
////			
//			return new CacheRequest() {
//
//				@Override
//				public OutputStream getBody() throws IOException {
//					OutputStream outputStream = new FileOutputStream(path);
//					return outputStream;
//				}
//
//				@Override
//				public void abort() {
//				}
//			};
//		}

		return null;
	}

	/**
	 * Creates a new HTTP response cache and sets it as the system default
	 * cache.
	 * 
	 * @param directory
	 * @return
	 */
	public static ResponseCache install(File directory) {
		ResponseCache resCache = new ResponseCacheCommand(directory);
		ResponseCache.setDefault(resCache);

		return resCache;
	}
}
