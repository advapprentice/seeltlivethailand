package com.touchtechnologies.net;

import android.content.Context;
import android.os.Build;
import android.support.v4.util.LruCache;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.wifi.WIFIMod;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

public class ServiceConnector {
	public static boolean cacheEnable;

	/**
	 * Service URL to send a service request to.
	 */
	private String uri;
	
	private boolean cancel;
	
	/**
	 * Cache age properties in second
	 */
	private int cacheAge;
	/**
	 * the maximum staleness in seconds.
	 */
	private final int maxStale = 60 * 60 * 24 * 2;// 2 day stale age
	
	private static LruCache<Integer, JSONObject> lruCache = new LruCache<Integer, JSONObject>(4);

	public ServiceConnector(String uri) {
		this.uri = uri;
		Log.i("====URL====",":"+uri);

	}

	public static void cache(Command command, JSONObject resp){
		if(cacheEnable){
			lruCache.put(command.hashCode(), resp);
		}
		
		Log.i(LibConfig.LOGTAG, "Cache count =" + lruCache.size());
	}
	
	/**
	 * Do not send a service response to a registered Handler.
	 */
	public void cancel(){
		this.cancel = true;
	}
	
	
	
	public ServiceResponse doAsynPut(Command command, boolean encrypt, List<NameValuePair> header){
		Log.i(LibConfig.LOGTAG, "\tdoAsynPost...");
		
		ServiceResponse response = new ServiceResponse();		
		
		if(!WIFIMod.isNetworkConnected(command.getContext())){
			response.setCode(HttpStatus.SC_BAD_GATEWAY);
			response.setMessage("No Internet connection");
			
			return response;
		}
		
		String content = command.toString();

		Log.d(LibConfig.LOGTAG, "\tto URL " + uri);
		
		
		HttpPut httpPut = new HttpPut(uri);
		DefaultHttpClient httpClient = new DefaultHttpClient();
		try {
			StringEntity entity = new StringEntity(content, HTTP.UTF_8);
			entity.setContentType("application/json;charset=UTF-8");
			entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));

			httpPut.setHeader("Accept", "application/json");
			httpPut.setHeader("User-Agent", Build.MODEL);

			if(header != null){
				for(NameValuePair nvp: header){
					httpPut.setHeader(nvp.getName(), nvp.getValue());
				}
			}
			httpPut.setEntity(entity);
			
			HttpConnectionParams.setSoTimeout(httpClient.getParams(), 30 * 1000);
			HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30 * 1000);

			Log.i(LibConfig.LOGTAG, "\texecuting...");
			HttpResponse httpResponse = httpClient.execute(httpPut);
			
			//reading response
			int responseCode = ((HttpResponse) httpResponse).getStatusLine().getStatusCode();
			response.setCode(responseCode);
			
			Log.i(LibConfig.LOGTAG, "\tresponse code=" + responseCode);
			
			if (responseCode == HttpStatus.SC_OK) {
				InputStream inputStream = httpResponse.getEntity().getContent();
				
				Log.i(LibConfig.LOGTAG, "\tgetting content from http response object("+ inputStream +")...");
				
				BufferedReader reader = null;
				StringBuilder sb = new StringBuilder();
				try {
					// Start the query
					reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"));
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line);
					}	
					
					//parsing response to json object to access it attributes value
					JSONObject jsonObject = new JSONObject(sb.toString());
					Log.d(LibConfig.LOGTAG, jsonObject.toString());

					Log.i("Response :==== >", jsonObject.toString());
					//get status (code) attribute value
					responseCode = jsonObject.getInt("status");
					response.setCode(responseCode);
					
					//get message attrubute value
					if(responseCode != 0 && !jsonObject.isNull("message")){
						response.setMessage(jsonObject.getString("message"));
					}					
					
					//get data attribute and decrypt it
					if(!jsonObject.isNull("data")){
						String cryptedText = jsonObject.getString("data");
						
					//	String text = command.getCipherInstanceForCommand().decrypt(cryptedText);	
						response.setContent(cryptedText);
					}
				}catch(JSONException json){
					Log.e(LibConfig.LOGTAG, "\tconvert json error", json);
					response.setCode(500);
					response.setMessage(sb.toString());
										
				}catch(Exception er){
					Log.e(LibConfig.LOGTAG, "\treading stream error", er);
					response.setCode(500);
					response.setMessage(er == null?"":er.getMessage());
					
				}finally{
					if(reader != null){
						reader.close();
					}
					if(inputStream != null){
						inputStream.close();
					}
				}
			} else {
				Log.i(LibConfig.LOGTAG, "\tgetting resason phrase");
				response.setMessage(((HttpResponse) httpResponse).getStatusLine().getReasonPhrase());
			}

		} catch (Exception e) {
			response.setCode(500);
			response.setMessage("Server error(" + e.getMessage() + ")");
			Log.e(LibConfig.LOGTAG, "service error ", e);			
		}

		return response;
	}
	
	
	/**
	 * @deprecated
	 * Post a command to service and parsing response to ServiceResponse object.
	 * This method expected 3 attributes in service's response json:
	 * 
	 * 1. status: response status code
	 * 2. message: response message or error message, if any.
	 * 3. data: encrypted data
	 * 
	 * If a response contains status==0 then this method will try to decrypting data and then set
	 * it to ServiceResponse's content.
	 * 
	 * @param command Command to post
	 * @param encrypt
	 * @return
	 */
	public ServiceResponse doAsynPost(Command command, boolean encrypt, List<NameValuePair> header){
		Log.i(LibConfig.LOGTAG, "\tdoAsynPost...");
		
		ServiceResponse response = new ServiceResponse();		
		
		if(!WIFIMod.isNetworkConnected(command.getContext())){
			response.setCode(HttpStatus.SC_BAD_GATEWAY);
			response.setMessage("No Internet connection");
			
			return response;
		}
		
		String content = command.toString();

		Log.d(LibConfig.LOGTAG, "\tto URL " + uri);
		
		
		HttpPost httpPost = new HttpPost(uri);
		DefaultHttpClient httpClient = new DefaultHttpClient();
		try {
			StringEntity entity = new StringEntity(content, HTTP.UTF_8);
			entity.setContentType("application/json;charset=UTF-8");
			entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));

			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("User-Agent", Build.MODEL);
			if(header != null){
				for(NameValuePair nvp: header){
					httpPost.setHeader(nvp.getName(), nvp.getValue());
				}
			}
			httpPost.setEntity(entity);
			
			HttpConnectionParams.setSoTimeout(httpClient.getParams(), 40 * 1000);
			HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 40 * 1000);

			Log.i(LibConfig.LOGTAG, "\texecuting...");
			HttpResponse httpResponse = httpClient.execute(httpPost);
			
			//reading response
			int responseCode = ((HttpResponse) httpResponse).getStatusLine().getStatusCode();
			response.setCode(responseCode);
			
			Log.i(LibConfig.LOGTAG, "\tresponse code=" + responseCode);
			
			if (responseCode == HttpStatus.SC_OK) {
				InputStream inputStream = httpResponse.getEntity().getContent();
				
				Log.i(LibConfig.LOGTAG, "\tgetting content from http response object("+ inputStream +")...");
				
				BufferedReader reader = null;
				StringBuilder sb = new StringBuilder();
				try {
					// Start the query
					reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"));
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line);
					}	
					
					//parsing response to json object to access it attributes value
					JSONObject jsonObject = new JSONObject(sb.toString());
					Log.d(LibConfig.LOGTAG, jsonObject.toString());
					
					//get status (code) attribute value
					if(!jsonObject.isNull("status")) {
						responseCode = jsonObject.getInt("status");
						response.setCode(responseCode);
					}

					//get message attrubute value
					if(responseCode != 0 && !jsonObject.isNull("message")){
						response.setMessage(jsonObject.getString("message"));
					}					
					
					//get data attribute and decrypt it
					if(!jsonObject.isNull("data")){
						String cryptedText = jsonObject.getString("data");
						
					//	String text = command.getCipherInstanceForCommand().decrypt(cryptedText);
						response.setContent(cryptedText);
					}
				}catch(JSONException json){
					Log.e(LibConfig.LOGTAG, "\tconvert json error", json);
					response.setCode(500);
					response.setMessage(sb.toString());
										
				}catch(Exception er){
					Log.e(LibConfig.LOGTAG, "\treading stream error", er);
					response.setCode(500);
					response.setMessage(er == null?"":er.getMessage());
					
				}finally{
					if(reader != null){
						reader.close();
					}
					if(inputStream != null){
						inputStream.close();
					}
				}
			} else {
				Log.i(LibConfig.LOGTAG, "\tgetting resason phrase");
				response.setMessage(((HttpResponse) httpResponse).getStatusLine().getReasonPhrase());
			}

		} catch (Exception e) {
			response.setCode(500);
			response.setMessage("Server error(" + e.getMessage() + ")");
			Log.e(LibConfig.LOGTAG, "service error ", e);			
		}

		return response;
	}
	
	/**
	 * Post a command to service and parsing response to ServiceResponse object.
	 * This method expected 3 attributes in service's response json:
	 * 
	 * 1. status: response status code
	 * 2. message: response message or error message, if any.
	 * 3. data: encrypted data
	 * 
	 * If a response contains status==0 then this method will try to decrypting data and then set
	 * it to ServiceResponse's content.
	 * 
	 * @param command Command to post
	 * @param encrypt
	 * @return
	 */
	public ServiceResponse doAsynPost(Command command, boolean encrypt){
		Log.i(LibConfig.LOGTAG, "\tdoAsynPost...");
		
		ServiceResponse response = new ServiceResponse();		
		
		if(!WIFIMod.isNetworkConnected(command.getContext())){
			response.setCode(HttpStatus.SC_BAD_GATEWAY);
			response.setMessage("No Internet connection");
			
			return response;
		}
		
		String content = command.toString();

		Log.d(LibConfig.LOGTAG, "\tto URL " + uri);
		Log.i(LibConfig.LOGTAG, "-> " + content);
		
		HttpPost httpPost = new HttpPost(uri);
		DefaultHttpClient httpClient = new DefaultHttpClient();
		try {
			StringEntity entity = new StringEntity(content, HTTP.UTF_8);
			entity.setContentType("application/json;charset=UTF-8");
			entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));

			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("User-Agent", Build.MODEL);
			httpPost.setEntity(entity);
			
			HttpConnectionParams.setSoTimeout(httpClient.getParams(), 40 * 1000);
			HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 40 * 1000);

			Log.i(LibConfig.LOGTAG, "\texecuting...");
			HttpResponse httpResponse = httpClient.execute(httpPost);
			
			//reading response
			int responseCode = ((HttpResponse) httpResponse).getStatusLine().getStatusCode();
			response.setCode(responseCode);
			
			Log.i(LibConfig.LOGTAG, "\tresponse code=" + responseCode);
			
			if (responseCode == HttpStatus.SC_OK) {
				InputStream inputStream = httpResponse.getEntity().getContent();
				
				Log.i(LibConfig.LOGTAG, "\tgetting content from http response object("+ inputStream +")...");
				
				BufferedReader reader = null;
				StringBuilder sb = new StringBuilder();
				try {
					// Start the query
					reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"));
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line);
					}	
					
					//parsing response to json object to access it attributes value
					JSONObject jsonObject = new JSONObject(sb.toString());
					Log.d(LibConfig.LOGTAG, jsonObject.toString());
					
					//get status (code) attribute value
					if(!jsonObject.isNull("status")) {
						responseCode = jsonObject.getInt("status");
						response.setCode(responseCode);
					}
					//get message attrubute value
					if(responseCode != 0 && !jsonObject.isNull("message")){
						response.setMessage(jsonObject.getString("message"));
					}					
					
					//get data attribute and decrypt it
					if(!jsonObject.isNull("data")){
						String cryptedText = jsonObject.getString("data");
						
						String text = encrypt?command.getCipherInstanceForCommand().decrypt(cryptedText):cryptedText;	
						response.setContent(text);
					}
				}catch(JSONException json){
					Log.e(LibConfig.LOGTAG, "\tconvert json error", json);
					response.setCode(500);
					response.setMessage(sb.toString());
										
				}catch(Exception er){
					Log.e(LibConfig.LOGTAG, "\treading stream error", er);
					response.setCode(500);
					response.setMessage(er == null?"":er.getMessage());
					
				}finally{
					if(reader != null){
						reader.close();
					}
					if(inputStream != null){
						inputStream.close();
					}
				}
			} else {
				Log.i(LibConfig.LOGTAG, "\tgetting resason phrase");
				response.setMessage(((HttpResponse) httpResponse).getStatusLine().getReasonPhrase());
			}

		} catch (Exception e) {
			response.setCode(500);
			response.setMessage("Server error(" + e.getMessage() + ")");
			Log.e(LibConfig.LOGTAG, "service error ", e);			
		}

		return response;
	}

	public ServiceResponse doAsynPatch(Command command, boolean encrypt, List<NameValuePair> header){
		Log.i(LibConfig.LOGTAG, "\tdoAsynPost...");

		ServiceResponse response = new ServiceResponse();

		if(!WIFIMod.isNetworkConnected(command.getContext())){
			response.setCode(HttpStatus.SC_BAD_GATEWAY);
			response.setMessage("No Internet connection");

			return response;
		}

		String content = command.toString();

		Log.d(LibConfig.LOGTAG, "\tto URL " + uri);


		HttpPatch httpPatch = new HttpPatch(uri);
		DefaultHttpClient httpClient = new DefaultHttpClient();
		try {
			StringEntity entity = new StringEntity(content, HTTP.UTF_8);
			entity.setContentType("application/json;charset=UTF-8");
			entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));

			httpPatch.setHeader("Accept", "application/json");
			httpPatch.setHeader("User-Agent", Build.MODEL);
			if(header != null){
				for(NameValuePair nvp: header){
					httpPatch.setHeader(nvp.getName(), nvp.getValue());
				}
			}
			httpPatch.setEntity(entity);

			HttpConnectionParams.setSoTimeout(httpClient.getParams(), 30 * 1000);
			HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30 * 1000);

			Log.i(LibConfig.LOGTAG, "\texecuting...");
			HttpResponse httpResponse = httpClient.execute(httpPatch);

			//reading response
			int responseCode = ((HttpResponse) httpResponse).getStatusLine().getStatusCode();
			response.setCode(responseCode);

			Log.i(LibConfig.LOGTAG, "\tresponse code=" + responseCode);

			if (responseCode == HttpStatus.SC_OK) {
				InputStream inputStream = httpResponse.getEntity().getContent();

				Log.i(LibConfig.LOGTAG, "\tgetting content from http response object("+ inputStream +")...");

				BufferedReader reader = null;
				StringBuilder sb = new StringBuilder();
				try {
					// Start the query
					reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"));
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line);
					}

					//parsing response to json object to access it attributes value
					JSONObject jsonObject = new JSONObject(sb.toString());
					Log.d(LibConfig.LOGTAG, jsonObject.toString());

					//get status (code) attribute value
					responseCode = jsonObject.getInt("status");
					response.setCode(responseCode);

					//get message attrubute value
					if(responseCode != 0 && !jsonObject.isNull("message")){
						response.setMessage(jsonObject.getString("message"));
					}

					//get data attribute and decrypt it
					if(!jsonObject.isNull("data")){
						String cryptedText = jsonObject.getString("data");

					//	String text = command.getCipherInstanceForCommand().decrypt(cryptedText);
						response.setContent(cryptedText);
					}
				}catch(JSONException json){
					Log.e(LibConfig.LOGTAG, "\tconvert json error", json);
					response.setCode(500);
					response.setMessage(sb.toString());

				}catch(Exception er){
					Log.e(LibConfig.LOGTAG, "\treading stream error", er);
					response.setCode(500);
					response.setMessage(er == null?"":er.getMessage());

				}finally{
					if(reader != null){
						reader.close();
					}
					if(inputStream != null){
						inputStream.close();
					}
				}
			} else {
				Log.i(LibConfig.LOGTAG, "\tgetting resason phrase");
				response.setMessage(((HttpResponse) httpResponse).getStatusLine().getReasonPhrase());
			}

		} catch (Exception e) {
			response.setCode(500);
			response.setMessage("Server error(" + e.getMessage() + ")");
			Log.e(LibConfig.LOGTAG, "service error ", e);
		}

		return response;
	}


	
	public ServiceResponse doPost(Command command, boolean encrypt){
		Log.i(LibConfig.LOGTAG, "\tdoAsynPost...");
		
		ServiceResponse response = new ServiceResponse();		
		
		if(!WIFIMod.isNetworkConnected(command.getContext())){
			response.setCode(HttpStatus.SC_BAD_GATEWAY);
			response.setMessage("No Internet connection");
			
			return response;
		}
		
		String content = command.toString();

		Log.d(LibConfig.LOGTAG, "\tto URL " + uri);
		Log.i(LibConfig.LOGTAG, "-> " + content);
		
		HttpPost httpPost = new HttpPost(uri);
		DefaultHttpClient httpClient = new DefaultHttpClient();
		try {
			StringEntity entity = new StringEntity(content, HTTP.UTF_8);
			entity.setContentType("application/json;charset=UTF-8");
			entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));

			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("User-Agent", Build.MODEL);
			httpPost.setEntity(entity);
			
			HttpConnectionParams.setSoTimeout(httpClient.getParams(), 30 * 1000);
			HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30 * 1000);

			Log.i(LibConfig.LOGTAG, "\texecuting...");
			HttpResponse httpResponse = httpClient.execute(httpPost);
			
			//reading response
			int responseCode = ((HttpResponse) httpResponse).getStatusLine().getStatusCode();
			response.setCode(responseCode);
			
			Log.i(LibConfig.LOGTAG, "\tresponse code=" + responseCode);
			
			if (responseCode == HttpStatus.SC_OK) {
				InputStream inputStream = httpResponse.getEntity().getContent();
				
				Log.i(LibConfig.LOGTAG, "\tgetting content from http response object("+ inputStream +")...");
				
				BufferedReader reader = null;
				StringBuilder sb = new StringBuilder();
				try {
					// Start the query
					reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"));
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line);
					}	
					
					//parsing response to json object to access it attributes value
					JSONObject jsonObject = new JSONObject(sb.toString());
					Log.d(LibConfig.LOGTAG, jsonObject.toString());
					
					//get status (code) attribute value
					responseCode = jsonObject.getInt("status");
					response.setCode(responseCode);
					
					//get message attrubute value
					if(responseCode != 0 && !jsonObject.isNull("message")){
						response.setMessage(jsonObject.getString("message"));
					}					
					
					//get data attribute and decrypt it
					if(!jsonObject.isNull("data")){
						String cryptedText = jsonObject.getString("data");
						
					//	String text = encrypt?command.getCipherInstanceForCommand().decrypt(cryptedText):cryptedText;	
						response.setContent(cryptedText);
					}
				}catch(JSONException json){
					Log.e(LibConfig.LOGTAG, "\tconvert json error", json);
					response.setCode(500);
					response.setMessage(sb.toString());
										
				}catch(Exception er){
					Log.e(LibConfig.LOGTAG, "\treading stream error", er);
					response.setCode(500);
					response.setMessage(er == null?"":er.getMessage());
					
				}finally{
					if(reader != null){
						reader.close();
					}
					if(inputStream != null){
						inputStream.close();
					}
				}
			} else {
				Log.i(LibConfig.LOGTAG, "\tgetting resason phrase");
				response.setMessage(((HttpResponse) httpResponse).getStatusLine().getReasonPhrase());
			}
		}catch(SocketTimeoutException se){
			response.setCode(500);
			response.setMessage("Can't connect to server. " + (se.getMessage() == null?"":se.getMessage()));

			Log.e(LibConfig.LOGTAG, "service error ", se);
		} catch (Exception e) {
			response.setCode(500);
			response.setMessage("Server error(" + e.getMessage() + ")");
			Log.e(LibConfig.LOGTAG, "service error ", e);			
		}

		return response;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Post a given command for a service. The service host process a command
	 * and responses with a formated result.
	 * 
	 * @param context
	 * @param headers list of headers
	 */
	public String doDelete(Context context, final List<NameValuePair> headers) {
		String result;
		if (WIFIMod.isNetworkConnected(context)) {

			HttpDelete httpDelete = new HttpDelete(uri);
			DefaultHttpClient httpClient = new DefaultHttpClient();
			try {
				httpDelete.setHeader("Accept", "application/json");
				
				if(headers != null){
					for(NameValuePair nvp: headers){
						httpDelete.setHeader(nvp.getName(), nvp.getValue());
					}
				}
				
				HttpConnectionParams.setSoTimeout(httpClient.getParams(),
						10 * 1000);
				HttpConnectionParams.setConnectionTimeout(
						httpClient.getParams(), 15 * 1000);

				HttpResponse response = httpClient.execute(httpDelete);
				InputStream is = response.getEntity().getContent();

				byte[] data = new byte[128];
				int c;
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				while ((c = is.read(data)) != -1) {
					bos.write(data, 0, c);
				}

				result = new String(bos.toByteArray());
				Log.i(LibConfig.LOGTAG, "JSON: " + result);

			} catch (Exception e) {
				Log.e(LibConfig.LOGTAG, "service error ", e);
				result = e.getMessage();
			}

		} else {
			result = "No Internet Connection";
			Log.i(LibConfig.LOGTAG, result, null);
			
		}
		
		return result;
	}
	/**
	 * Execute Http get, non-thread, and return raw result text.
	 * This method should be called by thread or AsyncTask object.
	 * @param context
	 * @return result text
	 */
	public String doDelete(Context context) {
		return doDelete(context, null);
	}	

	
	
	
	
	public String doGet(Context context, final List<NameValuePair> headers) {
		String result;
		if (WIFIMod.isNetworkConnected(context)) {

			HttpGet httpGet = new HttpGet(uri);
			DefaultHttpClient httpClient = new DefaultHttpClient();
			try {
				httpGet.setHeader("Accept", "application/json");
				
				if(headers != null){
					for(NameValuePair nvp: headers){
						httpGet.setHeader(nvp.getName(), nvp.getValue());
					}
				}
				
				HttpConnectionParams.setSoTimeout(httpClient.getParams(),
						50 * 1000);
				HttpConnectionParams.setConnectionTimeout(
						httpClient.getParams(), 50 * 1000);

  				HttpResponse response = httpClient.execute(httpGet);
				InputStream is = response.getEntity().getContent();

				byte[] data = new byte[512];
				int c;
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				while ((c = is.read(data)) != -1) {
					bos.write(data, 0, c);
				}

				result = new String(bos.toByteArray());
				Log.i(LibConfig.LOGTAG, "JSON: " + result);

			} catch (Exception e) {
				Log.e(LibConfig.LOGTAG, "service error ", e);
				result = e.getMessage();
			}

		} else {
			result = "No Internet Connection!";
			Log.i(LibConfig.LOGTAG, result, null);
			
		}
		
		return result;
	}
	/**
	 * Execute Http get, non-thread, and return raw result text.
	 * This method should be called by thread or AsyncTask object.
	 * @param context
	 * @return result text
	 */
	public String doGet(Context context) {
		return doGet(context, null);
	}	


	/**
	 * Post a given command for a service. The service host process a command
	 * and responses with a formated result.
	 * 
	 * @param command
	 * @param headers list of headers
	 */
	public void doPost(final Command command, final List<NameValuePair> headers){
		if(WIFIMod.isNetworkConnected(command.getContext())){
			
			Thread serrvice = new Thread() {
				@Override
				public void run() {
					String content = command.toString();
					
					int cmdIdentity = command.hashCode();
					JSONObject cacheResponse = null;
					if(cacheEnable){
						cacheResponse = lruCache.get(cmdIdentity);
					}
					
					if(cacheResponse != null && !cancel){
						try {
							command.httpResponse(cacheResponse);
						} catch (GeneralSecurityException ge) {
						}
						
					}else if(!cancel){
						HttpPost httpPost = new HttpPost(uri);
						DefaultHttpClient httpClient = new DefaultHttpClient();
						try {
	
							StringEntity entity = new StringEntity(content);
							entity.setContentType("application/json;charset=UTF-8");
							entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
							httpPost.setHeader("Accept", "application/json");
	
							if(headers != null){
								for(NameValuePair nv: headers){
									httpPost.addHeader(nv.getName(), nv.getValue());
								}
							}
							httpPost.setEntity(entity);
	
							HttpConnectionParams.setSoTimeout(httpClient.getParams(), 30 * 1000);
							HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30 * 1000);
	
							HttpResponse response = httpClient.execute(httpPost);
							command.httpResponse(response);
	
						} catch (Exception e) {
							Log.e(LibConfig.LOGTAG, "service error ", e);
							try {
								command.httpResponse(e);
							} catch (GeneralSecurityException ge) {
							}
						}
					}
				}
			};
			serrvice.start();
			
		}else{
			Log.i(LibConfig.LOGTAG, "NOP: No Internet", null);
//			try {
//				command.httpResponse(new IOException("No Internet Connection"));
//			} catch (GeneralSecurityException ge) {
//			}
		}
		
	}
	
	/**
	 * Post a given command for a service. The service host process a command
	 * and responses with a formated result.
	 * 
	 * @param command
	 */
	public void doPost(Command command) {
		doPost(command, null);
	}
	
	
	/**
	 * upload profilepic form local path for see it live thailand
	 * 
	 * 
	 * 
	 * **/
	public ServiceResponse doUploadpic(Command command,final List<NameValuePair> header){
		Log.i(LibConfig.LOGTAG, "\tdoAsynPost...");
		
		ServiceResponse response = new ServiceResponse();		
		
		if(!WIFIMod.isNetworkConnected(command.getContext())){
			response.setCode(HttpStatus.SC_BAD_GATEWAY);
			response.setMessage("No Internet connection");
			
			return response;
		}
		
		String content = command.toString();

		Log.d(LibConfig.LOGTAG, "\tto URL " + uri);
		
		//MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		HttpPost httpPost = new HttpPost(uri);
		DefaultHttpClient httpClient = new DefaultHttpClient();
		String strPho = content;
		try {
			/*StringEntity entity = new StringEntity(content, HTTP.UTF_8);
			entity.setContentType("application/json;charset=UTF-8");
			entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));*/
			
			try {
		         JSONObject  jsonRootObject = new JSONObject(content);
		         
		       strPho = jsonRootObject.optString("photo").toString();
		      // String boundary = "===" + "*********" + "===";
		     //  strPho = strPho+boundary;
		       
		       
		      } catch (JSONException e) {e.printStackTrace();}
			//ByteArrayOutputStream bao = new ByteArrayOutputStream();
			//byte [] ba = strPho.getBytes(Charset.forName("UTF-8"));
			File ba = new File(strPho);

			String boundary = "-------------" + System.currentTimeMillis();
			HttpEntity entity = MultipartEntityBuilder.create()
					.setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
					.setBoundary(boundary)
					.addPart("file_upload", new FileBody(ba))
					.build();
			httpPost.setHeader("User-Agent", Build.MODEL);
			httpPost.setHeader("Content-type", "multipart/form-data; boundary="+boundary);
			
			if(header != null){
				for(NameValuePair nvp: header){
					httpPost.setHeader(nvp.getName(), nvp.getValue());
				}
			}
			
			httpPost.setEntity(entity);
			
			HttpConnectionParams.setSoTimeout(httpClient.getParams(), 30 * 1000);
			HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30 * 1000);

			Log.i(LibConfig.LOGTAG, "\texecuting...");
			HttpResponse httpResponse = httpClient.execute(httpPost);
			
			//reading response
			int responseCode = ((HttpResponse) httpResponse).getStatusLine().getStatusCode();
			response.setCode(responseCode);
			
			Log.i(LibConfig.LOGTAG, "\tresponse code=" + responseCode);
			
			if (responseCode == HttpStatus.SC_OK) {
				InputStream inputStream = httpResponse.getEntity().getContent();
				
				Log.i(LibConfig.LOGTAG, "\tgetting content from http response object("+ inputStream +")...");
				
				BufferedReader reader = null;
				StringBuilder sb = new StringBuilder();
				try {
					// Start the query
					reader = new BufferedReader(new InputStreamReader(inputStream,"utf-8"));
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line);
					}	
					
					//parsing response to json object to access it attributes value
					JSONObject jsonObject = new JSONObject(sb.toString());
					Log.d(LibConfig.LOGTAG, jsonObject.toString());
					
					//get status (code) attribute value
					responseCode = jsonObject.getInt("status");
					response.setCode(responseCode);
					
					//get message attrubute value
					if(responseCode != 0 && !jsonObject.isNull("message")){
						response.setMessage(jsonObject.getString("message"));
					}					
					
					//get data attribute and decrypt it
					if(!jsonObject.isNull("data")){
						String cryptedText = jsonObject.getString("data");
						
				//		String text = command.getCipherInstanceForCommand().decrypt(cryptedText);	
						response.setContent(cryptedText);
					}
				}catch(JSONException json){
					Log.e(LibConfig.LOGTAG, "\tconvert json error", json);
					response.setCode(500);
					response.setMessage(sb.toString());
										
				}catch(Exception er){
					Log.e(LibConfig.LOGTAG, "\treading stream error", er);
					response.setCode(500);
					response.setMessage(er == null?"":er.getMessage());
					
				}finally{
					if(reader != null){
						reader.close();
					}
					if(inputStream != null){
						inputStream.close();
					}
				}
			} else {
				Log.i(LibConfig.LOGTAG, "\tgetting resason phrase");
				response.setMessage(((HttpResponse) httpResponse).getStatusLine().getReasonPhrase());
			}

		} catch (Exception e) {
			response.setCode(500);
			response.setMessage("Server error(" + e.getMessage() + ")");
			Log.e(LibConfig.LOGTAG, "service error ", e);			
		}

		return response;
	}
	
	
	
	
	// /**
	// * Post a given command for a service. If "use cache" is true then
	// HttpResponseCache will we used (assuming that if it was set in Activity
	// ).
	// * The service host process a command and responses with a formated
	// result.
	// * @param command
	// * @deprecated
	// */
	// public void doPost(final Command command, final ResponseCache cache){
	// if(cache == null){
	// doPost(command);
	// }else{
	// Thread service = new Thread() {
	// @Override
	// public void run() {
	// HttpURLConnection httpCon = null;
	// OutputStream os = null;
	// InputStream in = null;
	//
	// try {
	// URL url = new URL(uri);
	//
	// httpCon = (HttpURLConnection)url.openConnection();
	// httpCon.setUseCaches(true);
	// httpCon.setDoInput(true);
	// // httpCon.setDoOutput(true);
	// httpCon.setConnectTimeout(10 * 1000);
	// httpCon.setReadTimeout(8 * 1000);
	// // httpCon.setDefaultUseCaches(true);
	// httpCon.setRequestMethod(HttpPost.METHOD_NAME);
	//
	// String commandText = command.toString();
	// httpCon.addRequestProperty("Hash-Command",
	// Math.abs(commandText.hashCode()) + "");
	//
	// httpCon.connect();
	//
	// // httpCon.addRequestProperty("Cache-Control", "max-age=3600");
	// // httpCon.addRequestProperty("Cache-Control", "max-stale=" + maxStale);
	//
	// //write response
	// byte data[] = commandText.getBytes();
	// os = httpCon.getOutputStream();
	// os.write(data);
	// os.flush();
	//
	// in = httpCon.getInputStream();
	//
	// //notify response to the command
	// BasicStatusLine statusline = new BasicStatusLine(new
	// ProtocolVersion("http", 1, 1), httpCon.getResponseCode(),
	// httpCon.getResponseMessage());
	// DefaultHttpResponseFactory defaultHttpResponseFactory = new
	// DefaultHttpResponseFactory();
	//
	// BasicHttpEntity basicEntity = new BasicHttpEntity();
	// basicEntity.setContent(in);
	// basicEntity.setContentEncoding("utf-8");
	// basicEntity.setContentType("text/plain");
	//
	// HttpResponse httpResponse =
	// defaultHttpResponseFactory.newHttpResponse(statusline, null);
	// httpResponse.setEntity(basicEntity);
	//
	// command.httpResponse(httpResponse);
	//
	// } catch (Exception e) {
	// Log.log("service post (cache:" + cache + ") to " + uri + " ", e);
	//
	// try{
	// command.httpResponse(e);
	// }catch(GeneralSecurityException ge){
	// }
	//
	// } finally {
	// if(httpCon != null){
	// httpCon.disconnect();
	// }
	//
	// if(os != null){
	// try{
	// os.close();
	// }catch(IOException ioe){
	// }
	// }
	//
	// if(in != null){
	// try{
	// in.close();
	// }catch(IOException ioe){
	// }
	// }
	// }
	// }
	// };
	// service.start();
	// }
	// }
	private String getValueByKey(ArrayList<NameValuePair> _list, String key) {
	    for(NameValuePair nvPair : _list) {
	        if(nvPair.getName().equals(key)) {
	            return nvPair.getValue().toString();
	        }
	    }
	    return null;
	}
}
