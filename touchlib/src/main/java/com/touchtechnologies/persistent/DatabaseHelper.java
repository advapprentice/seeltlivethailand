package com.touchtechnologies.persistent;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public abstract class DatabaseHelper extends SQLiteOpenHelper {
	protected Context context;
	
	public DatabaseHelper(Context context, String db, int version) {
		super(context, db, null, version);
		this.context = context;
	}

	
	public abstract void onCreate(SQLiteDatabase db);

	
	public abstract void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion);

	
	public String getString(Cursor cursor, String columnName){
		return cursor.getString(cursor.getColumnIndex(columnName));
	}
	
	public int getInt(Cursor cursor, String columnName){
		return cursor.getInt(cursor.getColumnIndex(columnName));
	}

	/**
	 * Deleting all rows in the given tables
	 */
	public void deleteAll(String tables[]) {
		for (int i = 0; i < tables.length; i++) {
			delete(tables[i]);
		}
	}

	/**
	 * Deleting rows in the given table
	 * 
	 * @param table
	 */
	public void delete(String table) {
		getWritableDatabase().delete(table, null, null);
	}
}
