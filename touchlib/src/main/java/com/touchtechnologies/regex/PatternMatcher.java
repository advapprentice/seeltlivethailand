package com.touchtechnologies.regex;

import java.util.regex.Pattern;

import android.util.Patterns;
import android.widget.EditText;
import android.widget.TextView;

public class PatternMatcher {
	private static String[] startNumber4Mobile = new String[]{"08", "09"};
	private static String[] startNumber4Tel = new String[]{
		"01", "02", "03", "04", "05",
		"06", "07"};
	private static String[] startNumber4TelInter = new String[]{
		"1", "2", "3", "4", "5",
		"6", "7"};
	private static String[] startNumber4MobileInter = new String[]{"8","9"};
	
	public static boolean isValidEmail(String email){
		boolean valid = false;
		
		if(email != null){
			Pattern pattern = Patterns.EMAIL_ADDRESS;
			valid = pattern.matcher(email).find(); 
		}
		
		return valid;
	}
	

	
	private static boolean isValidThaiTelImpl(String number, String[] prefix, int length){
		boolean valid = false;
		
		number = number.trim();
		Pattern phonePattern = Patterns.PHONE;
		valid = phonePattern.matcher(number).find();
		
		if(valid){//Thailand specific
			boolean thaiNumber = false;
			for(int i=0; i<prefix.length; i++){
				thaiNumber |= number.startsWith(prefix[i]); 
			}
			thaiNumber &= number.length() == length;
			
			valid &= thaiNumber; 
		}
		
		return valid;
	}
	/**
	 * Is a given text view, edit text contains text and text's length >= a given length
	 * @param txtView
	 * @param length
	 * @return true if valid
	 */
	public static boolean isValidLength(TextView txtView, int length){
		String text = txtView.getText().toString();
		return text.trim().length() >= length;
	}
	
	public static boolean isValidThaiMobileNumber(String number){
		return isValidThaiTelImpl(number, startNumber4Mobile, 10)
				|| isValidThaiTelImpl(number, startNumber4MobileInter,9);
	}
	
	public static boolean isEmpty(TextView tv){
		return tv == null?true:isEmpty(tv.getText().toString());
	}
	
	public static boolean isEmpty(EditText tv){
		
		return tv == null?true:isEmpty(tv.getText().toString());
	}
	
	public static boolean isEmpty(String text){
		return text.trim().length() > 0?false:true;
	}
	
	public static boolean isValidThaiPhoneNumber(String number){
		return isValidThaiTelImpl(number, startNumber4Mobile, 10)
				|| isValidThaiTelImpl(number, startNumber4Tel, 9)
				|| isValidThaiTelImpl(number, startNumber4TelInter, 8)
				|| isValidThaiTelImpl(number, startNumber4MobileInter,9);
	}
	
   public static boolean isValidThaiPhoneNumber(EditText tv){
	   String number = tv.getText().toString();
		return isValidThaiPhoneNumber(number);
	}
   
   public static boolean isInvalidCitizenID(EditText tv){
		
		return tv == null?true:(tv.getText().toString().trim().length()==13?false:true);
	}
   
   public static boolean isInvalidPassword(EditText tv){
		return tv == null?true:(tv.getText().toString().trim().length()>5?false:true);
	}
   
   public static boolean isInvalidUsername(EditText tv){
		return tv == null?true:(tv.getText().toString().trim().length()>5?false:true);
	}
	public static boolean isValidPhoneNumberGlobal(TextView txtView){
		boolean valid =false;
		String  number = txtView.getText().toString();
		Pattern phonePattern = Patterns.PHONE;
		valid = phonePattern.matcher(number).find();
		return valid;
	}
  
   /**
	 * Is a given string contains special character
	 * @param s
	 * @return true if contains special character
	 */
	public static boolean isContainsSpecialCharacter(String s) {
		return s.matches("[$&+,:;=?@#|'<>.-^*()%!]");//
	}


	/**
	 * Is a given string contains a character which is not a number
	 * @param s
	 * @return
	 */
	public static boolean isContainsNotNumber(String s) {
		return s.matches("([\\w&&[^0-9]])*");
	}
	
	/**
	 * Is a given string contains a number
	 * @param s
	 * @return
	 */
	public static boolean isContainsNumber(String s) {
		return s.matches("\\d");
	}

	/**
	 * Is a given string contains non-thai ch aracter
	 * @param s
	 * @return
	 */
	public static boolean isContainsNonThaiCharacter(String s) {
		return  s.matches("([\\w&&[^ฮ-9]])*");
	}
	
	/**
	 * Is a given text contains only A-Z, a-z
	 * @param s
	 * @return
	 */
	public static boolean isValidEnglishCharacter(String s){
		return s.matches("\\w");
	}
	
	/**
	 * Is a given text contains only thai charater
	 * @param s
	 * @return
	 */
	public static boolean isValidThaiCharacter(String s){
		return s.matches("[^ก-ฮ]");
	}
   
}
