package com.touchtechnologies.mall.dataobject;

import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;

import android.graphics.Bitmap;

import com.touchtechnologies.location.dataobject.Position;
import com.touchtechnologies.mall.dataobject.Category.CategoryType;


public class Feed implements Favoritable, Sharable{
	public static final int EVENT_START = 0X54A201;
	public static final int EVENT_END 	= 0X54A202;
	public static final int EVENT_WHEN 	= 0X54A203;	
		
	private boolean favorite;
	private boolean highlight;
	private String title;
	private String info;
	private String uriImage;
	private Hashtable<Integer, Date> hEventDate;
	private LinkedHashSet<Position> lHSPosition;
	private String tags;
	private CategoryType category;
	
	public Feed(){
		hEventDate = new Hashtable<Integer, Date>();
		lHSPosition = new LinkedHashSet<Position>();
	}
	
	public void addPosition(Position position){
		lHSPosition.add(position);
	}
	
	public int getPositionSize(){
		return lHSPosition.size();
	}
	
	public Iterator<Position> getIteratorPosition(){
		return lHSPosition.iterator();
	}	
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setInfo(String info) {
		this.info = info;
	}
	
	public String getInfo() {
		return info;
	}
	
	public void setUriImage(String uriImage) {
		this.uriImage = uriImage;
	}
	
	public String getUriImage() {
		return uriImage;
	}
	
	public Bitmap getCoverImage(){
		Bitmap bmp = null;
		
		return bmp;
	}
	
	public void setEvent(int type, Date date){
		hEventDate.put(type, date);
	}
	
	public Date getEvent(int type){
		return hEventDate.get(type);
	}
	
	
	public boolean isHighlight() {
		return highlight;
	}
	
	public void setHighlight(boolean highlight) {
		this.highlight = highlight;
	}
	
	public void setTags(String tags){
		this.tags = tags;
	}
	
	public boolean containsTag(String tag){
		return (tags != null) && (tags.indexOf(tag) != -1);
	}
	
	public void setCategory(CategoryType category) {
		this.category = category;
	}
	
	public CategoryType getCategory() {
		return category;
	}
	
	@Override
	public void setFavorite(boolean fav) {
		this.favorite = fav;
	}
	
	@Override
	public boolean isFavorite(){
		return favorite;
	}
}
