package com.touchtechnologies.mall.dataobject;

public class Category implements Favoritable{
	private int id;
	private String name;
	private boolean favorite;
	
	enum CategoryType{
		FOOD, FASHION, KIDS, LIFESTYLE, TECH,
		WELLNESS, SPORT, HOME, BANKING, GEMS
	}
	 
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
		
	@Override
	public boolean isFavorite() {		
		return favorite;
	}
	
	@Override
	public void setFavorite(boolean fav) {
		this.favorite = fav;
	}
}
