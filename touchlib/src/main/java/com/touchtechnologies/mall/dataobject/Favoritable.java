package com.touchtechnologies.mall.dataobject;

public interface Favoritable {
	boolean isFavorite();
	void setFavorite(boolean fav);
}
