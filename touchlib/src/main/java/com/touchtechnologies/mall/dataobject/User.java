package com.touchtechnologies.mall.dataobject;

import java.util.Hashtable;
import java.util.Vector;

import android.graphics.Bitmap;

public class User {
	public static final int TYPE_PHONE_MOBILE = 0XF001;
	public static final int TYPE_EMAIL = 0XF002;
	public static final int TYPE_NAME = 0XE001;
	public static final int TYPE_PHOTO_URI = 0XC001;
	
	private Hashtable<Integer, String> hContacts;
	private boolean notificationEmail;
	private boolean notificationPush;
	private Vector<Shop> vFavShop;
	private Vector<Feed> vFavFeed;

	public User(){
		hContacts = new Hashtable<Integer, String>();
		vFavShop = new Vector<Shop>();
		vFavFeed = new Vector<Feed>();
	}
	
	public void setName(String name) {
		hContacts.put(TYPE_NAME, name);
	}
	
	
	public String getName() {
		return hContacts.get(TYPE_NAME);
	}
	
	public void setEmail(String email) {
		hContacts.put(TYPE_EMAIL, email);
	}
	
	public String getEmail() {
		return hContacts.get(TYPE_EMAIL);
	}
	
	public String getPhotoURI(){
		return hContacts.get(TYPE_PHOTO_URI);
	}
	
	public void setPhotoURI(String uri){
		hContacts.put(TYPE_PHOTO_URI, uri);
	}
	
	public Bitmap getPhoto(){
		Bitmap bmp = null;
				
		return bmp;		
	}
	
	public boolean isNotificationEmail() {
		return notificationEmail;
	}
	
	public boolean isNotificationPush() {
		return notificationPush;
	}
	
	public void setNotificationEmail(boolean notificationEmail) {
		this.notificationEmail = notificationEmail;
	}
	
	public void setNotificationPush(boolean notificationPush) {
		this.notificationPush = notificationPush;
	}
	
	
}
