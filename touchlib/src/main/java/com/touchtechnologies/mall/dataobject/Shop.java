package com.touchtechnologies.mall.dataobject;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Vector;

import com.touchtechnologies.location.dataobject.Position;
import com.touchtechnologies.mall.dataobject.Category.CategoryType;

public class Shop implements Favoritable{
	public static final int TYPE_PHONE_MOBILE = 0XF041;
	public static final int TYPE_PHONE_WORK = 0XF042;
	public static final int TYPE_PHONE_FAX = 0XF043;	
	public static final int TYPE_EMAIL = 0XF002;
	public static final int TYPE_PHOTO_URI = 0XC001;
	public static final int TYPE_GALLERY_URI = 0XC002;
	public static final int TYPE_WEB_URL = 0X44EB;
		
	private int id;
	private boolean favorite;
	
	private String info;
	private String name;
	private String openHourWeekday;
	private String openHourWeekend;
	private Vector<Position> vPosition;
	private LinkedHashSet<Promotion> linkedHSetPromotion;
	private CategoryType categoryType;
	private Category category;
	
	private String uriImageCover;
	private Vector<String> vUriGallery;	
	
	private Hashtable<Integer, String> hEntry;
	
	public Shop(){
		hEntry = new Hashtable<Integer, String>();
		vPosition = new Vector<Position>(1);
		vUriGallery = new Vector<String>();
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setUriImageCover(String uriCover) {
		this.uriImageCover = uriCover;
	}
	
	public String getUriImageCover() {
		return uriImageCover;
	}
	
	public int getGallerySize(){
		return vUriGallery.size();
	}
	
	public void addUriGallery(String uri){
		vUriGallery.add(uri);
	}
	
	public String getUriGallery(int index){
		return vUriGallery.elementAt(index);
	}
	
	public void setInfo(String info) {
		this.info = info;
	}
	
	public String getInfo() {
		return info;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setOpenHourWeekday(String openHourWeekday) {
		this.openHourWeekday = openHourWeekday;
	}
	
	public String getOpenHourWeekday() {
		return openHourWeekday;
	}
	
	public void setOpenHourWeekend(String openHourWeekend) {
		this.openHourWeekend = openHourWeekend;
	}
	
	public String getOpenHourWeekend() {
		return openHourWeekend;
	}
	
	
	public void setEntry(int type, String data){
		
	}
	
	public String getEntry(int type){
		return hEntry.get(type);
	}
	
	public void setPosition(Position pos){
		vPosition.removeAllElements();
		vPosition.add(pos);
	}
	
	public int getPositionSize(){
		return vPosition.size();
	}
	
	public Position getPosition(int index){
		return vPosition.elementAt(index);
	}
	
	public int getPromotionSize(){
		return linkedHSetPromotion.size();
	}
	
	public void addPromotion(Promotion promo){
		linkedHSetPromotion.add(promo);
	}
	
	public Iterator<Promotion> getIteratePromotion(){
		return linkedHSetPromotion.iterator();
	}
	
	public void setPhone(int type, String number){
		hEntry.put(type, number);
	}
	
	public String getPhone(int type){
		return hEntry.get(type);
	}
	
	public String getEmail(){
		return hEntry.get(TYPE_EMAIL);
	}
	
	public void setEmail(String email){
		hEntry.put(TYPE_EMAIL, email);
	}
	
	public void setWeb(String url){
		hEntry.put(TYPE_WEB_URL, url);
	}
	
	public String getWeb(){
		return hEntry.get(TYPE_WEB_URL);
	}
	
	public CategoryType getCategoryType() {
		return categoryType;
	}
	
	public void setCategoryType(CategoryType category) {
		this.categoryType = category;
	}
	
	public void setCategory(Category category) {
		this.category = category;
	}
	
	public Category getCategory() {
		return category;
	}
	
	@Override
	public void setFavorite(boolean fav) {
		
		this.favorite = fav;
	}
	
	@Override
	public boolean isFavorite(){
		return favorite;
	}
}
