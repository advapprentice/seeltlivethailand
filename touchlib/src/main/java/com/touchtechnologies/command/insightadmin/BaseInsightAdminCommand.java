package com.touchtechnologies.command.insightadmin;

import java.util.Enumeration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.crypto.CipherUtil;
import com.touchtechnologies.dataobject.insightadmin.LiveStreamConnection;
import com.touchtechnologies.dataobject.insightthailand.Comment;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.dataobject.reporter.LiveStreamSchedule;
import com.touchtechnologies.dataobject.reporter.Notification;
import com.touchtechnologies.telephony.TelephonyInfo;

public class BaseInsightAdminCommand extends Command{
	static String version;
	User user;
	LiveStreamSchedule schedule;
	LiveStreamConnection livestream;
	Notification notification;
	Comment comment;
	/**
	 * Create a reporter's command object.
	 * Default reporter's command parameter will be set.
	 * @param context
	 * @param number
	 */
	public BaseInsightAdminCommand(Context context, String number) {
		super(context, number, "BcILclihFSTbm3tGpfKfrbdW");
								
		//clear dirty paramaters
		hFields.clear();
		
		hFields.put(PARAMS.client, "touchinsightmobileapp");
		
		//set default param for TouchInsightThailand
		hFields.put(PARAMS.command, number);
		hFields.put(PARAMS.appName, "touchinsightmobileapp");
		
		if(version == null){
			try{
				PackageInfo pckInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES);
				version = pckInfo.versionName;
			}catch(Exception e){
				Log.e(LibConfig.LOGTAG, "Get version error", e);
			}			
		}		
		hFields.put(PARAMS.appVersion, version);
		
		hFields.put(PARAMS.OS, "Android");
		hFields.put(PARAMS.OSVersion, Build.VERSION.RELEASE);
		hFields.put(PARAMS.mobileAgent, Build.FINGERPRINT);
		hFields.put(PARAMS.deviceID, TelephonyInfo.getInfo(context, TelephonyInfo.TELEPHONY_INFO.IMEI));
		hFields.put(PARAMS.language, "TH");
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user){
		this.user = user;
	}
	public void setCommentcontent(Comment comment){
		this.comment = comment;
	}
	public void setLivestream(LiveStreamConnection livestream){
		this.livestream = livestream;
	}
	public void setLiveStreamSchedule(LiveStreamSchedule schedule){
		this.schedule = schedule;
	}
	/**
	 * Return a data in json form. 
	 * @return data attributes/ values (exclude "data" key)
	 */
	public JSONObject toJson() throws JSONException{		
		return new JSONObject(toString());
	}	
	
	@Override
	public String toString(){
		String content = null;
		
		JSONObject jCommandContainer = new JSONObject();
		PARAMS key;		
		try{
			//put ALL PARAMS values to json form
//			JSONObject jParams = new JSONObject();
			Enumeration<PARAMS> keys = hFields.keys();
			Object value;
			while(keys.hasMoreElements()){
				key = keys.nextElement();
				value = hFields.get(key);
				
				if(value instanceof String){
					jCommandContainer.put(key.toString(), value);
					
				}else if(value instanceof String[]){
					String[] values = (String[])value;
					
					JSONArray jArray = new JSONArray();
					for(int i=0; i<values.length; i++){
						jArray.put(values[i]);
					}
					
					jCommandContainer.put(key.toString(), jArray);
				}				
			}
						
			/*
			 * construct a JSON in form of
			 * 	{
  			 *	  "command": "command number",
  			 *	  "client": "app client",
  			 *	  "mask": "mask key",
  			 *	  "data": "encrypted data"
			 *	} 
			 */
			//command
			jCommandContainer.put(PARAMS.command.toString(), getNumber());			
			
			//client
			if(hFields.get(PARAMS.client) != null){
				jCommandContainer.put(PARAMS.client.toString(), hFields.get(PARAMS.client));
			}
			
			//"data" values
			JSONObject sibJson = this.toJson();
			if(sibJson != null){
				jCommandContainer.put(PARAMS.data.toString(), sibJson);

				Log.d(LibConfig.LOGTAG, "Reporter JSON -> " + jCommandContainer);
				
				//construct a JSON in form
				CipherUtil cipherUtil = CipherUtil.getInstance(this.key);
				content = cipherUtil.encrypt(jCommandContainer);
			}else{
				content = jCommandContainer.toString();				
			}
			
			
			Log.d(LibConfig.LOGTAG, "Post command -> "  + content);
		}catch(Exception e){
			e.printStackTrace();
			Log.d(LibConfig.LOGTAG, "Command toString", e);
		}		
		
		hashCode = content.hashCode();
		
		return content;
	}
}
