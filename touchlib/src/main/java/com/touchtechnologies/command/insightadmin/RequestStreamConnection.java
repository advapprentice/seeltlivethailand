package com.touchtechnologies.command.insightadmin;


import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;



public class RequestStreamConnection extends BaseInsightAdminCommand{
	public static final String COMMAND_NUMBER = "2810";	
	private JSONObject feedFilter;
	
	public RequestStreamConnection(Context context) {
		super(context,COMMAND_NUMBER);
		feedFilter = new JSONObject();
		
	}
	
	public void setProtocol(String protocol) {
		try{
		JSONObject jsonhas = new JSONObject();

		if(feedFilter.isNull("protocol")){
			jsonhas = new JSONObject();
			
		}else{
			jsonhas = feedFilter.getJSONObject("protocol");
		}
		
		feedFilter.put("protocol", protocol);
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}

	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();


	//	jsonlive.put("protocol", "RTSP");
		feedFilter.put("title",livestream.getTitle());
		feedFilter.put("note", livestream.getNote());
		
        json.put("streaming",feedFilter);
	
		
		return json;
	}
}
