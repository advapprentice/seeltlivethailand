package com.touchtechnologies.command;

import java.net.SocketTimeoutException;
import java.security.GeneralSecurityException;
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.crypto.CipherUtil;
import com.touchtechnologies.net.ServiceConnector;

public class Command {
	private Context context;
	protected Handler handler;
	public static final int SUCCESS = 0;
	public static final int FAILED  = 1;
	protected int hashCode;
	protected String key;
	
	public enum PARAMS{
		command, data, mask,
		email, password, fullname, mobileNumber,
		client,
		clientAgent,clientVersion,clientUDID,clientIMEI,clientIMSI,
		clientLanguage, status, site_id, message, referenceId, latitude, 
		longitude, distance, unit, limit, offset, sortby, providerType, providerTag, category, name,
		token, attachProviderListBy, newsType,
		/* Cloud/ Kiosk Parameters */
		Channel, Interface, Command, Version, IP, SiteID, Keycode, PosUsername, PosPassword,
		TransactionID, PackageID, NoAcc, Prefix, Postfix, MobileNo, Email, Fname, Lname, 
		IDCard, Passport, SendAccount, address, content,
		/* Politician Watcher */
		displayName, userID, groupID, groupName, groupDescription, groupPrivate, topicID, commentID, session,
		politicianID, politicianName,
		/* CAT Wifi */
		pin,ip, username,
		/* Thai citizen reporter*/
		appName, appVersion, OS, OSVersion, mobileAgent, deviceID, language,
		/* WiFi package */
		packageType, packageBase
	}
	
	private String number;
	protected Hashtable<PARAMS, Object> hFields;		
	
	public Command(Context context, String number, String key){
		this(context, number);
		
		this.key = key;
	}
	
	public Command(Context context, String number){
		this.context = context;
		this.number = number;
		
		hFields = new Hashtable<PARAMS, Object>();
		
		hFields.put(PARAMS.Command, number);
		hFields.put(PARAMS.clientLanguage, "EN");
		
		//Software version		
		ComponentName comp = new ComponentName(context, context.getClass());
	    try{
			PackageInfo pinfo = context.getPackageManager().getPackageInfo(comp.getPackageName(), 0);
			hFields.put(PARAMS.clientVersion, pinfo.versionName);
	    }catch(Exception e){
	    	Log.e(LibConfig.LOGTAG, "Get version", e);
	    }
	}	
	
	public Context getContext(){
		return context;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setId(String rid){
		hFields.put(PARAMS.referenceId, rid);
	}
	
	public void setName(String name){
		hFields.put(PARAMS.name, name);
	}
	
	public void setLat(Double lat){
		hFields.put(PARAMS.latitude, lat.toString());
	}
	
	public void setLng(Double lng){
		hFields.put(PARAMS.longitude, lng.toString());
	}
	
	public void setDistance(String dis){
		hFields.put(PARAMS.distance, dis);
	}
	
	public void setUnit(String unit){
		hFields.put(PARAMS.unit, unit);
	}
	
	public void setLimit(int limit){
		hFields.put(PARAMS.limit, "" + limit);
	}
	
	public void setOffset(int offset){
		hFields.put(PARAMS.offset, "" + offset);
	}
	
	public void setSiteID(String id){
		hFields.put(PARAMS.site_id, id);
	}
	
	public void setToken(String token){
		hFields.put(PARAMS.token, token);
	}
	
	public void setSort(String sort){
		hFields.put(PARAMS.sortby, sort);
	}
	
	public void setProviderType(String type){
		hFields.put(PARAMS.providerType, type);
	}
	
	public void setProviderTags(String[] tags){
		hFields.put(PARAMS.providerTag, tags);
	}
		
	public String getProviderType(){
		Object provider = hFields.get(PARAMS.providerType);
		return provider == null?"Unknown":provider.toString();
	}
	
	public void setProviderType(String[] type){
		hFields.put(PARAMS.providerType, type);
	}
	
	public void setCategory(String cate){
		hFields.put(PARAMS.category, cate);
	}
	
	public void setAttachList(String attachType){
		hFields.put(PARAMS.attachProviderListBy, attachType);
	}
	
	/**
	 * Set Command handler to handle a service response.
	 * @param handler handle a response; "what" is http response code and "obj" is JSONObject object or phrase string if http error occurred.
	 */
	public void setHandler(Handler handler){
		this.handler = handler;
	}
				
	public synchronized void httpResponse(Object response) throws GeneralSecurityException {
		Message message = new Message();

		try {
			if (response instanceof HttpResponse) {
				int responseCode = ((HttpResponse) response).getStatusLine().getStatusCode();
				message.what = responseCode;

				if (responseCode == HttpStatus.SC_OK) {
					JSONObject json = CipherUtil.getInstance(key).getJSONResponse((HttpResponse) response);

					int status = json.getInt(PARAMS.status.toString());
					if (status == 0) {
						message.what = SUCCESS;
						message.obj = json;
						
						ServiceConnector.cache(this, json);
						
					} else {
						message.what = FAILED;
						message.obj = json.getString(PARAMS.message.toString());
					}

				} else {
					message.obj = ((HttpResponse) response).getStatusLine()
							.getReasonPhrase();
				}

			} else if(response instanceof JSONObject){
				message.what = SUCCESS;
				message.obj = response;
				
			}else{
				message.what = FAILED;
				if(response instanceof SocketTimeoutException){
					message.obj = "Connection timed out";
				}else{
					message.obj = response;
				}
			}
		} catch (Exception e) {
			message.what = FAILED;
			message.obj = e.getMessage();
		}

		if (handler != null) {
			handler.sendMessage(message);
		}
	}
	
	@Override
	public String toString(){
		String content = null;
		
		JSONObject jCommandContainer = new JSONObject();
		PARAMS key;		
		try{
			//put ALL PARAMS values to json form
			JSONObject jParams = new JSONObject();
			Enumeration<PARAMS> keys = hFields.keys();
			Object value;
			while(keys.hasMoreElements()){
				key = keys.nextElement();
				value = hFields.get(key);
				
				if(value instanceof String){
					jParams.put(key.toString(), value);
					
				}else if(value instanceof String[]){
					String[] values = (String[])value;
					
					JSONArray jArray = new JSONArray();
					for(int i=0; i<values.length; i++){
						jArray.put(values[i]);
					}
					
					jParams.put(key.toString(), jArray);
				}				
			}
						
			/*
			 * construct a JSON in form of
			 * 	{
  			 *	  "command": "command number",
  			 *	  "client": "app client",
  			 *	  "mask": "mask key",
  			 *	  "data": "encrypted data"
			 *	} 
			 */
			//command
			jCommandContainer.put(PARAMS.command.toString(), number);			
			
			//client
			if(hFields.get(PARAMS.client) != null){
				jCommandContainer.put(PARAMS.client.toString(), hFields.get(PARAMS.client));
			}
			
			//put all parameters to "data"
			jCommandContainer.put(PARAMS.data.toString(), jParams);			
			Log.d(LibConfig.LOGTAG, "JSON -> " + jCommandContainer);
			
			//construct a JSON in form
			CipherUtil cipherUtil = CipherUtil.getInstance(this.key);
			content = cipherUtil.encrypt(jCommandContainer);
			
			Log.d(LibConfig.LOGTAG, "Post command -> "  + content);
		}catch(Exception e){
			Log.d(LibConfig.LOGTAG, "Command toString", e);
		}		
		
		hashCode = content.hashCode();
		
		return content;
	}
	
	public CipherUtil getCipherInstanceForCommand() throws GeneralSecurityException{
		return CipherUtil.getInstance(this.key);
	}
	
	@Override
	public int hashCode() {
		return hashCode;
	}
}
