package com.touchtechnologies.command.kiosk;

import android.content.Context;

public class GetInternetAccount extends BaseKioskCommand{
	
	public GetInternetAccount(Context context) {
		super(context, "9100");
	}

	public void setPackageID(String pid){
		hFields.put(PARAMS.PackageID, pid);
	}
	
	/**
	 * A number of account to create
	 * @param noAcc
	 */
	public void setNumberOfAccount(int noAcc){
		hFields.put(PARAMS.NoAcc, noAcc);
	}	
	
}
