package com.touchtechnologies.command.kiosk;

import java.security.GeneralSecurityException;
import java.util.Enumeration;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.os.Message;

import com.touchtechnologies.command.Command;
import com.touchtechnologies.crypto.CipherUtil;
import com.touchtechnologies.log.Log;
import com.touchtechnologies.net.ServiceConnector;

public class BaseKioskCommand extends Command{
	
	public BaseKioskCommand(Context context, String number) {
		super(context, number);
		
		hFields.put(PARAMS.Interface, "CloudPOS");
		hFields.put(PARAMS.Channel, "POS");
		hFields.put(PARAMS.Version, "1.0");
		
	}
	
	/**
	 * Set command version
	 * @param version
	 */
	public void setVersion(String version){
		hFields.put(PARAMS.Version, version);
	}

	public void setClientIPAddress(String ip){
		hFields.put(PARAMS.IP, ip == null?"":ip);
	}

	@Override
	public void setSiteID(String id) {
		hFields.put(PARAMS.SiteID, id);
	}
	
	public void setKeyCode(String code){
		hFields.put(PARAMS.Keycode, code);
	}
	
	public void setPOSUsername(String username){
		hFields.put(PARAMS.PosUsername, username);
	}
	
	public void setPOSPassword(String password){
		hFields.put(PARAMS.PosPassword, password);
	}
	
	@Override
	public String toString() {
		String content = null;
		
		JSONObject jObj = new JSONObject();
		PARAMS key;		
		try{
//			jObj.put(PARAMS.command.toString(), getNumber());
			
			if(hFields.get(PARAMS.client) != null){
				jObj.put(PARAMS.client.toString(), hFields.get(PARAMS.client));
			}
			
//			JSONObject jData = new JSONObject();
			Enumeration<PARAMS> keys = hFields.keys();
			Object value;
			while(keys.hasMoreElements()){
				key = keys.nextElement();
				value = hFields.get(key);
				
				if(value instanceof String){
					jObj.put(key.toString(), value);
					
				}else if(value instanceof String[]){
					String[] values = (String[])value;
					
					JSONArray jArray = new JSONArray();
					for(int i=0; i<values.length; i++){
						jArray.put(values[i]);
					}
					
					jObj.put(key.toString(), jArray);
				}				
			}
			
//			jObj.put(PARAMS.data.toString(), jData);			
//			Log.println("JSON -> " + jObj);
			
//			CipherUtil cipherUtil = CipherUtil.getInstance(this.key);
			content = jObj.toString();
			
			Log.println("Post command -> "  + content);
		}catch(Exception e){
			Log.log("Command toString", e);
		}		
		
		hashCode = content.hashCode();
		
		return content;
	}
	
	public synchronized void httpResponse(Object response) throws GeneralSecurityException {
		Message message = new Message();
		
		Log.println("httResponse: " + response);
		
		try {
			if (response instanceof HttpResponse) {
				int responseCode = ((HttpResponse) response).getStatusLine().getStatusCode();
				message.what = responseCode;
				
				
				if (responseCode == HttpStatus.SC_OK) {
					JSONObject json = CipherUtil.getInstance(key).getJSONResponse(((HttpResponse) response).getEntity().getContent());

					Log.println(json.toString());
					
					String status = json.getString(PARAMS.Command.toString());
					if (getNumber().equals(status)) {
						message.what = SUCCESS;
						message.obj = json;
						
//						ServiceConnector.cache(this, json);
						
					} else {
						message.what = FAILED;
						message.obj = json.getString("Message");
					}

				} else {
					message.obj = ((HttpResponse) response).getStatusLine().getReasonPhrase();
				}

			} else if(response instanceof JSONObject){
				message.what = SUCCESS;
				message.obj = response;
				
			}else{
				message.what = FAILED;
				message.obj = (response instanceof ConnectTimeoutException)?"Connect to mediator timed out":
					(response  instanceof Exception?((Exception)response).getClass().getSimpleName() + " " + ((Exception)response).getMessage():response);
			}
		} catch (Exception e) {
			message.what = FAILED;
			message.obj = e.getMessage();
			
			Log.log("Read json response ", e);
		}

		if (handler != null) {
			handler.sendMessage(message);
		}
	}
}
