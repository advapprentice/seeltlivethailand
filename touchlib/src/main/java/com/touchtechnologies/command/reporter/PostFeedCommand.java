package com.touchtechnologies.command.reporter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.touchtechnologies.dataobject.reporter.Feed;
import com.touchtechnologies.dataobject.reporter.TargetGroup;
import com.touchtechnologies.dataobject.reporter.User;

public class PostFeedCommand extends BaseReporterCommand{
	public static final String COMMAND_NUMBER = "6100";
	
	private JSONArray jsonMediaPost; 
	
	public PostFeedCommand(Context context) {
		super(context, COMMAND_NUMBER);
	}
	
	public void setMedias(JSONArray json){
		this.jsonMediaPost = json;
	}
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
		
		User userTmp = new User();
		userTmp.setSession(user.getSession());
		userTmp.setUserID(user.getUserID());
		
		json.put("user", userTmp.toJson());
				
		JSONObject jsonFeed = new JSONObject();
		jsonFeed.put("feedType",feed.getTypeKey());
		jsonFeed.put("feedTitle", feed.getTitle());
		jsonFeed.put("feedContent", feed.getContent());
		
		if(feed.getLatitude() != Double.MIN_VALUE){
			jsonFeed.put("latitude", feed.getLatitude());
		}
		if(feed.getLongitude() != Double.MIN_VALUE){
			jsonFeed.put("longitude", feed.getLongitude());
		}
		if(feed.getAnonymous()>=0){
			jsonFeed.put("anonymous", feed.getAnonymous() + "");
		}
		
		if(feed.getPublic() >= 0){
			jsonFeed.put("public", feed.getPublic() + "");
		}
		
		if(Feed.TYPE_COMPLAINT.equals(feed.getTypeKey())){
			if(feed.getTargetGroups() != null){
				JSONArray tmpGroup = new JSONArray();
				for(TargetGroup tg: feed.getTargetGroups()){
					tmpGroup.put(tg.toJson());
				}
				
				jsonFeed.put("targetGroup", tmpGroup);
			}
			
		}else if(Feed.TYPE_REPORTNEWS.equals(feed.getTypeKey())){
			if(feed.getFeedCategory() != null){
				jsonFeed.put("feedCategory", feed.getFeedCategory().getID());
			}
		}
		
		json.put("feed", jsonFeed);
		
		if(jsonMediaPost != null){
			json.put("medias", jsonMediaPost);
		}
		return json;
	}
}
