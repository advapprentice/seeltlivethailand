package com.touchtechnologies.command.reporter;

import java.util.Hashtable;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.dataobject.reporter.Feed;
import com.touchtechnologies.json.JSONUtil;

public class ListFeedsCommand extends BaseReporterCommand{
	public static final String COMMAND_NUMBER = "6600";	
	private JSONObject feedFilter;
	private Hashtable<String, String> hFilter;
	
	public ListFeedsCommand(Context context) {
		super(context, COMMAND_NUMBER);
		
		hFilter = new Hashtable<String, String>();
		feedFilter  = new JSONObject();
	}
	
	public void setDistance(int km){
		hFilter.put("distance", "" + km);
	}
	
	public void setLat(Double lat){
		hFilter.put("latitude", lat.toString());
	}
	
	public void setLng(Double lng){
		hFilter.put("longitude", lng.toString());
	}
	
//	public void setManageable(String yn){
//		hFilter.put("manageable", yn);
//	}
//	
	@Override
	public void setOffset(int offset) {
		try{
			hFilter.put("offset", "" + offset);
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, e.getMessage());
		}
	}
	
	@Override
	public void setLimit(int limit) {
		try{
			hFilter.put("limit", "" + limit);
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, e.getMessage());
		}
	}
	
	/**
	 * Add feed type filter
	 * @see Feed.TYPE_ 
	 * @param type
	 */
	public void addFeedType(String type){
		try{
			JSONArray jrFeedType = null;
			
			if(feedFilter.isNull("feedType")){
				jrFeedType = new JSONArray();
				
			}else{
				jrFeedType = feedFilter.getJSONArray("feedType");
			}
			
			jrFeedType.put(type);
						
			feedFilter.put("feedType", jrFeedType);
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
	}
	
	public void addFeedTarget(String targetID){
		String targetGroup = "targetGroup";
		String groupType = "groupType";
		
		try{
			JSONObject jsonTargetGroup = null;
			JSONArray jsonGroupIDs = null;
			
			if(feedFilter.isNull(targetGroup)){
				
				jsonTargetGroup= new JSONObject();
				
				jsonGroupIDs = new JSONArray();
				jsonTargetGroup.put(groupType, jsonGroupIDs);
				
			}else{
				jsonTargetGroup = feedFilter.getJSONObject(targetGroup);
				jsonGroupIDs = jsonTargetGroup.getJSONArray(groupType);
			}
			
			jsonGroupIDs.put(targetID);			
			jsonTargetGroup.put(groupType, jsonGroupIDs);
			
			feedFilter.put("targetGroup", jsonTargetGroup );
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed target error");
		}
	}
	
	/**
	 * Add category type/id to filter
	 * @param category id
	 */
	public void addFeedCategory(String category){
		try{
			JSONArray jrFeedCat = null;
			
			if(feedFilter.isNull("feedCategory")){
				jrFeedCat = new JSONArray();
				
			}else{
				jrFeedCat = feedFilter.getJSONArray("feedCategory");
			}
			
			jrFeedCat.put(category);
			feedFilter.put("feedCategory", jrFeedCat);
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
	}
	
	/**
	 * Add feed status to filter
	 * @see Feed.STATUS_
	 * @param status
	 */
	public void addFeedStatus(String status){
		try{
			JSONArray jrFeedStatus = null;
			
			if(feedFilter.isNull("feedStatus")){
				jrFeedStatus = new JSONArray();
				
			}else{
				jrFeedStatus = feedFilter.getJSONArray("feedStatus");
			}
			
			jrFeedStatus.put(status);
			feedFilter.put("feedStatus", jrFeedStatus);
			
		
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}
	
	public void setManageable(String manageable){
		try{
		JSONObject json = new JSONObject();
		
		if(feedFilter.isNull("manageable")){
			json = new JSONObject();
			
		}else{
			json = feedFilter.getJSONObject("manageable");
		}
		
		json.put(manageable, false);
		feedFilter.put("manageable", manageable);
		
	
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
		if(user != null){
			json.put("user", user.toJson());
		}
			
		Set<String> keys = hFilter.keySet();
		for(String key: keys){
			json.put(key, hFilter.get(key));
		}
		
		json.put("feed", feedFilter);
				
		return json;
	}
}
