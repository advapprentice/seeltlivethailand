package com.touchtechnologies.command.reporter;

import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.dataobject.reporter.Comment;
import com.touchtechnologies.dataobject.reporter.User;

import android.content.Context;
import android.util.Log;

public class ListStreamingSchedule extends BaseReporterCommand{
	public static final String COMMAND_NUMBER = "2400";	
	private JSONObject feedFilter = new JSONObject();

	public ListStreamingSchedule(Context context) {
		super(context, COMMAND_NUMBER);		
	}
	
	@Override
	public void setOffset(int offset) {
		try{
			feedFilter.put("offset", offset);
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, e.getMessage());
		}
	}
	
	@Override
	public void setLimit(int limit) {
		try{
			feedFilter.put("limit", limit);
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, e.getMessage());
		}
	}
	
	public void setStreamable(String isstreamable) {
		try{
		JSONObject jsonlive = new JSONObject();

		if(feedFilter.isNull("isStreamable")){
			jsonlive = new JSONObject();
			
		}else{
			jsonlive = feedFilter.getJSONObject("isStreamable");
		}
			
		jsonlive.put("isStreamable", isstreamable);
		feedFilter.put("streamingSchedule", jsonlive);
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}
	
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
		if(user != null){
			
			User userTmp = new User();
			userTmp.setSession(user.getSession());
			
			json.put("user", userTmp.toJson());
			
			
		}
			
		Iterator<String> keys = feedFilter.keys();
		String key;
		while(keys.hasNext()){
			key = keys.next();
			json.put(key, feedFilter.get(key));
		}
				
		return json;
	}
}
