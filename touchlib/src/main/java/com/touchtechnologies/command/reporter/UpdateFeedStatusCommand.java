package com.touchtechnologies.command.reporter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.touchtechnologies.dataobject.reporter.Feed;
import com.touchtechnologies.dataobject.reporter.TargetGroup;
import com.touchtechnologies.dataobject.reporter.User;

public class UpdateFeedStatusCommand extends BaseReporterCommand{
	private ArrayList<Feed> arrayListFeeds;
	private ArrayList<TargetGroup> targetGroups;
	
	public UpdateFeedStatusCommand(Context context) {
		super(context, "6220");
		
		arrayListFeeds = new ArrayList<Feed>();
		
	}
	
	/**
	 * Add feed to change status
	 * @param feed
	 */
	public void addUpdatedFeed(Feed feed){
		arrayListFeeds.add(feed);
	}
	
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
		
		User userTmp = new User();
		userTmp.setSession(user.getSession());
		userTmp.setUserID(user.getUserID());
		userTmp.setUsername(user.getUsername());
		userTmp.setEmail(user.getEmail());
		
		json.put("user", userTmp.toJson());
		
		
		JSONArray jArrayFeed = new JSONArray();
		for(Feed feed: arrayListFeeds){
			JSONObject jsonFeed = new JSONObject();
			jsonFeed.put("feedID", feed.getId());
			jsonFeed.put("feedStatus", feed.getStatus());
			
			//check targer group
			//set target group
			ArrayList<TargetGroup> targetGroups = feed.targetGroups;
			if(targetGroups != null){
				JSONObject jsonFeedAssignee = new JSONObject();
				JSONArray jArrayTargetGruop = new JSONArray();
				for(TargetGroup targetGroup: targetGroups){
					if(targetGroup.getMemberCount() > 0){
						JSONObject jsonTargetGroup = new JSONObject();
						jsonTargetGroup.put("groupType", targetGroup.getGroupTypeID());
						
						
						List<User> users = targetGroup.getMemberUsers();
						
						JSONArray jArrayUsers = new JSONArray();
						for (User user : users) {
							if (targetGroup.getMemberCount() > 0) {

								JSONObject objusers = new JSONObject();

								jsonTargetGroup.put("users", jArrayUsers);

								objusers.put("username", user.getUsername());

								jArrayUsers.put(objusers);
							}
						}
						jArrayTargetGruop.put(jsonTargetGroup);
				
					}
				}
				jsonFeedAssignee.put("targetGroup", jArrayTargetGruop);
				jsonFeed.put("feedAssignee", jsonFeedAssignee);
			}
			jArrayFeed.put(jsonFeed);
		}
		
		json.put("feed", jArrayFeed);
		
		return json;
	}
}
