package com.touchtechnologies.command.reporter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.touchtechnologies.dataobject.reporter.Comment;
import com.touchtechnologies.dataobject.reporter.User;

public class PostCommentCommand extends BaseReporterCommand{
	private Comment comment;
	private JSONArray jsonMediaPost; 
	

	public PostCommentCommand(Context context) {
		super(context, "8100");
	}
	
	public void setComment(Comment comment) {
		this.comment = comment;
	}
	
	public void setMedias(JSONArray json){
		this.jsonMediaPost = json;
	}
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
		
		User userTmp = new User();
		userTmp.setSession(user.getSession());
		userTmp.setUserID(user.getUserID());
		
		json.put("user", userTmp.toJson());
		
		JSONObject jsonFeedSimple = new JSONObject();
		jsonFeedSimple.put("feedID", feed.getId());
		jsonFeedSimple.put("feedType", feed.getTypeKey());
		
		json.put("feed", jsonFeedSimple);
		
		Comment commentTmp = new Comment();
		commentTmp.setCommentContent(comment.getCommentContent());
		
		json.put("comment", commentTmp.toJson());
		
		if(jsonMediaPost != null){
			json.put("medias", jsonMediaPost);
		}
		
		return json;
	}
}
