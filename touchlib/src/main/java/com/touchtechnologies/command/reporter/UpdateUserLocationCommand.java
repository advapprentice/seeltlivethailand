package com.touchtechnologies.command.reporter;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.dataobject.reporter.User;

import android.content.Context;

public class UpdateUserLocationCommand extends BaseReporterCommand{
	
	public UpdateUserLocationCommand(Context context) {
		super(context, "4240");
	}

	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
		
		User userTmp = new User();
		userTmp.setSession(user.getSession());
		userTmp.setUserID(user.getUserID());
		userTmp.setUsername(user.getUsername());
		userTmp.setLatitude(user.getLatitude());
		userTmp.setLongitude(user.getLongitude());
		
		json.put("user", userTmp.toJson());
		
		return json;
	}
}
