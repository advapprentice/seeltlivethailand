package com.touchtechnologies.command.reporter;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.touchtechnologies.dataobject.reporter.Feed;
import com.touchtechnologies.dataobject.reporter.User;

public class GetFeedDetailsCommand extends BaseReporterCommand{

	public GetFeedDetailsCommand(Context context) {
		super(context, "6800");
	}
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
		
		if(user != null){
			User userTmp = new User();
			userTmp.setSession(user.getSession());
			userTmp.setUserID(user.getUserID());
			json.put("user", userTmp.toJson());			
		}
		
		Feed feedTmp = new Feed();
		feedTmp.setId(feed.getId());
		feedTmp.setTypeKey(feed.getTypeKey());
		
		json.put("feed", feedTmp.toJson());
		
		return json;
	}
}
