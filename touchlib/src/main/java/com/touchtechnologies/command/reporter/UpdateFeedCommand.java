package com.touchtechnologies.command.reporter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.dataobject.reporter.TargetGroup;
import com.touchtechnologies.dataobject.reporter.User;

import android.content.Context;

public class UpdateFeedCommand extends BaseReporterCommand{
	public static final String COMMAND_NUMBER = "6200";
	public static final String ACTION_DELETE = "delete";
	public static final String ACTION_UPDATE = "update";
	public static final String ACTION_ABANDON = "abandon";
	
	/**
	 * Update options: delete or update
	 */
	protected String action;
	
	private JSONArray jsonMediaPost; 
	private JSONArray jsonMediaDelete; 
	
	public UpdateFeedCommand(Context context, String action) {
		super(context, COMMAND_NUMBER);
		
		this.action = action;
	}
	
	/**
	 * Set media information to be posted:
	 * 
	 *  [{
   	 *		 "thumbnailFilename": "image filename with extension",
   	 *		 "mediaFilename": "filename with extension",
   	 *		 "latitude": "latitude",
   	 *		 "longitude": "longitude"
   	 *	 }]
	 * @param json 
	 */
	public void setMediasPost(JSONArray json){
		this.jsonMediaPost = json;
	}
	
	/**
	 * Set media information to be deleted:
	 * 
	 *   [{"mediaID": "media ID"}]
	 *
	 * @param json 
	 */
	public void setMediasDelete(JSONArray json){
		this.jsonMediaDelete = json;
	}	
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject jsonCommand = new JSONObject();
		
		jsonCommand.put("action", action);
		
		User userTmp = new User();
		userTmp.setUserID(user.getUserID());
		userTmp.setSession(user.getSession());
		
		jsonCommand.put("user", userTmp.toJson());
		
		JSONObject jsonFeed = new JSONObject();
		jsonFeed.put("feedID", feed.getId());
		jsonFeed.put("feedType",feed.getTypeKey());
		jsonFeed.put("feedTitle", feed.getTitle());
		jsonFeed.put("feedContent", feed.getContent());
		if(feed.getFeedCategory() != null){
			jsonFeed.put("feedCategory", feed.getFeedCategory().getID());
		}
		
		if(feed.getTargetGroups() != null){
			JSONArray tmpGroup = new JSONArray();
			for(TargetGroup tg: feed.getTargetGroups()){
				tmpGroup.put(tg.toJson());
			}
			
			jsonFeed.put("targetGroup", tmpGroup);
		}
		
		jsonCommand.put("feed", jsonFeed);
		
		if(jsonMediaPost != null){
			jsonCommand.put("medias", jsonMediaPost);
		}
		
		if(jsonMediaDelete != null){
			jsonCommand.put("deleteMedias", jsonMediaDelete);
		}
		
		return jsonCommand;
	}
}
