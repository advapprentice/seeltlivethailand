package com.touchtechnologies.command.reporter;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.dataobject.reporter.User;

import android.content.Context;

public class UpdateRegisteredPushCommand extends BaseReporterCommand{
	private String registeredID;
	
	public UpdateRegisteredPushCommand(Context context) {
		super(context, "4260");
	}
	
	public void setPushID(String registeredID){
		this.registeredID = registeredID;
	}
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
		
		User userTmp = new User();
		userTmp.setSession(user.getSession());
		
		JSONObject jsonUser = userTmp.toJson();
		jsonUser.put("deviceToken", registeredID);
		
		json.put("user", jsonUser);
		
		
		return json;
	}
}
