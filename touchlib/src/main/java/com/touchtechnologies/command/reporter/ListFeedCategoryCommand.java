package com.touchtechnologies.command.reporter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

/**
 * News category
 * @author MEe
 *
 */
public class ListFeedCategoryCommand extends BaseReporterCommand{
	public static final String COMMAND_NUMBER = "9610";

	public ListFeedCategoryCommand(Context context) {
		super(context, COMMAND_NUMBER);
	}
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
		
		JSONArray feedTypes = new JSONArray();
		feedTypes.put(2);
		
		json.put("feed", feedTypes);
		
		return json;
	}
}
