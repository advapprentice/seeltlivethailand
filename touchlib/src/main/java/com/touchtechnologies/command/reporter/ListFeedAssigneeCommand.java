package com.touchtechnologies.command.reporter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.touchtechnologies.dataobject.reporter.Feed;
import com.touchtechnologies.dataobject.reporter.TargetGroup;
import com.touchtechnologies.dataobject.reporter.User;

public class ListFeedAssigneeCommand extends BaseReporterCommand{
	public static final String COMMAND_NUMBER = "6840";	
	private ArrayList<Feed> arrayListFeeds;
	private ArrayList<TargetGroup> targetGroups;
	
	public ListFeedAssigneeCommand(Context context) {
		super(context,COMMAND_NUMBER);
		
		arrayListFeeds = new ArrayList<Feed>();
		
	}
	
	/**
	 * Add feed to change status
	 * @param feed
	 */
	public void addUpdatedFeed(Feed feed){
		arrayListFeeds.add(feed);
	}
	
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
		
		User userTmp = new User();
		userTmp.setSession(user.getSession());
	
		
		json.put("user", userTmp.toJson());
		
		JSONObject jsonFeed = new JSONObject();
		jsonFeed.put("feedID", feed.getId());
	
        json.put("feed",jsonFeed);
		
		return json;
	}
}
