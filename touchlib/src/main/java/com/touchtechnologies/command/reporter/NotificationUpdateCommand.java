package com.touchtechnologies.command.reporter;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.dataobject.reporter.Notification;
import com.touchtechnologies.dataobject.reporter.User;

public class NotificationUpdateCommand extends BaseReporterCommand{
	public static final String COMMAND_NUMBER = "4520";	
	public static final String STATUS_READ = "Read";
	public static final String STATUS_DELETE = "Delete";
	public static final String STATUS_CELAR_ALL = "ClearAllUnread";
	
	private ArrayList<Notification> notifications;
	
	private String status;
	
	public NotificationUpdateCommand(Context context) {
		super(context, COMMAND_NUMBER);	
	}
	
	
	public void setNotifyStatus(String notifyStatus){
		this.status = notifyStatus;
	}
	
	public void setNotifications(ArrayList<Notification> notifications) {
		this.notifications = notifications;
	}
	
//	public void setNotifications(ArrayList<Notification> notifications){
//		this.notifications = notifications;
//		try{
//			JSONArray jrNotification= null;
//			if(notifyCount.isNull("notificationID")){
//				jrNotification= new JSONArray();
//				
//			}else{
//				jrNotification= notifyCount.getJSONArray("notificationID");
//			}
//			
//			jrNotification.put(notificationID);
//			notifyCount.put("notificationID", jrNotification);
//			
//		
//			
//		}catch(Exception e){
//			Log.e(LibConfig.LOGTAG, "add feed type error");
//		}
//		
//	}

	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
		if(user != null){
			 
			User userTmp = new User();
			userTmp.setSession(user.getSession());
			userTmp.setUserGroup(user.getUserGroup());
			
			json.put("user", userTmp.toJson());
	
		}
	
		//TODO insert notification ID
//		"notification" : {
//            "notifyStatus": "Read | Delete | ClearAllUnread",
//            "notificationID": "notification ID"
//        } 
		
		JSONObject jsonNotic = new JSONObject();
	
		
		try{
			//set status
			jsonNotic.put("notifyStatus", status);
		
			//set IDs
			JSONArray jsonIDs = new JSONArray();
			for(Notification notic:notifications){
				jsonIDs.put(notic.getNotificationID());
			}
			
			jsonNotic.put("notificationID", jsonIDs);
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
			json.put("notification", jsonNotic);	
			
		
		return json;
	}
	
}
