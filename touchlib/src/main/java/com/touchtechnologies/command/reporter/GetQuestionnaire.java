package com.touchtechnologies.command.reporter;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.dataobject.reporter.User;

import android.content.Context;

public class GetQuestionnaire extends BaseReporterCommand{

	public GetQuestionnaire(Context context) {
		super(context, "3800");
	}
	
	@Override
	public JSONObject toJson() throws JSONException {
		User userTmp = new User();
		userTmp.setSession(user.getSession());
		
		JSONObject questionnaire = new JSONObject();
		questionnaire.put("questionnaireID", "");
		questionnaire.put("contents", "");
		
		
		
		JSONObject json = new JSONObject();
		json.put("user", userTmp.toJson());
		json.put("questionnaire", questionnaire);
				
		return json;
	}
}
