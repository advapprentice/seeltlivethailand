package com.touchtechnologies.command.reporter;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class ListTopics extends BaseReporterCommand{

	public ListTopics(Context context) {
		super(context, "7610");
	}
	
	@Override
	public JSONObject toJson() throws JSONException {
		return new JSONObject();
	}
}
