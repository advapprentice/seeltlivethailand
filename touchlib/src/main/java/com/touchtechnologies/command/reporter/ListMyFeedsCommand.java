package com.touchtechnologies.command.reporter;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.dataobject.reporter.User;

import android.content.Context;

public class ListMyFeedsCommand extends BaseReporterCommand{
	public static final String COMMAND_NUMBER = "6400";

	public ListMyFeedsCommand(Context context) {
		super(context, COMMAND_NUMBER);
	}
	
	@Override
	public JSONObject toJson() throws JSONException{
		JSONObject jsonUser = new JSONObject();
		
		User userTmp = new User();
		userTmp.setSession(user.getSession());
		userTmp.setUserID(user.getUserID());
		
		jsonUser.put("user", userTmp.toJson());
		
		return jsonUser;
	}
}
