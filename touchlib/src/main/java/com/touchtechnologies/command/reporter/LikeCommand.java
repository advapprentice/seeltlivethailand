package com.touchtechnologies.command.reporter;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.dataobject.reporter.Feed;
import com.touchtechnologies.dataobject.reporter.User;

import android.content.Context;

public class LikeCommand extends BaseReporterCommand{
	public static final String LIKE =  "like";
	public static final String UNLIKE =  "unlike";
	public static final String DISLIKE =  "dislike";
	public static final String UNDISLIKE =  "unDislike";
	public static final String FOLLOW =  "follow";
	public static final String UNFOLLOW =  "unfollow";
	
	private String action;
	
	public LikeCommand(Context context) {
		super(context, "6700");
	}
	
	/**
	 * Set like, unlike
	 * @param like
	 */
	public void setLike(boolean like){
		action = like?LIKE:UNLIKE;
	}
	
	public String getAction() {
		return action;
	}
	
	/**
	 * Set dislike, unDislike
	 * @param dislike 
	 */
	public void setDislike(boolean dislike){
		action = dislike?DISLIKE:UNDISLIKE;
	}

	/**
	 * Set follow, unfollow
	 * @param follow
	 */
	public void setFollow(boolean follow){
		action = follow?FOLLOW:UNFOLLOW;
	}
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json  = new JSONObject();
		
		json.put("action", action);
		
		User userTmp = new User();
		userTmp.setSession(user.getSession());
		userTmp.setUserID(user.getUserID());
		json.put("user", user.toJson());
		
		Feed feedTmp = new Feed();
		feedTmp.setId(feed.getId());
		feedTmp.setTypeKey(feed.getTypeKey());
		
		json.put("feed", feedTmp.toJson());
		
		return json;
	}
	
}
