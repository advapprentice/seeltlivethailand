package com.touchtechnologies.command.reporter;

import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.dataobject.reporter.Comment;
import com.touchtechnologies.dataobject.reporter.User;

import android.app.Notification;
import android.content.Context;
import android.util.Log;

public class ListNotificationCountCommand extends BaseReporterCommand{
	public static final String COMMAND_NUMBER = "4510";	
	private JSONObject feedFilter = new JSONObject();
	private JSONObject notifyCount = new JSONObject();
	public ListNotificationCountCommand(Context context) {
		super(context, COMMAND_NUMBER);		
	}
	
	
	public void setNotifyStatus(String notifyStatus){
		try{
			JSONArray jrNotification= null;
			if(notifyCount.isNull("notifyStatus")){
				jrNotification= new JSONArray();
				
			}else{
				jrNotification= notifyCount.getJSONArray("notifyStatus");
			}
			
			jrNotification.put(notifyStatus);
			notifyCount.put("notifyStatus", jrNotification);
			
		
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}
	
	public void setNotifyEvent(String notifyEvent){
		try{
			JSONArray jrNotification= null;
			if(notifyCount.isNull("notifyEvent")){
				jrNotification= new JSONArray();
				
			}else{
				jrNotification= notifyCount.getJSONArray("notifyEvent");
			}
			
			jrNotification.put(notifyEvent);
			notifyCount.put("notifyEvent", jrNotification);
			
		
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}
	
	
	
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
		if(user != null){
			 
			User userTmp = new User();
			userTmp.setSession(user.getSession());
			userTmp.setUserGroup(user.getUserGroup());
			
			json.put("user", userTmp.toJson());
		

	
			
		}
			
		Iterator<String> keys = feedFilter.keys();
		String key;
		while(keys.hasNext()){
			key = keys.next();
			json.put(key, feedFilter.get(key));
		}
			
		json.put("notification", notifyCount);	
		
		
		return json;
	}
}
