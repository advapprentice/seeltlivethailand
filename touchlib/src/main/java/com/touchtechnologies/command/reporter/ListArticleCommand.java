package com.touchtechnologies.command.reporter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.dataobject.reporter.Article;
import com.touchtechnologies.dataobject.reporter.Feed;
import com.touchtechnologies.dataobject.reporter.TargetGroup;
import com.touchtechnologies.dataobject.reporter.Topic;
import com.touchtechnologies.dataobject.reporter.User;
import com.touchtechnologies.dataobject.reporter.Guru;;
public class ListArticleCommand extends BaseReporterCommand{
	public static final String COMMAND_NUMBER = "7600";	
	private ArrayList<Topic> arrayTopics;
	private JSONObject feedFilter;
	public ListArticleCommand(Context context) {
		super(context,COMMAND_NUMBER);
		arrayTopics = new ArrayList<Topic>();
	}
	
	

	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
//		
		User userTmp = new User();
	
		//	userTmp.setSession(user.getSession());
		//	
//		
		json.put("user", userTmp.toJson());
		
		JSONObject jsonTopic = new JSONObject();
		jsonTopic.put("topicID", topic.getId());
		
        json.put("article",jsonTopic);
	
		
		return json;
	}
}
