package com.touchtechnologies.command.reporter;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.dataobject.reporter.Feed;
import com.touchtechnologies.dataobject.reporter.User;

import android.content.Context;

public class DeleteFeedCommand extends UpdateFeedCommand{
	
	public DeleteFeedCommand(Context context, String action) {
		super(context, action);
	}
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
		
		if(user != null){
			User userTmp = new User();
			userTmp.setSession(user.getSession());
			userTmp.setUserID(user.getUserID());
			
			json.put("user", userTmp.toJson());
		}
		
		json.put("action", action);
		
		Feed feedTmp = new Feed();
		feedTmp.setId(feed.getId());
		
		json.put("feed", feedTmp.toJson());
		
		return json;
	}
}
