package com.touchtechnologies.command.reporter;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.dataobject.reporter.Topic;
import com.touchtechnologies.dataobject.reporter.User;

import android.content.Context;

public class ListArticle extends BaseReporterCommand{
	private Topic topic;

	public ListArticle(Context context) {
		super(context, "7600");
	}
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
		
		User tmp = new User();
		tmp.setSession(user.getSession());
		
		JSONObject jTopic = new JSONObject();
		jTopic.put("topicID", topic.getId());
		
		//command
		json.put("user", tmp);
		json.put("article", jTopic);
		
		return json;
	}
}
