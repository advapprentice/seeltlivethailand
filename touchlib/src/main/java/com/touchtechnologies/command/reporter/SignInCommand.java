package com.touchtechnologies.command.reporter;


import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class SignInCommand extends BaseReporterCommand{
	public static final String COMMAND_NUMBER = "4000";
	
	public SignInCommand(Context context) {
		super(context, COMMAND_NUMBER);
	} 
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
		json.put("user", user.toJson());
		return json;
	}
}
