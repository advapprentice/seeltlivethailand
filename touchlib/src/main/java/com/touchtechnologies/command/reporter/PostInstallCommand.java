package com.touchtechnologies.command.reporter;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class PostInstallCommand extends BaseReporterCommand{

	public PostInstallCommand(Context context) {
		super(context, "1000");
	}
	
	@Override
	public JSONObject toJson() throws JSONException {
		return new JSONObject();
	}
}
