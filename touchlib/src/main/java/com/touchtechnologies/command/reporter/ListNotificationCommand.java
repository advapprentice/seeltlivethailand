package com.touchtechnologies.command.reporter;

import java.util.Hashtable;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.dataobject.reporter.Comment;
import com.touchtechnologies.dataobject.reporter.User;

import android.app.Notification;
import android.content.Context;
import android.util.Log;

public class ListNotificationCommand extends BaseReporterCommand{
	public static final String COMMAND_NUMBER = "4500";	
	private JSONObject feedFilter = new JSONObject();
	private JSONObject notice = new JSONObject();
	private Hashtable<String, String> hFilter;
	public ListNotificationCommand(Context context) {
		super(context, COMMAND_NUMBER);		
	}
	
	
	
	@Override
	public void setOffset(int offset) {
		try{
			feedFilter.put("offset", "" + offset);
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, e.getMessage());
		}
	}
	
	@Override
	public void setLimit(int limit) {
		try{
			feedFilter.put("limit", "" + limit);
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, e.getMessage());
		}
	}
		

	
	public void addNotification(String status){
		try{
			JSONArray jrNotification= null;
			if(feedFilter.isNull("notification")){
				jrNotification= new JSONArray();
				
			}else{
				jrNotification= feedFilter.getJSONArray("notification");
			}
			
			jrNotification.put(status);
			feedFilter.put("notification", jrNotification);
			
		
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}

	public void setNotifyEvent(String notifyEvent){
		try{
			JSONArray jrNotification= null;
			if(notice.isNull("notifyEvent")){
				jrNotification= new JSONArray();
				
			}else{
				jrNotification= notice.getJSONArray("notifyEvent");
			}
			
			jrNotification.put(notifyEvent);
			notice.put("notifyEvent", jrNotification);
			
		
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}

	
	public void setNotifyStatus(String notifyStatus){
		try{
			JSONArray jrNotification= null;
			if(notice.isNull("notifyStatus")){
				jrNotification= new JSONArray();
				
			}else{
				jrNotification= feedFilter.getJSONArray("notifyStatus");
			}
			
			jrNotification.put(notifyStatus);
			notice.put("notifyStatus", jrNotification);
			
		
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}
	
	
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
		if(user != null){			
			User userTmp = new User();
			userTmp.setSession(user.getSession());
			userTmp.setUserGroup(user.getUserGroup());
			
			json.put("user", userTmp.toJson());			
		}
			
	
			
		
		
		
		Iterator<String> keys = feedFilter.keys();
		String key;
		while(keys.hasNext()){
			key = keys.next();
			json.put(key, feedFilter.get(key));
		}
		
	
	//	JSONArray notice = new JSONArray();
		
		
		
		json.put("notification", notice);	

				
		return json;
	}
}
