package com.touchtechnologies.command.reporter;

import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.dataobject.reporter.Comment;
import com.touchtechnologies.dataobject.reporter.User;

import android.content.Context;
import android.util.Log;

public class ListTargetGroupAdminUser extends BaseReporterCommand{
	public static final String COMMAND_NUMBER = "4620";	
	private JSONObject feedFilter = new JSONObject();

	public ListTargetGroupAdminUser(Context context) {
		super(context, COMMAND_NUMBER);		
	}
	

	
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
		if(user != null){
			
			User userTmp = new User();
			userTmp.setSession(user.getSession());
			userTmp.setUserID(user.getUserID());
			userTmp.setUsername(user.getUsername());
			userTmp.setEmail(user.getEmail());
			userTmp.setUserGroup(user.getUserGroup());
			
			json.put("user", userTmp.toJson());
			
			
		}
			
		Iterator<String> keys = feedFilter.keys();
		String key;
		while(keys.hasNext()){
			key = keys.next();
			json.put(key, feedFilter.get(key));
		}
				
		return json;
	}
}
