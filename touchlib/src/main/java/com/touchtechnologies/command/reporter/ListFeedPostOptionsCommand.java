package com.touchtechnologies.command.reporter;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

/**
 * Report to, complaint
 * @author MEe
 *
 */
public class ListFeedPostOptionsCommand extends BaseReporterCommand{

	public static final String COMMAND_NUMBER = "9600";
	
	public ListFeedPostOptionsCommand(Context context) {
		super(context, COMMAND_NUMBER);
	}
	
	@Override
	public JSONObject toJson() throws JSONException {
		return new JSONObject();
	}
	
}
