package com.touchtechnologies.command.reporter;


import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.dataobject.reporter.Article;
import com.touchtechnologies.dataobject.reporter.Feed;
import com.touchtechnologies.dataobject.reporter.TargetGroup;
import com.touchtechnologies.dataobject.reporter.Topic;
import com.touchtechnologies.dataobject.reporter.User;
import com.touchtechnologies.dataobject.reporter.Guru;;


public class ListStremingChannelCommand extends BaseReporterCommand{
	public static final String COMMAND_NUMBER = "2600";	
	private JSONObject feedFilter;
	public ListStremingChannelCommand(Context context) {
		super(context,COMMAND_NUMBER);
		feedFilter  = new JSONObject();
	}
	
	public void setDisplayStatus(String displaystatus) {
		try{
		JSONObject jsonlive = new JSONObject();

		if(feedFilter.isNull("displayStatus")){
			jsonlive = new JSONObject();
			
		}else{
			jsonlive = feedFilter.getJSONObject("displayStatus");
		}
			
		jsonlive.put("displayStatus", displaystatus);
		feedFilter.put("streaming", jsonlive);
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}
	public void setHasStreaming(String hasStreaming) {
		try{
		JSONObject jsonhas = new JSONObject();

		if(feedFilter.isNull("hasStreaming")){
			jsonhas = new JSONObject();
			
		}else{
			jsonhas = feedFilter.getJSONObject("hasStreaming");
		}
		
		feedFilter.put("hasStreaming", hasStreaming);
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}
		
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
//	
//		JSONObject jsonSchedule = new JSONObject();
//		jsonSchedule.put("hasStreaming", "Y");

//		JSONObject jsonlive = new JSONObject();
//		jsonlive.put("displayStatus", "Display");
//		feedFilter.put("streaming", jsonlive);
//		json.put("streamingChannel",jsonSchedule);
		
//		feedFilter.put("hasStreaming", "Y");
		
        json.put("streamingChannel",feedFilter);
        

//		
		return json;
	}
}
