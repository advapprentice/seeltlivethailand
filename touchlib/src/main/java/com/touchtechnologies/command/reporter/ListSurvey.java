package com.touchtechnologies.command.reporter;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.dataobject.reporter.User;

import android.content.Context;

/**
 *  "startDatetime": "Start Datetime",
 *  "endDatetime": "End Datetime",
 *	"isAvailable": "Y | N",
 *	"isAllowAnswer": "Y | N",
 *	"isAllowViewReport": "Y | N",
 *	"isAnswered": "Y | N"
 * 
 * @author MEe
 *
 */
public class ListSurvey extends BaseReporterCommand{
	private boolean isAvailable;
	
	public ListSurvey(Context context) {
		super(context, "3600");
	}
	
	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	
	public boolean isAvailable() {
		return isAvailable;
	}
	
	@Override
	public JSONObject toJson() throws JSONException {
		
		JSONObject questionnaire = new JSONObject();
		//questionnaire.put("startDatetime", "");
		//questionnaire.put("endDatetime", "");
		questionnaire.put("isAvailable", isAvailable?"Y":"N");
		questionnaire.put("isAllowAnswer","Y");
//		questionnaire.put("isAllowViewReport", "Y");
//		questionnaire.put("isAnswered", "");
		
		JSONObject json = new JSONObject();
		
		if(user != null){
			User userTmp = new User();
			userTmp.setSession(user.getSession());
			json.put("user", userTmp.toJson());
		}
		json.put("questionnaire", questionnaire);
				
		return json;
	}
}
