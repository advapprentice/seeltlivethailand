package com.touchtechnologies.command.reporter;

import org.json.JSONException;
import org.json.JSONObject;

import com.touchtechnologies.dataobject.reporter.User;

import android.content.Context;

public class GetUserProfileCommand extends BaseReporterCommand{
	public static final String COMMAND_NUMBER = "4300";

	public GetUserProfileCommand(Context context) {
		super(context, COMMAND_NUMBER);
	}
	
	@Override
	public JSONObject toJson() throws JSONException {
		User userTmp = new User();
		userTmp.setSession(user.getSession());
		userTmp.setUserID(user.getUserID());
		
		JSONObject json = new JSONObject();
		json.put("user", user.toJson());
		
		return json;
	}
}
