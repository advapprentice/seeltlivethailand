package com.touchtechnologies.command.reporter;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class RegisterCommand extends BaseReporterCommand{
	public static final String COMMAND_NUMBER = "4100";

	public RegisterCommand(Context context) {
		super(context, COMMAND_NUMBER);
	}
	
	@Override
	public JSONObject toJson() throws JSONException{
		JSONObject jsonUser = new JSONObject();
		jsonUser.put("user", user.toJson());
		
		return jsonUser;
	}
	
}
