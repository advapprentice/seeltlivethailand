package com.touchtechnologies.command;

import android.content.Context;
import android.os.Build;

import com.touchtechnologies.telephony.TelephonyInfo;
import com.touchtechnologies.telephony.TelephonyInfo.TELEPHONY_INFO;

/**
 * //email,password,fullname,mobileNumber,clientAgent,clientVersion,clientUDID,clientIMEI,clientIMSI,clientLanguage
 * @author Sayan Chaipaen
 *
 */
public class Signup extends Command{
	
	public Signup(Context context) {
		super(context, "010100");		
		 
		hFields.put(PARAMS.clientIMEI, TelephonyInfo.getInfo(context, TELEPHONY_INFO.IMEI));
		hFields.put(PARAMS.clientIMSI, TelephonyInfo.getInfo(context, TELEPHONY_INFO.IMSI));
		
		hFields.put(PARAMS.clientAgent, "Android " + Build.FINGERPRINT);		
	}	
	
	public void setEmail(String email){
		hFields.put(PARAMS.email, email);
	}
	
	public void setFullName(String name){
		hFields.put(PARAMS.fullname, name);
	}
	
	public void setMobileNo(String number){
		hFields.put(PARAMS.mobileNumber, number);
	}	
	
	public void setPassword(String password){
		hFields.put(PARAMS.password, password);
	}
}
