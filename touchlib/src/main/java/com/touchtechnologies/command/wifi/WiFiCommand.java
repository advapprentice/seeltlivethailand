package com.touchtechnologies.command.wifi;

import android.content.Context;

import com.touchtechnologies.command.Command;
import com.touchtechnologies.dataobject.WiFiAccount;
import com.touchtechnologies.dataobject.WiFiClient;

public class WiFiCommand extends Command{
	private WiFiClient wifiClient;
	

	public WiFiCommand(Context context, String number) {
		super(context, number, "KWDvyaFTZbZYS88hs8w6hTKj");
		
		hFields.put(PARAMS.client, "icsradiuscaller");
	}
	
	public void setWiFiClient(WiFiClient client){
		this.wifiClient = client;
		hFields.put(PARAMS.ip, client.getIp());
		
		WiFiAccount wc = client.getAccount();
		if(wc != null){
			hFields.put(PARAMS.username, wc.getUsername());
			hFields.put(PARAMS.password, wc.getPassword());
		}
	}
	
	@Override
	public String toString() {
		String json = super.toString();
		
		return json;
	}
}
