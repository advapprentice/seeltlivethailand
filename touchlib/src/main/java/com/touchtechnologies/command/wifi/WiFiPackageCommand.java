package com.touchtechnologies.command.wifi;

import android.content.Context;

/**
 * List WiFI package
 * @author Touch
 *
 */
public class WiFiPackageCommand extends WiFiCommand{
	

	public WiFiPackageCommand(Context context, String number) {
		super(context, number);
	}
	
	/**
	 * Set WiFi package type filter parmater
	 * @param type Prepaid or Postpaid
	 */
	public void setPackageType(String type){
		hFields.put(PARAMS.packageType, type);
	}
	
}
