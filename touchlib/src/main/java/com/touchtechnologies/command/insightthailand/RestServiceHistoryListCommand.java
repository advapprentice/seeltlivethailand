package com.touchtechnologies.command.insightthailand;


import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

/**
 * List ROI
 * @author MEe
 *
 */
public class RestServiceHistoryListCommand extends AsyncTask<Void, Void, List<StreamHistory>>{
	private Context context;
	private  User user;
	/**
	 * Rest url
	 */
	private String url;
	
	public RestServiceHistoryListCommand(Context context,User user, String url){
		this.context = context;
		this.url = url;
		this.user = user;
	}

	@Override
	protected List<StreamHistory> doInBackground(Void... params) {
		List<StreamHistory> his = new ArrayList<StreamHistory>();
		try{
			ServiceConnector service = new ServiceConnector(url);


			List<NameValuePair> headers = new ArrayList<NameValuePair>();
			headers.add(new BasicNameValuePair("Content-Type", "application/json"));
			headers.add(new BasicNameValuePair("X-TIT-ACCESS-TOKEN", user.getToken()));
			Log.d("headers", " :" +headers);
			try {
				//parsing response to json
				String response = service.doGet(context, headers);
				JSONArray jArray = new JSONArray(response);
				for(int i=0; i<jArray.length(); i++){
					his.add(new StreamHistory(jArray.getJSONObject(i)));
				}
			}catch (Exception e){
				Log.e("Error", ":" + e.getMessage());
			}

			//parsing json element to ROI object

		}catch(Exception e){
			Log.e(this.getClass().getSimpleName(), "context " + context + " error:" + e.getMessage(), e);
		}
		
		return his;
	}
	
}
