package com.touchtechnologies.command.insightthailand;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

/**
 * List ROI
 * @author TaTar
 *
 */
public class RestServiceCCTVHistoryCommand extends AsyncTask<Void, Void, CCTV>{
	private Context context;
	/**
	 * Rest url
	 */
	private String url;
	private CCTV cctv;
	public RestServiceCCTVHistoryCommand(Context context,CCTV cctv, String url){
		this.context = context;
		this.url = url;
		this.cctv = cctv;
	}

	@Override
	protected CCTV doInBackground(Void... params) {
	
		try{
			String cctvid = cctv.getId();
			
			ServiceConnector service = new ServiceConnector(url+cctvid);
		
			
			Log.d("test", "="+url+cctvid);
		//	service.doGet(context, headers);
			
			
			String response = service.doGet(context);
			JSONObject objuser = new JSONObject(response);
			
		    cctv = new CCTV(objuser);
			
		}catch(Exception e){
			Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
		}
		
		return cctv;
	}
	
}
