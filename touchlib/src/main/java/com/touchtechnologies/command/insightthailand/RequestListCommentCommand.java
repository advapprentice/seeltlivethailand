package com.touchtechnologies.command.insightthailand;


import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import com.touchtechnologies.dataobject.insightthailand.Comment;
import com.touchtechnologies.net.ServiceConnector;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class RequestListCommentCommand extends AsyncTask<Void, Void, List<Comment>>{
	/**
	 * Rest URL
	 */
	private String url;
	private Context context;
	
	private int limit = 50;
	private int offset= 0;
	
	
	/**
	 * 
	 * @param context
	 * @param poi around me center
	 * @param category ex hotel, restaurant
	 * @param url
	 */
	public RequestListCommentCommand(Context context, String url){
		this.context = context;
		this.url = url;
		
	}
	
	/**
	 *@TODO 
	 */
	@Override
	protected List<Comment> doInBackground(Void... params) {
		List<Comment> comment = new ArrayList<Comment>();
		try{
			

			Log.e("url","near by " + url);

			ServiceConnector service = new ServiceConnector(url );
//			ServiceConnector service = new ServiceConnector(url);
			
			System.out.print("test"+url);
			
			String response = service.doGet(context);
			
			//parsing response to json
			JSONArray jArray = new JSONArray(response);
			
			//parsing json element to ROI object
			for(int i=0; i<jArray.length(); i++){
			
				comment.add(new Comment(jArray.getJSONObject(i)));
				
			}
		}catch(Exception e){
			Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
		}
		
		return comment;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
}
