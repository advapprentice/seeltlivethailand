package com.touchtechnologies.command.insightthailand;

import java.util.Hashtable;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.command.Command.PARAMS;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.telephony.TelephonyInfo;
import com.touchtechnologies.wifi.WIFIMod;


public class RequestLivestreamCommand extends BaseInsightCommand{
	private JSONObject json = new JSONObject();
	String idpoi;
	
	public RequestLivestreamCommand(Context context) {
		super(context, "");
	}
	
	@Override
	public JSONObject toJson() throws JSONException{
		JSONObject jsonUser = new JSONObject();
		jsonUser.put("user", user.toJson());
	
		return jsonUser;
	}
	
	
	public void setProtocol(String protocol) {
		try{
		JSONObject jsonhas = new JSONObject();

		if(json.isNull("protocol")){
			jsonhas = new JSONObject();
			
		}else{
			jsonhas = json.getJSONObject("protocol");
		}
		
		json.put("protocol", protocol);
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}


	public void setConnectingTimeout(String timeout) {
		try{
			JSONObject jsonhas = new JSONObject();

			if(json.isNull("connectingTimeoutDatetime")){
				jsonhas = new JSONObject();

			}else{
				jsonhas = json.getJSONObject("connectingTimeoutDatetime");
			}

			json.put("connectingTimeoutDatetime", timeout);

		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}

	}
	
	public void setIdPoi(String idpoi) {
		this.idpoi = idpoi;
		try{
		JSONObject objpoi = new JSONObject();

		if(json.isNull("poi_id")){
			objpoi = new JSONObject();
			
		}else{
			objpoi = json.getJSONObject("poi_id");
		}
		
		json.put("poi_id", idpoi);
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}
	
	@Override
	public String toString() {
		try {
			
			User userTmp = new User();
			userTmp.setToken(user.getToken());
			
			json.put("access_token", user.getToken());
			json.put("title",livestream.getTitle());
			json.put("note",livestream.getNote());
			json.put("categoryID",livestream.getIdCategory());
			json.put("latitude",livestream.getLatitude());
			json.put("longitude",livestream.getLongitude());
			json.put("public",livestream.getIspublic());
			json.put("poi_id", idpoi);
			json.put("OS", "Android");
			json.put("operatorName",TelephonyInfo.getInfo(getContext(), TelephonyInfo.TELEPHONY_INFO.OPERATOR_NAME));
			json.put("osVersion", Build.VERSION.RELEASE);
			json.put("mobileAgent", Build.BRAND);
			json.put("rawAgent", Build.FINGERPRINT);
			json.put("deviceID", TelephonyInfo.getInfo(getContext(), TelephonyInfo.TELEPHONY_INFO.IMEI));
			
			json.put("wifiSSID", WIFIMod.getConnectedSSID(getContext()));
			json.put("language", "TH");
			
			
			
		} catch (JSONException e) {
			Log.e(LibConfig.LOGTAG, e.getMessage());
		}
		Log.d("POST","POST JSON:"+json.toString());
		Log.i("POST", "Content:" + json.toString());
	return json.toString();

	}
	
}
