package com.touchtechnologies.command.insightthailand;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.touchtechnologies.dataobject.Restaurant;
import com.touchtechnologies.net.ServiceConnector;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Get Hotel information
 * @author MEe
 *
 */
public class RestServiceGetRestaurantCommand extends AsyncTask<Void, Void, Restaurant>{
	private Context context;
	/**
	 * Rest url
	 */
	private String url;
	
	public RestServiceGetRestaurantCommand(Context context, String url){
		this.context = context;
		this.url = url;
	}

	@Override
	protected Restaurant doInBackground(Void... params) {
		Restaurant restaurant = null;
		try{
			ServiceConnector service = new ServiceConnector(url);
			String response = service.doGet(context);
			
			//parsing response to json
			JSONObject json = new JSONObject(response);
			
			restaurant= new Restaurant(json);
		}catch(Exception e){
			Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
		}
		
		return restaurant;
	}
	
}
