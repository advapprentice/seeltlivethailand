package com.touchtechnologies.command.insightthailand;

import org.json.JSONObject;

import com.touchtechnologies.app.LibConfig;

import android.content.Context;
import android.util.Log;

public class LoginSocialCommand extends LoginCommand{
	
	public LoginSocialCommand(Context context) {
		super(context);
	}
	
	@Override
	public String toString() {
		JSONObject json = new JSONObject();
		try{
			json.put("identifier", user.getId());
			json.put("access_token", user.getToken());
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, e.getMessage());
		}
		Log.d("POST","POST JSON:"+json.toString());
		return json.toString();
	}
}
