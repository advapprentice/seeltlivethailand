package com.touchtechnologies.command.insightthailand;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.touchtechnologies.dataobject.Hospitality;
import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.net.ServiceConnector;

import org.json.JSONObject;

/**
 * Created by TouchICS on 9/1/2016.
 */
public class RestServiceGetPoisCommand extends AsyncTask<Void, Void, Hospitality> {
    private Context context;
    /**
     * Rest url
     */
    private String url;

    public RestServiceGetPoisCommand(Context context, String url){
        this.context = context;
        this.url = url;
    }

    @Override
    protected Hospitality doInBackground(Void... params) {
        Hospitality poi = null;
        try{
            ServiceConnector service = new ServiceConnector(url);
            String response = service.doGet(context);

            //parsing response to json
            JSONObject json = new JSONObject(response);

            poi= new Hospitality(json);
        }catch(Exception e){
            Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
        }

        return poi;
    }

}
