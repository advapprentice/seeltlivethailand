package com.touchtechnologies.command.insightthailand;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import com.touchtechnologies.command.GetClientAccount;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.net.ServiceConnector;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

/**
 * List ROI
 * @author MEe
 *
 */
public class RestServiceROIListCommand extends AsyncTask<Void, Void, List<ROI>>{
	private Context context;
	/**
	 * Rest url
	 */
	private String url;
	
	public RestServiceROIListCommand(Context context, String url){
		this.context = context;
		this.url = url;
	}

	@Override
	protected List<ROI> doInBackground(Void... params) {
		List<ROI> rois = new ArrayList<ROI>();
		try{
			ServiceConnector service = new ServiceConnector(url);
			String response = service.doGet(context);
			
			//parsing response to json
			JSONArray jArray = new JSONArray(response);
			
			//parsing json element to ROI object
			for(int i=0; i<jArray.length(); i++){
				ROI roi = new ROI(jArray.getJSONObject(i));
				 for(CCTV cctv : roi.getCCTVs())
				 		cctv.setUrl(cctv.getUrl()+"&datatype=image&imagesize=small");

				rois.add(roi);
			}
		}catch(Exception e){
			Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
			
		}
		
		return rois;
	}
	
}
