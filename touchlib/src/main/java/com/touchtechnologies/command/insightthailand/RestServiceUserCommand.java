package com.touchtechnologies.command.insightthailand;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

/**
 * List ROI
 * 
 * @author MEe
 * 
 */
public class RestServiceUserCommand extends AsyncTask<Void, Void, User> {
	private Context context;
	
	/**
	 * Rest url
	 */
	private String url;
	private User user;
	protected JSONObject jsonResponse;

	public RestServiceUserCommand(Context context, User user, String url) {
		this.context = context;
		this.url = url;
		this.user = user;
	}

	@Override
	protected User doInBackground(Void... params) {
		// User user = new User();

		try {

			ServiceConnector service = new ServiceConnector(url);

			@SuppressWarnings("deprecation")
			List<NameValuePair> headers = new ArrayList<NameValuePair>();
			headers.add(new BasicNameValuePair("Content-Type","application/json"));
			headers.add(new BasicNameValuePair("X-TIT-ACCESS-TOKEN", user
					.getToken()));

			// service.doGet(context, headers);

			String response = service.doGet(context, headers);
			jsonResponse = new JSONObject(response);

			JSONObject objuser = null;
			int status = jsonResponse.getInt("status");
			if (status == 0) {
				objuser = jsonResponse.getJSONObject("data");
			}

			user = new User(objuser);

		} catch (Exception e) {
			Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
		}

		return user;
	}

}
