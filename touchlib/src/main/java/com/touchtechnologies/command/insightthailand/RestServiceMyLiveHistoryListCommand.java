package com.touchtechnologies.command.insightthailand;


import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

/**
 * List ROI
 * @author MEe
 *
 */
public class RestServiceMyLiveHistoryListCommand extends AsyncTask<Void, Void,List<StreamHistory>>{
	private Context context;
	/**
	 * Rest url
	 */
	private String url;
	private User user;
	public RestServiceMyLiveHistoryListCommand(Context context,User user, String url){
		this.context = context;
		this.url = url;
		this.user = user;
	}

	protected List<StreamHistory> doInBackground(Void... params) {
		List<StreamHistory> his = new ArrayList<StreamHistory>();
		try{
			ServiceConnector service = new ServiceConnector(url);
			
			
			Log.d("URL =", "tit:"+url);
			Log.d("URL =", "USER:"+user.getToken());
			List<NameValuePair> headers = new ArrayList<NameValuePair>();
			headers.add(new BasicNameValuePair("Content-Type", "application/json"));
			headers.add(new BasicNameValuePair("X-TIT-ACCESS-TOKEN", user.getToken()));
		   
		//	service.doGet(context, headers);
			
			
			String response = service.doGet(context,headers);
			
			
			//parsing response to json
			JSONArray jArray = new JSONArray(response);
			
			//parsing json element to ROI object
			for(int i=0; i<jArray.length(); i++){
				his.add(new StreamHistory(jArray.getJSONObject(i)));
			}
		}catch(Exception e){
			Log.e(this.getClass().getSimpleName(), "context " + context + " error:" + e.getMessage(), e);
		}
		
		return his;
	}
	
}
