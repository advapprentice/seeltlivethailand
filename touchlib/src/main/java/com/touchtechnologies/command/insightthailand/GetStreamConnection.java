package com.touchtechnologies.command.insightthailand;


import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.dataobject.reporter.Article;
import com.touchtechnologies.dataobject.reporter.Feed;
import com.touchtechnologies.dataobject.reporter.TargetGroup;
import com.touchtechnologies.dataobject.reporter.Topic;
import com.touchtechnologies.dataobject.reporter.User;
import com.touchtechnologies.dataobject.reporter.Guru;;


public class GetStreamConnection extends BaseInsightCommand{
	public static final String COMMAND_NUMBER = "2810";	
	private JSONObject feedFilter;
	
	public GetStreamConnection(Context context) {
		super(context,COMMAND_NUMBER);
		feedFilter = new JSONObject();
		
	}
	
	public void setProtocol(String protocol) {
		try{
		JSONObject jsonhas = new JSONObject();

		if(feedFilter.isNull("protocol")){
			jsonhas = new JSONObject();
			
		}else{
			jsonhas = feedFilter.getJSONObject("protocol");
		}
		
		feedFilter.put("protocol", protocol);
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}

	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
//		
	
	//	jsonlive.put("protocol", "RTSP");
		feedFilter.put("title",livestream.getTitle());
		feedFilter.put("note", livestream.getNote());
		
        json.put("streaming",feedFilter);
	
		
		return json;
	}
}
