package com.touchtechnologies.command.insightthailand;

/**
 * Created by TouchICS on 2/12/2016.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LoveUndoCommand extends AsyncTask<Void, Void, JSONObject> {
    private Context context;
    /**
     * Rest url
     */
    private String url;
    private User user;
    private JSONObject  obj;
    public LoveUndoCommand(Context context,User user, String url){
        this.context = context;
        this.url = url;
        this.user = user;

    }

    @Override
    protected JSONObject doInBackground(Void... params) {

        try{


            ServiceConnector service = new ServiceConnector(url);

            List<NameValuePair> headers = new ArrayList<NameValuePair>();
            headers.add(new BasicNameValuePair("Content-Type", "application/json"));
            headers.add(new BasicNameValuePair("X-TIT-ACCESS-TOKEN", user.getToken()));

            //	service.doGet(context, headers);
            Log.d("headers", "URL :"+headers);

            String response = service.doDelete(context,headers);
            JSONObject objresponse = new JSONObject(response);

            obj = objresponse;

        }catch(Exception e){
            Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
        }

        return obj;
    }

}
