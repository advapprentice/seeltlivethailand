package com.touchtechnologies.command.insightthailand;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;

public class UserExistCommand extends BaseInsightCommand{
	private JSONObject feedFilter = new JSONObject();
	
	public UserExistCommand(Context context) {
		super(context, "");
	}
	
	@Override
	public String toString() {
		try {
			feedFilter.put("identifier", user.getId());
			feedFilter.put("access_token", user.getToken() == null?"":user.getToken());
			
		} catch (JSONException e) {
			Log.e(LibConfig.LOGTAG, e.getMessage());
		}
		
	return feedFilter.toString();

	}
	
}
