package com.touchtechnologies.command.insightthailand;

import java.util.Enumeration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.crypto.CipherUtil;
import com.touchtechnologies.dataobject.insightthailand.Comment;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamConnection;
import com.touchtechnologies.dataobject.insightthailand.Notification;
import com.touchtechnologies.dataobject.insightthailand.Report;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.dataobject.wifi.WiFiGenerate;
import com.touchtechnologies.telephony.TelephonyInfo;

public class BaseInsightCommand extends Command{
	static String version;
	User user;
	LiveStreamConnection livestream;
	Report report;
	Notification notification;
	Comment comment;
	WiFiGenerate wiFiGenerate;
	/**
	 * Create a reporter's command object.
	 * Default reporter's command parameter will be set.
	 * @param context
	 * @param number
	 */
	public BaseInsightCommand(Context context, String number) {
		super(context, number, "Fq1J9CkqVnXPyhMtUufn9vBn");



		//clear dirty paramaters
		hFields.clear();
	    hFields.put(PARAMS.client, "titapp");
		//set default param for TouchInsightThailand
		hFields.put(PARAMS.command, number);
		hFields.put(PARAMS.appName, "titapp");
		
		if(version == null){
			try{
				PackageInfo pckInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES);
				version = pckInfo.versionName;
			}catch(Exception e){
				Log.e(LibConfig.LOGTAG, "Get version error", e);
			}			
		}
		try{
			hFields.put(PARAMS.appVersion, version);
			hFields.put(PARAMS.OS, "Android");
			hFields.put(PARAMS.OSVersion, Build.VERSION.RELEASE);
			hFields.put(PARAMS.mobileAgent, Build.FINGERPRINT);
			hFields.put(PARAMS.deviceID, TelephonyInfo.getInfo(context, TelephonyInfo.TELEPHONY_INFO.IMEI));
			//	hFields.put(PARAMS.deviceID,deviceId);
			hFields.put(PARAMS.language, "TH");

		}catch (Exception e){
			Log.i(LibConfig.LOGTAG, "Error :"+e.getMessage());
		}

		Log.i(LibConfig.LOGTAG, "Telephone Info :"+hFields );
	}
	
	public User getUser() {
		return user;
	}

		public void setUser(User user){
		this.user = user;
	}

	public void setWiFiGenerate(WiFiGenerate wiFiGenerate) {
		this.wiFiGenerate = wiFiGenerate;
	}

	public void setCommentcontent(Comment comment){
		this.comment = comment;
	}
	public void setLivestream(LiveStreamConnection livestream){
		this.livestream = livestream;
	}

	public void setNotification(Notification notification) {
		this.notification = notification;
	}

	public void setReport(Report report) {
		this.report = report;
	}

	/**
	 * Return a data in json form. 
	 * @return data attributes/ values (exclude "data" key)
	 */
	public JSONObject toJson() throws JSONException{		
		return null;
	}	
	
	@Override
	public String toString(){
		String content = null;
		
		JSONObject jCommandContainer = new JSONObject();
		PARAMS key;		
		try{
			//put ALL PARAMS values to json form
//			JSONObject jParams = new JSONObject();
			Enumeration<PARAMS> keys = hFields.keys();
			Object value;
			while(keys.hasMoreElements()){
				key = keys.nextElement();
				value = hFields.get(key);
				
				if(value instanceof String){
					jCommandContainer.put(key.toString(), value);
					
				}else if(value instanceof String[]){
					String[] values = (String[])value;
					
					JSONArray jArray = new JSONArray();
					for(int i=0; i<values.length; i++){
						jArray.put(values[i]);
					}
					
					jCommandContainer.put(key.toString(), jArray);
				}				
			}
						
			/*
			 * construct a JSON in form of
			 * 	{
  			 *	  "command": "command number",
  			 *	  "client": "app client",
  			 *	  "mask": "mask key",
  			 *	  "data": "encrypted data"
			 *	} 
			 */
			//command
			jCommandContainer.put(PARAMS.command.toString(), getNumber());			
			
			//client
			if(hFields.get(PARAMS.client) != null){
				jCommandContainer.put(PARAMS.client.toString(), hFields.get(PARAMS.client));
			}
			
			//"data" values
			JSONObject sibJson = this.toJson();
			if(sibJson != null){
				jCommandContainer.put(PARAMS.data.toString(), sibJson);

				Log.d(LibConfig.LOGTAG, "Reporter JSON -> " + jCommandContainer);
				
				//construct a JSON in form
				CipherUtil cipherUtil = CipherUtil.getInstance(this.key);
				content = cipherUtil.encrypt(jCommandContainer);
			}else{
				content = jCommandContainer.toString();				
			}
			
			
			Log.d(LibConfig.LOGTAG, "Post command -> "  + content);
		}catch(Exception e){
			e.printStackTrace();
			Log.d(LibConfig.LOGTAG, "Command toString", e);
		}		
		
		hashCode = content.hashCode();
		
		return content;
	}
}
