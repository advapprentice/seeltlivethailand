package com.touchtechnologies.command.insightthailand;


import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;


public class ListstreamingCommand extends BaseInsightCommand{
	private JSONObject json = new JSONObject();
	
	public ListstreamingCommand(Context context) {
		super(context, "");
	}
	
	
	public void setStatus(String status) {
		try{
		JSONObject jsonlive = new JSONObject();

		if(json.isNull("status")){
			jsonlive = new JSONObject();
			
		}else{
			jsonlive = json.getJSONObject("status");
		}
			
		jsonlive.put("status", status);
		json.put("streaming", jsonlive);
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}
	public void setHasStreaming(String hasStreaming) {
		try{
		JSONObject jsonhas = new JSONObject();

		if(json.isNull("hasStreaming")){
			jsonhas = new JSONObject();
			
		}else{
			jsonhas = json.getJSONObject("hasStreaming");
		}
		
		json.put("hasStreaming", hasStreaming);
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}
		
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();
//	
//		JSONObject jsonSchedule = new JSONObject();
//		jsonSchedule.put("hasStreaming", "Y");

//		JSONObject jsonlive = new JSONObject();
//		jsonlive.put("displayStatus", "Display");
//		feedFilter.put("streaming", jsonlive);
//		json.put("streamingChannel",jsonSchedule);
		
//		feedFilter.put("hasStreaming", "Y");
		
 //       json.put("streamingChannel",feedFilter);
        

//		
		return json;
	}
	
	@Override
	public String toString() {
	

	Log.d("POST","POST JSON:"+json.toString());	
	return json.toString();

	}
	
	
}
