package com.touchtechnologies.command.insightthailand;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.dataobject.insightthailand.User;


public class RequestLviestreamCommand extends BaseInsightCommand{
	private JSONObject json = new JSONObject();
	String idpoi;
	
	public RequestLviestreamCommand(Context context) {
		super(context, "");
	}
	
	@Override
	public JSONObject toJson() throws JSONException{
		JSONObject jsonUser = new JSONObject();
		jsonUser.put("user", user.toJson());
	
		return jsonUser;
	}
	
	
	public void setProtocol(String protocol) {
		try{
		JSONObject jsonhas = new JSONObject();

		if(json.isNull("protocol")){
			jsonhas = new JSONObject();
			
		}else{
			jsonhas = json.getJSONObject("protocol");
		}
		
		json.put("protocol", protocol);
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}
	
	public void setIdPoi(String idpoi) {
		this.idpoi = idpoi;
		try{
		JSONObject objpoi = new JSONObject();

		if(json.isNull("poi_id")){
			objpoi = new JSONObject();
			
		}else{
			objpoi = json.getJSONObject("poi_id");
		}
		
		json.put("poi_id", idpoi);
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}
	
	@Override
	public String toString() {
		try {
			User userTmp = new User();
			userTmp.setToken(user.getToken());
			
			json.put("access_token",user.getToken());
			json.put("title",livestream.getTitle());
			json.put("note",livestream.getNote());
			json.put("latitude",livestream.getLatitude());
			json.put("longitude",livestream.getLongitude());
			json.put("poi_id", idpoi);
		} catch (JSONException e) {
			Log.e(LibConfig.LOGTAG, e.getMessage());
		}
		Log.d("POST","POST JSON:"+json.toString());
	return json.toString();

	}
	
}
