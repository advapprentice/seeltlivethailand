package com.touchtechnologies.command.insightthailand;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.dataobject.insightthailand.User;


public class RequestcommentCommand extends BaseInsightCommand{
	private JSONObject json = new JSONObject();
	String idpoi;
	
	public RequestcommentCommand(Context context) {
		super(context, "");
	}
	
	@Override
	public JSONObject toJson() throws JSONException{
		JSONObject jsonUser = new JSONObject();
		jsonUser.put("user", user.toJson());
	
		return jsonUser;
	}
	
	
	@Override
	public String toString() {
		try {
			User userTmp = new User();
			userTmp.setToken(user.getToken());
			
			json.put("access_token",user.getToken());
			json.put("comment_content",comment.getCommentContent());
		
		} catch (JSONException e) {
			Log.e(LibConfig.LOGTAG, e.getMessage());
		}
		Log.d("POST","POST JSON:"+json.toString());
	return json.toString();

	}
	
}
