package com.touchtechnologies.command.insightthailand;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.touchtechnologies.dataobject.insightthailand.CategoryLiveStream;
import com.touchtechnologies.dataobject.insightthailand.Report;
import com.touchtechnologies.net.ServiceConnector;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by TouchICS on 3/7/2016.
 */
public class RestServiceReportListCommand extends AsyncTask<Void, Void, List<Report>> {
    private Context context;
    /**
     * Rest url
     */
    private String url;
    public RestServiceReportListCommand(Context context, String url){
        this.context = context;
        this.url = url;
    }

    @Override
    protected List<Report> doInBackground(Void... params) {
        List<Report> report = new ArrayList<Report>();
        try{
            ServiceConnector service = new ServiceConnector(url);
            String response = service.doGet(context);

            //parsing response to json
            JSONArray jArray = new JSONArray(response);

            //parsing json element to ROI object
            for(int i=0; i<jArray.length(); i++){
                report.add(new Report(jArray.getJSONObject(i)));
            }
        }catch(Exception e){
            Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
        }

        return report;
    }

}
