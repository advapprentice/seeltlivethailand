package com.touchtechnologies.command.insightthailand;

import java.util.Hashtable;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.command.Command.PARAMS;
import com.touchtechnologies.dataobject.insightthailand.Report;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.telephony.TelephonyInfo;
import com.touchtechnologies.wifi.WIFIMod;


public class ReportCommand extends BaseInsightCommand{
    private JSONObject json = new JSONObject();
    public ReportCommand(Context context) {
        super(context, "");
    }

    @Override
    public JSONObject toJson() throws JSONException{
        JSONObject jsonUser = new JSONObject();
        return jsonUser;
    }


    @Override
    public String toString() {
        try {
            json.put("improper_report_reason_id",report.getId());

        } catch (JSONException e) {
            Log.e(LibConfig.LOGTAG, e.getMessage());
        }
        Log.d("POST","POST JSON:"+json.toString());
        return json.toString();

    }

}
