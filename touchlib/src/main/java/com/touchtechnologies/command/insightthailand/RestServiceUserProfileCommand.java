package com.touchtechnologies.command.insightthailand;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;

public class RestServiceUserProfileCommand extends BaseInsightCommand{
	private JSONObject json = new JSONObject();
	
	public RestServiceUserProfileCommand(Context context) {
		super(context, "");
	}
	
	@Override
	public JSONObject toJson() throws JSONException{
		JSONObject jsonUser = new JSONObject();
		jsonUser.put("user", user.toJson());
	
		return jsonUser;
	}
	
	
	@Override
	public String toString() {
		try {
		
			
			json.put("access_token",user.getToken());
			
		} catch (JSONException e) {
			Log.e(LibConfig.LOGTAG, e.getMessage());
		}
		Log.d("POST","POST JSON:"+json.toString());
	return json.toString();

	}
	
}