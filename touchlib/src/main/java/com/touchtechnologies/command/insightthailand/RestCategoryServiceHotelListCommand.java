package com.touchtechnologies.command.insightthailand;


import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import com.touchtechnologies.dataobject.Category;
import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.net.ServiceConnector;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * List Hotel
 * @author MEe
 *
 */
public class RestCategoryServiceHotelListCommand extends AsyncTask<Void, Void, List<Hotel>>{
	private Context context;
	private ROI roi;
	private Category category;
	private int limit = 50;
	private int offset= 0;
	
	/**
	 * Rest url
	 */
	private String url;
	
	public RestCategoryServiceHotelListCommand(Context context, ROI roi, @Nullable Category category, String url){
		this.context = context;
		this.url = url;
		this.roi = roi;
		this.category = category;
	}
	

	@Override
	protected List<Hotel> doInBackground(Void... params) {
		List<Hotel> hotels = new ArrayList<Hotel>();
		try{
			String query = "filters[roi][provider_information_tag_keyname][operator]==&filters[roi][provider_information_tag_keyname]"
					+ "[value]="+roi.getKeyName()+"&filters[category][category_keyname][operator]==&filters[category][category_keyname]"
					+ "[value]="+ (category==null?"":category.getKeyname())+"";
		
			Log.e("url","="+query);

			ServiceConnector service = new ServiceConnector(url + query);
			String response = service.doGet(context);
			
			//parsing response to json
			JSONArray jArray = new JSONArray(response);
			
			//parsing json element to ROI object
			for(int i=0; i<jArray.length(); i++){
				hotels.add(new Hotel(jArray.getJSONObject(i)));
			}
		}catch(Exception e){
			Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
		}
		
		return hotels;
	}
	
}
