package com.touchtechnologies.command.insightthailand;

/**
 * Created by TouchICS on 6/10/2016.
 */

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamConnection;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.dataobject.insightthailand.Notification;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

/**
 * List ROI
 *
 * @author MEe
 *
 */
public class RestServiceLiveStreamPropertieCommand extends AsyncTask<Void, Void, LiveStreamConnection> {
    private Context context;

    /**
     * Rest url
     */
    private String url;
    private LiveStreamConnection liveStreamingChannel;
    protected JSONObject jsonResponse;

    public RestServiceLiveStreamPropertieCommand(Context context,  String url) {
        this.context = context;
        this.url = url;

    }

    @Override
    protected LiveStreamConnection doInBackground(Void... params) {
        // User user = new User();


            try{
            ServiceConnector service = new ServiceConnector(url);
            String response = service.doGet(context);


            jsonResponse = new JSONObject(response);

            int status = jsonResponse.getInt("status");
            if(status == 0||status == 200){
                JSONObject json = jsonResponse.getJSONObject("data");
                //parsing response to json
              //  JSONObject obj = json.getJSONObject("streamingChannels");


                liveStreamingChannel = new LiveStreamConnection(json);
                Log.i("json Response",":"+liveStreamingChannel.toString());

            }



        } catch (Exception e) {
            Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
        }

        return liveStreamingChannel;
    }

}
