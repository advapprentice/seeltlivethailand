package com.touchtechnologies.command.insightthailand;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

/**
 * List Hotel
 * @author MEe
 *
 */
public class RequestListLiveStreamingCommand extends AsyncTask<Void, Void, List<LiveStreamingChannel>>{
	private Context context;
	private int limit = 50;
	private int offset= 0;
	private  LiveStreamingChannel live;
	/**
	 * Rest url
	 */
	private String url;
	private User user;
	public RequestListLiveStreamingCommand(Context context,User user, String url){
		this.context = context;
		this.url = url;
		this.user = user;
	}
	
	@Override
	protected List<LiveStreamingChannel> doInBackground(Void... params) {
		List<LiveStreamingChannel> lives = new ArrayList<LiveStreamingChannel>();
		try{
	//		String query = "hasStreaming=Y";
			String query = "streaming[status]=Streaming";
			Log.i("==url==","set url:"+query.toString());

			ServiceConnector service = new ServiceConnector(url + query);

			List<NameValuePair> headers = new ArrayList<NameValuePair>();
			headers.add(new BasicNameValuePair("Content-Type", "application/json"));
			headers.add(new BasicNameValuePair("X-TIT-ACCESS-TOKEN", user.getToken()));
			Log.i("headers", " :" +headers);

			String response = service.doGet(context,headers);

			
			JSONObject json = new JSONObject(response);
			
			int status = json.getInt("status");
			if(status == 0){
				json = json.getJSONObject("data");
				//parsing response to json
				JSONArray jArray = json.getJSONArray("streamingChannels");
				
				//parsing json element to ROI object
				for(int i=0; i<jArray.length(); i++){
					lives.add(new LiveStreamingChannel(jArray.getJSONObject(i)));
				}
			}
			
		}catch(Exception e){
			Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
		}
		
		return lives;
	}
	
}
