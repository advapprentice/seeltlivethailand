package com.touchtechnologies.command.insightthailand;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.dataobject.reporter.Article;
import com.touchtechnologies.dataobject.reporter.Feed;
import com.touchtechnologies.dataobject.reporter.TargetGroup;
import com.touchtechnologies.dataobject.reporter.Topic;
import com.touchtechnologies.dataobject.reporter.User;
import com.touchtechnologies.dataobject.reporter.Guru;;


public class ListStreamingHistoryCommand extends BaseInsightCommand{
	public static final String COMMAND_NUMBER = "2970";	
	private JSONObject feedFilter;
	public ListStreamingHistoryCommand(Context context) {
		super(context,COMMAND_NUMBER);
		feedFilter  = new JSONObject();
	}
	
	public void setStatus(String status) {
		try{
		JSONObject jsonlive = new JSONObject();

		if(feedFilter.isNull("status")){
			jsonlive = new JSONObject();
			
		}else{
			jsonlive = feedFilter.getJSONObject("status");
		}
			
		jsonlive.put("status", status);
		feedFilter.put("streaming", jsonlive);
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}
	public void setHasStreaming(String hasStreaming) {
		try{
		JSONObject jsonhas = new JSONObject();

		if(feedFilter.isNull("hasStreaming")){
			jsonhas = new JSONObject();
			
		}else{
			jsonhas = feedFilter.getJSONObject("hasStreaming");
		}
		
		feedFilter.put("hasStreaming", hasStreaming);
			
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, "add feed type error");
		}
		
	}
		
	
	@Override
	public JSONObject toJson() throws JSONException {
		JSONObject json = new JSONObject();

		return json;
	}
}
