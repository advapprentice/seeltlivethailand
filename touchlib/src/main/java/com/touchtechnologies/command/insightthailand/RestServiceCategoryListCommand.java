package com.touchtechnologies.command.insightthailand;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.insightthailand.CategoryLiveStream;
import com.touchtechnologies.net.ServiceConnector;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TouchICS on 3/7/2016.
 */
public class RestServiceCategoryListCommand extends AsyncTask<Void, Void, List<CategoryLiveStream>> {
    private Context context;
    /**
     * Rest url
     */
    private String url;

    public RestServiceCategoryListCommand(Context context, String url){
        this.context = context;
        this.url = url;
    }

    @Override
    protected List<CategoryLiveStream> doInBackground(Void... params) {
        List<CategoryLiveStream> cctv = new ArrayList<CategoryLiveStream>();
        try{
            ServiceConnector service = new ServiceConnector(url);
            String response = service.doGet(context);

            //parsing response to json
            JSONArray jArray = new JSONArray(response);

            //parsing json element to ROI object
            for(int i=0; i<jArray.length(); i++){
                cctv.add(new CategoryLiveStream(jArray.getJSONObject(i)));
            }
        }catch(Exception e){
            Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
        }

        return cctv;
    }

}
