package com.touchtechnologies.command.insightthailand;

import java.util.Hashtable;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.command.Command.PARAMS;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.telephony.TelephonyInfo;
import com.touchtechnologies.wifi.WIFIMod;

/**
 * Created by TouchICS on 6/9/2016.
 */
public class NotificationRegisterCommand extends BaseInsightCommand{
    private JSONObject json = new JSONObject();
    public NotificationRegisterCommand(Context context) {
        super(context, "");
    }

    @Override
    public JSONObject toJson() throws JSONException{
        JSONObject jsonUser = new JSONObject();
        return jsonUser;
    }


    @Override
    public String toString() {


        try {
            json.put("unique_device_id",""+TelephonyInfo.getInfo(getContext(), TelephonyInfo.TELEPHONY_INFO.IMEI));
            json.put("platform",notification.getPlatform());
            json.put("os",notification.getOs());
            json.put("user_id",notification.getUserId());
            json.put("push_token",notification.getUniqueDeviceId());
            //   json.put("is_connected_socket",noti.getIs_connected_socket());
            //  json.put("language",noti.getLanguage());

        } catch (JSONException e) {
            Log.e(LibConfig.LOGTAG, e.getMessage());
        }
        Log.i("POST","POST JSON:"+json.toString());
        return json.toString();

    }

}
