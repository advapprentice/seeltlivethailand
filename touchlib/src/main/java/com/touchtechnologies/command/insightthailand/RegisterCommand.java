package com.touchtechnologies.command.insightthailand;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.dataobject.insightthailand.User;

public class RegisterCommand extends BaseInsightCommand{
	private JSONObject feedFilter = new JSONObject();
	
	public RegisterCommand(Context context) {
		super(context, "");
	}
	
	@Override
	public JSONObject toJson() throws JSONException{
		JSONObject jsonUser = new JSONObject();
		jsonUser.put("user", user.toJson());
	
		return jsonUser;
	}

	@Override
	public String toString() {
		try {
			feedFilter.put("email", user.getEmail());
			feedFilter.put("password", user.getPassword());
			feedFilter.put("password_confirmation", user.getPasswordconfrim());
			feedFilter.put("first_name", user.getFirstName());
			feedFilter.put("last_name", user.getLastName());
			feedFilter.put("birth_date", user.getBirthDate());
			feedFilter.put("access_token", user.getToken());
			feedFilter.put("gender", user.getGender());
			feedFilter.put("country", user.getCountry());
			feedFilter.put("zipcode", user.getZipcode());
			feedFilter.put("address", user.getAddress());
		
		} catch (JSONException e) {
			Log.e(LibConfig.LOGTAG, e.getMessage());
		}
		
	return feedFilter.toString();

	}
	
}
