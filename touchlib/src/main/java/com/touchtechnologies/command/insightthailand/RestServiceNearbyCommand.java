package com.touchtechnologies.command.insightthailand;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import com.google.android.gms.maps.model.LatLng;
import com.touchtechnologies.dataobject.DataItem;
import com.touchtechnologies.dataobject.DataItemFactory;
import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.net.ServiceConnector;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

public class RestServiceNearbyCommand extends AsyncTask<LatLng, Void, List<DataItem>>{
	/**
	 * Rest URL
	 */
	private String url;
	private Context context;
	
	private String category;
	
	private int limit = 50;
	private int offset= 0;
	
	
	/**
	 * 
	 * @param context
	 * @param poi around me center
	 * @param category ex hotel, restaurant
	 * @param url
	 */
	public RestServiceNearbyCommand(Context context, @Nullable DataItem poi, String category, String url){
		this.context = context;
		this.url = url;
		this.category = category;
	}
	
	/**
	 *@TODO 
	 */
	@Override
	protected List<DataItem> doInBackground(LatLng... params) {
		List<DataItem> pois = new ArrayList<DataItem>();
		try{
			
//			
//			String query = "filters[roi][provider_information_tag_keyname][operator]==" +
//					"&filters[roi][provider_information_tag_keyname][value]=pattaya" +
//					"&filters[poi][provider_type_keyname][operator]==" +
//					"&filters[poi][provider_type_keyname][value]=restaurant" +
//					"&filtersPage=0&filterLimit=50&filterOrder[name_th]=desc&filterOrder[name_en]=asc";
//			
			
		    String query = "50?filters[poi][provider_type_keyname][operator]==&filters[poi]"
		    		+ "[provider_type_keyname][value]=restaurant";
			
			Log.e("url","near by " + query);

			ServiceConnector service = new ServiceConnector(url + query);
//			ServiceConnector service = new ServiceConnector(url);
			
			System.out.print("test"+url);
			
			String response = service.doGet(context);
			
			//parsing response to json
			JSONArray jArray = new JSONArray(response);
			
			//parsing json element to ROI object
			for(int i=0; i<jArray.length(); i++){
				pois.add(DataItemFactory.newDataItem(jArray.getJSONObject(i)));
			}
		}catch(Exception e){
			Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
		}
		
		return pois;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
}
