package com.touchtechnologies.command.insightthailand;

import android.util.Log;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.touchtechnologies.dataobject.Hospitality;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TouchICS on 9/1/2016.
 */
public class RestServiceNearbyPoiListCommand extends AsyncTask<Void, Void,List<Hospitality>> {
    private Context context;
    /**
     * Rest url
     */
    private String url;
    public RestServiceNearbyPoiListCommand(Context context, String url){
        this.context = context;
        this.url = url;

    }
    protected List<Hospitality> doInBackground(Void... params) {
        List<Hospitality> his = new ArrayList<Hospitality>();
        try{
            ServiceConnector service = new ServiceConnector(url);
            Log.d("URL =", "tit:"+url);

            //	service.doGet(context, headers);
            String response = service.doGet(context);
            //parsing response to json
            JSONArray jArray = new JSONArray(response);

            //parsing json element to ROI object
            for(int i=0; i<jArray.length(); i++){
                his.add(new Hospitality(jArray.getJSONObject(i)));
            }
        }catch(Exception e){
            Log.e(this.getClass().getSimpleName(), "context " + context + " error:" + e.getMessage(), e);
        }

        return his;
    }

}
