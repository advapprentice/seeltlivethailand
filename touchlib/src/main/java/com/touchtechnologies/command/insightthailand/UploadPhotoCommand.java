package com.touchtechnologies.command.insightthailand;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.dataobject.insightthailand.User;

public class UploadPhotoCommand extends BaseInsightCommand{
	private JSONObject feedFilter = new JSONObject();
	
	public UploadPhotoCommand(Context context) {
		super(context, "");
	}
	
	@Override
	public JSONObject toJson() throws JSONException{
	//	JSONObject jsonUser = new JSONObject();
	//	jsonUser.put("user", user.toJson());
	
		return null;
	}

	@Override
	public String toString() {
		try {
			feedFilter.put("photo", user.getAvatar());
			//feedFilter.put("access_token", user.getToken());
		
		} catch (JSONException e) {
			Log.e(LibConfig.LOGTAG, e.getMessage());
		}
		
	return feedFilter.toString();

	}
	
}
