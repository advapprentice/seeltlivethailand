package com.touchtechnologies.command.insightthailand;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.net.ServiceConnector;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

/**
 * List ROI
 * @author MEe
 *
 */
public class RestServiceCCTVListCommand extends AsyncTask<Void, Void, List<CCTV>>{
	private Context context;
	/**
	 * Rest url
	 */
	private String url;
	
	public RestServiceCCTVListCommand(Context context, String url){
		this.context = context;
		this.url = url;
	}

	@Override
	protected List<CCTV> doInBackground(Void... params) {
		List<CCTV> cctv = new ArrayList<CCTV>();
		try{
			ServiceConnector service = new ServiceConnector(url);
			String response = service.doGet(context);
			
			//parsing response to json
			JSONArray jArray = new JSONArray(response);
			
			//parsing json element to ROI object
			for(int i=0; i<jArray.length(); i++){
				cctv.add(new CCTV(jArray.getJSONObject(i)));
			}
		}catch(Exception e){
			Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
		}
		
		return cctv;
	}
	
}
