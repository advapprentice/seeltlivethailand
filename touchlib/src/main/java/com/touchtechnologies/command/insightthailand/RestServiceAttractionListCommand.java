package com.touchtechnologies.command.insightthailand;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.dataobject.Restaurant;
import com.touchtechnologies.net.ServiceConnector;

/**
 * List Hotel
 * @author MEe
 *
 */
public class RestServiceAttractionListCommand extends AsyncTask<Void, Void, List<Restaurant>>{
	private Context context;
	private ROI roi;
	private int limit = 50;
	private int offset= 0;
	
	/**
	 * Rest url
	 */
	private String url;
	
	/**
	 * 
	 * @param context
	 * @param roi
	 * @param url
	 */
	public RestServiceAttractionListCommand(Context context, @Nullable ROI roi, String url){
		this.context = context;
		this.url = url;
		this.roi = roi;
	}
	

	@Override
	protected List<Restaurant> doInBackground(Void... params) {
		List<Restaurant> restaurants = new ArrayList<Restaurant>();
		try{
			
			
//			String query = "filters[category][category_keyname][operator]==&filters[category][category_keyname][value]=restaurant&filters[roi][provider_information_tag_keyname][operator]==" +
//					"&filters[roi][provider_information_tag_keyname][value]=" + (roi==null?"":roi.getKeyName()) + 
//					"&filtersPage=" + offset + "&filterLimit=" + limit + "&filterOrder[name_th]=desc&&filterOrder[name_en]=asc";
//			
			
			String query = "filters[poi][provider_type_keyname][operator]==&filters[poi][provider_type_keyname][value]=attraction&filters[roi][provider_information_tag_keyname][operator]=="+
					"&filters[roi][provider_information_tag_keyname][value]=" + (roi==null?"":roi.getKeyName()) +
					"&filtersPage=&filterLimit=&filterOrder[name_th]=desc&&filterOrder[name_en]=asc";
			
			
			Log.e("url","roi"+query);

			ServiceConnector service = new ServiceConnector(url + query);
			String response = service.doGet(context);
			
			//parsing response to json
			JSONArray jArray = new JSONArray(response);
			
			//parsing json element to ROI object
			for(int i=0; i<jArray.length(); i++){
				restaurants.add(new Restaurant(jArray.getJSONObject(i)));
			}
		}catch(Exception e){
			Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
		}
		
		return restaurants;
	}
	
}
