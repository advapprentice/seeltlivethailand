package com.touchtechnologies.command.insightthailand;

import org.json.JSONObject;

import com.touchtechnologies.app.LibConfig;

import android.content.Context;
import android.util.Log;

public class LoginCommand extends BaseInsightCommand{
	private JSONObject json = new JSONObject();

	public LoginCommand(Context context) {
		super(context, "");
	}
	
	@Override
	public String toString() {
		try{
			json.put("email", user.getEmail());
			json.put("password", user.getPassword());
		}catch(Exception e){
			Log.e(LibConfig.LOGTAG, e.getMessage());
		}
		Log.d("POST","POST JSON:"+json.toString());
		return json.toString();
	}
}
