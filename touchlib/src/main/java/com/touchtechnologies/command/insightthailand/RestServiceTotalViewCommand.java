package com.touchtechnologies.command.insightthailand;

import org.json.JSONArray;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.touchtechnologies.net.ServiceConnector;

/**
 * List ROI
 * @author MEe
 *
 */
public class RestServiceTotalViewCommand extends AsyncTask<Void, Void, JSONArray>{
	private Context context;
	/**
	 * Rest url
	 */
	private String url;
	
	public RestServiceTotalViewCommand(Context context, String url){
		this.context = context;
		this.url = url;
	}

	@Override
	protected JSONArray doInBackground(Void... params) {
		JSONArray jsonArray = null;
		try{
			ServiceConnector service = new ServiceConnector(url);
			String response = service.doGet(context);
			
			//parsing response to json
			jsonArray = new JSONArray(response);
		}catch(Exception e){
			Log.e(this.getClass().getSimpleName(), e.getMessage(), e);
			
		}
		
		return jsonArray;
	}
	
}
