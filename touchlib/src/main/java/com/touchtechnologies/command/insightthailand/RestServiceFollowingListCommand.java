package com.touchtechnologies.command.insightthailand;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TouchICS on 3/15/2016.
 */
public class RestServiceFollowingListCommand extends AsyncTask<Void, Void, List<User>> {
private Context context;
private  User user;
/**
 * Rest url
 */
private String url;

public RestServiceFollowingListCommand(Context context,User user, String url){
        this.context = context;
        this.url = url;
        this.user = user;
        }

@Override
protected List<User> doInBackground(Void... params) {
        List<User> arruser = new ArrayList<User>();
        try{

        ServiceConnector service = new ServiceConnector(url);

        List<NameValuePair> headers = new ArrayList<NameValuePair>();
        headers.add(new BasicNameValuePair("Content-Type", "application/json"));
        headers.add(new BasicNameValuePair("X-TIT-ACCESS-TOKEN", user.getToken()));
        Log.d("headers", " :" + headers);
        String response = service.doGet(context,headers);

        //parsing response to json
        JSONArray jArray = new JSONArray(response);

        //parsing json element to ROI object
        for(int i=0; i<jArray.length(); i++){
        arruser.add(new User(jArray.getJSONObject(i)));
        }
        }catch(Exception e){
        Log.e(this.getClass().getSimpleName(), "context " + context + " error:" + e.getMessage(), e);
        }

        return arruser;
        }

        }
   