package com.touchtechnologies.command;

import android.content.Context;
import android.os.Build;

/**
 * command, client version, lang, useragent, email, UDID, password hash		
 * @author MEe
 *
 */
public class Signin extends Command{
	
	public Signin(Context context) {
		super(context, "010200");		
		hFields.put(PARAMS.clientAgent, "Android " + Build.FINGERPRINT);
	}
	
	public void setPassword(String password){		
		hFields.put(PARAMS.password, password);
	}
	
	public void setEmail(String email){
		hFields.put(PARAMS.email, email);
	}
}
