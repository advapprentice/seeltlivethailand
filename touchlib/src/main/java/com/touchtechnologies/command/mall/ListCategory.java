package com.touchtechnologies.command.mall;

import com.touchtechnologies.command.Command;

import android.content.Context;

public class ListCategory extends Command{
	public ListCategory(Context context) {
		super(context, "030100");
	}
	
	public void setToken(String token){
		hFields.put(PARAMS.token, token);
	}
}
