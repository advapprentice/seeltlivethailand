package com.touchtechnologies.animate.button;

interface OnAnimationEndListener {

    public void onAnimationEnd();
}
