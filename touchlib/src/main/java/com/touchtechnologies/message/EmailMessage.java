/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.touchtechnologies.message;

/**
 *
 * @author MEe
 */
public class EmailMessage extends Message{
    private String htmlText;

    /**
     * @return the htmlText
     */
    public String getHtmlText() {
        return htmlText;
    }

    /**
     * @param htmlText the htmlText to set
     */
    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }


}
