/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.touchtechnologies.message;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author MEe
 */
public class Message {
    private long timeStamp;
    private String address;
    private String text;

    protected Message(){        
    }

    /**
     * @return the timeStamp
     */
    public long getTimeStamp() {
        return timeStamp;
    }

    /**
     * @param timeStamp the timeStamp to set
     */
    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public static Message newMessage(String address){
        Message message = null;

        Pattern pattern = Pattern.compile("");
        Matcher phoneMatcher = pattern.matcher("");
        Matcher emailMatcher = pattern.matcher("");

        if(phoneMatcher.matches()){
            message = new TextMessage();
        }else if(emailMatcher.matches()){
            message = new EmailMessage();
        }

        message.setAddress(address);
        message.setTimeStamp(System.currentTimeMillis());

        return message;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }
}
