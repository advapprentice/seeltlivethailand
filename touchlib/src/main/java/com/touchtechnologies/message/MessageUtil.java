/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.touchtechnologies.message;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import android.util.Log;

/**
 *
 * @author MEe
 */
public class MessageUtil {
    private static Properties properties;
    static final String TAG = "MESSAGEU-TIL";
    
    static {
        try {
        	//TODO read resource
            properties = new Properties();
            properties.setProperty("GW_USER","josecomsci");
            properties.setProperty("GW_PWD","44315");
            properties.setProperty("GW_SOURCEID","00000");
            properties.setProperty("GW_HOST","http://27.254.47.201:9080/TouchSMSWeb/TouchSMSEnqueueSMSServlet");
            
        } catch (Exception e) {
            Log.e(TAG, "Load properties error", e);
        }
    }

    public static void send(Message message) {
        Vector msg = new Vector();
        msg.add(message);

        send(msg);
    }

    public static void send(final Vector<? extends Message> messages) {
        
        Thread sendThread = new Thread() {

            public void run() {
                Log.i(TAG,"Starting send message to alert to sms/email");

                HttpURLConnection httpConn = null;
                DataOutputStream dos = null;
                DataInputStream input = null;
                URL url = null;

                for (Message message : messages) {
                    if (message instanceof TextMessage) {//send SMS
                        try {
                            url = new URL(properties.getProperty("GW_HOST"));
                            Log.i(TAG, url.toString());
                            httpConn = (HttpURLConnection) url.openConnection();
                            httpConn.setRequestMethod("POST");
                            httpConn.setDoOutput(true);

                            StringBuffer query = new StringBuffer("CMD=ENQUEUESMS&");
                            query.append("TO=");
                            query.append(message.getAddress());
                            query.append("&");
                            query.append("SENDER=TOUCHSMS&");
                            query.append("CTYPE=UNICODE&");
                            query.append("SMSMSG=");
                            query.append(URLEncoder.encode(message.getText(), "UTF-8"));
//                            query.append(message.getText());
                            query.append("&");
                            query.append("USERNAME=");
                            query.append(properties.getProperty("GW_USER"));
                            query.append("&");
                            query.append("PASSWORD=");
                            query.append(properties.getProperty("GW_PWD"));
                            query.append("&");
                            query.append("SOURCEID=");
                            query.append(properties.getProperty("GW_SOURCEID"));

                            Log.i(TAG, "REQ SMSGATEWAY:" + query.toString());

                            dos = new DataOutputStream(httpConn.getOutputStream());
                            dos.write(query.toString().getBytes());

                            input = new DataInputStream(httpConn.getInputStream());

// read in each character until end-of-stream is detected
                            StringBuffer buff = new StringBuffer();
                            for (int c = input.read(); c != -1; c = input.read()) {
                                buff.append((char) c);
                            }
                            input.close();

                            Log.i(TAG, "Send SMS response:" + buff.toString());
                        } catch (Exception e) {
                        	Log.e(TAG, "Send alert message error", e);
                            if (url != null) {
                            	Log.e(TAG,"To URL: " + url.getHost() + ":" + url.getPort() + url.getPath() + "?" + url.getQuery());
                            }
                        } finally {
                            if (dos != null) {
                                try {
                                    dos.close();
                                } catch (Exception e) {
                                }
                            }
                            if (input != null) {
                                try {
                                    input.close();
                                } catch (Exception e) {
                                }
                            }
                            if (httpConn != null) {
                                httpConn.disconnect();
                            }
                        }
                    } else {//send email
                    }
                }

            }
        };

        sendThread.start();
    }
}
