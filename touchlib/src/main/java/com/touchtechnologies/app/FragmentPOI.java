package com.touchtechnologies.app;

import java.util.ArrayList;

import com.touchtechnologies.dataobject.DataItem;
import com.touchtechnologies.lib.R;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentPOI extends Fragment{
	private ArrayList<DataItem> arrPlaceItems;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_poi, container, false);
		return view;
	}

	public void setPlaceItems(ArrayList<DataItem> arrPlaceItems){
		this.arrPlaceItems = arrPlaceItems;
	}
}
