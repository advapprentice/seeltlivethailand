package com.touchtechnologies.app;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.touchtechnologies.dataobject.Image;
import com.touchtechnologies.lib.R;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class GalleryFragment extends Fragment{
	private ArrayList<Image> arrImages;
	private LayoutInflater inflater;
	private ImageLoader imageLoader = ImageLoader.getInstance();
	private GridView gridView;
	private DisplayImageOptions dispImageOptions;
	private OnItemClickListener onItemClickListener;
	
	public void initDispImageOptions(int imgForEmptyUri, int imgOnFailed, int imgStub){ 
		
		dispImageOptions = new DisplayImageOptions.Builder()
		.showStubImage(imgStub)
		.showImageForEmptyUri(imgForEmptyUri)
		.showImageOnFail(imgOnFailed)
		.cacheInMemory()
		.cacheOnDisc()
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
		
		imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
		
	}
	
	public void setImages(ArrayList<Image> arrImages){
		this.arrImages = arrImages;
	}
	
	public void setNumColumns(int numColumns){
		gridView.setNumColumns(numColumns);
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		this.inflater = inflater;
		
		imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
		
		View view = inflater.inflate(R.layout.fragment_gallery, container, false);
		gridView = (GridView)view.findViewById(R.id.gridView);
		gridView.setAdapter(new GalleryAdapter());
		gridView.setOnItemClickListener(onItemClickListener);
		
		return view;
	}
	
	public void setOnItemClickListener(OnItemClickListener listener){
		this.onItemClickListener = listener;
		
		if(gridView != null){
			gridView.setOnItemClickListener(listener);
		}
	}
	
	class GalleryAdapter extends BaseAdapter{
		
		@Override
		public int getCount() {
			return arrImages == null?0:arrImages.size();
		}
		
		@Override
		public String getItem(int position) {
			return arrImages == null?null:arrImages.get(position).getThumbnail();
		}
		
		@Override
		public long getItemId(int position) {
			return position;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder viewHolder = null;
			
			if(convertView == null){
				convertView = inflater.inflate(R.layout.dataitem_gallery, parent, false);
				
				viewHolder = new ViewHolder();
				viewHolder.imageView = (ImageView)convertView.findViewById(R.id.imageView);
				
				convertView.setTag(viewHolder);
			
			}else{
				viewHolder = (ViewHolder)convertView.getTag();				
			}
			
			imageLoader.displayImage(getItem(position), viewHolder.imageView, dispImageOptions);			
			
			return convertView;
		}
		
		class ViewHolder{
			ImageView imageView;
		}
	}
}


