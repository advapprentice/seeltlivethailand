package com.touchtechnologies.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class CommonAlertDialog extends AlertDialog implements DialogInterface.OnClickListener{

	protected CommonAlertDialog(Context context) {
		super(context);		
	}
	
	public static CommonAlertDialog newInstance(Context context, String title, String message){
		CommonAlertDialog alertDialog = new CommonAlertDialog(context);
		
		if(title != null){
			alertDialog.setTitle(title);
		}
		
		if(message != null){
			alertDialog.setMessage(message);
		}
		
		return alertDialog;
	}
	
	/**
	 * Set button with default close on click
	 * @param whichButton
	 * @param text
	 */
	public void setButton(int whichButton, CharSequence text) {
		setButton(whichButton, text, this);
	}
	
	@Override
	public void onClick(DialogInterface dialog, int which) {
		dialog.dismiss();
	}	
}
