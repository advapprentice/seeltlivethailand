package com.touchtechnologies.app;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.touchtechnologies.command.Command;
import com.touchtechnologies.lib.R;
import com.touchtechnologies.net.ServiceResponse;

public class CommonServiceFragmentDialog extends DialogFragment implements View.OnClickListener{
	AsyncTask<Command, Void, ServiceResponse> asyncTask;
	
	String title;
	String message;
	String Button;
	boolean canceled;
	
	public void setAsyncTask(AsyncTask<Command, Void, ServiceResponse> asyncTask) {
		this.asyncTask = asyncTask;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getDialog().setContentView(R.layout.fragment_common_service_dialog);
				
		Bundle  argument = getArguments();
		title = argument.getString("title");
		message = argument.getString("message");
		Button = argument.getString("button");
		//set title
		if(title != null){
			getDialog().setTitle(title);
		}
		
		//sent text content, GONE if no content
		if(message != null){
			((TextView)getDialog().findViewById(R.id.textViewDialogMessage)).setText(message);
		
		}else{
			((TextView)getDialog().findViewById(R.id.textViewDialogMessage)).setVisibility(View.GONE);
			
		}
		setCancelable(false);
		if(Button != null){
	       Button btn =(Button)getDialog().findViewById(R.id.buttonCancelLogin);
	       btn.setText(Button);
	       btn.setOnClickListener(this);
		}else{
			getDialog().findViewById(R.id.buttonCancelLogin).setVisibility(View.GONE);
		}
	       
	       
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		canceled = true;
		if(asyncTask != null){
			asyncTask.cancel(true);
		}
	}
	
	
	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.buttonCancelLogin){
			getDialog().cancel();
			dismiss();
		}
	}
}