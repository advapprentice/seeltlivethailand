package com.touchtechnologies.app;

import com.touchtechnologies.lib.R;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Alert fragment which display simple title and message without button. 
 * User click on the dialog or press Back button to dissmiss.<br/>
 * Dialog title and message can pass through a set argument call. 
 * @author MEe
 *
 */
public class CommonFragmentDialog extends DialogFragment implements OnClickListener{
	String title;
	String message;

	@Override
	public void setArguments(Bundle args) {
		if(!args.containsKey("title") & args.containsKey("message")){
			throw new IllegalArgumentException("No \"title\" or \"message\" !");
		}
		super.setArguments(args);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getDialog().setContentView(R.layout.fragment_common_dialog);
				
		Bundle  argument = getArguments();
		title = argument.getString("title");
		message = argument.getString("message");
				
		//set title
		if(title != null){
			getDialog().setTitle(title);
		}
		
		//sent text content, GONE if no content
		TextView textView;
		if(message != null){
			textView = (TextView)getDialog().findViewById(R.id.textViewDialogMessage);
			textView.setText(message);
			
		}else{
			textView = (TextView)getDialog().findViewById(R.id.textViewDialogMessage);
			textView.setVisibility(View.GONE);
		}
		
		getDialog().findViewById(R.id.button).setOnClickListener(this);
		
		
		return super.onCreateView(inflater, container, savedInstanceState);
	}	

	@Override
	public void onClick(View v) {
		dismiss();
	}
}