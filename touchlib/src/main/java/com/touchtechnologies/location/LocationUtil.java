package com.touchtechnologies.location;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;

import com.touchtechnologies.app.LibConfig;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.provider.LiveFolders;
import android.util.Log;

public class LocationUtil {
	private static NumberFormat numFormat;
	private static Location loc1, loc2;
	
	static{
		numFormat = NumberFormat.getIntegerInstance(Locale.ENGLISH);
		numFormat.setMaximumFractionDigits(2);
		
		loc1 = new Location(LocationManager.GPS_PROVIDER);
		loc2 = new Location(LocationManager.GPS_PROVIDER);
	}
	
	private LocationUtil(){		
	}
	
	public static String getFormatedDistance(Location loc1, double lat2, double lon2){
		loc2.setLatitude(lat2);
		loc2.setLongitude(lon2);
		
		return getFormatedDistance(loc1, loc2);
	}
	
	public static String getFormatedDistance(double lat1, double lon1, double lat2, double lon2){
		loc1.setLatitude(lat1);
		loc1.setLongitude(lat2);
		
		loc2.setLatitude(lat2);
		loc2.setLongitude(lon2);
		
		return getFormatedDistance(loc1, loc2);
	}
	
	public static String getFormatedDistance(Location loc1, Location loc2){
		StringBuffer builder = new StringBuffer();
		
		if(loc1 == null || loc2 == null){
			builder.append("N/A");
		}else{
			double distance = (double)loc1.distanceTo(loc2);
			
			builder.append(numFormat.format(distance/1000));
			builder.append(' ');
			builder.append("Km");
		}
		
		return builder.toString();
	}
	
	/**
	 * Example
	 * {
	 *	  		  "countryName": "ประเทศไทย",
	 *			  "postalCode": "10310",
	 *			  "countryCode": "TH",
	 *			  "addressLines": "ถนน พระราม 9 แขวง ห้วยขวาง เขต ห้วยขวาง กรุงเทพมหานคร 10310 ",
	 *			  "locality": "เขต ห้วยขวาง",
	 *			  "longitude": 100.5781958,
	 *			  "provinceName": "กรุงเทพมหานคร",
	 *			  "latitude": 13.7550465
	 * 	}
	 * @param context
	 * @param location
	 * @param handler
	 * @return
	 */
	public static JSONObject getAddressFromLocation(Context context, Location location, Handler handler) {
		Geocoder geocoder = new Geocoder(context, Locale.getDefault());
		
		JSONObject json = new JSONObject();
		try {
			List<Address> list = geocoder.getFromLocation(
					location.getLatitude(), location.getLongitude(), 1);
			if (list != null && list.size() > 0) {
				Address address = list.get(0);
				// sending back first address line and locality

				StringBuilder builder = new StringBuilder();
				for(int index=0; index < address.getMaxAddressLineIndex(); index++){
					builder.append(address.getAddressLine(index));
					builder.append(" ");
				}
				json.put("addressLines", builder.toString());
				json.put("provinceName", address.getAdminArea());
				json.put("locality", address.getLocality());
				json.put("postalCode", address.getPostalCode());
				json.put("countryName", address.getCountryName());
				json.put("countryCode", address.getCountryCode());
				if(address.hasLatitude()){
					json.put("latitude", address.getLatitude());
				}
				if(address.hasLongitude()){
					json.put("longitude", address.getLongitude());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(LibConfig.LOGTAG, "convet geocoder error", e);
		}
		
		return json;
		
	}
}
