package com.touchtechnologies.location;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.touchtechnologies.location.dataobject.AccessPoint;
import com.touchtechnologies.location.dataobject.FingerPrint;
import com.touchtechnologies.location.dataobject.Position;
import com.touchtechnologies.location.persistent.DBLocationUtil;

public final class MicroLocationManager implements LocationListener, SensorEventListener, WiFiFilter{
	static final String TAG = "MLocationManager";
	/** Minimum access point's rssi level to add into finger print database*/ 
	final int RSSI_THRESHOLD = -80;
	
	/** Maximum number of access point to compare with finger print */
	final int MAX_ACCESSPOINT_COMPARE = 3;
	
	final int RSSI_DIFF = 6;
	
	/** A number of WiFi scan request */
	final int SCAN_MAX = 3;	

	final int LOCATION_MINTIME = 1500;//x seconds
	final int LOCATION_MINDISTANCE = 1;//1 meter
	final int LOCATION_TIMEOUT = 2 * 60 * 1000;//2 minutes
	
	final String URI_SNMP = "/develop/test/SNMPCaller/1.3.1/SNMPCallerService.php?callRuckusGetAccesspointClientList";
		
	private static MicroLocationManager mclManager;
	private static final String SSID_START[] = new String[]{"@tot_wi-fi"/*, "Nanar", "bella_cctv", "@TRUEWIFI"*/}; 
//	private static final String MAC_ADDRESSES[] = new String[]{
//		"00:02:6F:D0:58:A0","00:02:6F:D0:58:A8","00:02:6F:D0:58:A4","00:02:6F:D0:58:9C"
//	};
	private WiFiFilter wifiFilter;
		
	private FingerprintSensorListener listener;
	private Context context;
	
	private BroadcastReceiver broadReceiver;
	private IntentFilter intentFilter;
	private LocationManager locManager;
	private SensorManager sensorManager;
	private Sensor sensorMagnetic, sensorOrientation;
	private TelephonyManager telephonyManager;
	private WifiManager wManager;
	private LocationDataUtil locUtil;
	
	private Vector<FingerPrint> vFingerprint;
	private Vector<AccessPoint> vFoundAccessPoints;	
	private FingerPrint.EuclideanComparator fpEuComparator;
	private Timer timer;
	/** Minimum time in milliseconds */
	private int minTime;
	private int minDistance;
	/** True if run in test mode */
	private boolean testMode;
	
	private int optimizedLostCount;
	private final Hashtable<Position, Integer> optimizedHashPos;
	private Position optimizedLastKnownPosition;
	
	private int scanCount = 0;
			
	public static MicroLocationManager getInstance(Context context){
		if(mclManager == null){
			mclManager = new MicroLocationManager(context);	
		}
				
		return mclManager;
	}
	
	public static MicroLocationManager getTestInstance(Context context, String asset){
		mclManager = getInstance(context);
		
		mclManager.testMode = true;
		
		return mclManager;
	}
	
	private MicroLocationManager(Context context){
		this.context = context;		
		this.wifiFilter = this;
		
		locManager  		= (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);		
		telephonyManager 	= (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);		
		wManager 			= (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
						
		sensorManager 		= (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		sensorMagnetic 		= sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		sensorOrientation	= sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
				
		//Orientation
		sensorManager.registerListener(this, sensorOrientation, SensorManager.SENSOR_DELAY_FASTEST);
				
		intentFilter 		= new IntentFilter();
		intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
		
		fpEuComparator 		= new FingerPrint().new EuclideanComparator();
		locUtil 			= new LocationDataUtil(context);
		
		DBLocationUtil sql 	= new DBLocationUtil(context);
		vFingerprint 			= sql.getFingerPrints();		
		sql.close();
		
		optimizedHashPos 	= new Hashtable<Position, Integer>();
		
		broadReceiver 		= new BroadcastReceiver() {			
			@Override
			public void onReceive(Context context, Intent intent) {	
				List<ScanResult> lScanResults = wManager.getScanResults();				
				if(listener != null){
					Position position = null;
					
					/*
					 * 1/3 Notify onScanCompleted(List<ScanResult> ..)
					 */
					if(wifiFilter == null){
						if(listener != null){
							listener.onScanCompleted(lScanResults);	
						}
					
					}else{
						List<ScanResult> filterredList = new ArrayList<ScanResult>();
						for(ScanResult rs: lScanResults){
							if(wifiFilter.accept(rs)){
								filterredList.add(rs);
							}
						}						
						
						if(listener != null){
							listener.onScanCompleted(filterredList);
						}
						
						/*
						 *Set scan results set to a filtered version if WiFiFilter is set.  
						 */
						lScanResults = filterredList;
					}
					
					/*
					 * Create access point objects to use in calculation process
					 */
					final Vector<AccessPoint> vFoundAccessPoints = new Vector<AccessPoint>();
					for (ScanResult scanResult : lScanResults) {			
						AccessPoint ap = new AccessPoint();
						ap.setMac(scanResult.BSSID);
						ap.setRssi(scanResult.level);
						ap.setSsid(scanResult.SSID);
						ap.setFreq(scanResult.frequency);

						vFoundAccessPoints.addElement(ap);		
					}
					
					mclManager.scanCount++;
					if(mclManager.scanCount == 1){
						mclManager.vFoundAccessPoints = vFoundAccessPoints;
						
					}else{
						int index;
						int rssi;
						for(AccessPoint ap: vFoundAccessPoints){
							index = mclManager.vFoundAccessPoints.indexOf(ap);
							if(index >= 0){
								rssi = (ap.getRssi() + mclManager.vFoundAccessPoints.elementAt(index).getRssi())/2;
								ap.setRssi(rssi);
							}
						}
						
						if(scanCount >= SCAN_MAX){		
							scanCount = 0;
							Object[] obj = mclManager.getApproximateLocation(vFoundAccessPoints, "", false);				

							/*
							 * 2/3 Notify onScanFingerprintCompleted((List<FingerPrint>) ..)
							 */
							if(listener != null){
								listener.onScanFingerprintCompleted((List<FingerPrint>) obj[1]);
							}
							
							/*
							 * 3/3 Notify onScanFingerprintCompleted((List<FingerPrint>) ..)
							 */
							position = (Position)obj[0];
							onLocationChanged(position);
						}						
					}
				}				
				wManager.startScan();
			}
		};
	}
	
	public void setMinLocationTime(int minTime){
		this.minTime = minTime;
	}
	
	public void requestLocationUpdates(FingerprintSensorListener listener){
		this.listener = listener;
		
		locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		
		Location loc = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		onLocationChanged(loc);
		
		/*
		 * Register SensorEventListner
		 */
		//Magnetic
		//sensorManager.registerListener(this, sensorMagnetic, SensorManager.SENSOR_DELAY_UI);
		
		
		//register receiver for WiFi
		context.registerReceiver(broadReceiver, intentFilter);		
		
		//cancel previous timer if exist
		if(timer != null){
			timer.cancel();
		}
		
		wManager.startScan();		
		
		//start WiFi & Cell scan thread
		timer = new Timer();
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				wManager.startScan();				
				
				CellLocation cellLocation = telephonyManager.getCellLocation();
				List<NeighboringCellInfo> lNeighborCellInfos = telephonyManager.getNeighboringCellInfo();
				if(MicroLocationManager.this.listener != null){
					MicroLocationManager.this.listener.onScanCompleted(cellLocation, lNeighborCellInfos);	
				}				
			}
		}, 0, minTime);
	}
	
	
	public void unregisterReceiver(){
		if(timer !=  null){
			timer.cancel();
			timer = null;
		}		
		
		locManager.removeUpdates(this);		
		//sensorManager.unregisterListener(this, sensorMagnetic);		
		context.unregisterReceiver(broadReceiver);
	}	
	
	@Override
	protected void finalize() throws Throwable {
		try{
			sensorManager.unregisterListener(this, sensorOrientation);
		}catch(Exception e){			
		}
		
		super.finalize();
	}
	
	public void setWifiFilter(WiFiFilter wifiFilter) {
		this.wifiFilter = wifiFilter;
	}
	
	@Override
	public void onLocationChanged(Location location) {
		if(listener != null){
			listener.onLocationChanged(location);
		}
	}
	
	@Override
	public void onProviderDisabled(String provider) {
		if(listener != null){
			listener.onProviderDisabled(provider);
		}
	}
	
	@Override
	public void onProviderEnabled(String provider) {
		if(listener != null){
			listener.onProviderEnabled(provider);
		}		
	}
	
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		if(listener != null){
			listener.onStatusChanged(provider, status, extras);
		}
	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {	
		
	}
	
	@Override
	public void onSensorChanged(SensorEvent event) {
		switch(event.accuracy){
		case SensorManager.SENSOR_STATUS_ACCURACY_HIGH:		
		case SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM:
			if(listener != null){
				switch(event.sensor.getType()){
//				case Sensor.TYPE_ROTATION_VECTOR:
				case Sensor.TYPE_ORIENTATION:
				case Sensor.TYPE_MAGNETIC_FIELD:	
					listener.onSensorChanged(event);
				}
			}
			break;		
		case SensorManager.SENSOR_STATUS_ACCURACY_LOW:
			//Discard low accuracy
			break;					
		}		
	}
	
	/**
	 * Finger print
	 */
	public Vector<FingerPrint> getFingerPrints(){
		return vFingerprint;
	}
	
	public void setFingerPrints(Vector<FingerPrint> vFingerPrints){
		this.vFingerprint = vFingerPrints;
	}
	
	@Override
	public boolean accept(ScanResult scr) {
		boolean accept = false;
		for(int i=0; i<SSID_START.length; i++){
			if(scr.SSID.toLowerCase().contains(SSID_START[i])){
				accept = true;
				//Log.d(TAG, "accept invoked >" + scr.SSID.toLowerCase());
				break;
			}
		}		
		return accept;
	}	
	
	public synchronized Object[] getApproximateLocation(Vector<AccessPoint> vApFound, final String pos, boolean save){
		Object object[] = new Object[2];
		Position position = null;
				
		/*
		 * Sort AccessPoint using rssi, descending order.
		 * Most strength rssi come first.
		 */
		Collections.sort(vApFound);		
		
		printf(vApFound);
		
		/*
		 * Looking for BEST 4 probability finger prints.
		 */
		final List<FingerPrint> lFingFiltered = new ArrayList<FingerPrint>();
		for(FingerPrint fingerprint: vFingerprint){		
			
			/*
			 * Calculate only adjoin position, skip otherwise
			 */
//			if(optimizedLastKnownPosition != null){
//				if(!optimizedLastKnownPosition.isAdjoin(fingerprint.getPosition())){
//					continue;
//				}
//			}
			
			if(!fingerprint.isMatch(vApFound, FingerPrint.MATCH_CRITERIA_SIGNAL_PATTERN)){
				continue;
			}
			
			if(getEuclideanDistance(fingerprint, vApFound) != -1){
				lFingFiltered.add(fingerprint);
			}			
		}
				
		if(lFingFiltered.size() > 0){
			/*
			 * Sort Fingerprint using Euclidean distance, ascending order.  
			 * Shortest distance come first. 
			 */
			Collections.sort(lFingFiltered, fpEuComparator);
			
			int size = lFingFiltered.size();
			for(int i = size - 1; i >= 4; i-- ){
				lFingFiltered.remove(i);
			}
			
			position = lFingFiltered.get(0).getPosition();
			
			Log.d(TAG, "MicroLocation.getApproximateLocation ->");	
			for(FingerPrint fingerprint: lFingFiltered){
				Log.d(TAG, fingerprint.toString());
			}
		}else{
			Log.d(TAG, "No location found");
		}
		
		if(save){
			Thread thSave = new Thread(){
				public void run(){
					try{
						locUtil.writeWalktestData(lFingFiltered, pos);
					}catch(Exception e){
						Log.e(TAG, "Save fine walktest ", e);
					}
				}
			};		
			thSave.start();
		}
			
		/*
		 *Optimization  
		 */
		if(position == null){
			optimizedLostCount++;			
			if(optimizedLostCount * LOCATION_MINTIME >= LOCATION_TIMEOUT){
				optimizedLastKnownPosition = null;
				optimizedHashPos.clear();
				return object;
			}else{
				object[0] = optimizedLastKnownPosition;
				object[1] = lFingFiltered;
				return object;
			}
		}else{
			optimizedLostCount = 0;
			optimizedLastKnownPosition = position;			
		}

		object[0] = position;
		object[1] = lFingFiltered;
		
		return object;
	}
	
	/**
	 * Calculate Euclidean distance and set it to Fingerprint's eu.
	 * @param fingerprint
	 * @param lApFound
	 * @return -1 if can't calculate Euclidean distacne.
	 */
	public double getEuclideanDistance(FingerPrint fingerprint, Vector<AccessPoint> lApFound){		
		double distanceSum = 0;
		
		int foundCount = 0;
		AccessPoint apMatched;
		for(AccessPoint ap: lApFound){
			if((apMatched = fingerprint.match(ap)) != null){
				foundCount++;
				distanceSum += Math.pow(Math.abs(ap.getRssi() - apMatched.getRssi()), 2);
			}
			if(foundCount == MAX_ACCESSPOINT_COMPARE){
				break;
			}
		}
		
		double euclidean = 0;
		if(foundCount >= MAX_ACCESSPOINT_COMPARE){
			euclidean = Math.sqrt(distanceSum); 
		}else{
			euclidean = -1;
		}
		
		fingerprint.setEuclidean(euclidean);
		fingerprint.setMatchedAccesspoint(lApFound);
		
		return euclidean;
	}
	
	void printf(Vector<AccessPoint> aps){
		StringBuilder stBuilder = new StringBuilder();
		for(AccessPoint ap: aps){
			stBuilder.append(ap.toString());
			stBuilder.append("\r\n");
		}
		
		Log.d(TAG, "AP FOUND >>\r\n" + stBuilder.toString());
	}
}
