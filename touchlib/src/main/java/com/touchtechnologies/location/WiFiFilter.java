package com.touchtechnologies.location;

import android.net.wifi.ScanResult;

public interface WiFiFilter {
	boolean accept(ScanResult scr);	
}
