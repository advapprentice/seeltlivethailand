package com.touchtechnologies.location;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Environment;

import com.touchtechnologies.location.dataobject.AccessPoint;
import com.touchtechnologies.location.dataobject.FingerPrint;
import com.touchtechnologies.location.dataobject.Position;
import com.touchtechnologies.log.Log;
import com.touchtechnologies.location.persistent.DBLocationMetaData;
import com.touchtechnologies.location.persistent.DBLocationMetaData.TABLE;
import com.touchtechnologies.location.persistent.DBLocationUtil;

public class LocationDataUtil {
	final String TAG = "LOCATION-UTIL";
	
	/** Root directory use to store location files */
	final String DIR_BASE = Environment.getExternalStorageDirectory().getAbsolutePath()+"/touchtechnologies.co.th";
	final String DIR_ROOT = DIR_BASE + "/fp";
	final String DIR_ROOT_EXPORT = DIR_BASE + "/exported";
	final String DIR_ROOT_WALKTEST = DIR_BASE + "/walktest";	
	
	private static String FILENAME_FINGERPRINT = "fpTab7";
	private static String FILENAME_ACCESSPOINT = "apTab7";
	private static final String FILENAME_CATEGORY = "cat";
	private static final String FILENAME_SHOP = "shop";
	private static String FILENAME_POSITION = "posTab7";
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.US);
	
	static{
		String model = Build.MODEL.trim();
		
		if(model.equals("SM-T211")){ //tab7
			FILENAME_FINGERPRINT = "fpTab7";
			FILENAME_ACCESSPOINT = "apTab7";
			FILENAME_POSITION = "posTab7";
			
			
		}else if(model.equals("SM-N900")){//??Note พี่จ้าว
			FILENAME_FINGERPRINT = "fpNote4";
			FILENAME_ACCESSPOINT = "apNote4";
			FILENAME_POSITION = "posNote4";
			
		}else if(model.equals("GT-N7100")){//My S2
			FILENAME_FINGERPRINT = "fpII";
			FILENAME_ACCESSPOINT = "apII";
			FILENAME_POSITION = "posII";
			
		}else{
			FILENAME_FINGERPRINT = "fpII";
			FILENAME_ACCESSPOINT = "apII";
			FILENAME_POSITION = "posII";
			
		}
	}
	
	private Context context;
	
	public LocationDataUtil(Context context) {
		this.context = context;
	}
	
	/**
	 * Parsing access point data from asset.
	 * @return a Vector contains AccessPoints
	 */
	public List<AccessPoint> parsingAccessPointFromAsset(){
		List<AccessPoint> lAccessPoints = new ArrayList<AccessPoint>();
		
		InputStream is = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		DBLocationUtil sqlObj = null;
		try{
			
			sqlObj = new DBLocationUtil(context);
			SQLiteDatabase sql = sqlObj.getDatabase();
			
			is = context.getAssets().open(FILENAME_ACCESSPOINT);
			isr = new InputStreamReader(is);			
			br = new BufferedReader(isr);
			
			String lineRead;
			String apID, apMAC, apSSID, apFreq, apLat, apLon;
			
			int lineCount = 0;
			while((lineRead = br.readLine()) != null){
				android.util.Log.d(TAG, "AccessPoint text: " + lineRead);
				
				if(lineCount++ == 0){//skip column names
					continue;
				}
				
				//Line red: _id, mac, ssid, lat, lon, freq
				
				StringTokenizer stk = new StringTokenizer(lineRead, ",");
								
				apID   = stk.nextElement().toString();
				apMAC  = stk.nextElement().toString();
				apSSID = stk.nextElement().toString();
				apLat  = stk.nextElement().toString();
				apLon  = stk.nextElement().toString();
				apFreq = stk.nextElement().toString();
				
				AccessPoint ap = new AccessPoint();
				ap.set_id(Integer.parseInt(apID));
				ap.setMac(apMAC);
				ap.setSsid(apSSID);
				ap.setFreq(Integer.parseInt(apFreq));
				ap.setLat(apLat);
				ap.setLon(apLon);
				
				lAccessPoints.add(ap);
				
				//Insert into database
				ContentValues cv = new ContentValues();
				cv.put(DBLocationMetaData.ACCESSPOINT_ID, ap.getID());
				cv.put(DBLocationMetaData.ACCESSPOINT_SSID, ap.getSsid());
				cv.put(DBLocationMetaData.ACCESSPOINT_MAC, ap.getMac());
				cv.put(DBLocationMetaData.ACCESSPOINT_FREQ, ap.getFreq());
				cv.put(DBLocationMetaData.ACCESSPOINT_LAT, ap.getLat());
				cv.put(DBLocationMetaData.ACCESSPOINT_LON, ap.getLon());
				
				sql.insert(TABLE.ACCESSPOINT.getName(), null, cv);
			}			
			
			android.util.Log.d(TAG,  lAccessPoints.size() + " AccessPoint(s) were created from asset.");	
		}catch(Exception e){
			android.util.Log.e(TAG, "parsingAccessPointFromAsset", e);			
			
		}finally{
			try{
				if(is != null) is.close();
			}catch(Exception ee){
				
			}
			try{
				if(isr != null) isr.close();
			}catch(Exception ee){
				
			}
			try{
				if(br != null) br.close();
			}catch(Exception ee){
				
			}
			
			if(sqlObj != null){
				sqlObj.close();
			}
		}		
		
		return lAccessPoints;
	}
	
	/**
	 * Parsing position data from asset.
	 * @return a Vector contains Positions
	 */
	public List<Position> pasringPositionFromAsset(){
		List<Position> lPositions = new ArrayList<Position>();
		
		InputStream is = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		
		DBLocationUtil sqlObj = null;
		String lineRead = null;
		try{			
			sqlObj = new DBLocationUtil(context);
			SQLiteDatabase sql = sqlObj.getDatabase();
			
			is = context.getAssets().open(FILENAME_POSITION);
			isr = new InputStreamReader(is);			
			br = new BufferedReader(isr);
			
			
			String posID, posX, posY, posLat, posLon, posFloor, posName;
			
			int lineCount = 0;
			while((lineRead = br.readLine()) != null){
				android.util.Log.d(TAG, "Position text: " + lineRead);
				if(lineCount++ == 0){//skip column name
					continue;
				}
				
				//_id, x, y, lat, lon, name, floor
				
				StringTokenizer stk = new StringTokenizer(lineRead, ",");
				posID   = stk.nextElement().toString();
				posX   	= stk.nextElement().toString();
				posY  	= stk.nextElement().toString();
				posLat 	= stk.nextElement().toString();
				posLon  = stk.nextElement().toString();
				posName = stk.nextElement().toString();
				posFloor= stk.nextElement().toString();
				
				//add to Array
				Position pos = new Position(posID);
				pos.setName(posName);
				pos.setX(Integer.parseInt(posX));
				pos.setY(Integer.parseInt(posY));
				pos.setFloor(posFloor);
				pos.setLatitude(Double.parseDouble(posLat));
				pos.setLongitude(Double.parseDouble(posLon));
				
				lPositions.add(pos);		
				
				ContentValues cv = new ContentValues();
				cv.put(DBLocationMetaData.POSITION_ID, pos.getID());
				cv.put(DBLocationMetaData.POSITION_X, pos.getX());
				cv.put(DBLocationMetaData.POSITION_Y, pos.getY());
				cv.put(DBLocationMetaData.POSITION_NAME, pos.getName());
				cv.put(DBLocationMetaData.POSITION_FLOORLEVEL, pos.getFloor());
				cv.put(DBLocationMetaData.POSITION_LAT, pos.getLatitude());
				cv.put(DBLocationMetaData.POSITION_LON, pos.getLongitude());
				
				//insert data
				sql.insert(TABLE.POSITION.getName(), null, cv);
			}
			
			android.util.Log.d(TAG,  lPositions.size() + " Point(s) were created from asset!");	
		}catch(Exception e){
			android.util.Log.e(TAG, "pasringPositionFromAsset " + lineRead, e);
			
		}finally{
			try{
				if(is != null) is.close();
			}catch(Exception ee){
				
			}
			try{
				if(isr != null) isr.close();
			}catch(Exception ee){
				
			}
			try{
				if(br != null) br.close();
			}catch(Exception ee){
				
			}
			
			if(sqlObj != null){
				sqlObj.close();
			}
		}
		
		return lPositions;
	}

	/**
	 * Parsing Fingerprint data from asset. 
	 * Set Fingerprint's Position and add all AccessPoint objects to Fingerprint object.
	 * Also clone AccessPoint object and then set AccessPoint's rssi specific values for each finger print. 
	 * @param lAccessPoints
	 * @param lPositions
	 * @return
	 */
	public List<FingerPrint> parsingFingerprintFromAsset(List<AccessPoint> lAccessPoints, List<Position> lPositions){
		List<FingerPrint> lFingerPrints = new ArrayList<FingerPrint>();
		
		InputStream is = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		
		DBLocationUtil sqlObj = null;
		
		try{			
			sqlObj = new DBLocationUtil(context);
			SQLiteDatabase sql = sqlObj.getDatabase();
			
			is = context.getAssets().open(FILENAME_FINGERPRINT);
			isr = new InputStreamReader(is);			
			br = new BufferedReader(isr);
			
			int lineCount = 0;
			String lineRead;
			int fpID, apID, posID, minRssi, maxRssi, normRssi, snmpRssi;
			String lat, lon;
			
			while((lineRead = br.readLine()) != null){
				android.util.Log.d(TAG, "Fingerprint text: " + lineRead);
				if(lineCount++ == 0){
					continue;
				}
				
				//_id, pos_id, ap_id, rssi, rssi_min, rssi_max, rssi_snmp, lat, lon
				
				StringTokenizer stk = new StringTokenizer(lineRead, ",");
				fpID		= Integer.parseInt(stk.nextElement().toString());
				posID     	= Integer.parseInt(stk.nextElement().toString());
				apID   	 	= Integer.parseInt(stk.nextElement().toString());
				normRssi    = Integer.parseInt(stk.nextElement().toString());		
				minRssi 	= Integer.parseInt(stk.nextElement().toString());
				maxRssi 	= Integer.parseInt(stk.nextElement().toString());
				snmpRssi	= Integer.parseInt(stk.nextElement().toString());	
				lat			= stk.nextElement().toString();
				lon			= stk.nextElement().toString();
								
				FingerPrint fp = new FingerPrint();
				fp.set_id(fpID);
								
				for(AccessPoint ap: lAccessPoints){
					if(ap.getID() == apID){
						android.util.Log.d(TAG, "Source:" + ap);						
						ap = (AccessPoint)ap.clone();
						
						android.util.Log.d(TAG, "Clone:" + ap);						
						
						ap.setRssi(normRssi);
						ap.setRssiMax(maxRssi);
						ap.setRssiMin(minRssi);
						ap.setRssiSNMP(snmpRssi);
						
						fp.add(ap);
					}
				}
				
				for(Position pos: lPositions){
					if(pos.getID() == posID){
						fp.setPosition(pos);
						break;
					}
				}				
				
				ContentValues cv = new ContentValues();
				cv.put(DBLocationMetaData.FINGERPRINT_ID, fp.getID());
				cv.put(DBLocationMetaData.FINGERPRINT_AP_ID, apID);
				cv.put(DBLocationMetaData.FINGERPRINT_POS_ID, posID);
				cv.put(DBLocationMetaData.FINGERPRINT_RSSI, normRssi);
				cv.put(DBLocationMetaData.FINGERPRINT_RSSI_MAX, maxRssi);
				cv.put(DBLocationMetaData.FINGERPRINT_RSSI_MIN, minRssi);
				cv.put(DBLocationMetaData.FINGERPRINT_RSSI_SNMP, snmpRssi);
				cv.put(DBLocationMetaData.FINGERPRINT_LAT, lat);
				cv.put(DBLocationMetaData.FINGERPRINT_LON, lon);
				
				//insert data
				sql.insert(TABLE.FINGERPRINT.getName(), null, cv);				
			}	
						
			android.util.Log.d(TAG, lFingerPrints.size() + " Fingerprint(s) were created from asset.");	
		}catch(Exception e){			
			android.util.Log.e(TAG, "InitFongerprintAsset", e);
			
		}finally{
			try{
				if(is != null) is.close();
			}catch(Exception ee){
				
			}
			
			try{
				if(isr != null) isr.close();
			}catch(Exception ee){
				
			}
			
			try{
				if(br != null) br.close();
			}catch(Exception ee){
				
			}
			
			if(sqlObj != null){
				sqlObj.close();
			}
		}		
		
		return lFingerPrints;
	}
	
	static final class FilenameLocation implements FilenameFilter{
		private String startName;
		public FilenameLocation(String startName) {
			
		}
		
		@Override
		public boolean accept(File dir, String filename) {			
			return filename.startsWith(startName);
		}
	}
	
	/**
	 * Save a walk test data to file.
	 * @param posName An manual input position name 
	 * @param pos a best matched Position that return from a location provider. 
	 * @param vAp a Vector contains AccessPoint objects return from WiFi scanner. 
	 * @throws IOException
	 */
	public void writeWalktestData(String posName, Position pos, Vector<AccessPoint> vAp) throws IOException{
		File file = new File(DIR_ROOT_WALKTEST);
		FileWriter fw = null;
		
		android.util.Log.d(TAG, "saving file");
		try{
			if(!file.exists()){
				file.mkdirs();
			}
			
			file = new File(DIR_ROOT_WALKTEST + "/wk_" + System.currentTimeMillis()+ ".txt");			
			fw = new FileWriter(file);
			
			fw.write("Real pos: " + posName);
			fw.write("\r\n");
			
			if(pos != null){
				fw.write(pos.getName());
			}else{
				fw.write("Not found!");
			}
			fw.write("\r\n");
			
			for(int i=0; i<vAp.size(); i++){
				fw.write(vAp.elementAt(i).toString());
				fw.write("\r\n");
			}
			
			fw.flush();
			
			android.util.Log.d(TAG, "completed.");
		}catch(Exception e){			
			android.util.Log.e(TAG, "writeWalktestData", e);
			throw new IOException(e.getMessage());
		}finally{
			
			if(fw != null){
				try{
					fw.close();
				}catch(Exception ee){					
				}
			}
		}
	}
	
	public void writeWalktestData(List<FingerPrint> lFingerPrints, String position) throws IOException{
		File file = new File(DIR_ROOT_WALKTEST);
		FileWriter fw = null;
		
		android.util.Log.d(TAG, "saving file");
		try{
			if(!file.exists()){
				file.mkdirs();
			}
			
			file = new File(DIR_ROOT_WALKTEST + "/wkdebug_" + sdf.format(new Date(System.currentTimeMillis())) + ".txt");			
			fw = new FileWriter(file);
			
			int count = 1;
			for(FingerPrint fingerPrint: lFingerPrints){
				fw.write("" + count++);
				fw.write("\tEuclidean: ");
				fw.write(fingerPrint.getEuclidean() + "");
				fw.write("\r\n");
				fw.write(fingerPrint.toString());
				fw.write("-----------\r\n");
				fw.flush();
			}
			
			if(position != null){
				fw.write("User input=" + position );
			}
			android.util.Log.d(TAG, "completed.");
		}catch(Exception e){			
			android.util.Log.e(TAG, "writeWalktestData", e);
			throw new IOException(e.getMessage());
		}finally{
			
			if(fw != null){
				try{
					fw.close();
				}catch(Exception ee){					
				}
			}
		}	
		
	}
	
	public int updatePositionLocation(Context context, String[][] pos){
		int effect = 0;
		
		DBLocationUtil sqlObj = new DBLocationUtil(context);
		SQLiteDatabase sql = sqlObj.getDatabase();
		for(int i=0; i<pos.length; i++){
			ContentValues cv = new ContentValues();
			cv.put(DBLocationMetaData.POSITION_X, pos[i][1]);
			cv.put(DBLocationMetaData.POSITION_Y, pos[i][2]);
			
			int rowEffect = sql.update(TABLE.POSITION.getName(), cv, 
					DBLocationMetaData.POSITION_NAME + " LIKE '"+ pos[i][0] +"%'", null);
			
			if(rowEffect != -1){
				effect++;
			}
		}
		
		sqlObj.close();
		
		return effect;
	}
	
	/**
	 * Export Fingerprint's toString() to file.
	 * @throws IOException
	 */
	public void exportFingerprintObjectToFiles() throws IOException{
		File file = new File(DIR_ROOT);
		FileWriter fw = null;
		
		DBLocationUtil sq = new DBLocationUtil(context);
		Vector<FingerPrint> vFingerPrints = sq.getFingerPrints();
		Collections.sort(vFingerPrints.subList(0, vFingerPrints.size()));
		
		sq.close();
		try{
			if(!file.exists()){
				file.mkdirs();
			}
			
			file = new File(DIR_ROOT_EXPORT + "/FingerprintObject.txt");			
			fw = new FileWriter(file);
			
			for(FingerPrint fp: vFingerPrints){
				fw.write(fp.toString());				
				fw.flush();
			}
		}catch(Exception e){
			android.util.Log.e(TAG, "exportFingerprintObjectToFiles", e);
			throw new IOException(e.getMessage());
		}finally{
			
			if(fw != null){
				try{
					fw.close();
				}catch(Exception ee){					
				}
			}
		}		
	}	
	
	
	/**
	 * Export all databases data to files.
	 * @throws IOException
	 */
	public void exportDatabaseToFiles() throws IOException{
		DBLocationUtil sqDbo = new DBLocationUtil(context);
		SQLiteDatabase sqDb = sqDbo.getDatabase();
		
		Cursor cursor =  null;
		String table = null;
		
		TABLE tables[] = TABLE.values();
		for(int i=0; i<tables.length; i++){
			table = tables[i].getName();
			cursor = sqDb.rawQuery("SELECT * FROM " + table, null);
			writeToFileImpl(table, cursor);
		}
		
		sqDbo.close();
	}
	
	
	
	/**
	 * Write a Cursor data to file.
	 * @param filename file name to write content to.
	 * @param cursor
	 * @throws IOException
	 */
	private void writeToFileImpl(String filename, Cursor cursor) throws IOException{
		File file = new File(DIR_ROOT_EXPORT);
		FileWriter fw = null;
		
		StringBuilder debugBuff = new StringBuilder();
		try{
			if(!file.exists()){
				file.mkdirs();
			}
			
			file = new File(DIR_ROOT_EXPORT + "/" + filename + ".txt");			
			fw = new FileWriter(file);
			
			String tmp;
			int columnCount = cursor.getColumnCount();	
			
			//write column name
			for(int i=0; i < columnCount; i++){
				debugBuff.append(cursor.getColumnName(i));
				if(i<columnCount - 1){
					debugBuff.append(", ");
				}
			};
			fw.write(debugBuff.toString());
			fw.write("\r\n");
			android.util.Log.d(TAG, debugBuff.toString());
			
			//write data of each column
			while(cursor.moveToNext()){	
				debugBuff.delete(0, debugBuff.length());
				
				for(int i=0; i<columnCount; i++){
					tmp = cursor.getString(i);
					debugBuff.append(tmp);
					
					fw.write(tmp == null?"": tmp);
					
					if(i<columnCount - 1){
						fw.write(",");						
						debugBuff.append(",");
					}
				}
				fw.write("\r\n");				
				
				android.util.Log.d(TAG, debugBuff.toString());				
			}			
			fw.flush();			
						
		}catch(Exception e){
			android.util.Log.e(TAG, "writeToFileImpl", e);
			throw new IOException(e.getMessage());
		}finally{
			if(cursor != null){
				cursor.close();
			}
			
			if(fw != null){
				try{
					fw.close();
				}catch(Exception ee){					
				}
			}
		}
	}	
	
}
