package com.touchtechnologies.location.persistent;


public interface DBLocationMetaData {
	int VERSION = 1;
	String DBNAME = "locdb";
	
	/**
	 * Application columns name
	 */
	String APPLICATION_SITEID = "site_id";
	String APPLICATION_LATITUDE = "lat";
	String APPLICATION_LONGITUDE = "lon";
	String APPLICATION_TITLE = "title";
	String APPLICATION_MAPVERSION = "mapversion";
	String APPLICATION_VIEW_COUNT = "view_count";
	String APPLICATION_CATEGORIES_UPDATE = "categories_update";
	String APPLICATION_SHOP_UPDATE = "shop_update";
	String APPLICATION_LANGUAGE = "language";
	
	
	/*
	 * Access point columns name
	 */
	String  ACCESSPOINT_ID = "_id";
	String 	ACCESSPOINT_MAC = "mac";
	String 	ACCESSPOINT_SSID = "ssid";
	String 	ACCESSPOINT_LAT = "lat";
	String 	ACCESSPOINT_LON = "lon";
	String 	ACCESSPOINT_FREQ = "freq";

	/*
	 * Position columns name
	 */
	String 	POSITION_ID = "_id";
	String 	POSITION_NAME = "name";
	String 	POSITION_FLOORLEVEL = "floor";
	String 	POSITION_X = "x";
	String 	POSITION_Y = "y";
	String 	POSITION_LAT = "lat";
	String 	POSITION_LON = "lon";
	
	/*
	 * Finger print columns name
	 */
	String 	FINGERPRINT_ID = "_id";
	String 	FINGERPRINT_POS_ID = "pos_id";
	String 	FINGERPRINT_AP_ID = "ap_id";
	String 	FINGERPRINT_RSSI = "rssi";
	String 	FINGERPRINT_RSSI_MIN = "rssi_min";
	String 	FINGERPRINT_RSSI_MAX = "rssi_max";
	String 	FINGERPRINT_RSSI_SNMP = "rssi_snmp";
	String 	FINGERPRINT_LAT = "lat";
	String 	FINGERPRINT_LON = "lon";	
	String 	FINGERPRINT_AZIMUTH = "azimuth";
	
	/**
	 * 
	 * Table's meta data use in application.
	 * Meta data contains a table name and a create table SQL statement  
	 */
	public enum TABLE{	
		ACCESSPOINT("accesspoint"),
		FINGERPRINT("fingerprint"),
		POSITION("position"),
		FINGERPRINT_RAW("fingerprint_raw")
		;				
		
		TABLE(String name){
			this.name = name;
		}
		private String name;
		
		/**
		 * Get table name
		 * @return table name
		 */
		public String getName(){
			return name;
		}
		
		/**
		 * Get SQLite command to create a database.<br/>
		 * Currently it is not use foreign key constraint as it need to support Android version 2.2 and less  
		 * @return
		 */
		public String getCreateSQL(){
			switch(this){
			case ACCESSPOINT:
				return "CREATE TABLE IF NOT EXISTS " + name + " (" +
						"_id integer primary key autoincrement," +
						"mac text UNIQUE not null," +
						"ssid text not null," +
						"lat text," +
						"lon text," +
						"freq integer" +
						")";
			case FINGERPRINT:
				return "CREATE TABLE IF NOT EXISTS " + name + " (" +
						"_id integer primary key autoincrement," +
						"pos_id integer not null," +
						"ap_id integer not null," +						 
						"rssi integer not null," +
						"rssi_min integer not null," +
						"rssi_max integer not null," +
						"rssi_snmp integer," +
						"lat text," +
						"lon text," +
						"azimuth integer" +
						")";
			case FINGERPRINT_RAW:
				return "CREATE TABLE IF NOT EXISTS " + name + " (" +
						"_id integer primary key autoincrement," +
						"pos_name text not null," +
						"ap_mac text not null," +
						"ssid text not null," + 
						"rssi integer not null," +
						"fq integer not null," +
						"accuracy integer," +
						"lat text," +
						"lon text," +
						"azimuth integer," +
						"date_ text" + 
						")";	
			case POSITION:
				return "CREATE TABLE IF NOT EXISTS " + name + " (" +
						"_id integer primary key autoincrement," +
						"x integer not null," +
						"y integer  not null," +
						"lat text," +
						"lon text," +
						"name text UNIQUE," +
						"floor text" + 
						")";
			default:
					throw new IllegalArgumentException("No SQL provide for " + this);
			}
			
		}
		
	}
			
}
