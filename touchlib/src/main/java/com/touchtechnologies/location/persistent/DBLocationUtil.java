package com.touchtechnologies.location.persistent;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.wifi.ScanResult;

import com.touchtechnologies.location.LocationDataUtil;
import com.touchtechnologies.location.dataobject.AccessPoint;
import com.touchtechnologies.location.dataobject.FingerPrint;
import com.touchtechnologies.location.dataobject.Position;
import com.touchtechnologies.log.Log;
import com.touchtechnologies.location.persistent.DBLocationMetaData.TABLE;
import com.touchtechnologies.persistent.DatabaseHelper;

public class DBLocationUtil{
	private SQLiteDatabase sqliteDb;
	private DatabaseHelper dbHelper;
	private Hashtable<String, Integer> hCachedPosition;
	private Hashtable<String, Integer> hCachedAcsPoint;
	private Vector<Position> vPositions;
	
	public DBLocationUtil(Context context) {
		dbHelper = new DBLocationHelper(context);
		sqliteDb = dbHelper.getWritableDatabase();
		
		hCachedPosition = new Hashtable<String, Integer>();
		hCachedAcsPoint = new Hashtable<String, Integer>();
		vPositions = new Vector<Position>();
	}
	
	public Vector<AccessPoint> getAccessPoints(){
		Vector<AccessPoint> vAps = new Vector<AccessPoint>();
		
		return vAps;
	}
	

	/**
	 * Get SQLiteDatabase for some low level access & control. 
	 * @return
	 */
	public SQLiteDatabase getDatabase(){
		return sqliteDb;
	}
	
	
	
	/**
	 * @deprecated
	 * @param context
	 * @param file
	 * @return
	 */
	private List<AccessPoint> initAPAssetImlp(Context context, String file){
		List<AccessPoint> lAccessPoints = new ArrayList<AccessPoint>();
		
		InputStream is = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		try{
			is = context.getAssets().open(file);
			isr = new InputStreamReader(is);			
			br = new BufferedReader(isr);
			
			sqliteDb.delete(TABLE.ACCESSPOINT.getName(), null, null);
			android.util.Log.d(DBLocationUtil.class.getSimpleName(),"Deleted " + TABLE.ACCESSPOINT.getName());
			
			int count = 0;
			String lineRead;
			String apID, apMAC, apSSID, apFreq, apLat, apLon;
			while((lineRead = br.readLine()) != null){
				android.util.Log.d(DBLocationUtil.class.getSimpleName(), "AP:" + lineRead);
						
				StringTokenizer stk = new StringTokenizer(lineRead, ",");
								
				apID   = stk.nextElement().toString();
				apMAC  = stk.nextElement().toString();
				apSSID = stk.nextElement().toString();
				apFreq = stk.nextElement().toString();
				apLat  = stk.nextElement().toString();
				apLon  = stk.nextElement().toString();
				
				AccessPoint ap = new AccessPoint();
				ap.set_id(Integer.parseInt(apID));
				ap.setMac(apMAC);
				ap.setSsid(apSSID);
				ap.setFreq(Integer.parseInt(apFreq));
				ap.setLat(apLat);
				ap.setLon(apLon);
				
				lAccessPoints.add(ap);
								
				//insert data
				ContentValues cv = new ContentValues();
				cv.put(DBLocationMetaData.ACCESSPOINT_ID, apID);
				cv.put(DBLocationMetaData.ACCESSPOINT_MAC, apMAC);
				cv.put(DBLocationMetaData.ACCESSPOINT_SSID, apSSID);
				cv.put(DBLocationMetaData.ACCESSPOINT_FREQ, apFreq);
				cv.put(DBLocationMetaData.ACCESSPOINT_LAT, apLat);
				cv.put(DBLocationMetaData.ACCESSPOINT_LON, apLon);
				
				long affected = sqliteDb.insert(TABLE.ACCESSPOINT.getName(), null, cv);
				count++;
				android.util.Log.d(DBLocationUtil.class.getSimpleName(), "Inserted (" + affected + ") " + apMAC);
			}			
			
			android.util.Log.d(DBLocationUtil.class.getSimpleName(), count + " rows inserted!");	
		}catch(Exception e){			
			Log.log("InitAccesspointAsset", e);
			
		}finally{
			try{
				if(is != null) is.close();
			}catch(Exception ee){
				
			}
			try{
				if(isr != null) isr.close();
			}catch(Exception ee){
				
			}
			try{
				if(br != null) br.close();
			}catch(Exception ee){
				
			}
		}		
		
		return lAccessPoints;
	}
	
	/**
	 * @deprecated
	 * @param context
	 * @param file
	 * @return
	 */
	private List<Position> initPositionAssetImlp(Context context, String file){
		List<Position> lPositions = new ArrayList<Position>();
		
		InputStream is = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		try{
			is = context.getAssets().open(file);
			isr = new InputStreamReader(is);			
			br = new BufferedReader(isr);
			
			sqliteDb.delete(TABLE.POSITION.getName(), null, null);
			android.util.Log.d(DBLocationUtil.class.getSimpleName(),"Deleted " + TABLE.POSITION.getName());
			
			int count = 0;
			String lineRead;
			String posID, posX, posY, posLat, posLon, posFloor, posName;
			while((lineRead = br.readLine()) != null){
				android.util.Log.d(DBLocationUtil.class.getSimpleName(),"Position:" + lineRead);
				
				StringTokenizer stk = new StringTokenizer(lineRead, ",");
				posID   = stk.nextElement().toString();
				posX   	= stk.nextElement().toString();
				posY  	= stk.nextElement().toString();
				posLat 	= stk.nextElement().toString();
				posLon  = stk.nextElement().toString();
				posName = stk.nextElement().toString();
				posFloor= stk.nextElement().toString();
				
				//add to Array
				Position pos = new Position(posID);
				pos.setName(posName);
				pos.setX(Integer.parseInt(posX));
				pos.setY(Integer.parseInt(posY));
				pos.setFloor(posFloor);
				pos.setLatitude(Double.parseDouble(posLat));
				pos.setLongitude(Double.parseDouble(posLon));
				
				lPositions.add(pos);				
				
				//update DB
				ContentValues cv = new ContentValues();				
				cv.put(DBLocationMetaData.POSITION_ID, posID);
				cv.put(DBLocationMetaData.POSITION_X, posX);
				cv.put(DBLocationMetaData.POSITION_Y, posY);
				cv.put(DBLocationMetaData.POSITION_LAT, posLat);
				cv.put(DBLocationMetaData.POSITION_LON, posLon);
				cv.put(DBLocationMetaData.POSITION_NAME, posName);
				cv.put(DBLocationMetaData.POSITION_FLOORLEVEL, posFloor);
				
				long affected = sqliteDb.insert(TABLE.POSITION.getName(), null, cv);
				count++;
				android.util.Log.d(DBLocationUtil.class.getSimpleName(),"Inserted (" + affected + ") " + posName);
			}
			
			android.util.Log.d(DBLocationUtil.class.getSimpleName(), count + " rows inserted!");	
		}catch(Exception e){
			Log.log("InitPositionAsset", e);
			
		}finally{
			try{
				if(is != null) is.close();
			}catch(Exception ee){
				
			}
			try{
				if(isr != null) isr.close();
			}catch(Exception ee){
				
			}
			try{
				if(br != null) br.close();
			}catch(Exception ee){
				
			}
		}
		
		return lPositions;
	}
	
	/**
	 * @deprecated
	 * @param context
	 * @param lAp
	 * @param lPos
	 * @param file
	 */
	private void initFingerprintAssetImlp(Context context, List<AccessPoint> lAp, List<Position> lPos, String file){
		
		InputStream is = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		try{
			is = context.getAssets().open(file);
			isr = new InputStreamReader(is);			
			br = new BufferedReader(isr);
			
			sqliteDb.delete(TABLE.FINGERPRINT.getName(), null, null);
			android.util.Log.d(DBLocationUtil.class.getSimpleName(),"Deleted " + TABLE.FINGERPRINT.getName());
			
			int count = 0;
			String lineRead;
			String posName, apMAC, minRssi, maxRssi, normRssi, snmpRssi = "", lat="0", lon="0";
			String apID = "", posID = "";
			while((lineRead = br.readLine()) != null){
				android.util.Log.d(DBLocationUtil.class.getSimpleName(),"Fingerprint:" + lineRead);
				
				StringTokenizer stk = new StringTokenizer(lineRead, ",");
				posName  = stk.nextElement().toString();
				apMAC   	= stk.nextElement().toString();
				maxRssi 	= stk.nextElement().toString();
				minRssi 	= stk.nextElement().toString();
				normRssi    = stk.nextElement().toString();
								
				
				for(int iAp=0; iAp<lAp.size(); iAp++){
					if(lAp.get(iAp).getMac().equals(apMAC)){
						apID = lAp.get(iAp).getID() + "";
						break;
					}
				}
				
				for(int iPos=0; iPos<lPos.size(); iPos++){
					if(lPos.get(iPos).getName().equals(posName)){
						posID = lPos.get(iPos).getID() + "";
						break;
					}
				}				
				
				//update DB
				ContentValues cv = new ContentValues();				
				cv.put(DBLocationMetaData.FINGERPRINT_POS_ID, posID);
				cv.put(DBLocationMetaData.FINGERPRINT_AP_ID, apID);
				cv.put(DBLocationMetaData.FINGERPRINT_RSSI_MIN, minRssi);
				cv.put(DBLocationMetaData.FINGERPRINT_RSSI_MAX, maxRssi);
				cv.put(DBLocationMetaData.FINGERPRINT_RSSI, normRssi);
				cv.put(DBLocationMetaData.FINGERPRINT_LAT, lat);
				cv.put(DBLocationMetaData.FINGERPRINT_LON, lon);				
				cv.put(DBLocationMetaData.FINGERPRINT_RSSI_SNMP, snmpRssi);
				
				long affected = sqliteDb.insert(TABLE.FINGERPRINT.getName(), null, cv);
				count++;
				android.util.Log.d(DBLocationUtil.class.getSimpleName(),"Inserted (" + affected + ") ");
			}			
			
			android.util.Log.d(DBLocationUtil.class.getSimpleName(),count + " rows inserted!");	
		}catch(Exception e){
			Log.log("InitFongerprintAsset", e);
			
		}finally{
			try{
				if(is != null) is.close();
			}catch(Exception ee){
				
			}
			try{
				if(isr != null) isr.close();
			}catch(Exception ee){
				
			}
			try{
				if(br != null) br.close();
			}catch(Exception ee){
				
			}
		}		
	}
		
	public void initFromAsset(Context context){
		LocationDataUtil locUtil = new LocationDataUtil(context);
		
		List<AccessPoint> lAp = locUtil.parsingAccessPointFromAsset();
		List<Position> lPos   = locUtil.pasringPositionFromAsset();
				
		List<FingerPrint> lFp = locUtil.parsingFingerprintFromAsset(lAp, lPos);
		
		//TODO insert to db
	}
	
//	public void initFromRAWDatabase(Context context){
//		Cursor cursor = sqliteDb.rawQuery("SELECT * FROM " + TABLE.FINGERPRINT_RAW.getName(), null);
//		
//		Vector<String> vPosname = new Vector<String>();
//		
//		String posName;
//		String mac;
//		while(cursor.moveToNext()){
//			posName = cursor.getString(cursor.getColumnIndex("pos_name"));
//			mac = cursor.getString(cursor.getColumnIndex("ap_mac"));
//			"_id integer primary key autoincrement," +
//					"pos_name text not null," +
//					"ap_mac text not null," +
//					"ssid text not null," + 
//					"rssi integer not null," +
//					"fq integer not null," +
//					"accuracy integer," +
//					"lat text," +
//					"lon text," +
//					"date_ text" + 
//		}
//	}
	
	public void close(){
		try{
			if(hCachedAcsPoint != null){
				hCachedAcsPoint.clear();
				hCachedAcsPoint = null;
			}
			
			if(hCachedPosition != null){
				hCachedPosition.clear();
				hCachedPosition = null;
			}
			
			if(vPositions != null){
				vPositions.removeAllElements();
				vPositions = null;
			}
			
			if(sqliteDb != null){
				sqliteDb.close();
			}
			
			if(dbHelper != null){
				dbHelper.close();
			}
			
			System.gc();
			
		}catch(Exception e){
			Log.log("Close database error", e);
		}
	}
	
	public Cursor getRawFingerprint(){
		return sqliteDb.rawQuery("SELECT * FROM " + TABLE.FINGERPRINT_RAW.getName(), null);
	}
	
	public synchronized void addRawFingerprint(List<ScanResult> lResult, float accuracy, double lat, double lon, int azimuth, String name){
		String table = TABLE.FINGERPRINT_RAW.getName();
		String date = System.currentTimeMillis() + "";
		
		try{
			for(ScanResult sr: lResult){
				ContentValues cv = new ContentValues();
				cv.put("pos_name", name);
				cv.put("ap_mac", sr.BSSID);
				cv.put("ssid", sr.SSID);
				cv.put("rssi", sr.level);			
				cv.put("fq", sr.frequency);
				cv.put("accuracy", accuracy);
				cv.put("lat", lat + "");
				cv.put("lon", lon + "");
				cv.put("azimuth", azimuth);
				cv.put("date_", date);
				
				sqliteDb.insert(table, null, cv);
			}			
		}catch(Exception e){
			Log.log("addRawFingerprint",e);
		}
	}
	
	
	/**
	 * 
	 * @param mac
	 * @param ssid
	 * @param lat
	 * @param lon
	 * @param isOwn
	 * @return @see SQliteDatabase.insertWithOnConflict
	 */
	public long insertOrUpdateAccessPoint(String mac, String ssid, String lat, String lon, boolean isOwn){
		long effected = 0;
		try{
			//set values
			ContentValues cv = new ContentValues();
			cv.put(DBLocationMetaData.ACCESSPOINT_MAC, mac);
			cv.put(DBLocationMetaData.ACCESSPOINT_SSID, ssid);
			cv.put(DBLocationMetaData.ACCESSPOINT_LAT, lat);			
			cv.put(DBLocationMetaData.ACCESSPOINT_LON, lon);
			
			//insert
			effected = sqliteDb.insertWithOnConflict(TABLE.ACCESSPOINT.getName(), null, cv, SQLiteDatabase.CONFLICT_REPLACE);
		}catch(Exception e){
			Log.log("insertOrUpdateAccessPoint", e);
		}
		
		return effected;
	}
	
	public long addFingerPrint(String posName, String mac, String ssid, String level, String rssi, String lat, String lon){
		long effected = 0;
		
		Integer positionID = null;
		Integer acsPointID = null;
		
		//check input param
		if(isEmpty(posName, mac, level, rssi)){
			throw new IllegalArgumentException("Not allow empty string for position name, mac address, level and rssi");
		
		//check/initialize a cached position, ap
		}else{			
			if((positionID = hCachedPosition.get(posName)) == null){
				positionID = getPositionIDByName(posName);
				hCachedPosition.put(posName, positionID);
			}
			
			if((acsPointID = hCachedAcsPoint.get(mac)) == null){
				acsPointID = getAccessPointIDByMac(mac, ssid);
				hCachedAcsPoint.put(mac, acsPointID);
			}
		}

		if(positionID == null || acsPointID == null){
			throw new IllegalStateException("AccessPoint or POsition not initialized");
		}else{
		
		try{
				//set values
				ContentValues cv = new ContentValues();
//				cv.put(DatabaseMetaData.FINGERPRINT_LEVEL, level);
				cv.put(DBLocationMetaData.FINGERPRINT_RSSI, rssi);			
				cv.put(DBLocationMetaData.FINGERPRINT_LAT, lat);
				cv.put(DBLocationMetaData.FINGERPRINT_LON, lon);
				cv.put(DBLocationMetaData.FINGERPRINT_AP_ID, acsPointID.intValue());
				cv.put(DBLocationMetaData.FINGERPRINT_POS_ID, positionID.intValue());
				
				//insert
				effected = sqliteDb.insertWithOnConflict(TABLE.FINGERPRINT.getName(), null, cv, SQLiteDatabase.CONFLICT_NONE);
			}catch(Exception e){
				Log.log("addFingerPrint", e);
			}
		}
		
		return effected;
	}
	
	/**
	 * Get position ID by it name.
	 * This method query position data from database. If position data is not exist in the database then insert 
	 * a given position name into database.
	 * @param posName
	 * @return position ID
	 */
	protected final Integer getPositionIDByName(String posName, int... xy){
		Integer postionID = null;
			
		//read from database if not exist in cached
		String posTable = TABLE.POSITION.getName().toString() ;
		Cursor cursor =  sqliteDb.rawQuery("SELECT * FROM " + TABLE.POSITION.getName() + " WHERE name=?" , new String[]{posName});
		
		if(cursor.moveToFirst()){
			postionID = cursor.getInt(cursor.getColumnIndex(DBLocationMetaData.POSITION_ID));
			
		}else{//insert if not exist
			ContentValues cv = new ContentValues();
			cv.put(DBLocationMetaData.POSITION_NAME, posName);
			
			sqliteDb.execSQL("INSERT INTO position(name, x, y) VALUES('" + posName + "','" + (xy.length > 0?xy[0]:0) + " ',' " + (xy.length > 1?xy[1]:0) + " ') ");
			
			//query an inserted row
			return getPositionIDByName(posName);
		}
		
		return postionID;
	}
	
	/**
	 * Get AccessPoint ID by it mac address.
	 * This method query AccessPoint data from database. If not exist in the database then insert 
	 * a given access point mac address into database.
	 * @param mac
	 * @return AccessPoint ID
	 */
	protected final Integer getAccessPointIDByMac(String mac, String ssid){
		Integer id = null;		
			
		// read from database if not exist in cached
		String table = TABLE.ACCESSPOINT.getName().toString();
		Cursor cursor = sqliteDb.rawQuery("SELECT * FROM " + table + " WHERE " + DBLocationMetaData.ACCESSPOINT_MAC + "=?", new String[] { mac });

		if (cursor.moveToFirst()) {
			id = cursor.getInt(cursor.getColumnIndex(DBLocationMetaData.ACCESSPOINT_ID));

		} else {// insert if not exist
			ContentValues cv = new ContentValues();
			cv.put(DBLocationMetaData.ACCESSPOINT_MAC, mac.toLowerCase());
			cv.put(DBLocationMetaData.ACCESSPOINT_SSID, ssid);
//			cv.put(DatabaseMetaData.ACCESSPOINT_ISOWN, "n");
			
			sqliteDb.insertWithOnConflict(TABLE.ACCESSPOINT.toString(), null, cv, SQLiteDatabase.CONFLICT_IGNORE);

			// query an inserted row
			return getAccessPointIDByMac(mac, ssid);
		}
		
		return id;
	}	
	
	public static boolean isEmpty(String... name){
		for(int i=0; i<name.length; i++){
			if(name[i] == null || name[i].length() == 0){
				return true;
			}
		}
		
		return false;
	}
	
	public Vector<Position> getPositions(){
		if(vPositions.size() == 0){
			getFingerPrints();
		}
		return vPositions;
	}

	/**
	 * A position that was query from database
	 * @param pos
	 * @return
	 */
//	private String[] getPositionXY(Position pos){
//		int length = POSITION.length;
//		
//		String posDBName;
//		for(int i=0; i<length; i++){
//			posDBName = pos.getName();
//			if(posDBName.indexOf(POSITION[i][0]) != -1){
//				return POSITION[i];
//			}
//		}
//		
//		return null;
//	}
	
	public Vector<FingerPrint> getFingerPrints(){
		Vector<FingerPrint> vFingerPrints = new Vector<FingerPrint>();
		
		try{		
			Cursor cursor = sqliteDb.rawQuery("SELECT _id, name, x, y, lat, lon, name, floor FROM " + TABLE.POSITION.getName(), null);
		    while(cursor.moveToNext()){	
		    			    			    	
		    	Position position = new Position(cursor.getInt(cursor.getColumnIndex("_id")));
		    	position.setX(cursor.getInt(cursor.getColumnIndex("x")));
		    	position.setY(cursor.getInt(cursor.getColumnIndex("y")));
		    	position.setLatitude(cursor.getInt(cursor.getColumnIndex("lat")));
		    	position.setLongitude(cursor.getInt(cursor.getColumnIndex("lon")));
		    	position.setName(cursor.getString(cursor.getColumnIndex("name")));
		    	position.setFloor(cursor.getString(cursor.getColumnIndex("floor")));
		    	
				vPositions.add(position);
//				android.util.Log.d(DBLocationUtil.class.getSimpleName(),"Position db:" + position);
				
				FingerPrint fp = new FingerPrint();
		    	fp.setPosition(position);
				
				Cursor cursor2 = sqliteDb.rawQuery(
						"SELECT fingerprint._id as fid, pos_id, " +
						"ap_id, rssi, rssi_min, rssi_max, rssi_snmp,  " +
						"fingerprint.lat as flat, fingerprint.lon as flon, " +
						"mac, ssid, accesspoint.lat as alat, accesspoint.lon as alon," +
						"freq " +
						"FROM fingerprint LEFT JOIN accesspoint " +
						"ON  fingerprint.ap_id=accesspoint._id " +
						"WHERE pos_id=? ORDER BY rssi DESC", 
						new String[]{position.getID() + ""});
				
				while(cursor2.moveToNext()){
					fp.set_id(cursor2.getInt(cursor2.getColumnIndex("fid")));
					
					AccessPoint ap = new AccessPoint();					
					ap.set_id(cursor2.getInt(cursor2.getColumnIndex("ap_id")));
					ap.setMac(cursor2.getString(cursor2.getColumnIndex("mac")));
					ap.setRssi(cursor2.getInt(cursor2.getColumnIndex("rssi")));
					ap.setRssiMin(cursor2.getInt(cursor2.getColumnIndex("rssi_min")));
					ap.setRssiMax(cursor2.getInt(cursor2.getColumnIndex("rssi_max")));
					ap.setRssiSNMP(cursor2.getInt(cursor2.getColumnIndex("rssi_snmp")));
					ap.setSsid(cursor2.getString(cursor2.getColumnIndex("ssid")));
					ap.setLat(cursor2.getString(cursor2.getColumnIndex("alat")));
					ap.setLon(cursor2.getString(cursor2.getColumnIndex("alon")));
					ap.setFreq(cursor2.getInt(cursor2.getColumnIndex("freq")));
					
//					android.util.Log.d(DBLocationUtil.class.getSimpleName(),"Accesspoint db:" + ap);
					
					fp.add(ap);
				}
				
				cursor2.close();
				vFingerPrints.add(fp);
			}
				
		    cursor.close();	
		}catch(Exception e){
			Log.log("getFingerPrints", e);
		}
		return vFingerPrints;
	}
	
	
	@Override
	protected void finalize() throws Throwable {
		close();
		super.finalize();
	}
}
