package com.touchtechnologies.location.persistent;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.touchtechnologies.location.persistent.DBLocationMetaData.TABLE;
import com.touchtechnologies.log.Log;
import com.touchtechnologies.persistent.DatabaseHelper;

public class DBLocationHelper extends DatabaseHelper {
	public DBLocationHelper(Context context) {
		super(context, DBLocationMetaData.DBNAME, DBLocationMetaData.VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		TABLE tables[] = TABLE.values();
		for (int i = 0; i < tables.length; i++) {
			try {
				db.execSQL(tables[i].getCreateSQL());
			} catch (Exception e) {
				Log.log("DatabaseHelper", e);
			}
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		TABLE tables[] = TABLE.values();
		for (int i = 0; i < tables.length; i++) {
			db.execSQL("DROP TABLE IF EXISTS " + tables[i].getName());
		}

	}
}
