package com.touchtechnologies.location.dataobject;

public class AccessPoint implements Comparable<AccessPoint>, Cloneable{
	private int _id;
	private String mac;
	private String ssid;
	private String lat;
	private String lon;
	private boolean isOwn;
	private int rssi;
	private int rssiMin;
	private int rssiMax;
	private int rssiSNMP;
	private int freq;
	
	public int getID() {
		return _id;
	}
	
	public void set_id(int _id) {
		this._id = _id;
	}
	
	public String getMac() {
		return mac;
	}
	
	public void setMac(String mac) {
		this.mac = mac.toLowerCase().trim();
	}
	
	public String getSsid() {
		return ssid;
	}
	
	public void setSsid(String ssid) {
		this.ssid = ssid;
	}
		
	public String getLat() {
		return lat;
	}
	
	public void setFreq(int freq) {
		this.freq = freq;
	}
	
	public int getFreq() {
		return freq;
	}
	
	public String getLon() {
		return lon;
	}
	
	public void setLat(String lat) {
		this.lat = lat;
	}
	
	public void setLon(String lon) {
		this.lon = lon;
	}
	
	public void setRssi(int rssi) {
		this.rssi = rssi;
	}
	
	public int getRssi() {
		return rssi;
	}
	
	public boolean isOwn() {
		return isOwn;
	}
	
	public void setOwn(boolean isOwn) {
		this.isOwn = isOwn;
	}
	
	@Override
	public boolean equals(Object o) {		
		String mac = ((AccessPoint)o).getMac();
		return getMac().equalsIgnoreCase(mac);
	}
	
	@Override
	public int compareTo(AccessPoint another) {	
		int rssi = another.getRssi();
		if(this.rssi < rssi){
			return 1;
		}else if (this.rssi > rssi){
			return -1;
		}
		
		return 0;
	}
	
	public void setRssiMin(int rssiMin) {
		this.rssiMin = rssiMin;
	}
	
	public int getRssiMin() {
		return rssiMin;
	}
	
	public void setRssiMax(int rssiMax) {
		this.rssiMax = rssiMax;
	}
	
	public int getRssiMax() {
		return rssiMax;
	}
	
	public void setRssiSNMP(int rssiSNMP) {
		this.rssiSNMP = rssiSNMP;
	}
	
	public int getRssiSNMP() {
		return rssiSNMP;
	}	
	
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getID());
		builder.append("\t ");		
		builder.append(getMac());
		builder.append("\t ");
		builder.append(getRssiMax());
		builder.append('/');
		builder.append(getRssiMin());
		builder.append('/');
		builder.append(getRssi());
		builder.append("\t ");
		builder.append(getFreq());
		builder.append("MHz");
		builder.append("\t ");
		builder.append(getSsid());	
		
		return builder.toString();
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
