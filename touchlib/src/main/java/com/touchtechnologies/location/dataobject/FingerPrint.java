package com.touchtechnologies.location.dataobject;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

public class FingerPrint implements Comparable<FingerPrint>{
	private int _id;	
	public static final int MATCH_CRITERIA_SIGNAL_PATTERN = 1;
//	private static final int INDEX_PATTERN[] = new int[Constant.MAX_ACCESSPOINT_COMPARE];
	
	/**
	 * Euclidean distance.
	 */
	double euclidean;
	private Vector<AccessPoint> vAccessPoint;
	/**
	 * A set of AccessPint that say it's matched to this finger print. 
	 */
	private List<AccessPoint> lApFound;
	private Position position;
	
	public FingerPrint() {
		vAccessPoint = new Vector<AccessPoint>();
	}
	
	public void set_id(int _id) {
		this._id = _id;
	}		
	
	public void setPosition(Position position) {
		this.position = position;
	}
	
	public int getID() {
		return _id;
	}
	
	public int getAccessPointCount(){
		return vAccessPoint.size();
	}
	
	public void add(AccessPoint ap){		
		if(vAccessPoint.indexOf(ap) == -1){
			vAccessPoint.add(ap);
			Collections.sort(vAccessPoint.subList(0, vAccessPoint.size()));			
		}
	}	
	
	public AccessPoint match(AccessPoint ap){
		int index = vAccessPoint.indexOf(ap);
		return index > -1?vAccessPoint.elementAt(index):null;		
	}
	
	@Override
	public int compareTo(FingerPrint another) {
		return position.getName().compareTo(another.getPosition().getName());		
	}
	
	public Vector<AccessPoint> getAccessPoints(){
		return vAccessPoint;
	}
	
	public Position getPosition() {
		return position;
	}
	
	public void setEuclidean(double euclidean) {
		this.euclidean = euclidean;
	}
	
	public double getEuclidean() {
		return euclidean;
	}
	
	/**
	 * A set of AccessPint that say it's matched to this finger print. 
	 */
	public void setMatchedAccesspoint(List<AccessPoint> lApFound){
		this.lApFound = lApFound;
	}
	
	/**
	 * A set of AccessPint that say it's matched to this finger print. 
	 */
	public List<AccessPoint> getMatchedAccpoint(){
		return lApFound;
	}
	
	public boolean isMatch(List<AccessPoint> lAccesspoint, int criteria){
		boolean match = true;
		/**
		 *  Pattern sample
		 * 	AP1	AP2	AP3	AP4
			
			AP1	AP2	AP3	
			AP1	AP2	AP4	
			AP1	AP3	AP4	
			AP2	AP3	AP4	

		 */
		switch(criteria){
		case MATCH_CRITERIA_SIGNAL_PATTERN:
//			for(int i=0; i<Constant.MAX_ACCESSPOINT_COMPARE; i++){
//				AccessPoint ap = vAccessPoint.elementAt(i);
//				//if(lAccesspoint.)
//			}
//			
			break;
		}
		
		
		return match;
	}
	
	@Override
	public String toString() {
		StringBuilder sbuilder = new StringBuilder();
		sbuilder.append("FPID=");
		sbuilder.append(getID());
		sbuilder.append("\t");
		sbuilder.append(position.toString());		
				
		sbuilder.append("\r\n\t");
		for(AccessPoint ap: vAccessPoint){
			sbuilder.append(ap.toString());
			sbuilder.append("\r\n\t");
		}		
		
		if(lApFound != null){
			sbuilder.append("Accesspoint matched to this Fingerprint. Euclidean = ");
			sbuilder.append(euclidean);
			sbuilder.append("\r\n\t");
			for(AccessPoint ap: lApFound){
				sbuilder.append("\t -->");
				sbuilder.append(ap.toString());
				sbuilder.append("\r\n\t");
			}
		}
		
		sbuilder.append("\r\n");
		
		
		return sbuilder.toString();
	}
	
	public final class EuclideanComparator implements Comparator<FingerPrint>{
		
		@Override
		public int compare(FingerPrint fp1, FingerPrint fp2) {
			if(fp1.euclidean > fp2.euclidean){
				return 1;
			}else if (fp1.euclidean < fp2.euclidean){
				return -1;
			}
			return 0;
		}
	}
}
