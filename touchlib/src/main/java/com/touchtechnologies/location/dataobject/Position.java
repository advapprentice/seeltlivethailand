package com.touchtechnologies.location.dataobject;

import android.location.Location;

public class Position extends Location{		
	private int _id;
	private int x;
	private int y;
	private String comment;
	private String name;
	private String floor;
	//TODO remove implement specific
//	private int row;
	
	public Position(String id){
		this(Integer.parseInt(id));
	}
	
	public Position(int id){		
		super("Touch Technologies");		
		this._id = id;		
	}
	
	public int getID() {
		return _id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getSimpleName(){
		int lIndex = name.indexOf('.');
		if(lIndex != -1){
			return this.name.substring(0, lIndex);
		}
		name = new String("" + name.charAt(0)).toUpperCase()
				+ name.substring(1, name.length());
		
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
//		row = Integer.parseInt(name.substring(name.indexOf('-') + 1, name.lastIndexOf('-')));
		
//		int lIndex = name.lastIndexOf('.');
//		if(lIndex != -1){
//			this.name = this.name.substring(0, lIndex);
//		}
	};
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		if(this.x == 0){
			this.x = x;
		}
	}
	
	public void setY(int y) {
		if(this.y == 0){
			this.y = y;
		}
	}
	
	//TODO remove, avoid implementation specific
//	public int getRowJoinIndex(){
//		return row;
//	}
	
//	public boolean isAdjoin(Position pos){
//		return Math.abs(row - pos.getRowJoinIndex()) <= 1;
//	}
	
	public String getSubName(){
		int index;
		if(name != null){
			index = name.indexOf('-');
			if(index != -1){
				return name.substring(0, index);
			}else{
				return null;
			}
		}else{
			return null;
		}
	}
	
	public int getY() {
		return y;
	}
			
	public String getFloor() {
		return floor;
	}
	
	public void setFloor(String floor) {
		this.floor = floor;
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public String getComment() {
		return comment;
	}
	
	@Override
	public String toString() {
		StringBuilder sbuilder = new StringBuilder();
		
		sbuilder.append("Position[");		
		sbuilder.append(getName());		
		sbuilder.append("\t");		
		sbuilder.append("x:");
		sbuilder.append(getX());
		sbuilder.append(",y:");
		sbuilder.append(getY());
		sbuilder.append("]");
		
		return sbuilder.toString();
	}
	
	@Override
	public boolean equals(Object o) {
		try{
			return ((Position)o).getName().equals(getName());			
		}catch(Exception e){
			
		}
		
		return false;
	}
}
