package com.touchtechnologies.location;

import java.util.List;

import com.touchtechnologies.location.dataobject.FingerPrint;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.location.Location;
import android.location.LocationListener;
import android.net.wifi.ScanResult;
import android.os.Bundle;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;

public interface FingerprintSensorListener extends LocationListener{
	int LOC_GPS = 17;
	int LOC_NETWORKCELL = 111;
	int LOC_WIFI = 113;	
	int LOC_GEOMAGNATIC = Sensor.TYPE_MAGNETIC_FIELD;	
	int LOC_ORIENTATION = Sensor.TYPE_ORIENTATION;	
	int LOC_ROTATION_VECTOR = Sensor.TYPE_ROTATION_VECTOR;
	int LOC_SNMP = 119;	
	
	int MSG_UPDATE_POSITION = 11;
	int MSG_SCANWIFI_RESULT = 12;
	int MSG_SCANFINGERPRINT_RESULT = 13;
	
	void onLocationChanged(Location location);	
	void onScanFingerprintCompleted(List<FingerPrint> lFingerprint);
	void onProviderDisabled(String provider);
	void onProviderEnabled(String provider);
	void onStatusChanged(String provider, int status, Bundle extras);	
	void onScanCompleted(List<ScanResult> lScanResults);
	void onScanCompleted(CellLocation cellLocation, List<NeighboringCellInfo> lNeighborCellInfos);
	void onSensorChanged(SensorEvent event);
}
