package com.opensource.pagerindicator;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;

import com.nostra13.universalimageloader.core.assist.LoadedFrom;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;

public class CircleBitmapDisplayer implements BitmapDisplayer {

	private float borderWidth = 0;
	private int borderColor;

	public CircleBitmapDisplayer() {
		super();
	}

	public CircleBitmapDisplayer(int borderColor, int borderWidth){
		super();
		this.borderColor = borderColor;
		this.borderWidth = borderWidth;
	}

	@Override
    public void display(Bitmap bitmap, ImageAware imageAware, LoadedFrom loadedFrom) {
		 int widthLight = bitmap.getWidth();
	        int heightLight = bitmap.getHeight();
		try {
			Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);

			Canvas canvas = new Canvas(output);
			Paint paintColor = new Paint();
			paintColor.setFlags(Paint.ANTI_ALIAS_FLAG);

			RectF rectF = new RectF(new Rect(0, 0, widthLight, heightLight));

			canvas.drawRoundRect(rectF, widthLight / 2, heightLight / 2, paintColor);

			Paint paintImage = new Paint();
			paintImage.setXfermode(new PorterDuffXfermode(Mode.SRC_ATOP));
			canvas.drawBitmap(bitmap, 0, 0, paintImage);

			//--ADD BORDER IF NEEDED
			if (this.borderWidth > 0) {
				final Paint paint2 = new Paint();
				paint2.setAntiAlias(true);
				paint2.setColor(this.borderColor);
				paint2.setStrokeWidth(this.borderWidth);
				paint2.setStyle(Paint.Style.STROKE);
				canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, (float) (bitmap.getWidth() / 2 - Math.ceil(this.borderWidth / 2)), paint2);
			}
			imageAware.setImageBitmap(output);
		}catch (Exception e){
			Log.d(CircleBitmapDisplayer.class.getSimpleName(), "display: "+e);
		}


    }

}