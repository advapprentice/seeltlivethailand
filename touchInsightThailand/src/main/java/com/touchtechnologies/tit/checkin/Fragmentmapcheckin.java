package com.touchtechnologies.tit.checkin;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.touchtechnologies.json.JSONUtil;
import com.touchtechnologies.tit.LocationAwareFragment;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

public class Fragmentmapcheckin extends LocationAwareFragment implements RoutingListener, OnMapReadyCallback {
	private GoogleMap googleMap;
	private JSONArray checkin;
	private LatLng mlocation;
	private int state = 0;
	GoogleApiClient mGoogleApiClient = null;
	ArrayList<LatLng> arrayList = new ArrayList<>();
	Location mLastLocation;

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		checkin = UserUtil.getCheckIns(getActivity(), "hotel");

		if (mGoogleApiClient == null) {
			mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
					.addConnectionCallbacks(this)
					.addOnConnectionFailedListener(this)
					.addApi(LocationServices.API)
					.build();
		}


	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_fragmentmapcheckin, container, false);


		return view;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		super.onCreateOptionsMenu(menu, inflater);
	}


	@Override
	public void onResume() {
		super.onResume();
		try {
			if (googleMap == null) {
				SupportMapFragment MapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_checkin);
				MapFragment.getMapAsync(this);
				if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
						ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
					// TODO: Consider calling
					//    ActivityCompat#requestPermissions
					// here to request the missing permissions, and then overriding
					//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
					//                                          int[] grantResults)
					// to handle the case where the user grants the permission. See the documentation
					// for ActivityCompat#requestPermissions for more details.
					return;
				}
				googleMap.setMyLocationEnabled(true);

			}
				googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
				googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
					@Override
					public void onInfoWindowClick(Marker arg0) {
						LatLng latLng =  arg0.getPosition();
						Uri gmmIntentUri = Uri
								.parse("google.navigation:" + latLng.latitude + ","
										+ latLng.longitude).buildUpon()
								.appendQueryParameter("q", arg0.getTitle()).build();
						Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
						mapIntent.setPackage("com.google.android.apps.maps");
						startActivity(mapIntent);
					}
				});
				ArrayList<Marker>  markers = new ArrayList<Marker>();
				 LatLng latLng;
				if(checkin != null && state == 0){
					 for(int i =0 ; i < checkin.length(); i++){
						 JSONObject mCheckin = checkin.getJSONObject(i);			
						 latLng = new LatLng(JSONUtil.getDouble(mCheckin, "latitude"), JSONUtil.getDouble(mCheckin, "longitude"));
						arrayList.add(latLng);
						markers.add(googleMap.addMarker(new MarkerOptions()
								 .position(latLng)
								 .title(mCheckin.getString("name_en"))
								 .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_maker))));
					 }
					 state =1;
					 LatLngBounds.Builder builder = new LatLngBounds.Builder();
					 for (Marker marker : markers) {
					     builder.include(marker.getPosition());
					 }
					 LatLngBounds bounds = builder.build();
					 int padding = 0; // offset from edges of the map in pixels
					 CameraUpdate cam = CameraUpdateFactory.newLatLngZoom(markers.get(1).getPosition(), 12F);
					 googleMap.animateCamera(cam);
					googleMap.moveCamera(cam);
//					 CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
//					googleMap.animateCamera(cu);
					Go_routeMap();
				}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
	}

	@Override
	public void onLocationChanged(Location location) {
		mLastLocation = location;
		super.onLocationChanged(location);
	}

	@Override
	public void onConnected(Bundle dataBundle) {

		 if(mLastLocation !=null){
		if ( googleMap != null) {
			location = mLastLocation;
			if(location != null){
				onResume();
				
			}
		}
		 }
		
	}



	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		// TODO Auto-generated method stub
		super.onConnectionFailed(connectionResult);
	}




	@Override
	public void onConnectionSuspended(int arg0) {
		Log.d("", arg0+"");
		
	}


    @Override
    public void onRoutingFailure(RouteException e) {

    }

    @Override
	public void onRoutingStart() {
		
		
	}

	@Override
	public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
		PolylineOptions mPolyOptions = route.get(shortestRouteIndex).getPolyOptions();
		PolylineOptions polyoptions = new PolylineOptions();
        polyoptions.color(Color.BLUE);
        polyoptions.width(10);
        polyoptions.addAll(mPolyOptions.getPoints());
        googleMap.addPolyline(polyoptions);

		
	}

	@Override
	public void onRoutingCancelled() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onStart() {
		mGoogleApiClient.connect();
		super.onStart();
	}
	
	@Override
	public void onStop() {
		mGoogleApiClient.disconnect();
		super.onStop();
	}


	@SuppressLint("MissingPermission")
    private void  Go_routeMap(){
		location = googleMap.getMyLocation();
		if(location == null){
		location = ResourceUtil.getLocation(getActivity());
		}
		mlocation = new LatLng(location.getLatitude(), location.getLongitude());
		Routing mRouting = new Routing.Builder()
				.travelMode(AbstractRouting.TravelMode.DRIVING)
				.withListener(this)
				.waypoints(mlocation,arrayList.get(0),arrayList.get(1))
				.build();
		mRouting.execute();
		if(googleMap.isMyLocationEnabled())
			googleMap.setMyLocationEnabled(false);

	}

	@Override
	public void onMapReady(GoogleMap googleMapv) {
		googleMap = googleMapv;
	}
}
