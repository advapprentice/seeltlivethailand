package com.touchtechnologies.tit.checkin;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.database.AbstractCursor;
import android.database.Cursor;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

public class CheckInActivity extends SettingActionbarActivity implements ConnectionCallbacks,
        OnConnectionFailedListener, LocationListener, OnClickListener{
	GoogleApiClient mGoogleApiClient;
	LocationRequest mLocationRequest;
	boolean  mRequestingLocationUpdates;
	Location mLastLocation;
	PoiCursorAdapter poiCursorAdapter;
	JSONObject selectedPoi;
	int page = 0;
	SearchView searchView;
	String urlNearBy;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mTitle = "Back to Hotel";
		setContentView(R.layout.activity_checkin);

		MainApplication.getInstance().trackScreenView("Menu_MyDestination_Save");
		poiCursorAdapter = new PoiCursorAdapter(this);

		searchView = (SearchView)findViewById(R.id.searchView);
		searchView.setSuggestionsAdapter(poiCursorAdapter);
		searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
			@Override
			public boolean onSuggestionClick(int position) {
				Cursor cursor = poiCursorAdapter.getCursor();
				cursor.moveToPosition(position);

				String name = cursor.getString(1);

				searchView.setQuery(name, false);
				searchView.clearFocus();

				JSONArray jsonPois = PoiCursorAdapter.jsonPois;
				try {
					for (int i = 0; i < jsonPois.length(); i++) {
						if (name.equals(jsonPois.getJSONObject(i).getString("name_en"))) {
							selectedPoi = jsonPois.getJSONObject(i);
                            CheckInFragment  cm = (CheckInFragment)getSupportFragmentManager().findFragmentById(R.id.fragmentCheckIn);
                            cm.showexample(selectedPoi,selectedPoi.getInt("provider_type_id"));
							break;
						}
					}
				} catch (Exception e) {
					Log.e(CheckInActivity.class.getName(), "get selected poi error", e);
				}

				return true;
			}

			@Override
			public boolean onSuggestionSelect(int position) {
				return false;
			}
		});
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {

				searchView.clearFocus();
				return true;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				return false;
			}
		});

		buildGoogleApiClient();
		createLocationRequest();
		//startLocationUpdates();
		
		mGoogleApiClient.connect();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		if (mGoogleApiClient.isConnected() && !mRequestingLocationUpdates) {
	        startLocationUpdates();
	    }
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.clear();
        //TODO  hide menu if want show delete comment
//		if(page == 0){
//			getMenuInflater().inflate(R.menu.fragmentmapcheckin, menu);
//		}else {
//			getMenuInflater().inflate(R.menu.termofservice, menu);
//		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Fragment fragment = new Fragmentmapcheckin();
		FragmentManager manager = getSupportFragmentManager();
		if(item.getItemId() == R.id.icmapcheckin){
			fragment = new Fragmentmapcheckin();
			findViewById(R.id.fragmentCheckIn).setVisibility(View.GONE);
			findViewById(R.id.framelayout_fragment).setVisibility(View.VISIBLE);
			//findViewById(R.id.icmapcheckin).setVisibility(View.GONE);
			manager.beginTransaction().replace(R.id.framelayout_fragment, fragment).commit();
			
			page =1;
			invalidateOptionsMenu();
			//onPrepareOptionsMenu(null);
		}else if(item.getItemId() == R.id.icmapcheckin2){
			findViewById(R.id.fragmentCheckIn).setVisibility(View.VISIBLE);
			findViewById(R.id.framelayout_fragment).setVisibility(View.GONE);
			//findViewById(R.id.icmapcheckin).setVisibility(View.VISIBLE);
			fragment = new CheckInFragment();
			manager.beginTransaction().replace(R.id.fragmentCheckIn, fragment).commit();
			page =0;
			//onPrepareOptionsMenu(null);
			invalidateOptionsMenu();
		}else if(item.getItemId() == android.R.id.home){
			finish();
		}
		
		return true;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.buttonCheckin:

			if(selectedPoi != null){
				//save check in
				UserUtil.addCheckIn(CheckInActivity.this, selectedPoi,"other");
				
				CheckInFragment  cm = (CheckInFragment)getSupportFragmentManager().findFragmentById(R.id.fragmentCheckIn);
				cm.update("other");
				cm.hideexample();
				selectedPoi =null;
				searchView.setQuery("", false);
			
//				Toast.makeText(this, "saving...",Toast.LENGTH_SHORT).show();
				
			}	
			
			break;
        case R.id.button_addhotel:
            if(selectedPoi != null) {
                UserUtil.addCheckIn(CheckInActivity.this, selectedPoi, "hotel");
                CheckInFragment cm = (CheckInFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentCheckIn);
                cm.update("hotel");
                cm.hideexample();
                selectedPoi = null;
                searchView.setQuery("", false);
//          Toast.makeText(this, "saving...",Toast.LENGTH_SHORT).show();
            }
                break;
			
			
		}
	}
	
	protected synchronized void buildGoogleApiClient() {
	    mGoogleApiClient = new GoogleApiClient.Builder(this)
	        .addConnectionCallbacks(this)
	        .addOnConnectionFailedListener(this)
	        .addApi(LocationServices.API)
	        .build();
	}
	
	protected void createLocationRequest() {
	    mLocationRequest = new LocationRequest();
	    mLocationRequest.setInterval(30*1000);
	    mLocationRequest.setFastestInterval(5*1000);
	    mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
	}
	
	@Override
	public void onLocationChanged(Location newLoc) {
		mLastLocation = newLoc;
		ResourceUtil.setLocation(CheckInActivity.this,mLastLocation);
		stopLocationUpdates();
	}
	
	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Toast.makeText(this, "Can't get your loction", Toast.LENGTH_LONG).show();
	}

	@Override
	public void onConnected(Bundle data) {
//		Toast.makeText(this, "Connected", Toast.LENGTH_LONG).show();


//		mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
//                mGoogleApiClient);
		startLocationUpdates();
		mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		if(mLastLocation == null){
			Toast.makeText(this, "Can't get your loction", Toast.LENGTH_LONG).show();
			return;
		}
	//	Toast.makeText(this, "get your loction="+ mLastLocation, Toast.LENGTH_LONG).show();
		urlNearBy = ResourceUtil.getServiceUrl(this);
		urlNearBy += getString(R.string.app_service_NearByMe);
		urlNearBy +=  mLastLocation.getLatitude();
		urlNearBy +=  "/" + mLastLocation.getLongitude();
		urlNearBy += "/" + 100;
		
		//execute poi listing
		new MyCheckInAsynTask(this).execute(urlNearBy);
	}

	protected void startLocationUpdates() {
	    LocationServices.FusedLocationApi.requestLocationUpdates(
	            mGoogleApiClient, mLocationRequest, this);
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		
		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		stopLocationUpdates();
	}
	
	protected void stopLocationUpdates() {
	    LocationServices.FusedLocationApi.removeLocationUpdates(
	            mGoogleApiClient, this);
	}
}


class PoiCursorAdapter extends SimpleCursorAdapter{
	private static final String[] mFields  = { "_id", "result" };
	private static final String[] mVisible = { "result" };
	private static final int[]    mViewIds = { android.R.id.text1 };
	
	static JSONArray jsonPois;
	
	public PoiCursorAdapter(Context context) {
		super(context, android.R.layout.simple_list_item_1, null, mVisible, mViewIds, 0);
	}
	
	@Override
	public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
		return new SuggestionsCursor(constraint);
	}
	
	private static class SuggestionsCursor extends AbstractCursor
	{
		private static ArrayList<JSONObject> mResults;
		
		public SuggestionsCursor(CharSequence constraint)
		{
			mResults = new ArrayList<JSONObject>(20);
			
			if(jsonPois != null && !TextUtils.isEmpty(constraint)){
				try{
					constraint = constraint.toString().toLowerCase();
					JSONObject json;
					String nameEn, nameTh;
					for(int i=0; i<jsonPois.length(); i++){
						json = jsonPois.getJSONObject(i);
						nameEn = json.getString("name_en").toLowerCase();
						nameTh = json.getString("name_th").toLowerCase();
						
						if(nameEn.contains(constraint) || nameTh.contains(constraint)){
							mResults.add(json);
						}
					}
				}catch(Exception e){
					Log.e(CheckInActivity.class.getName(), "error " + e.getMessage(), e);
				}
			}
		}
		
		@Override
		public int getCount()
		{
			return mResults.size();
		}
		
		@Override
		public String[] getColumnNames()
		{
			return mFields;
		}
		
		@Override
		public long getLong(int column)
		{
			if(column == 0){
				return mPos;
			}
			throw new UnsupportedOperationException("unimplemented");
		}
		
		@Override
		public String getString(int column)
		{
			if(column == 1){
				String name = "";
				try{
					name = mResults.get(mPos).getString("name_en");
				}catch(Exception e){
				}
				return name;
			}
			throw new UnsupportedOperationException("unimplemented");
		}
		
		@Override
		public short getShort(int column)
		{
			throw new UnsupportedOperationException("unimplemented");
		}
		
		@Override
		public int getInt(int column)
		{
			throw new UnsupportedOperationException("unimplemented");
		}
		
		@Override
		public float getFloat(int column)
		{
			throw new UnsupportedOperationException("unimplemented");
		}
		
		@Override
		public double getDouble(int column)
		{
			throw new UnsupportedOperationException("unimplemented");
		}
		
		@Override
		public boolean isNull(int column)
		{
			return false;
		}
	}
}

class MyCheckInAsynTask extends AsyncTask<String, Void, JSONArray>{
	Context context;
	
	public MyCheckInAsynTask(Context context) {
		this.context = context;
	}
	
	@Override
	public JSONArray doInBackground(String... params) {
		JSONArray jsons = null;
		
		try{
			ServiceConnector service = new ServiceConnector(params[0]);
			
			String response = service.doGet(context);
			jsons = new JSONArray(response);
		}catch(Exception e){
			Log.e("MyCheckInAsynTask", "Get poi near by error " + e.getMessage(), e);
		}
	
		return jsons;
	}
	
	@Override
	protected void onPostExecute(JSONArray result) {
		PoiCursorAdapter.jsonPois = result;
		((CheckInActivity)context).poiCursorAdapter.notifyDataSetChanged();
	}
}