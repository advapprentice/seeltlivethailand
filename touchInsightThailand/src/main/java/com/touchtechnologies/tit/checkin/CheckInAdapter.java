package com.touchtechnologies.tit.checkin;


import java.util.Timer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;

 @SuppressLint("NewApi") public class CheckInAdapter extends BaseAdapter implements OnClickListener {
	
	private Context context;
	private Runnable runnable;
	protected ImageLoader imageLoader;
	protected DisplayImageOptions options;
	
	public boolean delete;
	
	Timer t;
	JSONArray checkin;
	int positionselect;
	private CCTV cctvs;
	String url;
	public CheckInAdapter(Context context) {
		this.context = context;
		

		imageLoader = ImageLoader.getInstance();
		
		
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.NONE)
		.showImageForEmptyUri(R.drawable.bg_noimage)
		.showImageOnFail(R.drawable.bg_noimage)
	//	.showImageOnLoading(R.drawable.bg_loading)
		.cacheInMemory(true)
		.displayer(new FadeInBitmapDisplayer(300)) //fade in images
	  	.resetViewBeforeLoading(true)
	  	.build();

 	    imageLoader = ImageLoader.getInstance();
	
		
	}
	public void setData(JSONArray checkin){
		this.checkin = checkin;
		notifyDataSetChanged();
	}

	public void toggleDeleteButtons() {
		delete = !delete;
		Toast.makeText(context, "POI"+delete, Toast.LENGTH_SHORT).show();
		notifyDataSetChanged();
		
	}
	
	public void setDeleteButtons(boolean visible){
		this.delete = visible;
		notifyDataSetChanged();
	}
	
	public boolean getDeleteStatus(){
		return delete;
	}

	@Override
	public int getCount() {
		return checkin == null?0:checkin.length();
	}
	
	@Override
	public JSONObject getItem(int position) {
		JSONObject object = null;

		try {
			object =	checkin.getJSONObject(checkin.length()-1 -position);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return object;
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}

	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		
		if(convertView == null){
			convertView = LayoutInflater.from(context).inflate(R.layout.item_checkin, parent, false);


			viewHolder = new ViewHolder();
			
		
			viewHolder.textpoi = (TextView)convertView.findViewById(R.id.textnamepoi);
			viewHolder.btnroute = (Button)convertView.findViewById(R.id.btnroute);
			viewHolder.textaddress = (TextView)convertView.findViewById(R.id.txt_address);
			viewHolder.textdistance= (TextView)convertView.findViewById(R.id.txt_distance);
			viewHolder.btndelete = (Button)convertView.findViewById(R.id.btndelete_visible);
            try {
                if(getItem(position).getInt("provider_type_id") == 3) {
//#FFFC0707
                    viewHolder.btnroute.setBackgroundColor(Color.parseColor("#FFFC0707"));
                    viewHolder.btnroute.setText("Back to Hotel");
                }else {
//                    viewHolder.btnroute.setBackgroundColor(viewHolder.btnroute);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            viewHolder.btndelete.setVisibility(View.GONE);
			
			
			viewHolder.btnroute.setOnClickListener(this);
			
			viewHolder.btndelete.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View arg0) {
               
                	 AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                     alertDialog.setTitle("You Sure delete check in?");
                     alertDialog.setIcon(R.drawable.ic_warning);
                     alertDialog.setPositiveButton("YES",
                             new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog,int which) {
                                     SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_CHECKIN, Context.MODE_PRIVATE);
									//checkin.remove(checkin.length() - 1 - position);
                                     String type = "hotel";
                                     try {
                                         if(getItem(position).getInt("provider_type_id") == 3) {
                                             type = "hotel";
                                         }else {
                                             type = "other";
                                         }
                                     } catch (JSONException e) {
                                         e.printStackTrace();
                                     }
                                     if(jsonArrayNativeRemove){
										 checkin.remove(checkin.length() - 1 - position);
									 }else {
										 checkin = remove(checkin, checkin.length() - 1 - position);
									 }

                                     sharedPreferences.edit().putString(type, checkin.toString()).commit();

									notifyDataSetChanged();

            						
                                 }
                             });
                     alertDialog.setNegativeButton("NO",
                             new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int which) {
                                 	dialog.dismiss();
                                 }
                             });

                     alertDialog.show();
            	
                	
                }
             });

			
			
			
			
		convertView.setTag(viewHolder);
			
			
		}else{
			viewHolder = (ViewHolder)convertView.getTag();
		}
		JSONObject  poi = getItem(position);
		
		try {
			viewHolder.textpoi.setText(poi.getString("name_en"));
			viewHolder.textaddress.setText(poi.getString("address_en")); 
			viewHolder.textdistance.setText(poi.getString("distance")+" "+"Km");
					
			

			viewHolder.btnroute.setVisibility(!delete?View.VISIBLE:View.GONE);
			viewHolder.btndelete.setVisibility(!delete?View.GONE:View.VISIBLE);
			
			
		} catch (JSONException e) {
			Log.e(CheckInActivity.class.getName(), "get selected poi error", e);
		}

		positionselect = position ;

		
		
		viewHolder.btnroute.setTag(getItem(position));
		viewHolder.btndelete.setTag(getItem(position));
		
		Log.i("Check in", "reloaded");
     return convertView;
	}



	class ViewHolder{
	
		TextView textpoi,textaddress,textdistance;
		Button btnroute,btndelete;
		
	}



	@Override
	public void onClick(View v) {
		final JSONObject  poiJson = (JSONObject)v.getTag();
		
		
	switch (v.getId()) {
	case R.id.btnroute:
		Uri gmmIntentUri;

		MainApplication.getInstance().trackScreenView("Menu_MyDestination_Route");
		MainApplication.getInstance().trackEvent("Menu_MyDestination_Route", "Menu_MyDestination_Route", "Send event Menu_MyDestination_Route");
		try {
			gmmIntentUri = Uri.parse("google.navigation:"+poiJson.getString("longitude")+","+poiJson.getString("latitude"))
			.buildUpon()
			.appendQueryParameter("q",poiJson.getString("name_en"))
			.build();
			Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
			mapIntent.setPackage("com.google.android.apps.maps");
			context.startActivity(mapIntent);
		} catch (JSONException e) {
			Toast.makeText(context, "POI"+e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	
	
		break;
	
	 }	
	}






	 private JSONArray remove(JSONArray a, int pos) {
		       JSONArray na = new JSONArray();
		         try {
			             for (int i = 0; i < a.length(); i++){
				                 if (i != pos) {
					                     na.put(a.get(i));
					                 }
				             }
			         } catch (JSONException e) {
			            // throw new JSONException(e);
			        }
		         return na;
		     }
	 private  boolean jsonArrayNativeRemove;
			     {
		         try {
			            jsonArrayNativeRemove = JSONArray.class.getMethod("remove", int.class) != null;
			         } catch (NoSuchMethodException e) {
			            jsonArrayNativeRemove = false;
			         }
		    }

 }