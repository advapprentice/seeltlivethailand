package com.touchtechnologies.tit.checkin;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.util.UserUtil;
import com.touchtechnologies.widget.ExpandableHeightListView;

import org.json.JSONException;
import org.json.JSONObject;

public class CheckInFragment extends Fragment implements OnClickListener,AnimationListener{
	private Button btnCheckin,btnDeleteAll,btnDelete;
    private Button btnDeleteAll_des,btnDelete_des;
	private ListView listCheckin;
	private CheckInAdapter  adapter;
    private CheckInAdapter  adapter_other;
	public View imgView,viewEdit,viewClear,viewMyLast,viewExit,viewNotCheckin;
    public View viewEdit_des,viewClear_des,viewMyLast_des,viewExit_des,viewNotCheckin_des;
	private Animation animBounce;
    private View headerView;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_main_checkin, container, false);

		adapter = new CheckInAdapter(getActivity());
        adapter_other = new CheckInAdapter(getActivity());

		CheckInDataObserver chkObserver = new CheckInDataObserver();
        CheckInDataObserver chkObserver2 = new CheckInDataObserver();

        headerView = inflater.inflate(R.layout.fragment_checkin, null);
        View view_preview =  headerView.findViewById(R.id.preview_provider);
        view_preview.setVisibility(View.GONE);
		ExpandableHeightListView listView = (ExpandableHeightListView) view.findViewById(R.id.list);
        ExpandableHeightListView listView2 = (ExpandableHeightListView) view.findViewById(R.id.list2);
        listView.setExpanded(true);
        listView2.setExpanded(true);
		listView.addHeaderView(headerView, null, false);
		listView.setAdapter(adapter);
        listView2.setAdapter(adapter_other);


		animBounce = AnimationUtils.loadAnimation(getActivity(),R.anim.bounce);
		animBounce.setAnimationListener(this);
//        imgView = headerView.findViewById(R.id.vewhead);
        btnDeleteAll = (Button)headerView.findViewById(R.id.delete_all_hotel_checkin);
        viewEdit = headerView.findViewById(R.id.viewEdit);
        btnDelete = (Button)headerView.findViewById(R.id.btndelete_hotel);


        viewEdit_des = view.findViewById(R.id.viewEdit_des);
        viewClear_des = view.findViewById(R.id.viewclear_des);
        viewMyLast_des = view.findViewById(R.id.view_my_latest_des);
        btnDeleteAll_des = (Button) view.findViewById(R.id.delete_all_des);
        btnDelete_des = (Button) view.findViewById(R.id.btndelete_des);
        viewNotCheckin_des = view.findViewById(R.id.viewNotCheckin_des);
        viewExit_des = view.findViewById(R.id.viewExit_des);

        viewNotCheckin = headerView.findViewById(R.id.viewNotCheckin);

        btnCheckin = (Button)view.findViewById(R.id.buttonCheckin);
        viewClear = view.findViewById(R.id.viewclear);
        viewMyLast= view.findViewById(R.id.view_my_latest);

        viewExit = headerView.findViewById(R.id.viewExit);
        btnCheckin.setOnClickListener((CheckInActivity)getActivity());
        headerView.findViewById(R.id.button_addhotel).setOnClickListener((CheckInActivity)getActivity());
        headerView.findViewById(R.id.button_addhotel).setVisibility(View.GONE);
        btnCheckin.setVisibility(View.GONE);
        btnDeleteAll.setOnClickListener(this);
        viewEdit.setOnClickListener(this);
        viewExit.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        viewEdit_des.setOnClickListener(this);
        viewExit_des.setOnClickListener(this);
        btnDeleteAll_des.setOnClickListener(this);
        btnDelete_des.setOnClickListener(this);
        view.findViewById(R.id.btndelete_visible_ex).setOnClickListener(this);


//		listView.setOnScrollListener(new AbsListView.OnScrollListener() {
//
//			@Override
//			public void onScrollStateChanged(AbsListView view, int scrollState) {
//
//			}
//
//			@Override
//			public void onScroll(AbsListView view, int firstVisibleItem,
//					int visibleItemCount, int totalItemCount) {
//
//				View headerView = view.findViewById(R.id.header);
//
////				final float mTop = -headerView.getTop();
////				float height = headerView.getHeight();
////				if (mTop > height) {
////					// ignore
////					return;
////				}
//
//
//
//
////				imgView.setTranslationY(mTop / 2f);
//
//			}
//		});

				
		adapter.registerDataSetObserver(chkObserver);
        adapter_other.registerDataSetObserver(chkObserver2);
		update("hotel");
        update("other");
		
		return view;
	}

	public void update(String key) {
        if(key == "hotel") {
            adapter.setData(UserUtil.getCheckIns(getActivity(), key));
            updateViews();
        }else {
            adapter_other.setData(UserUtil.getCheckIns(getActivity(),key));
            updataViews_other();
        }
	}
	
	void updateViews(){
		if(adapter == null || adapter.isEmpty()){
			 viewEdit.setVisibility(View.GONE);
			 viewMyLast.setVisibility(View.VISIBLE);
			 viewClear.setVisibility(View.GONE);
			 btnDeleteAll.setVisibility(View.GONE);
			 viewNotCheckin.setVisibility(View.VISIBLE);
			 
			 adapter.delete = false;
			 
		 }else if(adapter != null && !adapter.isEmpty()){
			 viewNotCheckin.setVisibility(View.GONE);
			 if(adapter.getDeleteStatus()){
				viewEdit.setVisibility(View.GONE); 
				viewMyLast.setVisibility(View.GONE);
				viewClear.setVisibility(View.VISIBLE);
				 btnDeleteAll.setVisibility(View.VISIBLE);
				
			 }else{
				viewEdit.setVisibility(View.VISIBLE); 
				viewMyLast.setVisibility(View.VISIBLE);
				viewClear.setVisibility(View.GONE);
				btnDeleteAll.setVisibility(View.GONE);
			}
		 }
	}

    void updataViews_other(){
        if(adapter_other == null || adapter_other.isEmpty()){
            viewEdit_des.setVisibility(View.GONE);
            viewMyLast_des.setVisibility(View.VISIBLE);
            viewClear_des.setVisibility(View.GONE);
            btnDeleteAll_des.setVisibility(View.GONE);
            viewNotCheckin_des.setVisibility(View.VISIBLE);

            adapter_other.delete = false;

        }else if(adapter_other != null && !adapter_other.isEmpty()){
            viewNotCheckin_des.setVisibility(View.GONE);
            if(adapter_other.getDeleteStatus()){
                viewEdit_des.setVisibility(View.GONE);
                viewMyLast_des.setVisibility(View.GONE);
                viewClear_des.setVisibility(View.VISIBLE);
                btnDeleteAll_des.setVisibility(View.VISIBLE);

            }else{
                viewEdit_des.setVisibility(View.VISIBLE);
                viewMyLast_des.setVisibility(View.VISIBLE);
                viewClear_des.setVisibility(View.GONE);
                btnDeleteAll_des.setVisibility(View.GONE);
            }
        }

    }

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.viewEdit:
				adapter.setDeleteButtons(true);
				update("hotel");
				viewClear.startAnimation(animBounce);
				break;
            case R.id.viewEdit_des:
                adapter_other.setDeleteButtons(true);
                update("other");
                viewClear_des.startAnimation(animBounce);
                break;
			case R.id.viewExit:
				adapter.setDeleteButtons(false);
				update("hotel");
				break;
            case R.id.viewExit_des:
                adapter_other.setDeleteButtons(false);
                update("other");
                break;
			case R.id.delete_all_hotel_checkin:
				
				 AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
		            alertDialog.setTitle("You Sure delete all?");
		            alertDialog.setIcon(R.drawable.ic_warning);
		            alertDialog.setPositiveButton("YES",
		                    new DialogInterface.OnClickListener() {
		                        public void onClick(DialogInterface dialog,int which) {
		                        	adapter.setDeleteButtons(false);
		                        	
		                        	UserUtil.clearCheckin(getActivity(),"hotel");
		                        	update("hotel");
		                        }
		                    });
		            alertDialog.setNegativeButton("NO",
		                    new DialogInterface.OnClickListener() {
		                        public void onClick(DialogInterface dialog, int which) {
		                        	dialog.dismiss();
		                        }
		                    });

		            alertDialog.show();
				break;
            case R.id.delete_all_des:
                AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(getActivity());
                alertDialog2.setTitle("You Sure delete all?");
                alertDialog2.setIcon(R.drawable.ic_warning);
                alertDialog2.setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int which) {
                                adapter_other.setDeleteButtons(false);

                                UserUtil.clearCheckin(getActivity(),"other");
                                update("other");
                            }
                        });
                alertDialog2.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog2.show();
                break;
            case R.id.btndelete_visible_ex:
                hideexample();
                break;
		}
		
	}

	@Override
	public void onAnimationStart(Animation animation) {
		
		
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}

	class CheckInDataObserver extends DataSetObserver {
		
		@Override
		public void onChanged() {
			updateViews();
            updataViews_other();
		}
	
		@Override
		public void onInvalidated() {
			Toast.makeText(getActivity(), "onInvalidated", Toast.LENGTH_SHORT).show();
		}
	}

    public void showexample(final JSONObject json, int type){
        if(headerView != null){
           final View view =  headerView.findViewById(R.id.preview_provider);
            view.setVisibility(View.VISIBLE);
            TextView textView = (TextView)headerView.findViewById(R.id.txt_distance_ex);
            TextView textView2 = (TextView)headerView.findViewById(R.id.textnamepoi_ex);
            TextView textView3 = (TextView)headerView.findViewById(R.id.txt_address_ex);

            try {
                textView2.setText(json.getString("name_en"));
                textView3.setText(json.getString("address_en"));
                textView.setText(json.getString("distance")+" "+"Km");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ImageView imageView = (ImageView)headerView.findViewById(R.id.img_pin_poi_ex);
            if(type == 2){
                imageView.setImageResource(R.drawable.ic_list_pin_res_2);
                headerView.findViewById(R.id.button_addhotel).setVisibility(View.GONE);
                headerView.findViewById(R.id.buttonCheckin).setVisibility(View.VISIBLE);
            }else if (type == 3){
                imageView.setImageResource(R.drawable.ic_list_pin_hotel_2);
                headerView.findViewById(R.id.button_addhotel).setVisibility(View.VISIBLE);
                headerView.findViewById(R.id.buttonCheckin).setVisibility(View.GONE);

            }else {
                imageView.setImageResource(R.drawable.ic_list_pin_att_2);
                headerView.findViewById(R.id.button_addhotel).setVisibility(View.GONE);
                headerView.findViewById(R.id.buttonCheckin).setVisibility(View.VISIBLE);

            }



        }


    }

    public void hideexample(){
        if(headerView != null){
            final View view =  headerView.findViewById(R.id.preview_provider);
            view.setVisibility(View.GONE);
            headerView.findViewById(R.id.button_addhotel).setVisibility(View.GONE);
            headerView.findViewById(R.id.buttonCheckin).setVisibility(View.GONE);
        }
    }

	
	
	
}