package com.touchtechnologies.tit;

import android.graphics.drawable.Drawable;

public class MenuInfo {
	/**
	 * Drawable resource use to display menu icon
	 */
	public Drawable icon;
	/**
	 * String use to display menu title
	 */
	public String titleString;
//	public boolean selected;
	
	/**
	 * Menu description
	 */
	public String desc;
	
	public MenuInfo(){
	}
	
	public MenuInfo(Drawable icon, String title){
		this.icon = icon;
		this.titleString = title;
	}
	
}
