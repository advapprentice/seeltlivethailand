package com.touchtechnologies.tit.live.liststreaming;


import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.tit.R;

 
public class LiveStreamingPlayer extends  FragmentActivity implements OnClickListener{
	private static final int SCREEN_ORIENTATION_LANDSCAPE = 90;
	private static ProgressDialog progressDialog;
    private LiveStreamingChannel livechannel; 
    private TextView titlevideo,detaillive,reporter,load;
    int position;
    int increment;
    ArrayList<LiveStreamingChannel> arrlive;
	ProgressDialog progDailog;
	DisplayImageOptions options;
	ImageView imageViewAvatar,refresh,mediaplay;
	Button infoshow,infocolse;
	VideoView vid;
	Button playvideo;
    ProgressBar progressBar ;
    Animation animBlink;
    DisplayMetrics dm; 
    SurfaceView sur_View; 
    MediaController media_Controller; 
    View viewback,viewdetaillive;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
      	setContentView(R.layout.activity_live_videoplayer_pager);

    	livechannel = (LiveStreamingChannel) getIntent().getSerializableExtra(LiveStreamingChannel.class.getName());	
  
        
    }
   
    	@Override
    	protected void onResume() {
    		super.onResume();
    
	//	final ProgressDialog progDailog = new ProgressDialog(this);
    	final MediaController mediaController = new MediaController(this);

    	final ProgressDialog pDialog = new ProgressDialog(this);

    	vid = (VideoView) findViewById(R.id.videoPlay);
    	titlevideo = (TextView)findViewById(R.id.textViewtitlevideo);     
    	detaillive = (TextView)findViewById(R.id.detaillive);     
    	reporter = (TextView)findViewById(R.id.reporter); 
    	mediaplay = (ImageView)findViewById(R.id.mediaplay); 
    	mediaplay.setVisibility(View.GONE);
    	
    	viewdetaillive =(View)findViewById(R.id.viewdetaillive);
    	viewdetaillive.setVisibility(View.GONE);
    	
    	infoshow = (Button)findViewById(R.id.btninfo); 
    	infoshow.setOnClickListener(this);
    	
    	infocolse = (Button)findViewById(R.id.btninfoclose);
    	infocolse.setOnClickListener(this);
    	infocolse.setVisibility(View.GONE);
    	
    	load = (TextView) findViewById(R.id.loading);
    	viewback = (View)findViewById(R.id.Viewback);
    	viewback.setOnClickListener(this);
    	
    	progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		progressBar.setVisibility(View.VISIBLE);

		playvideo = (Button) findViewById(R.id.play);
		playvideo.setVisibility(View.GONE);

		progressBar.setVisibility(View.VISIBLE);
		playvideo.setVisibility(View.GONE);

		mediaController.setAnchorView(vid);
	//	Uri video = Uri.parse("rtsp://203.170.193.73:1935/vod/sample.mp4");
		Uri video = Uri.parse(livechannel.getLiveStream().getUrlForm());
		titlevideo.setText(livechannel.getLiveStream().getTitle());
		
		if(livechannel.getLiveStream().getNote().length()<1){
			detaillive.setText(getText(R.string.up_detail_live));
		}else{
			detaillive.setText(livechannel.getLiveStream().getNote() == null?"":livechannel.getLiveStream().getNote() );	
		}
	//	reporter.setText(livechannel.getLiveStream().getReporter().getFullName());

	//	Uri video = Uri.parse(AppConfig.STREAM_URL );
		vid.setMediaController(mediaController);
		vid.setVideoURI(video);
		
		   	   	 vid.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

					public void onPrepared(MediaPlayer mp) {

						progressBar.setVisibility(View.GONE);
						load.setVisibility(View.GONE);
						
						vid.requestFocus();
						vid.start();
					 
						// Uri video =
						// Uri.parse("http://clips.vorwaerts-gmbh.de/VfE_html5.mp4");
						mediaController.setAnchorView(vid);
						vid.setMediaController(mediaController);
						mediaController.setMediaPlayer(vid);
						// progDailog.dismiss();
						mediaController.show(8000);
						if (pDialog.isShowing()) {
     		                pDialog.dismiss();
     		            }
     		            
						
					}
				});
		   		
			//////////// video complete	
				vid.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
		            public void onCompletion(MediaPlayer mp) {
		            	
		            	 mediaController.show(0);
		            	
		        			
		        				vid.requestFocus();
		        				vid.start();
		        				
		        				mediaController.setAnchorView(vid);
								vid.setMediaController(mediaController);
		        				mediaController.setMediaPlayer(vid);
								mediaController.show(0);
								
		        			}
		            	});
		           
			/////////////	
				vid.setOnErrorListener(new MediaPlayer.OnErrorListener() {
		            @Override
		            public boolean onError(MediaPlayer mp, int what, int extra) {
		            	pDialog.dismiss();
						AlertDialog alert = new Builder(LiveStreamingPlayer.this)
						.create();

						alert.setMessage(getText(R.string.up_not_livestream));
						alert.setButton(AlertDialog.BUTTON_NEUTRAL,getText(R.string.up_not_again),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								
								
								vid.requestFocus();
		        				vid.start();
		        				
		        				mediaController.setAnchorView(vid);
								vid.setMediaController(mediaController);
		        				mediaController.setMediaPlayer(vid);
								mediaController.show(0);
								
								finish();
								}
							});
						alert.show();

					
		                return true;
		            }
		        });
		//mediaController.setAnchorView(vid);
		//vid.setMediaController(mediaController);
	}
    	public void onConfigurationChanged(Configuration newConfig) {
    		super.onConfigurationChanged(newConfig);

    		// Checks the orientation of the screen
    		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
    			
    		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

    		
    		}
    	}
    	
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.Viewback:
				finish();
			break;
		case R.id.btninfo:
			viewdetaillive.setVisibility(View.VISIBLE);
			mediaplay.setVisibility(View.VISIBLE);
	    	infoshow.setVisibility(View.GONE);
			infocolse.setVisibility(View.VISIBLE);
		break;
		
		case R.id.btninfoclose:
			mediaplay.setVisibility(View.GONE);
			viewdetaillive.setVisibility(View.GONE);
			infoshow.setVisibility(View.VISIBLE);
			infocolse.setVisibility(View.GONE);
		break;
		

		
		}
		
	}

}
