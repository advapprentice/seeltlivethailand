package com.touchtechnologies.tit.live.liststreaming;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArrayMap;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.devbrackets.android.exomedia.listener.OnCompletionListener;
import com.devbrackets.android.exomedia.listener.OnErrorListener;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.EMVideoView;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.opensource.pagerindicator.CircleBitmapDisplayer;
import com.touchtechnologies.animate.dialog.MaterialDialog;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.LoveCommand;
import com.touchtechnologies.command.insightthailand.LoveUndoCommand;
import com.touchtechnologies.dataobject.insightthailand.Comment;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.dataobject.Datasocket;
import com.touchtechnologies.tit.live.mylivestream.MyliveGridviewActivity;
import com.touchtechnologies.tit.login.LoginsActivity;
import com.touchtechnologies.tit.member.MemberProfileActivity;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class LiveOnAirPlayerActivity extends  FragmentActivity implements OnClickListener{
	private static final int SCREEN_ORIENTATION_LANDSCAPE = 90;
	private static ProgressDialog progressDialog;
    private LiveStreamingChannel livechannel; 
    private TextView titlevideo,detaillive,reporter,load,currentView,textview_lovecount,
			textViewlocation,textView_watchcount;
	private boolean isStreamend = false;

	private View viewlivechat,viewAll,lin_title;
	private ImageLoader imageLoader;
	final private String TAG = LiveOnAirPlayerActivity.class.getSimpleName();
	int position;
    int increment;
    ArrayList<LiveStreamingChannel> arrlive;
	private Chat_list_adpter mChat_list_adpter;
	private ListView listView_chat;
	private List<Comment> lComments;
	private final Map<String, String> mHeaders = new ArrayMap<String, String>();
	private EditText  editText_addComment;
	ProgressDialog progDailog;
	DisplayImageOptions options;
	ImageView imageViewAvatar,refresh,ic_comment;
	VideoView vid;
    ProgressBar progressBar ;
	Socket mSocket;
	String cityName;
    Animation animBlink;
    DisplayMetrics dm; 
    SurfaceView sur_View; 
    MediaController media_Controller; 
    View viewback,viewDetail,viewMore,viewTitle,viewReporter,viewByRePort,viewLove,viewUnLove;
    ImageView imageReporter;
	EMVideoView mEmVideoView;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	//	this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_live_onairs);
		if(savedInstanceState != null){
			livechannel = (LiveStreamingChannel) savedInstanceState.getSerializable("save");
		}
        imageLoader = ImageLoader.getInstance();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
	        .threadPoolSize(3)
			.threadPriority(Thread.NORM_PRIORITY - 2)
			.denyCacheImageMultipleSizesInMemory()
            .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
			.memoryCacheSize(2 * 1024 * 1024)
            .defaultDisplayImageOptions(options).build();

		imageLoader.init(config);
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.imageScaleType(ImageScaleType.NONE)
				.showImageForEmptyUri(R.drawable.ic_new_imgaccount)
				.showImageOnFail(R.drawable.ic_new_imgaccount)
				.cacheOnDisc(true)
				.cacheInMemory(false)
				.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
				.bitmapConfig(Bitmap.Config.ARGB_8888)
				.displayer(new FadeInBitmapDisplayer(1000))
				.displayer(new CircleBitmapDisplayer())
				.build();
		
    	livechannel = (LiveStreamingChannel) getIntent().getSerializableExtra(LiveStreamingChannel.class.getName());
		//lComments = new ArrayList<>();
		if(savedInstanceState != null){
			lComments = (List<Comment>) savedInstanceState.getSerializable("test");
		}else {
			lComments = new ArrayList<>();
		}
		try{
			if (livechannel.getLiveStream().getLatitude() != 0) {
				try {
					Geocoder geocoder = new Geocoder(this, Locale.getDefault());
					List<Address> addresses;
					addresses = geocoder.getFromLocation(livechannel.getLiveStream().getLatitude(), livechannel.getLiveStream().getLongitude(), 1);
					//cityName = addresses.get(0).getAddressLine(0);
					cityName = addresses.get(0).getAdminArea();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}else {
				cityName = "Not found location";
			}
		}catch (Exception e){
			Log.e("ERROR",":"+e.getMessage());
		}

		if(UserUtil.isLoggedIn(this)){
			mHeaders.put("X-TIT-ACCESS-TOKEN", UserUtil.getUser(this).getToken());
		}
	//	ConnectSocket();

        
    }

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
    	protected void onResume() {
    		super.onResume();

    	final MediaController mediaController = new MediaController(this);
    	final ProgressDialog pDialog = new ProgressDialog(this);
		setview();

		mediaController.setAnchorView(vid);
	//	Uri video = Uri.parse("rtsp://203.170.193.73:1935/vod/sample.mp4");
		Uri video = Uri.parse(livechannel.getLiveStream().getHttp());
		textViewlocation.setText(cityName);
		try {
			titlevideo.setText(livechannel.getLiveStream().getTitle());
			reporter.setText(livechannel.getLiveStream().getUser().getFirstName() + " " + livechannel.getLiveStream().getUser().getLastName());
			currentView.setText(livechannel.getLiveStream().getWatchingCount());

			//currentView.setText(livechannel.getLiveStream().getWatchingCount());
		//	imageLoader.displayImage(livechannel.getLiveStream().getUser().getAvatar(), imageReporter, options);

		}catch (Exception e){
			Log.d("Error","Error"+e.getMessage());
		}

		if(livechannel.getLiveStream().getNote().length()<1){
		//	detaillive.setText(getText(R.string.not_detail));
		}else{
		//	detaillive.setText(livechannel.getLiveStream().getNote() == null?"":livechannel.getLiveStream().getNote() );
		}

		if(livechannel.getLiveStream().isLove()){
				viewUnLove.setVisibility(View.VISIBLE);
				viewLove.setVisibility(View.GONE);
		}else{
				viewLove.setVisibility(View.VISIBLE);
				viewUnLove.setVisibility(View.GONE);
		}
		ConnectSocket();
		mEmVideoView.setVideoURI(video);
		mEmVideoView.setOnPreparedListener(new OnPreparedListener() {
			@Override
			public void onPrepared() {
				mEmVideoView.start();
			}
		});
		mEmVideoView.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion() {
				Log.d(TAG, "onCompletion: Stream finished!!!!!!");
			}
		});
		mEmVideoView.setOnErrorListener(new OnErrorListener() {
			@Override
			public boolean onError() {
				Log.d(TAG, "onError: mEmVideoView");
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if(isStreamend) {
							Toast.makeText(LiveOnAirPlayerActivity.this, "live stream stop!!", Toast.LENGTH_LONG).show();
							final MaterialDialog materialDialog = new MaterialDialog(LiveOnAirPlayerActivity.this);
							materialDialog.setTitle("End")
									.setMessage("This Live Stream has finished!!!")
									.setPositiveButton("OK", new OnClickListener() {
										@Override
										public void onClick(View v) {
											materialDialog.dismiss();
											finish();
										}
									});
							materialDialog.show();
						}
					}
				});
				return false;
			}
		});
	

	}

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
    	getMenuInflater().inflate(R.menu.menushare, menu);
		super.onCreateOptionsMenu(menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
		}else if(item.getItemId() == R.id.menushare){
			try{
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_TEXT,livechannel.getLiveStream().getWeburl());
				startActivity(Intent.createChooser(intent, "Share whit..."));
				Log.d("url =", livechannel.getLiveStream().getWeburl());

				}catch(Exception e){
				Log.e("error", "share a link error", e);
				}
			
		}
		return super.onOptionsItemSelected(item);

	}

	@Override
	public void onClick(View v) {
		Intent intent;
		User user = livechannel.getLiveStream().getUser();
		User userLocal = UserUtil.getUser(this);
		switch (v.getId()) {
		case R.id.imageView_close:
				finish();
			break;
		case R.id.view_detail:
			viewMore.setVisibility(View.VISIBLE);
			viewDetail.setVisibility(View.GONE);
		break;
		case R.id.view_more:
			viewDetail.setVisibility(View.VISIBLE);
			viewMore.setVisibility(View.GONE);
		break;
		case R.id.imageview_ic_share_live:
			try{
				intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_TEXT,livechannel.getLiveStream().getUrlForm());
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				Log.d("url =",livechannel.getLiveStream().getWeburl());

				}catch(Exception e){
				Log.e("error", "share a link error", e);
				}
			
		break;
			case R.id.view_byreport:
				String idUser = userLocal.getId();

				if(user.getId().equals(idUser)){
					if(UserUtil.isLoggedIn(getApplicationContext())) {
						intent = new Intent(getApplicationContext(), MyliveGridviewActivity.class);
						startActivity(intent);
					}else{
						intent = new Intent(getApplicationContext(), LoginsActivity.class);
						startActivity(intent);
						finish();
					}
				}else{

					intent = new Intent(getApplicationContext(), MemberProfileActivity.class);
					intent.putExtra(User.class.getName(),user);
					startActivity(intent);
				}

				break;

			case R.id.viewLove:
				viewUnLove.setVisibility(View.VISIBLE);
				viewLove.setVisibility(View.GONE);
				if(UserUtil.isLoggedIn(this)) {
					isLove();
				}else{
					intent = new Intent(this, LoginsActivity.class);
					startActivity(intent);
				}

				break;
			case R.id.viewUnLove:
				viewUnLove.setVisibility(View.GONE);
				viewLove.setVisibility(View.VISIBLE);
				if(UserUtil.isLoggedIn(this)) {
					isUnLove();
				}else{
					intent = new Intent(this, LoginsActivity.class);
					startActivity(intent);
				}
				break;
			case R.id.imageView_comment_live:
					if (viewlivechat.getVisibility() == View.INVISIBLE){
						viewlivechat.setVisibility(View.VISIBLE);
					}else {
						viewlivechat.setVisibility(View.INVISIBLE);
					}
				break;
			case R.id.imageView_sent:
					addCommentfromtextbox();
				break;

		
		}
		
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);
		
//		ConnectSocket();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("save",livechannel);
		outState.putSerializable("test",mChat_list_adpter.getComments());
		disconnect();

	}

	@Override
	protected void onStop() {
		super.onStop();
		disconnect();

	}

	private Emitter.Listener watchcount = new Emitter.Listener() {
		@Override
		public void call(final Object... args) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Datasocket mDatasocket = new Datasocket(Datasocket.Socket_type.lovescount,args[0].toString());
							Datasocket.Vid_property mVid_property = (Datasocket.Vid_property)mDatasocket.getdata(Datasocket.Socket_type.lovescount);
							currentView.setText(mVid_property.getWatched()+"");
							livechannel.getLiveStream().setWatchingCount(mVid_property.getWatched());
						}
					});

				}
			});

		}
	};
	private Emitter.Listener mListenerlovecount = new Emitter.Listener() {
		@Override
		public void call(final Object... args) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					Datasocket mDatasocket = new Datasocket(Datasocket.Socket_type.lovescount,args[0].toString());
					Datasocket.Vid_property mVid_property = (Datasocket.Vid_property)mDatasocket.getdata(Datasocket.Socket_type.lovescount);
					textview_lovecount.setText(mVid_property.getLove()+"");
					livechannel.getLiveStream().setLove(mVid_property.getLove());

				}
			});
		}
	};

	public void isLove(){
		livechannel.getLiveStream().setLove(1);
		LoveCommand command = new LoveCommand(getApplicationContext());
		new Love(this).execute(command);

	}
	public void isUnLove(){

		User user = UserUtil.getUser(getApplicationContext());
		String url = ResourceUtil.getServiceUrl(getApplicationContext())+getApplicationContext().getString(R.string.app_service_love_live)+livechannel.getLiveStream().getId()+"/loves";
		Log.d("Source", "URL :" + url);
		new LoveUndoCommand(getApplicationContext(), user,url) {

			protected void onPostExecute(JSONObject result) {
					livechannel.getLiveStream().setLove(0);
				try {
					if(result!=null){
						int status = result.getInt("status");
						String message = result.getString("message");
						Log.d("message Response", ":" + message);
					}else{
						Toast.makeText(getApplicationContext(), "Fail! No Internet Connection", Toast.LENGTH_SHORT).show();
					}

				} catch (JSONException e) {
					e.getStackTrace();

				}

			}
		}.execute();

	}



	class Love extends CommonServiceTask {

		public Love(FragmentActivity context) {
			super(context);
		}


		@Override
		protected ServiceResponse doInBackground(Command... params) {

			String url = ResourceUtil.getServiceUrl(getApplicationContext())+getString(R.string.app_service_love_live)+livechannel.getLiveStream().getId()+"/loves";
			User user = UserUtil.getUser(getApplicationContext());
			Log.d("Source", "="); Log.d("Source", "URL :"+url);
			ServiceConnector srConnector = new ServiceConnector(url);

			List<NameValuePair> headers = new ArrayList<NameValuePair>();
			headers.add(new BasicNameValuePair("Content-Type", "application/json"));
			headers.add(new BasicNameValuePair("X-TIT-ACCESS-TOKEN", user.getToken()));

			ServiceResponse serviceResponse = srConnector.doAsynPost(params[0], true, headers);
			Log.d("headers", "URL :"+headers);

			try {
				int code = serviceResponse.getCode();
				if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
					JSONObject objuser = new JSONObject(serviceResponse.getContent().toString());

				}

			} catch (Exception e) {

				serviceResponse.setMessage(e.getMessage());
				Log.e(AppConfig.LOG, "doInBackground error", e);
			}

			return serviceResponse;
		}



		@Override
		protected void onPostExecute(ServiceResponse result) {
			super.onPostExecute(result);

			try {
				if (result.getCode() == ServiceResponse.SUCCESS|| result.getCode() == HttpStatus.SC_OK) {
					context.setResult(Activity.RESULT_OK);

				} else {
					Toast.makeText(context, "Update Fail"+"!\r\n" + result.getMessage(),Toast.LENGTH_LONG).show();

				}
			} catch (Exception e) {
				Toast.makeText(context, "ERROR!\r\n" + e.getMessage(),Toast.LENGTH_LONG).show();
				Log.e(AppConfig.LOG, "process response error", e);
			}
		}
	}


	private void ConnectSocket(){
		Log.d(TAG, "ConnectSocket: tttttttttttttttttttttttttt");
		mSocket = MainApplication.getmSocket();
		if (mSocket.connected()){
			offallsocketlistener();
			mSocket.emit(MainApplication.mJoinroom, "streamlive/" + livechannel.getLiveStream().getId(), new Ack() {
				@Override
				public void call(Object... args) {
					Log.d(TAG, "callsocket: ");
					mSocket.open();
					mSocket.on("lovescount:update",mListenerlovecount);
					mSocket.on("watchedcount:update",watchcount);
					mSocket.on("comment:new",mCommentNew);
					mSocket.on("streaming:finish",onStreamfinish);
				}
			});
		}else {
			mSocket.connect();
			mSocket.on("ack-connected", onAckConnected);
		}
	}

	private Emitter.Listener onAckConnected = new Emitter.Listener() {
		@Override
		public void call(Object... args) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					mSocket.off();

					mSocket.emit(MainApplication.mJoinroom, "streamlive/" + livechannel.getLiveStream().getId(), new Ack() {
						@Override
						public void call(Object... args) {
							Log.d(TAG, "callsocket:  join "+"streamlive/" + livechannel.getLiveStream().getId());
							mSocket.open();
							mSocket.on("watchedcount:update",watchcount);
							mSocket.on("lovescount:update",mListenerlovecount);
							mSocket.on("comment:new",mCommentNew);
							mSocket.on("streaming:finish",onStreamfinish);
						}
					});
				}
			});
		}
	};

	private Emitter.Listener mCommentNew = new Emitter.Listener() {
		@Override
		public void call(final Object... args) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Datasocket mDatasocket = new Datasocket(Datasocket.Socket_type.comment, args[0].toString());
					Datasocket.Vid_Comment comment = (Datasocket.Vid_Comment) mDatasocket.getdata(Datasocket.Socket_type.comment);
						mChat_list_adpter.addComments(comment.getContent());
						scrollMyListViewToBottom();
					Log.d(TAG, "mCommentNew: ");


				}
			});
		}
	};

	private void setview(){

		imageReporter = (ImageView) findViewById(R.id.imageReporter);
		mEmVideoView = (EMVideoView) findViewById(R.id.video_play_activity_video_view);
		vid = (VideoView) findViewById(R.id.videoPlay);
		titlevideo = (TextView)findViewById(R.id.textViewtitlevideo);
		ic_comment = (ImageView) findViewById(R.id.imageView_comment_live);
		//detaillive = (TextView)findViewById(R.id.detaillive);
		reporter = (TextView)findViewById(R.id.textView_name_stream);
		load = (TextView) findViewById(R.id.loading);
		currentView = (TextView) findViewById(R.id.currentView);
		listView_chat = (ListView) findViewById(R.id.listView5);
		viewback = findViewById(R.id.imageView_close);
		viewLove  = (View)findViewById(R.id.viewLove);
		viewUnLove = (View)findViewById(R.id.viewUnLove);
		textview_lovecount = (TextView) findViewById(R.id.textView_love_count);
		viewlivechat = findViewById(R.id.View_live_chat);
		textViewlocation = (TextView) findViewById(R.id.textView_location);
		editText_addComment = (EditText) findViewById(R.id.editText2);


		//viewDetail.setOnClickListener(this);
		//viewMore.setOnClickListener(this);
		viewback.setOnClickListener(this);
		//viewByRePort.setOnClickListener(this);
		viewLove.setOnClickListener(this);
		viewUnLove.setOnClickListener(this);
		ic_comment.setOnClickListener(this);
		findViewById(R.id.imageview_ic_share_live).setOnClickListener(this);
		findViewById(R.id.imageView_sent).setOnClickListener(this);


		progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		progressBar.setVisibility(View.VISIBLE);
		progressBar.setVisibility(View.VISIBLE);
		mChat_list_adpter = new Chat_list_adpter(this,imageLoader,options);
		mChat_list_adpter.setComments(lComments);
		listView_chat.setAdapter(mChat_list_adpter);
		int mOri =   getResources().getConfiguration().orientation;
		if (mOri == 2){
			viewAll = findViewById(R.id.view_all);
			lin_title = findViewById(R.id.LinearLayout_title);
			mEmVideoView.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (lin_title.getVisibility() == View.INVISIBLE) {
						lin_title.setVisibility(View.VISIBLE);
						lin_title.postDelayed(new Runnable() {
							@Override
							public void run() {
								lin_title.setVisibility(View.INVISIBLE);
							}
						},5000);
					}
					return false;
				}
			});
			lin_title.postDelayed(new Runnable() {
				@Override
				public void run() {
					lin_title.setVisibility(View.INVISIBLE);
				}
			},5000);
		}

	}

	private boolean checklogin(){
		return  UserUtil.isLoggedIn(this);
	}

	private void addCommentfromtextbox(){
		if(checklogin()) {
			String comment = editText_addComment.getText().toString().trim();
			View view = this.getCurrentFocus();
			if (view != null) {
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
			}
			if (!comment.isEmpty() && comment != null) {
				editText_addComment.setText("");
				String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_streamlive) + livechannel.getLiveStream().getId() + getResources().getString(R.string.app_service_rest_comments);
				JSONObject jsonObject = new JSONObject();
				try {
					jsonObject.put("comment_content", comment);
					JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
						@Override
						public void onResponse(JSONObject jsonObject) {
							//onRefersh();

						}
					}, new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError volleyError) {

						}
					}){
						@Override
						public Map<String, String> getHeaders() throws AuthFailureError {
							return mHeaders;
						}
					};

					MainApplication.getInstance().addToRequestQueue(jsonObjectRequest);
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
		}else {
			Toast.makeText(this,"please login before comment",Toast.LENGTH_SHORT).show();
		}
	}

	private void scrollMyListViewToBottom() {
		listView_chat.post(new Runnable() {
			@Override
			public void run() {
				// Select the last row so it will scroll into view...
				listView_chat.setSelection(mChat_list_adpter.getCount() - 1);
			}
		});
	}

	private Emitter.Listener onStreamfinish = new Emitter.Listener() {
		@Override
		public void call(Object... args) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
				 isStreamend = true;
					Log.d(TAG, "run: stream:end");
				}
			});
		}
	};

	private void disconnect(){
		mSocket.off("watchedcount:update",watchcount);
		mSocket.off("lovescount:update",mListenerlovecount);
		mSocket.off("comment:new",mCommentNew);
		mSocket.off("streaming:finish",onStreamfinish);
		mSocket.off();
		mSocket.disconnect();
	}
	private void offallsocketlistener(){
		mSocket.off("watchedcount:update",watchcount);
		mSocket.off("lovescount:update",mListenerlovecount);
		mSocket.off("comment:new",mCommentNew);
		mSocket.off("streaming:finish",onStreamfinish);
		mSocket.off();
	}

}
