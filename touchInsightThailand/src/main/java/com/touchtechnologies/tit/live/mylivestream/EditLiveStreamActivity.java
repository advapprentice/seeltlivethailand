package com.touchtechnologies.tit.live.mylivestream;

import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.animation.ValueAnimator;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.touchtechnologies.animate.button.CircularProgressButton;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.RequestLivestreamCommand;
import com.touchtechnologies.command.insightthailand.RestServiceDeleteMyLiveCommand;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamConnection;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Edit live stream content; title, description, delete and etc.
 * @author Touch
 *
 */
public class EditLiveStreamActivity  extends FragmentActivity implements OnClickListener{
	private StreamHistory myhistory;
	private ImageView imgSnapShot;
	private TextView txtTitle,txtCreate,txtUpdate,txtViewer;
	private DisplayImageOptions options;
	private ImageLoader imageLoader;
	private View viewVideo;
	private Switch mSwitch;
	private int is_public;
	private CircularProgressButton circularButton;
	EditText editTxtTitle,editTxtNote;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_update_live_stream);
			 imageLoader = ImageLoader.getInstance();

			imageLoader = ImageLoader.getInstance();
		    options = new DisplayImageOptions.Builder().cacheInMemory(true)
					.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
					.imageScaleType(ImageScaleType.NONE)
					.showImageForEmptyUri(R.drawable.bg_cctvdefault)
					.showImageOnFail(R.drawable.bg_cctvdefault)
					.showImageOnLoading(R.drawable.bg_cctvdefault)
					.resetViewBeforeLoading(true).build();

	 	    imageLoader = ImageLoader.getInstance();

		    ActionBar actionBar = getActionBar();
		    actionBar.setDisplayShowTitleEnabled(true);
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setBackgroundDrawable(new ColorDrawable(0xFF1275AE));
			actionBar.setTitle(Html.fromHtml("<font color='#FFFFFF'>"+"Update My Streaming"+"</font>"));
			actionBar.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
			actionBar.setDisplayUseLogoEnabled(false);

			myhistory = (StreamHistory) getIntent().getSerializableExtra(StreamHistory.class.getName());

	        circularButton = (CircularProgressButton) findViewById(R.id.btnUpdate);
	        circularButton.setOnClickListener(this);

			mSwitch = (Switch) findViewById(R.id.switch_public);
			txtTitle =(TextView)findViewById(R.id.txtTitle);
			txtCreate =(TextView)findViewById(R.id.txtCreate);
			txtUpdate =(TextView)findViewById(R.id.txtLastUpdate);
			imgSnapShot =(ImageView)findViewById(R.id.imgSnapShot);
			viewVideo = (View)findViewById(R.id.viewVideo);
			editTxtTitle= (EditText)findViewById(R.id.editTxtTitle);
			editTxtNote= (EditText)findViewById(R.id.editTxtNote);
			txtViewer = (TextView)findViewById(R.id.textView5);

			viewVideo.setOnClickListener(this);

			mSwitch.setChecked((myhistory.getIspublic() > 0)? true:false);
		   txtTitle.setText(myhistory.getTitle());
		   txtCreate.setText(myhistory.getCreatedtDates() > 0?myhistory.getCreatedDateStrings():"-");
		   txtUpdate.setText(myhistory.getUpDates() > 0?myhistory.getUpdatedDateStrings():"-");
		   editTxtTitle.setText(myhistory.getTitle());
			txtViewer.setText(myhistory.getWatchedCount());
		   editTxtNote.setText(myhistory.getNote());

		   imageLoader.displayImage(myhistory.getSnapshots(), imgSnapShot, options);
		mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				is_public = (isChecked)?1:0;
			}
		});
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {
		case R.id.viewVideo:
			intent = new Intent(this, MylivePlayer.class);
			intent.putExtra(StreamHistory.class.getName(),myhistory);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
		case R.id.btnUpdate:

			     if (circularButton.getProgress() == 0) {
					 commonstreaming();
					 circularButton.setIndeterminateProgressMode(true);
	                } else {
	                	circularButton.setProgress(0);
	                }


		break;
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.my_live, menu);


		return true;
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.delete) {
			deleteLiveStream();
			return true;

		}else if(id == android.R.id.home){
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void deleteLiveStream() {

		final User user = UserUtil.getUser(this);
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle("You Sure delete Video?");
		alertDialog.setIcon(R.drawable.ic_warning);

		alertDialog.setNegativeButton("NO",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});

		alertDialog.setPositiveButton("YES",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						final String url = ResourceUtil.getServiceUrl(getApplicationContext())
								+ getResources().getString(
										R.string.app_service_delete_my_live)
								+ user.getId() + "/stream/" + myhistory.getId();
						new RestServiceDeleteMyLiveCommand(getBaseContext(), user,url) {

							protected void onPostExecute(JSONObject result) {

								try {
									if(result!=null){
									int status = result.getInt("status");
									String message = result.getString("message");

										if(status == 0){

											Toast.makeText(getApplicationContext(),"Success ",Toast.LENGTH_SHORT).show();
											setResult(1);
											finish();
										}else{

										Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
									}
									}else{
										Toast.makeText(getApplicationContext(),"Fail! No Internet Connection",Toast.LENGTH_SHORT).show();
									}


								} catch (JSONException e) {
									e.getStackTrace();

								}

								Log.d("URL : ", "" + url);
							};
						}.execute();

					}
				});

		alertDialog.show();

    }


	LiveStreamConnection PutStreamConnection() {
		LiveStreamConnection stremconnect = new LiveStreamConnection();

		stremconnect.setTitle(editTxtTitle.getText().toString());
		stremconnect.setNote(editTxtNote.getText().toString());
		stremconnect.setIspublic(is_public);

		return stremconnect;
	}



	public void commonstreaming() {

		RequestLivestreamCommand getCommand = new RequestLivestreamCommand(getApplicationContext());

		getCommand.setLivestream(PutStreamConnection());
		getCommand.setUser(UserUtil.getUser(getApplicationContext()));
		Log.i("Command", "Content:" + getCommand.toString());

		new GetStreamServiceTask(this).execute(getCommand);

	}

	class GetStreamServiceTask extends CommonServiceTask {
		public GetStreamServiceTask(FragmentActivity context) {
			super(context);
		}

		@Override
		protected ServiceResponse doInBackground(Command... params) {
			ServiceConnector srConnector = new ServiceConnector(ResourceUtil.getServiceUrl(context) + getString(R.string.app_service_rest_edit_mylive_histories)+myhistory.getId());

			User user = UserUtil.getUser(getApplicationContext());
			List<NameValuePair> headers = new ArrayList<NameValuePair>();
			headers.add(new BasicNameValuePair("Content-Type", "application/json"));
			headers.add(new BasicNameValuePair("X-TIT-ACCESS-TOKEN", user.getToken()));
			ServiceResponse serviceResponse = srConnector.doAsynPatch(params[0], true, headers);
			Log.d("headers", "URL :"+headers);
			try {
				int code = serviceResponse.getCode();
				if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
					JSONObject json = new JSONObject(serviceResponse.getContent().toString());

					Log.d("TIT", "Response" + json);
				}
			} catch (Exception e) {
				Log.e("TIT", "doInBackground error", e);
			}

			return serviceResponse;
		}

		@Override
		protected void onPostExecute(ServiceResponse result) {
			super.onPostExecute(result);

			try {
				switch (result.getCode()) {
					case ServiceResponse.SUCCESS:
					case HttpStatus.SC_OK:
						simulateSuccessProgress(circularButton);
						context.setResult(Activity.RESULT_OK);
						setResult(1);


						break;
					case 2://restore token failed
						//TODO logout

						break;


				}
			} catch (Exception e) {
				// Toast.makeText(context, "ERROR!\r\n" + e.getMessage(),	Toast.LENGTH_LONG).show();
				Log.e("TIT", "process response error", e);
				simulateErrorProgress(circularButton);
			}
		}

		private void simulateSuccessProgress(final CircularProgressButton button) {
			ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 100);
			widthAnimation.setDuration(1500);
			widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
			widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
				@Override
				public void onAnimationUpdate(ValueAnimator animation) {
					Integer value = (Integer) animation.getAnimatedValue();
					button.setProgress(value);

					button.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							finish();
						}
					});


				}
			});
			widthAnimation.start();
		}

		private void simulateErrorProgress(final CircularProgressButton button) {
			ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 99);
			widthAnimation.setDuration(1500);
			widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
			widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
				@Override
				public void onAnimationUpdate(ValueAnimator animation) {
					Integer value = (Integer) animation.getAnimatedValue();
					button.setProgress(value);
					if (value == 99) {
						button.setProgress(-1);
					}
				}
			});
			widthAnimation.start();
		}

	}
}
