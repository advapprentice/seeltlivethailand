package com.touchtechnologies.tit.live.mylivestream;


import java.util.List;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.opensource.pagerindicator.CircleBitmapDisplayer;
import com.touchtechnologies.animate.gridview.FloatingAction;
import com.touchtechnologies.animate.gridview.GridViewWithHeaderAndFooter;
import com.touchtechnologies.animate.progress.RotateLoading;
import com.touchtechnologies.command.insightthailand.RestServiceMyLiveHistoryListCommand;
import com.touchtechnologies.command.insightthailand.RestServiceUserCommand;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.cctvitlive.LiveStreamFragment2;
import com.touchtechnologies.tit.cctvitlive.LiveStreamListener;
import com.touchtechnologies.tit.live.category.ScrollTabHolderFragment;
import com.touchtechnologies.tit.login.LoginsActivity;
import com.touchtechnologies.tit.member.ListFollowingActivity;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;
import com.touchtechnologies.wifi.WIFIMod;

/**
 * List my live stream fragment
 * @author Touch
 */
public class FragmentMyliveStream extends Fragment implements OnScrollListener, OnRefreshListener, LiveStreamListener,OnClickListener {
	private static final String ARG_POSITION = "position";
	private SwipeRefreshLayout mSwipeRefreshLayout;
	private View viewEmptyHint,viewnotlive;
	private View viewnolive,viewProfile;
	private FragmentMyliveStreamAdapter adapter;
	private GridViewWithHeaderAndFooter listView ;
	private Context context;
	private List<StreamHistory> hisstory;
	protected int action_refresh;
	private boolean loading = false;
	private int previousTotal = 0;
	private int visibleThreshold = 2;
	private User user;
	private FloatingAction mFloatingAction;
	private int mPosition;
	private int liveSize;
	private TextView txtVideoCount,txtName,txtCountFollower,txtCountFollowing;
	private View placeHolderView,viewDot;
	private View viewHeaderMylive,viewFollower,viewFollowing,viewLogin;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private ImageView imgProfile;
	private Button btnLogin;
	public static Fragment newInstance(int position) {
		FragmentMyliveStream fragment = new FragmentMyliveStream();
		Bundle b = new Bundle();
		b.putInt(ARG_POSITION, position);
		fragment.setArguments(b);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View listViewContainer = inflater.inflate(R.layout.fragment_mylive_gridview, container, false);
		user = UserUtil.getUser(getActivity());
		mPosition = getArguments().getInt(ARG_POSITION);
		liveSize = getArguments().getInt("liveSize",0);


		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.imageScaleType(ImageScaleType.NONE)
				.showImageForEmptyUri(R.drawable.anonymous)
				.showImageOnFail(R.drawable.anonymous)
				.cacheInMemory(false)
				.displayer(new FadeInBitmapDisplayer(100))
				.displayer(new CircleBitmapDisplayer())
				.build();
		imageLoader = ImageLoader.getInstance();

	//	Toast.makeText(getActivity(), "size " + liveSize, Toast.LENGTH_LONG).show();

		MainApplication.getInstance().trackScreenView("LiveStream_MyLive");

		viewEmptyHint = (View)listViewContainer.findViewById(R.id.emptyAgentHint);
		
		if(!WIFIMod.isNetworkConnected(getActivity())){
			viewEmptyHint.setVisibility(View.VISIBLE);
		}else{
			viewEmptyHint.setVisibility(View.GONE);
		}
	//	adapter = new FragmentMyliveStreamAdapter(getActivity(), this);
		adapter = new FragmentMyliveStreamAdapter(getActivity());
		mSwipeRefreshLayout = (SwipeRefreshLayout) listViewContainer.findViewById(R.id.container);
		mSwipeRefreshLayout.setOnRefreshListener(this);
	//	mSwipeRefreshLayout.setVisibility(View.GONE);


		viewDot = (View)listViewContainer.findViewById(R.id.viewLoading);
		viewDot.setVisibility(View.GONE);

		viewLogin = (View)listViewContainer.findViewById(R.id.viewLogin);
		viewLogin.setVisibility(View.GONE);


		viewnotlive =(View) listViewContainer.findViewById(R.id.viewnotlive);
		viewnotlive.setVisibility(View.GONE);

		listView = (GridViewWithHeaderAndFooter)listViewContainer.findViewById(R.id.grid_view);


		LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
		View headerView = layoutInflater.inflate(R.layout.item_header_my_live, null);
		txtVideoCount =(TextView)headerView.findViewById(R.id.txtVideoCount) ;
		txtName =(TextView)headerView.findViewById(R.id.txtName) ;
		txtCountFollower =(TextView)headerView.findViewById(R.id.txtCountFollower) ;
		txtCountFollowing =(TextView)headerView.findViewById(R.id.txtCountFollowing) ;

		viewFollower =(View)headerView.findViewById(R.id.viewFollower);
		viewFollowing =(View)headerView.findViewById(R.id.viewFollowing);
		btnLogin =(Button)listViewContainer.findViewById(R.id.btnLogin);


		viewHeaderMylive =(View)headerView.findViewById(R.id.viewHeaderMylive) ;
		imgProfile= (ImageView) headerView.findViewById(R.id.imgProfile);

		txtName.setText(user.getFirstName());
		imageLoader.displayImage(user.getAvatar(), imgProfile, options);

		viewHeaderMylive.setVisibility(View.GONE);

		listView.addHeaderView(headerView);

		/// setheader

	//	viewDot.setPadding(0, getResources().getDimensionPixelSize(R.dimen.pading_loading_null), 0, 0);

	//	placeHolderView.setPadding(0,getResources().getDimensionPixelSize(R.dimen.header_height_frame_null), 0, 0);


		listView.setAdapter(adapter);


		viewFollower.setOnClickListener(this);
		viewFollowing.setOnClickListener(this);
		btnLogin.setOnClickListener(this);

	    adapter.notifyDataSetChanged();
		listView.setOnItemClickListener(myOnItemClickListener);
		if(UserUtil.isLoggedIn(getActivity())){
			viewDot.setVisibility(View.VISIBLE);
			onRefresh();
		}else {
			viewLogin.setVisibility(View.VISIBLE);
			viewnotlive.setVisibility(View.GONE);
		}

		return listViewContainer;
	}

	public void updateFollow(){
		String url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_rest_user);
		new RestServiceUserCommand(getActivity(), user,url){

			protected void onPostExecute(User result ) {

				User getProfile  = new User();
				getProfile	= result;
				txtCountFollowing.setText(""+getProfile.getFollowing());
				txtCountFollower.setText("" +getProfile.getFollower());


			}
		}.execute();


	}

	@Override
	public void ready(List<LiveStreamingChannel> list) {
		liveSize = list.size();
	//	placeHolderView.setPadding(0,getResources().getDimensionPixelSize(list.size()==0?R.dimen.header_height_frame_null:R.dimen.header_height_frame), 0, 0);
	}
	public GridView getGridView() {
		return listView;
	}

	public void onRefresh(int action){
		Log.d(AppConfig.LOG, "refreshing my stream...");
		action_refresh = action;
		updateFollow();
	
		if (!loading) {
			String url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_rest_mylive_histories);
			
			new RestServiceMyLiveHistoryListCommand(getActivity(),user, url){
		
				protected void onPostExecute(java.util.List<StreamHistory> result) {
					hisstory = result;
					try{
						txtVideoCount.setText(""+hisstory.size());

						if(UserUtil.isLoggedIn(getActivity())){
							if (hisstory.size()==0) {
								viewnotlive.setVisibility(View.VISIBLE);
							} else {
								viewnotlive.setVisibility(View.GONE);
								viewHeaderMylive.setVisibility(View.VISIBLE);
							}
						}
					}catch (Exception e){
						Log.i("Error",":"+e.getMessage());
					}



					adapter.setData(hisstory);
				//	pager.setCurrentItem(1);
					adapter.notifyDataSetChanged();
					viewDot.setVisibility(View.GONE);
				};
			}.execute();

			
		}	
	}
	
	OnItemClickListener myOnItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			 try {
				  	Intent intent = new Intent(getActivity(), MylivePlayer.class);
					intent.putExtra(StreamHistory.class.getName(),adapter.getItem(position));
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
							
			} catch (ActivityNotFoundException activityException) {
			        Log.e("dialing", "Call failed", activityException);
		    }

		}
	};
	

	@Override
	public void onRefresh() {
		onRefresh(MotionEvent.ACTION_DOWN);
		mSwipeRefreshLayout.setRefreshing(false);
		
	}


	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		
	}

	public void onScroll(AbsListView abs, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		
		if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }
        if (!loading & totalItemCount > 0 && (totalItemCount + visibleItemCount) <= (firstVisibleItem - visibleThreshold)) {
            // load the next feeds using a background task,
        	onRefresh();
        	
            loading = true;
        }

		
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		Toast.makeText(getActivity(), "Aloha", Toast.LENGTH_SHORT).show();
	    if (requestCode == 1) {
	    	Toast.makeText(context,"1", Toast.LENGTH_SHORT).show();
	    	onRefresh();
	   }
	}//onActivityResult
	
	@Override
	public void onClick(View v) {
		Intent intent;
		StreamHistory myhistory = (StreamHistory)v.getTag();
		switch (v.getId()) {
		case R.id.imgEdit:
			intent = new Intent(getActivity(), EditLiveStreamActivity.class);
			intent.putExtra(StreamHistory.class.getName(),myhistory);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivityForResult(intent, AppConfig.REQUEST_CODE_EDIT_LIVESTREAM);
			break;
		case R.id.viewFollower:
			intent = new Intent(getActivity(),ListFollowingActivity.class);
			intent.putExtra(User.class.getName(),user);
			intent.putExtra("key", "follower");
			intent.putExtra("key_title", "Followers");
			getActivity().startActivityForResult(intent,3);
			break;
		case R.id.viewFollowing:
			intent = new Intent(getActivity(),ListFollowingActivity.class);
			intent.putExtra(User.class.getName(),user);
			intent.putExtra("key","following");
			intent.putExtra("key_title", "Following");
			getActivity().startActivityForResult(intent,3);
			break;
		case R.id.btnLogin:
			intent = new Intent(getActivity(), LoginsActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		updateFollow();
		onRefresh();
	}
}

