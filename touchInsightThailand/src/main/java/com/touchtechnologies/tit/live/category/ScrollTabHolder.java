package com.touchtechnologies.tit.live.category;

/**
 * Created by TouchICS on 3/26/2016.
 */

import android.widget.AbsListView;

public interface ScrollTabHolder {

    void adjustScroll(int scrollHeight);

    void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount, int pagePosition);
}