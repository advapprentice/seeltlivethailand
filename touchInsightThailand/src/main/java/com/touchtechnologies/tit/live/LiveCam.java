package com.touchtechnologies.tit.live;


import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.majorkernelpanic.streaming.Session;
import net.majorkernelpanic.streaming.SessionBuilder;
import net.majorkernelpanic.streaming.audio.AudioQuality;
import net.majorkernelpanic.streaming.gl.SurfaceView;
import net.majorkernelpanic.streaming.rtsp.RtspClient;
import net.majorkernelpanic.streaming.video.VideoQuality;
import net.majorkernelpanic.streaming.video.VideoStream;

import org.apache.http.HttpStatus;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.PorterDuff;
import android.hardware.Camera;

import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

//import com.facebook.android.DialogError;
//import com.facebook.android.Facebook;
//import com.facebook.android.Facebook.DialogListener;
//import com.facebook.android.FacebookError;
import com.facebook.share.ShareApi;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.appindexing.AppIndex;
//import com.google.android.gms.common.api.GoogleApiClient;
import com.touchtechnologies.animate.progress.DotsTextView;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.RequestLivestreamCommand;
import com.touchtechnologies.command.insightthailand.RestServiceCategoryListCommand;
import com.touchtechnologies.command.insightthailand.RestServiceUserCommand;
import com.touchtechnologies.dataobject.insightthailand.CategoryLiveStream;
import com.touchtechnologies.dataobject.insightthailand.Comment;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamConnection;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.dataobject.Datasocket;
import com.touchtechnologies.tit.live.liststreaming.Chat_list_adpter;
import com.touchtechnologies.tit.live.liststreaming.LiveOnAirPlayerActivity;
import com.touchtechnologies.tit.login.LoginsActivity;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.util.DataCacheUtill;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class LiveCam extends FragmentActivity implements RtspClient.Callback,
		Session.Callback, SurfaceHolder.Callback, OnClickListener {
	public final static String TAG = LiveCam.class.getSimpleName();
	private static SurfaceView mSurfaceView;
	private float mDist;
	// Rtsp session
	private Session mSession;
	private static RtspClient mClient;
	//	Animation animBlink;
	private LiveStreamConnection livestream;
	private Geocoder geocoder;
	private List<Comment> lComments;
	private Chat_list_adpter mChat_list_adpter;
	private Socket mSocket;
	private int[] resolutionX = {1280,800,640,720,720,320,176};
	private int[] resolutionY = {720,480,480,720,480,240,144};
	int count = 0;
	double sec;
	double min;
	double hr;
	double latitude;
	double longitude;
	boolean activityAlived = true;
	CheckBox flash;
	boolean live_unlimited = true;
	private ImageView live, imgSwitchCamera;
	private Timer T;
	private TextView texttime, textlive, textLocation, title, txtTitlelive, textTimeLive, txtTimeOut, waitConnect,
			 text_love_count,text_watch_count,textView_province;
	private DotsTextView textconnect;
	private View viewtime, viewconnect,  viewDialogTitle, viewTitlelive, viewTimeOut,viewShareFb,viewheader,viewfooter,
	     viewchat,viewExit;
	private Animation animBlink,animBlink2;
	private Button btnBubleStart, btnstop, btnstart,btnShareFacebook;
	private  ImageView imgLiveNow;
	SurfaceHolder surface_holder;
	ProgressBar progress;
	Spinner spinnercat;
	ListView list_chat;
	private List<CategoryLiveStream> arrCategoryLive = new ArrayList<CategoryLiveStream>();
	CategoryLiveStream categoryLiveStream;
	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API.
	 * See https://g.co/AppIndexing/AndroidStudio for more information.
	 */
//	private GoogleApiClient client;
	private Context context;
//	Facebook facebook;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live);
//      this.context = context;
//		facebook = new Facebook(ResourceUtil.getFacebookID(this));
        if(!activityAlived)
            return;
		livestream = (LiveStreamConnection) getIntent().getSerializableExtra(LiveStreamConnection.class.getName());
		mSurfaceView = (SurfaceView) findViewById(R.id.surface);
		progress = (ProgressBar) findViewById(R.id.progressBar1);
		progress.getIndeterminateDrawable().setColorFilter(0xFF568ae8, PorterDuff.Mode.MULTIPLY);

		textlive = (TextView) findViewById(R.id.textViewtitlelive);
		viewheader = findViewById(R.id.View_live_detail);
		textView_province = (TextView) findViewById(R.id.textview_province);
		viewfooter= findViewById(R.id.View_Live_Footer);
		texttime = (TextView) findViewById(R.id.tvtimer);
		textLocation = (TextView) findViewById(R.id.txtLocationName);
		title = (EditText) findViewById(R.id.editTitle);
		txtTitlelive = (TextView) findViewById(R.id.txtTitleLive);
		textTimeLive = (TextView) findViewById(R.id.txtTimeLive);
		txtTimeOut = (TextView) findViewById(R.id.txtTimeOut);
		viewTimeOut = (View) findViewById(R.id.viewTimeOut);
		viewTitlelive = (View) findViewById(R.id.View_live_detail);
		viewtime = (View) findViewById(R.id.Time);
		viewExit = (View) findViewById(R.id.viewExit);
		viewDialogTitle = (View) findViewById(R.id.viewDialogTitle);
		btnBubleStart = (Button) findViewById(R.id.btnBubleStart);
		btnstop = (Button) findViewById(R.id.btnstop);
		btnstart = (Button) findViewById(R.id.btnstart);
		flash = (CheckBox) findViewById(R.id.checkBoxFlash);
		imgSwitchCamera = (ImageView) findViewById(R.id.imgSwitchCamera);
		text_watch_count = (TextView) findViewById(R.id.textView_view_count);
		text_love_count = (TextView) findViewById(R.id.textView_love_count);
		viewchat = findViewById(R.id.View_live_chat);
		viewShareFb = (View)findViewById(R.id.viewShareFb);
		btnShareFacebook= (Button) findViewById(R.id.button_share);
		imgLiveNow =(ImageView)findViewById(R.id.imgLiveNow) ;


		txtTimeOut.setVisibility(View.GONE);
		texttime.setVisibility(View.GONE);
		viewtime.setVisibility(View.GONE);
		viewTimeOut.setVisibility(View.GONE);
		viewShareFb.setVisibility(View.GONE);


		flash.setOnClickListener(this);
		btnstop.setOnClickListener(this);
		btnstart.setOnClickListener(this);
		viewExit.setOnClickListener(this);
		imgSwitchCamera.setOnClickListener(this);
		viewShareFb.setOnClickListener(this);
		btnShareFacebook.setOnClickListener(this);


		findViewById(R.id.button_comment_live).setOnClickListener(this);
		findViewById(R.id.btnblack).setOnClickListener(this);

		viewconnect = (View) findViewById(R.id.viewconnect);
		textconnect = (DotsTextView) findViewById(R.id.connectting);
		waitConnect = (TextView) findViewById(R.id.waitConnect);
		list_chat = (ListView) findViewById(R.id.list_view_comment);
		list_chat.setDivider(null	);

		animBlink = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink);
		animBlink2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink);

		btnBubleStart.startAnimation(animBlink);

		waitConnect.setVisibility(View.GONE);
		textconnect.setVisibility(View.GONE);
		progress.setVisibility(View.GONE);
		mChat_list_adpter = new Chat_list_adpter(this,null,null);
		lComments = new ArrayList<>();
		mChat_list_adpter.setComments(lComments);
		list_chat.setAdapter(mChat_list_adpter);

		spinnercat = (Spinner) findViewById(R.id.spinnerCat);
		upDateCategory(DataCacheUtill.getCategoryStream(this));

		//listCategory
		categoryResults();
		//init streaming session
		initRtspClient();

		//validate and force to login if need
		validateToken();

	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {

			finish();
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Send check token and start login if need
	 */
	void validateToken() {
		if (UserUtil.isLoggedIn(this)) {
			String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_user);
			User user = UserUtil.getUser(this);
			new RestServiceUserCommand(this, user, url) {

				protected void onPostExecute(User result) {
					try {
						if (jsonResponse == null || jsonResponse.getInt("status") != 0) {
							AlertDialog dialog = new AlertDialog.Builder(LiveCam.this).create();
							dialog.setTitle("Session expired");
							dialog.setMessage("Please login and try again");
							dialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									startActivity(new Intent(LiveCam.this, LoginsActivity.class));
									finish();
								}
							});
							dialog.setCancelable(false);
							dialog.show();
						}
					} catch (Exception e) {
					}
				}
			}.execute();

		} else {
			startActivity(new Intent(this, LoginsActivity.class));
		}
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.button_share:
			case R.id.viewShareFb:

				Bundle parameters = new Bundle();
				parameters.putString("link",getUrlShare());
				sharefacebook_sentintent(getUrlShare());
				//facebook 4.7 sdk
//				facebook.dialog(this, "feed", parameters, new PostDialogListener());
//						ShareLinkContent shareLinkContent = new ShareLinkContent.Builder()
//								.setContentUrl(Uri.parse(getUrlShare())).build();
//						ShareDialog shareDialog = new ShareDialog(LiveCam.this);
//						shareDialog.show(shareLinkContent, ShareDialog.Mode.FEED);

				break;

			case R.id.btnstart:


				MainApplication.getInstance().trackScreenView("LiveStream_Record");
				MainApplication.getInstance().trackEvent("LiveStream_Record", "LiveStream_Record", "Send event LiveStream_Record");

				if (title.getText().toString().isEmpty()) {
					title.setError(getText(R.string.up_title_live));
				} else {
					textconnect.setVisibility(View.VISIBLE);
					waitConnect.setVisibility(View.VISIBLE);
					textconnect.start();

					viewconnect.setVisibility(View.VISIBLE);
					//	progress.setVisibility(View.VISIBLE);
					viewDialogTitle.setVisibility(View.GONE);
					btnstart.setVisibility(View.GONE);
					flash.setVisibility(View.GONE);
					imgSwitchCamera.setVisibility(View.GONE);
					//		btnstop.setVisibility(View.VISIBLE);

					viewExit.setVisibility(View.GONE);
					btnBubleStart.setVisibility(View.GONE);

					commonstreaming();
				}

				break;

			case R.id.btnstop:

				AlertDialog.Builder alertDialog = new AlertDialog.Builder(LiveCam.this);
				alertDialog.setTitle("You Stop Livestream?");
				alertDialog.setIcon(R.drawable.ic_warning);
				alertDialog.setPositiveButton("YES",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {

								finish();
							}
						});
				// Setting Negative "NO" Button
				alertDialog.setNegativeButton("NO",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();

							}
						});

				alertDialog.show();


				btnstop.setVisibility(View.VISIBLE);



				break;
			case R.id.btnblack:
				finish();
				break;
			case R.id.imgSwitchCamera:
				mSession.switchCamera();

				break;
			case R.id.viewExit:
				finish();
				break;
			case R.id.checkBoxFlash:
				mSession.toggleFlash();
				break;
			case R.id.button_comment_live:
				if (viewchat.getVisibility() == View.INVISIBLE){
					viewchat.setVisibility(View.VISIBLE);
				}else {
					viewchat.setVisibility(View.INVISIBLE);
				}
				break;


		}

	}



	@Override
	public void onBitrateUpdate(long bitrate) {
		//mSession.setVideoQuality(new VideoQuality());
//		Log.d(TAG, "onBitrateUpdate: " + bitrate);
	}

	@Override
	protected void onResume() {
		super.onResume();
		toggleStreaming();

	}

	@Override
	protected void onPause() {
		super.onPause();

		toggleStreaming();
	}

	private void initRtspClient() {
		// Configures the SessionBuilder
		int num = ResourceUtil.getResolution(this)-1;
		int fps = ResourceUtil.getFramerate(this);

		Log.d(TAG, "initRtspClient: resolution = "+resolutionX[num] +"x"+ resolutionY[num]);
		mSession = SessionBuilder.getInstance()
				.setCallback(this)
				.setSurfaceView(mSurfaceView)
				.setPreviewOrientation(0)
				.setContext(getApplicationContext())
				.setAudioEncoder(SessionBuilder.AUDIO_AAC)
				.setAudioQuality(new AudioQuality(16000, 32000))
				.setVideoEncoder(SessionBuilder.VIDEO_H264)
//				.setVideoQuality(new VideoQuality(176,144,30,20000))
//				.setVideoQuality(new VideoQuality(320, 240, 15, 500000 * 2))
//				.setVideoQuality(new VideoQuality(352, 288, 15,500000*2))
				.setVideoQuality(new VideoQuality(resolutionX[num], resolutionY[num],fps, 500000*4))
//				.setVideoQuality(new VideoQuality(640, 480, 20, 500000 * 2))
//    		    .setVideoQuality(new VideoQuality(768, 432, 20, 500000 * 2))
//				.setVideoQuality(new VideoQuality(800, 600, 15, 500000 * 4))
//				.setVideoQuality(new VideoQuality(960, 720, 15,1000000 ))
//				.setVideoQuality(new VideoQuality(720,480, 15,1000000))
				.build();


		// Configures the RTSP client
		mClient = new RtspClient();
		mClient.setSession(mSession);
		mClient.setCallback(this);


		mSurfaceView.setAspectRatioMode(SurfaceView.ASPECT_RATIO_PREVIEW);
		mSurfaceView.setKeepScreenOn(true);

		// We parse the URI written in the Editext
		mSession.startPreview();
		try {
			if (mSession.isPreviewstarted) {
				mSession.stopPreview();
				mSession.startPreview();
			}
				mSession.stopPreview();
			Thread.sleep(500);
			mSession.startPreview();
		} catch (Exception e) {

		}
		mSession.stopPreview();

		getGeoLocation();
//		Log.d("url","userid ?"+m );
	}

	public void setdata(LiveStreamConnection livestream2) {
		livestream = livestream2;

		String ip, port, path;
		Pattern uri = Pattern.compile("rtsp://(.+):(\\d+)/(.+)");
		// Matcher m = uri.matcher(AppConfig.STREAM_URL+iduser);
		   Matcher m = uri.matcher(livestream.getUrlForm());
		//Matcher m = uri.matcher(AppConfig.STREAM_URL);
		//Matcher m = uri.matcher("rtsp://203.151.189.172:1935/live/myStream");

		Log.d("url", "Stream url ?" + livestream.getUrlForm());

		m.find();
		ip = m.group(1);
		port = m.group(2);
		path = m.group(3);
		mClient.setCredentials(AppConfig.PUBLISHER_USERNAME, AppConfig.PUBLISHER_PASSWORD);
		mClient.setServerAddress(ip, Integer.parseInt(port));
		mClient.setStreamPath("/" + path);
		Log.d("url", "userid ?" + livestream.getUrlForm());
		mClient.startStream();
	}

	public String getUrlShare() {
		String urlShare = livestream.getWeburl();
		return urlShare;
	}



	private void toggleStreaming() {
		if (!mClient.isStreaming()) {
			// Start camera preview
			mSession.startPreview();

			// Start video stream
			//	mClient.startStream();


		} else {
			// already streaming, stop streaming
			// stop camera preview
			//mSession.stopPreview();

			// stop streaming
			//mClient.stopStream();
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		if(mSocket != null) {
			mSocket.off("watchedcount:update", mListenerwatched);
			mSocket.off("lovescount:update", mListenerlove);
			mSocket.disconnect();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		activityAlived = false;
		mClient.release();
		mSession.release();
		mSurfaceView.getHolder().removeCallback(this);

	}


	public void onSessionError(int reason, int streamType, Exception e) {
		Log.e(TAG, "onSessionError(" + reason + ")" + (e == null ? "" : e.getMessage()));
		switch (reason) {
			case Session.ERROR_CAMERA_ALREADY_IN_USE:
				break;
			case Session.ERROR_CAMERA_HAS_NO_FLASH:
				break;
			case Session.ERROR_INVALID_SURFACE:
				break;
			case Session.ERROR_STORAGE_NOT_READY:
				break;
			case Session.ERROR_CONFIGURATION_NOT_SUPPORTED:
				break;
			case Session.ERROR_OTHER:
				break;
		}

		if (e != null) {
			alertError(e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void onRtspUpdate(int message, Exception exception) {
		switch (message) {
			case RtspClient.ERROR_CONNECTION_FAILED:


			case RtspClient.ERROR_WRONG_CREDENTIALS:
				alertError(exception.getMessage());
				exception.printStackTrace();

				break;
		}
	}

	private void alertError(final String msg) {
		if (!activityAlived) return;

		//	final String error = ((msg == null) ? "Unknown error: " : (String) getText(R.string.up_not_connectstream));
		final String error = ((msg == null) ? "Unknown error: " : msg);
		AlertDialog.Builder builder = new AlertDialog.Builder(LiveCam.this);
		builder.setMessage(error).setPositiveButton(getText(R.string.up_Ok),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						finish();
					}
				});
		AlertDialog dialog = builder.create();
		dialog.show();
	}


	public void onPreviewStarted() {
		VideoQuality vq = mClient.getSession().getVideoTrack().getVideoQuality();
//        Toast.makeText(this, (vq.resX + "x" + vq.resY + " bit rate " + (vq.bitrate/1000) + "Kbps")
//        		, Toast.LENGTH_SHORT).show();
	}


	public void onSessionConfigured() {

	}


	public void onSessionStarted() {

		imgLiveNow.startAnimation(animBlink2);

		livestream.setTimeStreaming(5);
		int timeLimit = livestream.getTimeStreaming();
		txtTitlelive.setText(livestream.getTitle());
		viewTitlelive.setVisibility(View.VISIBLE);
		viewShareFb.setVisibility(View.VISIBLE);


		texttime.setVisibility(View.GONE);
		viewtime.setVisibility(View.GONE);
		textconnect.setVisibility(View.GONE);
		waitConnect.setVisibility(View.GONE);
		//viewheader.setVisibility(View.VISIBLE);
		viewfooter.setVisibility(View.VISIBLE);

		viewconnect.setVisibility(View.GONE);
		progress.setVisibility(View.GONE);
		btnstop.setVisibility(View.VISIBLE);
		int time = ResourceUtil.getLivetime(this);
		if (time == 0){
			live_unlimited = true;
		}else {
			live_unlimited = false;
		}
		if (live_unlimited) {
			timer();
		} else {
			onCountdonw(timeLimit);
			onCountdonwAlert(timeLimit);
		}

		findViewById(R.id.View_Live_Time).setVisibility(View.VISIBLE);

		//Toast.makeText(this, R.string.up_toast_connect, Toast.LENGTH_SHORT).show();
		MainApplication.getInstance().trackScreenView("Live_Success");
		MainApplication.getInstance().trackEvent("Live_Success", "Live_Success", "Send event Live_Fail");

		Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
		mSocket = MainApplication.getmSocket();
		if (mSocket.connected()){
			mSocket.emit(MainApplication.mJoinroom,"streamlive/" + livestream.getStreamId(), new Ack() {
				@Override
				public void call(final Object... args) {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Log.d(LiveCam.class.getSimpleName(), "callsocket: " + args[0].toString());
							mSocket.on("watchedcount:update", mListenerwatched);
							mSocket.on("lovescount:update", mListenerlove);
							mSocket.on("comment:new",mListenercommentnew);
							mSocket.emit("check", "/websocket#" + mSocket.id(), new Ack() {
								@Override
								public void call(Object... args) {
									Log.d(TAG, "callsocket: "+args[0].toString());
								}
							});
						}
					});

				}
			});

		}else {
			mSocket.connect();
			mSocket.on("ack-connected", onAckConnected);
		}
	}

	public void onSessionStopped() {
		Log.e(TAG, "onSessionStopped " + new Date());

		Toast.makeText(this, R.string.up_toast_disconnect, Toast.LENGTH_LONG).show();
	}


	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {

	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {

	}


	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {

	}


	public void onBitrareUpdate(long bitrate) {
		Log.d(TAG, "Bitrate: " + bitrate);
	}

	private void onCountdonw(int timeLimit) {
		final String FORMAT = "%02d:%02d:%02d";

		new CountDownTimer(timeLimit * 60000, 1000) {
			public void onTick(long millisUntilFinished) {

				textTimeLive.setText("" + String.format(FORMAT, TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
						TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
								TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
						TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
								TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

			}

			public void onFinish() {
				viewDialogTitle.setVisibility(View.VISIBLE);
				btnstart.setVisibility(View.VISIBLE);
				btnstop.setVisibility(View.GONE);
				btnBubleStart.setVisibility(View.GONE);
				viewTitlelive.setVisibility(View.INVISIBLE);
				viewShareFb.setVisibility(View.GONE);

				viewExit.setVisibility(View.VISIBLE);
				mClient.stopStream();
				onSessionStopped();

			}
		}.start();

	}

	private void onCountdonwAlert(int timeLimit) {
		new CountDownTimer((timeLimit * 60000) - 6000, 1000) {
			public void onTick(long millisUntilFinished) {

			}

			public void onFinish() {
				alertTimeOut();

			}
		}.start();
	}

	private void alertTimeOut() {
		final String alertTitle = "Finish Livestream?";
		new CountDownTimer(6000, 1000) {
			public void onTick(long millisUntilFinished) {
				txtTimeOut.setVisibility(View.VISIBLE);
				viewTimeOut.setVisibility(View.VISIBLE);
				txtTimeOut.setText("" + millisUntilFinished / 1000);
				txtTimeOut.startAnimation(animBlink);

			}

			public void onFinish() {
				txtTimeOut.setVisibility(View.GONE);
				viewTimeOut.setVisibility(View.GONE);
				stopstream(alertTitle);
			}
		}.start();

	}


	private void timer() {

		int locSec = 0;
		int locMin = 0;
		int locHr = 0;
		final DecimalFormat format = new DecimalFormat("00");
		String formatSecond = format.format(locSec);
		String formatMinute = format.format(locMin);
		String formatHour = format.format(locHr);

		sec = Integer.parseInt(formatSecond);
		min = Integer.parseInt(formatMinute);
		hr = Integer.parseInt(formatHour);
		T = new Timer();

		T.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						// 	textTimeLive.setVisibility(View.VISIBLE);
						//	viewtime.setVisibility(View.VISIBLE);
						textTimeLive.setText("" + format.format(Double.valueOf(hr)) + ":" + format.format(Double.valueOf(min)) + ":" + format.format(Double.valueOf(sec)));
						sec++;
						if (sec > 59) {
							sec = 0;
							min = min + 1;
						}

					}
				});
			}
		}, 1000, 1000);

	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		final String alertTitle = "You Close Livestream?";
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			stopstream(alertTitle);
			return true;
		}


		return super.onKeyDown(keyCode, event);
	}

	public void stopstream(String title) {
		if (!activityAlived) return;

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(LiveCam.this);
		alertDialog.setTitle(title);
		// Setting Icon to Dialog
		alertDialog.setIcon(R.drawable.icon_vdo);
		// Setting Positive "Yes" Button
		alertDialog.setPositiveButton("YES",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				});
		// Setting Negative "NO" Button
		alertDialog.setNegativeButton("NO",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();

					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	// get Location
	private void getGeoLocation() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String currentDateandTime = sdf.format(new Date());
		try {
			geocoder = new Geocoder(this, Locale.ENGLISH);
			List<Address> addresses = geocoder.getFromLocation(livestream.getLatitude(), livestream.getLongitude(), 1);

			if (addresses != null && !addresses.isEmpty()) {
				Address fetchedAddress = addresses.get(0);
				StringBuilder strAddress = new StringBuilder();
				strAddress.append(fetchedAddress.getAddressLine(1));
				textLocation.setText(strAddress.toString() + "," + fetchedAddress.getAddressLine(2));
				textView_province.setText(fetchedAddress.getAdminArea());
				title.setText(fetchedAddress.getAdminArea()+" "+fetchedAddress.getAddressLine(1));
			} else {
				textLocation.setText("No location found..!");
				textView_province.setText("No location found..!");
				try {
					title.setText(livestream.getUser().getFirstName() + " " + livestream.getUser().getLastName()
							+ "_" + currentDateandTime);

				} catch (Exception e) {
					Log.d("Error", "Null");
				}
			}



		} catch (IOException e) {
			e.printStackTrace();
			Toast.makeText(getApplicationContext(), "Could not get address..!", Toast.LENGTH_LONG).show();
		}

	}


	LiveStreamConnection PutStreamConnection() {
		LiveStreamConnection stremconnect = new LiveStreamConnection();

		stremconnect.setTitle(title.getText().toString());
		stremconnect.setLatitude(livestream.getLatitude());
		stremconnect.setLongitude(livestream.getLongitude());
		stremconnect.setIdCategory(arrCategoryLive.get(spinnercat.getSelectedItemPosition()).getId());


		return stremconnect;
	}

	public void commonstreaming() {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentDateandTime = sdf.format(new Date());

		RequestLivestreamCommand getCommand = new RequestLivestreamCommand(getApplicationContext());

		getCommand.setLivestream(PutStreamConnection());
		getCommand.setUser(UserUtil.getUser(getApplicationContext()));
		getCommand.setProtocol(LiveStreamConnection.SET_PROTOCOL);


		getCommand.setConnectingTimeout(currentDateandTime);


		new GetStreamServiceTask(this).execute(getCommand);

	}


	class GetStreamServiceTask extends CommonServiceTask {
		String message;

		public GetStreamServiceTask(FragmentActivity context) {
			super(context);
		}

		@Override
		protected ServiceResponse doInBackground(Command... params) {
			ServiceConnector srConnector = new ServiceConnector(ResourceUtil.getServiceUrl(context) + getString(R.string.app_service_liveStreamRequest));
			ServiceResponse serviceResponse = srConnector.doAsynPost(params[0], false);

			try {
				int code = serviceResponse.getCode();
				if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
					JSONObject json = new JSONObject(serviceResponse.getContent().toString());
					livestream = new LiveStreamConnection(json);


				}
			} catch (Exception e) {
				MainApplication.getInstance().trackException(e);
				Log.e("TIT", "doInBackground error", e);
			}

			return serviceResponse;
		}

		@Override
		protected void onPostExecute(ServiceResponse result) {
			super.onPostExecute(result);

			try {
				switch (result.getCode()) {
					case ServiceResponse.SUCCESS:
					case HttpStatus.SC_OK:
						context.setResult(Activity.RESULT_OK);
						LiveCam live = new LiveCam();
						live.setdata(livestream);
						break;
					case 2://restore token failed
						//TODO logout

						break;
					default:
						message = result.getMessage();
						String ms = message + "\r\n" + "Please retry start livestream again.";
						dialogFail(ms);
				}
			} catch (Exception e) {
				MainApplication.getInstance().trackException(e);

				Log.e("TIT", "process response error", e);

				MainApplication.getInstance().trackScreenView("Live_Fail");
				MainApplication.getInstance().trackEvent("Live_Fail", "Live_Fail", "Send event Live_Fail");

				message = e.getMessage();
				dialogFail(message);
			}
		}

	}

	public void dialogFail(String message) {
		if (!activityAlived) return;

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(LiveCam.this);
		alertDialog.setTitle(message);
		// Setting Icon to Dialog
		alertDialog.setIcon(R.drawable.ic_warning);
		// Setting Positive "Yes" Button
		alertDialog.setPositiveButton("YES",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				});

		alertDialog.show();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		Camera.Parameters params = null;
		try {
			params = VideoStream.mCamera.getParameters();
		}catch (Exception e){
			e.getMessage();
		}

		if (params != null) {
			if (event != null) {
				int action = event.getAction();
				if (event.getPointerCount() > 1) {
					// handle multi-touch events
					if (action == MotionEvent.ACTION_POINTER_DOWN) {
						mDist = getFingerSpacing(event);
					} else if (action == MotionEvent.ACTION_MOVE && params.isZoomSupported()) {
						VideoStream.mCamera.cancelAutoFocus();
						handleZoom(event, params);
					}
				} else {
					// handle single touch events
					if (action == MotionEvent.ACTION_UP) {
						handleFocus(event, params);
					}
				}
			}
		}
		return true;
	}

	private void handleZoom(MotionEvent event, Camera.Parameters params) {
		int maxZoom = params.getMaxZoom();
		int zoom = params.getZoom();
		float newDist = getFingerSpacing(event);
		if (newDist > mDist) {
			//zoom in
			if (zoom < maxZoom)
				zoom++;
		} else if (newDist < mDist) {
			//zoom out
			if (zoom > 0)
				zoom--;
		}
		mDist = newDist;
		params.setZoom(zoom);
		VideoStream.mCamera.setParameters(params);
	}

	public void handleFocus(MotionEvent event, Camera.Parameters params) {
		int pointerId = event.getPointerId(0);
		int pointerIndex = event.findPointerIndex(pointerId);
		// Get the pointer's current position
		float x = event.getX(pointerIndex);
		float y = event.getY(pointerIndex);

		List<String> supportedFocusModes = params.getSupportedFocusModes();
		if (supportedFocusModes != null && supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
			VideoStream.mCamera.autoFocus(new Camera.AutoFocusCallback() {
				@Override
				public void onAutoFocus(boolean b, Camera camera) {
					// currently set to auto-focus on single touch
				}
			});
		}
	}

	private float getFingerSpacing(MotionEvent event) {
		// ...
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return  (float) Math.ceil(x * x + y * y);
	//	FloatMath.sqrt(x * x + y * y);
	}



	// ListCategory Stream
	private void categoryResults() {

		String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_category_stream);
		new RestServiceCategoryListCommand(this, url) {
			protected void onPostExecute(List<CategoryLiveStream> result) {

				arrCategoryLive = result;
				if(arrCategoryLive.size() != 0){
					//DataCacheUtill.setCategoryStream(getApplicationContext(), arrCategoryLive);

					upDateCategory(arrCategoryLive);
				}

			}

		}.execute();

	}

	private void upDateCategory(List<CategoryLiveStream> arrCategoryLive){

		final String[] category = new String[arrCategoryLive.size()];
		int i = 0;
		for (CategoryLiveStream pv : arrCategoryLive) {
			category[i++] = pv.getCategoryName();
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, category);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnercat.setAdapter(adapter);
	}

	private Emitter.Listener mListenerlove = new Emitter.Listener() {
		@Override
		public void call(final Object... args) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Datasocket mDatasocket = new Datasocket(Datasocket.Socket_type.lovescount,args[0].toString());
							Datasocket.Vid_property mVid_property = (Datasocket.Vid_property)
									mDatasocket.getdata(Datasocket.Socket_type.lovescount);
							text_love_count.setText(mVid_property.getLove()+"");
							///text_watch_count.setText(mVid_property.getWatched()+"");

						}
					});
				}
			});
		}
	};
	private Emitter.Listener mListenerwatched = new Emitter.Listener() {
		@Override
		public void call(final Object... args) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Datasocket mDatasocket = new Datasocket(Datasocket.Socket_type.watchscount,args[0].toString());
							Datasocket.Vid_property mVid_property = (Datasocket.Vid_property)
									mDatasocket.getdata(Datasocket.Socket_type.watchscount);
							//text_love_count.setText(mVid_property.getLove()+"");
							text_watch_count.setText(mVid_property.getWatched()+"");

						}
					});
				}
			});
		}
	};

	private Emitter.Listener mListenercommentnew = new Emitter.Listener() {
		@Override
		public void call(final Object... args) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Datasocket mDatasocket = new Datasocket(Datasocket.Socket_type.comment,args[0].toString());
					Datasocket.Vid_Comment comment = (Datasocket.Vid_Comment) mDatasocket.getdata(Datasocket.Socket_type.comment);
					mChat_list_adpter.addComments(comment.getContent());
					scrollMyListViewToBottom();

				}
			});
		}
	};

	private Emitter.Listener onAckConnected = new Emitter.Listener() {
		@Override
		public void call(Object... args) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					mSocket.emit(MainApplication.mJoinroom, "streamlive/" + livestream.getId(), new Ack() {
						@Override
						public void call(final Object... args) {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									Log.d(LiveCam.class.getSimpleName(), "callsocket: " + args[0].toString());
								}
							});

						}
					});
					mSocket.on("watchedcount:update", mListenerwatched);
					mSocket.on("lovescount:update", mListenerlove);
					mSocket.on("comment:new",mListenercommentnew);
					mSocket.emit("check", "/websocket#" + mSocket.id(), new Ack() {
						@Override
						public void call(Object... args) {
							Log.d(TAG, "callsocket: " + args[0].toString());
						}
					});

				}
			});
		}
	};



	private void scrollMyListViewToBottom() {
		list_chat.post(new Runnable() {
			@Override
			public void run() {
				// Select the last row so it will scroll into view...
				list_chat.setSelection(mChat_list_adpter.getCount() - 1);
			}
		});
	}
	private void sharefacebook_sentintent(String uri){
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		shareIntent.setType("text/plain");
		shareIntent.putExtra(Intent.EXTRA_TEXT, uri);

		PackageManager pm = getPackageManager();
		List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
		for (final ResolveInfo app : activityList)
		{
			if ((app.activityInfo.name).startsWith("com.facebook.com"))
			{
				final ActivityInfo activity = app.activityInfo;
				shareIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(shareIntent);
				break;
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//Log.d(TAG, "onActivityResult: requestCode="+requestCode +"_resultcode="+resultCode +":"+data.getExtras().toString());
	}

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(LiveCam.this);
        alertDialog.setTitle("You Stop Livestream?");
        alertDialog.setIcon(R.drawable.ic_warning);
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        mClient.stopStream();
                    }
                });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });

        alertDialog.show();


        btnstop.setVisibility(View.VISIBLE);
    }
}

