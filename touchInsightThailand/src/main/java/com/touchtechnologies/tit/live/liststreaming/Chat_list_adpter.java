package com.touchtechnologies.tit.live.liststreaming;

import android.content.Context;
import android.text.Html;
import android.text.StaticLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.opensource.pagerindicator.CircleBitmapDisplayer;
import com.touchtechnologies.dataobject.insightthailand.Comment;
import com.touchtechnologies.tit.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TOUCH on 3/5/2559.
 */

public class Chat_list_adpter extends BaseAdapter{
    private List<Comment> comments;
    private Context mContext;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    public Chat_list_adpter(Context mContext, ImageLoader imageLoader, DisplayImageOptions options) {
        this.mContext = mContext;
        if(imageLoader == null){
            imageLoader = ImageLoader.getInstance();
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this.mContext)
                    .threadPoolSize(3)
                    .threadPriority(Thread.NORM_PRIORITY - 2)
                    .denyCacheImageMultipleSizesInMemory()
                    .memoryCacheSize(4194304)
                    .memoryCache(new WeakMemoryCache())
                    .defaultDisplayImageOptions(options).build();
            imageLoader.init(config);
            options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .imageScaleType(ImageScaleType.NONE)
                    .showImageForEmptyUri(R.drawable.ic_new_imgaccount)
                    .showImageOnFail(R.drawable.ic_new_imgaccount)
                    .showImageOnLoading(R.drawable.bg_loading_circle)
                    .cacheInMemory(true)
                    .displayer(new FadeInBitmapDisplayer(1000))
                    .build();
            this.imageLoader = imageLoader;
            this.options = options;
        }else {
            this.imageLoader = imageLoader;
            this.options = options;
        }
    }

    @Override
    public int getCount() {
        return comments.size();
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
        notifyDataSetChanged();
    }
    public void addComments(Comment comment) {

        if (comments == null) {
            comments = new ArrayList<Comment>();
            comments.add(comment);
        }
            else{
            comments.add(comment);
        }
        notifyDataSetChanged();

    }
    public ArrayList<Comment> getComments(){
        return (ArrayList<Comment>) comments;
    }

    @Override
    public Comment getItem(int position) {
        return comments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHodler viewHodler;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_comment_live, parent, false);
            viewHodler = new ViewHodler();
            viewHodler.imageView = (ImageView) convertView.findViewById(R.id.imageView_picpro);
          //  viewHodler.comment = (TextView) convertView.findViewById(R.id.textView_comment);
            viewHodler.commentator = (TextView)convertView.findViewById(R.id.textView_commentator);
            convertView.setTag(viewHodler);

        }else{
            viewHodler = (ViewHodler)convertView.getTag();
        }
//        String sourceString = "<span style=\"display: inline-block;\"><b>" + getItem(position).getUser().getFirstName()
//                +" "+getItem(position).getUser().getLastName()+":</b></span>"+getItem(position).getCommentContent();
                String sourceString = "<b>" + getItem(position).getUser().getFirstName()
                +"&nbsp;"+getItem(position).getUser().getLastName()+"<span style=\"white-space: nowrap;\">&nbsp;:&nbsp;</b></span>"+getItem(position).getCommentContent();

        viewHodler.commentator.setText(Html.fromHtml(sourceString));
           // viewHodler.comment.setText();
            imageLoader.displayImage(getItem(position).getUser().getAvatar(),viewHodler.imageView,options);

        return convertView;
    }

  class ViewHodler{
        ImageView imageView;
        TextView comment,commentator;

    }
}
