package com.touchtechnologies.tit.live.category;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.touchtechnologies.animate.progress.RotateLoading;
import com.touchtechnologies.command.insightthailand.RestServiceHistoryListCommand;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.history.LiveHistoryGridviewAdapter;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;
import com.touchtechnologies.wifi.WIFIMod;

import java.util.List;

/**
 * Created by TouchICS on 3/25/2016.
 */
public class ListLiveCategoryFragment extends Fragment implements AbsListView.OnScrollListener, SwipeRefreshLayout.OnRefreshListener {
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private View viewLoading, viewEmptyHint,viewnotlive;
    private LiveHistoryGridviewAdapter adapter;
    private ListView listView ;
    private Context context;
    List<StreamHistory> hisstory;
    private RotateLoading rotateLoading;
    protected int action_refresh;
    private boolean loading = false;
    private int previousTotal = 0;
    private int visibleThreshold = 3;
    private String url;
    private  int page;
    private String categoryID;
    public static final String KEY_CATEGORY_TYPE = "type";

    public static ListLiveCategoryFragment newInstance(int type){
        ListLiveCategoryFragment fragment = new ListLiveCategoryFragment();
        Bundle mBundle = new Bundle();
        mBundle.putInt(KEY_CATEGORY_TYPE,type);
        fragment.setArguments(mBundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View listViewContainer = inflater.inflate(R.layout.fragment_category_live, container, false);
        int positionId = getArguments().getInt(KEY_CATEGORY_TYPE);


        MainApplication.getInstance().trackScreenView("LiveStream_History");

        viewEmptyHint = (View)listViewContainer.findViewById(R.id.emptyAgentHint);
        if(!WIFIMod.isNetworkConnected(getActivity())){
            viewEmptyHint.setVisibility(View.VISIBLE);
        }else{
            viewEmptyHint.setVisibility(View.GONE);
        }
        adapter = new LiveHistoryGridviewAdapter(getActivity());


        mSwipeRefreshLayout = (SwipeRefreshLayout) listViewContainer.findViewById(R.id.container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        //	mSwipeRefreshLayout.setVisibility(View.GONE);

        rotateLoading = (RotateLoading) listViewContainer.findViewById(R.id.rotateloading);
        rotateLoading.start();
        rotateLoading.setVisibility(View.VISIBLE);
        viewnotlive =(View) listViewContainer.findViewById(R.id.viewnotlive);
        viewnotlive.setVisibility(View.GONE);

        listView = (ListView)listViewContainer.findViewById(R.id.grid_view);

        listView.setAdapter(adapter);

        listView.setOnScrollListener(this);
        adapter.notifyDataSetChanged();
        //    gridView.setOnItemClickListener(myOnItemClickListener);
        onRefresh();

        return listViewContainer;
    }


    public void onRefresh(int action){
        action_refresh = action;
        User user = UserUtil.getUser(getActivity());
        try {
            categoryID = getArguments().getString("categoryID");
            //  Toast.makeText(getActivity(),""+categoryID,Toast.LENGTH_SHORT).show();
            url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_rest_histories) + "?filters[stream_media][category_id][operator]==&filters[stream_media][category_id][value]=" + categoryID+"&filterLimit="+10+"&filtersPage="+1;
            Log.i("POST : Url =",""+url);
        }catch (Exception e){
            Log.e("Error",":"+e.getMessage());
        }

        if (!loading) {
            loading = true;
            new RestServiceHistoryListCommand(getActivity(),user, url){

                protected void onPostExecute(java.util.List<StreamHistory> result) {
                    hisstory = result;

                    if (hisstory.size()==0) {
                        viewnotlive.setVisibility(View.VISIBLE);
                    } else {
                        viewnotlive.setVisibility(View.GONE);
                    }

                    adapter.setData(hisstory);
                    //	pager.setCurrentItem(1);
                    adapter.notifyDataSetChanged();
                    rotateLoading.setVisibility(View.GONE);
                    loading = false;
                }
            }.execute();

            page = 1;
        }

    }
 /*
	OnItemClickListener myOnItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			 try {
				  	Intent intent = new Intent(getActivity(), LiveHistoryPlayer.class);
					intent.putExtra(StreamHistory.class.getName(),adapter.getItem(position));
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);

			} catch (ActivityNotFoundException activityException) {
			        Log.e("dialing", "Call failed", activityException);
		    }

		}
	};

*/


    @Override
    public void onRefresh() {
        onRefresh(MotionEvent.ACTION_DOWN);
        mSwipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // TODO Auto-generated method stub

    }

    public void onScroll(AbsListView abs, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        if (totalItemCount > previousTotal) {
            //loading = false;
            previousTotal = totalItemCount;
        }


        if (listView.getAdapter().getCount() >visibleThreshold){
            if (!loading &&	(listView.getLastVisiblePosition() >= listView.getAdapter().getCount() - visibleThreshold)) {
                loadMore();
            }
        }


    }



    public void loadMore(){
        page+=1;

        User user = UserUtil.getUser(getActivity());
     //   url = ResourceUtil.getServiceUrl(getActivity())+getResources().getString(R.string.app_service_rest_histories)+"?filterLimit="+10+"&filtersPage="+page;
      String  url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_rest_histories) + "?filters[stream_media][category_id][operator]==&filters[stream_media][category_id][value]=" + categoryID+"&filterLimit="+10+"&filtersPage="+page;
        Log.i("POST : Url =",""+url);
        if (!loading) {
            loading = true;
            new RestServiceHistoryListCommand(getActivity(),user, url){

                protected void onPostExecute(java.util.List<StreamHistory> result) {
                    hisstory = result;

                    adapter.addData(hisstory);
                    //	pager.setCurrentItem(1);
                    adapter.notifyDataSetChanged();

                    loading = false;
                }
            }.execute();


        }
    }


}

