package com.touchtechnologies.tit.live.liststreaming;

import java.util.List;

import android.app.ActionBar;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ListView;

import com.touchtechnologies.animate.progress.RotateLoading;
import com.touchtechnologies.command.insightthailand.RequestListLiveStreamingCommand;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.cctvitlive.LiveStreamFragment;
import com.touchtechnologies.tit.cctvitlive.LiveStreamFragment2;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;
import com.touchtechnologies.wifi.WIFIMod;

public class LiveStreamingListActivity extends SettingActionbarActivity implements OnScrollListener, OnRefreshListener {
	private SwipeRefreshLayout mSwipeRefreshLayout;
	private View viewLoading, viewEmptyHint,viewnotlive;
	private LiveStreamGridViewAdapter adapter;
	private ListView gridView ;
	private Context context;
	private List<LiveStreamingChannel> live;
	private RotateLoading rotateLoading;
	protected int action_refresh;
	private boolean loading = false;
	private int previousTotal = 0;
	private int visibleThreshold = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_livestream_gridview);

		mTitle ="Live";

		MainApplication.getInstance().trackScreenView("LiveStream_OnAir");

		viewEmptyHint = (View)findViewById(R.id.emptyAgentHint);

		if(!WIFIMod.isNetworkConnected(this)){
			viewEmptyHint.setVisibility(View.VISIBLE);

		}else{
			viewEmptyHint.setVisibility(View.GONE);
		}

		adapter = new LiveStreamGridViewAdapter(this);


		mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.container);
		mSwipeRefreshLayout.setOnRefreshListener(this);
		//	mSwipeRefreshLayout.setVisibility(View.GONE);

		rotateLoading = (RotateLoading)findViewById(R.id.rotateloading);
		rotateLoading.start();
		rotateLoading.setVisibility(View.VISIBLE);


		viewnotlive =(View)findViewById(R.id.viewnotlive);
		viewnotlive.setVisibility(View.GONE);



		gridView = (ListView)findViewById(R.id.grid_view);
		gridView.setAdapter(adapter);
//	    gridView.setOnItemClickListener(myOnItemClickListener);

		onRefresh();

	}

	public void onRefresh(int action){
		action_refresh = action;
		User user = UserUtil.getUser(this);

		if (!loading) {
			String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_liststreaming);

			new RequestListLiveStreamingCommand(this,user, url){

				protected void onPostExecute(java.util.List<LiveStreamingChannel> result) {
					live = result;

					if (live == null || live.size()==0) {
						viewnotlive.setVisibility(View.VISIBLE);
					} else {
						viewnotlive.setVisibility(View.GONE);
					}

					adapter.setData(live);
					//	pager.setCurrentItem(1);
					adapter.notifyDataSetChanged();
					rotateLoading.setVisibility(View.GONE);
				};
			}.execute();

		}
	}



	@Override
	public void onRefresh() {
		onRefresh(MotionEvent.ACTION_DOWN);
		mSwipeRefreshLayout.setRefreshing(false);

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}

	public void onScroll(AbsListView abs, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

		if (loading) {
			if (totalItemCount > previousTotal) {
				loading = false;
				previousTotal = totalItemCount;
			}
		}
		if (!loading & totalItemCount > 0 && (totalItemCount + visibleItemCount) <= (firstVisibleItem - visibleThreshold)) {
			// load the next feeds using a background task,
			onRefresh();

			loading = true;
		}


	}

	@Override
	protected void onStart() {
		super.onStart();
		onRefresh();
	}
}

