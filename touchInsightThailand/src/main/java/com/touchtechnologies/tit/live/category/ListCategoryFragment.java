package com.touchtechnologies.tit.live.category;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.touchtechnologies.animate.gridview.FloatingAction;
import com.touchtechnologies.animate.progress.RotateLoading;
import com.touchtechnologies.command.insightthailand.RestServiceCategoryListCommand;
import com.touchtechnologies.dataobject.insightthailand.CategoryLiveStream;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.cctvitlive.LiveStreamFragment2;
import com.touchtechnologies.tit.cctvitlive.LiveStreamListener;
import com.touchtechnologies.tit.util.DataCacheUtill;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;
import com.touchtechnologies.wifi.WIFIMod;

import java.util.List;

/**
 * Created by TouchICS on 3/25/2016.
 */
public class ListCategoryFragment extends ScrollTabHolderFragment implements AbsListView.OnScrollListener, SwipeRefreshLayout.OnRefreshListener,AdapterView.OnItemClickListener, LiveStreamListener {
    private static final String ARG_POSITION = "position";
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private View viewEmptyHint;
    private ListCategoryAdapter adapter;
    private ListView listView ;
    private Context context;
    List<CategoryLiveStream> category;
    private RotateLoading rotateLoading;
    protected int action_refresh;
    private boolean loading = false;
    private int previousTotal = 0;
    private int visibleThreshold = 2;
    private User user;
    private FloatingAction mFloatingAction;
    private View placeHolderView;
    private int mPosition;
    private int liveSize;
    public static Fragment newInstance(int position) {
        ListCategoryFragment fragment = new ListCategoryFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        fragment.setArguments(b);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View listViewContainer = inflater.inflate(R.layout.fragment_category, container, false);
        mPosition = getArguments().getInt(ARG_POSITION);
        liveSize = getArguments().getInt("liveSize", 0);
     //   Toast.makeText(getActivity(), "size " + liveSize, Toast.LENGTH_LONG).show();

        user = UserUtil.getUser(getActivity());
        MainApplication.getInstance().trackScreenView("LiveStream_Categories");

        viewEmptyHint = (View)listViewContainer.findViewById(R.id.emptyAgentHint);

        if(!WIFIMod.isNetworkConnected(getActivity())){
            viewEmptyHint.setVisibility(View.VISIBLE);
        }else{
            viewEmptyHint.setVisibility(View.GONE);
        }
        adapter = new ListCategoryAdapter(getActivity());

        mSwipeRefreshLayout = (SwipeRefreshLayout) listViewContainer.findViewById(R.id.container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        //	mSwipeRefreshLayout.setVisibility(View.GONE);

        rotateLoading = (RotateLoading) listViewContainer.findViewById(R.id.rotateloading);
        rotateLoading.start();
        rotateLoading.setVisibility(View.VISIBLE);


        listView = (ListView)listViewContainer.findViewById(R.id.grid_view);

        // setheader
        placeHolderView = (View)inflater.inflate(R.layout.frame_layout, listView, false);
        placeHolderView.setBackgroundColor(0xFFFFFFFF);
        listView.addHeaderView(placeHolderView);

      //  placeHolderView.setPadding(0, getResources().getDimensionPixelSize(liveSize == 0 ? R.dimen.header_height_frame_null : R.dimen.header_height_frame), 0, 0);
        placeHolderView.setPadding(0, getResources().getDimensionPixelSize(R.dimen.header_height_frame_null), 0, 0);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        listView.setOnScrollListener(new OnScroll());
        if(LiveStreamFragment2.NEEDS_PROXY){//in my moto phone(android 2.1),setOnScrollListener do not work well
            listView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (mScrollTabHolder != null)
                        mScrollTabHolder.onScroll(listView, 0, 0, 0, mPosition);
                    return false;
                }
            });
        }


        adapter.notifyDataSetChanged();
        onRefresh();

        return listViewContainer;
    }


    @Override
    public void ready(List<LiveStreamingChannel> list) {
        // setheader
       // placeHolderView.setPadding(0,getResources().getDimensionPixelSize(list.size()==0?R.dimen.header_height_frame_null:R.dimen.header_height_frame), 0, 0);
    }

    public ListView getGridView() {
        return listView;
    }

    public void onRefresh(int action){
        Log.d(AppConfig.LOG, "refreshing my stream...");
        action_refresh = action;

        if (!loading) {

            List<CategoryLiveStream> mCategory_videos = DataCacheUtill.getCategoryStream(getActivity());
            if(mCategory_videos ==null){
                String url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_rest_category_stream);
                Log.i(AppConfig.LOG, "URL :"+url);
                new RestServiceCategoryListCommand(getActivity(), url) {
                    protected void onPostExecute(List<CategoryLiveStream> result) {
                        category = result;
                        adapter.setData(category);
                        //	pager.setCurrentItem(1);
                        adapter.notifyDataSetChanged();
                        rotateLoading.setVisibility(View.GONE);
                    }

                    ;
                }.execute();
            }else{
                for (int i = 0; i < mCategory_videos.size(); i++) {
                    category = mCategory_videos;
                    adapter.setData(category);
                    adapter.notifyDataSetChanged();
                    rotateLoading.setVisibility(View.GONE);
                }
            }



        }

    }


    @Override
    public void onRefresh() {
        onRefresh(MotionEvent.ACTION_DOWN);
        mSwipeRefreshLayout.setRefreshing(false);

    }


    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // TODO Auto-generated method stub

    }

    public void onScroll(AbsListView abs, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }
        if (!loading & totalItemCount > 0 && (totalItemCount + visibleItemCount) <= (firstVisibleItem - visibleThreshold)) {
            // load the next feeds using a background task,
            onRefresh();

            loading = true;
        }


    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        //    Toast.makeText(getActivity(),"position"+position,Toast.LENGTH_LONG).show();
            Intent  intent = new Intent(getActivity(), PagerCategoryActivity.class);
            intent.putExtra("position",position-1);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

    }


    @Override
    public void adjustScroll(int scrollHeight) {
        if (scrollHeight == 0 && listView.getFirstVisiblePosition() >= 1) {
            return;
        }

        listView.setSelectionFromTop(1, scrollHeight);
    }
    public class OnScroll implements AbsListView.OnScrollListener {

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {
            if (mScrollTabHolder != null) {
                mScrollTabHolder.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount, mPosition);

            }
        }

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount, int pagePosition) {

    }
}

