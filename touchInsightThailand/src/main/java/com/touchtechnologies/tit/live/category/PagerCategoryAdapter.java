package com.touchtechnologies.tit.live.category;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.touchtechnologies.dataobject.insightthailand.CategoryLiveStream;
import com.touchtechnologies.tit.history.LiveHistoryGridviewFragment;
import com.touchtechnologies.tit.live.mylivestream.FragmentMyliveStream;
import com.touchtechnologies.tit.util.DataCacheUtill;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TouchICS on 3/25/2016.
 */
public class PagerCategoryAdapter  extends FragmentPagerAdapter {

    int PAGE_COUNT ;

    // Tab Titles
    private ArrayList<String> tabtitles = new ArrayList<>();
    private ArrayList<String> categoryIDs = new ArrayList<>();
    private Context context;
    public PagerCategoryAdapter(Context context, FragmentManager fm) {
        super(fm);

        List<CategoryLiveStream> mCategory_videos = DataCacheUtill.getCategoryStream(context);
        for(int i = 0; i<mCategory_videos.size();i++){
            String name = mCategory_videos.get(i).getCategoryName();
            String categoryID = ""+mCategory_videos.get(i).getId();

            if(name!=null){
                tabtitles.add(name);
            }
            if(categoryID!= null){
                categoryIDs.add(categoryID);
            }


        }

        PAGE_COUNT = tabtitles.size();
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = ListLiveCategoryFragment.newInstance(position);

        fragment.getArguments().putString("categoryID", categoryIDs.get(position));

        return fragment;

    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabtitles.get(position);
    }
}
