package com.touchtechnologies.tit.live.mylivestream;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.opensource.pagerindicator.CircleBitmapDisplayer;
import com.touchtechnologies.animate.gridview.GridViewWithHeaderAndFooter;
import com.touchtechnologies.animate.progress.RotateLoading;
import com.touchtechnologies.command.insightthailand.RestServiceMyLiveHistoryListCommand;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.setting.MyProfileActivity;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;


public class MyliveGridviewActivity extends SettingActionbarActivity implements OnClickListener,AbsListView.OnScrollListener, SwipeRefreshLayout.OnRefreshListener {
	private RotateLoading rotateLoading;
	private MyliveGridviewAdapter adapter;
	private GridView  gridView ;
	private List<StreamHistory> hisstory;
	private User user;
	private View viewnolive,viewProfile;
	private ImageLoader imageLoader;
	private boolean loading = false;
	private int previousTotal = 0;
	private int visibleThreshold = 2;
	protected int action_refresh;
	private SwipeRefreshLayout mSwipeRefreshLayout;
	private DisplayImageOptions options;
	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
	    	super.onCreate(savedInstanceState);
	    	setContentView(R.layout.activity_gridview_mylive);

		    MainApplication.getInstance().trackScreenView("LiveStream_MyLive");

	    	imageLoader = ImageLoader.getInstance();
			ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
		        .threadPoolSize(3)
				.threadPriority(Thread.NORM_PRIORITY - 2)
				.denyCacheImageMultipleSizesInMemory()
	            .memoryCacheSize(4194304)
	            .defaultDisplayImageOptions(options).build();
			
			imageLoader.init(config);
			options = new DisplayImageOptions.Builder().cacheInMemory(true)
					.imageScaleType(ImageScaleType.NONE)
					.showImageForEmptyUri(R.drawable.bg_cctvdefault)
					.showImageOnFail(R.drawable.ic_new_imgaccount)
					.cacheInMemory(true)
					.displayer(new FadeInBitmapDisplayer(100)) 
				//	.displayer(new RoundedBitmapDisplayer((int) 125.5f))
				    .displayer(new CircleBitmapDisplayer())
					.build();
		
	    	mTitle = "My Live Streaming";
	    	user = UserUtil.getUser(this);
	    	adapter = new MyliveGridviewAdapter(this);
			mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.container);
			mSwipeRefreshLayout.setOnRefreshListener(this);
			
			rotateLoading = (RotateLoading) findViewById(R.id.rotateloading);
	        rotateLoading.start();
	        rotateLoading.setVisibility(View.VISIBLE);
			
//	        gridView = (GridView) findViewById(R.id.gridViewmylive);
	        GridViewWithHeaderAndFooter gridView = (GridViewWithHeaderAndFooter) findViewById(R.id.gridViewmylive);

	        LayoutInflater layoutInflater = LayoutInflater.from(this);
	        View headerView = layoutInflater.inflate(R.layout.item_profile_header, null);
	       
	        viewProfile = (View)headerView.findViewById(R.id.viewProfile);
	        
	        
	        TextView name = (TextView)headerView.findViewById(R.id.txttitle);
	        ImageView imgprofile =(ImageView)headerView.findViewById(R.id.imageViewmylive);
	        
	        viewnolive = (View)findViewById(R.id.view_notlive);
	        viewnolive.setVisibility(View.GONE);
	        name.setText(user.getFirstName()+"  "+user.getLastName());
	    	imageLoader.displayImage(user.getAvatar(), imgprofile, options);
	    	
	    	
	    	viewProfile.setOnClickListener(this);
	    	
	        gridView.addHeaderView(headerView);

	        gridView.setAdapter(adapter);

			onRefresh();
	    	
	    }
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.viewProfile:
				 Intent intent = new Intent(this,MyProfileActivity.class);
				 intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				 startActivity(intent);
				break;

			
			}
			
		}
	public void onRefresh(int action){
		Log.d(AppConfig.LOG, "refreshing my stream...");
		action_refresh = action;
	    if (!loading) {
			String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_mylive_histories);

			new RestServiceMyLiveHistoryListCommand(this,user, url){

				protected void onPostExecute(java.util.List<StreamHistory> result) {
					hisstory = result;
					if (hisstory.size()==0) {
						viewnolive.setVisibility(View.VISIBLE);
					} else {
						viewnolive.setVisibility(View.GONE);
					}

					adapter.setData(hisstory);
					adapter.notifyDataSetChanged();
					rotateLoading.setVisibility(View.GONE);
				}
			}.execute();

		}
	}



	@Override
	public void onRefresh() {
		onRefresh(MotionEvent.ACTION_DOWN);
		mSwipeRefreshLayout.setRefreshing(false);
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

		if (loading) {
			if (totalItemCount > previousTotal) {
				loading = false;
				previousTotal = totalItemCount;
			}
		}
		if (!loading & totalItemCount > 0 && (totalItemCount + visibleItemCount) <= (firstVisibleItem - visibleThreshold)) {
			// load the next feeds using a background task,
			onRefresh();

			loading = true;
		}
	}

	@Override
	protected void onStart() {
		super.onStart();

		onRefresh();
	}
}
	
        