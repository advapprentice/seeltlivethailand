package com.touchtechnologies.tit.live.liststreaming;

import java.util.List;

import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.touchtechnologies.command.insightthailand.RequestListLiveStreamingCommand;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

public class LiveStreamingPager extends Fragment  {
	private boolean loading = false;
	private ViewPager pager;
	private final Handler handler = new Handler();
	private View viewnotlive;
	private ProgressBar pgloading;
	LiveStreamingPagerAdapter adapter;
	List<LiveStreamingChannel> live;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
			View listViewContainer = inflater.inflate(R.layout.fragment_list_pager_livestream, container, false);
			
			pager = (ViewPager) listViewContainer.findViewById(R.id.viewpager);
			adapter = new LiveStreamingPagerAdapter(getActivity());
		//	pager.setPageTransformer(true,new FullImagePagertransformer2());
			
			viewnotlive =(View) listViewContainer.findViewById(R.id.viewnotlive);
			viewnotlive.setVisibility(View.GONE);
			
			pgloading = (ProgressBar)listViewContainer.findViewById(R.id.progressBar1);
			pgloading.setVisibility(View.VISIBLE);
			
			pager.setClipToPadding(false);
			pager.setPageMargin(2);
			pager.setCurrentItem(0);
			pager.setAdapter(adapter);

			User user = UserUtil.getUser(getActivity());
			
			Point point = new Point();
		  	getActivity().getWindowManager().getDefaultDisplay().getSize(point);
		  	pager.getLayoutParams().height = (int)((point.x * 2)/4);
		 
		  	String url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_liststreaming);
		
			new RequestListLiveStreamingCommand(getActivity(),user, url){
		
				protected void onPostExecute(java.util.List<LiveStreamingChannel> result) {
					live = result;
					
					if (live.size()==0) {
						viewnotlive.setVisibility(View.VISIBLE);
						pager.setCurrentItem(1,true);
					} else {
						viewnotlive.setVisibility(View.GONE);
					}

					adapter.setData(live);
				//	pager.setCurrentItem(1);
					adapter.notifyDataSetChanged();
					pgloading.setVisibility(View.GONE);
				};
			}.execute();
	
			
		
			return listViewContainer;
		
	}  


}
