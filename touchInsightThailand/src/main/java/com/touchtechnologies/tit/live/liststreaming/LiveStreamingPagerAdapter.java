 package com.touchtechnologies.tit.live.liststreaming;

import java.util.ArrayList;
import java.util.List;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.opensource.pagerindicator.CircleBitmapDisplayer;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.tit.R;



public class LiveStreamingPagerAdapter extends PagerAdapter implements OnClickListener{
	private Context context;
	
	private ImageLoader imageLoader;
	private DisplayImageOptions options,optionsProfile;
	private ViewPager mPager;
	List<LiveStreamingChannel> arrlive;
	
	public LiveStreamingPagerAdapter(Context context) {
		this.context = context;


		imageLoader = ImageLoader.getInstance();

		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
				.showImageForEmptyUri(R.drawable.bg_cctvdefault)
				.showImageOnFail(R.drawable.bg_cctvdefault)
				.showImageOnLoading(R.drawable.bg_cctvdefault)
				.cacheInMemory(true)
				.handler(new Handler())
						//	.displayer(new FadeInBitmapDisplayer(500)) //fade in images
						//	.resetViewBeforeLoading(true)
				.build();
		optionsProfile = new DisplayImageOptions.Builder().cacheInMemory(true)
				.showImageForEmptyUri(R.drawable.ic_new_imgaccount)
				.showImageOnFail(R.drawable.ic_new_imgaccount)
				.cacheOnDisc(true)
				.cacheInMemory(true)
				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.displayer(new FadeInBitmapDisplayer(400))
				.displayer(new CircleBitmapDisplayer())
				.build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)

				.denyCacheImageMultipleSizesInMemory()
				.threadPoolSize(5)
				.threadPriority(Thread.NORM_PRIORITY - 2) // default
				.tasksProcessingOrder(QueueProcessingType.FIFO) // default
				.denyCacheImageMultipleSizesInMemory()
				.imageDownloader(new BaseImageDownloader(context)) // default
				.defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
				.writeDebugLogs()
				.build();

		// Initialize ImageLoader with configuration.


		imageLoader.init(config);
		imageLoader = ImageLoader.getInstance();
	}
	public void setData(List<LiveStreamingChannel> arrlive){
		this.arrlive = arrlive;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return arrlive == null ? 0 : arrlive.size();
	}

  /*
    @Override
    public float getPageWidth(int position) {
        return 0.53f;
    }
  */
	@Override
	public boolean isViewFromObject(View view, Object object) {
		return (view == object);		
	}
	
	public LiveStreamingChannel getItem(int position) {
		return arrlive.get(position);
	}


	public long getItemId(int position) {
		return position;
	}
	
    
	@Override
	public Object instantiateItem(ViewGroup collection, int position) {
		
//		imageLoader.displayImage(feed.medias.get(position).getThumbURL(),view, options);
		View view = LayoutInflater.from(context).inflate(R.layout.item_list_streaming_pager, collection, false);
		view.setTag(arrlive.get(position));
		
		TextView title= (TextView)view.findViewById(R.id.texttitlelive);
		ImageView imgviewlive = (ImageView)view.findViewById(R.id.imageViewMedia);
		View viewvideo = (View)view.findViewById(R.id.viewVideo);
		View viewimage= (View)view.findViewById(R.id.Viewimage);
		Button play = (Button)view.findViewById(R.id.btnPlayvideo);
		
		play.setOnClickListener(this); 
		play.setTag(position);
		viewvideo.setOnClickListener(this);
	    imgviewlive.setOnClickListener(this);
	    imgviewlive.setTag(position);
	    
	    
	 //   Toast.makeText(context, "position"+position, Toast.LENGTH_LONG).show();
	    title.setText(getItem(position).getLiveStream().getTitle());

	    imageLoader.displayImage(getItem(position).getLiveStream().getSnapshotMedium(),imgviewlive, options);
	
	//    viewimage.setBackgroundColor(Color.argb(125, position * 50, position * 10, position * 50));
	
	    collection.addView(view,0);
	
		return view;
	}
	

	@Override
	public void destroyItem(View collection, int position, Object view) {
		((ViewPager) collection).removeView((View)view);
		 
	}

	
	public void addData(ArrayList<LiveStreamingChannel> arrlives) {
		
		if (arrlive == null) {
			setData(arrlives);

		} else {
			for (LiveStreamingChannel live : arrlives) {
				if (!arrlive.contains(live)) {
					arrlive.add(live);
				}
			}			
		}
		
		notifyDataSetChanged();
	}
	
	@Override
	public void onClick(View v) {

	   		switch (v.getId()) {
	   		case R.id.btnPlayvideo:
			case R.id.imageViewMedia:
		
			 try {
					  	Intent intent = new Intent(context, LiveOnAirPlayerActivity.class);
						intent.putExtra(LiveStreamingChannel.class.getName(), arrlive.get((Integer)v.getTag()));
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						context.startActivity(intent);
								
				} catch (ActivityNotFoundException activityException) {
				        Log.e("dialing", "Call failed", activityException);
			    }
						 
			       
				break;
			}
	}
}