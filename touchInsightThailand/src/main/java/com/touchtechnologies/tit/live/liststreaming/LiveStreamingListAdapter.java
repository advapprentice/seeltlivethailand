package com.touchtechnologies.tit.live.liststreaming;


import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.opensource.pagerindicator.CircleBitmapDisplayer;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.LoveCommand;
import com.touchtechnologies.command.insightthailand.LoveUndoCommand;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.live.mylivestream.MyliveGridviewActivity;
import com.touchtechnologies.tit.login.LoginsActivity;
import com.touchtechnologies.tit.member.MemberProfileActivity;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class LiveStreamingListAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private Runnable runnable;
	protected ImageLoader imageLoader;
	protected DisplayImageOptions options,optionsProfile;
	private String liveId;
	private List<LiveStreamingChannel> arrlive;
	private LiveStreamingChannel live;
	String url;
	public LiveStreamingListAdapter(Context context) {
		this.context = context;


		imageLoader = ImageLoader.getInstance();

		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.NONE)
				.showImageForEmptyUri(R.drawable.bg_cctvdefault)
				.showImageOnFail(R.drawable.bg_cctvdefault)
				.showImageOnLoading(R.drawable.bg_cctvdefault)
				.displayer(new FadeInBitmapDisplayer(300))
				.resetViewBeforeLoading(false).build();


		optionsProfile = new DisplayImageOptions.Builder().cacheInMemory(true)
				.showImageForEmptyUri(R.drawable.ic_new_imgaccount)
				.showImageOnFail(R.drawable.ic_new_imgaccount)
				.cacheOnDisc(true)
				.cacheInMemory(true)
				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.displayer(new FadeInBitmapDisplayer(300))
				.displayer(new CircleBitmapDisplayer())
				.build();

		imageLoader = ImageLoader.getInstance();


	}
	public void setData(List<LiveStreamingChannel> arrlive){
		this.arrlive = arrlive;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return arrlive == null ? 0 : arrlive.size();
	}
	@Override
	public boolean hasStableIds() {
		return true;
	}


	public LiveStreamingChannel getItem(int position) {
		return arrlive.get(position);
	}


	public long getItemId(int position) {
		return position;
	}


	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.item_live_gridview, parent, false);


			viewHolder = new ViewHolder();


			viewHolder.imageView = (ImageView) convertView.findViewById(R.id.imageViewMedia);
			viewHolder.imgProfile = (ImageView)convertView.findViewById(R.id.imgProfile);
			viewHolder.textView = (TextView) convertView.findViewById(R.id.texttitlelive);
			viewHolder.viewimage = (View) convertView.findViewById(R.id.Viewimage);
			viewHolder.textUser = (TextView) convertView.findViewById(R.id.txtuser);
			viewHolder.textCount = (TextView) convertView.findViewById(R.id.txtCount);
			viewHolder.viewShare = (View)convertView.findViewById(R.id.viewShare);

			viewHolder.viewVideo = (View) convertView.findViewById(R.id.viewVideos);
			viewHolder.imgLove = (ImageView) convertView.findViewById(R.id.imgLoveLive);
			viewHolder.imgUnLove = (ImageView) convertView.findViewById(R.id.imgUnLoveLive);

			viewHolder.textviewLove = (TextView) convertView.findViewById(R.id.txtLoveLive);
			viewHolder.viewImgProfile = (View) convertView.findViewById(R.id.viewImgProfile);

			viewHolder.imgLove.setOnClickListener(this);
			viewHolder.imgUnLove.setOnClickListener(this);
			viewHolder.viewVideo.setOnClickListener(this);
			viewHolder.textUser.setOnClickListener(this);
			viewHolder.viewImgProfile.setOnClickListener(this);
			viewHolder.viewShare.setOnClickListener(this);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
			//		viewHolder.topic.setTag(getItem(position));

		}
		live = getItem(position);

		try {
			viewHolder.textCount.setText("" + getItem(position).getLiveStream().getWatchingCount());
			viewHolder.textView.setText(getItem(position).getLiveStream().getTitle());
			viewHolder.textUser.setText(getItem(position).getLiveStream().getUser().getFirstName() == null ? "" : getItem(position).getLiveStream().getUser().getFirstName());
			imageLoader.displayImage(getItem(position).getLiveStream().getSnapshotMedium(), viewHolder.imageView, options);
			imageLoader.displayImage(getItem(position).getLiveStream().getUser().getAvatar(), viewHolder.imgProfile, optionsProfile);

			viewHolder.textviewLove.setText("" + getItem(position).getLiveStream().getLove());

			if (getItem(position).getLiveStream().isLove()){
				viewHolder.imgLove.setVisibility(View.GONE);
				viewHolder.imgUnLove.setVisibility(View.VISIBLE);
			}else {
				viewHolder.imgLove.setVisibility(View.VISIBLE);
				viewHolder.imgUnLove.setVisibility(View.GONE);
			}


			//	viewHolder.viewimage.setBackgroundColor(Color.argb(180, position * 50, position * 107, position * 181));

		}catch (Exception e){
			Log.d("Error","Null"+e.getMessage());
		}

		viewHolder.viewVideo.setTag(arrlive.get(position));
		viewHolder.textUser.setTag(arrlive.get(position));
		viewHolder.viewImgProfile.setTag(arrlive.get(position));
		viewHolder.imgLove.setTag(live);
		viewHolder.imgUnLove.setTag(live);
		viewHolder.viewShare.setTag(live);
		viewHolder.imgLove.setTag(R.id.imgLove, viewHolder);
		viewHolder.imgUnLove.setTag(R.id.imgUnlove, viewHolder);

		return convertView;
	}



	class ViewHolder{
		ImageView imageView,imgLove,imgUnLove,imgProfile;
		TextView textView;
		View viewdisplays;
		TextView textUser;
		TextView textCount;
		View viewVideo;
		View viewimage ,viewImgProfile;
		View viewShare;
		TextView textviewLove;
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		LiveStreamingChannel live = (LiveStreamingChannel)v.getTag();
		User user = live.getLiveStream().getUser();
		User userLocal = UserUtil.getUser(context);
		ViewHolder viewHolder;
		switch (v.getId()){
			case R.id.viewVideos :
				intent = new Intent(context, LiveOnAirPlayerActivity.class);
				intent.putExtra(LiveStreamingChannel.class.getName(),live);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				((Activity) context).startActivityForResult(intent, 2);
				//	context.startActivity(intent);

				break;
			case R.id.imgLoveLive :
				if(UserUtil.isLoggedIn(context)) {
					live.getLiveStream().setIsLove(true);
					live.getLiveStream().setLove(live.getLiveStream().getLove() + 1);
					isLove(live.getLiveStream().getId());
					viewHolder = (ViewHolder)v.getTag(R.id.imgLove);
					viewHolder.imgLove.setVisibility(View.GONE);
					viewHolder.imgUnLove.setVisibility(View.VISIBLE);
				}else{
					intent = new Intent(context, LoginsActivity.class);
					context.startActivity(intent);

				}
				break;
			case R.id.imgUnLoveLive :
				if(UserUtil.isLoggedIn(context)) {
					live.getLiveStream().setIsLove(false);
					live.getLiveStream().setLove(live.getLiveStream().getLove()-1);
					isUnLove(live.getLiveStream().getId());
					viewHolder = (ViewHolder)v.getTag(R.id.imgUnlove);
					viewHolder.imgLove.setVisibility(View.VISIBLE);
					viewHolder.imgUnLove.setVisibility(View.GONE);
				}else{
					intent = new Intent(context, LoginsActivity.class);
					context.startActivity(intent);

				}
				break;
			case R.id.viewImgProfile:
			case R.id.txtuser :
				String idUser = userLocal.getId();

				if(user.getId().equals(idUser)){
					if(UserUtil.isLoggedIn(context)) {
						intent = new Intent(context, MyliveGridviewActivity.class);
						context.startActivity(intent);
					}else{
						intent = new Intent(context, LoginsActivity.class);
						context.startActivity(intent);
					}
				}else{

					intent = new Intent(context, MemberProfileActivity.class);
					intent.putExtra(User.class.getName(),user);
					((Activity) context).startActivityForResult(intent, 1);
					//	context.startActivity(intent);
				}
				break;
			case R.id.viewShare:

				try{
					Intent intents = new Intent(Intent.ACTION_SEND);
					//	intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intents.setType("text/plain");
					intents.putExtra(Intent.EXTRA_TEXT,live.getLiveStream().getWeburl());
					((Activity) context).startActivity(Intent.createChooser(intents, "Share with..."));
				}catch (Exception e){
					Log.e("Error",":"+e.getMessage());
				}


				break;

		}

	}


	public void isLove(String his){
		liveId = his;

		LoveCommand command = new LoveCommand(context);
		new Love((FragmentActivity)context).execute(command);

	}
	public void isUnLove(String his){
		liveId = his;
		User user = UserUtil.getUser(context);
		String url = ResourceUtil.getServiceUrl(context)+context.getString(R.string.app_service_love_live)+liveId+"/loves";
		Log.d("Source", "URL :" + url);
		new LoveUndoCommand(context, user,url) {

			protected void onPostExecute(JSONObject result) {

				try {
					if(result!=null){
						int status = result.getInt("status");
						String message = result.getString("message");
						Log.d("message Response",":"+message);
						notifyDataSetChanged();
					}else{
						Toast.makeText(context, "Fail! No Internet Connection", Toast.LENGTH_SHORT).show();
					}

				} catch (JSONException e) {
					e.getStackTrace();

				}

			};
		}.execute();

	}



	class Love extends CommonServiceTask {

		public Love(FragmentActivity context) {
			super(context);
		}


		@Override
		protected ServiceResponse doInBackground(Command... params) {

			String url = ResourceUtil.getServiceUrl(context)+context.getString(R.string.app_service_love_live)+liveId+"/loves";
			User user = UserUtil.getUser(context);
			Log.d("Source", "="); Log.d("Source", "URL :"+url);
			ServiceConnector srConnector = new ServiceConnector(url);

			List<NameValuePair> headers = new ArrayList<NameValuePair>();
			headers.add(new BasicNameValuePair("Content-Type", "application/json"));
			headers.add(new BasicNameValuePair("X-TIT-ACCESS-TOKEN", user.getToken()));

			ServiceResponse serviceResponse = srConnector.doAsynPost(params[0], true, headers);
			Log.d("headers", "URL :"+headers);

			try {
				int code = serviceResponse.getCode();
				if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
					JSONObject objuser = new JSONObject(serviceResponse.getContent().toString());

				}

			} catch (Exception e) {

				serviceResponse.setMessage(e.getMessage());
				Log.e(AppConfig.LOG, "doInBackground error", e);
			}

			return serviceResponse;
		}



		@Override
		protected void onPostExecute(ServiceResponse result) {
			super.onPostExecute(result);

			try {
				if (result.getCode() == ServiceResponse.SUCCESS|| result.getCode() == HttpStatus.SC_OK) {
					notifyDataSetChanged();
					context.setResult(Activity.RESULT_OK);

				} else {
					Toast.makeText(context, "Fail"+"!\r\n" + result.getMessage(),Toast.LENGTH_LONG).show();

				}
			} catch (Exception e) {
				Toast.makeText(context, "ERROR!\r\n" + e.getMessage(),Toast.LENGTH_LONG).show();
				Log.e(AppConfig.LOG, "process response error", e);
			}
		}
	}


}