package com.touchtechnologies.tit.live.category;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;

import com.touchtechnologies.animate.gridview.FloatingAction;
import com.touchtechnologies.animate.pager.PagerSlidingTabStrip;
import com.touchtechnologies.animate.progress.RotateLoading;
import com.touchtechnologies.app.TransparentDialog;
import com.touchtechnologies.command.insightthailand.RestServiceHistoryListCommand;
import com.touchtechnologies.command.insightthailand.RestServiceNearbyCommand;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.DataItem;
import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.dataobject.insightthailand.CategoryLiveStream;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamConnection;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.LocationAwareFragment;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.cctvitlive.LiveStreamFragment;
import com.touchtechnologies.tit.cctvitlive.LiveStreamPagerAdapter2;
import com.touchtechnologies.tit.history.LiveHistoryGridviewAdapter;
import com.touchtechnologies.tit.history.LiveHistoryGridviewFragment;
import com.touchtechnologies.tit.live.LiveCam;
import com.touchtechnologies.tit.live.mylivestream.FragmentMyliveStream;
import com.touchtechnologies.tit.login.LoginsActivity;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;
import com.touchtechnologies.wifi.WIFIMod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TouchICS on 3/25/2016.
 */
public class PagerCategoryActivity extends SettingActionbarActivity {

    int blue = 0xFF6495ED;
    private LiveStreamConnection stream;
    protected List<DataItem> pois;
    private Spinner spinner1;
    private PagerCategoryAdapter adapter;
    public static String KEY = "position";
    double latitude;
    double longitude ;
    Hotel hotel;
    CCTV cctv;
    LiveStreamingChannel live;
    StreamHistory hiss;
    TransparentDialog dialog;
    EditText title,detail;
    ViewPager viewPager;
    private int selectedPager;
    private FloatingAction mFloatingAction;
    private List<CategoryLiveStream> arrCategoryLive = new ArrayList<CategoryLiveStream>();
    ListView list;
    int position;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.simple_tab_category);

        mTitle = "CATEGORY LIVE";
        viewPager = (ViewPager)findViewById(R.id.pager);
        position = getIntent().getExtras().getInt(KEY);

        // Set the ViewPagerAdapter into ViewPager
        adapter = new PagerCategoryAdapter(this,getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(position);
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip)findViewById(R.id.tabs);
        tabs.setShouldExpand(true);
        tabs.setIndicatorColor(0xFFDD0000);
        tabs.setIndicatorHeight(5);
        tabs.setViewPager(viewPager);
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageSelected(int position) {
                selectedPager = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });

    }


}



