package com.touchtechnologies.tit.live.category;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.touchtechnologies.dataobject.insightthailand.CategoryLiveStream;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.live.mylivestream.MyEditLiveStreamActivity;
import com.touchtechnologies.tit.live.mylivestream.MylivePlayer;

import java.util.List;
import java.util.Timer;

/**
 * Created by TouchICS on 3/25/2016.
 */
public class ListCategoryAdapter extends BaseAdapter {

    private Context context;
    private Runnable runnable;
    protected ImageLoader imageLoader;
    protected DisplayImageOptions options;
    private View.OnClickListener listener;
    Timer t;
    private List<CategoryLiveStream> arrCategories;
    private StreamHistory history;
    String url;
    public ListCategoryAdapter(Context context) {
        this.context = context;
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .cacheInMemory(true)
                .handler(new Handler())
                //	.displayer(new FadeInBitmapDisplayer(500)) //fade in images
                //	.resetViewBeforeLoading(true)
                .build();
        imageLoader = ImageLoader.getInstance();


    }
    public void setData(List<CategoryLiveStream> arrmyhistory){
        this.arrCategories = arrmyhistory;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return arrCategories == null ? 0 : arrCategories.size();
    }
    @Override
    public boolean hasStableIds() {
        return true;
    }


    public CategoryLiveStream getItem(int position) {
        return arrCategories.get(position);
    }


    public long getItemId(int position) {
        return position;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_category, parent, false);
            viewHolder = new ViewHolder();


            viewHolder.imageView = (ImageView)convertView.findViewById(R.id.imgCover);
            viewHolder.textView  = (TextView)convertView.findViewById(R.id.textName);
            viewHolder.textCountCat = (TextView)convertView.findViewById(R.id.txtVideoCountCat);
            viewHolder.viewCategory = (View)convertView.findViewById(R.id.viewCat);

            convertView.setTag(viewHolder);

        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.textView.setText(getItem(position).getCategoryName());
        viewHolder.textCountCat.setText(getItem(position).getCountStream());
        imageLoader.displayImage(getItem(position).getIconCoverCategory(), viewHolder.imageView, options);


        //	viewHolder.viewimage.setBackgroundColor(Color.argb(180, position * 50, position * 107, position * 181));

        viewHolder.viewCategory.setTag(arrCategories.get(position));

        return convertView;
    }



    class ViewHolder{
        ImageView imageView;
        TextView textView,textCountCat;
        View viewCategory;

    }



}