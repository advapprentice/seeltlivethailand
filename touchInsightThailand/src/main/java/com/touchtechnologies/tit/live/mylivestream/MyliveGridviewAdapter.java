package com.touchtechnologies.tit.live.mylivestream;

import java.util.List;
import java.util.Timer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.tit.R;


public class MyliveGridviewAdapter extends BaseAdapter implements OnClickListener{

	private Context context;
	private Runnable runnable;
	protected ImageLoader imageLoader;
	protected DisplayImageOptions options;
	Timer t;
	private List<StreamHistory> arrmyhistory;
	private StreamHistory history;
	String url;
	public MyliveGridviewAdapter(Context context) {
		this.context = context;
		

		imageLoader = ImageLoader.getInstance();
		
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.NONE)
				.showImageForEmptyUri(R.drawable.bg_cctvdefault)
				.showImageOnFail(R.drawable.bg_cctvdefault)
				.showImageOnLoading(R.drawable.bg_cctvdefault)
				.resetViewBeforeLoading(true).build();
 	     	  
 	    imageLoader = ImageLoader.getInstance();
	
		
	}
	public void setData(List<StreamHistory> arrmyhistory){
		this.arrmyhistory = arrmyhistory;
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return arrmyhistory == null ? 0 : arrmyhistory.size();
	}
	@Override
	public boolean hasStableIds() {
		return true;
	}
	
	
	public StreamHistory getItem(int position) {
		return arrmyhistory.get(position);
	}


	public long getItemId(int position) {
		return position;
	}
	
	
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if(convertView == null){
		   convertView = LayoutInflater.from(context).inflate(R.layout.item_my_live_gridview, parent, false);
		
			
			viewHolder = new ViewHolder();
			

			viewHolder.imageView = (ImageView)convertView.findViewById(R.id.imageViewmylive);
			viewHolder.imgEdit = (ImageView)convertView.findViewById(R.id.imgEdit);
			viewHolder.textView  = (TextView)convertView.findViewById(R.id.txttitle);
			viewHolder.textViewdate  = (TextView)convertView.findViewById(R.id.textdate);
			viewHolder.textCount = (TextView)convertView.findViewById(R.id.txtCount);
			viewHolder.viewEdit = (View)convertView.findViewById(R.id.viewEdit);

			viewHolder.imageView.setOnClickListener(this); 
			viewHolder.imgEdit.setOnClickListener(this);
			viewHolder.viewEdit.setOnClickListener(this);

			convertView.setTag(viewHolder);
		}else{
			viewHolder = (ViewHolder)convertView.getTag();
	//		viewHolder.topic.setTag(getItem(position));
			
		}
		viewHolder.textCount.setText(getItem(position).getWatchedCount());
		viewHolder.textView.setText(getItem(position).getTitle());
		imageLoader.displayImage(getItem(position).getSnapshots(), viewHolder.imageView, options);
		viewHolder.textViewdate.setText(getItem(position).getCreatedtDates() > 0?getItem(position).getCreatedDateStrings():"-");
	//	viewHolder.viewimage.setBackgroundColor(Color.argb(180, position * 50, position * 107, position * 181));
	
		viewHolder.imageView.setTag(arrmyhistory.get(position));
		viewHolder.imgEdit.setTag(arrmyhistory.get(position));
		viewHolder.viewEdit.setTag(arrmyhistory.get(position));
		return convertView;
	}



	class ViewHolder{
		ImageView imageView;
		ImageView imgEdit;
		TextView textView;
		TextView  textViewdate;
		TextView textUser;
		TextView textCount;
		View viewdisplays;
		View viewEdit;
		
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		StreamHistory myhistory = (StreamHistory)v.getTag();
		switch (v.getId()) {
		case R.id.imageViewmylive:
			intent = new Intent(context, MylivePlayer.class);
			intent.putExtra(StreamHistory.class.getName(),myhistory);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			context.startActivity(intent);
			break;
		case R.id.imgEdit:
			intent = new Intent(context, MyEditLiveStreamActivity.class);
			intent.putExtra(StreamHistory.class.getName(),myhistory);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			context.startActivity(intent);
			break;
		case R.id.viewEdit:
			intent = new Intent(context, MyEditLiveStreamActivity.class);
			intent.putExtra(StreamHistory.class.getName(),myhistory);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			context.startActivity(intent);
			break;
			
			
		}
		
	}
	
	
	
}