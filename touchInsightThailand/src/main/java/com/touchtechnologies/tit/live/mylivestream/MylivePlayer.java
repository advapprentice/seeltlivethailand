package com.touchtechnologies.tit.live.mylivestream;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;


import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.EMVideoView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.tit.R;

 
public class MylivePlayer extends  FragmentActivity implements OnClickListener{
	
    private StreamHistory history; 
    private TextView load;
	private EMVideoView mEmVideoView;
    int position;
    int increment;
  	ProgressDialog progDailog;
	DisplayImageOptions options;
	ImageView imageViewAvatar,refresh;
	VideoView vid;
	ProgressBar progressBar ;
    Animation animBlink;
    DisplayMetrics dm; 
    SurfaceView sur_View; 
    MediaController media_Controller; 
    View viewExit;
    Button btnExit;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
      	setContentView(R.layout.activity_live_my_video);

      	history = (StreamHistory) getIntent().getSerializableExtra(StreamHistory.class.getName());	
  
	//	final ProgressDialog progDailog = new ProgressDialog(this);
    	final MediaController mediaController = new MediaController(this);

    	final ProgressDialog pDialog = new ProgressDialog(this);

    	vid = (VideoView) findViewById(R.id.videoPlay);
    	viewExit = (View)findViewById(R.id.viewExit);
    	btnExit = (Button)findViewById(R.id.btnExit);
    	load = (TextView) findViewById(R.id.loading);
		mEmVideoView = (EMVideoView) findViewById(R.id.video_play_activity_video_view);
    
    	progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		progressBar.setVisibility(View.VISIBLE);

		progressBar.setVisibility(View.VISIBLE);
		mediaController.setAnchorView(vid);
		
		
		viewExit.setOnClickListener(this);
		btnExit.setOnClickListener(this);
	//	Uri video = Uri.parse("rtsp://203.170.193.73:1935/vod/sample.mp4");
		Uri video = Uri.parse(history.getHttp());

		mEmVideoView.setVideoURI(video);
		mEmVideoView.setOnPreparedListener(new OnPreparedListener() {
			@Override
			public void onPrepared() {
				mEmVideoView.start();
			}
		});

	}
  
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnExit:
		case R.id.viewExit:
				finish();
			break;
		
		}
		
	}
	
    

}


