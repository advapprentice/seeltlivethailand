package com.touchtechnologies.tit;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.touchtechnologies.dataobject.DataItem;

/**
 * Simple POI adapter that create a simple image and title
 * @author Touch
 *
 */
public class POIAdapter extends BaseAdapter{
	protected List<? extends DataItem> pois;
	protected Context context;
	protected int resLayout;
	
	protected ImageLoader imageLoader;
	protected DisplayImageOptions options;
	
	public POIAdapter(Context context, int resLayout) {
		this.context = context;
		this.resLayout = resLayout;
		
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
				.showImageForEmptyUri(R.drawable.bg_noimage)
				.showImageOnFail(R.drawable.bg_noimage)
				.showImageOnLoading(R.drawable.bg_loading)
				.build();
	}
	
	public void setData(List<? extends DataItem> pois){
		this.pois = pois;
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return pois == null?0:pois.size();
	}
	
	@Override
	public DataItem getItem(int position) {
		return pois.get(position);
	}
	
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder vh = null;
		
		if(convertView == null){
			convertView = LayoutInflater.from(context).inflate(resLayout, parent, false);
		//	convertView = LayoutInflater.from(context).inflate(R.layout.item_list_channel_rerun, parent, false);
			vh = new ViewHolder();
			vh.imageView = (ImageView)convertView.findViewById(R.id.imageViewRoi);
			vh.textView  = (TextView)convertView.findViewById(R.id.textViewRoiName);
			
			convertView.setTag(vh);
		}else{
			vh = (ViewHolder)convertView.getTag();
		}
		
		vh.textView.setText(getItem(position).getName());
		imageLoader.displayImage(getItem(position).getImage().getUri(), vh.imageView, options);
		
		return convertView;
	}
	
	class ViewHolder{
		ImageView imageView;
		TextView textView;
	}
}
