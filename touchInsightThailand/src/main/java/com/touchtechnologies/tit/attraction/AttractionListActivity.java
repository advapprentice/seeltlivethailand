package com.touchtechnologies.tit.attraction;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.touchtechnologies.command.insightthailand.RestServiceAttractionListCommand;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.dataobject.Restaurant;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.restaurant.RestaurantPOIFilterAdapter;
import com.touchtechnologies.tit.search.SearchActionbarActivity;
import com.touchtechnologies.tit.util.ResourceUtil;

public class AttractionListActivity extends SearchActionbarActivity implements OnItemClickListener{
	private ROI roi;
	private RestaurantPOIFilterAdapter listAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		roi = (ROI)getIntent().getSerializableExtra(ROI.class.getName());
		mTitle = roi.getName();
		
		setContentView(R.layout.activity_restaurant_list);
		

		listAdapter = new RestaurantPOIFilterAdapter(this,R.layout.item_list_pois_restaurant);
		
//		View headerView = getLayoutInflater().inflate(R.layout.item_ads, null);
		ListView listView = (ListView)findViewById(R.id.listViewRestaurantPOIs);
		listView.setOnItemClickListener(this);
		listView.setAdapter(listAdapter);
		
		
		/*
		listView.addHeaderView(headerView, null, false);
		listView.setOnScrollListener(new AbsListView.OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {

				View headerView = view.findViewById(R.id.header);

				final float mTop = -headerView.getTop();
				float height = headerView.getHeight();
				if (mTop > height) {
					// ignore
					return;
				}
				View imgView = headerView.findViewById(R.id.viewads);
				imgView.setTranslationY(mTop / 2f);

			}
		});
		
		
		/*
		 * Get Restaurant POI
		 */
		String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_pois);
		new RestServiceAttractionListCommand(this, roi, url){
			
			protected void onPostExecute(List<Restaurant> result) {
				listAdapter.setData(result);
			}
			
		}.execute();
		
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	
		Intent intent = new Intent(getApplicationContext(), AttractionDetailActivity.class);
//		intent.putExtra(Restaurant.class.getName(), listAdapter.getItem(position-1));
		intent.putExtra(Restaurant.class.getName(), listAdapter.getItem(position));
		startActivity(intent);
	}
	
}
