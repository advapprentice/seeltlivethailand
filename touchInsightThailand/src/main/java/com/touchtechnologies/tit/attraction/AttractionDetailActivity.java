package com.touchtechnologies.tit.attraction;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.widget.TextView;

import com.touchtechnologies.command.insightthailand.RestServiceGetRestaurantCommand;
import com.touchtechnologies.dataobject.Restaurant;
import com.touchtechnologies.tit.POIUpdateListner;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.comment.CommentRestaurant;
import com.touchtechnologies.tit.search.SearchActionbarActivity;
import com.touchtechnologies.tit.util.ResourceUtil;


public class AttractionDetailActivity extends SearchActionbarActivity {
	private Restaurant poi;
	POIUpdateListner poiUpdateListeners;
//	RestaurantDetailFragment fragment;
	CommentRestaurant fragment;
	TextView texthotelname,adddress,email,tel,description,txtrating,paycredit;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_restaurant_detail);
		
		poi = (Restaurant)getIntent().getSerializableExtra(Restaurant.class.getName());
		mTitle = poi.getName();

		// frgTransaction.addToBackStack(null);
//		RestaurantDetailFragment hsFragment = new RestaurantDetailFragment(poi);
		CommentRestaurant hsFragment = new CommentRestaurant(poi);
		poiUpdateListeners = hsFragment;
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.fragmentRestaurant, hsFragment).commit();
		
		
	    final 	String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_poi_attraction_byID)+poi.getIdHotel();
		new RestServiceGetRestaurantCommand(this, url){
							
				protected void onPostExecute(Restaurant result) {
					poi = result;
					Log.d("url","=" +url);
					if(poiUpdateListeners != null){
						poiUpdateListeners.poiUpdated(result);
						
					
						
					}
				};
			}.execute();
	
			
	}
	


	
}
