package com.touchtechnologies.tit.dataobject;

import java.io.Serializable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Node use to draw poly line
 * @author Touch
 *
 */
public class Node implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8403510596740638500L;
	private int id;
	private LatLng latLng;
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setLatLng(LatLng latLng) {
		this.latLng = latLng;
	}
	
	public int getId() {
		return id;
	}
	
	public LatLng getLatLng() {
		return latLng;
	}
}
