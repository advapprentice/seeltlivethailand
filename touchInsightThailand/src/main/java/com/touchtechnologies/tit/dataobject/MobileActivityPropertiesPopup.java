package com.touchtechnologies.tit.dataobject;

import com.touchtechnologies.json.JSONUtil;

import org.json.JSONObject;

public class  MobileActivityPropertiesPopup {

    private int id;
    final static String Button_popup = "button",Image_popup = "image";
    public static String[] action_key ={"Cancel","Register","LiveStreaming","CCTV","Coupon","Activity"};

    private String text,text_color,background_color,type,image_url,content_url;
    private int popup_action;
    private String tedst;
    private  int action;

    public MobileActivityPropertiesPopup(JSONObject json, int id) {

        if(!json.isNull("type")){
            setId(id);
            setType(JSONUtil.getString(json, "type"));
        }
        if(!json.isNull("text")){
            setText(JSONUtil.getString(json, "text"));
        }
        if(!json.isNull("content_url")){
            setContent_url(JSONUtil.getString(json,"content_url"));
        }
        if(!json.isNull("text_color")){
            setText_color(JSONUtil.getString(json, "text_color"));
        }
        if(!json.isNull("background_color")){
            setBackground_color(JSONUtil.getString(json, "background_color"));
        }
        if(!json.isNull("action")){
            int action = JSONUtil.getInt(json, "action");
            setPopup_action(action);

        }
        if(!json.isNull("image_url")){
            setImage_url(JSONUtil.getString(json, "image_url"));
        }



    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText_color() {
        return text_color;
    }

    public void setText_color(String text_color) {
        this.text_color = text_color;
    }

    public String getBackground_color() {
        return background_color;
    }

    public void setBackground_color(String background_color) {
        this.background_color = background_color;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPopup_action() {
        return popup_action;
    }

    public void setPopup_action(int popup_action) {
        this.popup_action = popup_action;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String[] getAction_key(){
        return  action_key;
    }

    public String getContent_url() {
        return content_url;
    }

    public void setContent_url(String content_url) {
        this.content_url = content_url;
    }
}
