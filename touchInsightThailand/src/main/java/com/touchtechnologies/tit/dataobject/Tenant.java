package com.touchtechnologies.tit.dataobject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;

public class Tenant implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6130689720801838623L;

	private int id;
    private String name;
    private String type;
    private String floor;
    private String unit;
    private String tel;
    private String latitude;
    private String longitude;
    private String logo;
    private List<String> lGallery;
    
    public Tenant() {
	}
    
    public Tenant(JSONObject json) throws JSONException{
    	if(!json.isNull("id")){
    		setId(json.getInt("id"));
    	}
    	
    	if(!json.isNull("name")){
    		setName(json.getString("name"));
    	}
    	
    	if(!json.isNull("type")){
    		setType(json.getString("type"));
    	}
    	
    	if(!json.isNull("unit")){
    		setUnit(json.getString("unit"));
    	}
    	
    	if(!json.isNull("tel")){
    		setTel(json.getString("tel"));
    	}
    	
    	if(!json.isNull("latitude")){
    		setLatitude(json.getString("latitude"));
    	}
    	
    	if(!json.isNull("longitude")){
    		setLongitude(json.getString("longitude"));
    	}
    	if(!json.isNull("floor")){
    		setFloor(json.getString("floor"));
    	}
    	if(!json.isNull("logo")){
    		setLogo(json.getString("logo"));
    	}
       
    	if(!json.isNull("gallery")){
    		JSONArray jArray = json.getJSONArray("gallery");
    		for(int i=0; i<jArray.length(); i++){
    			addGallryImage(jArray.getString(i));
    		}
    	}
    }
    
    public void setId(int id) {
		this.id = id;
	}
    
    public int getId() {
		return id;
	}
    
    public void setName(String name) {
		this.name = name;
	}
    
    public String getName() {
		return name;
	}
    
    public void setTel(String tel) {
		this.tel = tel;
	}
    
    public String getTel() {
		return tel;
	}
    
    public void setType(String type) {
		this.type = type;
	}
    
    public String getType() {
		return type;
	}
    
    public void setUnit(String unit) {
		this.unit = unit;
	}
    
    public String getUnit() {
		return unit;
	}
    
    public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
    
    public String getLatitude() {
		return latitude;
	}
    
    public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
    
    public String getLongitude() {
		return longitude;
	}
    
    public void setFloor(String floor) {
		this.floor = floor;
	}
    public String getFloor() {
		return floor;
	}
    public void setLogo(String logo) {
		this.logo = logo;
	}
    public String getLogo() {
		return logo;
	}
    
    
    public void addGallryImage(String url){
    	if(lGallery == null){
    		lGallery = new ArrayList<>();
    	}
    	
    	lGallery.add(url);
    }
    
    /**
     * Get image url at position
     * @param postion
     * @return
     */
    public String getImageAt(int postion){
    	return lGallery.get(postion);
    }
    
    /**
     * NUmber of image in gallery
     * @return
     */
    public int getSizeImage(){
    	return lGallery == null?0:lGallery.size();
    }
    
    public LatLng getLatLng(){
    	LatLng ll = new LatLng(0, 0);
    	try{
    		ll = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
    	}catch(Exception e){
    		
    	}
    	return ll;
    }
    
    @Override
    public String toString() {
    	StringBuilder stringBuilder = new StringBuilder();
    	
    	stringBuilder.append("id:" + id + "\r\n");
    	stringBuilder.append("name:" + name + "\r\n");
    	stringBuilder.append("type:" + type + "\r\n");
    	stringBuilder.append("unit:" + unit + "\r\n");
    	stringBuilder.append("tel:" + tel + "\r\n");
    	stringBuilder.append("lat:" + latitude + "\r\n");
    	stringBuilder.append("long:" + longitude + "\r\n");
    	stringBuilder.append("floor:" + floor + "\r\n");
    	stringBuilder.append("logo:" + logo + "\r\n");
    	stringBuilder.append("gallery" + lGallery);
    	
    	return stringBuilder.toString();
    }
}
