package com.touchtechnologies.tit.dataobject;

import android.util.Log;
import android.view.View;

import com.touchtechnologies.json.JSONUtil;
import com.touchtechnologies.tit.R;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by TOUCH on 21/3/2559.
 */
public class HotlinePhone implements Serializable  {

    private final String KEY_ID = "id";
    private final String KEY_GROUPNAME = "hotline_group_name";
    private final String KEY_HOTLINEDATA = "hotlines";

    private int id;
    private String name;
    private List<Hotline> hotlines;


    public HotlinePhone(JSONObject jsonObject)throws JSONException{
        if(jsonObject == null){
            Log.d(HotlinePhone.class.getSimpleName(), "HotlinePhone: JsonArray is null");
            return;
        }else {
            if(!jsonObject.isNull(KEY_ID)){
                setId(JSONUtil.getInt(jsonObject,KEY_ID));
            }
            if(!jsonObject.isNull(KEY_GROUPNAME)){
                setName(JSONUtil.getString(jsonObject,KEY_GROUPNAME));
            }
            if(!jsonObject.isNull(KEY_HOTLINEDATA)){
                JSONArray jsonArray = jsonObject.getJSONArray(KEY_HOTLINEDATA);
                if(jsonArray != null){
                    for (int i =0 ; i<jsonArray.length();i++){
                        addHotline(new Hotline(jsonArray.getJSONObject(i)));
                    }
                }
            }

        }


    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Hotline> getHotlines() {
        return hotlines;
    }

    public void addHotline(Hotline hotline){
        if(hotlines == null){
            hotlines = new ArrayList<>();
        }
        hotlines.add(hotline);
    }

    public void setHotlines(List<Hotline> hotlines) {
        this.hotlines = hotlines;
    }

    public JSONObject toJson() throws JSONException{
        JSONObject jsonObject = new JSONObject();
                jsonObject.put(KEY_GROUPNAME,getName());
                jsonObject.put(KEY_ID,getId());
                JSONArray jsonArray = new JSONArray();
                for(int i =0; i<getHotlines().size();i++){
                    jsonArray.put(getHotlines().get(i).toJson());
                }
                jsonObject.put(KEY_HOTLINEDATA,jsonArray);
        return jsonObject;
    }



    public class Hotline {

        private final String KEY_ID = "id";
        private final String KEY_NAME ="hotline_name";
        private final String KEY_PHONE_NUM ="hotline_call";
        private final String KEY_IMAGE = "hotline_image";

        private int id;
        private String name;
        private String phonenum;
        private String image;

        public Hotline(JSONObject jsonObject){
            if(jsonObject == null){
                Log.d(Hotline.class.getSimpleName(), "Hotline: jsonobect is null");
                return;
            }else {
                    if (!jsonObject.isNull(KEY_ID)) {
                        setId(JSONUtil.getInt(jsonObject, KEY_ID));
                    }
                    if (!jsonObject.isNull(KEY_NAME)) {
                        setName(JSONUtil.getString(jsonObject, KEY_NAME));
                    }
                    if (!jsonObject.isNull(KEY_PHONE_NUM)) {
                        setPhonenum(JSONUtil.getString(jsonObject, KEY_PHONE_NUM));
                    }
                    if (!jsonObject.isNull(KEY_IMAGE)) {
                        setImage(JSONUtil.getString(jsonObject, KEY_IMAGE));
                    }
            }

        }

        public int getId() {
            return id;
        }

        private void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        private void setName(String name) {
            this.name = name;
        }

        public String getPhonenum() {
            return phonenum;
        }

        private void setPhonenum(String phonenum) {
            this.phonenum = phonenum;
        }

        public String getImage() {
            return image;
        }

        private void setImage(String image) {
            this.image = image;
        }

        protected   JSONObject toJson() throws JSONException{
            JSONObject jsonObject = new JSONObject();
                jsonObject.put(KEY_ID,getId());
                jsonObject.put(KEY_IMAGE,getImage());
                jsonObject.put(KEY_NAME,getName());
                jsonObject.put(KEY_PHONE_NUM,getPhonenum());
            return jsonObject;
        }


    }



}
