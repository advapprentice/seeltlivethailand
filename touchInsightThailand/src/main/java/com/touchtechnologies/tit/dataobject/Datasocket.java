package com.touchtechnologies.tit.dataobject;

import com.touchtechnologies.dataobject.insightthailand.Comment;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.json.JSONUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by TOUCH on 28/4/2559.
 */
public class Datasocket implements Serializable {
   private Socket_type mSocket_type;
    private String KEY_DATA = "data";
    private String KEY_ID   = "id";
    private String KEY_STREAMINGID = "StreamingID";
    private String KEY_WATCHCOUNT = "watchedCount";
    private String KEY_LOVECOUNT = "loves_count";
    private String KEY_COMMENT_CONTENT = "comment_content";
    private String KEY_COMMENTTATOR = "commentator";
    private Vid_Comment mVid_comment;
    private Vid_property mVid_property;

    public Datasocket(Socket_type mSocket_type, String json) {
        this.mSocket_type = mSocket_type;
        try {
            JSONObject jsonObject = new JSONObject(json);
            jsonObject = jsonObject.getJSONObject(KEY_DATA);
            switch (mSocket_type) {
                case watchscount:
                    mVid_property = new Vid_property(jsonObject);
                    break;
                case lovescount:
                    mVid_property = new Vid_property(jsonObject);
                    break;
                case comment:
                    mVid_comment = new Vid_Comment(jsonObject);
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Object getdata(Socket_type mSocket_type){
        switch (mSocket_type) {
            case lovescount:
                return  getmVid_property();


            case watchscount:
                return getmVid_property();


            case comment:
                return getmVid_comment();

            default:
                return null;

        }

    }

    private Vid_property getmVid_property() {
        return mVid_property;
    }

    private Vid_Comment getmVid_comment() {
        return mVid_comment;
    }


    public enum Socket_type{
        watchscount,lovescount,comment
    }

    public class Vid_Comment{
        private String id;
        private User user;
        private Comment content;

        public Vid_Comment(JSONObject json) throws JSONException {
            if(!json.isNull(KEY_ID)){
                setId(json.getString(KEY_ID));
            }
            if(!json.isNull(KEY_COMMENTTATOR)){
                setUser(new User(json.getJSONObject(KEY_COMMENTTATOR)));
            }
            if(!json.isNull(KEY_COMMENT_CONTENT)){
                setContent(new Comment(json));
            }
        }

        public String getId() {
            return id;
        }

        private void setId(String id) {
            this.id = id;
        }

        public User getUser() {
            return user;
        }

        private void setUser(User user) {
            this.user = user;
        }

        public Comment getContent() {
            return content;
        }

        private void setContent(Comment content) {
            this.content = content;
        }
    }

    public class Vid_property{
        private String StreamID;
        private String id;
        private int love;
        private int watched;

        public Vid_property(JSONObject jsonObject) {
            if(!jsonObject.isNull(KEY_ID)){
              setId(JSONUtil.getString(jsonObject,KEY_ID));
            }
            if(!jsonObject.isNull(KEY_STREAMINGID)){
              setStreamID(JSONUtil.getString(jsonObject,KEY_STREAMINGID));
            }
            if(!jsonObject.isNull(KEY_LOVECOUNT)){
                setLove(JSONUtil.getInt(jsonObject,KEY_LOVECOUNT));
            }
            if(!jsonObject.isNull(KEY_WATCHCOUNT)){
                setWatched(JSONUtil.getInt(jsonObject,KEY_WATCHCOUNT));
            }

        }

        public String getStreamID() {
            return StreamID;
        }

        private void setStreamID(String streamID) {
            StreamID = streamID;
        }

        public String getId() {
            return id;
        }

        private void setId(String id) {
            this.id = id;
        }

        public int getLove() {
            return love;
        }

        private void setLove(int love) {
            this.love = love;
        }

        public int getWatched() {
            return watched;
        }

        private void setWatched(int watched) {
            this.watched = watched;
        }
    }

}
