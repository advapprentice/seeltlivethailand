package com.touchtechnologies.tit.dataobject;

import com.touchtechnologies.json.JSONUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by TOUCH on 29/1/2559.
 */
public class PopupDao implements Serializable {


    private int id;
    private String keyname;
    private String Subject;
    private int template_id;
    private ArrayList<MobileActivityPropertiesPopup> mobileActivityPropertiesArrayList = new ArrayList<>();

    public PopupDao(JSONObject json) throws JSONException {
            if(!json.isNull("id")){
                setId(JSONUtil.getInt(json, "id"));
            }
            if(!json.isNull("subject")){
                setSubject(JSONUtil.getString(json,"subject"));
            }
            if(!json.isNull("keyname")){
                setKeyname(JSONUtil.getString(json, "keyname"));
            }
            if(!json.isNull("template_id")){
                setTemplate_id(JSONUtil.getInt(json, "template_id"));
            }
            if(!json.isNull("mobile_activity_properties")){
                JSONArray jsonArray =   json.getJSONArray("mobile_activity_properties");
                for (int i = 0 ; i < jsonArray.length(); i++){
                    MobileActivityPropertiesPopup mobileActivityPropertiesPopup = new MobileActivityPropertiesPopup(jsonArray.getJSONObject(i),i);
                    mobileActivityPropertiesArrayList.add(mobileActivityPropertiesPopup);
                }
            }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKeyname() {
        return keyname;
    }

    public void setKeyname(String keyname) {
        this.keyname = keyname;
    }

    public int getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(int template_id) {
        this.template_id = template_id;
    }

    private void addMobileActivityProp(MobileActivityPropertiesPopup mobileActivityProperties,int i){
            mobileActivityPropertiesArrayList.add(i,mobileActivityProperties);
    }

    public MobileActivityPropertiesPopup getimagefromMobileActivityProperties(){
        for (MobileActivityPropertiesPopup mobileActivityProperties:mobileActivityPropertiesArrayList){
           if(mobileActivityProperties.getType().equals(MobileActivityPropertiesPopup.Image_popup)){
                    return mobileActivityProperties;
                }
        }

        return null;
    }
    public ArrayList<MobileActivityPropertiesPopup> getButtonfromMobileActivityPropertiesPopups(){
        ArrayList<MobileActivityPropertiesPopup> MAPPS = new ArrayList<>();
        for (MobileActivityPropertiesPopup mobileActivityProperties:mobileActivityPropertiesArrayList) {
            if(mobileActivityProperties.getType().equals(MobileActivityPropertiesPopup.Button_popup)){
                MAPPS.add(mobileActivityProperties);
            }
        }
        return MAPPS;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }
}


