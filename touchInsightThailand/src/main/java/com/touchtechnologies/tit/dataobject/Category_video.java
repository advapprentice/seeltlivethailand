package com.touchtechnologies.tit.dataobject;

import com.touchtechnologies.dataobject.insightthailand.CategoryLiveStream;
import com.touchtechnologies.json.JSONUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by TOUCH on 14/3/2559.
 */
public class Category_video implements Serializable{
    static String key_name = "Category_name";
    static String key_id = "id";
    private ArrayList<CategoryLiveStream>  mCategorys = new ArrayList<>();

    public Category_video(JSONArray jsonArray){
        for(int i=0;i<jsonArray.length();i++){
            try {
                mCategorys.add(new CategoryLiveStream(jsonArray.getJSONObject(i)));
            }catch (JSONException e){
                break;
            }
        }
    }

    public int getSize(){
        return mCategorys.size();
    }

    public ArrayList<CategoryLiveStream> getmCategorys(){
        return mCategorys;
    }

    public JSONArray toJson() throws JSONException{
        JSONArray jsonArray = new JSONArray();
        for (int i=0;i<getSize();i++){
            jsonArray.put(mCategorys.get(i).toJson());
        }
        return jsonArray;
    }

//    private class Category {
//        private int id;
//        private String Category_name;
//
//        public Category(JSONObject jsonObject){
//            if(jsonObject.isNull(key_id)) {
//                setId(JSONUtil.getInt(jsonObject, key_id));
//            }
//            if(jsonObject.isNull(key_name)){
//                setCategory_name(JSONUtil.getString(jsonObject,key_name));
//            }
//        }
//
//        public int getId() {
//            return id;
//        }
//
//        public void setId(int id) {
//            this.id = id;
//        }
//
//        public String getCategory_name() {
//            return Category_name;
//        }
//
//        public void setCategory_name(String category_name) {
//            Category_name = category_name;
//        }
//
//        protected JSONObject toJson() throws JSONException{
//         JSONObject jsonObject =   new JSONObject();
//            jsonObject.put(key_id,getId());
//            jsonObject.put(key_name,getCategory_name());
//            return jsonObject;
//        }
//
//    }
}
