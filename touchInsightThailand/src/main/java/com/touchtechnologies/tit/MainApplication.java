package com.touchtechnologies.tit;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;
import java.util.Map;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.applinks.AppLinkData;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.touchtechnologies.tit.util.AnalyticsTrackers;
import com.touchtechnologies.tit.util.ResourceUtil;

import org.apache.http.HttpResponse;

import bolts.AppLinks;
import io.socket.client.Manager;
import io.socket.client.Socket;


public class MainApplication extends Application{

	private static  boolean popup_check = true;

	public static final String TAG = MainApplication.class.getSimpleName();
	public static String url_socket ;
	public static final String mJoinroom = "join";
	public static final String mleaveroom = "leave";
	private static Socket vSocket;
	private static MainApplication mInstance;
	private RequestQueue mRequestQueue;

	private static Socket mSocket;



	@Override
	public void onCreate() {
		super.onCreate();
		FacebookSdk.sdkInitialize(getApplicationContext());
		AppEventsLogger.activateApp(this);



		mInstance = this;
		url_socket = ResourceUtil.getSocketUrl(this);
		try {
			Manager manager = new Manager(new URI(url_socket));
			mSocket = manager.socket("/websocket");
		} catch (URISyntaxException e) {

		}
		AnalyticsTrackers.initialize(this);
		AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP_TRACKER);


		Log.d(AppConfig.LOG, "Application created...");
		// Create global configuration and initialize ImageLoader with this configuration
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
				.threadPriority(Thread.NORM_PRIORITY - 1)
				.denyCacheImageMultipleSizesInMemory()
				.build();

		ImageLoader imgLoader = ImageLoader.getInstance();
		imgLoader.init(config);
		imgLoader.clearDiscCache();
		imgLoader.clearMemoryCache();

		Locale locale = new Locale("th_TH");
		Locale.setDefault(locale);
		Configuration configuration = new Configuration();
		configuration.locale = locale;
		getApplicationContext().getResources().updateConfiguration(configuration, null);

		//run post install every time when an application start
	}

	public static synchronized MainApplication getInstance() {

		return mInstance;
	}

	public synchronized Tracker getGoogleAnalyticsTracker() {
		AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
		return analyticsTrackers.get(AnalyticsTrackers.Target.APP_TRACKER);
	}

	public static synchronized Socket getmSocket(){
				if(!mSocket.connected()) {
					mSocket.connect();
				}
		return mSocket;
	}

	public void trackScreenView(String screenName) {
		Tracker t = getGoogleAnalyticsTracker();

		// Set screen name.
		t.setScreenName(screenName);
		String dimensionValue = "Android";
		t.set("&cd1", dimensionValue);
		// Send a screen view.
		t.send(new HitBuilders.ScreenViewBuilder().setCustomDimension(1, dimensionValue).build());

		GoogleAnalytics.getInstance(this).dispatchLocalHits();
	}

	public void trackException(Exception e) {
		if (e != null) {
			Tracker t = getGoogleAnalyticsTracker();

			t.send(new HitBuilders.ExceptionBuilder()
							.setDescription(
									new StandardExceptionParser(this, null)
											.getDescription(Thread.currentThread().getName(), e))
							.setFatal(false)
							.build()
			);
		}
	}

	/***
	 * Tracking event
	 *
	 * @param category event category
	 * @param action   action of the event
	 * @param label    label
	 */
	public void trackEvent(String category, String action, String label) {
		Tracker t = getGoogleAnalyticsTracker();

		// Build and send an Event.
		t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
	}


	public static boolean getPopup_check() {
		return popup_check;
	}

	public static void setPopup_check(boolean popup_check) {
		MainApplication.popup_check = popup_check;
	}

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			HttpStack mStack = new MyHurlStack();
			// getApplicationContext() is key, it keeps you from leaking the
			// Activity or BroadcastReceiver if someone passes one in.
			mRequestQueue = Volley.newRequestQueue(this,mStack);
		}
		return mRequestQueue;
	}

	public <T> void addToRequestQueue(Request<T> req) {
		getRequestQueue().add(req);
	}



	public class MyHurlStack extends HurlStack {

		private Request<?> request;
		private Map<String, String> additionalHeaders;

//		@Override
//		public HttpResponse performRequest(Request<?> request, Map<String, String> additionalHeaders)
//				throws IOException, AuthFailureError {
//			this.request = request;
//			this.additionalHeaders = additionalHeaders;
//
//			Map<String, String> headers = request.getHeaders();
//			// put your global headers
////			headers.put("Via", "netroid");
////			headers.put("Accept-Charset", "UTF-8");
////			headers.put("Origin", "http://netroid.cn/");
//
//			return super.performRequest(request, additionalHeaders);
//		}
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
		getmSocket().disconnect();
	}

    public static boolean checkpremission(String prm, @NonNull Activity context, int resqusetcode){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(context != null) {

                if (context.checkSelfPermission(prm) != PackageManager.PERMISSION_GRANTED) {
                    context.requestPermissions(new String[]{prm}, resqusetcode);
                    return false;
                } else {
                    return true;
                }

            }else {
                return false;
            }
        }else {
            return true;
        }



    }



}
