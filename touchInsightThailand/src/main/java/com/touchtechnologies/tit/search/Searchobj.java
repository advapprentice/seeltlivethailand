package com.touchtechnologies.tit.search;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.json.JSONUtil;
import com.touchtechnologies.tit.util.ResourceUtil;

public class Searchobj {
	private String title;
	private String roi;
	private String module;
	private String urlthumpnail;
	private String linkurl;
	private String liveurl;
	private String name;
	private String id;
	private String snapshot;
	private CCTV objCctv;
	private String key_name = "name";
	private String key_module = "module";
	private String key_id	="id";
	private String key_title = "title";
	private String key_Stram = "id";
	private String key_roi  = "roi";
	private String key_link = "link_url";
	private String key_livepic = "live_url";
	private String key_snapshot = "thumb";
	private String key_thumbblog = "thumb_url";
	private Context context;
	
	public Searchobj(JSONObject json,Context context) throws JSONException {
		super();
		this.context = context;
			if (json.get(key_module).equals("CCTV")) {
				if(!json.isNull(key_id)){
					setId(JSONUtil.getString(json, key_id));
				}
				if(!json.isNull(key_title)){
					setTitle(JSONUtil.getString(json, key_title));
				}
				if(!json.isNull(key_module)){
					setModule(JSONUtil.getString(json, key_module));
				}
				if(!json.isNull(key_livepic)){
					setLiveurl(JSONUtil.getString(json, key_livepic));
				}
				if(!json.isNull(key_roi)){
					setRoi(JSONUtil.getString(json, key_roi));
				}
				CCTV cctv = new CCTV(json);
				setObjCctv(cctv);
			} else if(json.get(key_module).equals("LiveStream")) {
				if (!json.isNull(key_title)) {
					setTitle(JSONUtil.getString(json, key_title));
				}
				if (!json.isNull(key_module)) {
					setModule(JSONUtil.getString(json, key_module));
				}
				if (!json.isNull(key_Stram)) {
					setId(JSONUtil.getString(json, key_Stram));
				}

				if (!json.isNull(key_snapshot)) {
					setSnapshot(JSONUtil.getString(json, key_snapshot));
				}
			//// blog/////
			}else if(json.get(key_module).equals("Blog")){
				if(!json.isNull(key_id)){
					setId(JSONUtil.getString(json, key_id));
				}
				if(!json.isNull(key_name)){
					setTitle(JSONUtil.getString(json, key_title));
				}
				if(!json.isNull(key_module)){
					setModule(JSONUtil.getString(json, key_module));
				}
				if(!json.isNull(key_link)){
					setLinkurl(JSONUtil.getString(json, key_link));
				}
				if(!json.isNull(key_thumbblog)){
					setUrlthumpnail(JSONUtil.getString(json, key_thumbblog));

				}
				if(!json.isNull(key_title)){
					setTitle(JSONUtil.getString(json, key_title));
				}

			}
		
		
	}
	
	public String getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(String snapshot) {
		this.snapshot = snapshot;
	}

	public CCTV getObjCctv() {
		return objCctv;
	}

	public void setObjCctv(CCTV objCctv) {
		this.objCctv = objCctv;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getRoi() {
		return roi;
	}
	
	public void setRoi(String roi) {
		this.roi = roi;
	}

	public String getModule() {
		return module;
	}
	
	public void setModule(String module) {
		this.module = module;
	}
	
	public String getLinkurl() {
		return linkurl;
	}
	
	public void setLinkurl(String linkurl) {
		this.linkurl = linkurl;
	}
	
	public String getUrlthumpnail() {
		return urlthumpnail;
	}

	public void setUrlthumpnail(String urlthumpnail) {
		this.urlthumpnail = urlthumpnail;
	}
	
	public String getLiveurl() {
		return liveurl;
	}
	
	public void setLiveurl(String liveurl) {
		this.liveurl = liveurl;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return this.module + "\r\n" + this.title + "\r\n" +this.id + "\r\n" + this.roi;
		// "----------------------"+"\r\n"+        +"\r\n"+"----------------------"
	}
	
}
