package com.touchtechnologies.tit.search;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.touchtechnologies.command.insightthailand.RestServiceROIListCommand;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.Blogawebctivity;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.cctvitlive.CCTVLiveDetailActivity;
import com.touchtechnologies.tit.history.LiveHistoryPlayer;
import com.touchtechnologies.tit.persistent.DataHelperAsyn;
import com.touchtechnologies.tit.util.ResourceUtil;

public class Searchactivity extends Activity {
	ListView list;
	private List<ROI> listRoi = new ArrayList<>() ;
	private String[] key_module = {"CCTV","LiveStream","Blog"};
	List<Searchobj> sobj = new ArrayList<Searchobj>();
	CursorAdapter cursorAdapter;
	String keyword="";
	AutoCompleteTextView autoCompleteTextView;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_results);
		ActionBar actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(true);
		//actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(0xFF1275AE));
		actionBar.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		list = (ListView)findViewById(R.id.searchlist);

	}
		
	@Override
	protected void onResume() {
		//ArrayAdapter<Searchobj> attrs = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line,sobj);
		 
		//ArrayAdapter<Searchobj> array = new ArrayAdapter<Searchobj>(this,android.R.layout.simple_list_item_1,sobj);
		
		SearchListadapter mAdapterSearch = new SearchListadapter(this, sobj );
		if(sobj.size()==0){
			Log.d("sobj", "==0");
			list.setVisibility(ListView.GONE);
		}else{
			list.setVisibility(ListView.VISIBLE);
		}
		list.setAdapter(mAdapterSearch);
		mAdapterSearch.notifyDataSetChanged();
		//array.notifyDataSetChanged();
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Log.d("Click","------------------------------");
				final Searchobj searchobj = sobj.get(position);
				//searchobj.getModule();
				if(searchobj.getModule().equals(key_module[0])){
					 gotocctv(searchobj);
					Log.d("Click", "id_cctv = " + searchobj.getId());
					Log.d("Click","------------------------------");
				}else if (searchobj.getModule().equals(key_module[1])) {
					getvideoObj(searchobj);
					Log.d("Click", "id_live = " + searchobj.getId());
					Log.d("Click","------------------------------");
				}else if (searchobj.getModule().equals(key_module[2])) {
					gotoblog(searchobj);
					Log.d("Click", "id_cctv = " + searchobj.getLinkurl());
					Log.d("Click","------------------------------");
				}else{
				Log.d("Click", searchobj.toString());
				Log.d("Click","------------------------------");
				}
			}
			
			
		});
		//autoCompleteTextView.setAdapter(attrs);
		//attrs.notifyDataSetChanged();
		
		//mAdapterSearch.notifyDataSetChanged();
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	//	getMenuInflater().inflate(R.menu.search, menu);
		 MenuItem menuitem = menu.add("Search");
		 menuitem.setIcon(R.drawable.ic_search);
		 // SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
		    MenuItemCompat.setShowAsAction(menuitem, MenuItemCompat.SHOW_AS_ACTION_ALWAYS | MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
			menuitem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
				@Override
				public boolean onMenuItemActionExpand(MenuItem item) {
					Log.d(AppConfig.LOG, "onMenuItemActionExpand: 1");
					return true;
				}

				@Override
				public boolean onMenuItemActionCollapse(MenuItem item) {
					Log.d(AppConfig.LOG, "onMenuItemActionCollapse: ");
					finish();
					return true;
				}
			});
		   
		    menuitem.collapseActionView();
		    
//		  View searchView = SearchViewCompat.newSearchView(this);
		 
//		  try{
//			 //searchView = setupSearchMenuItem(menu);
//
//		  if(searchView != null){
//
//			  searchView.setFocusable(true);
//
//			  SearchViewCompat.setOnQueryTextListener(searchView, new OnQueryTextListenerCompat() {
//
//				  @Override
//				  public boolean onQueryTextChange(String newText) {
//
//					  if (newText.length() >= 3)
//						  indexchange(newText);
//					  return true;
//
//				  }
//			  });
////			  menuitem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
////				  @Override
////				  public boolean onMenuItemActionExpand(MenuItem item) {
////					  return false;
////				  }
////
////				  @Override
////				  public boolean onMenuItemActionCollapse(MenuItem item) {
////					  finish();
////					  return false;
////				  }
////			  });
//			  MenuItemCompat.setActionView(menuitem, searchView);
//			  MenuItemCompat.expandActionView(menuitem);
//
////			  searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
////		  	  searchView.setSubmitButtonEnabled(true);
////		  	  searchView.setQueryHint("Type Someting...");
////			  searchView.setQuery(keyword, true);
////		      searchView.setFocusable(true);
////		      searchView.setIconified(false);
//		  }
//
//		  }catch(Exception e){
//			  //throw e;
//			  MainApplication.getInstance().trackException(e);
//		  }
		  /****
		   * Searchview blinding text change or submit 
		   * 
		   * **/
//		  SearchView.OnQueryTextListener onQueryTextListener = new SearchView.OnQueryTextListener() {
//			
//			@Override
//			public boolean onQueryTextSubmit(String arg0) {
//				Log.d("summit",arg0);
//				//startintent(arg0);
//				//delayforsearch(arg0);
//				return false;
//			}
//			
//			@Override
//			public boolean onQueryTextChange(String arg0) {
//				Log.d("change",arg0);
//				if(arg0.length() >= 3)
//					indexchange(arg0);
//				return false;
//			}
//		};
//			searchView.setOnQueryTextListener(onQueryTextListener);
			
			//searchView.setSuggestionsAdapter(array);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case android.R.id.home:	
			finish();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void getDataformsearch(){
		 keyword = getIntent().getExtras().getString("search");
		String url = ResourceUtil.getServiceUrl(this)+ getResources().getString(R.string.app_service_rest_search)+keyword;
		JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Listener<JSONArray>() {

			@Override
			public void onResponse(JSONArray arg0) {

				for(int n = 0; n < arg0.length(); n++)
				{
					try {
						JSONObject object = arg0.getJSONObject(n);
						try{
							JSONArray  arr = object.getJSONArray("CCTV");
							if(arr != null){
								for(int i = 0; i < arr.length(); i++){
									JSONObject obj2 = arr.getJSONObject(i);
									//sobj = ;
									Searchobj objt = new Searchobj(obj2,Searchactivity.this);
									sobj.add(objt);
									//Log.d("CCTV",objt.getRoi()+objt.getTitle());

								}
							}
						}catch(Exception e){

						}
						try{
							JSONArray  arr = object.getJSONArray("LiveStream");
							if(arr != null){
								for(int i = 0; i < arr.length(); i++){
									JSONObject obj2 = arr.getJSONObject(i);
									Searchobj mSearchobj =  new Searchobj(obj2,Searchactivity.this);
									sobj.add(mSearchobj);
									Log.d("LiveStream",obj2.toString());
								}
							}


						}catch(Exception e){

						}
						try{
							JSONArray  arr = object.getJSONArray("Blog");
							if(arr != null){
								for(int i = 0; i < arr.length(); i++){
									JSONObject obj2 = arr.getJSONObject(i);
									Searchobj mSearchobj =  new Searchobj(obj2,Searchactivity.this);
									sobj.add(mSearchobj);
									Log.d("BLOG",obj2.toString());
								}
							}
						}catch(Exception e){

						}


					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				onResume();
			}


		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				Log.d("jarray error",arg0.toString());

			}
		});
		MySingleton.getInstance(this).addToRequestQueue(jsonArrayRequest);
		Log.d("test", "11111111111111");

	}
	
	private void gotocctv(Searchobj searchobj){
		final Searchobj sobj = searchobj;
		searchobj.getObjCctv();
		
//		Toast.makeText(getApplicationContext(),"CCTV ID:"+cctv.getId(), Toast.LENGTH_SHORT).show();
//		cctv = result;
		String cacheROI = DataHelperAsyn.get("KEY_CACHED_ROI_CCTVS2");
		Log.d("search", cacheROI);
		if(cacheROI != null){
			listRoi.clear();
			listRoi = DataHelperAsyn.parseROI(cacheROI);
		}
		
		String url = ResourceUtil.getServiceUrl(this)
				+ getResources().getString(R.string.app_service_rest_rois);
		new RestServiceROIListCommand(this, url) {
			
			protected void onPostExecute(java.util.List<ROI> result) {
				synchronized (listRoi) {
					listRoi.clear();
					
					//skip ROI with no CCTV
					for(ROI roi: result){
						if(roi.sizeCCTV() > 0){
							listRoi.add(roi);
						}
					}
				}
				gotocctvs(sobj);
			}
		}.execute();

	}
	
	private void getvideoObj(Searchobj searchobj){
		
		String url = ResourceUtil.getServiceUrl(getApplicationContext()) + getResources().getString(R.string.app_service_rest_history) + searchobj.getId();
		Log.d("livestream", url);
		JsonObjectRequest jObjectRequest = new JsonObjectRequest(url, null, new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject arg0) {
				try {
					StreamHistory streamHistory = new StreamHistory(arg0);
						gotohistoryplay(streamHistory);
				} catch (JSONException e) {
					Log.d("json error", e.toString());
				}
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				Log.d("Volley", arg0.toString());
			}
		} );
	MySingleton.getInstance(getBaseContext()).addToRequestQueue(jObjectRequest);
	}
	
	private void gotohistoryplay(StreamHistory history){
		Log.d("test", "gogo");
		Intent intent = new Intent(this, LiveHistoryPlayer.class);
		intent.putExtra(StreamHistory.class.getName(), history);
		intent.putExtra(Searchactivity.class.getName(), false);
		
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}
	
	private void gotoblog(Searchobj searchobj){
		Intent intent = new Intent(this,Blogawebctivity.class);
		intent.putExtra(Blogawebctivity.class.getName(), searchobj.getLinkurl());
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		
	}

	private void indexchange(String index){
		
		
		//String url = "http://192.168.9.117/seeitlivethailand/api/search?keyword="+index;
		String url = ResourceUtil.getServiceUrl(this)+ getResources().getString(R.string.app_service_rest_search)+index;

//			url = URLEncoder.encode(url,"UTF-8");
			JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Listener<JSONArray>() {
				@Override
				public void onResponse(JSONArray arg0) {
					sobj.clear();
					datatoObject(arg0);
					onResume();
				}


			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError arg0) {
					Log.d("jarray error",arg0.toString());
					sobj.clear();
					list.setVisibility(ListView.GONE);
				}
			});
			MySingleton.getInstance(this).addToRequestQueue(jsonArrayRequest);



	}

	private void datatoObject(JSONArray arg0){
		for(int n = 0; n < arg0.length(); n++)
		{	
		    try {
				JSONObject object = arg0.getJSONObject(n);
				try{
					JSONArray  arr = object.getJSONArray("CCTV");
					if(arr != null){
						for(int i = 0; i < arr.length(); i++){
							JSONObject obj2 = arr.getJSONObject(i);
							//sobj = ;
							Searchobj objt = new Searchobj(obj2,Searchactivity.this);
							sobj.add(objt);
							//Log.d("CCTV",objt.getRoi()+objt.getTitle());
							
						}
					}
				}catch(Exception e){
					
				}
				try{
					JSONArray  arr = object.getJSONArray("LiveStream");
					if(arr != null){
						for(int i = 0; i < arr.length(); i++){
							JSONObject obj2 = arr.getJSONObject(i);
							Searchobj mSearchobj =  new Searchobj(obj2,Searchactivity.this);
							sobj.add(mSearchobj);
						//	Log.d("LiveStream",obj2.toString());
						}
					}
				
					
				}catch(Exception e){
					
				}
				try{
					JSONArray  arr = object.getJSONArray("Blog");
					if(arr != null){
						for(int i = 0; i < arr.length(); i++){
							JSONObject obj2 = arr.getJSONObject(i);
							Searchobj mSearchobj =  new Searchobj(obj2,Searchactivity.this);
							sobj.add(mSearchobj);
						//	Log.d("BLOG",obj2.toString());
						}
					}
				}catch(Exception e){
					
				}


			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void gotocctvs(Searchobj searchobj){
		ROI mRoi = null;
		for(int i=0; i < listRoi.size()-1; i++ ){		
			String name = listRoi.get(i).getKeyName();
			String namer = searchobj.getRoi();
			if(namer.equals(name)){
				mRoi = listRoi.get(i);
					break;
			}
	}
	List<CCTV> cctvs = new ArrayList<CCTV>();
	int pageposition = 0;
	if (mRoi != null) {
		cctvs.addAll(mRoi.getCCTVs());
		for(int i=0; i < cctvs.size() ;i++ ){
		   if(searchobj.getId().equals(cctvs.get(i).getId())){
			   Log.d("test", Integer.toString(i));
			   pageposition=i;
			   break;
		   }
		}	
	}
	
	Intent intent = new Intent(getApplicationContext(),CCTVLiveDetailActivity.class);
	intent.putExtra(ROI.class.getName(), mRoi);
	intent.putExtra("position", pageposition);
	intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	startActivity(intent);
	}
	
}
