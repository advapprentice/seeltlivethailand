package com.touchtechnologies.tit.search;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.touchtechnologies.tit.R;

public class SearchListadapter extends ArrayAdapter<Searchobj>{
	
	private Context context;
	protected ImageLoader imageLoader;
	protected DisplayImageOptions options;
	private List<Searchobj> msSearchobjs;
	



	public SearchListadapter(Context context,
			List<Searchobj> objects) {
		super(context, 0, objects);
		this.msSearchobjs = objects;
		this.context = context;
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder()
				.cacheInMemory(true)
				.cacheOnDisc(true)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.NONE)
				.showImageForEmptyUri(R.drawable.bg_cctvdefault)
				.showImageOnFail(R.drawable.bg_cctvdefault)
				.showImageOnLoading(R.drawable.bg_cctvdefault)
				.cacheInMemory(true)
				.displayer(new FadeInBitmapDisplayer(300))															// images
				.resetViewBeforeLoading(true)
				.build();
		imageLoader = ImageLoader.getInstance();
		
		
		
		
	}
	
	
	
	
	


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return msSearchobjs.size();
	}



	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	
		Viewholder mViewholder = new Viewholder();
		   if(convertView == null)
		    {
			   convertView = LayoutInflater.from(context).inflate(R.layout.custom_searchlistview, parent, false);
		            // initialize the elements
		        mViewholder.imgcover = (ImageView) convertView.findViewById(R.id.search_img_cover);
		        mViewholder.txtv_name = (TextView) convertView.findViewById(R.id.txtv_search_name);
		        mViewholder.type_name = (TextView) convertView.findViewById(R.id.txtv_search_typename);

		        convertView.setTag(mViewholder);
		    }
		    else
		    {
		        mViewholder = (Viewholder)convertView.getTag();
		    }
			
	
	
			String imageUrl;
		
		

		//		viewHolder.imgcover.setImageBitmap();
		Searchobj mSearchobj = getItem(position);
		if(mSearchobj.getModule().equals("CCTV")){
			 imageUrl = mSearchobj.getLiveurl();
			 imageLoader.displayImage(imageUrl, mViewholder.imgcover, options);
		}else if (mSearchobj.getModule().equals("LiveStream")) {
			imageUrl = mSearchobj.getSnapshot();
			imageLoader.displayImage(imageUrl, mViewholder.imgcover, options);
		}else if (mSearchobj.getModule().equals("Blog")) {
			imageUrl = mSearchobj.getUrlthumpnail();
			imageLoader.displayImage(imageUrl, mViewholder.imgcover, options);
		}
		
		mViewholder.txtv_name.setText(mSearchobj.getTitle());
		mViewholder.type_name.setText(mSearchobj.getModule());

		
		return convertView;
		
	}


	
	class Viewholder{
		ImageView imgcover;
		TextView txtv_name;
		TextView type_name;
	}

}
