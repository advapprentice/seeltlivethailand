package com.touchtechnologies.tit.search;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;

public class CustomAdapterSearch extends ArrayAdapter<Searchobj>{
	
	private List<Searchobj> item;
	private List<Searchobj> itemsuggesstion;
	
	public CustomAdapterSearch(Context context, int resource,
			List<Searchobj> objects) {
		super(context, resource, objects);
		
		this.item = objects;
		this.itemsuggesstion = objects;
	}

	

	@Override
	public Searchobj getItem(int position) {
		
		return item.get(position);
	}


	@Override
	public long getItemId(int position) {
		
		return position;
	}



	@Override
	public int getCount() {
		
		return item == null?0:item.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		return super.getView(position, convertView, parent);
	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		return super.getFilter();
	}
	

}
