package com.touchtechnologies.tit.comment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.opensource.pagerindicator.CircleBitmapDisplayer;

import com.touchtechnologies.dataobject.insightthailand.Comment;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.util.UserUtil;


import java.util.ArrayList;

/**
 * Created by TOUCH on 8/2/2559.
 */
public class CCTVCommentAdapter extends BaseAdapter {

    private  DisplayImageOptions options;
    private  ImageLoader imageLoader;
    private View.OnClickListener onClickListener;
    ArrayList<Comment> comments;
    Context context;

    public CCTVCommentAdapter(Context context,View.OnClickListener onClickListener) {
        this.context = context;
        this.onClickListener = onClickListener;
        imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPoolSize(3)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCacheSize(4194304)
                .memoryCache(new WeakMemoryCache())
                .defaultDisplayImageOptions(options).build();

        imageLoader.init(config);
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .imageScaleType(ImageScaleType.NONE)
                .showImageForEmptyUri(R.drawable.ic_new_imgaccount)
                .showImageOnFail(R.drawable.ic_new_imgaccount)
                .showImageOnLoading(R.drawable.ic_new_imgaccount)
                .cacheInMemory(true)
                .displayer(new FadeInBitmapDisplayer(1000))
                        //	.displayer(new RoundedBitmapDisplayer((int) 125.5f))
                .displayer(new CircleBitmapDisplayer())
                .build();

    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public Object getItem(int position) {
        return comments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_cctv_comment, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imgProfile = (ImageView) convertView.findViewById(R.id.imageView_comment_cctv);
            viewHolder.txtCCTVComment = (TextView) convertView.findViewById(R.id.txt_comment_content);
            viewHolder.txtname = (TextView) convertView.findViewById(R.id.txt_comment_name);
            viewHolder.txt_lastname = (TextView) convertView.findViewById(R.id.txt_comment_last_name);
            viewHolder.imgv_bin = (ImageView)convertView.findViewById(R.id.imageView_bin_comment);
            viewHolder.imgv_bin.setOnClickListener(onClickListener);
            viewHolder.imgv_bin.setTag(comments.get(position));
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
            if(UserUtil.isLoggedIn(context)) {
                if(comments.get(position).getUser().getId().equals(UserUtil.getUser(context).getId()))
                    viewHolder.imgv_bin.setVisibility(View.VISIBLE);
                else
                    viewHolder.imgv_bin.setVisibility(View.INVISIBLE);
            }else {
                    viewHolder.imgv_bin.setVisibility(View.INVISIBLE);
            }
            imageLoader.displayImage(comments.get(position).getUser().getAvatar(),viewHolder.imgProfile,options);
            viewHolder.txtname.setText(comments.get(position).getUser().getFirstName());
            viewHolder.txt_lastname.setText(comments.get(position).getUser().getLastName());
            viewHolder.txtCCTVComment.setText(comments.get(position).getCommentContent());
        return convertView;
    }



    class ViewHolder {
        ImageView imgProfile;
        TextView txtname;
        TextView txt_lastname;
        TextView txtCCTVComment;
        ImageView imgv_bin;
    }

}
