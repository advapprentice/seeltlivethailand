package com.touchtechnologies.tit.comment;


import java.util.HashMap;

import org.apache.http.HttpStatus;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.RequestListCommentCommand;
import com.touchtechnologies.command.insightthailand.RequestcommentCommand;
import com.touchtechnologies.dataobject.Hospitality;
import com.touchtechnologies.dataobject.Restaurant;
import com.touchtechnologies.dataobject.insightthailand.Comment;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.GalleryAdapter;
import com.touchtechnologies.tit.GalleryFragment;
import com.touchtechnologies.tit.LocationAwareFragment;
import com.touchtechnologies.tit.POIUpdateListner;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.hotel.HotelAddressMapinfoAdapter;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

@SuppressLint("ValidFragment")
public class CommentRestaurant extends LocationAwareFragment  implements OnClickListener, POIUpdateListner ,OnMapClickListener,OnInfoWindowClickListener, OnMapReadyCallback {
private	TextView texthotelname,adddress,website,tel,description,txtrating,open,close;
private EditText edittextcomment;
private Button btnwrite,btnshares,btnsubmit,btncancel;
private View viewwrite,viewcomment;
private Restaurant poi;
private GalleryAdapter  galAdapter;
private GoogleMap googleMap;
private int mapZoomLevel = 24;
private HashMap<Marker, Restaurant> hMarkerpois;
private HotelAddressMapinfoAdapter adapterInfo;
double longitude ;
double latitude;
private String poiid;
private CommentAdapter listAdapter;
private int filtersPage = 1;
private View imgView;
public CommentRestaurant(Restaurant pois)  {
	this.poi = pois;
}

@Override
public View onCreateView(LayoutInflater inflater,
		@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
	View view = inflater.inflate(R.layout.activity_restaurant, container, false);
		
	
	
	listAdapter = new CommentAdapter(getActivity());

	View headerView = inflater.inflate(R.layout.activity_comment, null);
	ListView listView = (ListView)view.findViewById(R.id.listViewRestaurant);
	listView.setAdapter(listAdapter);
	
	listView.addHeaderView(headerView, null, false);
	listView.setOnScrollListener(new AbsListView.OnScrollListener() {

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {

		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {

			View headerView = view.findViewById(R.id.header);

			final float mTop = -headerView.getTop();
			float height = headerView.getHeight();
			if (mTop > height) {
				// ignore
				return;
			}
			imgView = headerView.findViewById(R.id.viewlistComment);
			

			
			texthotelname =(TextView)view.findViewById(R.id.txthotelname); 
			adddress = (TextView)view.findViewById(R.id.txtaddress); 
			website = (TextView)view.findViewById(R.id.txtewongnai); 
			tel = (TextView)view.findViewById(R.id.txttel); 
			txtrating = (TextView)view.findViewById(R.id.txtrating); 
			description = (TextView)view.findViewById(R.id.txtdescription); 
			edittextcomment= (EditText)view.findViewById(R.id.editdetail);  
			
			open = (TextView)view.findViewById(R.id.textopen); 
			close= (TextView)view.findViewById(R.id.textClose);
			
			btnwrite = (Button)view.findViewById(R.id.btnreview); 
			btnshares = (Button)view.findViewById(R.id.btnshare);
			btnsubmit =(Button)view.findViewById(R.id.btnsubmit);
			btncancel =(Button)view.findViewById(R.id.cancel);
			
			viewwrite= (View)view.findViewById(R.id.viewwrite);
			viewcomment= (View)view.findViewById(R.id.viewcomment);
			
			
			imgView.setTranslationY(mTop / 2f);

		}
	});
	
			btnwrite.setOnClickListener(this);
			btnshares.setOnClickListener(this);
			btnsubmit.setOnClickListener(this);
			btncancel.setOnClickListener(this);
			
			if(poi.getPhoneNumberAt(0).getNumber().isEmpty()||poi.getWebUrl().isEmpty()||poi.getDescription().isEmpty()){
	 			website.setText("No Data");
		 		tel.setText("No Data");
		 		description.setText("No Data");
		 		texthotelname.setText(poi.getName());
		 		adddress.setText(poi.getDisplayAddress());
		 	
	 		}else{
	 			
			texthotelname.setText(poi.getName());
	 		adddress.setText(poi.getDisplayAddress());
	 		website.setText(poi.getWebUrl()== null?"":poi.getWebUrl());
	 		tel.setText(poi.getPhoneNumberAt(0).getNumber());
	 		description.setText(poi.getDescription());
	 		}
			
			poiid = poi.getIdHotel();
			
		
			/*
			 * Get Restaurant POI
			 */
			requestcomment();
			
		
	 		
		return view;
	}



	private void requestcomment() {
		String url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_rest_poi)+poiid+"/comments?filtersPage="+filtersPage+"&filterLimit=50";
			new RequestListCommentCommand(getActivity(), url){
				protected void onPostExecute(java.util.List<Comment> result) {
					listAdapter.setData(result);
				}
		
			}.execute();
	}



@Override
public void onResume() {
	super.onResume();

	try {
		if (googleMap == null) {

			SupportMapFragment MapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
			MapFragment.getMapAsync(this);
	//		googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hotel.getLocation(),getResources().getInteger(R.integer.map_zoom_default)));
  //		googleMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(hotel.getLatitude(), hotel.getLongitude()) , 12.0f) );
			googleMap.setMyLocationEnabled(true);
			googleMap.setOnMapClickListener(this);
			googleMap.setOnInfoWindowClickListener(this);	
			googleMap.setInfoWindowAdapter(adapterInfo);
			
		}
		googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		googleMap.addMarker(new MarkerOptions()
				.position(new LatLng(poi.getLatitude(), poi.getLongitude()))
				.title(poi.getName())
				.snippet(poi.getDisplayAddress())
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_resdetail)));
			googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(poi.getLatitude(), poi.getLongitude()),12.0f));
	
		
		
	} catch (Exception e) {
		e.printStackTrace();
	}

}



@Override
public void onConnected(Bundle dataBundle) {
//	if (mLocationClient != null
//			&& googleMap != null) {
//		location = mLocationClient.getLastLocation();
//
		if(location != null){
			LatLng myLastLatLon = new LatLng(location.getLatitude(), location.getLongitude());
			CameraUpdate camUpdate = CameraUpdateFactory.newLatLngZoom(myLastLatLon,mapZoomLevel);
			googleMap.animateCamera(camUpdate);

		}
//	}
}	

public void setMapZoomLevel(int mapZoomLevel) {
	this.mapZoomLevel = mapZoomLevel;
}


public void setLocation(Location location) {
	this.location = location;
	
	if(googleMap != null){
		//move map to last know location if possible when started
		LatLng myLastLatLon = new LatLng(location.getLatitude(), location.getLongitude());
		CameraUpdate camUpdate = CameraUpdateFactory.newLatLngZoom(myLastLatLon, mapZoomLevel); 
		
		googleMap.animateCamera(camUpdate);
	}
}

@Override
public void onLocationChanged(Location location) {
	super.onLocationChanged(location);
	//mLocationClient.disconnect();
}


	
	@Override
	public void onMapClick(LatLng point) {
		MarkerOptions mark = new MarkerOptions();
		googleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
		mark.position(point);
		
	
	}
	

	@Override
	public void onInfoWindowClick(Marker mark) {
		Uri gmmIntentUri = Uri.parse("google.navigation:"+poi.getLatitude()+","+poi.getLongitude())
				.buildUpon()
	            .appendQueryParameter("q",poi.getName())
	            .build();
		//		Uri gmmIntentUri = Uri.parse("google.navigation:q="+hotel.getName()+","+hotel.getLatitude()+","+hotel.getLongitude()+"&mode=b");
				Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
				mapIntent.setPackage("com.google.android.apps.maps");
				startActivity(mapIntent);
	}


@Override
public void poiUpdated(Hospitality pois) {
	 this.poi = (Restaurant)pois;
	// Toast.makeText(getActivity(), "test"+poi.getPaycredit(), Toast.LENGTH_SHORT).show();
	 //update view
	 

			galAdapter = new GalleryAdapter(getActivity(), R.layout.item_gallery_preview);
			galAdapter.setData(poi.getImageGallery());

			GalleryFragment fragment = new GalleryFragment(galAdapter);
			FragmentManager fManager = getFragmentManager();
			fManager.beginTransaction().add(R.id.fragmentGalleryContainer, fragment).commit();
			
	 		if(poi.getPhoneNumberAt(0).getNumber().isEmpty()||poi.getWebUrl().isEmpty()||poi.getDescription().isEmpty()){
	 			website.setText("No Data");
		 		tel.setText("No Data");
		 		description.setText("No Data");
		 		texthotelname.setText(poi.getName());
		 		adddress.setText(poi.getDisplayAddress());
		 	
	 		}else{
	 			
			texthotelname.setText(poi.getName());
	 		adddress.setText(poi.getDisplayAddress());
	 		website.setText(poi.getWebUrl()== null?"":poi.getWebUrl());
	 		tel.setText(poi.getPhoneNumberAt(0).getNumber());
	 		description.setText(poi.getDescription());
	 		}
			String create = poi.getOpentime()== null?"":poi.getOpentime();
			String timecreate = create.substring(0,5);
			open.setText(timecreate);
						
			String update = poi.getClosetime()== null?"": poi.getClosetime();
			String timeupdate = update.substring(0,5);
			close.setText("-"+timeupdate);
	
	 		int rating = poi.getRatingInt();
	 		String stringdouble = Integer.toString(rating);
	 		txtrating.setText(stringdouble);
	
	 	onResume(); 
	}

@Override
public void onClick(View v) {
	switch (v.getId()) {
		case R.id.btnreview:
			viewcomment.setVisibility(View.VISIBLE);
			viewwrite.setVisibility(View.GONE);
			edittextcomment.setText("");
			break;
		case R.id.btnshare:
			viewcomment.setVisibility(View.GONE);
			viewwrite.setVisibility(View.VISIBLE);
			break;
		case R.id.btnsubmit:
			
			if (edittextcomment.getText().toString().isEmpty()) {
				edittextcomment.setError(getText(R.string.text_empty));
			} else {
				viewcomment.setVisibility(View.GONE);
				viewwrite.setVisibility(View.VISIBLE);
			
				comment();
			
			}
			
			break;
		case R.id.cancel:
			viewcomment.setVisibility(View.GONE);
			viewwrite.setVisibility(View.VISIBLE);
			break;
		

	}
	
}




Comment PutStreamConnection() {
	Comment comment = new Comment();
	comment.setCommentContent(edittextcomment.getText().toString());

	return comment;
}



private void comment() {

	RequestcommentCommand getCommand = new RequestcommentCommand(getActivity());
	
	getCommand.setCommentcontent(PutStreamConnection());
	getCommand.setUser(UserUtil.getUser(getActivity()));
	
	new GetStreamServiceTask(getActivity()).execute(getCommand);

}

	@Override
	public void onMapReady(GoogleMap googleMapv) {
		googleMap = googleMapv;
	}

	class GetStreamServiceTask extends CommonServiceTask {

public GetStreamServiceTask(FragmentActivity context) {
	super(context, context.getString(R.string.wait), null,null);
}


@Override
protected ServiceResponse doInBackground(Command... params) {
	
	ServiceConnector srConnector = new ServiceConnector(ResourceUtil.getServiceUrl(context)+getString(R.string.app_service_rest_poi)+poiid+getString(R.string.app_service_rest_comment));
	ServiceResponse serviceResponse = srConnector.doAsynPost(params[0],  false);
	
	try {
		int code = serviceResponse.getCode();
		if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
			JSONObject json = new JSONObject(serviceResponse.getContent().toString());
		    Comment	comment = new Comment(json);
			
		}
	} catch (Exception e) {
		Log.e(AppConfig.LOG, "doInBackground error", e);
	}

	return serviceResponse;
}

@Override
protected void onPostExecute(ServiceResponse result) {
	super.onPostExecute(result);

	dialog.dismiss();
	try {
		if (result.getCode() == ServiceResponse.SUCCESS|| result.getCode() == HttpStatus.SC_OK) {
		//	Toast.makeText(context, R.string.post_success, Toast.LENGTH_LONG).show();

			context.setResult(Activity.RESULT_OK);
			requestcomment();
			

		} else {
			Toast.makeText(context, getText(R.string.up_not_connectstream)+"!\r\n" + result.getMessage(),
					Toast.LENGTH_LONG).show();

		}
	} catch (Exception e) {
		Toast.makeText(context, "ERROR!\r\n" + e.getMessage(),
				Toast.LENGTH_LONG).show();
		Log.e(AppConfig.LOG, "process response error", e);
	}
}
}






}
