package com.touchtechnologies.tit.comment;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.opensource.pagerindicator.CircleBitmapDisplayer;

import com.touchtechnologies.dataobject.insightthailand.Comment;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.json.JSONUtil;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by TOUCH on 3/3/2559.
 */
public class LiveHistoryComment extends SettingActionbarActivity implements View.OnClickListener{


    private ArrayList<Comment> comments ;
    private ListView listcomment;
    private boolean checkcansend = false;
    private TextView txt_send_comment;
    private EditText editText_addComment;
    private ImageView user_avatar;
    private CCTVCommentAdapter cctvCommentAdapter;
    private View btn_sendcomment;
    private View list_empty;
    private StreamHistory livestream;
    private DisplayImageOptions options;
    private ImageLoader imageLoader;
    private final Map<String, String> mHeaders = new ArrayMap<String, String>();
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cctvcomment);

        imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPoolSize(3)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCacheSize(4194304)
                .memoryCache(new WeakMemoryCache())
                .defaultDisplayImageOptions(options).build();

        imageLoader.init(config);
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .imageScaleType(ImageScaleType.NONE)
                .showImageForEmptyUri(R.drawable.ic_new_imgaccount)
                .showImageOnFail(R.drawable.ic_new_imgaccount)
                .showImageOnLoading(R.drawable.ic_new_imgaccount)
                .cacheInMemory(true)
                .displayer(new FadeInBitmapDisplayer(1000))
    //	.displayer(new RoundedBitmapDisplayer((int) 125.5f))
    .displayer(new CircleBitmapDisplayer())
            .build();
    if(comments!=null){
        comments.clear();
    }
    editText_addComment = (EditText)findViewById(R.id.editText_addcomment);
    mTitle = "Comments";
    btn_sendcomment = findViewById(R.id.view_send_comment);
    btn_sendcomment.setOnClickListener(this);
    txt_send_comment = (TextView)findViewById(R.id.textView_send_comment);
    livestream = (StreamHistory) getIntent().getSerializableExtra(StreamHistory.class.getName());

    list_empty = findViewById(R.id.view_empty);
    list_empty.setVisibility(View.GONE);
    comments = livestream.getComments();
     if(comments ==null){
         onRefersh();

     }
    editText_addComment.addTextChangedListener(new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(count > 0){
                if(!checkcansend){
                    btn_sendcomment.setClickable(true);
                    checkcansend = true;
                    txt_send_comment.setTextColor(Color.BLUE);
                }

            }else {
                btn_sendcomment.setClickable(false);
                checkcansend = false;
                txt_send_comment.setTextColor(Color.parseColor("#a7a4a4"));
            }

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(s.toString().trim().length() > 0){
                if(!checkcansend){
                    btn_sendcomment.setClickable(true);
                    checkcansend = true;
                    txt_send_comment.setTextColor(Color.BLUE);
                }
            }else {
                btn_sendcomment.setClickable(false);
                checkcansend = false;
                txt_send_comment.setTextColor(Color.parseColor("#a7a4a4"));
            }

        }
    });

    listcomment = (ListView) findViewById(R.id.listview_cctvcomment);

    user_avatar = (ImageView) findViewById(R.id.imageView_user_comment);
    if(UserUtil.isLoggedIn(this)){
        imageLoader.displayImage(UserUtil.getUser(this).getAvatar(),user_avatar,options);
        mHeaders.put("X-TIT-ACCESS-TOKEN", UserUtil.getUser(LiveHistoryComment.this).getToken());
    }


}

    @Override
    protected void onResume() {
        super.onResume();
        if (comments == null) {

        }else {
            TextView textView = (TextView) findViewById(R.id.textView_headcomment);
            cctvCommentAdapter = new CCTVCommentAdapter(this,this);
            cctvCommentAdapter.setComments(comments);
            listcomment.setAdapter(cctvCommentAdapter);
            textView.setText(comments.size()+" Comment");
            cctvCommentAdapter.notifyDataSetChanged();
        }
    }



    private void deleteCCTVcomment(Comment comment){
        String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_stream)
                + livestream.getId() + getResources().getString(R.string.app_service_comment)+"/"+comment.getId();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                if(JSONUtil.getInt(jsonObject, "status") == 0) {
                    onRefersh();
                }else {
                    Toast.makeText(LiveHistoryComment.this, JSONUtil.getString(jsonObject, "message"), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return mHeaders;
            }
        };
        MainApplication.getInstance().addToRequestQueue(jsonObjectRequest);

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void addCommentfromtextbox(){
        if(checklogin()) {
            // final Map<String, String> mHeaders = new ArrayMap<String, String>();
            //  mHeaders.put("X-TIT-ACCESS-TOKEN", UserUtil.getUser(CCTVCommentActivity.this).getToken());
            String comment = editText_addComment.getText().toString().trim();
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            if (!comment.isEmpty() && comment != null) {
                editText_addComment.setText("");
                String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_stream) + livestream.getId() + getResources().getString(R.string.app_service_comment);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("comment_content", comment);
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            onRefersh();

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                        }
                    }){
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            return mHeaders;
                        }
                    };

                    MainApplication.getInstance().addToRequestQueue(jsonObjectRequest);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }else {
            Toast.makeText(this,"please login before comment",Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checklogin(){
        return  UserUtil.isLoggedIn(this);
    }

    private void onRefersh(){
        String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_stream)
                + livestream.getId() + getResources().getString(R.string.app_service_comment);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,jsonArrayListener, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        MainApplication.getInstance().addToRequestQueue(jsonArrayRequest);
    }

private Response.Listener<JSONArray> jsonArrayListener = new Response.Listener<JSONArray>() {
    @Override
    public void onResponse(JSONArray jsonArray) {
        //Toast.makeText(CCTVLiveDetailActivity.this,jsonArray.toString(),Toast.LENGTH_LONG).show();
        if(jsonArray.length()==0){
            list_empty.setVisibility(View.VISIBLE);

        }else {
            list_empty.setVisibility(View.GONE);
        }


        if(comments != null)
            comments.clear();
        else
            comments =new ArrayList<Comment>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Comment comment = new Comment(jsonObject);
                comments.add(comment);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        onResume();

    }
};

    @Override
    protected void onDestroy() {
        super.onDestroy();
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.view_send_comment:
                addCommentfromtextbox();
                break;
            case R.id.imageView_bin_comment:
                showDialogDelete((Comment)v.getTag());
                break;
            default:
                break;
        }
    }

    private void showDialogDelete(final Comment mComment){
        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(LiveHistoryComment.this);

        mBuilder.setTitle("Delete Comment");
        mBuilder.setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        mBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteCCTVcomment(mComment);
                dialog.dismiss();
            }
        });
        AlertDialog mAlertDialog = mBuilder.create();
        mAlertDialog.show();

    }
}
