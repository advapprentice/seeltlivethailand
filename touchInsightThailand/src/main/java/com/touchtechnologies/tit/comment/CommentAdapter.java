package com.touchtechnologies.tit.comment;


import java.util.List;
import java.util.Timer;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.touchtechnologies.dataobject.insightthailand.Comment;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.tit.R;

public class CommentAdapter extends BaseAdapter {
	private Context context;
	private Runnable runnable;
	protected ImageLoader imageLoader;
	protected DisplayImageOptions options;
	Timer t;
	private List<Comment> arrcomment;
	private LiveStreamingChannel cctvs;
	String url;
	public CommentAdapter(Context context) {
		this.context = context;
		

	}
	public void setData(List<Comment> arrcomment){
		this.arrcomment = arrcomment;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return arrcomment == null?0:arrcomment.size();
	}
	
	@Override
	public Comment getItem(int position) {
		return arrcomment.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	public void remove(Comment comment){
		boolean success = arrcomment.remove(comment);
		if(success){
			notifyDataSetChanged();
		}
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		
		if(convertView == null){
			convertView = LayoutInflater.from(context).inflate(R.layout.item_comment, parent, false);
			viewHolder = new ViewHolder();
	
			viewHolder.txtbyname = (TextView)convertView.findViewById(R.id.txtbyname);
			viewHolder.txtdescription = (TextView)convertView.findViewById(R.id.txtcomment);
			viewHolder.txtdate = (TextView)convertView.findViewById(R.id.txtdatetime);
			
			//   Toast.makeText(context, "position"+position, Toast.LENGTH_LONG).show();
		   
			

			convertView.setTag(viewHolder);
			
			
		}else{
			viewHolder = (ViewHolder)convertView.getTag();
		}
		
		if(getItem(position).getUser()!=null){
			viewHolder.txtbyname.setText(getItem(position).getUser().getFirstName()); 
		//	viewHolder.txtbyname.setText(getItem(position).getUser().getFirstName()== null ? "" : getItem(position).getUser().getFirstName());
		}else{
			viewHolder.txtbyname.setText("-"); 
		}
	   	viewHolder.txtdescription.setText(getItem(position).getCommentContent());
	  	viewHolder.txtdate.setText(getItem(position).getCreatedDatetime()> 0?getItem(position).getCreateDateString():"-");
			
			
	  	
		Log.i("LiveActivity", "reloaded");

	
		return convertView;
	}



	class ViewHolder{
		TextView txtdate;
		TextView txtbyname;
		TextView txtdescription;
		
	}
	
	
	
}