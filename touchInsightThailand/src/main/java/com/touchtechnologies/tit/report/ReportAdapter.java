package com.touchtechnologies.tit.report;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.touchtechnologies.dataobject.insightthailand.CategoryLiveStream;
import com.touchtechnologies.dataobject.insightthailand.Report;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.tit.R;

import java.util.List;
import java.util.Timer;

/**
 * Created by TouchICS on 5/31/2016.
 */
public class ReportAdapter extends BaseAdapter  {

    private Context context;
    private Runnable runnable;
    protected ImageLoader imageLoader;
    protected DisplayImageOptions options;
    private View.OnClickListener listener;
    Timer t;
    private List<Report> arrReport;
    private StreamHistory history;
    private boolean selectChoice =false;
    private int posi;
    public ReportAdapter(Context context) {
        this.context = context;


        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .cacheInMemory(true)
                .handler(new Handler())
                .build();
        imageLoader = ImageLoader.getInstance();


    }
    public void setData(List<Report> arrmyhistory){
        this.arrReport = arrmyhistory;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return arrReport == null ? 0 : arrReport.size();
    }
    @Override
    public boolean hasStableIds() {
        return true;
    }


    public Report getItem(int position) {
        return arrReport.get(position);
    }


    public long getItemId(int position) {
        return position;
    }

    public void setRadioButtons(boolean visible,int posi){
        this.selectChoice = visible;
        this.posi = posi;
        notifyDataSetChanged();
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_report, parent, false);
            viewHolder = new ViewHolder();

            viewHolder.txtReport  = (TextView)convertView.findViewById(R.id.txtExplanation);
            viewHolder.imgChoice = (ImageView) convertView.findViewById(R.id.imgChoice);
            viewHolder.imgSelectChoice = (ImageView) convertView.findViewById(R.id.imgSelectChoice);

            convertView.setTag(viewHolder);

        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.txtReport.setText(getItem(position).getExplanation());

      //   imageLoader.displayImage(getItem(position).getIconCoverCategory(), viewHolder.imageView, options);
      //   viewHolder.viewimage.setBackgroundColor(Color.argb(180, position * 50, position * 107, position * 181));

        viewHolder.txtReport.setTag(arrReport.get(position));

        if(posi == position&&selectChoice){
            viewHolder.imgChoice.setVisibility(!selectChoice?View.VISIBLE:View.GONE);
            viewHolder.imgSelectChoice.setVisibility(!selectChoice?View.GONE:View.VISIBLE);
        }else{
            viewHolder.imgChoice.setVisibility(View.VISIBLE);
            viewHolder.imgSelectChoice.setVisibility(View.GONE);
        }


     //   radioButton.setTag(arrReport.get(position));


    return convertView;
    }


    class ViewHolder{
        ImageView imageView;
        TextView txtReport,textCountCat;
        View viewCategory;
        ImageView imgChoice,imgSelectChoice;

    }



}