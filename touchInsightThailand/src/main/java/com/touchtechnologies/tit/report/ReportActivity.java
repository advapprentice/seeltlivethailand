package com.touchtechnologies.tit.report;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.touchtechnologies.animate.button.CircularProgressButton;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.ReportCommand;
import com.touchtechnologies.command.insightthailand.RequestLivestreamCommand;
import com.touchtechnologies.command.insightthailand.RestServiceCategoryListCommand;
import com.touchtechnologies.command.insightthailand.RestServiceReportListCommand;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.insightthailand.CategoryLiveStream;
import com.touchtechnologies.dataobject.insightthailand.Comment;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamConnection;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.dataobject.insightthailand.Report;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.interfacesupport.InterfaceCommand;
import com.touchtechnologies.tit.live.category.ListCategoryAdapter;
import com.touchtechnologies.tit.login.LoginsActivity;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TouchICS on 5/31/2016.
 */
public class ReportActivity extends SettingActionbarActivity implements InterfaceCommand ,AdapterView.OnItemClickListener,View.OnClickListener {
    private static final String KEY_RESOURCES = "key_resources";
    private static final String KEY_CCTVS= "cctvs";
    private static final String KEY_STREAMINGS = "streamings";
    private static final String KEY_STREAMLIVE = "streamlives";
    private static final String KEY_USER = "users";
    private static final String KEY_COMMENT = "comments";
    private StreamHistory history;
    private View viewLoading;
    private ListView listView;
    private ReportAdapter adapter;
    private String resourcesName;
    private List<Report> arrreport;
    private Report report;
    private CircularProgressButton btnReport;
    private String idResources;
    private CCTV cctv;
    private User user;
    private Comment comment;
    private LiveStreamingChannel liveStreamingChannel;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        mTitle = "Improper Report";


        resourcesName = getIntent().getExtras().getString(KEY_RESOURCES);

        switch (resourcesName){
            case KEY_CCTVS:
                cctv = (CCTV)getIntent().getSerializableExtra(CCTV.class.getName());
                idResources = cctv.getId();

                break;
            case KEY_STREAMINGS:
                history = (StreamHistory) getIntent().getSerializableExtra(StreamHistory.class.getName());
                idResources = history.getId();

                break;
            case KEY_STREAMLIVE:
                liveStreamingChannel = (LiveStreamingChannel) getIntent().getSerializableExtra(LiveStreamingChannel.class.getName());
                idResources = liveStreamingChannel.getLiveStream().getId();

                break;
            case KEY_USER:
                user = (User) getIntent().getSerializableExtra(User.class.getName());
                idResources = user.getId();

                break;
            case KEY_COMMENT:
                comment = (Comment)getIntent().getSerializableExtra(Comment.class.getName());
                idResources = comment.getId();
                break;
        }

        adapter = new ReportAdapter(this);
        listView = (ListView) findViewById(R.id.list);
        btnReport = (CircularProgressButton) findViewById(R.id.btnReport);
        viewLoading =(View)findViewById(R.id.viewLoading);

        btnReport.setOnClickListener(this);

        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setOnItemClickListener(this);
        getCommand();

    }



    @Override
    public void getCommand() {
        String url = ResourceUtil.getServiceUrl(this) + "/" + resourcesName + getResources().getString(R.string.app_service_rest_cimproperReportReasons);
        Log.i(AppConfig.LOG, "URL :" + url);
        new RestServiceReportListCommand(this, url) {
            protected void onPostExecute(List<Report> result) {
                arrreport = result;
                adapter.setData(arrreport);
                //	pager.setCurrentItem(1);
                adapter.notifyDataSetChanged();
                viewLoading.setVisibility(View.GONE);
            }

        }.execute();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        report = adapter.getItem(position);
        adapter.setRadioButtons(true, position);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnReport:

                if(UserUtil.isLoggedIn(this)){

                    if (btnReport.getProgress() == 0) {
                        if (report == null) {
                            Toast.makeText(getApplicationContext(), "Please Select to report ...", Toast.LENGTH_SHORT).show();
                        } else {
                            postReport();
                        }
                        btnReport.setIndeterminateProgressMode(true);
                    } else {
                        btnReport.setProgress(0);
                    }

                }else{

                    Intent intent = new Intent(this, LoginsActivity.class);
                    startActivity(intent);

                }



                break;


        }
    }

    Report putCommand() {
        Report reportSelect = new Report();
        reportSelect.setId(report.getId());
        return report;
    }

    public void postReport() {

        ReportCommand putCommand = new ReportCommand(getApplicationContext());
        putCommand.setReport(putCommand());
        Log.i("Command", "Content:" + putCommand.toString());
        new GetSelectServiceTask(this).execute(putCommand);

    }

    class GetSelectServiceTask extends CommonServiceTask {
        public GetSelectServiceTask(FragmentActivity context) {
            super(context);
        }

        @Override
        protected ServiceResponse doInBackground(Command... params) {
            ServiceConnector srConnector = new ServiceConnector(ResourceUtil.getServiceUrl(context) + "/"+resourcesName+"/"+idResources+getResources().getString(R.string.app_service_rest_improperReports));
            Log.i("Url", ":"+srConnector);
            User user = UserUtil.getUser(getApplicationContext());
            List<NameValuePair> headers = new ArrayList<NameValuePair>();
            headers.add(new BasicNameValuePair("Content-Type", "application/json"));
            headers.add(new BasicNameValuePair("X-TIT-ACCESS-TOKEN", user.getToken()));
            ServiceResponse serviceResponse = srConnector.doAsynPost(params[0], true, headers);
            Log.i("headers", "URL :" + headers);
            try {
                int code = serviceResponse.getCode();
                if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
                    JSONObject json = new JSONObject(serviceResponse.getContent().toString());

                    Log.d("TIT", "Response" + json);

                }

            } catch (Exception e) {
                Log.e("TIT", "doInBackground error", e);

            }

            return serviceResponse;
        }

        @Override
        protected void onPostExecute(ServiceResponse result) {
            super.onPostExecute(result);

            try {
                switch (result.getCode()) {
                    case ServiceResponse.SUCCESS:
                    case HttpStatus.SC_OK:
                        context.setResult(Activity.RESULT_OK);
                        simulateSuccessProgress(btnReport);

                        break;
                    case 2://restore token failed
                        //TODO logout

                        break;


                }
            } catch (Exception e) {
                // Toast.makeText(context, "ERROR!\r\n" + e.getMessage(),	Toast.LENGTH_LONG).show();
                Log.e("TIT", "process response error", e);

                simulateErrorProgress(btnReport);
            }
        }


    }


    private void simulateSuccessProgress(final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 100);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                button.setProgress(value);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });

            }
        });
        widthAnimation.start();

    }

    private void simulateErrorProgress(final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 99);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                button.setProgress(value);
                if (value == 99) {
                    button.setProgress(-1);
                }
            }
        });
        widthAnimation.start();
    }



}



