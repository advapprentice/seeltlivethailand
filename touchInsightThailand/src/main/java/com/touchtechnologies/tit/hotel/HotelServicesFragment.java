package com.touchtechnologies.tit.hotel;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;

import com.touchtechnologies.dataobject.Hospitality;
import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.tit.POIUpdateListner;

@SuppressLint("ValidFragment")
public class HotelServicesFragment extends Fragment implements POIUpdateListner{
	private Hotel hotel;
	
	public HotelServicesFragment(Hotel hotel) {
		this.hotel = hotel;
	}
	
	@Override
	public void poiUpdated(Hospitality poi) {
		 this.hotel = (Hotel)poi;
		 
		 //update view
		 onResume(); 
	}
}
