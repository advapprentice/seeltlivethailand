package com.touchtechnologies.tit.hotel;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.touchtechnologies.command.insightthailand.RestServiceGetHotelCommand;
import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.tit.POIUpdateListner;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.search.SearchActionbarActivity;
import com.touchtechnologies.tit.util.ResourceUtil;

public class HotelActivity extends SearchActionbarActivity implements OnClickListener{
	Hotel hotel;
	POIUpdateListner poiUpdateListeners;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		hotel = (Hotel)getIntent().getSerializableExtra(Hotel.class.getName());
		
		mTitle = hotel.getName();
		
		setContentView(R.layout.activity_hotel);

		HotelSummaryFragment hsFragment = new HotelSummaryFragment(hotel);
		poiUpdateListeners = hsFragment;
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.container, hsFragment).commit();
		
		//register hotel menus
		findViewById(R.id.summary).setOnClickListener(this);
		findViewById(R.id.checkprice).setOnClickListener(this);
		findViewById(R.id.detail).setOnClickListener(this);
		findViewById(R.id.map).setOnClickListener(this);
		
		//TODO call asyn to get more hotel information
		
		   final String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_poi_byID)+hotel.getIdHotel();
			 new RestServiceGetHotelCommand(this, url){
							
				protected void onPostExecute(Hotel result) {
					hotel = result;

					if(poiUpdateListeners != null){
						poiUpdateListeners.poiUpdated(result);
					}
				};
			}.execute();


			Log.e("url","=="+url);
		
		
		//doInBackground -> hotel = new Hotel(json)
		//onPostExecute -> update fragment with new hotel object
	}
	
	/**
	 * Set fragment when a hotel menu is clicked
	 */
	@Override
	public void onClick(View v) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		
		switch(v.getId()){
		case R.id.summary:
			HotelSummaryFragment hsFragment = new HotelSummaryFragment(hotel);
			poiUpdateListeners = hsFragment;
			fragmentManager.beginTransaction().replace(R.id.container, hsFragment).commit();
			break;
		case R.id.checkprice:
//			fragmentManager.beginTransaction().replace(R.id.container, new HotelPriceFragment(hotel)).commit();
			
			HotelPriceFragment hpFragment = new HotelPriceFragment(hotel);
			poiUpdateListeners = hpFragment;
			fragmentManager.beginTransaction().replace(R.id.container, hpFragment).commit();
			
			break;
		case R.id.detail:
	
			HotelServicesFragment hsfFragment = new HotelServicesFragment(hotel);
			poiUpdateListeners = hsfFragment;
			fragmentManager.beginTransaction().replace(R.id.container, hsfFragment).commit();
			
			
			break;
		case R.id.map:

		
			fragmentManager.beginTransaction().replace(R.id.container, new HotelAddressMapFragment(hotel)).commit();
			break;
		}
	}
}
