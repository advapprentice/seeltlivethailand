package com.touchtechnologies.tit.hotel;



import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.touchtechnologies.dataobject.Hotel.RoomType;
import com.touchtechnologies.tit.R;

public class HotelPriceAdapter extends BaseAdapter {
	private Context context;
	
	protected ImageLoader imageLoader;
	protected DisplayImageOptions options;
	private List<RoomType> arrroomtype;
	public HotelPriceAdapter(Context context) {
		this.context = context;
		
	}
	public void setData(List<RoomType> arrroom){
		this.arrroomtype = arrroom;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return arrroomtype == null?0:arrroomtype.size();
	}
	
	@Override
	public RoomType getItem(int position) {
		return arrroomtype.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	public void remove(RoomType room){
		boolean success = arrroomtype.remove(room);
		if(success){
			notifyDataSetChanged();
		}
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		
		if(convertView == null){
			convertView = LayoutInflater.from(context).inflate(R.layout.item_price, parent, false);
			
			viewHolder = new ViewHolder();
			

			
			viewHolder.tvroomtype = (TextView)convertView.findViewById(R.id.txtroomtype);
			viewHolder.tvprice = (TextView)convertView.findViewById(R.id.txtprice);

			convertView.setTag(viewHolder);
			
			
		}else{
			viewHolder = (ViewHolder)convertView.getTag();
		}
		viewHolder.tvroomtype.setText(getItem(position).getName());
	
		double price = getItem(position).getPrice();
		String stringdouble= Double.toString(price);
		viewHolder.tvprice.setText(stringdouble);
		
		
//		Toast.makeText(context, "url"+getItem(position).getName(),Toast.LENGTH_SHORT).show();

	
		Log.i("CCTVActivity", "reloaded");

	
		return convertView;
	}



	class ViewHolder{
		
		TextView tvroomtype;
		TextView tvprice;
		
		View Viewvideoschedule;
		View topic;
		
	}
	
}