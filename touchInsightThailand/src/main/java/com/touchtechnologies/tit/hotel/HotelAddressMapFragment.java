package com.touchtechnologies.tit.hotel;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.tit.LocationAwareFragment;
import com.touchtechnologies.tit.R;

@SuppressLint("ValidFragment")
public class HotelAddressMapFragment extends LocationAwareFragment implements OnClickListener, OnMapClickListener, OnMapReadyCallback {
	public HotelAddressMapFragment(Hotel hotel) {
		this.hotel = hotel;
		// TODO Auto-generated constructor stub
	}
 //   static final LatLng TutorialsPoint = new LatLng(13 ,100);
	private Hotel hotel;
	private GoogleMap googleMap;
	private Circle myCircle;
	private TextView txtaddress,txtemail,txtphone;
	private int mapZoomLevel = 24;
	private HashMap<Marker, Hotel> hMarkers;
	private HotelAddressMapinfoAdapter adapterInfo;
	private Context context;
	double longitude ;
	double latitude;
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_hotel_map, container, false);
		view.findViewById(R.id.buttonMapDirection).setOnClickListener(this);
		hMarkers = new HashMap<Marker, Hotel>();
	
		adapterInfo = new HotelAddressMapinfoAdapter(getActivity());
		adapterInfo.setMarker(hMarkers);

		txtaddress = (TextView)view.findViewById(R.id.txtaddress);
		txtemail = (TextView)view.findViewById(R.id.txtemail);
		txtphone = (TextView)view.findViewById(R.id.txtphone);
		
		txtaddress.setText(hotel.getDisplayAddress());
		txtemail.setText(hotel.getEmail());
		txtphone.setText(hotel.getPhoneNumberAt(0).getNumber());
		return view;
	}

	
	
	@Override
	public void onResume() {
		super.onResume();

		try {
			if (googleMap == null) {

				SupportMapFragment MapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
				MapFragment.getMapAsync(this);
		//		googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hotel.getLocation(),getResources().getInteger(R.integer.map_zoom_default)));
	  //		googleMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(hotel.getLatitude(), hotel.getLongitude()) , 12.0f) );
				googleMap.setMyLocationEnabled(true);
				googleMap.setInfoWindowAdapter(adapterInfo);
	
				
				
				
				
			}
			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			Marker TP = googleMap.addMarker(new MarkerOptions()
					.position(new LatLng(hotel.getLatitude(), hotel.getLongitude()))
					.title(hotel.getName())
					.snippet(hotel.getDisplayAddress())
					.icon(BitmapDescriptorFactory.fromResource(R.drawable.maker)));
			hMarkers.put(TP, hotel);
			googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(hotel.getLatitude(), hotel.getLongitude()),12.0f));
		
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	

	@Override
	public void onConnected(Bundle dataBundle) {
//		if (mLocationClient != null
//				&& googleMap != null) {
//			location = mLocationClient.getLastLocation();
//
			if(location != null){
				LatLng myLastLatLon = new LatLng(location.getLatitude(), location.getLongitude());
				CameraUpdate camUpdate = CameraUpdateFactory.newLatLngZoom(myLastLatLon,mapZoomLevel); 
				googleMap.animateCamera(camUpdate);
				
			}
//		}
	}	
	
	public void setMapZoomLevel(int mapZoomLevel) {
		this.mapZoomLevel = mapZoomLevel;
	}

	
	public void setLocation(Location location) {
		this.location = location;
		
		if(googleMap != null){
			//move map to last know location if possible when started
			LatLng myLastLatLon = new LatLng(location.getLatitude(), location.getLongitude());
			CameraUpdate camUpdate = CameraUpdateFactory.newLatLngZoom(myLastLatLon, mapZoomLevel); 
			
			googleMap.animateCamera(camUpdate);
		}
	}
	
	@Override
	public void onLocationChanged(Location location) {
		super.onLocationChanged(location);
	//	mLocationClient.disconnect();
	}
	

		
		@Override
		public void onMapClick(LatLng point) {
			MarkerOptions mark = new MarkerOptions();
			
			googleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
			mark.position(point);
		
		}
		
	
	/**
	 * Start map navigation
	 */
	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.buttonMapDirection){
			Uri gmmIntentUri = Uri.parse("google.navigation:"+hotel.getLatitude()+","+hotel.getLongitude())
			.buildUpon()
            .appendQueryParameter("q",hotel.getName())
            .build();
	//		Uri gmmIntentUri = Uri.parse("google.navigation:q="+hotel.getName()+","+hotel.getLatitude()+","+hotel.getLongitude()+"&mode=b");
			Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
			mapIntent.setPackage("com.google.android.apps.maps");
			startActivity(mapIntent);
			
		//	Toast.makeText(getActivity(),"latlong"+hotel.getLatitude(), Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onMapReady(GoogleMap googleMapv) {
		googleMap = googleMapv;
	}
}
