package com.touchtechnologies.tit.hotel;


import java.util.HashMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
//import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.tit.R;


public class HotelAddressMapinfoAdapter implements InfoWindowAdapter {
	LayoutInflater inflater = null;
	protected Context context;
	
	private HashMap<Marker, Hotel> hMarkers;
	private HashMap<Marker, View> hViews;
	
	Hotel hotels;
	protected ImageLoader imageLoader;
	protected DisplayImageOptions options;
	
	
	public HotelAddressMapinfoAdapter(Context context) {
		this.context = context;
		this.inflater = LayoutInflater.from(context);
		imageLoader = ImageLoader.getInstance();
		
		hViews = new HashMap<Marker, View>();
		
		options = new DisplayImageOptions.Builder()
		.cacheInMemory(true).cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)    	 
		.imageScaleType(ImageScaleType.NONE)
		.showImageForEmptyUri(R.drawable.empty_cover)
		
//		.showImageOnLoading(R.drawable.loading)
		.build();
	}
	 
	
	@Override
	public View getInfoContents(final Marker marker) {
		View popup = hViews.get(marker);
		
		if(popup == null){
			popup = inflater.inflate(R.layout.agent_info_window_map_adapter, null, false);
		
			hotels = hMarkers.get(marker);
			String url = hotels.getImage().getUri();
			ImageView imageViewAvatar = ((ImageView)popup.findViewById(R.id.infoAvatar));
			
			hViews.put(marker, popup);
			
			imageLoader.displayImage( url,imageViewAvatar,options, new ImageLoadingListener() {
				
				@Override
				public void onLoadingStarted(String imageUri, View view) {
					
				}
				
				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason){
					
				}
				
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					marker.showInfoWindow();
				}
				
				@Override
				public void onLoadingCancelled(String imageUri, View view) {
					
				}
			});
		
			TextView tv = (TextView) popup.findViewById(R.id.title);
			tv.setText(marker.getTitle());
	
			tv = (TextView) popup.findViewById(R.id.address);
			tv.setTextColor(Color.BLUE);
			tv.setText(marker.getSnippet());
			tv.setText(marker.getSnippet());
		}
		
		return (popup);
	}

	


	@Override
	public View getInfoWindow(Marker marker) {
		
		return null;
//		return getInfoContents(marker) ;
	}

	public void setMarker( HashMap<Marker, Hotel>  hMarkers){
		this.hMarkers = hMarkers;
		
		
	}
}	