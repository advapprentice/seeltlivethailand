package com.touchtechnologies.tit.hotel;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.touchtechnologies.command.insightthailand.RestServiceROIListCommand;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.ROIAdapter;
import com.touchtechnologies.tit.search.SearchActionbarActivity;
import com.touchtechnologies.tit.util.ResourceUtil;

/**
 * Display Hotel ROI/ destination list
 * @author Touch
 *
 */
public class HotelROIActivity extends SearchActionbarActivity implements OnItemClickListener{
	private List<ROI> rois;
	private ROIAdapter adapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_hotel_rois);
		
		adapter = new ROIAdapter(this, R.layout.item_grid_circle);
		
		GridView gridView = (GridView)findViewById(R.id.gridViewHotelROIs);
		gridView.setAdapter(adapter);
		gridView.setOnItemClickListener(this);
		
		mTitle = getString(R.string.menu_tit_hotel);
		
		String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_rois);
		
		new RestServiceROIListCommand(this, url){
	
			protected void onPostExecute(java.util.List<ROI> result) {
				rois = result;
				adapter.setData(rois);
				adapter.notifyDataSetChanged();
			};
		}.execute();
	}
	
	/**
	 * Start Hotel list activity by passing a clicked ROI object
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		
		Intent intent = new Intent(this, HotelListActivity.class);
		intent.putExtra(ROI.class.getName(), rois.get(position));
		startActivity(intent);
	}
}
