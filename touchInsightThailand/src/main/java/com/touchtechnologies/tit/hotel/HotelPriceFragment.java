package com.touchtechnologies.tit.hotel;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.touchtechnologies.dataobject.Hospitality;
import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.tit.GalleryAdapter;
import com.touchtechnologies.tit.GalleryFragment;
import com.touchtechnologies.tit.POIUpdateListner;
import com.touchtechnologies.tit.R;


@SuppressLint("ValidFragment")
public class HotelPriceFragment extends Fragment implements OnItemClickListener, POIUpdateListner{
	private Hotel hotel;

	private ListView listView ;
	private HotelPriceAdapter adapter;
	private GalleryAdapter  galAdapter;
	public HotelPriceFragment(Hotel hotel) {
		this.hotel = hotel;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_hotel_price, container, false);
		
		adapter = new HotelPriceAdapter(getActivity());
		listView = (ListView)view.findViewById(R.id.listViewHotelPrice);
		listView.setAdapter(adapter);
		adapter.setData(hotel.getRoomType());
		listView.setOnItemClickListener(this);

		galAdapter = new GalleryAdapter(getActivity(), R.layout.item_gallery_preview);
		galAdapter.setData(hotel.getImageGallery());
		
		GalleryFragment fragment = new GalleryFragment(galAdapter);
		FragmentManager fManager = getFragmentManager();
		fManager.beginTransaction().add(R.id.fragmentGalleryContainer, fragment).commit();
		
		return view;
	}
	

	@Override
	public void poiUpdated(Hospitality poi) {
		 this.hotel = (Hotel)poi;
		 
		 adapter.setData(hotel.getRoomType());
		/// pagergallery
		 galAdapter.setData(hotel.getImageGallery());
	}
	
	/**
	 * When room type/ price is clicked
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		
	}

	/**
	 * View of hotel price
	 * @author Touch
	 *
	 */
	

}
