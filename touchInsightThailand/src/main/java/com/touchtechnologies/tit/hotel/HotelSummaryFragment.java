package com.touchtechnologies.tit.hotel;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.opensource.pagerindicator.CirclePageIndicator;
import com.touchtechnologies.dataobject.Hospitality;
import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.tit.GalleryAdapter;
import com.touchtechnologies.tit.GalleryFragment;
import com.touchtechnologies.tit.POIUpdateListner;
import com.touchtechnologies.tit.R;

@SuppressLint("ValidFragment")
public class HotelSummaryFragment extends Fragment implements POIUpdateListner{
	private Hotel hotel;
	private GalleryAdapter  galAdapter;
    CirclePageIndicator circlepager;
   
	private TextView texthotelname,adddress,email,tel,txtrating,description ;
	public HotelSummaryFragment(Hotel hotel)  {
		this.hotel = hotel;
	}

	@Override
	public void poiUpdated(Hospitality poi) {
		 this.hotel = (Hotel)poi;
		 galAdapter.setData(hotel.getImageGallery());
		 //update view
		 onResume(); 
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_hotel_summary, container, false);
		

		galAdapter = new GalleryAdapter(getActivity(), R.layout.item_gallery_preview);
		galAdapter.setData(hotel.getImageGallery());
		
		GalleryFragment fragment = new GalleryFragment(galAdapter);
		FragmentManager fManager = getFragmentManager();
		fManager.beginTransaction().add(R.id.fragmentGalleryContainer, fragment).commit();
		
		
		texthotelname =(TextView)view.findViewById(R.id.txthotelname); 
		adddress = (TextView)view.findViewById(R.id.txtaddress); 
		email = (TextView)view.findViewById(R.id.txtemail); 
		tel = (TextView)view.findViewById(R.id.txttel); 
		txtrating = (TextView)view.findViewById(R.id.txtrating); 
		description = (TextView)view.findViewById(R.id.txtdescription); 
		
		
		texthotelname.setText(hotel.getName());
		adddress.setText(hotel.getDisplayAddress());
		email.setText(hotel.getEmail()== null?"":hotel.getEmail());
	    tel.setText(hotel.getPhoneNumberAt(0).getNumber());
		description.setText(hotel.getDescription());
		
		
		double rating = hotel.getRating();
		String stringdouble= Double.toString(rating);
		txtrating.setText(stringdouble);
	
		
		
		
		
		return view;
	}

}
