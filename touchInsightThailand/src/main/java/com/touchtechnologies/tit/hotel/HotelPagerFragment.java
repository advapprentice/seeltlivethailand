package com.touchtechnologies.tit.hotel;

import java.util.List;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.touchtechnologies.command.insightthailand.RestServiceROIListCommand;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.roi.ROIFragment;
import com.touchtechnologies.tit.util.ResourceUtil;

public class HotelPagerFragment extends Fragment{
	private List<ROI> rois;
	ViewPager mViewPager;
	PagerAdapter pagerAdapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		String url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_rest_rois);
		
		new RestServiceROIListCommand(getActivity(), url){
	
			protected void onPostExecute(java.util.List<ROI> result) {
				rois = result;
				pagerAdapter.notifyDataSetChanged();
			};
		}.execute();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_hotel_pager, container, false);
		
		PagerTabStrip pagerTabStrip = (PagerTabStrip)view.findViewById(R.id.pager_title_strip);
				pagerTabStrip.setDrawFullUnderline(true);
				pagerTabStrip.setTabIndicatorColor(Color.parseColor("#0066CC"));
		
		pagerAdapter = new ROIAdapter(getFragmentManager());
				
		mViewPager = (ViewPager) view.findViewById(R.id.pager);
		mViewPager.setAdapter(pagerAdapter);
		mViewPager.setOnPageChangeListener(new PageChangeListner());
		return view;
	}

//	@Override
//	public void onAttach(Activity activity) {
//		super.onAttach(activity);
//		((MainActivity) activity).onSectionAttached(roi == null?getString(R.string.app_name):roi.getName());
//	}

	/**
	 * A class use to create a fragment to put in each pager's screen 
	 * @author MEe
	 *
	 */
	class ROIAdapter extends FragmentStatePagerAdapter {
		public ROIAdapter(FragmentManager fm) {
			super(fm);
		}
		
		@Override
		public int getCount() {
			return rois == null?0:rois.size();
		}
		
		@Override
		public CharSequence getPageTitle(int position) {
			return rois.get(position).getName();
		}
		
		/**
		 * @see StringArray business_manager_pager
		 * 
		 */
		@Override
		public Fragment getItem(int position) {
			Fragment fragment = new ROIFragment(rois.get(position));
			return fragment;
		}
	}
	
	class PageChangeListner implements ViewPager.OnPageChangeListener{
		@Override
		public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
			
		}
		
		@Override
		public void onPageScrollStateChanged(int state) {
			Log.d("PageChanged", "state="+state);
		}
		
		@Override
		public void onPageSelected(int position) {
			Log.d("PageChanged", "selected"+position);
		}
	}
}
