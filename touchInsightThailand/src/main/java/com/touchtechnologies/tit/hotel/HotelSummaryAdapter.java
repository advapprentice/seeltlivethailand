package com.touchtechnologies.tit.hotel;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.touchtechnologies.tit.R;

public class HotelSummaryAdapter extends PagerAdapter {
	
	private LayoutInflater inflater;
	Activity context;
    int _position;
    int[] galImages;
  
    ImageLoader imageLoader;
    DisplayImageOptions options;
    public HotelSummaryAdapter(Activity context,  int[] imageArra) {
    	this.context=context;
        this.galImages =  imageArra;
    
     
		imageLoader = ImageLoader.getInstance();
		
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.NONE)
		.cacheInMemory(true)
		.displayer(new FadeInBitmapDisplayer(1000)) //fade in images
		.resetViewBeforeLoading(true).build();
			  
 	    imageLoader = ImageLoader.getInstance();  
             
	}
	@Override
    public int getCount() {
		return galImages == null ? 0 : galImages.length;
    
    	}

	  
	@Override
	public boolean isViewFromObject(View view, Object object) {
		return (view == object);		
	}

	public long getItemId(int position) {
		return position;
	}
   
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
    	
      View view = LayoutInflater.from(context).inflate(R.layout.item_gallery_hotel, container, false);
      ImageView imageView = (ImageView) view.findViewById(R.id.imgDisplay);
   
   
     
      imageView.setImageResource(galImages[position]);
   //   imageLoader.displayImage(galImages[position], imageView,options);
      container.addView(view,0);
    
      return view;
    }


	@Override
	public void destroyItem(View collection, int position, Object view) {
		((ViewPager) collection).removeView((View)view);
		 
	}
	
  }