package com.touchtechnologies.tit.hotel;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.touchtechnologies.command.insightthailand.RestServiceHotelListCommand;
import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.tit.POIAdapter;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.search.SearchActionbarActivity;
import com.touchtechnologies.tit.util.ResourceUtil;

/**
 * Display a list of hotel in a selected ROI
 * @author Touch
 *
 */
public class HotelListActivity extends SearchActionbarActivity implements OnItemClickListener{
	private ROI roi;
	private POIAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_hotel_list);
		
		roi = (ROI)getIntent().getSerializableExtra(ROI.class.getName());
		
		adapter = new POIAdapter(this, R.layout.item_grid_square);
		
		GridView gridView = (GridView)findViewById(R.id.gridViewHotelPOIs);
		gridView.setAdapter(adapter);
		gridView.setOnItemClickListener(this);
		
		mTitle = roi.getName();
		
		String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_pois);
		new RestServiceHotelListCommand(this, roi, url){
			
			protected void onPostExecute(java.util.List<Hotel> result) {
				adapter.setData(result);
			};
		}.execute();
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	
		Intent intent = new Intent(getApplicationContext(), HotelActivity.class);
		intent.putExtra(Hotel.class.getName(), adapter.getItem(position));
		
		startActivity(intent);
	}
}
