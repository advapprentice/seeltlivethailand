package com.touchtechnologies.tit.cctv;

import java.util.List;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.opensource.pagerindicator.CirclePageIndicator;
import com.touchtechnologies.command.insightthailand.RestServiceCCTVListCommand;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.lib.animator.DepthPageTransformer;
import com.touchtechnologies.tit.util.ResourceUtil;


public class CCTVPagerActivity extends Fragment{

private List<CCTV> cctvs;
private CCTV cctv;
private CCTVPagerAdapter adapter;
Context context;
ViewPager pager;
CirclePageIndicator circlepager;
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_cctv, container, false);
		
		adapter = new CCTVPagerAdapter(getActivity());
	
		pager = (ViewPager)view.findViewById(R.id.viewpager);
	
		pager.setPageTransformer(true,new DepthPageTransformer());
		
		pager.setAdapter(adapter);
		pager.setCurrentItem(0);
		pager.setClipToPadding(false);

		
		circlepager = (CirclePageIndicator)view.findViewById(R.id.indicator);
		circlepager.setViewPager(pager);
		
		
		 final float density = getResources().getDisplayMetrics().density;
		
		 circlepager.setRadius(6 * density);
		 circlepager.setPageColor(0xFFCFCFCF);
         circlepager.setFillColor(0xFF3399FF);
         circlepager.setStrokeColor(0xFFCFCFCF);
         circlepager.setStrokeWidth(1 * density);
    	
		
		Point point = new Point();
	  	getActivity().getWindowManager().getDefaultDisplay().getSize(point);
	  	pager.getLayoutParams().height = (int)((point.x * 2.5)/4);

	  	String url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_rest_cctvs);
		
		new RestServiceCCTVListCommand(getActivity(), url){
	
			protected void onPostExecute(java.util.List<CCTV> result) {
				cctvs = result;
				adapter.setData(cctvs);
			//	pager.setCurrentItem(1);
				adapter.notifyDataSetChanged();

			};
		}.execute();
	
		
		return view;
	
	}
	
	/**
	 * Start Hotel list activity by passing a clicked ROI object
	 */
	
}


