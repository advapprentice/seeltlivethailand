package com.touchtechnologies.tit.cctv;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.tit.R;
@SuppressLint("ValidFragment")
public class HomeCCTVActivity extends Fragment{
	CCTV cctv;

	public HomeCCTVActivity(CCTV cctv) {
		this.cctv = cctv;
	}
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_main_cctv, container, false);
		return view;
	}
}
