package com.touchtechnologies.tit.cctv;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

import com.touchtechnologies.command.insightthailand.RestServiceCCTVListCommand;
import com.touchtechnologies.command.reporter.ListUserCommand;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.reporter.User;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.util.ResourceUtil;

@SuppressLint("ValidFragment")
public class CCTVListActivity extends ListFragment implements OnScrollListener, OnRefreshListener {
	
	private boolean loading = false;
	protected int action_refresh;
	private Handler handler = new Handler();
	private SwipeRefreshLayout mSwipeRefreshLayout;
	private int visibleThreshold = 2;
    private int previousTotal = 0;
    private Runnable runnable;
	CCTVListAdapter adapter;
	List<CCTV> cctvs;
	ListView cctvView;
	View viewLoading, viewEmptyHint;
	User feedLongClicked;
	View listViewContainer;
	ListUserCommand command;
    Context context; 
    CCTV cctv;
    @SuppressLint("ValidFragment")
	public CCTVListActivity(CCTV cctv) {
	this.cctv = cctv;
    }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View listViewContainer = inflater.inflate(R.layout.fragment_mobilelive_list_cctv, container, false);
	
		mSwipeRefreshLayout = (SwipeRefreshLayout) listViewContainer.findViewById(R.id.container);
		mSwipeRefreshLayout.setColorScheme(R.color.swipe_gray, R.color.swipe_green,R.color.swipe_gray, R.color.swipe_green);  
		mSwipeRefreshLayout.setOnRefreshListener(this);
	//	mSwipeRefreshLayout.setVisibility(View.GONE);
		
        viewLoading = listViewContainer.findViewById(android.R.id.empty);
		viewEmptyHint = listViewContainer.findViewById(R.id.emptyAgentHint);
		cctvView = (ListView) listViewContainer.findViewById(android.R.id.list);
		cctvView.setOnScrollListener(this);
		viewEmptyHint.setVisibility(View.GONE);
		
		
		adapter = new CCTVListAdapter(getActivity());
		setListAdapter(adapter);
		onRefresh();
	

		return listViewContainer;
	}
	
	
	public void onRefresh(int action){
		action_refresh = action;
	
		
		if (!loading) {
	
			String url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_rest_cctvs);
			
		
			new RestServiceCCTVListCommand(getActivity(), url){
		
				protected void onPostExecute(java.util.List<CCTV> result) {
				
					cctvs = result;
					adapter.setData(cctvs);
					adapter.notifyDataSetChanged();
					
					int count = adapter.getCount();
					if(count > 0){
						mSwipeRefreshLayout.setVisibility(View.VISIBLE);
					}else{
						mSwipeRefreshLayout.setVisibility(View.GONE);
						viewLoading.setVisibility(View.GONE);
						viewEmptyHint.setVisibility(View.VISIBLE);
					}
				
				};
			}.execute();
			
		}	
	}
	
	public void onRefresh() {
		onRefresh(MotionEvent.ACTION_DOWN);
		mSwipeRefreshLayout.setRefreshing(false);
	}

	public void onScroll(AbsListView abs, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
	
			if (loading) {
	            if (totalItemCount > previousTotal) {
	                loading = false;
	                previousTotal = totalItemCount;
	            }
	        }
	        if (!loading & totalItemCount > 0 && (totalItemCount + visibleItemCount) <= (firstVisibleItem - visibleThreshold)) {
	            // load the next feeds using a background task,
	        	onRefresh();
	        	
	            loading = true;
	        }
	
			
		}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		
			Intent intent = new Intent(getActivity(), CCTVDetailActivity.class);
			intent.putExtra(CCTV.class.getName(), adapter.getItem(position));
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			
	
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		
	}


}