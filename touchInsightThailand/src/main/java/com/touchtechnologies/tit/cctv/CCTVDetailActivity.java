package com.touchtechnologies.tit.cctv;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.tit.R;


public class CCTVDetailActivity extends Activity{


	private  ImageLoader imageLoader;
	private Runnable runnable;
	private ImageView imglive;
	private TextView tvlocation,tvcreatedate,tvupdate;
	private CCTV cctv;
	protected DisplayImageOptions options;
	int count=0;
	Context context;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_detail_cctv);
	
		imageLoader = ImageLoader.getInstance();
		
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
		.cacheInMemory(true)
		.resetViewBeforeLoading(true).build();
		
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
		.memoryCache(new WeakMemoryCache())
		.threadPriority(Thread.NORM_PRIORITY)
        .denyCacheImageMultipleSizesInMemory()
        .build();
 	     	  
 	    imageLoader = ImageLoader.getInstance();
		if(!imageLoader.isInited()){
			imageLoader.init(config);	
		}

		cctv = (CCTV) getIntent().getSerializableExtra(CCTV.class.getName());
		
		imglive = (ImageView)findViewById(R.id.imgcctv);
		tvlocation = (TextView)findViewById(R.id.txtlocation);
		tvcreatedate = (TextView)findViewById(R.id.txtcreatedate);
		tvupdate = (TextView)findViewById(R.id.txtupdate);
		
		
		tvlocation.setText(cctv.getKeyName());
		tvcreatedate.setText(cctv.getCreated());
		tvupdate.setText(cctv.getUpdated());
	
		
		runnable = new ReloadImage();
		runOnUiThread(runnable);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
		
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onDestroy() {
		runnable = null;
		super.onDestroy();
	}

	/**
	 * 
	 * @author Touch
	 *
	 */
	class ReloadImage implements Runnable{
		@Override
		public void run() {
			imageLoader.displayImage(cctv.getUrl()+"&seed=" + Math.random(), imglive);
			Log.i("CCTVActivity", "reloaded");
			if(runnable != null){
				runnable = new ReloadImage();
				imglive.postDelayed(runnable, 1800);
			}
		}
	}

	@Override
	public MenuInflater getMenuInflater() {
		// TODO Auto-generated method stub
		return super.getMenuInflater();
	}
	
	
	
}