package com.touchtechnologies.tit.cctv;


import java.util.List;
import java.util.Timer;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.tit.R;

public class CCTVListAdapter extends BaseAdapter {
	private Context context;
	private Runnable runnable;
	protected ImageLoader imageLoader;
	protected DisplayImageOptions options;
	Timer t;
	private List<CCTV> arrcctv;
	private CCTV cctvs;
	String url;
	public CCTVListAdapter(Context context) {
		this.context = context;
		

		imageLoader = ImageLoader.getInstance();
		
		
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.NONE)
		.showImageForEmptyUri(R.drawable.bg_noimage)
		.showImageOnFail(R.drawable.bg_noimage)
	//	.showImageOnLoading(R.drawable.bg_loading)
		.cacheInMemory(true)
		.displayer(new FadeInBitmapDisplayer(300)) //fade in images
	  	.resetViewBeforeLoading(true)
	  	.build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
		.memoryCache(new WeakMemoryCache())
		.threadPriority(Thread.NORM_PRIORITY)
        .denyCacheImageMultipleSizesInMemory()
        .build();
 	     	  
 	    imageLoader = ImageLoader.getInstance();
		if(!imageLoader.isInited()){
			imageLoader.init(config);	
		}
		
	}
	public void setData(List<CCTV> arrcctv){
		this.arrcctv = arrcctv;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return arrcctv == null?0:arrcctv.size();
	}
	
	@Override
	public CCTV getItem(int position) {
		return arrcctv.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	public void remove(CCTV artis){
		boolean success = arrcctv.remove(artis);
		if(success){
			notifyDataSetChanged();
		}
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		
		if(convertView == null){
			convertView = LayoutInflater.from(context).inflate(R.layout.item_mobilelive_list_cctv, parent, false);
			
			viewHolder = new ViewHolder();
			
		
			viewHolder.imglive = (ImageView)convertView.findViewById(R.id.imgcctv);
			viewHolder.tvlocation = (TextView)convertView.findViewById(R.id.txtlocation);
			viewHolder.tvcreatedate = (TextView)convertView.findViewById(R.id.txtcreatedate);
			viewHolder.tvupdate = (TextView)convertView.findViewById(R.id.txtupdate);
		
			
//			Toast.makeText(context, "���ͺ"+getItem(position).getUrl(), Toast.LENGTH_LONG).show();
			convertView.setTag(viewHolder);
			
			
		}else{
			viewHolder = (ViewHolder)convertView.getTag();
		}
		cctvs = getItem(position);
		viewHolder.tvlocation.setText(getItem(position).getKeyName());
		viewHolder.tvcreatedate.setText(getItem(position).getCreated());
		viewHolder.tvupdate.setText(getItem(position).getUpdated());
	  	imageLoader.displayImage(getItem(position).getUrl()+"&seed="+Math.random(),viewHolder.imglive, options);
		Log.i("CCTVActivity", "reloaded");

	
		return convertView;
	}



	class ViewHolder{
		ImageView imglive;
		TextView tvlocation;
		TextView tvcreatedate;
		TextView tvupdate;
		View Viewvideoschedule;
		View topic;
		
	}
	
	
	
}