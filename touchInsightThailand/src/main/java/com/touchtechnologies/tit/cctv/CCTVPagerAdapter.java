package com.touchtechnologies.tit.cctv;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.tit.R;

/**
 * Simple ROI adapter that create a simple image and title
 * @author Touch
 *
 */
public class CCTVPagerAdapter extends PagerAdapter implements OnClickListener{
	private List<CCTV> cctvs;
	private Context context;
	private int resLayout;
	CCTV lcctv;
	Bitmap bitmap;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	
	public CCTVPagerAdapter(Context context) {
		this.context = context;
		
	
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.NONE)
				.showImageForEmptyUri(R.drawable.bg_noimage)
				.showImageOnFail(R.drawable.bg_noimage)
				.showImageOnLoading(R.drawable.bg_loading)
				.cacheInMemory(true)
				.displayer(new FadeInBitmapDisplayer(200)) //fade in images
			  	.resetViewBeforeLoading(true).build();
				 imageLoader = ImageLoader.getInstance();  
	}
	public void setData(List<CCTV> arrcctv){
		this.cctvs = arrcctv;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return cctvs == null ? 0 : cctvs.size();
	}
//    @Override
//    public float getPageWidth(int position) {
//        return 0.93f;
//    }
	@Override
	public boolean isViewFromObject(View view, Object object) {
		return (view == object);		
	}
	
	public CCTV getItem(int position) {
		return cctvs.get(position);
	}


	public long getItemId(int position) {
		return position;
	}
	
	  
		@Override
		public Object instantiateItem(ViewGroup collection, int position) {
			
//			imageLoader.displayImage(feed.medias.get(position).getThumbURL(),view, options);
			View convertView = LayoutInflater.from(context).inflate(R.layout.item_cctv_history, collection, false);
			convertView.setTag(cctvs.get(position));
			
			ImageView imageView = (ImageView)convertView.findViewById(R.id.imgDisplay);
			TextView textView  = (TextView)convertView.findViewById(R.id.location);
			TextView textdate = (TextView)convertView.findViewById(R.id.txtdate);
			TextView texttime  = (TextView)convertView.findViewById(R.id.txttime);
			
			View viewcctv = (View)convertView.findViewById(R.id.viewcctv);
			viewcctv.setOnClickListener(this);
			viewcctv.setTag(position);

			textView.setText(getItem(position).getKeyName());
			textdate.setText(getItem(position).getCreated());
			
			String create = getItem(position).getCreated();
			String timecreate = create.substring(0, 10);
			textdate.setText(timecreate);
						
			String update = getItem(position).getUpdated();
			String timeupdate = update.substring(11, 19);
			texttime.setText(timeupdate);
			
			imageLoader.displayImage(getItem(position).getUrl()+"&seed="+Math.random(), imageView, options);
			collection.addView(convertView,0);
		
			
			
			return convertView;
	}
	
	class ViewHolder{
		ImageView imageView;
		TextView textView;
	}
	
	@Override
	public void destroyItem(View collection, int position, Object view) {
		((ViewPager) collection).removeView((View)view);
		 
	}
	@Override
	public void onClick(View v) {
	    	switch (v.getId()) {
			case R.id.viewcctv:

				Intent intent = new Intent(context, CCTVDetailActivity.class);
				intent.putExtra(CCTV.class.getName(), cctvs.get((Integer)v.getTag()));
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				context.startActivity(intent);
				break;

			}
		
	}
	
	
}
