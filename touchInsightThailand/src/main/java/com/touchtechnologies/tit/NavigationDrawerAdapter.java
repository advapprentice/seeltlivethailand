package com.touchtechnologies.tit;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class NavigationDrawerAdapter extends BaseAdapter{
	private List<MenuInfo> menuInfos; 
	Context context;
	/** Default menu layout */
	private int layout = R.layout.item_menu;
	
	public NavigationDrawerAdapter(Context context) {
		this.context = context;
	}
	
	public void setMenuInfos(List<MenuInfo> menuInfos) {
		this.menuInfos = menuInfos;
		notifyDataSetChanged();
	}
	
	public void addMenuInfo(MenuInfo menu){
		if(menuInfos == null){
			menuInfos = new ArrayList<MenuInfo>();
		}
		
		if(!menuInfos.contains(menu)){
			menuInfos.add(menu);
			notifyDataSetChanged();
		}
	}
	
	/**
	 * Set menu layout
	 * @param layout a layout resource o be set
	 */
	public void setMenuLayout(int layout){
		this.layout = layout;
	}
	
	public int getCount() {
		return menuInfos == null?0:menuInfos.size();
	}
	
	@Override
	public MenuInfo getItem(int position) {
		return menuInfos.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			convertView = LayoutInflater.from(context).inflate(layout, parent, false);
			
		}
		
		MenuInfo menuInfo = getItem(position);
		((ImageView)convertView.findViewById(R.id.imageViewMenuIcon)).setImageDrawable(menuInfo.icon);
		
		return convertView;
	}
}
