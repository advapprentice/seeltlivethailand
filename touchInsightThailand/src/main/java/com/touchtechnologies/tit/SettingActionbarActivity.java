package com.touchtechnologies.tit;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


public class SettingActionbarActivity extends FragmentActivity {
	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private static final int PERMISSIONS_REQUEST_PHONE_STATE = 201;
	private String mManifestPersmission;
	private int mRequestCode;

	protected CharSequence mTitle;

	  
	public void restoreActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		//actionBar.setBackgroundDrawable(new ColorDrawable(0xFF1275AE));
		actionBar.setBackgroundDrawable(new ColorDrawable(0xFF000000));
		actionBar.setTitle(Html.fromHtml("<font color='#FFFFFF'>"+mTitle+"</font>"));
		actionBar.setIcon( new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		actionBar.setDisplayUseLogoEnabled(false);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		restoreActionBar();
		getPermission();
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
		
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void getPermission(){
		mManifestPersmission = Manifest.permission.READ_PHONE_STATE;
		mRequestCode = PERMISSIONS_REQUEST_PHONE_STATE;

		int permerssion = ActivityCompat.checkSelfPermission(this, mManifestPersmission);
		boolean should = ActivityCompat.shouldShowRequestPermissionRationale(this, mManifestPersmission);
		if (permerssion != PackageManager.PERMISSION_GRANTED) {
			requestPermission();
		}

	}

	private void requestPermission() {
		ActivityCompat.requestPermissions(this, new String[]{mManifestPersmission}, mRequestCode);
	}

}
