package com.touchtechnologies.tit.util;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.touchtechnologies.dataobject.WiFiAccount;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.AppConfig;

public class UserUtil {

	/**
	 * Is a current application session contains a user login session.
	 * 
	 * @param context
	 * @return log in status
	 */
	public static boolean isLoggedIn(Context context) {
		boolean loggedIn = false;

		User user = getUser(context);
		String session = user == null?null:user.getToken();
		
		if (session != null && session.trim().length() > 0){
			loggedIn = true;
			
		//	loggedIn = user.isLoggedIn();
		}
		
		return loggedIn;
	}
	
	/**
	 * Save user json to share preference
	 * @param context
	 * @param user
	 */
	public static void persist(Context context, User user){
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_USER, Context.MODE_PRIVATE);
		sharedPreferences.edit().putString(User.class.getName(), user.toString()).commit();
		
	}
	
	/**
	 * Remove user json from share preference
	 * @param context
	 */
	public static void clear(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_USER, Context.MODE_PRIVATE);
		sharedPreferences.edit().remove(User.class.getName()).commit();
		
		sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_CHECKIN, Context.MODE_PRIVATE);
		sharedPreferences.edit().remove(AppConfig.PREFERENCE_CHECKIN).commit();
	}	
	
	public static WiFiAccount getWiFiAccount(Context context){
		WiFiAccount wifiAccount = null;
		
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_WIFI_ACCOUNT, Context.MODE_PRIVATE);
		String json = sharedPreferences.getString(WiFiAccount.class.getName(),  null);
		if (json != null) {
			try {
				wifiAccount = new WiFiAccount(new JSONObject(json));
			} catch (Exception e) {
				Log.e(AppConfig.LOG, "Get WiFi account error", e);
			}
		}
		
		//TODO remove
		if(wifiAccount == null){
			wifiAccount = new WiFiAccount();
		}
		
		wifiAccount.setUsername("TPAA369");
		wifiAccount.setPassword("689434");
		
		return wifiAccount;
	}
	
	public static void setWiFiAccount(Context context, WiFiAccount account){
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_WIFI_ACCOUNT, Context.MODE_PRIVATE);
		sharedPreferences.edit().putString(WiFiAccount.class.getName(), account.toJson().toString()).commit();
	}

	/**
	 * Add new checkin json to persistent storage
	 * @param context
	 * @param json
	 */
	public static void addCheckIn(Context context, JSONObject json,String key){
		JSONArray jsonArray = new JSONArray();
		
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_CHECKIN, Context.MODE_PRIVATE);
		
		String savedString = sharedPreferences.getString(key, "");
		if(!savedString.equals("")){
			try{
				jsonArray = new JSONArray(savedString);

				
			}catch(Exception e){
				Log.e(UserUtil.class.getSimpleName(), "convert checkin error", e);
			}
		}
		
		jsonArray.put(json);
		
		sharedPreferences.edit().putString(key, jsonArray.toString()).commit();
	}
	
	/**
	 * Get all checked in poi
	 * @param context
	 * @return
	 */
	public static JSONArray getCheckIns(Context context,String key){
		JSONArray jsonArray = new JSONArray();
		
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_CHECKIN, Context.MODE_PRIVATE);
		String savedString = sharedPreferences.getString(key, "");
		
		if(!savedString.equals("")){
			try{
				jsonArray = new JSONArray(savedString);
			}catch(Exception e){
				Log.e(UserUtil.class.getSimpleName(), "convert checkin error", e);
			}
		}
		
		return jsonArray;
	}
	
	public static void clearCheckin(Context context,String key){
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_CHECKIN, Context.MODE_PRIVATE);
		sharedPreferences.edit().remove(key).commit();
	}	
	

	/**
	 * Get a user object stored in preference
	 * 
	 * @param context
	 * @return user object or null
	 */
	
	
	public static User getUser(Context context) {
		User user = null;

		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_USER, Context.MODE_PRIVATE);
		String userJson = sharedPreferences.getString(User.class.getName(),  null);
		if (userJson != null) {
			try {
				user = new User(userJson);
			} catch (Exception e) {
				Log.e(AppConfig.LOG, "Get user from json error", e);
			}
		}else{
			user =  new User();
		}

		return user;
	}
	

	
}
