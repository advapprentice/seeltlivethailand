package com.touchtechnologies.tit.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import android.util.Log;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.R;


public class ResourceUtil {
	/**
	 * Production server's service url
	 */
	public static final int MODE_PRODUCTION = 0;
	/**
	 * Developer server's service url
	 */
	public static final int MODE_INTRANET = 1;

	public static final int MODE_PRODUCTION_DEVELOP = 2;
//	/**
//	 * Tester server's service url
//	 */
//	public static final int MODE_INTRANET_IP102 = 3;	
	
//	public static final int MODE_INTRANET_IP90_ENPOINT = 2;	
	
	private static String PREFERENCE_MODE = "pref_mode";
	
	public static String getServiceUrl(Context context){
		String url = null;
		
		switch (getMode(context)) {
		case MODE_PRODUCTION:
			url = context.getResources().getString(R.string.app_service_production_url);
			break;		
		case MODE_INTRANET:
			url = context.getResources().getString(R.string.app_service_intranet_url);
			break;
		case MODE_PRODUCTION_DEVELOP:
			url = context.getResources().getString(R.string.app_service_intranet_production_url);
			break;
		
		}
		
		return url;
	}
	public static String getSocketUrl(Context context){
		String url = null;

		switch (getMode(context)) {
			case MODE_PRODUCTION:
				url = context.getResources().getString(R.string.app_service_production_socket);
				break;
			case MODE_INTRANET:
				url = context.getResources().getString(R.string.app_service_intranet_socket);
				break;
			case MODE_PRODUCTION_DEVELOP:
				url = context.getResources().getString(R.string.app_service_intranet_socket);
				break;

		}

		return url;
	}
	
	public static String getGoogleProjectID(Context context){
		String id = null;
		
		switch (getMode(context)) {
		case MODE_PRODUCTION:
			id = context.getResources().getString(R.string.app_push_sender_id_production);
			break;	
		case MODE_INTRANET:
			id = context.getResources().getString(R.string.app_push_sender_id_intranet);
			break;
		case MODE_PRODUCTION_DEVELOP:
			id = context.getResources().getString(R.string.app_push_sender_id_production);
			break;
		}
		
		Log.d(AppConfig.LOG, "Google project ID: " + id);
		
		return id;
		
	}
	

	public static String getFacebookID(Context context){
		String id = null;
		
		switch (getMode(context)) {
		case MODE_PRODUCTION:
			id = context.getResources().getString(R.string.app_facebook_app_id_production);
			break;	
		case MODE_INTRANET:
			id = context.getResources().getString(R.string.app_facebook_app_id_intranet);
			break;
		case MODE_PRODUCTION_DEVELOP:
			id = context.getResources().getString(R.string.app_facebook_app_id_production);
			break;
		}
		
		Log.d(AppConfig.LOG, "FacebookID ID: " + id);
		
		return id;
		
	}

	
	public static int getMode(Context context){
		int mode = MODE_PRODUCTION;
		
		try{
			String modeString = PreferenceManager.getDefaultSharedPreferences(context).getString(PREFERENCE_MODE, MODE_PRODUCTION + "");
			
			mode = Integer.parseInt(modeString);
			if(mode > 3){
				mode = MODE_PRODUCTION;
				
				PreferenceManager.getDefaultSharedPreferences(context).edit().putString(PREFERENCE_MODE, MODE_PRODUCTION + "").commit();
			}
		}catch(Exception e){
			Log.e("SIT-PREF", e.getMessage(), e);
		}
		
		return mode;
	}
	
	public static void setpopup(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_POPUP, Context.MODE_PRIVATE);
		sharedPreferences.edit().putBoolean("popup", false).commit();
	}

	public static boolean getpopup(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_POPUP, Context.MODE_PRIVATE);
		return sharedPreferences.getBoolean("popup", true);
	}
	public static void setLocation(Context context,Location location) {
		try {
			SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_LOCATION, Context.MODE_PRIVATE);
			sharedPreferences.edit().putLong("Latitude",Double.doubleToLongBits(location.getLatitude())).commit();
			sharedPreferences.edit().putLong("Longitude",Double.doubleToLongBits(location.getLongitude())).commit();
		}catch (Exception e){
			//MainApplication.getInstance().trackException(e);
		}

	}

	public static void setResolution(Context context,int re){
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_RESOLUTION, Context.MODE_PRIVATE);
		sharedPreferences.edit().putInt(AppConfig.PREFERENCE_RESOLUTION,re).commit();
	}
	public static int getResolution(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_RESOLUTION, Context.MODE_PRIVATE);
		return sharedPreferences.getInt(AppConfig.PREFERENCE_RESOLUTION,6);
	}
	public static void setFramerate(Context context,int fps){
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_FRAMERATE, Context.MODE_PRIVATE);
		sharedPreferences.edit().putInt(AppConfig.PREFERENCE_FRAMERATE,fps).commit();
	}
	public static int getFramerate(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_FRAMERATE, Context.MODE_PRIVATE);
		return sharedPreferences.getInt(AppConfig.PREFERENCE_FRAMERATE,10);
	}
	public static int getLivetime(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_LIVETIME, Context.MODE_PRIVATE);
		return sharedPreferences.getInt(AppConfig.PREFERENCE_LIVETIME,5);
	}
	public static void setLivetime(Context context,int time){
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_LIVETIME, Context.MODE_PRIVATE);
		sharedPreferences.edit().putInt(AppConfig.PREFERENCE_LIVETIME,time).commit();
	}

	/**
	 * @param context
	 * @return Location
	 *
	 */
	public static Location getLocation(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_LOCATION, Context.MODE_PRIVATE);
		double latitude = Double.longBitsToDouble(sharedPreferences.getLong("Latitude", 0));
		double longitude = Double.longBitsToDouble(sharedPreferences.getLong("Longitude",0));
		if(latitude == 0 && longitude == 0){
			return  null;
		}
		Location location = new Location("lastkonwlocation");
		location.setLatitude(latitude);
		location.setLongitude(longitude);
		return location;
	}
	
}
