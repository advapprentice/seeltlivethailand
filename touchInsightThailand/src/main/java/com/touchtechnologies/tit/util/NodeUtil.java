package com.touchtechnologies.tit.util;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.model.LatLng;
import com.touchtechnologies.tit.dataobject.Node;

public class NodeUtil {
	static List<Node> lNodes;
	
	static{
		lNodes = new ArrayList<>();
		
		double[][] lls = new double[][]{
				{1, 13.733264453965472,100.52927434444427},
				{2, 13.733227650871685,100.52933000028133},	
				{3, 13.733195407448466,100.52923444658518},
				{4, 13.733187916551541,100.52932295948267,},	
				{5, 13.733153067593095,100.52942454814911},
				{6, 13.733122126924705,100.52952982485294},	
				{7, 13.733106168052052,100.52959889173508},
				{8, 13.733086952265158,100.52969042211771},	
				{9, 13.733094443165328,100.5298426374793},
				{10, 13.733060896958403,100.52988689392805},	
				{11, 13.733037447179827,100.52992880344391},
				{12, 13.73301041479329,100.52975781261921},	
				{13, 13.733068062168043,100.52974373102188},
				{14, 13.732977845648742,100.52981313318014},	
				{15, 13.73299119899856,100.529878847301},
				{16, 13.733013020324652,100.52990701049566},	
				{17, 13.733075878760141,100.53003139793873},
				{18, 13.733008460644731,100.5302570387721},	
				{19, 13.732991850381438,100.52996501326561},
				{20, 13.73296579506411,100.53001597523689},	
				{21, 13.732925735008044,100.53017724305391},	
				{22, 13.732929317614976,100.5302556976676}
		};
		
		for(int i=0; i<lls.length; i++){
			Node node = new Node();
			node.setId((int)lls[i][0]);
			node.setLatLng(new LatLng(lls[i][1], lls[i][2]));
			
			lNodes.add(node);
		}
	}
	
	public static List<LatLng> getNodesBetween(LatLng ll1, LatLng ll2){
		List<LatLng> nodes = new ArrayList<>();
		
		nodes.add(ll1);
		
		LatLng baseLL;
		while(true){
			baseLL = nodes.get(nodes.size() - 1);
			
			LatLng nearLL = getNearest(baseLL);
			nodes.add(nearLL);
			
			if(baseLL.latitude == ll2.latitude
					&& baseLL.longitude == ll2.longitude){
				break;
			}
			
			
		}
		
		return nodes;
	}
	
	/**
	 * Get nearest node to a given LatLng
	 * @param ll
	 * @return
	 */
	public static LatLng getNearest(LatLng ll){
		double minLat = Integer.MAX_VALUE;
		double minLon = Integer.MAX_VALUE;
		
		LatLng minLatLon = null;
		LatLng llTmp;
		
		for(int i=0; i<lNodes.size(); i++){
				llTmp = lNodes.get(i).getLatLng();
				if(llTmp.latitude == 0 & llTmp.longitude == 0)
					continue;
				if(llTmp.latitude == ll.latitude & llTmp.longitude == ll.longitude)
					continue;
				
				if(Math.abs(llTmp.latitude - ll.latitude) <= minLat
						& Math.abs(llTmp.longitude - ll.longitude) <= minLon){
					minLatLon = llTmp;
				}
			}
		
		
		return minLatLon;
	}
	
	public static List<Node> getNodes(){
		return lNodes; 
	}
}
