package com.touchtechnologies.tit.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import android.content.res.AssetManager;
import android.util.Log;

import com.touchtechnologies.tit.dataobject.Tenant;

public class TenantUtil {

	public static List<Tenant> getTenantFromAsset(AssetManager am) throws IOException{
		List<Tenant> tenants = new ArrayList<>();
		
		InputStream ins  = null;
		BufferedReader bis = null;
		InputStreamReader isr = null;
		try{
			
			ins = am.open("chamchuri_square.txt");
			isr = new InputStreamReader(ins);
			bis = new BufferedReader(isr);
			
			StringBuilder stBuilder = new StringBuilder();
			String tmp;
			while((tmp = bis.readLine()) != null){
				stBuilder.append(tmp);
			}
			
			JSONArray jsonArray = new JSONArray(stBuilder.toString());
			Tenant tenant;
			for(int i=0; i<jsonArray.length(); i++){
				tenant = new Tenant(jsonArray.getJSONObject(i));
//				Log.d("TENANT", i + " " + tenants.toString());
				
				tenants.add(tenant);
			}
			
		}catch(Exception e){
			Log.e(TenantUtil.class.getSimpleName(), "error " + e.getMessage());
		}finally{
			if(ins != null){
				ins.close();
			}
			if(isr != null){
				isr.close();
			}
			if(bis != null){
				bis.close();
			}
		}
		
		return tenants;
	}
}
