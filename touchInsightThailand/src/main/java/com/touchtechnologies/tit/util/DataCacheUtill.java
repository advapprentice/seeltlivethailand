package com.touchtechnologies.tit.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.touchtechnologies.dataobject.Hospitality;
import com.touchtechnologies.dataobject.insightthailand.CategoryLiveStream;
import com.touchtechnologies.location.Constant;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.dataobject.Category_video;
import com.touchtechnologies.tit.dataobject.HotlinePhone;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TouchICS on 3/8/2016.
 */
public class DataCacheUtill {

    private static final String PREF_CATEGORY_STREAM_CACHED = "pref_category_stream";
    private static final String PREF_HELPFUL_CONTACT =  "pref_helpful_contact";
    private static final String PREF_NEAR_BY_POI =  "pref_nearby_poi";
    
    public static void setCategoryStream(Context context, Category_video categoryLiveStreams){


//        JSONArray jsonCategoryLiveStreams = new JSONArray();
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_CATEGORY_STREAM_CACHED, Context.MODE_PRIVATE);

        String savedString = sharedPreferences.getString(PREF_CATEGORY_STREAM_CACHED, "");
//
//        try{
//            for(CategoryLiveStream pv: categoryLiveStreams){
//                jsonCategoryLiveStreams.put(pv);
//                Log.d("Category List", ":" + pv.toString());
//            }
//        }catch(Exception e){
//            Log.e("Error", e.getMessage());
//        }
// list CategoryLiveStreams
        if(context == null)
            return;

        try {

            sharedPreferences.edit().putString(PREF_CATEGORY_STREAM_CACHED,categoryLiveStreams.toJson().toString()).commit();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static List<CategoryLiveStream> getCategoryStream(Context context){
        List<CategoryLiveStream> listCategory = new ArrayList<CategoryLiveStream>();
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_CATEGORY_STREAM_CACHED, Context.MODE_PRIVATE);

        String json = sharedPreferences.getString(PREF_CATEGORY_STREAM_CACHED, null);

        try{

            if(json != null){
                JSONArray jsonProvinces = new JSONArray(json);

                for(int i=0; i<jsonProvinces.length(); i++){
                    listCategory.add(new CategoryLiveStream(jsonProvinces.getJSONObject(i)));
                }
            }
        }catch(Exception e){
            Log.e(AppConfig.LOG, e.getMessage());
        }

        return listCategory;
    }

    public static void setHotlinePhone(Context context, List<HotlinePhone> mHotlinePhones) {
        if (context == null)
            return;
        JSONArray jsonArray = new JSONArray();
            for (HotlinePhone mHotlinePhone:mHotlinePhones) {
                try {
                    jsonArray.put(mHotlinePhone.toJson());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_HELPFUL_CONTACT, Context.MODE_PRIVATE);
        String savedString = sharedPreferences.getString(PREF_HELPFUL_CONTACT, "");
                sharedPreferences.edit().putString(PREF_HELPFUL_CONTACT,jsonArray.toString().trim()).commit();

    }

    public static List<HotlinePhone> getHotlinePhones (Context mContext) throws JSONException{
        List<HotlinePhone> mHotlinePhones = new ArrayList<>();
        SharedPreferences mSharedPreferences = mContext.getSharedPreferences(PREF_HELPFUL_CONTACT,Context.MODE_PRIVATE);
        String json = mSharedPreferences.getString(PREF_HELPFUL_CONTACT,null);
        JSONArray mJsonArray = new JSONArray(json);
        for (int i =0 ; i< mJsonArray.length();i++)
            mHotlinePhones.add(new HotlinePhone(mJsonArray.getJSONObject(i)));
        return mHotlinePhones;
    }

    public static void setSearchNearby(Context context, List<Hospitality> arrpoi) {
        if (context == null)
            return;
        JSONArray jsonArray = new JSONArray();
        for (Hospitality poi : arrpoi) {
            jsonArray.put(poi.toJson());
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NEAR_BY_POI, Context.MODE_PRIVATE);
        String saveObj = sharedPreferences.getString(PREF_NEAR_BY_POI, "");
        sharedPreferences.edit().putString(PREF_NEAR_BY_POI,jsonArray.toString().trim()).commit();

    }

    public static List<Hospitality> getSearchNearby (Context mContext) throws JSONException{
        List<Hospitality> mHotlinePhones = new ArrayList<>();
        SharedPreferences mSharedPreferences = mContext.getSharedPreferences(PREF_NEAR_BY_POI,Context.MODE_PRIVATE);
        String json = mSharedPreferences.getString(PREF_NEAR_BY_POI,null);
        JSONArray mJsonArray = new JSONArray(json);
        for (int i =0 ; i< mJsonArray.length();i++)
            mHotlinePhones.add(new Hospitality(mJsonArray.getJSONObject(i)));
        return mHotlinePhones;
    }

}
