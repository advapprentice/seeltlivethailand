package com.touchtechnologies.tit.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.support.annotation.Nullable;
import android.util.Log;

import com.touchtechnologies.tit.AppConfig;

import java.util.Random;

public class PushRegisterUtil {
	private static final String SHARE_PREFERENCE = "PUSH_PREF"; 
	private static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	
	
	/**
	 * @return Application's {@code Sh aredPreferences}.
	 */
	private static SharedPreferences getGCMPreferences(Context context) {		
		//TODO remove this comment -> each user use difference key to store a share preference separately per user per project key
		
//		String userPreferenceKey = SHARE_PREFERENCE + UserUtil.getUser(context).getUsername().hashCode();
//		userPreferenceKey += ResourceUtil.getGoogleProjectID(context);
		
//		Log.d(AppConfig.LOG, "user pref key[" + userPreferenceKey + "]");
		
	    return context.getSharedPreferences(SHARE_PREFERENCE , Context.MODE_PRIVATE);
	}
	
	/**
	 * Clear default share preference
	 * @param context
	 */
	public static void clear(Context context){
		context.getSharedPreferences(SHARE_PREFERENCE, Context.MODE_PRIVATE).edit().clear();
	}

	@Nullable
	private static String getRegistrationKey(Context context){
		try{
			Random rand = new Random();
			int value = rand.nextInt(50);
			String ggg = PROPERTY_REG_ID + "_GID" + ResourceUtil.getGoogleProjectID(context) + "_UID" + UserUtil.getUser(context).getFirstName().hashCode()+value;
			Log.d("tag", "getRegistrationKey: "+ggg);
			return ggg;
		}catch (Exception e){
			return null;
		}

	}
	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 *
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	public static String getRegistrationId(Context context) {
		
		SharedPreferences userPrefs = getGCMPreferences(context);
		String registrationId = userPrefs.getString(getRegistrationKey(context), "");

		if (registrationId.isEmpty()) {
			Log.i(AppConfig.LOG, "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = userPrefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(AppConfig.LOG, "App version changed.");
			return "";
		}
		
		Log.d(AppConfig.LOG, "PUSH[" + registrationId+ "]");

		return registrationId;
	}
	
	/**
	 * Stores the registration ID and app versionCode in the application's
	 * {@code SharedPreferences}.
	 *
	 * @param context application's context.
	 * @param regId registration ID
	 */
	public static void storeRegistrationId(Context context, String regId) {
	    SharedPreferences prefs = getGCMPreferences(context);
	    int appVersion = getAppVersion(context);
	    Log.i(AppConfig.LOG, "Saving regId on app version " + appVersion);
	    
	    clear(context);
	    
	    SharedPreferences.Editor editor = prefs.edit();
	    editor.putString(getRegistrationKey(context), regId);
	    editor.putInt(PROPERTY_APP_VERSION, appVersion);
	    editor.commit();
	}
	
	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
	    try {
	        PackageInfo packageInfo = context.getPackageManager()
	                .getPackageInfo(context.getPackageName(), 0);
	        return packageInfo.versionCode;
	    } catch (NameNotFoundException e) {
	        // should never happen
	        throw new RuntimeException("Could not get package name: " + e);
	    }
	}
}
