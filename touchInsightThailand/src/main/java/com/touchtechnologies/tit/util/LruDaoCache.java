package com.touchtechnologies.tit.util;

import android.support.v4.util.LruCache;

public class LruDaoCache extends LruCache<String, String>{
	private static LruCache<String, String> instance;
	
	private LruDaoCache() {
		super(128);
	}	
	
	public static LruCache<String, String> getInstance(){
		if(instance == null){
			instance = new LruDaoCache();
		}
		
		return instance;
	}
}
