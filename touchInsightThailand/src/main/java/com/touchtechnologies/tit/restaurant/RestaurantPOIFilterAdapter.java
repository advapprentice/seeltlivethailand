package com.touchtechnologies.tit.restaurant;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.touchtechnologies.tit.POIAdapter;
import com.touchtechnologies.tit.R;


public class RestaurantPOIFilterAdapter extends POIAdapter{

	public RestaurantPOIFilterAdapter(Context context, int resLayout) {
		super(context, resLayout);
		
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
				.showImageForEmptyUri(R.drawable.bg_noimage)
				.showImageOnFail(R.drawable.bg_noimage)
				.showImageOnLoading(R.drawable.icon_food)
				.cacheInMemory(true)
			    .displayer(new FadeInBitmapDisplayer(300)) //fade in images
			    .resetViewBeforeLoading(true).build();
		imageLoader = ImageLoader.getInstance();  
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		
		if(convertView == null){
			LayoutInflater inflater = LayoutInflater.from(context);
			convertView = inflater.inflate(resLayout, parent, false);
			
			viewHolder = new ViewHolder();
			convertView.setTag(viewHolder);
			
			viewHolder.imageView = (ImageView)convertView.findViewById(R.id.imagerastaurant);
			viewHolder.textView  = (TextView)convertView.findViewById(R.id.textname);
			
		}else{
			viewHolder = (ViewHolder)convertView.getTag();
		}
		viewHolder.textView.setText(getItem(position).getName());
		imageLoader.displayImage(getItem(position).getImage().getThumbnail(), viewHolder.imageView, options);
		
		return convertView;
	}
	
	class ViewHolder{
		ImageView imageView;
		TextView textView;
		
	}
}
