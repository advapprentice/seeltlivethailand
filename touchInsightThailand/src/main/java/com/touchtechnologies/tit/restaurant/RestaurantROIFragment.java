package com.touchtechnologies.tit.restaurant;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.ROIAdapter;
@SuppressLint("ValidFragment")
public class RestaurantROIFragment extends Fragment{
	private ROIAdapter listAdapter;
	
	public RestaurantROIFragment(){
	}
	
	public RestaurantROIFragment(ROIAdapter listAdapter) {
		this.listAdapter = listAdapter;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			 ViewGroup container, Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_restaurant_where_to_go, container, false);
		
		//register onClick activity
		view.findViewById(R.id.restaurantWhere2Go).setOnClickListener((RestaurantTabsActivity)getActivity());
		view.findViewById(R.id.restaurantNearMe).setOnClickListener((RestaurantTabsActivity)getActivity());
		
		GridView gridView = (GridView)view.findViewById(R.id.gridViewRestaurantROIs);
		gridView.setAdapter(listAdapter);
		gridView.setOnItemClickListener((RestaurantTabsActivity)getActivity());
		
		return view;
	}
	
	
}
