package com.touchtechnologies.tit.restaurant;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.touchtechnologies.tit.R;
@SuppressLint("ValidFragment")
public class RestaurantPOIFragment extends Fragment{
	private ListAdapter listAdapter;
	
	public RestaurantPOIFragment(ListAdapter listAdapter) {
		this.listAdapter = listAdapter;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_restaurant_near_me, container, false);
		ListView listView = (ListView)view.findViewById(R.id.listViewRestaurants);
		listView.setAdapter(listAdapter);
		listView.setOnItemClickListener((RestaurantTabsActivity)getActivity());
		
		//register onClick activity
		view.findViewById(R.id.restaurantWhere2Go).setOnClickListener((RestaurantTabsActivity)getActivity());
		view.findViewById(R.id.restaurantNearMe).setOnClickListener((RestaurantTabsActivity)getActivity());
		
		return view;
	}
}
