package com.touchtechnologies.tit.restaurant;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.touchtechnologies.dataobject.DataItem;
import com.touchtechnologies.dataobject.Restaurant;
import com.touchtechnologies.tit.POIAdapter;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.R.id;


public class RestaurantNearByAdapter extends POIAdapter implements OnClickListener{
	DataItem respoi;
	Restaurant restaurant;
	public RestaurantNearByAdapter(Context context, int resLayout) {
		super(context, resLayout);
		
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
				.showImageForEmptyUri(R.drawable.bg_noimage)
				.showImageOnFail(R.drawable.bg_noimage)
				.cacheInMemory(true)
			    .displayer(new FadeInBitmapDisplayer(300)) //fade in images
			    .resetViewBeforeLoading(true).build();
		imageLoader = ImageLoader.getInstance();  
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;

		
		if(convertView == null){
			LayoutInflater inflater = LayoutInflater.from(context);
			convertView = inflater.inflate(resLayout, parent, false);
			viewHolder = new ViewHolder();
			convertView.setTag(viewHolder);
			viewHolder.viewnearme = (View)convertView.findViewById(R.id.viewNearme);
			viewHolder.getdirection = (Button)convertView.findViewById(R.id.buttonGetDirection);
			viewHolder.imageCover = (ImageView)convertView.findViewById(R.id.imageViewRestaurant);
			viewHolder.textViewName =(TextView)convertView.findViewById(id.textViewName);
			viewHolder.textViewHighlight =(TextView)convertView.findViewById(id.textViewHighlight);
			viewHolder.textViewDistance =(TextView)convertView.findViewById(id.textViewDistance);
			viewHolder.getdirection.setOnClickListener(this);
			viewHolder.viewnearme.setOnClickListener(this);
			
		}else{
			viewHolder = (ViewHolder)convertView.getTag();
		}
		viewHolder.getdirection.setTag(getItem(position));
		viewHolder.viewnearme.setTag(getItem(position));
		
		respoi= getItem(position);
	
		
		viewHolder.textViewName.setText(getItem(position).getName());
		viewHolder.textViewDistance.setText(getItem(position).getDistance()+" Km");
		//		viewHolder.tvcreatedate.setText(getItem(position).getCreated());
		imageLoader.displayImage(getItem(position).getImage().getThumbnail(), viewHolder.imageCover, options);
	    
		
		return convertView;
	}
	
	class ViewHolder{
		View  viewnearme;
		ImageView imageCover;
		TextView textViewName;
		TextView textViewHighlight;
		TextView textViewDistance;
		Button getdirection;
		
	}

	@Override
	public void onClick(View v) {
		respoi = (DataItem)v.getTag();
		switch (v.getId()) {
		case R.id.buttonGetDirection:
			Uri gmmIntentUri = Uri.parse("google.navigation:"+respoi.getLocation())
			.buildUpon()
            .appendQueryParameter("q",respoi.getName())
            .build();
	//		Uri gmmIntentUri = Uri.parse("google.navigation:q="+hotel.getName()+","+hotel.getLatitude()+","+hotel.getLongitude()+"&mode=b");
			Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
			mapIntent.setPackage("com.google.android.apps.maps");
			context.startActivity(mapIntent);
			break;

		case R.id.viewNearme:
			
			Intent intent = new Intent(context, RestaurantActivity.class);
			intent.putExtra(Restaurant.class.getName(),respoi) ;
	//		Toast.makeText(context, "test"+respoi.getName(),Toast.LENGTH_SHORT).show();
			context.startActivity(intent);
			break;

		
		}
		
	}

}
