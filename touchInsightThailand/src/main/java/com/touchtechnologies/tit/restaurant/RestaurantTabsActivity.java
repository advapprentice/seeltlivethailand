package com.touchtechnologies.tit.restaurant;

import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ListView;

import com.touchtechnologies.command.insightthailand.RestServiceNearbyCommand;
import com.touchtechnologies.command.insightthailand.RestServiceROIListCommand;
import com.touchtechnologies.dataobject.DataItem;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.dataobject.Restaurant;
import com.touchtechnologies.tit.POIAdapter;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.ROIAdapter;
import com.touchtechnologies.tit.search.SearchActionbarActivity;
import com.touchtechnologies.tit.util.ResourceUtil;

public class RestaurantTabsActivity extends SearchActionbarActivity implements OnClickListener, 
	OnItemClickListener{
	//Restaurant ROI item view/ adapter
	private ROIAdapter listROIAdapter;
	private POIAdapter listPOIAdapter;
	double latitude;
	double longitude ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		setContentView(R.layout.activity_restaurant_rois);
		
		
		
		
		
		listROIAdapter = new ROIAdapter(this, R.layout.item_grid_circle);
		listPOIAdapter = new RestaurantNearByAdapter(this, R.layout.item_restaurant_near_by);
		
		FragmentManager frManager = getSupportFragmentManager();
		frManager.beginTransaction().add(R.id.fragment_container, new RestaurantROIFragment(listROIAdapter)).commit();
		
		mTitle = getString(R.string.menu_tit_restaurant);
		
		/*
		 * get ROIs
		 */
		String urlROIs = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_rois);
		
		new RestServiceROIListCommand(this, urlROIs){
	
			protected void onPostExecute(java.util.List<ROI> result) {
				listROIAdapter.setData(result);
			}
		}.execute();
	
		/*
		 * get nearby restaurant POIs
		 */
	
	
		LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE); 
	    LocationListener locationListener = new LocationListener() {
             public void onLocationChanged(Location location) {
               // Called when a new location is found by the network location provider.

                 double longitude = location.getLongitude();
                 double latitude = location.getLatitude();

             }

             public void onStatusChanged(String provider, int status, Bundle extras) {}

             public void onProviderEnabled(String provider) {}

             public void onProviderDisabled(String provider) {}
           };

         // Register the listener with the Location Manager to receive location updates
         lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

         String locationProvider = LocationManager.NETWORK_PROVIDER;
         // Or use LocationManager.GPS_PROVIDER

         Location lastKnownLocation = lm.getLastKnownLocation(locationProvider);
         if (lastKnownLocation!=null){
        	 longitude = lastKnownLocation.getLongitude();
        	 latitude = lastKnownLocation.getLatitude();
         }else{
        	AlertDialog alertDialog = new Builder(this).create();
 			alertDialog.setIcon(R.drawable.ic_warning);
 			alertDialog.setMessage("Near Me Need turn on location ");
 			alertDialog.setTitle("Warning Please turn on location!");
 			alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
 				
 				@Override
 				public void onClick(DialogInterface dialog, int which) {
 					dialog.dismiss();
 				}
 			});
 			alertDialog.show();
         }
		
		Log.d("longitude","Lat:"+longitude);
		Log.d("latitude","Long:"+latitude);
		
		
	//	String urlPOIs = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_pois);
		String urlPOIs = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_NearByMe)+latitude+"/"+longitude+"/";
		System.out.print("test"+urlPOIs);
		
		new RestServiceNearbyCommand(this, null, "restaurant", urlPOIs){
			protected void onPostExecute(List<DataItem> result) {
//				Log.d("near by restaurant", result == null?"-":result.toString());
				listPOIAdapter.setData(result);
			};
			
		}.execute();
	}


	@Override
	public void onClick(View v) {
		FragmentManager frManager = getSupportFragmentManager();
		
		switch(v.getId()){
		case R.id.restaurantWhere2Go:
			frManager.beginTransaction().replace(R.id.fragment_container, new RestaurantROIFragment(listROIAdapter)).commit();
			break;
		case R.id.restaurantNearMe:
			frManager.beginTransaction().replace(R.id.fragment_container, new RestaurantPOIFragment(listPOIAdapter)).commit();
			break;
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			//POI clicked
		if(parent instanceof ListView){
			Intent intent = new Intent(this, RestaurantActivity.class);
			intent.putExtra(Restaurant.class.getName(), listPOIAdapter.getItem(position));
			startActivity(intent);
		
			//ROI clicked
		}else if(parent instanceof GridView){
			Intent intent = new Intent(this, RestaurantListActivity.class);
			intent.putExtra(ROI.class.getName(), listROIAdapter.getItem(position));
			startActivity(intent);
		}
		
	}

}
