package com.touchtechnologies.tit;

import org.apache.http.HttpStatus;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.LoginCommand;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.cctvitlive.CCTVMenuMainActivity;
import com.touchtechnologies.tit.login.LoginsActivity;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.util.PushRegisterUtil;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

public class LoginAsynTask extends CommonServiceTask {

	public LoginAsynTask(FragmentActivity context) {
		super(context, context.getString(R.string.wait), null,null);
	}

	@Override
	protected ServiceResponse doInBackground(Command... params) {
		LoginCommand command = (LoginCommand)params[0];
		
		User user = command.getUser();
		Log.d("LoginAsynTask", "login user " + user.toString());
		
		String url = ResourceUtil.getServiceUrl(context) 
				+ context.getString(R.string.app_service_login);
		
		String signSource = user.getSignupSource();
		signSource = (signSource == null)?User.SIGNUP_SOURCE_SYSTEM:signSource;
		
		switch(signSource){
		case User.SIGNUP_SOURCE_FACEBOOK:
			url += "/Facebook";
			break;
		case User.SIGNUP_SOURCE_GOOGLEPLUS:
			url += "Google";
		}
		
		ServiceConnector srConnector = new ServiceConnector(url);
		ServiceResponse serviceResponse = srConnector.doAsynPost(params[0], false);

		try {
			int code = serviceResponse.getCode();
			if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
				JSONObject json = new JSONObject(serviceResponse.getContent().toString());
				Log.d("POST", "doPost login : " + json);

				// persist user session
				// User user = new User(json.getJSONObject("data"));
	        	User userReturned = new User(json);
				userReturned.setSignupSource(user.getSignupSource());
				userReturned.setLoggedIn(true);
			
				UserUtil.persist(context, userReturned);
				PushRegisterUtil.clear(context);
				
			} else {
				Log.e(AppConfig.LOG, "doPost login failed :" + code
						+ serviceResponse.getMessage());
			}

		} catch (Exception e) {
			serviceResponse.setCode(500);
			serviceResponse.setMessage(e.getMessage());
			Log.e(AppConfig.LOG, "doInBackground error", e);
		}

		return serviceResponse;
	}

	@Override
	protected void onPostExecute(ServiceResponse result) {
		super.onPostExecute(result);

		try {
			if (result.getCode() == ServiceResponse.SUCCESS
					|| result.getCode() == HttpStatus.SC_OK) {

				dialog.dismiss();
				// launch main activity
				Intent intent = new Intent(context, CCTVMenuMainActivity.class);
			//	Intent intent = new Intent(context, CCTVMenuMainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				context.startActivity(intent);

				context.setResult(Activity.RESULT_OK);
				context.finish();

			} else {
				dialog.dismiss();
				AlertDialog alertDialog = new Builder(context).create();
				alertDialog.setTitle(result.getMessage());
				alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								if(!(context instanceof LoginsActivity)){
									context.finish();
								}
							}
						});
				alertDialog.show();

			}
		} catch (Exception e) {
			Toast.makeText(context, "ERROR!\r\n" + e.getMessage(),
					Toast.LENGTH_LONG).show();
			Log.e(AppConfig.LOG, "process response error", e);
		}
	}
}
