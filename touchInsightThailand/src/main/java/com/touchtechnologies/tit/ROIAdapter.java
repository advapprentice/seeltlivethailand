package com.touchtechnologies.tit;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.opensource.pagerindicator.CircleBitmapDisplayer;
import com.touchtechnologies.dataobject.ROI;

/**
 * Simple ROI adapter that create a simple image and title
 * @author Touch
 *
 */
public class ROIAdapter extends BaseAdapter{
	private List<ROI> rois;
	private Context context;
	private int resLayout;
	Bitmap bitmap;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	
	public ROIAdapter(Context context, int resLayout) {
		this.context = context;
		this.resLayout = resLayout;
		
		imageLoader = ImageLoader.getInstance();
		


        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
	        .threadPoolSize(3)
			.threadPriority(Thread.NORM_PRIORITY - 2)
			.denyCacheImageMultipleSizesInMemory()
            .memoryCacheSize(4194304)
            .defaultDisplayImageOptions(options).build();
		
		imageLoader.init(config);
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
			//	.cacheOnDisc(true).bitmapConfig(Bitmap.Config.ARGB_8888)
			//	.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.NONE)
				.showImageForEmptyUri(R.drawable.bg_noimage_circle)
				.showImageOnFail(R.drawable.bg_noimage_circle)
				.showImageOnLoading(R.drawable.bg_loading_circle)
				.cacheInMemory(true)
				.showImageOnLoading(android.R.color.transparent)
				.displayer(new FadeInBitmapDisplayer(1000)) //fade in images
			//	.displayer(new RoundedBitmapDisplayer((int) 125.5f))
			    .displayer(new CircleBitmapDisplayer())
				.build();
		
		//		 imageLoader = ImageLoader.getInstance();  
	}
	
	public void setData(List<ROI> rois){
		this.rois = rois;
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return rois == null?0:rois.size();
	}
	
	@Override
	public ROI getItem(int position) {
		return rois.get(position);
	}
	
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder vh = null;
		
		if(convertView == null){
			convertView = LayoutInflater.from(context).inflate(resLayout, parent, false);
			
			vh = new ViewHolder();
			vh.imageView = (ImageView)convertView.findViewById(R.id.imageViewRoi);
			vh.textView  = (TextView)convertView.findViewById(R.id.textViewRoiName);
			
			Matrix m = vh.imageView.getImageMatrix();
			float imageHeight = 100;
			float imageWidth =100;
			RectF drawableRect = new RectF(0, 0, imageWidth, imageHeight);
			RectF viewRect = new RectF(0, 0, vh.imageView.getWidth(), vh.imageView.getHeight());
			m.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.CENTER);
			
			vh.imageView.setImageMatrix(m);
			
			convertView.setTag(vh);
		}else{
			vh = (ViewHolder)convertView.getTag();
		}
		
		vh.textView.setText(getItem(position).getName());
		imageLoader.displayImage(getItem(position).getImageUrlLogo(), vh.imageView, options);
		
		return convertView;
	}
	
	class ViewHolder{
		ImageView imageView;
		TextView textView;
	}
}
