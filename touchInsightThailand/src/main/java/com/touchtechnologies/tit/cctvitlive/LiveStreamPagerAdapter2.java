package com.touchtechnologies.tit.cctvitlive;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.util.SparseArrayCompat;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.Toast;

import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.tit.history.LiveHistoryGridviewFragment;
import com.touchtechnologies.tit.live.category.ListCategoryFragment;
import com.touchtechnologies.tit.live.category.ScrollTabHolder;
import com.touchtechnologies.tit.live.category.ScrollTabHolderFragment;
import com.touchtechnologies.tit.live.mylivestream.FragmentMyliveStream;
import com.touchtechnologies.tit.member.MemberLiveStream;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by TouchICS on 3/24/2016.
 */
public class LiveStreamPagerAdapter2 extends FragmentPagerAdapter {

    int PAGE_COUNT = 3;
    private LiveHistoryGridviewFragment fragment1;
    private ListCategoryFragment fragment2;
    private FragmentMyliveStream fragment3;
    // Tab Titles
    private SparseArrayCompat<ScrollTabHolder> mScrollTabHolders;
    private ScrollTabHolder mListener;
    private ArrayList<String> tabtitles = new ArrayList<>();
    private Context context;
    private String tabTitles[] = new String[] { "History", "Category", "My Live" };
    private List<LiveStreamListener> liveListener;
    private List<LiveStreamingChannel> liveContent;
    int liveSize;
    public LiveStreamPagerAdapter2(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
        mScrollTabHolders = new SparseArrayCompat<ScrollTabHolder>();

        liveListener = new ArrayList<>();
    }

    public void setLiveStreamContent(List<LiveStreamingChannel> list){
        this.liveContent = list;

        for(LiveStreamListener listener: liveListener){
            listener.ready(list);
        }
    }

    public void setTabHolderScrollingContent(ScrollTabHolder listener) {
        mListener = listener;
    }


    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment3;
		switch (position) {
			// Open FragmentTab1.java
		case 0:

            ScrollTabHolderFragment fragment1 = (ScrollTabHolderFragment) LiveHistoryGridviewFragment.newInstance(0);
            liveListener.add((LiveHistoryGridviewFragment) fragment1);
            mScrollTabHolders.put(0, fragment1);
            if (mListener != null) {
                fragment1.setScrollTabHolder(mListener);

            }

            fragment1.getArguments().putInt("liveSize", liveContent == null?0:liveContent.size());

            return fragment1;
			// Open FragmentTab2.java
		case 1:
            ScrollTabHolderFragment fragment2 = (ScrollTabHolderFragment) ListCategoryFragment.newInstance(1);
            liveListener.add((ListCategoryFragment) fragment2);
            mScrollTabHolders.put(1, fragment2);
            if (mListener != null) {
                fragment2.setScrollTabHolder(mListener);
         //
            }
            fragment2.getArguments().putInt("liveSize", liveContent==null?0:liveContent.size());

            return fragment2;
		case 2:

            fragment3 = FragmentMyliveStream.newInstance(position);

            fragment3.getArguments().putInt("liveSize", liveContent==null?0:liveContent.size());

            return fragment3;

		}
		return null;

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
        // save the appropriate reference depending on position
        switch (position) {
            case 0:
                fragment1 = (LiveHistoryGridviewFragment) createdFragment;
                fragment1.getArguments().putInt("liveSize", liveContent == null?0:liveContent.size());
                break;
            case 1:
                fragment2 = (ListCategoryFragment) createdFragment;
                fragment1.getArguments().putInt("liveSize", liveContent == null?0:liveContent.size());
                break;
            case 2:
                fragment3 = (FragmentMyliveStream) createdFragment;
                break;
        }
        return createdFragment;
    }


    public SparseArrayCompat<ScrollTabHolder> getScrollTabHolders() {
        return mScrollTabHolders;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
