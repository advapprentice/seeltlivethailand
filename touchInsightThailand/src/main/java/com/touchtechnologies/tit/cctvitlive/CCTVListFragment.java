package com.touchtechnologies.tit.cctvitlive;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.touchtechnologies.animate.dialog.MaterialDialog;
import com.touchtechnologies.animate.progress.RotateLoading;
import com.touchtechnologies.app.TransparentDialog;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.Category;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.dataobject.insightthailand.CategoryLiveStream;
import com.touchtechnologies.json.JSONUtil;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.BuildConfig;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.banners.ViewStatisticFragment;
import com.touchtechnologies.tit.cctv.CCTVDetailActivity;
import com.touchtechnologies.tit.dataobject.Category_video;
import com.touchtechnologies.tit.dataobject.HotlinePhone;
import com.touchtechnologies.tit.dataobject.MobileActivityPropertiesPopup;
import com.touchtechnologies.tit.dataobject.PopupDao;
import com.touchtechnologies.tit.hotline.Hotline_activity;
import com.touchtechnologies.tit.login.RegistersActivity;
import com.touchtechnologies.tit.search.MySingleton;
import com.touchtechnologies.tit.util.DataCacheUtill;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.wifi.WIFIMod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CCTVListFragment extends ListFragment {
	protected int action_refresh;
	private RotateLoading rotateLoading;
	private CCTVListsAdapter adapter;
	private List<ROI> listRoi;
	private ListView cctvlistView;
	private View viewEmptyHint;
	final String TAG = "CCTV-FRAGMENT";
	private LayoutInflater inflater;
	private  ImageLoader imageLoader;
	private DisplayImageOptions options;
	private MaterialDialog	mMaterialDialog;

	public CCTVListFragment(){

	}
	
	@SuppressLint("ValidFragment")
	public CCTVListFragment(List<ROI> liRois) {
		this.listRoi = liRois;
	}
	
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		adapter = new CCTVListsAdapter(getActivity());
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().cacheInMemory(false)
				.cacheOnDisc(false).bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.NONE)
				.showImageForEmptyUri(R.drawable.bg_cctvdefault)
				.showImageOnFail(R.drawable.bg_cctvdefault)
				.showImageOnLoading(R.drawable.bg_cctvdefault)
				.resetViewBeforeLoading(false).build();
		imageLoader = ImageLoader.getInstance();

		imageLoader = ImageLoader.getInstance();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.inflater =inflater;
		View listViewContainer = inflater.inflate(R.layout.fragment_mobilelive_list_cctv, container, false);
		rotateLoading = (RotateLoading) listViewContainer.findViewById(R.id.rotateloading);
		rotateLoading.start();
		cctvlistView = (ListView) listViewContainer.findViewById(android.R.id.list);
		viewEmptyHint = (View) listViewContainer.findViewById(R.id.emptyAgentHint);

		if (!WIFIMod.isNetworkConnected(getActivity())) {
			viewEmptyHint.setVisibility(View.VISIBLE);
		}

		//add header
		LayoutInflater infalter = getLayoutInflater(savedInstanceState);
		ViewGroup header = (ViewGroup) infalter.inflate(
				R.layout.item_banner_total_view, cctvlistView, false);
		cctvlistView.addHeaderView(header);



		adapter.setData(listRoi);
		setListAdapter(adapter);


		if(MainApplication.getPopup_check()) {
			//Get_Popup_Form_Api();
		}
//		if(AppConfig.check_update){
//			GetCheckVersion();
//		}
//			get_Category();
//			get_hotline();
		
		return listViewContainer;
	}
	
	
	/**
	 * Update statistic banners
	 */
	void updateFragmentViewStatistic(List<JSONObject> listViewStatistic){
		// update banners view
	
			try{
				Fragment fragment = getChildFragmentManager().findFragmentById(R.id.fragmentBanners);
				((ViewStatisticFragment)fragment).setData(listViewStatistic);
			}catch(Exception e){
				Log.e(TAG, "update state view error", e);
			}
		
		
	}
	
	public CCTVListsAdapter getAdapter() {
		if (adapter != null) {
			rotateLoading.setVisibility(View.GONE);

		} else {

			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				public void run() {
					rotateLoading.setVisibility(View.GONE);
				}
			}, 1000);
		}

		return adapter;
	}

	@Override
	public void onResume() {
		super.onResume();

	//	if(adapter.getCount() == 0)
	//		cctvlistView.setEmptyView(viewEmptyHint);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (adapter != null) {
			adapter.onDestroyed();
			Log.i(TAG, "-destroyed");
		}
		imageLoader.clearMemoryCache();

	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {

		Intent intent = new Intent(getActivity(), CCTVDetailActivity.class);
		intent.putExtra(CCTV.class.getName(), adapter.getItem(position));
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}

//	private void Get_Popup_Form_Api(){
//		try {
//
//			String url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_popup);
//			JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
//				@Override
//				public void onResponse(JSONObject jsonObject) {
//					try {
//
//						setpopupfromapi(new PopupDao(jsonObject));
//					} catch (JSONException e) {
//						e.printStackTrace();
//					}
//				}
//			}, new Response.ErrorListener() {
//				@Override
//				public void onErrorResponse(VolleyError volleyError) {
//					Log.d(TAG, "onErrorResponse: ");
//					return;
//				}
//			});
//			MySingleton.getInstance(getActivity().getApplicationContext()).addToRequestQueue(jsonObjectRequest);
//
//			//	return true;
//		}catch (Exception e){
//			MainApplication.getInstance().trackException(e);
//		}
//	}

//	private void setpopupfromapi(PopupDao popupDao){
//		Button button1 = null,button2 = null;
//		//GradientDrawable drawable = (GradientDrawable)
//		final Button[] buttons = {button1,button2};
//		final ImageView imageView;
//		if(getActivity() == null)
//			return;
//			if(popupDao.getTemplate_id() == 1) {
//				final TransparentDialog alertDialog = new TransparentDialog(getActivity(), R.layout.item_dialog_popup_gopro);
//				final TransparentDialog transparentDialog = alertDialog;
//
//				//alertDialog.findViewById(R.id.image_popup_1);
//				transparentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//				button1 = (Button) alertDialog.findViewById(R.id.btn_popup_0);
//				button2 = (Button) alertDialog.findViewById(R.id.btn_popup_1);
//				buttons[0] = button1;
//				buttons[1] = button2;
//				imageView = (ImageView) alertDialog.findViewById(R.id.image_popup_1);
//				final MobileActivityPropertiesPopup MAPP = popupDao.getimagefromMobileActivityProperties();
//
//				if (MAPP != null)
//					imageLoader.displayImage(MAPP.getImage_url(), imageView, options);
//				imageView.setBackground(new ColorDrawable(Color.TRANSPARENT));
//				ArrayList<MobileActivityPropertiesPopup> MAPPBUTTON = popupDao.getButtonfromMobileActivityPropertiesPopups();
//
//				if (MAPPBUTTON.size() > 0) {
//					for (int i = 0; i < MAPPBUTTON.size(); i++) {
//						final Button tmp = buttons[i];
//						tmp.setText(MAPPBUTTON.get(i).getText());
//						tmp.setTextColor(Color.parseColor(MAPPBUTTON.get(i).getText_color()));
//						tmp.setBackgroundColor(Color.parseColor(MAPPBUTTON.get(i).getBackground_color()));
//						tmp.setTag(MAPPBUTTON.get(i).getPopup_action());
//
//
//					}
//				}
//
//
//				alertDialog.findViewById(R.id.btn_popup_0).setOnClickListener(
//						new OnClickListener() {
//
//							@Override
//							public void onClick(View v) {
//								MainApplication.setPopup_check(false);
//								transparentDialog.dismiss();
//
//							}
//						});
//
//				alertDialog.findViewById(R.id.btn_popup_1).setOnClickListener(
//						new OnClickListener() {
//							Intent intent;
//
//							@Override
//							public void onClick(View v) {
//								switch ((int) v.getTag()) {
//									case 1:
//										intent = new Intent(getActivity(), RegistersActivity.class);
//										startActivity(intent);
//										MainApplication.setPopup_check(false);
//										transparentDialog.dismiss();
//										break;
//									case 2:
//										((CCTVMenuMainActivity) getActivity()).changepageFragment(2);
//										MainApplication.setPopup_check(false);
//										transparentDialog.dismiss();
//										break;
//									case 4:
//										((CCTVMenuMainActivity) getActivity()).changepageFragment(4);
//										MainApplication.setPopup_check(false);
//										transparentDialog.dismiss();
//										break;
//									case 5:
//										((CCTVMenuMainActivity) getActivity()).changepageFragment(5);
//										MainApplication.setPopup_check(false);
//										transparentDialog.dismiss();
//										break;
//									default:
//
//										break;
//								}
//
//
//							}
//						});
//
//				alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//				alertDialog.show();
//			}else {
//				final TransparentDialog alertDialog = new TransparentDialog(getActivity(), R.layout.item_dialog_popup_iwatch);
//				final TransparentDialog transparentDialog = alertDialog;
//
//				transparentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//				imageView = (ImageView) alertDialog.findViewById(R.id.image_popup_iwatch);
//				final MobileActivityPropertiesPopup MAPP = popupDao.getimagefromMobileActivityProperties();
//
//				if (MAPP != null)
//					imageLoader.displayImage(MAPP.getImage_url(), imageView, options);
//				imageView.setBackground(new ColorDrawable(Color.TRANSPARENT));
//
//				imageView.setOnClickListener(new OnClickListener() {
//					@Override
//					public void onClick(View v) {
//						Intent intent;
//						switch ((int) v.getTag()) {
//							case 1:
//								intent = new Intent(getActivity(), RegistersActivity.class);
//								startActivity(intent);
//								MainApplication.setPopup_check(false);
//								transparentDialog.dismiss();
//								break;
//							case 2:
//								((CCTVMenuMainActivity) getActivity()).changepageFragment(2);
//								MainApplication.setPopup_check(false);
//								transparentDialog.dismiss();
//								break;
//							case 4:
//								((CCTVMenuMainActivity) getActivity()).changepageFragment(4);
//								MainApplication.setPopup_check(false);
//								transparentDialog.dismiss();
//								break;
//							case 5:
//								((CCTVMenuMainActivity) getActivity()).changepageFragment(5);
//								MainApplication.setPopup_check(false);
//								transparentDialog.dismiss();
//								break;
//							default:
//
//								break;
//						}
//					}
//				});
//
//				transparentDialog.setCanceledOnTouchOutside(true);
//
//				transparentDialog.show();
//			}
//
//	}

	private void GetCheckVersion(){
		String url = ResourceUtil.getServiceUrl(getActivity()) +getResources().getString(R.string.app_service_checkversion);
		JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject jsonObject) {
				Log.d(TAG, "onResponse: "+ jsonObject.toString());
				ShowAlertDialog(jsonObject);
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				Log.d(TAG, "onErrorResponse: "+volleyError.getMessage());
			}
		}){
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				String versionName = BuildConfig.VERSION_NAME;
				HashMap<String, String> mapheader = new HashMap<>();
				mapheader.put("x-tit-os","android");
				mapheader.put("x-tit-version",versionName);
				return mapheader;
			}
		};
		try {
			Map<String, String> header = objectRequest.getHeaders();
			Log.d(TAG, "getcheckversion: "+header);
		} catch (AuthFailureError authFailureError) {
			authFailureError.printStackTrace();
		}
		MainApplication.getInstance().addToRequestQueue(objectRequest);


	}

	private void ShowAlertDialog(JSONObject jsonObject){
		if(0 == JSONUtil.getInt(jsonObject,"status")){
			try {
				JSONObject dataJsonObject = jsonObject.getJSONObject("data");
				if(JSONUtil.getBoolean(dataJsonObject,"status_update")){

					if(JSONUtil.getBoolean(dataJsonObject,"force_update")) {
						Call_Update();
						getActivity().finish();
					}else {
					    mMaterialDialog = new MaterialDialog(getActivity());
						mMaterialDialog.setTitle("New Update "+dataJsonObject.getString("last_version"))
								.setMessage(dataJsonObject.getString("detail"))
										//mMaterialDialog.setBackgroundResource(R.drawable.background);
								.setPositiveButton("OK", new View.OnClickListener() {
									@Override public void onClick(View v) {
										mMaterialDialog.dismiss();
										Call_Update();
									}
								})
								.setNegativeButton("CANCEL",
										new View.OnClickListener() {
											@Override public void onClick(View v) {
												mMaterialDialog.dismiss();
											//	Toast.makeText(getActivity(),"Cancel", Toast.LENGTH_LONG).show();
											}
										})
								.setCanceledOnTouchOutside(true)
								.setOnDismissListener(
										new DialogInterface.OnDismissListener() {
											@Override
											public void onDismiss(DialogInterface dialog) {
											//	Toast.makeText(getActivity(),"onDismiss",Toast.LENGTH_SHORT).show();
											}
										})
								.show();

					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	* sent user to play store
	 *
	 *
	* */
	private void Call_Update(){
		Intent intent;
		final String appPackageName = getActivity().getPackageName();
		try {
			intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
		}catch (Exception e){
			intent = new Intent(Intent.ACTION_VIEW,  Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
		}
		startActivity(intent);
	}

	private void get_Category(){
		String url = ResourceUtil.getServiceUrl(getActivity())+getResources().getString(R.string.app_service_rest_category_stream);
		JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
			@Override
			public void onResponse(JSONArray jsonArray) {
				//Log.d(TAG, "onResponse: "+jsonArray.toString());
				try{
					DataCacheUtill.setCategoryStream(getActivity(),new Category_video(jsonArray));
				}catch (Exception e){
					e.getMessage();
				}

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				Log.d(TAG, "onErrorResponse: ");
			}
		}){

		};
		MainApplication.getInstance().addToRequestQueue(jsonArrayRequest);

	}

	private void get_hotline(){
		String url = ResourceUtil.getServiceUrl(getActivity()) + getString(R.string.app_service_rest_hotline);
		JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
			@Override
			public void onResponse(JSONArray jsonArray) {
				List<HotlinePhone> mHotlinePhones = new ArrayList<>();
				for (int i= 0 ;i <jsonArray.length();i++) {
					try {
						mHotlinePhones.add(new HotlinePhone(jsonArray.getJSONObject(i)));
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				DataCacheUtill.setHotlinePhone(getActivity(),mHotlinePhones);

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {

			}
		});
		MainApplication.getInstance().addToRequestQueue(jsonArrayRequest);
	}




}