package com.touchtechnologies.tit.cctvitlive;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.cctvitlive.CCTVListPagerAdapter.AsynLoadBitmap;

/**
 * Simple ROI adapter that create a simple image and title
 * 
 * @author Touch
 * 
 */
public class CCTVLiveDetailPagerAdapter extends PagerAdapter implements OnClickListener, CCTVViewDestroyListener{
	private Context context;
	private ROI roi;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private HashMap<Integer, ImageView> hashImageView = new HashMap<>();
	private int selectedPageIndex;
	/**
	 * Pause time before load each cctv image
	 */
	final int LOAD_THRESHOLD_MILLI = 2500;
	private Timer asynLoadBitmap;
	
	public CCTVLiveDetailPagerAdapter(Context context) {
		this.context = context;

		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
		.showImageForEmptyUri(R.drawable.bg_cctvdefault)
		.showImageOnFail(R.drawable.bg_cctvdefault)
//		.showImageOnLoading(R.drawable.bg_cctvdefault)
		.cacheInMemory(true)
	  	.build();
 	     	  
 	    imageLoader = ImageLoader.getInstance();
	}

	public void setData(ROI rois) {
		this.roi = rois;
		notifyDataSetChanged();
	}
	
	public void setSelectedPageIndex(int selectedPageIndex) {
		this.selectedPageIndex = selectedPageIndex;
		
		Log.d("CCTV", "set selected page at " + selectedPageIndex);
		startAsynLoad();
	}

	
	public int getCount() {
		return roi == null ? 0 : roi.sizeCCTV();
	}

	public boolean isViewFromObject(View view, Object object) {
		return (view == object);
	}

	public CCTV getItem(int position) {
		return roi.getCCTVAt(position);
	}

	public long getItemId(int position) {
		return position;
	}
	@Override
	public CharSequence getPageTitle(int position) {
		return roi.getCCTVAt(position).getLabel_en();
	}
	@Override
	public Object instantiateItem(ViewGroup collection, int position) {
		View convertView = LayoutInflater.from(context).inflate(
				R.layout.item_cctv_detail, collection, false);
		convertView.setTag(roi.getCCTVAt(position));
		if(roi.getCCTVAt(position).getOnlineStatus() == 0){
			convertView.findViewById(R.id.imageView_live_cctv).setVisibility(View.INVISIBLE);
		}else {
			convertView.findViewById(R.id.imageView_live_cctv).setVisibility(View.VISIBLE);
		}
		
		ImageView imageView = (ImageView) convertView.findViewById(R.id.imagedetailcctv);
		imageView.setOnClickListener(this);

		imageView.setTag(roi.getCCTVAt(position));
		
		hashImageView.put(position, imageView);

		String imageUrl = roi.getCCTVAt(position).getUrl();
		imageLoader.displayImage(imageUrl, imageView, options);
		
		Log.i(this.getClass().getSimpleName(), imageUrl);
		collection.addView(convertView, 0);

		return convertView;
	}

	class ViewHolder {
		ImageView imageView;
		TextView textView;
	}

	@Override
	public void destroyItem(View collection, int position, Object view) {
		hashImageView.remove(position);
		
		((ViewPager) collection).removeView((View) view);
	}
	
	@Override
	public void onDestroyed() {
		if(asynLoadBitmap != null){
			asynLoadBitmap.cancel();
		}
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		CCTV cctv = (CCTV)v.getTag();
		
		switch (v.getId()) {
		case R.id.imagedetailcctv:
			//  Toast.makeText(context,"position "+cctv.getLabel_en(),Toast.LENGTH_SHORT).show();
			
			    intent = new Intent(context,CCTVLiveDetailFullScreenActivity.class);
				intent.putExtra(CCTV.class.getName(),cctv);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				context.startActivity(intent);
			break;

		
		}
		
	}

	void startAsynLoad(){
		try{
			if(asynLoadBitmap != null){
				asynLoadBitmap.cancel();
			}
			
			asynLoadBitmap = new Timer();
			asynLoadBitmap.schedule(new TimerTask() {
				
				@Override
				public void run() {
					new AsynLoadBitmap().execute(selectedPageIndex);
				}
			},0, LOAD_THRESHOLD_MILLI);
		}catch(Exception e){
		}
	}
	
	class AsynLoadBitmap extends AsyncTask<Integer, Void, Void>{
		CCTV cctv;
		Integer cctvLoadingIndex;
		
		@Override
		protected Void doInBackground(Integer... params) {
			cctvLoadingIndex = params[0];
			cctv = roi.getCCTVAt(cctvLoadingIndex);
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(cctvLoadingIndex == selectedPageIndex){
				ImageView imgView = hashImageView.get(cctvLoadingIndex);
				if(imgView != null && imgView.isShown()){
					String url  = cctv.getUrl() + "&reloadseed=" + Math.random();
					Log.i("CCTV-REFRESH", cctv.getKeyName());
					imageLoader.displayImage(url, imgView, options);
				}
			}

		}
	}
}
