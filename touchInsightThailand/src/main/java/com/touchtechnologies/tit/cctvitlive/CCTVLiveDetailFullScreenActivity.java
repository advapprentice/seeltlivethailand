package com.touchtechnologies.tit.cctvitlive;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.touchtechnologies.animate.imageview.TouchImageView;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.tit.R;

public class CCTVLiveDetailFullScreenActivity extends Activity implements OnClickListener{
	private CCTV cctv;
	private  ImageLoader imageLoader;
	private Runnable runnable;
	private TouchImageView imglive;
	protected DisplayImageOptions options;
	private int count=0;
	private Button btndone;
	private View viewcctvshare;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	    requestWindowFeature(Window.FEATURE_NO_TITLE);
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.item_cctv_full_screen);
	
		imageLoader = ImageLoader.getInstance();
		
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
		.showImageOnLoading(R.id.bgcctvlive)
		.cacheInMemory(true)
		.resetViewBeforeLoading(true).build();
		
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
		.memoryCache(new WeakMemoryCache())
		.threadPriority(Thread.NORM_PRIORITY)
        .denyCacheImageMultipleSizesInMemory()
        .build();
 	     	  
 	    imageLoader = ImageLoader.getInstance();
		if(!imageLoader.isInited()){
			imageLoader.init(config);	
		}
		cctv = (CCTV) getIntent().getSerializableExtra(CCTV.class.getName());
		
		viewcctvshare= (View)findViewById(R.id.viewcctvshare);
		btndone = (Button)findViewById(R.id.btndone);
		btndone.setOnClickListener(this);
		viewcctvshare.setOnClickListener(this);
		
		
		imglive = (TouchImageView)findViewById(R.id.bgcctvlive);
		imageLoader.displayImage(cctv.getUrl(), imglive);
	
		
		
		
		runnable = new ReloadImage();
		runOnUiThread(runnable);
		
	}
	@Override
	protected void onDestroy() {
		runnable = null;
		super.onDestroy();
	}
	
	class ReloadImage implements Runnable{
	
		@Override
		public void run() {
			
			imageLoader.displayImage(cctv.getUrl()+"&reloadtime=" +Math.random(), imglive);
			Log.i("CCTVActivity", "reloaded");
			
			Log.i("URL","="+ imageLoader);
			if(runnable != null){
				runnable = new ReloadImage();
				imglive.postDelayed(runnable, 2000);
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btndone:
			finish();
			
			break;
		case R.id.viewcctvshare:
		Intent	intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT,cctv.getUrlCCTV());
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		    startActivity(intent);

			break;
		}
		
	}
	

}
