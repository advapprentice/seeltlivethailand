package com.touchtechnologies.tit.cctvitlive;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.tit.R;

/**
 * Simple ROI adapter that create a simple image and title
 * 
 * @author Touch
 * 
 */
public class CCTVListPagerAdapter extends PagerAdapter implements OnClickListener, CCTVViewDestroyListener {
	private Context context;
	private ROI roi;
	private ImageLoader imageLoader;
	private HashMap<Integer, ImageView> hashImageView = new HashMap<>();
	private DisplayImageOptions options;
	private int selectedPageIndex;
	/**
	 * Pause time before load each cctv image
	 */
	final int LOAD_THRESHOLD_MILLI = 2500;
	
	private Timer asynLoadBitmap;
	
	public CCTVListPagerAdapter(Context context) {
		this.context = context;

		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
				.showImageForEmptyUri(R.drawable.bg_cctvdefault)
				.showImageOnFail(R.drawable.bg_cctvdefault)
				//.showImageOnLoading(R.drawable.shadow)
				.cacheInMemory(true) // fade
				.resetViewBeforeLoading(false)
				.build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				context).threadPoolSize(3)
				.threadPriority(Thread.NORM_PRIORITY - 2)
				.denyCacheImageMultipleSizesInMemory().memoryCacheSize(3*1024*1024)
				.defaultDisplayImageOptions(options).build();
	}

	public void setData(ROI rois) {
		this.roi = rois;
		notifyDataSetChanged();
	}

	public int getCount() {
		return roi == null ? 0 : roi.sizeCCTV();
	}

	public boolean isViewFromObject(View view, Object object) {
		return (view == object);
	}

	public CCTV getItem(int position) {
		return roi.getCCTVAt(position);
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public Object instantiateItem(ViewGroup collection, int position) {
		CCTV cctv = roi.getCCTVAt(position);
		ViewHolder viewHolder = new ViewHolder();
		View convertView = LayoutInflater.from(context).inflate(R.layout.item_cctv_it_live, collection, false);
		convertView.setTag(cctv);

		ImageView imageView = (ImageView) convertView.findViewById(R.id.imgDisplay);
		imageView.setOnClickListener(this);
		imageView.setTag(position);
		
		hashImageView.put(position, imageView);

		String imageUrl = cctv.getUrl();
		imageLoader.displayImage(imageUrl, imageView, options);
		collection.addView(convertView, 0);
		
		Log.d("CCTV","create cctv view at "+ position + ": " + cctv.getKeyName());

		return convertView;
	}
	
	public void setSelectedPageIndex(int selectedPageIndex) {
		this.selectedPageIndex = selectedPageIndex;
		
		Log.d("CCTV", "set selected page at " + selectedPageIndex);
		//startAsynLoad();
	}
	
	void startAsynLoad(){
		try{
			if(asynLoadBitmap != null){
				asynLoadBitmap.cancel();
			}
			
			asynLoadBitmap = new Timer();
			asynLoadBitmap.schedule(new TimerTask() {
				
				@Override
				public void run() {
					new AsynLoadBitmap().execute(selectedPageIndex);
				}
			},0, LOAD_THRESHOLD_MILLI);
		}catch(Exception e){
		}
	}

	@Override
	public void onDestroyed() {
		if(asynLoadBitmap != null){
			asynLoadBitmap.cancel();
		}
	}
	
	class ViewHolder {
		ImageView imageView;
		TextView textView;
	}

	@Override
	public void destroyItem(View collection, int position, Object view) {
		CCTV cctv = (CCTV)((View)view).getTag();
		
		hashImageView.remove(position);
		Log.d("CCTV","destroy cctv view:" + cctv.getKeyName());
		
		((ViewPager) collection).removeView((View) view);
	}
	
	public ImageView getImageViewAt(int position){
		return hashImageView.get(position);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imgDisplay:

			Intent intent = new Intent(context, CCTVLiveDetailActivity.class);
			
			// intent.putExtra(CCTV.class.getName(),
			// arrroi.get((Integer)v.getTag()));
			intent.putExtra(ROI.class.getName(), roi);
			intent.putExtra("position", Integer.parseInt(v.getTag().toString()));
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			context.startActivity(intent);
			break;

		}

	}

	class AsynLoadBitmap extends AsyncTask<Integer, Void, Void>{
		CCTV cctv;
		Integer cctvLoadingIndex;
		
		@Override
		protected Void doInBackground(Integer... params) {
			
			try {
				cctvLoadingIndex = params[0];
				cctv = roi.getCCTVAt(cctvLoadingIndex);
				
			} catch (Exception e) {
				e.getMessage();
			}
		
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(cctvLoadingIndex == selectedPageIndex){
				ImageView imgView = hashImageView.get(cctvLoadingIndex);
				if(imgView != null && imgView.isShown()){
					String url  = cctv.getUrl() + "&reloadseed=" + Math.random();
					Log.i("CCTV-REFRESH", cctv.getKeyName());
					imageLoader.displayImage(url, imgView, options);
				}
			}

		}
	}
	
}
