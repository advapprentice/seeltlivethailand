package com.touchtechnologies.tit.cctvitlive;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.SparseArrayCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.touchtechnologies.animate.gridview.FloatingAction;
import com.touchtechnologies.animate.pager.PagerSlidingTabStrip;
import com.touchtechnologies.app.TransparentDialog;
import com.touchtechnologies.command.insightthailand.RequestListLiveStreamingCommand;
import com.touchtechnologies.command.insightthailand.RestServiceHistoryListCommand;
import com.touchtechnologies.command.insightthailand.RestServiceNearbyCommand;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.DataItem;
import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.dataobject.insightthailand.CategoryLiveStream;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamConnection;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.LocationAwareFragment;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.history.LiveHistoryGridviewFragment;
import com.touchtechnologies.tit.history.LiveHistoryPagerAdapter;
import com.touchtechnologies.tit.live.LiveCam;
import com.touchtechnologies.tit.live.category.ListCategoryFragment;
import com.touchtechnologies.tit.live.category.ScrollTabHolder;
import com.touchtechnologies.tit.live.liststreaming.LiveStreamGridViewAdapter;
import com.touchtechnologies.tit.live.liststreaming.LiveStreamingListActivity;
import com.touchtechnologies.tit.live.liststreaming.LiveStreamingListAdapter;
import com.touchtechnologies.tit.live.liststreaming.LiveStreamingPagerAdapter;
import com.touchtechnologies.tit.live.mylivestream.FragmentMyliveStream;
import com.touchtechnologies.tit.login.LoginsActivity;
import com.touchtechnologies.tit.more.FragmentMore;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TouchICS on 3/24/2016.
 */
public class LiveStreamFragment2 extends LocationAwareFragment implements View.OnClickListener,ScrollTabHolder {
    public static final boolean NEEDS_PROXY = Integer.valueOf(Build.VERSION.SDK_INT).intValue() < 11;
    private static final int PERMISSIONS_REQUEST_PHONE_STATE = 123;
    private static final int PERMISSIONS_REQUEST_PHONE_STATE_AUDIO = 124;
    private static final int PERMISSIONS_REQUEST_PHONE_STATE_PHONE = 125;
    private String mManifestPersmission,mManifestPersmissionaudio,mManifestPersmissionPhone;
    private RequestListLiveStreamingCommand requestListLiveStreamingCommand;
    private int mRequestCode,mRequestCode2,mRequestCode3;
    int blue = 0xFF6495ED;
    protected List<DataItem> pois;
    private List<LiveStreamingChannel> hits;
    private LiveStreamPagerAdapter2 adapter;
    private LiveStreamingPagerAdapter adapteLiver;
    double latitude;
    double longitude ;
    private ViewPager viewPager;
    private ViewPager viewPagerLive;
    private int selectedPager;
    private FloatingAction mFloatingAction;
    private List<CategoryLiveStream> arrCategoryLive = new ArrayList<CategoryLiveStream>();
    private FragmentManager fragmentManager;
    private PagerSlidingTabStrip tabs;
    private ListView list;

    private TextView txtLiveCount,txtLiveCountText;


    /// setheader
    private View mHeader;
    private int mMinHeaderHeight;
    private int mHeaderHeight;
    private int mMinHeaderTranslation;
    private int mLastY;
    int liveEmpty;
    @Override
    public void onConnected(Bundle bundle) {
        super.onConnected(bundle);

        if(mLastLocation != null)
            categoriespoi(mLastLocation);
        return;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {

        final	View rootview = inflater.inflate(R.layout.simple_tab, container, false);

        fragmentManager = getActivity().getSupportFragmentManager();


        mHeader =(View) rootview.findViewById(R.id.header);
        mHeader.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.header_height_null)));

        txtLiveCount = (TextView)rootview.findViewById(R.id.txtLiveCount);
        txtLiveCountText = (TextView)rootview.findViewById(R.id.txtLiveCountText);

        txtLiveCount.setOnClickListener(this);
        txtLiveCountText.setOnClickListener(this);

        adapteLiver = new LiveStreamingPagerAdapter(getActivity());
        viewPagerLive = (ViewPager)rootview.findViewById(R.id.viewpagerLive);

        //	pager.setPageTransformer(true,new FullImagePagertransformer2());
        viewPagerLive.setAdapter(adapteLiver);
        viewPagerLive.setCurrentItem(0);
        viewPagerLive.setClipToPadding(false);

        Point point = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(point);
    //   viewPagerLive.getLayoutParams().height = (int)((point.x * 3)/4);



        hits = new ArrayList<LiveStreamingChannel>();

        // Set the ViewPagerAdapter into ViewPager List
        adapter = new LiveStreamPagerAdapter2(getActivity(), getChildFragmentManager());
        viewPager = (ViewPager) rootview.findViewById(R.id.pager);
        viewPager.setAdapter(adapter);
        adapter.setTabHolderScrollingContent(this);


        tabs = (PagerSlidingTabStrip) rootview.findViewById(R.id.tabs);
        tabs.setShouldExpand(true);
        tabs.setIndicatorColor(0xFFDD0000);//0xFFDD0000
        tabs.setIndicatorHeight(5);
        tabs.setViewPager(viewPager);
        // set tabs pager
        mLastY=0;
        Location location = ResourceUtil.getLocation(getActivity());
        if(location != null)
            categoriespoi(location);



        return rootview;
    }

    public void setDataLive(List<LiveStreamingChannel> result){
        hits = result;
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                final Fragment activefragment = getChildFragmentManager().findFragmentByTag("android:switcher:" + viewPager.getId() + ":" + position);
                int currentItem = viewPager.getCurrentItem();
                if(mFloatingAction != null) {
                    switch (position) {
                        case 0:
                            mFloatingAction.show();
                            mFloatingAction.listenTo(((LiveHistoryGridviewFragment) activefragment).getGridView());
                            if (hits.size() == 0) {
                                setHeader(true);

                            } else {
                                setHeader(false);
                            }
                            break;
                        case 1:
                            mFloatingAction.show();
                            mFloatingAction.listenTo(((ListCategoryFragment) activefragment).getGridView());
                            setHeader(true);

                            break;
                        case 2:
                            mFloatingAction.hide();
                            //mFloatingAction.listenTo(((FragmentMyliveStream) activefragment).getGridView());
                            setHeader(true);

                            break;
                    }
                }

            }

            @Override
            public void onPageSelected(int position) {
                selectedPager = position;


                SparseArrayCompat<ScrollTabHolder> scrollTabHolders = adapter.getScrollTabHolders();

                /// positoin = position page 1
                ScrollTabHolder currentHolder = scrollTabHolders.valueAt(0);
                if(NEEDS_PROXY){
                    //TODO is not good
                    currentHolder.adjustScroll(mHeader.getHeight()-mLastY);
                    mHeader.postInvalidate();
                }else{
                    currentHolder.adjustScroll((int) (mHeader.getHeight() +mHeader.getTranslationY()));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });

   }


   public void setHeader(boolean check){
        if (mHeader == null)
            return;
        try{

            if (check==true ){
                mHeader.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.header_height_null)));
                mMinHeaderHeight = getResources().getDimensionPixelSize(R.dimen.min_header_height_null);
                mHeaderHeight = getResources().getDimensionPixelSize(R.dimen.header_height_null);
                mMinHeaderTranslation = - mMinHeaderHeight;
            }else{
                mHeader.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,getResources().getDimensionPixelSize(R.dimen.header_height)));
                mMinHeaderHeight = getResources().getDimensionPixelSize(R.dimen.min_header_height);
                mHeaderHeight = getResources().getDimensionPixelSize(R.dimen.header_height);
                mMinHeaderTranslation = - mMinHeaderHeight;
            }

        }catch (Exception e){
            Log.i("Error",":"+e.getMessage());
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        if(mFloatingAction == null ) {
            mFloatingAction = FloatingAction.from(getActivity())
                    .colorResId(R.color.branding)
                    .color(0xFFFFFFFF)
                    .icon(R.drawable.ic_livestream1)
                    .listener(LiveStreamFragment2.this)
                    .build();
        }
        mFloatingAction.show();

        getActivity().getWindow().setBackgroundDrawable(null);
        attchlistener();


    }

    @Override
    public void onPause() {
        super.onPause();

        mFloatingAction.hide();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
     //   Toast.makeText(getActivity(), "Update live stream list"+requestCode, Toast.LENGTH_SHORT).show();


        /*
         if (requestCode == 1) {
            viewPager.setCurrentItem(0,true);
        }
        else if(requestCode ==3){

            viewPager.setAdapter(new LiveStreamPagerAdapter(getActivity(), getChildFragmentManager()));
            viewPager.setCurrentItem(2);
         }

        */
        if(requestCode ==3){

            viewPager.setCurrentItem(2);
        }

    }

    public void notifyDatasetChanged(){
   //     viewPager.setAdapter(new LiveStreamPagerAdapter(getActivity(), getChildFragmentManager()));
   //     viewPager.setCurrentItem(selectedPager);
   // 		Toast.makeText(getActivity(), "Update live stream list", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {

        getPermission();

    }

    private void categoriespoi(Location lastKnownLocation) {

        if (lastKnownLocation!=null){
            longitude = lastKnownLocation.getLongitude();
            latitude = lastKnownLocation.getLatitude();
            Log.d(LiveStreamFragment.class.getSimpleName(),"MY LOCATION ="+latitude+"/"+longitude);
            ResourceUtil.setLocation(getActivity(), lastKnownLocation);
            stopLocationUpdates();
        } else{
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
            alertDialog.setIcon(R.drawable.ic_warning);
            alertDialog.setMessage("LiveStream need turn on location ");
            alertDialog.setTitle("Warning Please turn on location!");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
        }

        String urlPOIs = ResourceUtil.getServiceUrl(getActivity()) +getResources().getString(R.string.app_service_NearByMe)+latitude+"/"+longitude+"/";


//        new RestServiceNearbyCommand(getActivity(), null, "restaurant", urlPOIs){
//            protected void onPostExecute(List<DataItem> result) {
////				Log.d("near by restaurant", result == null?"-":result.toString());
//                pois = result;
//
//            }
//
//        }.execute();
    }

    @Override
    public void adjustScroll(int scrollHeight) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount, int pagePosition) {
        if (viewPager.getCurrentItem() == pagePosition) {
            int scrollY = getScrollY(view);
            if(NEEDS_PROXY){
                //TODO is not good
                mLastY=-Math.max(-scrollY, mMinHeaderTranslation);
                Log.i("scrollY", ":" + String.valueOf(scrollY));
                mHeader.scrollTo(0, mLastY);
                mHeader.postInvalidate();
            }else{
                mHeader.setTranslationY(Math.max(-scrollY, mMinHeaderTranslation));
            }
        }
    }


    public int getScrollY(AbsListView view) {
        View c = view.getChildAt(0);
        if (c == null) {
            return 0;
        }
        int firstVisiblePosition = view.getFirstVisiblePosition();
        int top = c.getTop();

        int headerHeight = 0;
        if (firstVisiblePosition >= 1) {
            headerHeight = mHeaderHeight;
        }
        return -top + firstVisiblePosition * c.getHeight() + headerHeight;
    }
    public static float clamp(float value, float max, float min) {
        return Math.max(Math.min(value, min), max);
    }


    public void getPermission(){
        mManifestPersmission = Manifest.permission.CAMERA;
        mManifestPersmissionaudio = Manifest.permission.RECORD_AUDIO;
        mManifestPersmissionPhone = Manifest.permission.READ_PHONE_STATE;


        mRequestCode = PERMISSIONS_REQUEST_PHONE_STATE;
        mRequestCode2 = PERMISSIONS_REQUEST_PHONE_STATE_AUDIO;
        mRequestCode3 = PERMISSIONS_REQUEST_PHONE_STATE_PHONE;
        int permerssion = ActivityCompat.checkSelfPermission(getActivity(), mManifestPersmission);
        int permerssionaudio = ActivityCompat.checkSelfPermission(getActivity(), mManifestPersmissionaudio);
        int permerssionphone = ActivityCompat.checkSelfPermission(getActivity(), mManifestPersmissionPhone);

        boolean should = ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), mManifestPersmission);
        if (permerssion != PackageManager.PERMISSION_GRANTED||permerssionaudio != PackageManager.PERMISSION_GRANTED||permerssionphone!= PackageManager.PERMISSION_GRANTED) {
            requestPermission();
        }else {
            LiveStreamConnection stremconnect = new LiveStreamConnection();
            stremconnect.setLatitude(latitude);
            stremconnect.setLongitude(longitude);
            stremconnect.setUser(UserUtil.getUser(getActivity()));
            if(UserUtil.isLoggedIn(getActivity())){
                Intent intent = new Intent(getActivity(), LiveCam.class);
                intent.putExtra(LiveStreamConnection.class.getName(),stremconnect);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(intent, 1);

            }else{
                startActivity(new Intent(getActivity(), LoginsActivity.class));

            }
        }

    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{mManifestPersmission}, mRequestCode);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
       // super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSIONS_REQUEST_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{mManifestPersmissionaudio}, mRequestCode2);
                } else {

                }
                return;
            }
            case PERMISSIONS_REQUEST_PHONE_STATE_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{mManifestPersmissionPhone}, mRequestCode3);
                } else {

                }
                return;
            }
            case PERMISSIONS_REQUEST_PHONE_STATE_AUDIO:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    LiveStreamConnection stremconnect = new LiveStreamConnection();
                    stremconnect.setLatitude(latitude);
                    stremconnect.setLongitude(longitude);
                    stremconnect.setUser(UserUtil.getUser(getActivity()));
                    if(UserUtil.isLoggedIn(getActivity())){
                        Intent intent = new Intent(getActivity(),LiveCam.class);
                        intent.putExtra(LiveStreamConnection.class.getName(),stremconnect);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivityForResult(intent, 1);

                    }else{
                        startActivity(new Intent(getActivity(), LoginsActivity.class));

                    }
                } else {

                }
                return;
            }

        }
    }

    public void attchlistener(){


        final	Fragment activefragment = getChildFragmentManager().findFragmentByTag("android:switcher:" + viewPager.getId() + ":"	+ viewPager.getCurrentItem());
        int position = viewPager.getCurrentItem();

        if(activefragment!=null) {
            switch (position){
                case 0:
                    mFloatingAction.show();
                    mFloatingAction.listenTo(((LiveHistoryGridviewFragment) activefragment).getGridView());
                    break;
                case 1:
                    mFloatingAction.show();
                    mFloatingAction.listenTo(((ListCategoryFragment) activefragment).getGridView());
                    break;
                case 2:
                    mFloatingAction.hide();
                   // mFloatingAction.listenTo(((FragmentMyliveStream) activefragment).getGridView());
                    break;
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mFloatingAction != null) {
            mFloatingAction.onDestroy();
            mFloatingAction = null;
        }
      //  requestListLiveStreamingCommand.cancel(true);
    }



    public  void getlistlivestream(){
        String url = ResourceUtil.getServiceUrl(getActivity())+getResources().getString(R.string.app_service_liststreaming);
        User user = UserUtil.getUser(getActivity());
          new RequestListLiveStreamingCommand(getActivity(),user, url){
            protected void onPostExecute(java.util.List<LiveStreamingChannel> result) {
                hits = result;
                setDataLive(hits);
                LiveStreamingListAdapter adapterLive = new LiveStreamingListAdapter(getActivity());
                adapterLive.setData(hits);

                if (hits.size()==0) {
                    setHeader(true);

                } else {
                    setHeader(false);
                }
                txtLiveCount.setText(""+hits.size());
                txtLiveCount.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), LiveStreamingListActivity.class);
                        startActivity(intent);
                    }
                });
                txtLiveCountText.setText("("+hits.size()+")"+" Live");
                txtLiveCountText.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), LiveStreamingListActivity.class);
                        startActivity(intent);
                    }
                });


                adapteLiver.setData(hits);
                adapter.setLiveStreamContent(hits);
                adapteLiver.notifyDataSetChanged();


            }

        }.execute();
    }



}

