package com.touchtechnologies.tit.cctvitlive;


import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.touchtechnologies.animate.gridview.FloatingAction;
import com.touchtechnologies.app.TransparentDialog;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.RequestLivestreamCommand;
import com.touchtechnologies.command.insightthailand.RestServiceCategoryListCommand;
import com.touchtechnologies.command.insightthailand.RestServiceNearbyCommand;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.DataItem;
import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.dataobject.Restaurant;
import com.touchtechnologies.dataobject.insightthailand.CategoryLiveStream;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamConnection;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.LocationAwareFragment;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.history.LiveHistoryGridviewFragment;
import com.touchtechnologies.tit.live.LiveCam;
import com.touchtechnologies.tit.live.liststreaming.Checklivestreamonline;
import com.touchtechnologies.tit.live.liststreaming.LiveStreamGridViewFragment;
import com.touchtechnologies.tit.live.mylivestream.FragmentMyliveStream;
import com.touchtechnologies.tit.login.LoginsActivity;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.util.DataCacheUtill;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

public class LiveStreamFragment extends LocationAwareFragment implements OnClickListener,Checklivestreamonline{
	
	int blue = 0xFF6495ED;
	private LiveStreamConnection stream;
	protected List<DataItem> pois;
	private Spinner spinner1;
	private LiveStreamPagerAdapter adapter;
	double latitude;
	double longitude ;
	Hotel hotel;
	CCTV cctv;
	LiveStreamingChannel live;
	StreamHistory hiss;
	TransparentDialog dialog;
	EditText title,detail;
	ViewPager viewPager;
	private int selectedPager;
	private FloatingAction mFloatingAction;
	private List<CategoryLiveStream> arrCategoryLive = new ArrayList<CategoryLiveStream>();
	ListView list;
	@Override
	public void onConnected(Bundle bundle) {
		super.onConnected(bundle);

		if(mLastLocation != null)
			categoriespoi(mLastLocation);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container,  Bundle savedInstanceState) {
	final	View rootview = inflater.inflate(R.layout.new_livestream, container, false);
	
		viewPager = (ViewPager) rootview.findViewById(R.id.pager);

		// Set the ViewPagerAdapter into ViewPager
		adapter = new LiveStreamPagerAdapter(getActivity(), getChildFragmentManager());
		viewPager.setAdapter(adapter);
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


			}

			@Override
			public void onPageSelected(int position) {
				selectedPager = position;
				int pagecount = adapter.getCount();
				final Fragment activefragment = getChildFragmentManager().findFragmentByTag("android:switcher:" + viewPager.getId() + ":" + position);
//				switch (position){
//					case 0:
//						mFloatingAction.listenTo(((LiveStreamGridViewFragment) activefragment).getGridView());
//
//						break;
//					case 1:
//						mFloatingAction.listenTo(((LiveHistoryGridviewFragment) activefragment).getGridView());
//
//						break;
//					case 2:
//						mFloatingAction.listenTo(((FragmentMyliveStream) activefragment).getGridView());
//
//						break;
//				}
				if (mFloatingAction != null && activefragment != null) {
					if (position > 0 && position < pagecount - 1) {
						mFloatingAction.listenTo(((LiveHistoryGridviewFragment) activefragment).getGridView());
					} else if (position == 0) {
						mFloatingAction.listenTo(((LiveStreamGridViewFragment) activefragment).getGridView());
					} else {
						mFloatingAction.listenTo(((FragmentMyliveStream) activefragment).getGridView());
					}

					return;
				}

			}

			@Override
			public void onPageScrollStateChanged(int state) {


			}
		});


		PagerTabStrip pagerTabStrip = (PagerTabStrip)rootview.findViewById(R.id.PagerTabStrip);
		pagerTabStrip.setDrawFullUnderline(true);
		pagerTabStrip.setTabIndicatorColor(blue);

	    Button btnlive = (Button)rootview.findViewById(R.id.btnlive);
	    btnlive.setOnClickListener(this);
		//register hotel menus
		Location location = ResourceUtil.getLocation(getActivity());
		if(location != null)
		 	categoriespoi(location);

		return rootview;
	}

	@Override
	public void onResume() {
		super.onResume();
		final	Fragment activefragment = getChildFragmentManager().findFragmentByTag("android:switcher:" + viewPager.getId() + ":"	+ viewPager.getCurrentItem());
		mFloatingAction = FloatingAction.from(getActivity())
				//	.listenTo(iv.setData(list))
				.colorResId(R.color.branding)
				.color(0xFFFFFFFF)
				.icon(R.drawable.ic_livestream1)
				.listener(LiveStreamFragment.this)
				.build();

		getActivity().getWindow().setBackgroundDrawable(null);
		int position = viewPager.getCurrentItem();
		int pagecount = adapter.getCount();
		if(activefragment!=null) {
			if (position > 0 && position < pagecount - 1) {
				mFloatingAction.listenTo(((LiveHistoryGridviewFragment) activefragment).getGridView());
			} else if (position == 0) {
				mFloatingAction.listenTo(((LiveStreamGridViewFragment) activefragment).getGridView());
			} else {
				mFloatingAction.listenTo(((FragmentMyliveStream) activefragment).getGridView());
			}
		}
	}


	@Override
	public void onPause() {
		super.onPause();

		mFloatingAction.onDestroy();
	//	Toast.makeText(getActivity(), "Floating Action Clicked", Toast.LENGTH_SHORT).show();

	}



	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Toast.makeText(getActivity(), "Floating Action Clicked"+requestCode, Toast.LENGTH_SHORT).show();
	    Log.d("MY LOCATION ", "=" + resultCode + "requestCode :" + requestCode);
	   if (requestCode == 1) {
	    	viewPager.setCurrentItem(2,true);
	   }else if(requestCode ==2){
			viewPager.setCurrentItem(1,true);
		}
	}//onActivityResult

	public void notifyDatasetChanged(){
		viewPager.setAdapter(new LiveStreamPagerAdapter(getActivity(), getChildFragmentManager()));
		viewPager.setCurrentItem(selectedPager);

//		Toast.makeText(getActivity(), "Update live stream list", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onClick(View v) {
		LiveStreamConnection stremconnect = new LiveStreamConnection();
		stremconnect.setLatitude(latitude);
		stremconnect.setLongitude(longitude);
		stremconnect.setUser(UserUtil.getUser(getActivity()));
		switch(v.getId()){
		case R.id.btnlive:

			 Log.d("MY DEVELOP ","="+latitude+"/"+longitude);
			if(UserUtil.isLoggedIn(getActivity())){
				Intent intent = new Intent(getActivity(),LiveCam.class);
				intent.putExtra(LiveStreamConnection.class.getName(),stremconnect);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivityForResult(intent, 1);
			 //     dialogLivestream();

			}else{
				startActivity(new Intent(getActivity(), LoginsActivity.class));

			}
			break;
		default:

			if(UserUtil.isLoggedIn(getActivity())){
				Intent intent = new Intent(getActivity(),LiveCam.class);
				intent.putExtra(LiveStreamConnection.class.getName(),stremconnect);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivityForResult(intent, 1);

				//     dialogLivestream();
			}else{
				startActivity(new Intent(getActivity(), LoginsActivity.class));

			}
			//	Toast.makeText(getActivity(), "Floating Action Clicked", Toast.LENGTH_SHORT).show();

			break;

		}
	}

	/**
	 * Set fragment when a hotel menu is clicked
	 */

	// Dialog live stream
	private void dialogLivestream() {
		dialog = new TransparentDialog(getActivity(), R.layout.dialog_main);
		title = (EditText) dialog.findViewById(R.id.editTitle);
		title.requestFocus();
		detail = (EditText)dialog.findViewById(R.id.editdetail);
		
		
		spinner1 = (Spinner)dialog.findViewById(R.id.spinner1);
		
		if(pois == null) return;
		
		String[] names = new String[pois.size()+1];
		names[0] = "Tag location";
		int i=1;
	
		
		for(DataItem item: pois){
			names[i++] =  item.getName();
		}
		  
		ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, names);
	//	myAdapter.add("Please Select");
		spinner1.setAdapter(myAdapter);
		
		 
		 
	    dialog.findViewById(R.id.exit).setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			dialog.dismiss();
			
		}
	    });
	    
	    dialog.findViewById(R.id.liveok).setOnClickListener(new OnClickListener() {
	    				
	    	         @Override
	    	         public void onClick(View v) {
	    	        	 if (title.getText().toString().isEmpty()) {
							 		 title.setError(getText(R.string.up_title_live));
							} else {
								commonstreaming();
								dialog.dismiss();
							}
	    			 }
	    	      });
	    dialog.show();
	   
	}
	
	 
	 

	
	private void categoriespoi(Location lastKnownLocation) {

         if (lastKnownLocation!=null){
        	 longitude = lastKnownLocation.getLongitude();
        	 latitude = lastKnownLocation.getLatitude();
        	 Log.d(LiveStreamFragment.class.getSimpleName(),"MY LOCATION ="+latitude+"/"+longitude);
			 ResourceUtil.setLocation(getActivity(), lastKnownLocation);
			 stopLocationUpdates();
         } else{
         	AlertDialog alertDialog = new Builder(getActivity()).create();
  			alertDialog.setIcon(R.drawable.ic_warning);
  			alertDialog.setMessage("LiveStream need turn on location ");
  			alertDialog.setTitle("Warning Please turn on location!");
  			alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
  				
  				@Override
  				public void onClick(DialogInterface dialog, int which) {
  					dialog.dismiss();
  				}
  			});
  			alertDialog.show();
          }

		String urlPOIs = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_NearByMe)+latitude+"/"+longitude+"/";
		
		
		new RestServiceNearbyCommand(getActivity(), null, "restaurant", urlPOIs){
			protected void onPostExecute(List<DataItem> result) {
//				Log.d("near by restaurant", result == null?"-":result.toString());
				pois = result;
				
			}
			
		}.execute();
	}

	
	
	
	LiveStreamConnection PutStreamConnection() {
		LiveStreamConnection stremconnect = new LiveStreamConnection();

		stremconnect.setTitle(title.getText().toString());
		stremconnect.setNote(detail.getText().toString());
		stremconnect.setLatitude(latitude);
		stremconnect.setLongitude(longitude);
		
		return stremconnect;
	}

	@Override
	public void _onListlivestream(int num) {
		if(num == 0){
			viewPager.setCurrentItem(1,true);
		}
	}
	public void commonstreaming() {

		RequestLivestreamCommand getCommand = new RequestLivestreamCommand(getActivity());
		
		getCommand.setLivestream(PutStreamConnection());
		getCommand.setUser(UserUtil.getUser(getActivity()));
		getCommand.setProtocol(LiveStreamConnection.SET_PROTOCOL);
	
		if(spinner1.getSelectedItemPosition() > 0){
			getCommand.setIdPoi(((Restaurant) pois.get(spinner1.getSelectedItemPosition() - 1)).getIdHotel());
		}
		new GetStreamServiceTask(getActivity()).execute(getCommand);

	}



	class GetStreamServiceTask extends CommonServiceTask {

	public GetStreamServiceTask(FragmentActivity context) {
		super(context, "loading...", null,null);
	}
	
	
	@Override
	protected ServiceResponse doInBackground(Command... params) {
		ServiceConnector srConnector = new ServiceConnector(ResourceUtil.getServiceUrl(context)+getString(R.string.app_service_liveStreamRequest));
		ServiceResponse serviceResponse = srConnector.doAsynPost(params[0],  false);
		
		try {
			int code = serviceResponse.getCode();
			if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
				JSONObject json = new JSONObject(serviceResponse.getContent().toString());
				stream = new LiveStreamConnection(json);
				
			}
		} catch (Exception e) {
			Log.e(AppConfig.LOG, "doInBackground error", e);
		}

		return serviceResponse;
	}

	@Override
	protected void onPostExecute(ServiceResponse result) {
		super.onPostExecute(result);

		dialog.dismiss();
		try {
			if (result.getCode() == ServiceResponse.SUCCESS|| result.getCode() == HttpStatus.SC_OK) {
			//	Toast.makeText(context, R.string.save_success, Toast.LENGTH_LONG).show();

				context.setResult(Activity.RESULT_OK);
		//		context.finish();
				

				Intent intent = new Intent(getActivity(),LiveCam.class);
				intent.putExtra(LiveStreamConnection.class.getName(),stream);
		//		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);

			} else {
				Toast.makeText(context, getText(R.string.up_not_connectstream)+"!\r\n" + result.getMessage(),
						Toast.LENGTH_LONG).show();

			}
		} catch (Exception e) {
			Toast.makeText(context, "ERROR!\r\n" + e.getMessage(),
					Toast.LENGTH_LONG).show();
			Log.e(AppConfig.LOG, "process response error", e);
		}
	}
 }


	
	
	
}