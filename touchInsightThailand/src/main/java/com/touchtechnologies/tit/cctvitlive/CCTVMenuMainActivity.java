package com.touchtechnologies.tit.cctvitlive;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.touchtechnologies.animate.dialog.MaterialDialog;
import com.touchtechnologies.app.TransparentDialog;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.NotificationRegisterCommand;
import com.touchtechnologies.command.insightthailand.RestServiceLiveStreamPropertieCommand;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamConnection;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.dataobject.insightthailand.Notification;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.dataobject.wifi.WiFiGenerate;
import com.touchtechnologies.json.JSONUtil;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.net.dataobject.InternetAccount;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.BuildConfig;
import com.touchtechnologies.tit.CustomWebview;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.dataobject.Category_video;
import com.touchtechnologies.tit.dataobject.HotlinePhone;
import com.touchtechnologies.tit.dataobject.MobileActivityPropertiesPopup;
import com.touchtechnologies.tit.dataobject.PopupDao;
import com.touchtechnologies.tit.live.liststreaming.LiveOnAirPlayerActivity;
import com.touchtechnologies.tit.login.LoginsActivity;
import com.touchtechnologies.tit.login.RegistersActivity;
import com.touchtechnologies.tit.more.FragmentMore;
import com.touchtechnologies.tit.nearby.Facebookplace;
import com.touchtechnologies.tit.nearby.NearByMapFragment;
import com.touchtechnologies.tit.search.MySingleton;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.setting.MyProfileActivity;
import com.touchtechnologies.tit.util.DataCacheUtill;
import com.touchtechnologies.tit.util.PushRegisterUtil;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;
import com.touchtechnologies.wifi.WIFIMod;

import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CCTVMenuMainActivity extends FragmentActivity implements
		OnClickListener, Response.ErrorListener {
	private static final int PERMISSIONS_REQUEST_PHONE_STATE = 123;
	private static final int PERMISSIONS_REQUEST_PHONE_STATE_AUDIO = 124;
	private static final int PERMISSIONS_REQUEST_PHONE_STATE_PHONE = 125;
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	public static final String KEY_TYPE_LIVE = "Streamlive";
	public static final String EXTRA_MESSAGE = "message";
	private Notification livechannel;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private MaterialDialog mMaterialDialog;
	private FragmentManager fragmentManager;
	private ImageView imgcctv, imgdiscount, imgjorney, imgabout, imglogin;
	private long backPressedTime = 0;
	private String senderID;
	private GoogleCloudMessaging gcm;
	private BroadcastReceiver gcmReceiver;
	private String regid;
	private int mRequestCode;
	private WifiManager mainWifi;
	private WifiScanReceiver wifiReciever;
	String wifis[];
	int num = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.activity_cctv_it_live);


			imageLoader = ImageLoader.getInstance();
			options = new DisplayImageOptions.Builder().cacheInMemory(false)
				.cacheOnDisc(false).bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.NONE)
				.showImageForEmptyUri(R.drawable.bg_cctvdefault)
				.showImageOnFail(R.drawable.bg_cctvdefault)
				.showImageOnLoading(R.drawable.bg_cctvdefault)
				.resetViewBeforeLoading(false).build();
			imageLoader = ImageLoader.getInstance();


            MainApplication.checkpremission(Manifest.permission.ACCESS_FINE_LOCATION,this,001);
            MainApplication.checkpremission(Manifest.permission.ACCESS_COARSE_LOCATION,this,002);
			fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.container, new LiveStreamFragment2()).commit();

			imgcctv = (ImageView) findViewById(R.id.cctv);
			imgdiscount = (ImageView) findViewById(R.id.discount);
			imgjorney = (ImageView) findViewById(R.id.jorney);
			imgabout = (ImageView) findViewById(R.id.about);
			imglogin = (ImageView) findViewById(R.id.login);

			View viewcctv = findViewById(R.id.view_cctv);
			View viewcoupon = findViewById(R.id.view_coupon);
			View viewjourney = findViewById(R.id.view_journey);
			View viewlive = findViewById(R.id.view_live);
			View viewlogin = findViewById(R.id.viewlogin);

		//	imgcctv.setBackgroundResource(R.drawable.ic_cctv1);
			imgabout.setBackgroundResource(R.drawable.ic_livestream1);

			imgcctv.setOnClickListener(this);
			imgdiscount.setOnClickListener(this);
			imgjorney.setOnClickListener(this);
			imgabout.setOnClickListener(this);
			imglogin.setOnClickListener(this);

			viewcctv.setOnClickListener(this);
			viewcoupon.setOnClickListener(this);
			viewjourney.setOnClickListener(this);
			viewlive.setOnClickListener(this);
			viewlogin.setOnClickListener(this);
			MainApplication.getInstance().trackScreenView("Main_Page");

		/// wifi scaner
			mainWifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
			WifiInfo info = mainWifi.getConnectionInfo();


		Log.i("SSID",":"+info.getSSID());


		if (mainWifi.isWifiEnabled()) {
			scanWiFi();

		} else{

			mMaterialDialog = new MaterialDialog(this);
			mMaterialDialog.setTitle("Please turn On WiFi")
					.setMessage("Please turn On WiFi ?")
					//mMaterialDialog.setBackgroundResource(R.drawable.background);
					.setPositiveButton("OK", new View.OnClickListener() {
						@Override public void onClick(View v) {
							mMaterialDialog.dismiss();
							startActivityForResult(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS),99);
						}
					})
					.setNegativeButton("CANCEL",
							new View.OnClickListener() {
								@Override public void onClick(View v) {
									mMaterialDialog.dismiss();
									//	Toast.makeText(getActivity(),"Cancel", Toast.LENGTH_LONG).show();
								}
							})
					.setCanceledOnTouchOutside(true)
					.setOnDismissListener(
							new DialogInterface.OnDismissListener() {
								@Override
								public void onDismiss(DialogInterface dialog) {
									//	Toast.makeText(getActivity(),"onDismiss",Toast.LENGTH_SHORT).show();
								}
							})
					.show();


		}




		if(AppConfig.check_update){
			GetCheckVersion();
		}
		if(MainApplication.getPopup_check()) {
			Get_Popup_Form_Api();
		}
		get_Category();
		get_hotline();

		senderID = ResourceUtil.getGoogleProjectID(this);
		gcmReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				try{
					try{
						String message = intent.getStringExtra("message");
						Log.i("Message", ":" + message);

					}catch(Exception e){
						e.printStackTrace();
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		};
		registerReceiver(gcmReceiver, new IntentFilter("tit.notifyMessage"));



		livechannel = (Notification) getIntent().getSerializableExtra(Notification.class.getName());

		if(livechannel!=null){
			switch (livechannel.getNotificationOption().getNotifiableType()){
				case KEY_TYPE_LIVE:
					getNotificationLiveStream();
					break;
			}

		}



	}
// ScanWiFi detacter
	public void scanWiFi(){

		WifiInfo info = mainWifi.getConnectionInfo();

		Log.i("SSID",":"+info.getSSID());
		if (mainWifi.isWifiEnabled()&&info.getSSID().equals("")) {
			wifiReciever = new WifiScanReceiver();
			mainWifi.startScan();

		} else{
			mainWifi.setWifiEnabled(true);

		}


	}

	private class WifiScanReceiver extends BroadcastReceiver{
		public void onReceive(Context c, Intent intent) {
			List<ScanResult> wifiScanList = mainWifi.getScanResults();
			wifis = new String[wifiScanList.size()];

			for(int i = 0; i < wifiScanList.size(); i++){
				wifis[i] = ((wifiScanList.get(i)).SSID.toString());
				if((wifiScanList.get(i).SSID).equals("TOUCH")){

					num ++;
					Log.i("1",":"+num);
					if(num == 1){
						showDialogAddAccount();

						Vibrator vi = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
						vi.vibrate(1500);
						// wifisetconnect
						saveWepConfig();
					}
				//	Toast.makeText(CCTVMenuMainActivity.this,"Connecting to :"+wifiScanList.get(i).SSID,Toast.LENGTH_SHORT).show();
				}

			}

		}
	}

	void showDialogAddAccount() {
		final TransparentDialog td = new TransparentDialog(this,R.layout.item_dialog_scan);

		// show dialog
		td.findViewById(R.id.btnConnect).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent;

					//	postGetAccount();

						if(UserUtil.isLoggedIn(CCTVMenuMainActivity.this)){

							intent = new Intent(CCTVMenuMainActivity.this, CCTVMenuMainActivity.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
							getWiFiGenerate();

						}else{
							intent = new Intent(CCTVMenuMainActivity.this, LoginsActivity.class);
							startActivity(intent);
						}

				    	td.dismiss();


					}
				});

		td.show();
	}


	void saveWepConfig() {
		WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		WifiConfiguration wifiConfiguration = new WifiConfiguration();
		wifiConfiguration.SSID = "\"TOUCH\"";
		wifiConfiguration.hiddenSSID = true;
		wifiConfiguration.status = WifiConfiguration.Status.ENABLED;
		wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
		int netId = wifiManager.addNetwork(wifiConfiguration);
		Log.d("WifiPreference", "add Network returned " + netId);
		boolean checkEnableWifi = wifiManager.enableNetwork(netId, true);
		Log.d("WifiPreference", "enableNetwork returned " + checkEnableWifi);



	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	//	Toast.makeText(this, "Main"+requestCode, Toast.LENGTH_SHORT).show();

		Fragment fragmentmain = getSupportFragmentManager().findFragmentById(R.id.container);
		fragmentmain.onActivityResult(requestCode, resultCode, data);


		if(requestCode==99){
			scanWiFi();
		}



		if(requestCode==1){
			Fragment fragment = fragmentManager.findFragmentById(R.id.container);
			if(fragment != null && fragment instanceof LiveStreamFragment2){
				((LiveStreamFragment2)fragment).notifyDatasetChanged();
			}

		}

		if(requestCode==5){
			Fragment fragment2 = fragmentManager.findFragmentById(R.id.container);
			if(fragment2 != null && fragment2 instanceof FragmentMore){
				((FragmentMore)fragment2).notifyDatasetChanged();
			}

		}

	}



	@Override
	protected void onResume() {
		super.onResume();

		// wifi scaner
		registerReceiver(wifiReciever, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

		if (checkPlayServices() & UserUtil.isLoggedIn(this)) {
			gcm = GoogleCloudMessaging.getInstance(this);

			regid = PushRegisterUtil.getRegistrationId(getApplicationContext());

			Log.i("=========regid=========",":"+regid);
			if (regid.isEmpty()) {
				registerInBackground();

			}else{
				postNotificationRegister();

			}
		}
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(gcmReceiver);

	}


	private class NewPushIncomingReceiver extends BroadcastReceiver{
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d("", "REGISTER WITH ID[" + senderID+ "]");
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);


	}

	@Override
	public void onClick(View v) {
		int action = 0;

		switch (v.getId()) {
		case R.id.view_cctv:
		case R.id.cctv:
			fragmentManager.beginTransaction().replace(R.id.container, new CCTVMenuFillterFragment()).addToBackStack(null).commit();

			if (action == MotionEvent.ACTION_DOWN) {

				imgcctv.setBackgroundResource(R.drawable.ic_cctv1);
				imgdiscount.setBackgroundResource(R.drawable.ic_discount2);
				//	imgjorney.setBackgroundResource(R.drawable.ic_jorney1);
				imgjorney.setBackgroundResource(R.drawable.new_nearby1);
				imgabout.setBackgroundResource(R.drawable.ic_livestream2);
				imglogin.setBackgroundResource(R.drawable.ic_more2);
			} else if (action == MotionEvent.ACTION_UP) {
				imgcctv.setBackgroundResource(R.drawable.ic_cctv2);
			}


			break;
		case R.id.view_coupon:
		case R.id.discount:
			fragmentManager.beginTransaction()
					.replace(R.id.container, new DiscountWebviewActivity()).addToBackStack(null)
					.commit();

			if (action == MotionEvent.ACTION_DOWN) {

				imgcctv.setBackgroundResource(R.drawable.ic_cctv2);
				imgdiscount.setBackgroundResource(R.drawable.ic_discount1);
				//	imgjorney.setBackgroundResource(R.drawable.ic_jorney1);
				imgjorney.setBackgroundResource(R.drawable.new_nearby1);
				imgabout.setBackgroundResource(R.drawable.ic_livestream2);
				imglogin.setBackgroundResource(R.drawable.ic_more2);
			} else if (action == MotionEvent.ACTION_UP) {
				imgdiscount.setBackgroundResource(R.drawable.ic_discount2);
			}

			break;
		case R.id.view_journey:
		case R.id.jorney:

//			fragmentManager.beginTransaction()
//					.replace(R.id.container, new JourneyWebviewActivity()).addToBackStack(null)
//					.commit();
			fragmentManager.beginTransaction()
					.replace(R.id.container, new NearByMapFragment()).addToBackStack(null).commit();
			if (action == MotionEvent.ACTION_DOWN) {

				imgdiscount.setBackgroundResource(R.drawable.ic_discount2);
				imgcctv.setBackgroundResource(R.drawable.ic_cctv2);
				//	imgjorney.setBackgroundResource(R.drawable.ic_jorney1);
				imgjorney.setBackgroundResource(R.drawable.new_nearby2);
				imgabout.setBackgroundResource(R.drawable.ic_livestream2);
				imglogin.setBackgroundResource(R.drawable.ic_more2);
			} else if (action == MotionEvent.ACTION_UP) {
				imgjorney.setBackgroundResource(R.drawable.new_nearby1);
			}

			break;
		case R.id.view_live:
		case R.id.about:

			fragmentManager.beginTransaction()
					.replace(R.id.container, new LiveStreamFragment2()).addToBackStack(null).commit();

			if (action == MotionEvent.ACTION_DOWN) {

				imgdiscount.setBackgroundResource(R.drawable.ic_discount2);
				imgcctv.setBackgroundResource(R.drawable.ic_cctv2);
				//	imgjorney.setBackgroundResource(R.drawable.ic_jorney1);
				imgjorney.setBackgroundResource(R.drawable.new_nearby1);
				imgabout.setBackgroundResource(R.drawable.ic_livestream1);
				imglogin.setBackgroundResource(R.drawable.ic_more2);
			} else if (action == MotionEvent.ACTION_UP) {
				imgabout.setBackgroundResource(R.drawable.ic_livestream2);
			}

			break;
		case R.id.login:
		case R.id.viewlogin:
			fragmentManager.beginTransaction()
					.replace(R.id.container, new FragmentMore()).addToBackStack(null).commit();

			if (action == MotionEvent.ACTION_DOWN) {

				imgdiscount.setBackgroundResource(R.drawable.ic_discount2);
				imgcctv.setBackgroundResource(R.drawable.ic_cctv2);
				//	imgjorney.setBackgroundResource(R.drawable.ic_jorney2);
				imgjorney.setBackgroundResource(R.drawable.new_nearby1);
				imgabout.setBackgroundResource(R.drawable.ic_livestream2);
				imglogin.setBackgroundResource(R.drawable.ic_more);
			} else if (action == MotionEvent.ACTION_UP) {
				imglogin.setBackgroundResource(R.drawable.ic_more2);
			}

			break;

		}


	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

	//	Toast.makeText(getApplicationContext(),"keyCode"+keyCode,Toast.LENGTH_SHORT).show();
		Fragment fragment = fragmentManager.findFragmentById(R.id.container);
	    if(fragment != null && fragment instanceof JourneyWebviewActivity){
			if (keyCode == KeyEvent.KEYCODE_BACK && fragment != null && ((JourneyWebviewActivity)fragment).canGoBack()) {
				((JourneyWebviewActivity)fragment).goBack();
				return true;
			}else{
				finish();
			}
		}else {
			if(keyCode==KeyEvent.KEYCODE_BACK){
				finish();
			}

		}
		return super.onKeyDown(keyCode, event);

	}

	@Override
	public void onBackPressed() {
		if(fragmentManager.getBackStackEntryCount() == 0){
			finish();
		//	System.exit(0);
		}else{
			super.onBackPressed();
		}
	//	super.onBackPressed();
	//	System.exit(0);
	}


	public void changepageFragment(int page){
		int action = 0;
		switch (page) {
			case 4:
				fragmentManager.beginTransaction()	.replace(R.id.container, new DiscountWebviewActivity()).commit();

				if (action == MotionEvent.ACTION_DOWN) {

					imgcctv.setBackgroundResource(R.drawable.ic_cctv2);
					imgdiscount.setBackgroundResource(R.drawable.ic_discount1);
					//	imgjorney.setBackgroundResource(R.drawable.ic_jorney2);
					imgjorney.setBackgroundResource(R.drawable.new_nearby1);
					imgabout.setBackgroundResource(R.drawable.ic_livestream2);
					imglogin.setBackgroundResource(R.drawable.ic_more2);
				} else if (action == MotionEvent.ACTION_UP) {
					imgdiscount.setBackgroundResource(R.drawable.ic_discount2);
				}

				break;
			case 5:

				fragmentManager.beginTransaction()
						.replace(R.id.container, new JourneyWebviewActivity())
						.commit();

				if (action == MotionEvent.ACTION_DOWN) {

					imgdiscount.setBackgroundResource(R.drawable.ic_discount2);
					imgcctv.setBackgroundResource(R.drawable.ic_cctv2);
				//	imgjorney.setBackgroundResource(R.drawable.ic_jorney1);
					imgjorney.setBackgroundResource(R.drawable.new_nearby1);
					imgabout.setBackgroundResource(R.drawable.ic_livestream2);
					imglogin.setBackgroundResource(R.drawable.ic_more2);
				} else if (action == MotionEvent.ACTION_UP) {
					imgjorney.setBackgroundResource(R.drawable.new_nearby2);
				}

				break;
			case 2:
				fragmentManager.beginTransaction()
						.replace(R.id.container, new LiveStreamFragment()).commit();

				if (action == MotionEvent.ACTION_DOWN) {

					imgdiscount.setBackgroundResource(R.drawable.ic_discount2);
					imgcctv.setBackgroundResource(R.drawable.ic_cctv2);
					//	imgjorney.setBackgroundResource(R.drawable.ic_jorney1);
					imgjorney.setBackgroundResource(R.drawable.new_nearby1);
					imgabout.setBackgroundResource(R.drawable.ic_livestream1);
					imglogin.setBackgroundResource(R.drawable.ic_more2);
				} else if (action == MotionEvent.ACTION_UP) {
					imgabout.setBackgroundResource(R.drawable.ic_livestream2);
				}

				break;
			case 7:
				fragmentManager.beginTransaction()
						.replace(R.id.container, new FragmentMore()).addToBackStack(null).commit();
				if (action == MotionEvent.ACTION_DOWN) {

					imgdiscount.setBackgroundResource(R.drawable.ic_discount2);
					imgcctv.setBackgroundResource(R.drawable.ic_cctv2);
					//	imgjorney.setBackgroundResource(R.drawable.ic_jorney1);
					imgjorney.setBackgroundResource(R.drawable.new_nearby1);
					imgabout.setBackgroundResource(R.drawable.ic_livestream2);
					imglogin.setBackgroundResource(R.drawable.ic_more);
				} else if (action == MotionEvent.ACTION_UP) {
					imglogin.setBackgroundResource(R.drawable.ic_more2);
				}
				break;


		}

	}


	private void setpopupfromapi(final PopupDao popupDao){
		Button button1 = null,button2 = null;
		//GradientDrawable drawable = (GradientDrawable)
		final Button[] buttons = {button1,button2};
		final ImageView imageView;
		if(popupDao.getTemplate_id() == 1) {
			final TransparentDialog alertDialog = new TransparentDialog(this, R.layout.item_dialog_popup_gopro);
			final TransparentDialog transparentDialog = alertDialog;

			//alertDialog.findViewById(R.id.image_popup_1);
			transparentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			button1 = (Button) alertDialog.findViewById(R.id.btn_popup_0);
			button2 = (Button) alertDialog.findViewById(R.id.btn_popup_1);
			buttons[0] = button1;
			buttons[1] = button2;
			imageView = (ImageView) alertDialog.findViewById(R.id.image_popup_1);
			final MobileActivityPropertiesPopup MAPP = popupDao.getimagefromMobileActivityProperties();

			if (MAPP != null)
				imageLoader.displayImage(MAPP.getImage_url(), imageView, options);
			imageView.setBackground(new ColorDrawable(Color.TRANSPARENT));
			imageView.setTag(MAPP.getPopup_action());
			imageView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent;
					switch ((int) v.getTag()) {
						case 0:
							MainApplication.setPopup_check(false);
							transparentDialog.dismiss();
							break;
						case 1:
							intent = new Intent(CCTVMenuMainActivity.this, RegistersActivity.class);
							startActivity(intent);
							MainApplication.setPopup_check(false);
							transparentDialog.dismiss();
							break;
						case 2:
							changepageFragment(2);
							MainApplication.setPopup_check(false);
							transparentDialog.dismiss();
							break;
						case 4:
							changepageFragment(4);
							MainApplication.setPopup_check(false);
							transparentDialog.dismiss();
							break;
						case 5:
							changepageFragment(5);
							MainApplication.setPopup_check(false);
							transparentDialog.dismiss();
							break;
						case 6:
							intent = new Intent(CCTVMenuMainActivity.this, CustomWebview.class);
							intent.putExtra(CustomWebview.class.getName(),MAPP.getContent_url());
							intent.putExtra("title",popupDao.getSubject());
							startActivity(intent);
							MainApplication.setPopup_check(false);
							transparentDialog.dismiss();
							break;
						case 7:
							if(UserUtil.isLoggedIn(CCTVMenuMainActivity.this)){
								changepageFragment(7);
								MainApplication.setPopup_check(false);
								transparentDialog.dismiss();
							}else{
								intent = new Intent(CCTVMenuMainActivity.this, LoginsActivity.class);
								startActivity(intent);
								MainApplication.setPopup_check(false);
								transparentDialog.dismiss();
							}
							break;
						default:

							break;
					}
				}
			});


			ArrayList<MobileActivityPropertiesPopup> MAPPBUTTON = popupDao.getButtonfromMobileActivityPropertiesPopups();

			if (MAPPBUTTON.size() > 0) {
				for (int i = 0; i < MAPPBUTTON.size(); i++) {
					final Button tmp = buttons[i];
					tmp.setText(MAPPBUTTON.get(i).getText());
					tmp.setTextColor(Color.parseColor(MAPPBUTTON.get(i).getText_color()));
					tmp.setBackgroundColor(Color.parseColor(MAPPBUTTON.get(i).getBackground_color()));
					tmp.setTag(MAPPBUTTON.get(i).getPopup_action());


				}
			}


			alertDialog.findViewById(R.id.btn_popup_0).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent intent;
							switch ((int) v.getTag()) {
								case 0:
									MainApplication.setPopup_check(false);
									transparentDialog.dismiss();
									break;
								case 1:
									intent = new Intent(CCTVMenuMainActivity.this, RegistersActivity.class);
									startActivity(intent);
									MainApplication.setPopup_check(false);
									transparentDialog.dismiss();
									break;
								case 2:
									changepageFragment(2);
									MainApplication.setPopup_check(false);
									transparentDialog.dismiss();
									break;
								case 4:
									changepageFragment(4);
									MainApplication.setPopup_check(false);
									transparentDialog.dismiss();
									break;
								case 5:
									changepageFragment(5);
									MainApplication.setPopup_check(false);
									transparentDialog.dismiss();
									break;
								case 6:
									intent = new Intent(CCTVMenuMainActivity.this, CustomWebview.class);
									startActivity(intent);
									MainApplication.setPopup_check(false);
									transparentDialog.dismiss();
									break;
								case 7:
									if(UserUtil.isLoggedIn(CCTVMenuMainActivity.this)){
										changepageFragment(7);
										MainApplication.setPopup_check(false);
										transparentDialog.dismiss();
									}else{
										intent = new Intent(CCTVMenuMainActivity.this, LoginsActivity.class);
										startActivity(intent);
										MainApplication.setPopup_check(false);
										transparentDialog.dismiss();
									}
									break;
								default:

									break;
							}

						}
					});

			alertDialog.findViewById(R.id.btn_popup_1).setOnClickListener(
					new OnClickListener() {
						//Intent intent;

						@Override
						public void onClick(View v) {
							Intent intent;
							switch ((int) v.getTag()) {
								case 0:
									MainApplication.setPopup_check(false);
									transparentDialog.dismiss();
									break;
								case 1:
									intent = new Intent(CCTVMenuMainActivity.this, RegistersActivity.class);
									startActivity(intent);
									MainApplication.setPopup_check(false);
									transparentDialog.dismiss();
									break;
								case 2:
									changepageFragment(2);
									MainApplication.setPopup_check(false);
									transparentDialog.dismiss();
									break;
								case 4:
									changepageFragment(4);
									MainApplication.setPopup_check(false);
									transparentDialog.dismiss();
									break;
								case 5:
									changepageFragment(5);
									MainApplication.setPopup_check(false);
									transparentDialog.dismiss();
									break;
								case 6:
									intent = new Intent(CCTVMenuMainActivity.this, CustomWebview.class);
									startActivity(intent);
									MainApplication.setPopup_check(false);
									transparentDialog.dismiss();
									break;
								case 7:
									if(UserUtil.isLoggedIn(CCTVMenuMainActivity.this)){
										changepageFragment(7);
										MainApplication.setPopup_check(false);
										transparentDialog.dismiss();
									}else{
										intent = new Intent(CCTVMenuMainActivity.this, LoginsActivity.class);
										startActivity(intent);
										MainApplication.setPopup_check(false);
										transparentDialog.dismiss();
									}
									break;
								default:

									break;
							}


						}
					});

			alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			alertDialog.setCanceledOnTouchOutside(true);
			alertDialog.show();
		}else {
			final TransparentDialog alertDialog = new TransparentDialog(this, R.layout.item_dialog_popup_iwatch);
			final TransparentDialog transparentDialog = alertDialog;

			transparentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			imageView = (ImageView) alertDialog.findViewById(R.id.image_popup_iwatch);
			final MobileActivityPropertiesPopup MAPP = popupDao.getimagefromMobileActivityProperties();

			if (MAPP != null)
				imageLoader.displayImage(MAPP.getImage_url(), imageView, options);
			imageView.setBackground(new ColorDrawable(Color.TRANSPARENT));

			imageView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent;
					switch ((int) v.getTag()) {
						case 1:
							intent = new Intent(CCTVMenuMainActivity.this, RegistersActivity.class);
							startActivity(intent);
							MainApplication.setPopup_check(false);
							transparentDialog.dismiss();
							break;
						case 2:
							changepageFragment(2);
							MainApplication.setPopup_check(false);
							transparentDialog.dismiss();
							break;
						case 4:
							changepageFragment(4);
							MainApplication.setPopup_check(false);
							transparentDialog.dismiss();
							break;
						case 5:
							changepageFragment(5);
							MainApplication.setPopup_check(false);
							transparentDialog.dismiss();
							break;
						case 6:
							intent = new Intent(CCTVMenuMainActivity.this, CustomWebview.class);
							startActivity(intent);
							MainApplication.setPopup_check(false);
							transparentDialog.dismiss();
							break;
						case 7:
							intent = new Intent(CCTVMenuMainActivity.this, LoginsActivity.class);
							startActivity(intent);
							MainApplication.setPopup_check(false);
							transparentDialog.dismiss();
							break;
						default:

							break;
					}
				}
			});

			transparentDialog.setCanceledOnTouchOutside(true);

			transparentDialog.show();
		}

	}

	private void Get_Popup_Form_Api(){
		try {

			String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_popup);
			JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject jsonObject) {
					try {

						setpopupfromapi(new PopupDao(jsonObject));
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError volleyError) {
					//Log.d(TAG, "onErrorResponse: ");
					return;
				}
			});
			MySingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

			//	return true;
		}catch (Exception e){
			MainApplication.getInstance().trackException(e);
		}
	}


	private void GetCheckVersion(){
		String url = ResourceUtil.getServiceUrl(this) +getResources().getString(R.string.app_service_checkversion);
		JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject jsonObject) {
				Log.d(CCTVMenuMainActivity.class.getSimpleName(), "onResponse: "+ jsonObject.toString());
				ShowAlertDialog(jsonObject);
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				Log.d(CCTVMenuMainActivity.class.getSimpleName(), "onErrorResponse: "+volleyError.getMessage());
			}
		}){
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				String versionName = BuildConfig.VERSION_NAME;
				HashMap<String, String> mapheader = new HashMap<>();
				mapheader.put("x-tit-os","android");
				mapheader.put("x-tit-version",versionName);
				return mapheader;
			}
		};
		try {
			Map<String, String> header = objectRequest.getHeaders();
			Log.d(CCTVMenuMainActivity.class.getSimpleName(), "getcheckversion: "+header);
		} catch (AuthFailureError authFailureError) {
			authFailureError.printStackTrace();
		}
		MainApplication.getInstance().addToRequestQueue(objectRequest);


	}

	/**
	 * sent user to play store
	 *
	 *
	 * */
	private void Call_Update(){
		Intent intent;
		final String appPackageName = getPackageName();
		try {
			intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
		}catch (Exception e){
			intent = new Intent(Intent.ACTION_VIEW,  Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
		}
		startActivity(intent);
	}

	private void get_Category(){
		String url = ResourceUtil.getServiceUrl(this)+getResources().getString(R.string.app_service_rest_category_stream);
		JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
			@Override
			public void onResponse(JSONArray jsonArray) {
				//Log.d(TAG, "onResponse: "+jsonArray.toString());
				try{
					DataCacheUtill.setCategoryStream(CCTVMenuMainActivity.this,new Category_video(jsonArray));
				}catch (Exception e){
					e.getMessage();
				}

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				Log.d(CCTVMenuMainActivity.class.getSimpleName(), "onErrorResponse: "+volleyError.getMessage());
			}
		}){

		};
		MainApplication.getInstance().addToRequestQueue(jsonArrayRequest);

	}

	private void get_hotline(){
		String url = ResourceUtil.getServiceUrl(this) + getString(R.string.app_service_rest_hotline);
		JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
			@Override
			public void onResponse(JSONArray jsonArray) {
				List<HotlinePhone> mHotlinePhones = new ArrayList<>();
				for (int i= 0 ;i <jsonArray.length();i++) {
					try {
						mHotlinePhones.add(new HotlinePhone(jsonArray.getJSONObject(i)));
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				DataCacheUtill.setHotlinePhone(CCTVMenuMainActivity.this,mHotlinePhones);

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				Log.d(CCTVMenuMainActivity.class.getSimpleName(), "onErrorResponse: "+volleyError.getMessage());
			}
		});
		MainApplication.getInstance().addToRequestQueue(jsonArrayRequest);
	}

	private void ShowAlertDialog(JSONObject jsonObject){
		if(0 == JSONUtil.getInt(jsonObject,"status")){
			try {
				JSONObject dataJsonObject = jsonObject.getJSONObject("data");
				if(JSONUtil.getBoolean(dataJsonObject,"status_update")){

					if(JSONUtil.getBoolean(dataJsonObject,"force_update")) {
						Call_Update();
						finish();
					}else {
						mMaterialDialog = new MaterialDialog(this);
						mMaterialDialog.setTitle("New Update "+dataJsonObject.getString("last_version"))
								.setMessage(dataJsonObject.getString("detail"))
								//mMaterialDialog.setBackgroundResource(R.drawable.background);
								.setPositiveButton("OK", new View.OnClickListener() {
									@Override public void onClick(View v) {
										mMaterialDialog.dismiss();
										Call_Update();
									}
								})
								.setNegativeButton("CANCEL",
										new View.OnClickListener() {
											@Override public void onClick(View v) {
												mMaterialDialog.dismiss();
												//	Toast.makeText(getActivity(),"Cancel", Toast.LENGTH_LONG).show();
											}
										})
								.setCanceledOnTouchOutside(true)
								.setOnDismissListener(
										new DialogInterface.OnDismissListener() {
											@Override
											public void onDismiss(DialogInterface dialog) {
												//	Toast.makeText(getActivity(),"onDismiss",Toast.LENGTH_SHORT).show();
											}
										})
								.show();

					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		Fragment fragmentmain = getSupportFragmentManager().findFragmentById(R.id.container);
		if (fragmentmain != null) {
			fragmentmain.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}

	private void registerInBackground() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
					}
					gcm.unregister();
					Log.d("", "REGISTER WITH ID[" + senderID+ "]");
					regid = gcm.register(senderID);

					Log.i("", "GCM ID[" + regid + "]");

					postNotificationRegister();
				//	postNotificationRegister();
					PushRegisterUtil.storeRegistrationId(getApplicationContext(), regid);
				} catch (IOException ex) {
					Log.e("", "register push in background error", ex);
				}

				return PushRegisterUtil.getRegistrationId(getApplicationContext());
			}
		}.execute(null, null, null);
	}



    private Notification putCommand() {
		User user = UserUtil.getUser(this);
		Notification noti = new Notification();
		noti.setUniqueDeviceId(regid);
		noti.setUserId(user.getId());
		noti.setOs("Android");
		noti.setPlatform("mobile-app");
		noti.setLanguage("en");
		return noti;
	}

	public void postNotificationRegister() {

		NotificationRegisterCommand putCommand = new NotificationRegisterCommand(getApplicationContext());

		putCommand.setNotification(putCommand());

		Log.i("Command", "Content:" + putCommand.toString());

		new GetSelectServiceTask(this).execute(putCommand);

	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i("==========", "This device is not supported.");
				finish();
			}

			Toast.makeText(this, "Google play service is need to use push feature", Toast.LENGTH_LONG).show();

			return false;
		}
		return true;
	}


	class GetSelectServiceTask extends CommonServiceTask {
		public GetSelectServiceTask(FragmentActivity context) {
			super(context);
		}

		@Override
		protected ServiceResponse doInBackground(Command... params) {
			JSONObject json = null;
			ServiceConnector srConnector = new ServiceConnector(ResourceUtil.getServiceUrl(getApplicationContext()) + "/devices");
			Log.i("Url", ":" + srConnector);
			User user = UserUtil.getUser(getApplicationContext());
				List<NameValuePair> headers = new ArrayList<NameValuePair>();
				headers.add(new BasicNameValuePair("Content-Type", "application/json"));
				headers.add(new BasicNameValuePair("X-TIT-ACCESS-TOKEN", user.getToken()));
				ServiceResponse serviceResponse = srConnector.doAsynPost(params[0], true, headers);
				Log.i("headers", "URL :" + headers);
			try {
				int code = serviceResponse.getCode();
				Log.i("code", ":" + code);
				if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
					json = new JSONObject(serviceResponse.getContent().toString());

					Log.i("TIT", "Response" + json);
				} else if (code == HttpStatus.SC_INTERNAL_SERVER_ERROR) {
					json = new JSONObject(serviceResponse.getContent().toString());
					Log.i("TIT", "Response" + json.toString());
				}



			} catch (Exception e) {
				Log.e("TIT", "doInBackground error", e);
			}
			return serviceResponse;
		}


		@Override
		protected void onPostExecute(ServiceResponse result) {
			super.onPostExecute(result);

			try {
				switch (result.getCode()) {
					case ServiceResponse.SUCCESS:
					case HttpStatus.SC_OK:
						context.setResult(Activity.RESULT_OK);

						break;
					case 2://restore token failed
						//TODO logout

						break;


				}
			} catch (Exception e) {
				// Toast.makeText(context, "ERROR!\r\n" + e.getMessage(),	Toast.LENGTH_LONG).show();
				Log.e("TIT", "process response error", e);

			}
		}
	}


	/////////// WiFi GetAccount command


	public void getWiFiGenerate(){

		String	url ="http://10.200.0.1/mediator/index.php";
		//create command object
		WiFiGenerate obj = new WiFiGenerate();
			obj.setPosUsername("admin@touchtechnologies.co.th");
			obj.setPosPassword("T0UCHics");
			obj.setIp(WIFIMod.getLocalAddress(this));
			obj.setPackageID("3");
			obj.setCommand("9100");
			obj.setInterfaces("CloudPOS");
			obj.setChannel("POS");
			Log.i("Url ", url + " \nwith " + obj.toJson());

		JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, obj.toJson(), new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject obj) {

				try {
					WiFiGenerate objWiFi = new WiFiGenerate(obj);

					Log.i("===Response===",">"+objWiFi.getMessage());

					JSONObject json = new JSONObject(obj.toString());
					JSONArray jAccount = json.getJSONArray("Accounts");
						InternetAccount internetAccount = new InternetAccount(jAccount.getJSONObject(0));
						Log.i("===Response===", "> User :" + internetAccount.getUsername().toString() + "\nPassword :" + internetAccount.getPassword().toString());

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
		}, this);

		((MainApplication) getApplicationContext()).addToRequestQueue(jsonRequest);

	}

	@Override
	public void onErrorResponse(VolleyError volleyError) {

	}

///////////////////// getNotification command

	public void getNotificationLiveStream() {
		String url = ResourceUtil.getServiceUrl(this) +getResources().getString(R.string.app_service_love_live)+livechannel.getNotificationOption().getNotifiableId();
		Log.i(AppConfig.LOG, "URL :" + url);
		new RestServiceLiveStreamPropertieCommand(this, url) {
			protected void onPostExecute(LiveStreamConnection result) {

				if (result!=null) {
					LiveStreamConnection live = result;
					LiveStreamingChannel liveNoti = new LiveStreamingChannel();
					liveNoti.setLiveStream(live);
						Log.i(AppConfig.LOG, "RESPONE :" + liveNoti);
					Intent intent = new Intent(getApplicationContext(), LiveOnAirPlayerActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra(LiveStreamingChannel.class.getName(), liveNoti);
					startActivity(intent);
					//  PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
					//finish();

				}
				return ;

			}

		}.execute();
	}


}
