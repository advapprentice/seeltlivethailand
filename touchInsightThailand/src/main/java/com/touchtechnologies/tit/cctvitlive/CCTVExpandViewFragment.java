package com.touchtechnologies.tit.cctvitlive;

import java.util.List;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.lib.animator.FloatingGroupExpandableListView;
import com.touchtechnologies.tit.lib.animator.WrapperExpandableListAdapter;
import com.touchtechnologies.wifi.WIFIMod;


@SuppressLint("ValidFragment")
public class CCTVExpandViewFragment extends Fragment {
	int greenToWhite = 0xFFFFFFFF;
	int blue = 0xFF0099FF;
	int green = 0xFF00EE00;
	private List<ROI> listRoi;
	private CCTVExpandViewAdapter adapter;
	CCTV cctv;
	View viewEmptyHint;
	FloatingGroupExpandableListView list;
	
	@SuppressLint("ValidFragment")
	public CCTVExpandViewFragment(List<ROI> listRoi) {
		this.listRoi = listRoi;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_cctv_main, container, false);
		
        viewEmptyHint = (View)view.findViewById(R.id.emptyAgentHint);
		
		if(!WIFIMod.isNetworkConnected(getActivity())){
			viewEmptyHint.setVisibility(View.VISIBLE);
		}else{
			viewEmptyHint.setVisibility(View.GONE);
		}
		
		 
		adapter = new CCTVExpandViewAdapter(getActivity());
		adapter.setData(listRoi);
	
		
		  list = (FloatingGroupExpandableListView)view.findViewById(R.id.sample_activity_list);
		
		final LayoutInflater newinflater = getLayoutInflater(savedInstanceState);
		
        final View header = newinflater.inflate(R.layout.activity_cctv_list_header, list, false);
		list.addHeaderView(header);

//        final View footer = newinflater.inflate(R.layout.activity_cctv_list_footer, list, false);
//        footer.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.touchinsightthailand.com"));
//                startActivity(intent);
//            }
//        });
//		list.addFooterView(footer);
		
		// Even though the child divider has already been set on the layout file, we have to set it again here
		// This prevents a bug where the background turns to the color of the child divider when the list is expanded
		list.setChildDivider(new ColorDrawable(Color.WHITE));
		
//		final CCTVExpandViewAdapter adapter = new CCTVExpandViewAdapter(getActivity());
		final WrapperExpandableListAdapter wrapperAdapter = new WrapperExpandableListAdapter(adapter);
		list.setAdapter(wrapperAdapter);
		
		for(int i = 0; i < wrapperAdapter.getGroupCount(); i++) {
			list.expandGroup(i);
		}

		
		list.setOnScrollFloatingGroupListener(new FloatingGroupExpandableListView.OnScrollFloatingGroupListener() {
			
			@Override
			public void onScrollFloatingGroupListener(View floatingGroupView, int scrollY) {
				float interpolation = - scrollY / (float) floatingGroupView.getHeight();
				
				// Changing from RGB(162,201,85) to RGB(255,255,255)
				final int greenToWhiteRed = (int) (greenToWhite * interpolation);
				final int greenToWhiteGreen = (int) (green * interpolation);
				final int greenToWhiteBlue = (int) (blue * interpolation);
				final int greenToWhiteColor = Color.argb(blue, greenToWhiteGreen, greenToWhiteGreen, greenToWhiteGreen);
				
				// Changing from RGB(255,255,255) to RGB(0,0,0)
				final int whiteToBlackRed = (int) (255 - 255 * interpolation);
				final int whiteToBlackGreen = (int) (255 - 255 * interpolation);
				final int whiteToBlackBlue = (int) (255 - 255 * interpolation);
				final int whiteToBlackColor = Color.argb(255, whiteToBlackRed, 107, 118);
				
//				final ImageView image = (ImageView) floatingGroupView.findViewById(R.id.sample_activity_list_group_item_image);
//				image.setBackgroundColor(greenToWhiteGreen);
//				
//				final Drawable imageDrawable = image.getDrawable().mutate();
//				imageDrawable.setColorFilter(whiteToBlackColor, PorterDuff.Mode.SRC_ATOP);
				
				final View background = floatingGroupView.findViewById(R.id.sample_activity_list_group_item_background);
				background.setBackgroundColor(blue);
				
				final TextView text = (TextView) floatingGroupView.findViewById(R.id.sample_activity_list_group_item_text);
				text.setTextColor(greenToWhite);
				

//				icon expand
//				final ImageView expanded = (ImageView) floatingGroupView.findViewById(R.id.sample_activity_list_group_expanded_image);
//				final Drawable expandedDrawable = expanded.getDrawable().mutate();
//				expandedDrawable.setColorFilter(whiteToBlackColor, PorterDuff.Mode.SRC_ATOP);
			}
		});
		
		
		return view;
	}


	@Override
	public void onResume() {
		super.onResume();
		if(adapter.getGroupCount() == 0 )
			list.setEmptyView(viewEmptyHint);


	}

	public CCTVExpandViewAdapter getAdapter() {
		return adapter;
	}
}
