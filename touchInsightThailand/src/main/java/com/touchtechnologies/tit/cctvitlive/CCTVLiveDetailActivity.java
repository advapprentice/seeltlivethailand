package com.touchtechnologies.tit.cctvitlive;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Intent;

import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.opensource.pagerindicator.CirclePageIndicator;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.dataobject.insightthailand.Comment;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.attraction.AttractionListActivity;
import com.touchtechnologies.tit.cctvitlive.history.ListCCTVHistoryActivity;
import com.touchtechnologies.tit.comment.CCTVCommentActivity;
import com.touchtechnologies.tit.hotel.HotelListActivity;
import com.touchtechnologies.tit.lib.animator.DepthPageTransformer;
import com.touchtechnologies.tit.restaurant.RestaurantListActivity;
import com.touchtechnologies.tit.search.Searchactivity;
import com.touchtechnologies.tit.util.ResourceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CCTVLiveDetailActivity extends FragmentActivity implements
		OnClickListener, OnMapClickListener, OnMarkerClickListener, OnMapReadyCallback,
		OnInfoWindowClickListener {

	private ROI roi;
	private TextView txtnamecam, txtnamepoi, txtnameroi, txtCCTVTotalView,
			txtPhoneNumber, txtWebsite, txtFacebook;
	private boolean checklistcomment = false;
	private TextView txtdescription, txtv_comment_count;
	private CCTVLiveDetailPagerAdapter adapter;
	private HashMap<Marker, CCTV> hMarkers;
	private CCTVLiveDetailMapinfoAdapter adapterInfo;
	private GoogleMap googleMap;
	private ArrayList<Comment> comments;
	protected Location location;
	private int mapZoomLevel = 12;
	private List<CCTV> arraycctv;
	private View viewall, viewattraction, viewrestaurant, viewhotel,
			viecctvhistory, viewStatusOnline;
	private TextView textView_showcomment;
	private ScrollView mScrollView;
	private Button btnGetDirection, btnCallNow;
	private int currentcctv;

	// private RelativeLayout mRelativeLayout;
	CCTV cctv;
	ViewPager pager;
	CirclePageIndicator circlepager;
	Marker iconadd;
	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API.
	 * See https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	private GoogleApiClient client;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail_cctvitlive);
		mScrollView = (ScrollView) findViewById(R.id.viewscroll);
		comments = new ArrayList<Comment>();
		hMarkers = new HashMap<>();

		roi = (ROI) getIntent().getSerializableExtra(ROI.class.getName());
		findViewById(R.id.view_comment_count).setOnClickListener(this);

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(0xFF1275AE));
		actionBar.setTitle(Html.fromHtml("<font color='#FFFFFF'>" + roi.getName() + "</font>"));
		actionBar.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));

		adapterInfo = new CCTVLiveDetailMapinfoAdapter(this);
		adapterInfo.setMarker(hMarkers);

		arraycctv = roi.getCCTVs();

		cctv = roi.getCCTVAt(getIntent().getIntExtra("position", 0));
		getcctvcomment(cctv, false);

		txtnamecam = (TextView) findViewById(R.id.roiname);
		txtnamepoi = (TextView) findViewById(R.id.namepoi);
		txtnameroi = (TextView) findViewById(R.id.txtroiname);
		txtdescription = (TextView) findViewById(R.id.txtdescriptionroi);

		txtCCTVTotalView = (TextView) findViewById(R.id.txtCCTVTotalView);
		txtPhoneNumber = (TextView) findViewById(R.id.txtPhoneNumber);
		txtWebsite = (TextView) findViewById(R.id.txtWebsite);
		txtFacebook = (TextView) findViewById(R.id.txtFacebookFanpage);
		textView_showcomment = (TextView) findViewById(R.id.txtv_show_comment_cctv);
		txtv_comment_count = (TextView) findViewById(R.id.textView_comment_count);

		viewall = (View) findViewById(R.id.viewall);
		viecctvhistory = (View) findViewById(R.id.viewhistory);
		viewattraction = (View) findViewById(R.id.viewattraction);
		viewrestaurant = (View) findViewById(R.id.viewrestaurant);
		viewhotel = (View) findViewById(R.id.viewhotel);
		btnGetDirection = (Button) findViewById(R.id.btnGetDirection);
		btnCallNow = (Button) findViewById(R.id.btnCallNow);
		viewStatusOnline = (View) findViewById(R.id.viewStatus);

		textView_showcomment.setOnClickListener(this);
		viecctvhistory.setOnClickListener(this);
		viewall.setOnClickListener(this);
		viewattraction.setOnClickListener(this);
		viewrestaurant.setOnClickListener(this);
		viewhotel.setOnClickListener(this);
		btnGetDirection.setOnClickListener(this);


		txtnamecam.setText(roi.getName());
		txtnamepoi.setText(cctv.getLabel_en());
		txtnameroi.setText(cctv.getLabel_en());

		if (cctv.getOnlineStatus() == 0) {
			viewStatusOnline.setVisibility(View.VISIBLE);

		} else {
			viewStatusOnline.setVisibility(View.GONE);
		}

		try {

			String cctvName = cctv.getLabel_en().replaceAll(" ", "_").toLowerCase();
			MainApplication.getInstance().trackScreenView("CCTV_" + cctvName);

			txtCCTVTotalView.setText("" + NumberFormat.getInstance(Locale.US).format(cctv.getView()));
			txtPhoneNumber.setText(cctv.getPhoneNumber().equals("") ? "-" : cctv.getPhoneNumber());
			txtWebsite.setText(cctv.getWebSite().equals("") ? "-" : cctv.getWebSite());
			txtFacebook.setText(cctv.getFacebook().equals("") ? "-" : cctv.getFacebook());
			txtdescription.setText(Html.fromHtml("<font color='#000000'>" + cctv.getDescription() + "</font>"));

			if (cctv.getPhoneNumber().equals("")) {
				btnCallNow.setOnClickListener(null);
				btnCallNow.setBackground(getResources().getDrawable(R.drawable.bg_button_call_gray));
				btnCallNow.setPadding(50, 10, 50, 10);
			} else {
				btnCallNow.setOnClickListener(this);
				btnCallNow.setBackground(getResources().getDrawable(R.drawable.bg_button_call_blue));
				btnCallNow.setPadding(50, 10, 50, 10);
			}


		} catch (Exception e) {
			txtPhoneNumber.setText("-");
			txtWebsite.setText("-");
			txtFacebook.setText("-");
			txtdescription.setText("-");
			Log.d(AppConfig.LOG, "Error" + e.getMessage());
		}


		adapter = new CCTVLiveDetailPagerAdapter(this);
		adapter.setData(roi);

		PagerTabStrip pagerTabStrip = (PagerTabStrip) findViewById(R.id.PagerTabStrip);
		pagerTabStrip.setDrawFullUnderline(true);
		pagerTabStrip.setTabIndicatorColor(0xFF6495ED);

		pager = (ViewPager) findViewById(R.id.viewpager);
		pager.setPageTransformer(true, new DepthPageTransformer());
		pager.setAdapter(adapter);
		pager.setCurrentItem(getIntent().getIntExtra("position", 0));
		pager.setClipToPadding(false);

		circlepager = (CirclePageIndicator) findViewById(R.id.indicator);
		circlepager.setViewPager(pager);
		float density = getResources().getDisplayMetrics().density;
		circlepager.setRadius(6 * density);
		circlepager.setPageColor(0xFFCFCFCF);
		circlepager.setFillColor(0xFF777777);
		circlepager.setStrokeColor(0xFFCFCFCF);
		circlepager.setStrokeWidth(2 * density);

		// searchhandle(getIntent());

		circlepager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				cctv = roi.getCCTVAt(position);

				if (googleMap != null) {
					googleMap.clear();
					googleMap.animateCamera(CameraUpdateFactory
							.newLatLngZoom(new LatLng(cctv.getLatitude(),
									cctv.getLongitude()), mapZoomLevel));
				}

				txtnamepoi.setText(cctv.getLabel_en());
				// String test = cctv.getLabel_en();
				txtnameroi.setText(cctv.getLabel_en());
				if (cctv.getOnlineStatus() == 0) {
					viewStatusOnline.setVisibility(View.VISIBLE);
				} else {
					viewStatusOnline.setVisibility(View.GONE);
				}
				try {
					String cctvName = cctv.getLabel_en().replaceAll(" ", "_").toLowerCase();
					MainApplication.getInstance().trackScreenView("CCTV_" + cctvName);

					txtCCTVTotalView.setText("" + NumberFormat.getInstance(Locale.US).format(cctv.getView()));
					txtPhoneNumber.setText(cctv.getPhoneNumber().equals("") ? "-" : cctv.getPhoneNumber());
					txtWebsite.setText(cctv.getWebSite().equals("") ? "-" : cctv.getWebSite());
					txtFacebook.setText(cctv.getFacebook().equals("") ? "-" : cctv.getFacebook());
					txtdescription.setText(Html.fromHtml("<font color='#000000'>" + cctv.getDescription() + "</font>"));

					if (cctv.getPhoneNumber().equals("")) {
						//btnCallNow.setVisibility(View.GONE);
						btnCallNow.setOnClickListener(null);
						btnCallNow.setBackground(getResources().getDrawable(R.drawable.bg_button_call_gray));
						btnCallNow.setPadding(50, 10, 50, 10);
					} else {
						//btnCallNow.setVisibility(View.VISIBLE);
						btnCallNow.setOnClickListener(CCTVLiveDetailActivity.this);
						btnCallNow.setBackground(getResources().getDrawable(R.drawable.bg_button_call_blue));
						btnCallNow.setPadding(50, 10, 50, 10);
					}


				} catch (Exception e) {
					btnCallNow.setVisibility(View.GONE);
					txtPhoneNumber.setText("-");
					txtWebsite.setText("-");
					txtFacebook.setText("-");
					txtdescription.setText("-");
					Log.d(AppConfig.LOG, "Error" + e.getMessage());
				}
				onUpdatemap(arraycctv);

				if (googleMap != null) {
					iconadd = googleMap.addMarker(new MarkerOptions()
							.position(
									new LatLng(cctv.getLatitude(), cctv.getLongitude())).title(cctv.getLabel_en())
							// .title(cctv.getKeyName())
							// .snippet(hotel.getDisplayAddress())
							.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_maker2)));
					hMarkers.put(iconadd, cctv);
				}
				getcctvcomment(cctv, false);
				adapter.setSelectedPageIndex(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1,
									   int position) {

			}

			@Override
			public void onPageScrollStateChanged(int state) {
				Log.d("PageChanged", "state=" + state);

			}

		});

		Point point = new Point();
		this.getWindowManager().getDefaultDisplay().getSize(point);
		pager.getLayoutParams().height = (int) ((point.x * 2.5) / 3.5);
		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.share, menu);

		setupSearchMenuItem(menu);
		/*
		 * MenuItem menuitem = menu.findItem(R.id.action_search); SearchManager
		 * searchManager =
		 * (SearchManager)getSystemService(Context.SEARCH_SERVICE); SearchView
		 * searchView = (SearchView) menuitem.getActionView(); try{
		 *
		 *
		 * if(searchView != null)
		 * searchView.setSearchableInfo(searchManager.getSearchableInfo
		 * (getComponentName())); searchView.setSubmitButtonEnabled(true);
		 * searchView.setQueryHint("Type Someting...");
		 *
		 * }catch(Exception e){ throw e; } SearchView.OnQueryTextListener
		 * onQueryTextListener = new SearchView.OnQueryTextListener() {
		 *
		 * @Override public boolean onQueryTextSubmit(String arg0) {
		 * Log.d("summit",arg0); startintent(arg0); return false; }
		 *
		 * @Override public boolean onQueryTextChange(String arg0) {
		 * Log.d("change",arg0); return false; } };
		 * searchView.setOnQueryTextListener(onQueryTextListener);
		 */

		return true;
	}

	private void setupSearchMenuItem(Menu menu) {
		MenuItem searchItem = menu.findItem(R.id.menusearch);
		if (searchItem != null) {
			SearchView searchView = (SearchView) searchItem.getActionView();
			if (searchView != null) {
				SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
				searchView.setSearchableInfo(searchManager
						.getSearchableInfo(getComponentName()));

			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menushare) {
			try {
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_TEXT, cctv.getUrlCCTV());
				startActivity(Intent.createChooser(intent, "Share with..."));

				Log.i("url =", cctv.getUrlCCTV());

			} catch (Exception e) {
				Log.e("error", "share a link error", e);
			}

			return true;
		} else if (item.getItemId() == android.R.id.home) {

			finish();
			return true;
		} else if (item.getItemId() == R.id.menusearch) {
			Intent intent = new Intent(this, Searchactivity.class);
			startActivity(intent);

			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		adapter.onDestroyed();
	}


	void onUpdatemap(List<CCTV> list) {

		if (googleMap == null) {
			return;
		}
		// if(list

		try {
			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			for (CCTV cctv : list) {

				Marker tp = googleMap.addMarker(new MarkerOptions()
						.position(
								new LatLng(cctv.getLatitude(), cctv
										.getLongitude()))
						.title(cctv.getLabel_en())
						// .title(cctv.getKeyName())
						// .snippet(hotel.getDisplayAddress())
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.ic_pin_cctv)));

				hMarkers.put(tp, cctv);

				Log.i("CCTVLN",
						"CCTV " + cctv.getKeyName() + " " + cctv.getLatitude()
								+ "/" + cctv.getLongitude());
			}

			Marker tp = googleMap
					.addMarker(new MarkerOptions()
							.position(
									new LatLng(cctv.getLatitude(), cctv
											.getLongitude()))
							.title(cctv.getLabel_en())
							.icon(BitmapDescriptorFactory
									.fromResource(R.drawable.ic_maker2)));
			hMarkers.put(tp, cctv);

			googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
					new LatLng(cctv.getLatitude(), cctv.getLongitude()),
					mapZoomLevel));

		} catch (Exception e) {
			Log.e("map", "Map marker " + e.getMessage());
		}
	}


	public void setMapZoomLevel(int mapZoomLevel) {
		this.mapZoomLevel = mapZoomLevel;
	}

	public void setLocation(Location location) {
		this.location = location;

		if (googleMap != null) {
			// move map to last know location if possible when started
			LatLng myLastLatLon = new LatLng(location.getLatitude(),
					location.getLongitude());
			CameraUpdate camUpdate = CameraUpdateFactory.newLatLngZoom(
					myLastLatLon, mapZoomLevel);

			googleMap.animateCamera(camUpdate);
		}
	}

	public void onLocationChanged(Location location) {
		this.location = location;

		if (client.isConnected()) {
			client.disconnect();
		}

	}

	@Override
	public void onMapClick(LatLng point) {
		MarkerOptions mark = new MarkerOptions();

		googleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
		mark.position(point);

	}

	@Override
	public void onClick(View v) {
		Intent intent;
		Tracker t;
		String dimensionValue = "Android";
		String cctvName = cctv.getLabel_en().replaceAll(" ", "_").toLowerCase();
		switch (v.getId()) {
			case R.id.viewhistory:

				intent = new Intent(this, ListCCTVHistoryActivity.class);
				intent.putExtra(CCTV.class.getName(), cctv);
				startActivity(intent);
				break;

			case R.id.viewall:

				break;
			case R.id.viewattraction:
				intent = new Intent(this, AttractionListActivity.class);
				intent.putExtra(ROI.class.getName(), roi);
				startActivity(intent);
				break;
			case R.id.viewrestaurant:
				intent = new Intent(this, RestaurantListActivity.class);
				intent.putExtra(ROI.class.getName(), roi);
				startActivity(intent);

				break;
			case R.id.viewhotel:
				intent = new Intent(this, HotelListActivity.class);
				intent.putExtra(ROI.class.getName(), roi);
				startActivity(intent);
				break;
			case R.id.btnGetDirection:

				MainApplication.getInstance().trackScreenView("CCTV_" + cctvName + "_GetDirect");
				MainApplication.getInstance().trackEvent("CCTV_" + cctvName + "_GetDirect", "GetDirec", "Send event");

				Uri gmmIntentUri = Uri
						.parse("google.navigation:" + cctv.getLatitude() + ","
								+ cctv.getLongitude()).buildUpon()
						.appendQueryParameter("q", cctv.getLabel_en()).build();
				Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
				mapIntent.setPackage("com.google.android.apps.maps");
				startActivity(mapIntent);
				break;
			case R.id.btnCallNow:
				MainApplication.getInstance().trackScreenView("CCTV_" + cctvName + "_CallNow");
				MainApplication.getInstance().trackEvent("CCTV_" + cctvName + "_CallNow", "GetDirec", "Send event");

				try {

					Intent callIntent = new Intent(Intent.ACTION_CALL);
					callIntent.setData(Uri.parse("tel:" + cctv.getPhoneNumber()));
					if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
						// TODO: Consider calling
						//    ActivityCompat#requestPermissions
						// here to request the missing permissions, and then overriding
						//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
						//                                          int[] grantResults)
						// to handle the case where the user grants the permission. See the documentation
						// for ActivityCompat#requestPermissions for more details.
						return;
					}
					startActivity(callIntent);
				} catch (ActivityNotFoundException activityException) {
					Log.e("dialing", "Call failed", activityException);
				}
				break;
//			case R.id.txtv_show_comment_cctv:
//				getcctvcomment(cctv,true);
//				break;
			case R.id.view_comment_count:
				getcctvcomment(cctv,true);
				break;
		}

	}

	@Override
	public boolean onMarkerClick(Marker marker) {

		CCTV cctv = hMarkers.get(marker);
		int index = arraycctv.indexOf(cctv);

		pager.setCurrentItem(index);
		txtnamepoi.setText(cctv.getLabel_en());
		txtnameroi.setText(cctv.getLabel_en());
		txtdescription.setText(Html.fromHtml("<font color='#000000'>"
				+ cctv.getDescription() + "</font>"));

		return false;
	}

	@Override
	public void onInfoWindowClick(Marker marker) {

		CCTV cctv = hMarkers.get(marker);
		Uri gmmIntentUri = Uri
				.parse("google.navigation:" + cctv.getLatitude() + ","
						+ cctv.getLongitude()).buildUpon()
				.appendQueryParameter("q", cctv.getLabel_en()).build();
		// Uri gmmIntentUri =
		// Uri.parse("google.navigation:q="+hotel.getName()+","+hotel.getLatitude()+","+hotel.getLongitude()+"&mode=b");
		Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
		mapIntent.setPackage("com.google.android.apps.maps");
		startActivity(mapIntent);

	}


	private void getcctvcomment(CCTV cctv,boolean boo) {
		String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_cctv)
				+ cctv.getId() + getResources().getString(R.string.app_service_comment);

		Response.Listener<JSONArray> listener;
		if(boo){
			listener = jsonArrayListener_comment;
		}else {
			listener = jsonArrayListener2;
		}

		JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,listener, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {

			}
		});
		MainApplication.getInstance().addToRequestQueue(jsonArrayRequest);

	}

	private Response.Listener<JSONArray> jsonArrayListener_comment = new Response.Listener<JSONArray>() {
		@Override
		public void onResponse(JSONArray jsonArray) {
				comments.clear();
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					JSONObject jsonObject = jsonArray.getJSONObject(i);
					Comment comment = new Comment(jsonObject);
					comments.add(comment);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
				cctv.setComments(comments);
				Intent intent;
				intent = new Intent(CCTVLiveDetailActivity.this, CCTVCommentActivity.class);
				intent.putExtra(CCTV.class.getName(), cctv);
				startActivity(intent);

		}
	};

	private Response.Listener<JSONArray> jsonArrayListener2 = new Response.Listener<JSONArray>() {
		@Override
		public void onResponse(JSONArray jsonArray) {
			try{
				int length = jsonArray.length();
				txtv_comment_count.setText(""+length);
			}catch (Exception e){
				txtv_comment_count.setText("0");
			}

		}
	};


	@Override
	public void onStart() {
		super.onStart();


	}

	@Override
	public void onStop() {
		super.onStop();
//		hMarkers.clear();
//		arraycctv.clear();
//		comments.clear();
		if(client.isConnected()){
			client.disconnect();
		}
	}

	@Override
	public void onMapReady(GoogleMap googleMapv) {
		googleMap = googleMapv;
		try {
			if (googleMap == null) {
				SupportMapFragment  mapcustomview = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
				mapcustomview.getMapAsync(this);
				// googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hotel.getLocation(),getResources().getInteger(R.integer.map_zoom_default)));
				// googleMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new
				// LatLng(hotel.getLatitude(), hotel.getLongitude()) , 12.0f) );
				if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
					// TODO: Consider calling
					//    ActivityCompat#requestPermissions
					// here to request the missing permissions, and then overriding
					//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
					//                                          int[] grantResults)
					// to handle the case where the user grants the permission. See the documentation
					// for ActivityCompat#requestPermissions for more details.
					return;
				}
				googleMap.setMyLocationEnabled(false);
				googleMap.setOnInfoWindowClickListener(this);
				googleMap.setOnMarkerClickListener(this);
				googleMap.setInfoWindowAdapter(adapterInfo);

				((Mapcustomview) getSupportFragmentManager().findFragmentById(
						R.id.map))
						.setListener(new Mapcustomview.OnTouchListener() {
							@Override
							public void onTouch() {
								mScrollView
										.requestDisallowInterceptTouchEvent(true);
							}
						});

			}

			onUpdatemap(arraycctv);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
