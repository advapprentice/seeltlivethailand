package com.touchtechnologies.tit.cctvitlive.history;


import java.util.List;
import java.util.Timer;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.touchtechnologies.dataobject.CCTVHighlight;
import com.touchtechnologies.tit.R;

public class ListCCTVHistoryAdapter extends BaseAdapter  implements OnClickListener{
	private Context context;
	private Runnable runnable;
	protected ImageLoader imageLoader;
	protected DisplayImageOptions options;
	Timer t;
	private List<CCTVHighlight> arrcctv;
	private CCTVHighlight cctv;
	String url;
	public ListCCTVHistoryAdapter(Context context) {
		this.context = context;
		imageLoader = ImageLoader.getInstance();
			
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.NONE)
		.showImageForEmptyUri(R.drawable.bg_cctvdefault)
		.showImageOnFail(R.drawable.bg_cctvdefault)
		.showImageOnLoading(R.drawable.bg_cctvdefault)
		.cacheInMemory(true)
		.displayer(new FadeInBitmapDisplayer(300)) //fade in images
	  	.resetViewBeforeLoading(true)
	  	.build();

 	     	  
 	    imageLoader = ImageLoader.getInstance();
	
		
	}
	public void setData(List<CCTVHighlight> arrcctv){
		this.arrcctv =  arrcctv;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return arrcctv == null?0:arrcctv.size();
	}
	
	@Override
	public CCTVHighlight getItem(int position) {
		return arrcctv.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}

	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		
		if(convertView == null){
			convertView = LayoutInflater.from(context).inflate(R.layout.item_cctv_history, parent, false);
			
			viewHolder = new ViewHolder();
			
			viewHolder.viewHighlight =(View)convertView.findViewById(R.id.viewcctv);
			viewHolder.imgshare =(ImageView)convertView.findViewById(R.id.imgshare);
			viewHolder.tvtimer =(TextView)convertView.findViewById(R.id.txttimer);
			viewHolder.imglive =(ImageView)convertView.findViewById(R.id.imgDisplay);
			viewHolder.tvdate =(TextView)convertView.findViewById(R.id.txtdate);
			
			
			viewHolder.viewHighlight.setOnClickListener(this);
			viewHolder.imgshare.setOnClickListener(this);
			convertView.setTag(viewHolder);
		}else{
			viewHolder = (ViewHolder)convertView.getTag();
		}
		    String time = getItem(position).getHighlighttime().substring(0,2);
		  
		    String timeminute = getItem(position).getHighlighttime().substring(2);
		    
		
		    viewHolder.tvdate.setText(time+":"+ timeminute);
      		viewHolder.tvtimer.setText(getItem(position).getHighlighttime());
      		
      		imageLoader.displayImage(getItem(position).getHighlightUrl(), viewHolder.imglive, options);
      	
      		
      		
      		
			viewHolder.viewHighlight.setTag(arrcctv.get(position));
			viewHolder.imgshare.setTag(arrcctv.get(position));
      		return convertView;
	}



	class ViewHolder{
		ImageView imgshare;
		ImageView imglive;
		TextView tvtimer;
		TextView tvdate;
		TextView tvcctv;
		TextView tvupdate;
		View Viewvideoschedule;
		View viewHighlight;
		
	}



	@Override
	public void onClick(View v) {
		Intent intent;
		CCTVHighlight cctv = (CCTVHighlight)v.getTag();
		switch (v.getId()) {
		case R.id.imgshare:


			intent = new Intent(Intent.ACTION_SEND);
		//	intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT, cctv.getShareCCTVHighlight());
			context.startActivity(Intent.createChooser(intent, "Share with..."));

	  //  	Toast.makeText(context, "test"+cctv.getHighlightUrl(), Toast.LENGTH_SHORT).show();
			
			break;
		case R.id.viewcctv:

			 try {
				    intent = new Intent(context, CCTVHightlighFullScreenActivity.class);
					intent.putExtra(CCTVHighlight.class.getName(), cctv);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					context.startActivity(intent);
							
			} catch (ActivityNotFoundException activityException) {
			        Log.e("dialing", "Call failed", activityException);
		    }
			
	//		Toast.makeText(context, "test"+highlight.getHighlightUrl(), Toast.LENGTH_SHORT).show();
			
			break;

		}
		
	}
	
	
	
}