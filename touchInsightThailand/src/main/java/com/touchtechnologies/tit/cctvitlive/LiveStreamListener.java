package com.touchtechnologies.tit.cctvitlive;

import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;

import java.util.List;

/**
 * Created by TouchICS on 3/29/2016.
 */
public interface LiveStreamListener {
    void ready(List<LiveStreamingChannel> list);
}
