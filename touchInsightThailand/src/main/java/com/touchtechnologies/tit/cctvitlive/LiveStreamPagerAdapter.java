package com.touchtechnologies.tit.cctvitlive;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.touchtechnologies.dataobject.insightthailand.CategoryLiveStream;
import com.touchtechnologies.tit.dataobject.Category_video;
import com.touchtechnologies.tit.history.LiveHistoryGridviewFragment;
import com.touchtechnologies.tit.live.liststreaming.LiveStreamGridViewFragment;
import com.touchtechnologies.tit.live.mylivestream.FragmentMyliveStream;
import com.touchtechnologies.tit.util.DataCacheUtill;

import java.util.ArrayList;
import java.util.List;

public class LiveStreamPagerAdapter extends FragmentPagerAdapter {

	int PAGE_COUNT = 3;
	// Tab Titles
	private ArrayList<String> tabtitles = new ArrayList<>();
	private Context context;
	private String Onair = "On Air",history = "History Live",mylive = "My Livestream";

	public LiveStreamPagerAdapter(Context context, FragmentManager fm) {
		super(fm);
		tabtitles.add(Onair);
		tabtitles.add(history);
		//tabtitles.add(mylive);
		List<CategoryLiveStream> mCategory_videos = DataCacheUtill.getCategoryStream(context);
		for(int i = 0; i<mCategory_videos.size();i++){
			String name = mCategory_videos.get(i).getCategoryName();
			if(name!=null){
				tabtitles.add(name);
			}

		}
		tabtitles.add(mylive);
		PAGE_COUNT = tabtitles.size();
		this.context = context;
	}

	@Override
	public int getCount() {
		return PAGE_COUNT;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public Fragment getItem(int position) {		
//		switch (position) {
//
//			// Open FragmentTab1.java
//		case 0:
//
//			return Fragment.instantiate(context, LiveStreamGridViewFragment.class.getName());
//
//			// Open FragmentTab2.java
//		case 1:
//			//return Fragment.instantiate(context, LiveHistoryGridviewFragment.class.getName());
//			return LiveHistoryGridviewFragment.newInstance(position);
//		case 2:
//
//			return Fragment.instantiate(context, FragmentMyliveStream.class.getName());
//			// Open FragmentTab3.java
//
//		}
//		return null;
		if(position > 0 && position < PAGE_COUNT -1){
			return LiveHistoryGridviewFragment.newInstance(position);
		}else if(position == 0){
			return Fragment.instantiate(context, LiveStreamGridViewFragment.class.getName());
		}else {
			return Fragment.instantiate(context, FragmentMyliveStream.class.getName());
		}
		//return null;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return tabtitles.get(position);
	}
}
