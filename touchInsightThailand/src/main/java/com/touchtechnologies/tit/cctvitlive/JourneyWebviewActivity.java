package com.touchtechnologies.tit.cctvitlive;


import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.util.UserUtil;

public class JourneyWebviewActivity extends Fragment{
	private WebView webView;
	private ProgressBar progress;
	private User user;
	private Map <String, String> extraHeaders;
	@Override
	public View onCreateView(LayoutInflater inflater,
							 @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		final	View rootview = inflater.inflate(R.layout.activity_webview, container, false);

		MainApplication.getInstance().trackScreenView("BlogTravel");

		user = UserUtil.getUser(getActivity());
		String url = "http://seeitlivethailand.com/blogs";
		webView = (WebView)rootview.findViewById(R.id.webView);

		WebViewClient wc = new WebViewClient(){

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				extraHeaders = new HashMap<String, String>();
				if(user!= null) {
					extraHeaders.put("x-tit-access-token", user.getToken());
				}else {
					extraHeaders.put("x-tit-access-token", "");
				}
				extraHeaders.put("x-tit-web-view","Android");
				view.loadUrl(url, extraHeaders);
				return false;
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				progress =(ProgressBar)rootview.findViewById(R.id.progressBarWebLoading);
				progress.setVisibility(View.GONE);


			}


		};


		extraHeaders = new HashMap<String, String>();
		extraHeaders.put("x-tit-access-token",user.getToken());
		extraHeaders.put("x-tit-web-view","Android");

		webView.setWebViewClient(wc);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setEnableSmoothTransition(true);
		webView.getSettings().setUseWideViewPort(true);

		webView.loadUrl(url,extraHeaders);


		return rootview;
	}

	public boolean canGoBack() {
		return this.webView != null && this.webView.canGoBack();
	}

	public void goBack() {
		if(this.webView != null) {
			this.webView.goBack();
		}
	}


}
