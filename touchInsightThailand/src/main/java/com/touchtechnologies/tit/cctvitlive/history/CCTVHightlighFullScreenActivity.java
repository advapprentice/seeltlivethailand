package com.touchtechnologies.tit.cctvitlive.history;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.touchtechnologies.animate.imageview.TouchImageView;
import com.touchtechnologies.dataobject.CCTVHighlight;
import com.touchtechnologies.tit.R;

public class CCTVHightlighFullScreenActivity extends Activity implements OnClickListener{
	private CCTVHighlight cctv;
	private  ImageLoader imageLoader;
	private Runnable runnable;
	private TouchImageView imglive;
	protected DisplayImageOptions options;
	private int count=0;
	private Button btndone;
	private View viewshare;
	private Drawable image;
	private int zoomControler=20;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	    requestWindowFeature(Window.FEATURE_NO_TITLE);
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.item_cctv_hightligh_full_screen);
	
		imageLoader = ImageLoader.getInstance();
		
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
		.showImageForEmptyUri(R.drawable.bg_cctvdefault)
		.showImageOnFail(R.drawable.bg_cctvdefault)
		.showImageOnLoading(R.drawable.bg_cctvdefault)
		.cacheInMemory(true)
		.resetViewBeforeLoading(true).build(); 
		
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
		.memoryCache(new WeakMemoryCache())
		.threadPriority(Thread.NORM_PRIORITY)
        .denyCacheImageMultipleSizesInMemory()
        .build();
 	     	  
 	    imageLoader = ImageLoader.getInstance();
		if(!imageLoader.isInited()){
			imageLoader.init(config);	
		}
		cctv = (CCTVHighlight) getIntent().getSerializableExtra(CCTVHighlight.class.getName());
		
		
		viewshare = (View)findViewById(R.id.viewcctvshare);
		btndone = (Button)findViewById(R.id.btndone);
		btndone.setOnClickListener(this);
		viewshare.setOnClickListener(this);
		imglive = (TouchImageView)findViewById(R.id.bgcctvlive);
	
		imageLoader.displayImage(cctv.getHighlightUrl(), imglive);
		
	}
	
	    
	    
	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {
		case R.id.btndone:
			finish();
			
			break;
		case R.id.viewcctvshare:
			intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT,cctv.getShareCCTVHighlight());
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		    startActivity(intent);

			break;
			
		}
		
	}
	

}
