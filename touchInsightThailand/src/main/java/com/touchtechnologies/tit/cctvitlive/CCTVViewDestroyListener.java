package com.touchtechnologies.tit.cctvitlive;

public interface CCTVViewDestroyListener {
	void onDestroyed();
}
