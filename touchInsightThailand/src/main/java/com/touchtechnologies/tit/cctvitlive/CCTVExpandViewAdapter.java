package com.touchtechnologies.tit.cctvitlive;


import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;


import android.content.Context;

import android.content.Intent;
import android.graphics.Bitmap;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;


import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.tit.R;
public class CCTVExpandViewAdapter extends BaseExpandableListAdapter implements OnClickListener{
	private ImageLoader imageLoader;
	private final Context mContext;
	private final LayoutInflater mLayoutInflater;
	private List<ROI> rois;
	private CCTV cctv;
	private DisplayImageOptions options;
	private int posionselect;
	double latitude;
	double longitude;
	public CCTVExpandViewAdapter(Context context) {
		this.mContext = context;

		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.NONE)
				.showImageForEmptyUri(R.drawable.bg_cctvdefault)
				.showImageOnFail(R.drawable.bg_cctvdefault)
				.showImageOnLoading(R.drawable.bg_cctvdefault).cacheInMemory(true)
				.resetViewBeforeLoading(true).build();
		imageLoader = ImageLoader.getInstance();

		imageLoader = ImageLoader.getInstance();

		mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	//	updateLocationNearby();

	}
	
	
	public void setData(List<ROI> rois){
		this.rois = rois;
		
		
		notifyDataSetChanged();
	}
	
	@Override
	public int getGroupCount() {
		return rois == null?0:rois.size();
	}

	@Override
	public ROI getGroup(int groupPosition) {
		return rois.get(groupPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
	
		ExpandableListView mExpandableListView = (ExpandableListView) parent;
	    mExpandableListView.expandGroup(groupPosition);
		
		if(convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.activity_cctv_list_group_item, parent, false);
		}

		TextView text = (TextView) convertView.findViewById(R.id.sample_activity_list_group_item_text);
//		text.setText(mGroups[groupPosition]);
		text.setText(getGroup(groupPosition).getName() == null ? "" : getGroup(groupPosition).getName() );
//		Log.d("test", "TEST"+getGroup(groupPosition).getName());
		
		
//		
//		ImageView expandedImage = (ImageView) convertView.findViewById(R.id.sample_activity_list_group_expanded_image);
//		int resId = isExpanded ? R.drawable.ic_drop : R.drawable.ic_plus;
//		expandedImage.setImageResource(resId);

	
		return convertView;
	}

	
	
	@Override
	public int getChildrenCount(int groupPosition) {
	//	return mChilds[groupPosition].length;
		return rois.get(groupPosition).sizeCCTV();
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
	//	return mChilds[groupPosition][childPosition];
		
		return rois.get(groupPosition).getCCTVAt(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = new ViewHolder();

		if(convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.activity_cctv_list_child_item, parent, false);
			viewHolder.imgcam =(ImageView)convertView.findViewById(R.id.imgviewcam);
			viewHolder.text = (TextView) convertView.findViewById(R.id.namecam);
			viewHolder.textdate = (TextView) convertView.findViewById(R.id.textdatecam);
			viewHolder.txtCCTVTotalView= (TextView) convertView.findViewById(R.id.txtCCTVTotalView);
			viewHolder.txtDistance = (TextView) convertView.findViewById(R.id.txtDistance);
			//	text.setText(mChilds[groupPosition][childPosition]);
			viewHolder.viewdetail =(View)convertView.findViewById(R.id.viewdetail);
			convertView.setTag(viewHolder);
		}else {
			viewHolder =(ViewHolder)convertView.getTag();
		}



		viewHolder.viewdetail.setTag(new int[]{groupPosition, childPosition});
		viewHolder.viewdetail.setOnClickListener(this);
		
		Double distance = null;
		
		DecimalFormat format = new DecimalFormat("@@##");
		 distance = rois.get(groupPosition).getCCTVAt(childPosition).getDistance();
		 String formattedText = null;
		 if(distance != null){
			 formattedText = format.format(distance);
		 	viewHolder.txtDistance.setText(formattedText +" Km");
		 }else {
			 viewHolder.txtDistance.setText("null");
		}

		viewHolder.text.setText(rois.get(groupPosition).getCCTVAt(childPosition).getLabel_en());
		viewHolder.textdate.setText(rois.get(groupPosition).getCCTVAt(childPosition).getUpdated());
		viewHolder.txtCCTVTotalView.setText(""+NumberFormat.getInstance(Locale.US).format(rois.get(groupPosition).getCCTVAt(childPosition).getView()));
		
		imageLoader.displayImage(rois.get(groupPosition).getCCTVAt(childPosition).getUrl(), viewHolder.imgcam, options);
		
		
		Log.d("name=", +groupPosition+"group="+childPosition);
		
	
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		
		
	
//		 Toast.makeText(mContext, "group="+groupPosition+"child id="+childPosition, Toast.LENGTH_SHORT).show();
		return false;
	}


	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.viewdetail:
			
			int[] tag = (int[])v.getTag();
			int currentGroup = tag[0];
			int currentChild = tag[1];
			
			Intent intent = new Intent(mContext, CCTVLiveDetailActivity.class);
		
			intent.putExtra("position",currentChild);
			intent.putExtra(ROI.class.getName(), rois.get(currentGroup));
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			mContext.startActivity(intent);
			
			
		
		}
		
	}


	private class ViewHolder{
		ImageView imgcam ;
		TextView text ;
		TextView textdate ;
		TextView txtCCTVTotalView;
		TextView txtDistance ;
		View viewdetail ;
	}
}