package com.touchtechnologies.tit.cctvitlive.history;

import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.touchtechnologies.animate.progress.RotateLoading;
import com.touchtechnologies.command.insightthailand.RestServiceCCTVHistoryCommand;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.CCTVHighlight;
import android.util.Log;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.util.ResourceUtil;

public class ListCCTVHistoryActivity extends SettingActionbarActivity{
	private ListView cctvView;
	private ListCCTVHistoryAdapter adapter;
	private RotateLoading rotateLoading;
	CCTV cctv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cctv_list_history);
			cctv = (CCTV) getIntent().getSerializableExtra(CCTV.class.getName());
			mTitle= "History";


			String cctvName =   cctv.getLabel_en().replaceAll(" ", "_").toLowerCase();
        	MainApplication.getInstance().trackScreenView("CCTV_"+cctvName+"_History");



			cctvView = (ListView) findViewById(R.id.list);
			
			
			TextView tvcctv = (TextView)findViewById(R.id.txtcctv);
	        rotateLoading = (RotateLoading) findViewById(R.id.rotateloading);
	        rotateLoading.start();
			tvcctv.setText(cctv.getLabel_en());
			
			
			adapter = new ListCCTVHistoryAdapter(this);
			cctvView.setAdapter(adapter);

			
			
			
			String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_cctv);
			new RestServiceCCTVHistoryCommand(this,cctv, url){
		
				protected void onPostExecute(CCTV result) {
			
				cctv = result;
				List<CCTVHighlight> highlight  = cctv.getCCTVHighlights();
				
				adapter.setData(highlight);
				
				adapter.notifyDataSetChanged();
					
				rotateLoading.setVisibility(View.GONE);
				};
			}.execute();
		
			
	
			
	}
	

}
