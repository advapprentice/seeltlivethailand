package com.touchtechnologies.tit.cctvitlive;

import java.util.HashMap;
import java.util.Map;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.util.UserUtil;

public class DiscountWebviewActivity extends Fragment{
	private WebView webView;
	private ProgressBar progress;
	@Override
	public View onCreateView(LayoutInflater inflater,
							 @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		final View rootview = inflater.inflate(R.layout.activity_webview, container, false);


		MainApplication.getInstance().trackScreenView("Coupon");

		User user = UserUtil.getUser(getActivity());
		WebViewClient wc = new WebViewClient(){
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				progress =(ProgressBar)rootview.findViewById(R.id.progressBarWebLoading);
				progress.setVisibility(View.GONE);

			}

		};

		webView = (WebView)rootview.findViewById(R.id.webView);
		String url = "http://seeitlivethailand.com/coupon";

		Map <String, String> extraHeaders = new HashMap<String, String>();
		extraHeaders.put("x-tit-access-token",user.getToken());
		extraHeaders.put("x-tit-web-view","Android");

		webView.setWebViewClient(wc);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setJavaScriptEnabled(true);

		webView.loadUrl(url,extraHeaders);


		return rootview;
	}

}
