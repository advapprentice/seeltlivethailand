package com.touchtechnologies.tit.cctvitlive;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.Toast;



import com.touchtechnologies.command.insightthailand.RestServiceROIListCommand;
import com.touchtechnologies.command.insightthailand.RestServiceTotalViewCommand;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.LocationAwareFragment;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.persistent.DataHelperAsyn;
import com.touchtechnologies.tit.util.ResourceUtil;

import static com.touchtechnologies.tit.persistent.DataHelperAsyn.*;

public class CCTVMenuFillterFragment extends LocationAwareFragment implements
		OnClickListener, OnScrollListener {
	private final List<ROI> listRoi = new ArrayList<>();
	private boolean check_location = true,cachedata = true;
	private List<JSONObject> listViewStatistic = new ArrayList<>();
	private FragmentManager fragmentManager;
	private ImageView imglistcctv, imglistgrid;
	private Fragment fragment;
	private Location lastknowlocation;
	private double longitude;
	private double latitude;
	private final int NUMBER_OF_STATVIEW =3;
	int myLastVisiblePos;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//updateLocationNearby();
		mRequestingLocationUpdates = false;
		if (savedInstanceState == null) {
			String cachedRoi = DataHelperAsyn.get(KEY_CACHED_ROI_CCTVS);
			if (cachedRoi != null) {
				listRoi.clear();
				listRoi.addAll(DataHelperAsyn.parseROI(cachedRoi));
			}

			String cachedStat = DataHelperAsyn.get(KEY_CACHED_STATISTIC);
			if (cachedStat != null) {
				listViewStatistic.clear();
				listViewStatistic.addAll(DataHelperAsyn.parseStatictic(cachedStat));
			}
			if(savedInstanceState != null){
				lastknowlocation = savedInstanceState.getParcelable("lastlocation");
			}else{
				lastknowlocation = ResourceUtil.getLocation(getActivity());
			}

			// list roi


		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
							 @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_title_cctv, container,false);

		fragmentManager = getChildFragmentManager();

		imglistcctv = (ImageView) view.findViewById(R.id.cctvlist);
		imglistgrid = (ImageView) view.findViewById(R.id.cctvgrid);

		imglistcctv.setBackgroundResource(R.drawable.ic_list_cctv1);

		imglistcctv.setOnClickListener(this);
		imglistgrid.setOnClickListener(this);
		imglistgrid.setClickable(false);

		setActiveFragment(0);


		return view;
	}

	@Override
	public void onStart() {
		super.onStart();

	}

	void setActiveFragment(int index) {
		fragmentManager = getChildFragmentManager();
		switch (index) {
			case 0:
				fragment = new CCTVListFragment(listRoi);

				break;
			case 1:
				fragment = new CCTVExpandViewFragment(listRoi);
				break;
		}

		fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
	}

	/**
	 * Send command RestServiceROIListCommand
	 */
	void sendRestServiceROIListCommand() {
		String url = ResourceUtil.getServiceUrl(getActivity())
				+ getResources().getString(R.string.app_service_rest_rois);

		new RestServiceROIListCommand(getActivity(), url) {

			protected void onPostExecute(java.util.List<ROI> result) {

					setDatatoList(result);


			}

		}.execute();
	}

	void sendRestServiceTotalViewCommand() {
		FragmentActivity fragmentActivity = getActivity();
		if(fragmentActivity == null)
			return;

		String url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_properties);

		new RestServiceTotalViewCommand(getActivity(), url) {

			protected void onPostExecute(JSONArray json) {
				// update view
				if(getActivity() != null)
					updateFragmentViewStatistic(json);
			}
		}.execute();
	}

	@Override
	public void onClick(View v) {
		int action = 0;
		switch (v.getId()) {
			case R.id.cctvlist:
				sortbyview();
				setActiveFragment(0);

				if (action == MotionEvent.ACTION_DOWN) {
					imglistcctv.setBackgroundResource(R.drawable.ic_list_cctv1);
					imglistgrid.setBackgroundResource(R.drawable.ic_grid_cctv2);

				} else if (action == MotionEvent.ACTION_UP) {
					imglistcctv.setBackgroundResource(R.drawable.ic_list_cctv2);
				}

				updateFragmentView();
				break;
			case R.id.cctvgrid:
				sortbydistance();
				setActiveFragment(1);

				if (action == MotionEvent.ACTION_DOWN){
					imglistcctv.setBackgroundResource(R.drawable.ic_list_cctv2);
					imglistgrid.setBackgroundResource(R.drawable.ic_grid_cctv1);

				} else if (action == MotionEvent.ACTION_UP){
					imglistgrid.setBackgroundResource(R.drawable.ic_grid_cctv2);
				}

				updateFragmentView();
				break;
		}

	}

	void updateFragmentView() {
		// update cctv fragment
		if(fragment ==  null)
			fragment =getChildFragmentManager().findFragmentById(R.id.container);


		if (fragment instanceof CCTVListFragment) {
			CCTVListsAdapter adapter = ((CCTVListFragment)fragment).getAdapter();
			if (adapter != null) {
				if (sortbyview()) {
					adapter.setData(listRoi);
					((CCTVListFragment) fragment).updateFragmentViewStatistic(listViewStatistic);
				}
				imglistgrid.setClickable(true);
			}
		} else if (fragment instanceof CCTVExpandViewFragment) {
			CCTVExpandViewAdapter adapter = ((CCTVExpandViewFragment) fragment).getAdapter();
			if (adapter != null) {
				if (sortbydistance())
					adapter.setData(listRoi);
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.d("location", "onResume: "+mLastLocation);
		if(cachedata) {
			List<ROI> result = new ArrayList<ROI>();
			result.addAll(listRoi);
			if (listRoi != null && !listRoi.isEmpty())
				setDatatoList(result);
			else
				sendRestServiceROIListCommand();
		}else {
			sendRestServiceROIListCommand();
		}
	}

	/**
	 * Update statistic view
	 *
	 * @param jsonProperties
	 *            response from Rest
	 */
	void updateFragmentViewStatistic(JSONArray jsonProperties) {

		if (fragmentManager != null && fragment != null) {
			if (fragment instanceof CCTVListFragment) {
				CCTVListsAdapter adapter = ((CCTVListFragment)fragment).getAdapter();
				if (adapter != null) {
					adapter.setData(listRoi);
					// create roi view statistic
					listViewStatistic.clear();
					try {
						for (int i = 0; i < listRoi.size(); i++) {
							ROI roi = listRoi.get(i);
							JSONObject json = new JSONObject();
							json.put("title", roi.getKeyName().toUpperCase());
							json.put("view", roi.getViewCount());
							json.put("imageUrl", roi.getImageUrlCoverStatistic());

							listViewStatistic.add(json);
						}

						// sort roi by view count
						Collections.sort(listViewStatistic,
								new Comparator<JSONObject>() {
									@Override
									public int compare(JSONObject lhs,
													   JSONObject rhs) {
										int c = 0;
										try {
											int l = lhs.getInt("view");
											int r = rhs.getInt("view");

											if (l > r) {
												c = -1;
											} else if (l < r) {
												c = 1;
											}

										} catch (Exception e) {
											MainApplication.getInstance().trackException(e);
										}

										return c;
									}
								});

						// add total view
						JSONObject jsonTotal = new JSONObject();
						jsonTotal.put("title", "TOTAL VIEW");
						try {
							for (int i = 0; i < jsonProperties.length(); i++) {
								JSONObject tmp = jsonProperties.getJSONObject(i);
								String key_value = tmp.getString("keyname");
								if ("site_view_all_cover".equals(key_value)) {
									jsonTotal.put("imageUrl",
											tmp.getString("content_en"));

								} else if ("site_view_all".equals(key_value)) {
									jsonTotal.put("view",
											Integer.parseInt(tmp.getString("content_en")));
								}
							}
						}catch (Exception e){
							MainApplication.getInstance().trackException(e);
						}

						// use only first 2 roi
						try {
							listViewStatistic.add(0, jsonTotal);
							listViewStatistic = listViewStatistic.subList(0, NUMBER_OF_STATVIEW);

						}catch (Exception e){
							Log.e(AppConfig.LOG, e.getMessage(), e);
							MainApplication.getInstance().trackException(e);
						}

						// cache
						JSONArray jsonCache = new JSONArray();
						for (JSONObject json : listViewStatistic) {
							jsonCache.put(json);
						}
						DataHelperAsyn.set(KEY_CACHED_STATISTIC, jsonCache.toString());

						// update view
						((CCTVListFragment)fragment).updateFragmentViewStatistic(listViewStatistic);
						imglistgrid.setClickable(true);

					} catch (Exception e) {
						Log.e(AppConfig.LOG, e.getMessage(), e);
						MainApplication.getInstance().trackException(e);
					}
				}
			}
		}
	}

	private void updateLocationNearby(Location lastKnownLocation) {


		if (lastKnownLocation != null) {
			lastknowlocation = lastKnownLocation;
			longitude = lastKnownLocation.getLongitude();
			latitude = lastKnownLocation.getLatitude();
			Log.d("MY LOCATION ", "=" + latitude + "/" + longitude);
			ResourceUtil.setLocation(getActivity(),lastKnownLocation);
			stopLocationUpdates();


		} else {
			if(check_location) {
				if(getActivity() != null)
					Toast.makeText(getActivity(), "Can't get your location", Toast.LENGTH_SHORT).show();
				check_location =false;
			}
		}

	}

	private boolean sortbydistance() {
		if(listRoi != null){
			if(!listRoi.isEmpty()){
				for (ROI roi : listRoi) {
					List<CCTV> cctvs = roi.getCCTVs();
					Collections.sort(cctvs);
					roi.setDistance();
				}
				Collections.sort(listRoi, new Comparator<ROI>() {

					@Override
					public int compare(ROI lhs, ROI rhs) {

						return lhs.getDistance().compareTo(rhs.getDistance());
					}
				});
				return true;
			}else {
				return false;
			}
		}else {
			return false;
		}
	}

	private boolean sortbyview() {
		for (ROI roi : listRoi) {
			List<CCTV> cctvs = roi.getCCTVs();
			Collections.sort(cctvs, new Comparator<CCTV>() {
				@Override
				public int compare(CCTV lhs, CCTV rhs) {
					return rhs.getView() - lhs.getView();
				}
			});
			Collections.sort(cctvs, new Comparator<CCTV>() {

				@Override
				public int compare(CCTV lhs, CCTV rhs) {

					return rhs.getOnlineStatus() - lhs.getOnlineStatus();
				}
			});
		}
		Collections.sort(listRoi, new Comparator<ROI>() {

			@Override
			public int compare(ROI lhs, ROI rhs) {
				List<CCTV> lhscctv	= lhs.getcctvonline();
				List<CCTV> rhscctv	= rhs.getcctvonline();

				if(!lhscctv.isEmpty() && !rhscctv.isEmpty()){
					Collections.sort(lhscctv,new Comparator<CCTV>() {

						@Override
						public int compare(CCTV lhs, CCTV rhs) {

							return rhs.getView() - lhs.getView();
						}
					});
					Collections.sort(rhscctv,new Comparator<CCTV>() {

						@Override
						public int compare(CCTV lhs, CCTV rhs) {

							return rhs.getView() - lhs.getView();
						}
					});
					return rhs.getCCTVAt(0).getView() - lhs.getCCTVAt(0).getView();
				}else if(lhscctv.isEmpty()) {
					return rhs.getCCTVAt(0).getView() - 0;
				}else {
					return 0 - lhs.getCCTVAt(0).getView();
				}
			}
		});
		return true;

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
						 int visibleItemCount, int totalItemCount) {
		int currentFirstVisPos = view.getFirstVisiblePosition();
		if (currentFirstVisPos > myLastVisiblePos) {
			Log.d(AppConfig.LOG, "OnScoll Dows");

		}
		if (currentFirstVisPos < myLastVisiblePos) {
			Log.d(AppConfig.LOG, "OnScoll UP");

		}

		myLastVisiblePos = currentFirstVisPos;
	}

	@Override
	public void onDestroyView() {

		super.onDestroyView();
	}

	@Override
	protected void startLocationUpdates() {
		super.startLocationUpdates();
	}

	@Override
	protected void createLocationRequest() {
		super.createLocationRequest();
	}

	@Override
	protected void stopLocationUpdates() {
		super.stopLocationUpdates();
	}

	@Override
	public void onLocationChanged(Location location) {
		super.onLocationChanged(location);
		updateLocationNearby(location);
	}


	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putParcelable("lastlocation",lastknowlocation);
		super.onSaveInstanceState(outState);
	}

	private void setDatatoList(List<ROI> roiList){

		synchronized (listRoi) {
			listRoi.clear();
			// skip ROI with no CCTV
			for (ROI roi : roiList) {
				if (roi.sizeCCTV() > 0) {
					listRoi.add(roi);

				}
			}
		}
		if(mLastLocation != null)
			updateLocationNearby(mLastLocation);
		else
			updateLocationNearby(lastknowlocation);


		Location locationA = new Location("point A");
		locationA.setLatitude(latitude);
		locationA.setLongitude(longitude);

		for (ROI roi : listRoi) {
			List<CCTV> cctvs = roi.getCCTVs();
			for (CCTV cctv : cctvs) {
				Location locationB = new Location("point B");
				locationB.setLatitude(cctv.getLatitude());
				locationB.setLongitude(cctv.getLongitude());
				double distance = locationA.distanceTo(locationB) / 1000.0f;
				cctv.setDistance(distance);
			}
		}

		// caching
		DataHelperAsyn.setAsyn(listRoi, KEY_CACHED_ROI_CCTVS);
		//setActiveFragment(0);
		updateFragmentView();
		// request total view
		sendRestServiceTotalViewCommand();

	}


}