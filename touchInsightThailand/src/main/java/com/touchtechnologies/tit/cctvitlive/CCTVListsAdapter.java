package com.touchtechnologies.tit.cctvitlive;

import java.util.ArrayList;
import java.util.Locale;
import java.text.NumberFormat;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.opensource.pagerindicator.CircleBitmapDisplayer;
import com.opensource.pagerindicator.CirclePageIndicator;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.lib.animator.FullImagePagertransformer;

public class CCTVListsAdapter extends BaseAdapter  {
	private Context context;
	protected ImageLoader imageLoader;
	protected DisplayImageOptions options;
	
	private List<ROI> listRoi;
	private List<CCTVViewDestroyListener> viewDestroylisteners;
	private int[] selectedPagerIndex;
	
	ROI roi;
	int posi;
	
	public CCTVListsAdapter(Context context) {
		this.context = context;
		
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder()
				.cacheInMemory(true)
				.cacheOnDisc(true)
				.bitmapConfig(Bitmap.Config.ARGB_8888)
				// .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
				.showImageForEmptyUri(R.drawable.bg_noimage_circle)
				.showImageOnFail(R.drawable.bg_noimage_circle)
				// .showImageOnLoading(R.drawable.bg_loading_circle)
				.showImageOnLoading(android.R.color.transparent)
				.displayer(new FadeInBitmapDisplayer(300))
				// fade in images
				.resetViewBeforeLoading(true)
				//.displayer(new CircleBitmapDisplayer())
				.build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				context).threadPoolSize(3)
				.threadPriority(Thread.NORM_PRIORITY - 2)
				.denyCacheImageMultipleSizesInMemory().memoryCacheSize(3*1024*1024)
				.defaultDisplayImageOptions(options).build();

		imageLoader = ImageLoader.getInstance();
		if (!imageLoader.isInited()) {
			imageLoader.init(config);
		}

		viewDestroylisteners = new ArrayList<>();
	}

	public void setData(List<ROI> arrcctv) {
		this.listRoi = arrcctv;
		selectedPagerIndex = new int[arrcctv == null?0:arrcctv.size()];
		
		notifyDataSetChanged();
	}

	/**
	 * Calling by fragment when destroyes
	 */
	void onDestroyed(){
		for(CCTVViewDestroyListener listener: viewDestroylisteners){
			listener.onDestroyed();
		}
	}


	
	@Override
	public int getCount() {
		return listRoi == null ? 0 : listRoi.size();
	}

	@Override
	public ROI getItem(int position) {
		return listRoi.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;

		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.item_list_cctv_roi, parent, false);
			viewHolder = new ViewHolder();
			convertView.setTag(viewHolder);
			
			viewHolder.imglive = (ImageView) convertView.findViewById(R.id.logoroi);
			viewHolder.tvroi = (TextView) convertView.findViewById(R.id.txtroi);
			viewHolder.txtCCTVTotelView = (TextView) convertView.findViewById(R.id.txtCCTVTotelView);
			viewHolder.tvpoi = (TextView) convertView.findViewById(R.id.txtpoi);
			viewHolder.textView_pager_cctv = (TextView)convertView.findViewById(R.id.textView_cctv_pager_position);

			viewHolder.pager = (ViewPager) convertView.findViewById(R.id.viewpager);
			viewHolder.viewcctv = (View)convertView.findViewById(R.id.viewcctv);
			viewHolder.viewcctv.setTag(position);
			   
			viewHolder.pager.setCurrentItem(0);
			viewHolder.pager.setClipToPadding(false);
			viewHolder.pager.setPageTransformer(true, new FullImagePagertransformer());
			
			ViewGroup.LayoutParams params = viewHolder.pager.getLayoutParams();
			Point point = new Point();
			((Activity)context).getWindowManager().getDefaultDisplay().getSize(point);
			params.height = point.y * 1/3;
			viewHolder.pager.setLayoutParams(params);

			viewHolder.circlepager = (CirclePageIndicator) convertView.findViewById(R.id.indicator);
			
			
			float density = convertView.getResources().getDisplayMetrics().density;
			viewHolder.circlepager.setRadius(5 * density);
			viewHolder.circlepager.setPageColor(0xFFCFCFCF);
			viewHolder.circlepager.setFillColor(0xFF777777);
			viewHolder.circlepager.setStrokeColor(0xFFCFCFCF);
			viewHolder.circlepager.setStrokeWidth(2*density);
			
			
			
			Matrix m = viewHolder.imglive.getImageMatrix();
			float imageHeight = 100;
			float imageWidth = 100;
			RectF drawableRect = new RectF(0, 0, imageWidth, imageHeight);
			RectF viewRect = new RectF(0, 0, viewHolder.imglive.getWidth(),
					viewHolder.imglive.getHeight());
			m.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.CENTER);

			viewHolder.imglive.setImageMatrix(m);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();

		}

		CCTVListPagerAdapter adapter = new CCTVListPagerAdapter(context);
		viewHolder.adapter = adapter;
		viewHolder.pager.setAdapter(adapter);
		viewHolder.adapter.setSelectedPageIndex(selectedPagerIndex[position]);
		
		//register view destroy
		viewDestroylisteners.add(adapter);
		
		// roi = getItem(position);
		viewHolder.adapter.setData(listRoi.get(position));
		
		viewHolder.tvroi.setText(getItem(position).getName());
    	viewHolder.tvpoi.setText(getItem(position).getCCTVAt(0).getLabel_en());
    	viewHolder.txtCCTVTotelView.setText(""+NumberFormat.getInstance(Locale.US).format(getItem(position).getCCTVAt(0).getView()));
		viewHolder.textView_pager_cctv.setText("1/"+getItem(position).getCCTVs().size());
		viewHolder.circlepager.setViewPager(viewHolder.pager);
//		viewHolder.pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//			@Override
//			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//			}
//
//			@Override
//			public void onPageSelected(int positions) {
////				ROI roi  = getItem(position);
////				CCTV cctv = null;
////				if (roi != null) {
////					if(position < roi.getCCTVs().size())
////						cctv = roi.getCCTVAt(positions);
////					if (cctv != null) {
////						Log.e("CCTV", "cctv selected " + cctv.getKeyName());
////						selectedPagerIndex[position] = positions;
////						viewHolder.adapter.setSelectedPageIndex(positions);
////
////						//cctv = getItem(position).getCCTVAt(positions);
////						viewHolder.tvpoi.setText(cctv.getLabel_en());
////						viewHolder.txtCCTVTotelView.setText(""+NumberFormat.getInstance(Locale.US).format(cctv.getView()));
////						viewHolder.textView_pager_cctv.setText((positions+1) +"/"+roi.getCCTVs().size());
////					}
////				}
//
//
//			}
//
//			@Override
//			public void onPageScrollStateChanged(int state) {
//
//			}
//		});
		viewHolder.circlepager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int positions) {
//				CCTV cctv = getItem(position).getCCTVAt(positions);
//				Log.e("CCTV", "cctv selected " + cctv.getKeyName());
//				selectedPagerIndex[position] = positions;
//				viewHolder.adapter.setSelectedPageIndex(positions);
//
//				cctv = getItem(position).getCCTVAt(positions);
//		        viewHolder.tvpoi.setText(cctv.getLabel_en());
//		        viewHolder.txtCCTVTotelView.setText(""+NumberFormat.getInstance(Locale.US).format(cctv.getView()));
		//		Toast.makeText(context,"posision :"+position, Toast.LENGTH_SHORT).show();

				ROI roi  = getItem(position);
				CCTV cctv = null;
				if (roi != null) {
					//if(position < roi.getCCTVs().size())
						cctv = roi.getCCTVAt(positions);
					if (cctv != null) {
						Log.e("CCTV", "cctv selected " + cctv.getKeyName());
						selectedPagerIndex[position] = positions;
						viewHolder.adapter.setSelectedPageIndex(positions);

						//cctv = getItem(position).getCCTVAt(positions);
						viewHolder.tvpoi.setText(cctv.getLabel_en());
						viewHolder.txtCCTVTotelView.setText(""+NumberFormat.getInstance(Locale.US).format(cctv.getView()));
						viewHolder.textView_pager_cctv.setText((positions+1) +"/"+roi.getCCTVs().size());
						Spannable spanText = Spannable.Factory.getInstance().newSpannable(viewHolder.textView_pager_cctv.getText()); // <- EDITED: Use the original string, as `country` has been converted to lowercase.
						spanText.setSpan(new ForegroundColorSpan(Color.parseColor("#c2c2c2")), 1, viewHolder.textView_pager_cctv.getText().toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
						viewHolder.textView_pager_cctv.setText(spanText, TextView.BufferType.SPANNABLE);

					}
				}

			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int position) {
			
			}
			
			@Override
			public void onPageScrollStateChanged(int state) {
				Log.d("PageChanged", "state=" + state);
			}

		});
		
		Log.e("CCTV", "Get view pager at " + position);
		Spannable spanText = Spannable.Factory.getInstance().newSpannable(viewHolder.textView_pager_cctv.getText()); // <- EDITED: Use the original string, as `country` has been converted to lowercase.
		spanText.setSpan(new ForegroundColorSpan(Color.parseColor("#c2c2c2")), 1, viewHolder.textView_pager_cctv.getText().toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		viewHolder.textView_pager_cctv.setText(spanText, TextView.BufferType.SPANNABLE);
		
		return convertView;
	}
	
	public static float dipToPixels(Context context, float dipValue) {
	    DisplayMetrics metrics = context.getResources().getDisplayMetrics();
	    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
	}
	
	
	class ViewHolder {
		ImageView imglive;
		TextView tvroi;
    	TextView txtCCTVTotelView;
		TextView tvpoi;
		View Viewvideoschedule;
		View viewcctv;
		ViewPager pager;
		CCTVListPagerAdapter adapter;
		CirclePageIndicator circlepager;
		TextView textView_pager_cctv;
	}
}