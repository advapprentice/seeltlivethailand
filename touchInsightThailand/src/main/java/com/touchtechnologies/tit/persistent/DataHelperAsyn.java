package com.touchtechnologies.tit.persistent;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.util.LruCache;

import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.persistent.DatabaseHelper;
import com.touchtechnologies.tit.R;

/**
 * Provide data of poi, roi, category and cctv in  a json format.
 * When getting data this object will return a cached json and it will make a server call to get updated data.
 * @author Touch
 */
public class DataHelperAsyn extends DatabaseHelper{
	static LruCache<String, String> lruCached = new LruCache<>(32);
	
	public static final String KEY_CACHED_ROI_CCTVS = "KEY_CACHED_ROI_CCTVS2";
	public static final String KEY_CACHED_STATISTIC = "KEY_CACHED_STATISTIC";
	
	
	public DataHelperAsyn(Context context) {
		super(context,
				context.getResources().getString(R.string.app_database_name),
				context.getResources().getInteger(R.integer.app_database_version));
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		TABLE tables[] = TABLE.values();
		for (int i = 0; i < tables.length; i++) {
			try {
				db.execSQL(tables[i].getCreateSQL());
				Log.d("DatabaseHelper", tables[i].getName() + " table created");
			} catch (Exception e) {
				Log.e("DatabaseHelper", "create db", e);
			}
		}
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		TABLE tables[] = TABLE.values();
		for (int i = 0; i < tables.length; i++) {
			db.execSQL("DROP TABLE IF EXISTS " + tables[i].getName());
		}
	}
	
	public static String get(String key){
		return lruCached.get(key);
	}
	
	public static void set(String key, String value){
		lruCached.put(key, value);
	}
	
	public static List<ROI> parseROI(String content){
		List<ROI> listROI = new ArrayList<>();
		
		try{
			//parsing response to json
			JSONArray jArray = new JSONArray(content);
			
			//parsing json element to ROI object
			for(int i=0; i<jArray.length(); i++){
				listROI.add(new ROI(jArray.getJSONObject(i)));
			}
		}catch(Exception e){
			Log.e(DatabaseHelper.class.getSimpleName(), e.getMessage(), e);
		}
		
		return listROI;
	}
	
	public static List<JSONObject> parseStatictic(String content){
		List<JSONObject> listJson = new ArrayList<>();
		
		try{
			//parsing response to json
			JSONArray jArray = new JSONArray(content);
			
			//parsing json element to ROI object
			for(int i=0; i<jArray.length(); i++){
				listJson.add(jArray.getJSONObject(i));
			}
		}catch(Exception e){
			Log.e(DatabaseHelper.class.getSimpleName(), e.getMessage(), e);
		}
		
		return listJson;
	}
	
	public static void setAsyn(final List<ROI> listRoi, final String key){
		Thread th = new Thread(){
			public void run() {
				try{
					if(listRoi != null && listRoi.size() > 0){
						JSONArray jROIs = new JSONArray();
						
						for(ROI roi: listRoi){
							jROIs.put(roi.toJsonObject());
						}
						
						set(key, jROIs.toString());
					}
				}catch(Exception e){
					
				}
			};
		};
		th.start();
	}
}
