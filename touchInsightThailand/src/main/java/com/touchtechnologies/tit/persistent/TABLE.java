package com.touchtechnologies.tit.persistent;

enum TABLE {
	ROI("roi"),
	POI("poi"),
	CCTV("cctv"),
	CATEGORY("category");
	
	private String name;
	
	TABLE(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	/**
	 * Get a sql use to create table
	 * @return sql statement
	 */
	public String getCreateSQL(){
		switch(this){
		case ROI:
			return "";
		case POI:
			return "";
		}
		
		return null;
	}
}
