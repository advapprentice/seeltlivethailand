package com.touchtechnologies.tit;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;


import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.util.UserUtil;

import java.util.HashMap;
import java.util.Map;

public class Blogawebctivity extends SettingActionbarActivity {
	private WebView webView;
	private ProgressBar progress;
	private Map<String, String> extraHeaders;
	User user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_webview);
		user = UserUtil.getUser(this);
		mTitle = "Blogs";
		WebViewClient wc = new WebViewClient(){
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				extraHeaders = new HashMap<String, String>();
				if(user!= null) {
					extraHeaders.put("x-tit-access-token", user.getToken());
				}else {
					extraHeaders.put("x-tit-access-token", "");
				}
				extraHeaders.put("x-tit-web-view","Android");
				view.loadUrl(url, extraHeaders);
				return false;
			}
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				progress =(ProgressBar)findViewById(R.id.progressBarWebLoading);
				progress.setVisibility(View.GONE);
			}
			
		};
		String url = getIntent().getExtras().getString(Blogawebctivity.class.getName(),"http://seeitlivethailand.com/blogs");
		extraHeaders = new HashMap<String, String>();
		extraHeaders.put("x-tit-access-token",user.getToken());
		extraHeaders.put("x-tit-web-view","Android");
		webView = (WebView)findViewById(R.id.webView);
		webView.setWebViewClient(wc);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.loadUrl(url,extraHeaders);



	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			switch (keyCode) {
				case KeyEvent.KEYCODE_BACK:
					if (webView.canGoBack()) {
						webView.goBack();
					} else {
						finish();
					}
					return true;
			}

		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}




}
