package com.touchtechnologies.tit;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class Termofservice extends SettingActionbarActivity {
	private WebView webView;
	private ProgressBar progress;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_webview);
		//setTitle("TermsOfService");
		mTitle ="TermsOfService";
		WebViewClient wc = new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				progress = (ProgressBar) findViewById(R.id.progressBarWebLoading);
				progress.setVisibility(View.GONE);
			}

		};
		String url = "http://www.seeitlivethailand.com/termsofservice";
		webView = (WebView) findViewById(R.id.webView);
		webView.setWebViewClient(wc);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.loadUrl(url);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}
