package com.touchtechnologies.tit;

public interface AppConfig {
	String LOG = "TIT";
	String PREFERENCE_POSTINSTALL = "pref_installed";
	String PREFERENCE_WIFI_ACCOUNT = "pref_wifi_acc";
	String PREFERENCE_CHECKIN = "pref_checkin";
	String PREFERENCE_POPUP = "pref_popup";
	String PREFERENCE_RESOLUTION = "pref_resolution";
	String PREFERENCE_FRAMERATE = "pref_framerate";
	String PREFERENCE_LIVETIME = "pref_livetime";
	String PREFERENCE_LOCATION = "pref_location";

	
	int REQUEST_CODE_REGISTER = 4;
	int REQUEST_CODE_EDIT_LIVESTREAM  = 7;
	String PREFERENCE_USER = "tit_preference_user";
	boolean check_update = false;
	/**
	 * The fragment argument representing the section title for this
	 * fragment.
	 */
	String ARG_SECTION_TITLE = "section_title";
}
