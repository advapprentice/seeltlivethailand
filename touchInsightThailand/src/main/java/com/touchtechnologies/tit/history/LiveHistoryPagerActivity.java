package com.touchtechnologies.tit.history;

import java.util.List;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.opensource.pagerindicator.CirclePageIndicator;
import com.touchtechnologies.command.insightthailand.RestServiceHistoryListCommand;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;


public class LiveHistoryPagerActivity extends Fragment{

	private List<StreamHistory> hits;
	private StreamHistory cctv;
	private LiveHistoryPagerAdapter adapter;
	private View viewnotlive;
	private ProgressBar pgloading;
	Context context;
	ViewPager pager;
	CirclePageIndicator circlepager;
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_history, container, false);
		
		adapter = new LiveHistoryPagerAdapter(getActivity());
	
		pager = (ViewPager)view.findViewById(R.id.viewpager);
		
		//	pager.setPageTransformer(true,new FullImagePagertransformer2());
		pager.setAdapter(adapter);
		pager.setCurrentItem(0);
		pager.setClipToPadding(false);
		User user = UserUtil.getUser(getActivity());
		
		viewnotlive =(View) view.findViewById(R.id.viewnotlive);
		viewnotlive.setVisibility(View.GONE);
		
		pgloading = (ProgressBar)view.findViewById(R.id.progressBar1);
		pgloading.setVisibility(View.VISIBLE);
		
		Point point = new Point();
	  	getActivity().getWindowManager().getDefaultDisplay().getSize(point);
	  	pager.getLayoutParams().height = (int)((point.x * 2)/3);
		pager.setOffscreenPageLimit(1);
	  	String url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_rest_histories);
		
		new RestServiceHistoryListCommand(getActivity(),user, url){
	
			protected void onPostExecute(java.util.List<StreamHistory> result) {
				hits = result;
				
				
				if (hits.size()==0) {
					viewnotlive.setVisibility(View.VISIBLE);
				} else {
					viewnotlive.setVisibility(View.GONE);
				}
				
				adapter.setData(hits);
				adapter.notifyDataSetChanged();
				pgloading.setVisibility(View.GONE);
			}
		}.execute();
	
		
		return view;
	
	}
	
	/**
	 * Start Hotel list activity by passing a clicked ROI object
	 */
	
}


