package com.touchtechnologies.tit.history;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.opensource.pagerindicator.CircleBitmapDisplayer;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.live.liststreaming.LiveOnAirPlayerActivity;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import io.socket.client.Ack;

/**
 * Created by TOUCH on 21/4/2559.
 */
public class LiveHistoryDetailListAdapter extends BaseAdapter {

    private List<StreamHistory>  streamHistoryArrayList = new ArrayList<>();
    private Context mContext;
    protected ImageLoader imageLoader;
    protected DisplayImageOptions options,optionsProfile;
    private View.OnClickListener listener;

    public LiveHistoryDetailListAdapter(Context mContext, View.OnClickListener listener) {
       // this.streamHistoryArrayList = streamHistories;
        this.mContext = mContext;
        this.listener = listener;
        imageLoader = ImageLoader.getInstance();


        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .showImageForEmptyUri(R.drawable.bg_seeitlive)
                .showImageOnFail(R.drawable.bg_seeitlive)
                .showImageOnLoading(R.drawable.bg_seeitlive)
                .cacheOnDisc(true)
                .cacheInMemory(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new FadeInBitmapDisplayer(400))
                .build();
        optionsProfile = new DisplayImageOptions.Builder().cacheInMemory(true)
                .showImageForEmptyUri(R.drawable.ic_new_imgaccount)
                .showImageOnFail(R.drawable.ic_new_imgaccount)
                .cacheOnDisc(true)
                .cacheInMemory(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new FadeInBitmapDisplayer(400))
                .displayer(new CircleBitmapDisplayer())
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mContext)
                .denyCacheImageMultipleSizesInMemory()
                .threadPoolSize(3)
                .threadPriority(Thread.NORM_PRIORITY - 2) // default
                .tasksProcessingOrder(QueueProcessingType.FIFO) // default
                .denyCacheImageMultipleSizesInMemory()
                .imageDownloader(new BaseImageDownloader(mContext)) // default
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                .writeDebugLogs()
                .build();

        imageLoader.init(config);
        imageLoader = ImageLoader.getInstance();





    }
    public void setData(List<StreamHistory> arrcctv){
        this.streamHistoryArrayList= arrcctv;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return streamHistoryArrayList.size();
    }

    @Override
    public StreamHistory getItem(int position) {
        return streamHistoryArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_video_history_cat_view, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.imageView_picpro = (ImageView) convertView.findViewById(R.id.imageView_userimage);
            viewHolder.imageView_vid = (ImageView) convertView.findViewById(R.id.imageView_vid_image);
            viewHolder.textView_lovecount = (TextView) convertView.findViewById(R.id.textView_love_count);
            viewHolder.textView_namecat = (TextView) convertView.findViewById(R.id.textView_catname);
            viewHolder.textView_time = (TextView) convertView.findViewById(R.id.textView_vid_time);
            viewHolder.textView_title = (TextView) convertView.findViewById(R.id.textView_cat_his_title);
            viewHolder.viewSelectVideo = (View) convertView.findViewById(R.id.viewSelectVideo);
            convertView.setTag(viewHolder);


        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

        StreamHistory item = getItem(position);
        viewHolder.textView_title.setText(item.getTitle());
        viewHolder.textView_namecat.setText(item.getCategoryName());
        viewHolder.textView_lovecount.setText(item.getLove()+"");
        imageLoader.displayImage(item.getSnapshots(),viewHolder.imageView_vid,options);
        imageLoader.displayImage(item.getUser().getAvatar(),viewHolder.imageView_picpro,optionsProfile);
        int[] time = splitToComponentTimes(item.getTime_duration());
        String h = time[0] < 10 ? "0"+time[0]: time[0]+"";
        String m = time[1] < 10 ? "0"+time[1]: time[1]+"";
        String s = time[2] < 10 ? "0"+time[2]: time[2]+"";
        viewHolder.textView_time.setText(h+":"+m+":"+s);
        viewHolder.imageView_vid.setTag(getItem(position));
        viewHolder.imageView_vid.setOnClickListener(listener);
        viewHolder.viewSelectVideo.setTag(getItem(position));
        viewHolder.viewSelectVideo.setOnClickListener(listener);


        viewHolder.imageView_picpro.setTag(getItem(position).getUser());
        viewHolder.imageView_picpro.setOnClickListener(listener);




        return convertView;
    }

    class ViewHolder{
        ImageView imageView_vid;
        ImageView imageView_picpro;
        TextView  textView_lovecount;
        TextView  textView_title;
        TextView  textView_namecat;
        TextView  textView_time;
        View  viewSelectVideo;
    }

    public  int[] splitToComponentTimes(long biggy) {
        long longVal = biggy;
        int secs;
        int mins =0;
        int remainder;
        int hours=0;
        if(longVal >= 3600 ) {
             hours = (int) longVal / 3600;
             remainder = (int) longVal - hours * 3600;
             mins = remainder / 60;
             remainder = remainder - mins * 60;
             secs = remainder;
        }else if(longVal < 3600 && longVal > 59){
            mins = (int) longVal / 60;
            remainder = (int) (longVal - mins * 60);
            secs = remainder;
        }else {
            secs = (int) longVal;
        }

        int[] ints = {hours , mins , secs};
        return ints;
    }


}
