package com.touchtechnologies.tit.history;

import android.content.Context;
import android.view.View;

/**
 * Created by TouchICS on 5/6/2016.
 */
public class LiveNearbyAdapter extends LiveHistoryDetailListAdapter {
    private View.OnClickListener listener;
    public LiveNearbyAdapter(Context mContext, View.OnClickListener listener) {
        super(mContext, listener);
        this.listener = listener;
    }
}
