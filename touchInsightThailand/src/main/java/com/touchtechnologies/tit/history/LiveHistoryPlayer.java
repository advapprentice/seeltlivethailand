package com.touchtechnologies.tit.history;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IntRange;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.devbrackets.android.exomedia.listener.OnBufferUpdateListener;
import com.devbrackets.android.exomedia.listener.OnCompletionListener;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.animation.TopViewHideShowAnimation;
import com.devbrackets.android.exomedia.ui.widget.EMVideoView;
import com.devbrackets.android.exomedia.ui.widget.VideoControls;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.opensource.pagerindicator.CircleBitmapDisplayer;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.LoveCommand;
import com.touchtechnologies.command.insightthailand.LoveUndoCommand;
import com.touchtechnologies.command.insightthailand.RestServiceHistoryListCommand;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.insightthailand.CategoryLiveStream;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.comment.LiveHistoryComment;
import com.touchtechnologies.tit.dataobject.Category_video;
import com.touchtechnologies.tit.dataobject.Datasocket;
import com.touchtechnologies.tit.live.liststreaming.LiveOnAirPlayerActivity;
import com.touchtechnologies.tit.live.mylivestream.MyliveGridviewActivity;
import com.touchtechnologies.tit.login.LoginsActivity;
import com.touchtechnologies.tit.member.MemberProfileActivity;
import com.touchtechnologies.tit.report.ReportActivity;
import com.touchtechnologies.tit.search.Searchactivity;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.util.DataCacheUtill;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class LiveHistoryPlayer extends FragmentActivity implements OnClickListener{
    private static final int SCREEN_ORIENTATION_LANDSCAPE = 90;
    private static final String KEY_RESOURCES = "key_resources";
    private static ProgressDialog progressDialog;
    private StreamHistory history;
    private boolean video_end;
    private LiveHistoryDetailListAdapter adapter;
    private List<StreamHistory> historys;
    private TextView titlevideo,detaillive,reporter,load,
            currentView,histname,histcatname,name_cat_list,count_fllower,lovecount,commentcount;
    private ListView his_cat_list;
    private ImageView imageView_map,imgReport,image_replay;
    private ImageLoader imageLoader;
    private TextView location_name;
    private final String TAG = LiveHistoryPlayer.class.getSimpleName();
    private GoogleMap googlemap;
    View header;
    int position;
    int increment;
    Socket mSocket;
    private Boolean play_video = false,  flag;
    ArrayList<LiveStreamingChannel> arrlive;
    ProgressDialog progDailog;
    DisplayImageOptions options;
    ImageView imageViewAvatar,refresh,icon_cat;
    VideoView vid;
    ProgressBar progressBar ;

    Animation animBlink;
    DisplayMetrics dm;
    SurfaceView sur_View;
    MediaController media_Controller;
    String cityName;
    EMVideoView mEmVideoView;
    View viewback,viewDetail,viewMore,viewTitle,viewReporter,viewByRePort,viewLove,viewUnLove;
    ImageView imageReporter;
    Button playvideo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //	this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_live_videoplay);

        Setting_imageloader();
        flag = getIntent().getExtras().getBoolean(Searchactivity.class.getName().toString(),true);
        history = (StreamHistory) getIntent().getSerializableExtra(StreamHistory.class.getName());
        LayoutInflater inflater = getLayoutInflater();
        his_cat_list = (ListView) findViewById(R.id.listView_his);
        header = inflater.inflate(R.layout.new_header_his,null, false);
        his_cat_list.addHeaderView(header, null, false);
        position = getIntent().getExtras().getInt("position");


        //Get_video();

    }

    @Override
    protected void onStart() {
        super.onStart();
        mSocket = MainApplication.getmSocket();
        if(mSocket.connected()) {
            mSocket.emit(MainApplication.mJoinroom,"streaming/" + history.getId(), new Ack() {
                @Override
                public void call(final Object... args) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(LiveOnAirPlayerActivity.class.getSimpleName(), "call: " + args[0].toString());
                        }
                    });

                }
            });
            mSocket.on("watchedcount:update", mListener);
            mSocket.on("lovescount:update", mListenerlove);
            mSocket.emit("check", "/websocket#" + mSocket.id(), new Ack() {
                @Override
                public void call(Object... args) {
                    Log.d(TAG, "call: "+args[0].toString());
                }
            });
        }else {
            mSocket.connect();
            mSocket.on("ack-connected", onAckConnected);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        final MediaController mediaController = new MediaController(this);
        final ProgressDialog pDialog = new ProgressDialog(this);
        setView();

        //	MediaPlayer md =new MediaPlayer();

        //mediaController.setAnchorView(vid);
        final Uri video = Uri.parse(history.getHttp());
        //	Uri video = Uri.parse("rtsp://203.170.193.73:1935/vod/sample.mp4");
        mEmVideoView.setVideoURI(video);
        try {
            commentcount.setText(history.getCommentcount());
            lovecount.setText(history.getLove()+"");
            titlevideo.setText(history.getTitle());
            reporter.setText(history.getUser().getFirstName() + " " + history.getUser().getLastName());
            currentView.setText(history.getWatchedCount());

            imageLoader.displayImage(history.getUser().getAvatar(), imageReporter, options);
            if(cityName == null){
                location_name.setText("Location : -");
            }else {
                location_name.setText("Location : "+cityName);
            }

            histcatname.setText(history.getCategoryName());
            imageLoader.displayImage(history.getIcon_category(),icon_cat);
            name_cat_list.setText(history.getCategoryName());
            count_fllower.setText("followers : "+history.getUser().getFollower());
            if (history.getNote().length() < 1) {
                detaillive.setText(getText(R.string.not_detail));
            } else {
                detaillive.setText(history.getNote() == null ? "" : history.getNote());
            }
        } catch (Exception e) {

        }

        if (history.isLove()) {
            viewUnLove.setVisibility(View.VISIBLE);
            viewLove.setVisibility(View.GONE);
        } else {
            viewLove.setVisibility(View.VISIBLE);
            viewUnLove.setVisibility(View.GONE);
        }
        mEmVideoView.setOnPreparedListener(new OnPreparedListener() {
            @Override
            public void onPrepared() {
                if (image_replay.getVisibility() == View.VISIBLE)
                    image_replay.setVisibility(View.GONE);
                mEmVideoView.start();
            }
        });
        mEmVideoView.setOnBufferUpdateListener(new OnBufferUpdateListener() {
            @Override
            public void onBufferingUpdate(@IntRange(from = 0L, to = 100L) int percent) {

            }
        });
        mEmVideoView.getVideoControls().setTitlebar(findViewById(R.id.Title_view));
        mEmVideoView.setOnCompletionListener(new OnCompletionListener() {
            @Override
            public void onCompletion() {
                image_replay.setVisibility(View.VISIBLE);
                image_replay.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mEmVideoView.restart();
                        if (image_replay.getVisibility() == View.VISIBLE)
                            image_replay.setVisibility(View.GONE);
                    }
                });
            }
        });
        if (history.getLat() != null){
            String url = "https://maps.googleapis.com/maps/api/staticmap?center="
                    + history.getLat() + "," + history.getLng() +
                    "&zoom=13&size=800x200&markers=color:red%7Clabel:S%7C"
                    + history.getLat() + "," + history.getLng() +
                    "&key=AIzaSyBcIC9hYqAnvEq0U_KnIM4oDxftLCcV2OY";
            imageLoader.displayImage(url, imageView_map);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share, menu);
        //	super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }else if (item.getItemId() == R.id.menushare) {
            try{
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, history.getUrls());
                startActivity(Intent.createChooser(intent, "Share whit..."));
                Log.d("url =", history.getUrls());

            }catch(Exception e){
                Log.e("error", "share a link error", e);
            }
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onClick(View v) {

        User user =  history.getUser();
        User userLocal = UserUtil.getUser(getApplicationContext());
        Intent intent;
        switch (v.getId()) {
            case R.id.imgReport:
                intent = new Intent(LiveHistoryPlayer.this, ReportActivity.class);
                intent.putExtra(StreamHistory.class.getName(), history);
                intent.putExtra(KEY_RESOURCES,"streamings");
                startActivity(intent);

                break;

            case R.id.btnLiveAround:
                intent = new Intent(this, LiveNearbyActivity.class);
                intent.putExtra(StreamHistory.class.getName(), history);
                startActivity(intent);

                break;

            case R.id.Viewback:
                finish();
                break;
            case R.id.view_detail:
                viewMore.setVisibility(View.VISIBLE);
                viewDetail.setVisibility(View.GONE);
                break;
            case R.id.imageView_comment_his:
                intent = new Intent(LiveHistoryPlayer.this, LiveHistoryComment.class);
                intent.putExtra(StreamHistory.class.getName(), history);
                startActivity(intent);

//			viewDetail.setVisibility(View.VISIBLE);
//			viewMore.setVisibility(View.GONE);
                break;
            case R.id.play_history:
                play_video = true;
                flag = true;
                playvideo.setVisibility(View.GONE);
                vid.start();
                onResume();
                break;
            case R.id.imageview_ic_share:
                try{
                    intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT,history.getWeburl());
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    Log.d("url =", history.getWeburl());

                }catch(Exception e){
                    Log.e("error", "share a link error", e);
                }
                break;
            case R.id.view_byreport:
                String idUser = userLocal.getId();

                if(user.getId().equals(idUser)){
                    if(UserUtil.isLoggedIn(getApplicationContext())) {
                        intent = new Intent(getApplicationContext(), MyliveGridviewActivity.class);
                        startActivity(intent);
                    }else{
                        intent = new Intent(getApplicationContext(), LoginsActivity.class);
                        startActivity(intent);
                    }
                }else{

                    intent = new Intent(getApplicationContext(), MemberProfileActivity.class);
                    intent.putExtra(User.class.getName(),user);
                    startActivity(intent);
                }

                break;

            case R.id.viewLove:
                viewUnLove.setVisibility(View.VISIBLE);
                viewLove.setVisibility(View.GONE);
                if(UserUtil.isLoggedIn(this)) {
                    isLove();
                }else{
                    intent = new Intent(this, LoginsActivity.class);
                    startActivity(intent);
                }


                break;
            case R.id.viewUnLove:
                viewUnLove.setVisibility(View.GONE);
                viewLove.setVisibility(View.VISIBLE);
                if(UserUtil.isLoggedIn(this)) {
                    isUnLove();
                }else{
                    intent = new Intent(this, LoginsActivity.class);
                    startActivity(intent);
                }

                break;




        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            viewReporter.setVisibility(View.GONE);
            viewTitle.setVisibility(View.GONE);
            his_cat_list.setVisibility(View.GONE);
            imageView_map.setVisibility(View.GONE);
            findViewById(R.id.View_detail).setVisibility(View.GONE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            viewReporter.setVisibility(View.VISIBLE);
            viewTitle.setVisibility(View.VISIBLE);
            his_cat_list.setVisibility(View.VISIBLE);
            imageView_map.setVisibility(View.VISIBLE);
            findViewById(R.id.View_detail).setVisibility(View.VISIBLE);
        }
    }


    public void isLove(){
        LoveCommand command = new LoveCommand(getApplicationContext());
        new Love(this).execute(command);

    }


    public void isUnLove(){

        User user = UserUtil.getUser(getApplicationContext());
        String url = ResourceUtil.getServiceUrl(getApplicationContext())+getApplicationContext().getString(R.string.app_service_love)+history.getId()+"/loves";
        Log.d("Source", "URL :"+url);
        new LoveUndoCommand(getApplicationContext(), user,url) {

            protected void onPostExecute(JSONObject result) {

                try {
                    if(result!=null){
                        history.setLove(0);
                        int status = result.getInt("status");
                        String message = result.getString("message");
                        Log.d("message Response", ":" + message);
                    }else{
                        Toast.makeText(getApplicationContext(), "Fail! No Internet Connection", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.getStackTrace();

                }

            }
        }.execute();

    }



    class Love extends CommonServiceTask {

        public Love(FragmentActivity context) {
            super(context);
        }


        @Override
        protected ServiceResponse doInBackground(Command... params) {

            String url = ResourceUtil.getServiceUrl(context)+context.getString(R.string.app_service_love)+history.getId()+"/loves";
            User user = UserUtil.getUser(context);
            Log.d("Source", "="); Log.d("Source", "URL :"+url);
            ServiceConnector srConnector = new ServiceConnector(url);

            List<NameValuePair> headers = new ArrayList<NameValuePair>();
            headers.add(new BasicNameValuePair("Content-Type", "application/json"));
            headers.add(new BasicNameValuePair("X-TIT-ACCESS-TOKEN", user.getToken()));

            ServiceResponse serviceResponse = srConnector.doAsynPost(params[0], true, headers);
            Log.d("headers", "URL :"+headers);

            try {
                int code = serviceResponse.getCode();
                if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
                    JSONObject objuser = new JSONObject(serviceResponse.getContent().toString());
                    history.setLove(1);
                }

            } catch (Exception e) {

                serviceResponse.setMessage(e.getMessage());
                Log.e(AppConfig.LOG, "doInBackground error", e);
            }

            return serviceResponse;
        }



        @Override
        protected void onPostExecute(ServiceResponse result) {
            super.onPostExecute(result);

            try {
                if (result.getCode() == ServiceResponse.SUCCESS|| result.getCode() == HttpStatus.SC_OK) {
                    context.setResult(Activity.RESULT_OK);

                } else {
                    Toast.makeText(context, "Update Fail"+"!\r\n" + result.getMessage(),Toast.LENGTH_LONG).show();

                }
            } catch (Exception e) {
                Toast.makeText(context, "ERROR!\r\n" + e.getMessage(),Toast.LENGTH_LONG).show();
                Log.e(AppConfig.LOG, "process response error", e);
            }
        }
    }


    private void  setView(){
        Get_video();
        imageReporter = (ImageView) header.findViewById(R.id.imageReporter);

        mEmVideoView = (EMVideoView)findViewById(R.id.video_play_activity_video_view);
        imageView_map = (ImageView) header.findViewById(R.id.imageView_maphis);
        icon_cat = (ImageView) header.findViewById(R.id.imageView_icon_cat);

        lovecount = (TextView)header.findViewById(R.id.textView_lovecount);

        vid = (VideoView) findViewById(R.id.videoPlay);
        titlevideo = (TextView)findViewById(R.id.textViewtitlevideo);
        detaillive = (TextView)findViewById(R.id.detaillive);
        reporter = (TextView)findViewById(R.id.reporter);
        image_replay = (ImageView) findViewById(R.id.imageView34);

        imgReport = (ImageView)findViewById(R.id.imgReport);

        load = (TextView) findViewById(R.id.loading);
        currentView = (TextView) header.findViewById(R.id.currentView);
        location_name = (TextView) header.findViewById(R.id.textView_location_name);
        histcatname = (TextView) findViewById(R.id.textView_category_his);
        name_cat_list = (TextView) findViewById(R.id.textView_name_cat);
        count_fllower = (TextView) header.findViewById(R.id.textView_follow);
        adapter = new LiveHistoryDetailListAdapter(this,listener);
        his_cat_list.setAdapter(adapter);


        //his_cat_list.addHeaderView(findViewById());



        viewback = findViewById(R.id.Viewback);
        viewDetail = header.findViewById(R.id.view_detail);
        viewMore = header.findViewById(R.id.view_more);
        viewTitle = findViewById(R.id.view_title);
        viewReporter = header.findViewById(R.id.view_reporter);
        viewByRePort = findViewById(R.id.view_byreport);
        viewLove  = header.findViewById(R.id.viewLove);
        viewUnLove = header.findViewById(R.id.viewUnLove);

        playvideo = (Button)findViewById(R.id.play_history);

        playvideo.setOnClickListener(this);
        viewDetail.setOnClickListener(this);
        viewMore.setOnClickListener(this);
        viewback.setOnClickListener(this);
        viewByRePort.setOnClickListener(this);
        viewLove.setOnClickListener(this);
        viewUnLove.setOnClickListener(this);
        imgReport.setOnClickListener(this);

        findViewById(R.id.imageview_ic_share).setOnClickListener(this);
        header.findViewById(R.id.imageView_comment_his).setOnClickListener(this);
        commentcount = (TextView) header.findViewById(R.id.textView_commentcount);

        header.findViewById(R.id.btnLiveAround).setOnClickListener(this);

        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        progressBar.setVisibility(View.VISIBLE);


        playvideo.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        if(history.getLat() != null) {
            try {
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> addresses ;
                addresses = geocoder.getFromLocation(history.getLat(), history.getLng(), 1);
                //cityName = addresses.get(0).getAddressLine(0);
                cityName = addresses.get(0).getAdminArea();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            header.findViewById(R.id.view_map_his).setVisibility(View.GONE);
        }
        if (image_replay.getVisibility() == View.VISIBLE)
            image_replay.setVisibility(View.GONE);

    }

    private OnClickListener listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.imageView_vid_image:
                    Intent intent = new Intent(LiveHistoryPlayer.this,LiveHistoryPlayer.class);
                    intent.putExtra(StreamHistory.class.getName(), (StreamHistory)v.getTag());
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    break;
                case R.id.imageView_userimage:

                    break;

            }

        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mEmVideoView.setReleaseOnDetachFromWindow(true);
        mSocket.disconnect();

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");
        mSocket.emit(MainApplication.mleaveroom, "streaming/" + history.getId(), new Ack() {
            @Override
            public void call(Object... args) {
                Log.d(TAG, "call: "+args[0].toString());

            }
        });
        mSocket.off("watchedcount:update",mListener);
        mSocket.off();
        mSocket.disconnect();
        Intent intent = new Intent();
        Bundle bundle =new Bundle();
        bundle.putSerializable(StreamHistory.class.getSimpleName(),history);
        intent.putExtra(StreamHistory.class.getName(),bundle);
        setResult(position,intent);

    }

    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }


    private Emitter.Listener mListener = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Datasocket mDatasocket = new Datasocket(Datasocket.Socket_type.watchscount, args[0].toString());
                    Datasocket.Vid_property mVid_property = (Datasocket.Vid_property) mDatasocket.getdata(Datasocket.Socket_type.watchscount);
                    if (lovecount != null && currentView != null) {
                        currentView.setText(mVid_property.getWatched() + "");
                        history.setWatchedCount(mVid_property.getWatched()+"");
					    //currentView.setText(mVid_property.getWatched());
//                        lovecount.setText(mVid_property.getLove() + "");
                    }
                }
            });

        }
    };

    private Emitter.Listener mListenerlove = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Datasocket mDatasocket = new Datasocket(Datasocket.Socket_type.watchscount, args[0].toString());
                    Datasocket.Vid_property mVid_property = (Datasocket.Vid_property) mDatasocket.getdata(Datasocket.Socket_type.watchscount);
                    if (lovecount != null && currentView != null) {
//                        currentView.setText(mVid_property.getWatched() + "");
//					currentView.setText(mVid_property.getWatched());
                        lovecount.setText(mVid_property.getLove() + "");
                        history.setLove(mVid_property.getLove());
                    }
                }
            });

        }
    };

    private Emitter.Listener onAckConnected = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSocket.emit(MainApplication.mJoinroom, "streaming/" + history.getId(), new Ack() {
                        @Override
                        public void call(final Object... args) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d(LiveOnAirPlayerActivity.class.getSimpleName(), "call: " + args[0].toString());
                                }
                            });

                        }
                    });
                    mSocket.on("watchedcount:update", mListener);
                    mSocket.on("lovescount:update", mListenerlove);
                    mSocket.emit("check", "/websocket#" + mSocket.id(), new Ack() {
                        @Override
                        public void call(Object... args) {
                            Log.d(TAG, "call: " + args[0].toString());
                        }
                    });

                }
            });
        }
    };


    private void Get_video(){
        String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_histories) +
                "?filters[stream_media][category_id][operator]==&filters[stream_media][category_id][value]=" +
                history.getCategory_id() +"&filtersPage=1&filterLimit=10";
        User user = UserUtil.getUser(this);
        new RestServiceHistoryListCommand(this,user, url){

            protected void onPostExecute(java.util.List<StreamHistory> result) {
                historys = result;
                adapter.setData(historys);
                //	pager.setCurrentItem(1);
                adapter.notifyDataSetChanged();
                setListViewHeightBasedOnItems(his_cat_list);
//				rotateLoading.setVisibility(View.GONE);

            }
        }.execute();
    }

    private void Setting_imageloader(){
        imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPoolSize(3)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .defaultDisplayImageOptions(options).build();

        imageLoader.init(config);
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .imageScaleType(ImageScaleType.NONE)
                .showImageForEmptyUri(R.drawable.ic_new_imgaccount)
                .showImageOnFail(R.drawable.ic_new_imgaccount)
                .cacheOnDisc(true)
                .cacheInMemory(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new FadeInBitmapDisplayer(1000))
                .displayer(new CircleBitmapDisplayer())
                .build();
    }




}
