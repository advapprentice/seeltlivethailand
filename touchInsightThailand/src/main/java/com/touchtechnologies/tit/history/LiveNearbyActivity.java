package com.touchtechnologies.tit.history;

import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.opensource.pagerindicator.CircleBitmapDisplayer;
import com.touchtechnologies.command.insightthailand.RestServiceLiveNearbyListCommand;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.live.mylivestream.MylivePlayer;
import com.touchtechnologies.tit.util.ResourceUtil;

import java.util.HashMap;
import java.util.List;

/**
 * Created by TouchICS on 5/6/2016.
 */
public class LiveNearbyActivity extends SettingActionbarActivity implements GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener, OnMapReadyCallback {
    protected ImageLoader imageLoader;
    protected DisplayImageOptions options,optionsProfile;
    private List<StreamHistory> arrHisstory;
    private  StreamHistory history;
    private GoogleMap googleMap,googleMap2;
    private int mapZoomLevel = 18;
    private Marker iconadd;
    private HashMap<Marker, StreamHistory> hMarkers;
    private LiveNearbyInfoMapAdapter adapterInfo;
    private LiveNearbyAdapter adapter;
    private ListView listLiveNaerby;
    private TextView videoCount, txtTitle, txtHisCat,txtTime;
    private GoogleApiClient client;
    protected Location location;
    private ImageView imgSnapshot,imgUser;
    private View viewHeader,loading;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_around);

        imageLoader = ImageLoader.getInstance();


        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .showImageForEmptyUri(R.drawable.bg_seeitlive)
                .showImageOnFail(R.drawable.bg_seeitlive)
                .showImageOnLoading(R.drawable.bg_seeitlive)
                .cacheOnDisc(true)
                .cacheInMemory(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new FadeInBitmapDisplayer(400))
                .build();
        optionsProfile = new DisplayImageOptions.Builder().cacheInMemory(true)
                .showImageForEmptyUri(R.drawable.ic_new_imgaccount)
                .showImageOnFail(R.drawable.ic_new_imgaccount)
                .cacheOnDisc(true)
                .cacheInMemory(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new FadeInBitmapDisplayer(400))
                .displayer(new CircleBitmapDisplayer())
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .denyCacheImageMultipleSizesInMemory()
                .threadPoolSize(3)
                .threadPriority(Thread.NORM_PRIORITY - 2) // default
                .tasksProcessingOrder(QueueProcessingType.FIFO) // default
                .denyCacheImageMultipleSizesInMemory()
                .imageDownloader(new BaseImageDownloader(this)) // default
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                .writeDebugLogs()
                .build();

        imageLoader.init(config);
        imageLoader = ImageLoader.getInstance();


        mTitle = "Live Around";
        hMarkers = new HashMap<>();
        listLiveNaerby = (ListView) findViewById(R.id.list);
        adapter  = new LiveNearbyAdapter(this,listener);


        //  mTitle = "Live Around";
        history = (StreamHistory) getIntent().getSerializableExtra(StreamHistory.class.getName());

        adapterInfo = new LiveNearbyInfoMapAdapter(this);
        adapterInfo.setMarker(hMarkers);

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup headerView = (ViewGroup)inflater.inflate(R.layout.header_live_nearby, listLiveNaerby, false);

        viewHeader =  (View)headerView. findViewById(R.id.view_header);
        txtTitle =  (TextView)headerView. findViewById(R.id.txtTitle);
        txtHisCat =  (TextView)headerView. findViewById(R.id.txtCatName);
        txtTime = (TextView)headerView. findViewById(R.id.txtTime);
        imgSnapshot = (ImageView)headerView. findViewById(R.id.imgSnapShot) ;
        imgUser = (ImageView)headerView. findViewById(R.id.imgUser) ;
        videoCount = (TextView)headerView.findViewById(R.id.txtCountVideo);

        viewHeader.setVisibility(View.GONE);

        listLiveNaerby.addHeaderView(headerView, null, false);

        listLiveNaerby.setAdapter(adapter);

        loading = (View) findViewById(R.id.viewLoading);


        Log.i("LatLng",":"+history.getLat()+"/"+history.getLng());

        getVideoSelect(history);
        listLiveNearby();
    }

    @Override
    protected void onResume() {
        super.onResume();

        SupportMapFragment MapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        MapFragment.getMapAsync(this);
                googleMap.setMyLocationEnabled(false);
                googleMap.setOnInfoWindowClickListener(this);
                googleMap.setOnMarkerClickListener(this);
                googleMap.setInfoWindowAdapter(adapterInfo);


    }

    public void listLiveNearby(){

        String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_live_nearby)+"?at="+history.getLat()+","
               +history.getLng()+"&distance=5";
        Log.i("Url",":"+url);
        new RestServiceLiveNearbyListCommand(this,url){

            protected void onPostExecute(java.util.List<StreamHistory> result) {

                if(result != null){
                    loading.setVisibility(View.GONE);
                    viewHeader.setVisibility(View.VISIBLE);
                }

                arrHisstory = result;
                videoCount.setText(""+arrHisstory.size());

                adapter.setData(arrHisstory);
                adapter.notifyDataSetChanged();

                onUpdatemap(arrHisstory,history);

            }
        }.execute();
    }



    void onUpdatemap(List<StreamHistory> list ,StreamHistory hisFocus ) {

        if (googleMap == null) {
            return ;
        }

        try {
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            for (StreamHistory liveHis : list) {
                if(!hisFocus.equals(liveHis)) {
                    Marker tp = googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(liveHis.getLat(), liveHis.getLng()))
                            .title(liveHis.getTitle())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_live_nearby)));

                    hMarkers.put(tp, liveHis);
                }
                Log.i("Live",": " + liveHis.getTitle() + " " + liveHis.getLat()+ "/" + liveHis.getLng());
            }


            Marker tp = googleMap.addMarker(new MarkerOptions().position(
                    new LatLng(hisFocus.getLat(), hisFocus.getLng()))
                    .title(hisFocus.getTitle())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_live_nearby_red)));
            hMarkers.put(tp, hisFocus);
            getVideoSelect(hisFocus);
            tp.showInfoWindow();



            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(hisFocus.getLat(), hisFocus.getLng()),mapZoomLevel));

        } catch (Exception e) {
            Log.e("map", "Map marker " + e.getMessage());
        }
    }


    public void setLocation(Location location) {
        this.location = location;

        if (googleMap != null) {
            // move map to last know location if possible when started
            LatLng myLastLatLon = new LatLng(location.getLatitude(),
                    location.getLongitude());
            CameraUpdate camUpdate = CameraUpdateFactory.newLatLngZoom(
                    myLastLatLon, mapZoomLevel);

            googleMap.animateCamera(camUpdate);
        }
    }

    public void onLocationChanged(Location location) {
        this.location = location;

        if(client.isConnected()){
            client.disconnect();
        }

    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

          StreamHistory  history = (StreamHistory)v.getTag();

            switch (v.getId()){
                case R.id.viewSelectVideo:
                    if(googleMap != null) {
                        googleMap.clear();
                        googleMap.animateCamera(CameraUpdateFactory
                                .newLatLngZoom(new LatLng(history.getLat(),
                                        history.getLng()), mapZoomLevel));
                    }
                    onUpdatemap(arrHisstory,history);
                    getVideoSelect(history);

                    /*
                    Intent intent = new Intent(LiveHistoryPlayer.this,LiveHistoryPlayer.class);
                    intent.putExtra(StreamHistory.class.getName(), (StreamHistory)v.getTag());
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    */
                    break;
                case R.id.imageView_vid_image:
                    if(googleMap != null) {
                        googleMap.clear();
                        googleMap.animateCamera(CameraUpdateFactory
                                .newLatLngZoom(new LatLng(history.getLat(),
                                        history.getLng()), mapZoomLevel));
                    }
                    onUpdatemap(arrHisstory,history);
                    getVideoSelect(history);



                    break;

                case R.id.imageView_userimage:

                    break;

            }

        }
    };

    public void getVideoSelect(StreamHistory hisSelect){

        final StreamHistory his = hisSelect;

        txtTitle.setText(his.getTitle());
        txtHisCat.setText(his.getCategoryName());
        int[] time = splitToComponentTimes(his.getTime_duration());
        String h = time[0] < 10 ? "0"+time[0]: time[0]+"";
        String m = time[1] < 10 ? "0"+time[1]: time[1]+"";
        String s = time[2] < 10 ? "0"+time[2]: time[2]+"";
        txtTime.setText(h+":"+m+":"+s);

        imageLoader.displayImage(his.getSnapshots(),imgSnapshot,options);
        imageLoader.displayImage(his.getUser().getAvatar(),imgUser,optionsProfile);

        imgSnapshot.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LiveNearbyActivity.this,MylivePlayer.class);
                intent.putExtra(StreamHistory.class.getName(),his);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

    }




    @Override
    public void onInfoWindowClick(Marker marker) {
        StreamHistory hisSelect = hMarkers.get(marker);
        Intent intent = new Intent(this,MylivePlayer.class);
        intent.putExtra(StreamHistory.class.getName(), hisSelect);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);


    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        StreamHistory his = hMarkers.get(marker);
        googleMap.clear();
        onUpdatemap(arrHisstory,his);

        getVideoSelect(his);



        return false;
    }

    public  int[] splitToComponentTimes(long biggy) {
        long longVal = biggy;
        int secs;
        int mins =0;
        int remainder;
        int hours=0;
        if(longVal >= 3600 ) {
            hours = (int) longVal / 3600;
            remainder = (int) longVal - hours * 3600;
            mins = remainder / 60;
            remainder = remainder - mins * 60;
            secs = remainder;
        }else if(longVal < 3600 && longVal > 59){
            mins = (int) longVal / 60;
            remainder = (int) (longVal - mins * 60);
            secs = remainder;
        }else {
            secs = (int) longVal;
        }

        int[] ints = {hours , mins , secs};
        return ints;
    }


    @Override
    public void onMapReady(GoogleMap googleMapv) {
        googleMap = googleMapv;
    }
}
