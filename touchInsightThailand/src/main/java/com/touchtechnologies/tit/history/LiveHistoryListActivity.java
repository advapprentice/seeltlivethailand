package com.touchtechnologies.tit.history;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

import com.touchtechnologies.command.insightthailand.RestServiceHistoryListCommand;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;
@SuppressLint("ValidFragment")
public class LiveHistoryListActivity extends ListFragment implements OnScrollListener, OnRefreshListener {
	
	private boolean loading = false;
	protected int action_refresh;
	private Handler handler = new Handler();
	private SwipeRefreshLayout mSwipeRefreshLayout;
	private int visibleThreshold = 2;
    private int previousTotal = 0;
    private Runnable runnable;
    LiveHistoryListAdapter adapter;
	List<StreamHistory> hisstory;
	ListView hisView;
	View viewLoading, viewEmptyHint;
	User feedLongClicked;
	View listViewContainer;
    Context context; 
    StreamHistory hiss;
    public LiveHistoryListActivity(StreamHistory his) {
	this.hiss = his;
    }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View listViewContainer = inflater.inflate(R.layout.fragment_mobilelive_list_history, container, false);
	
		mSwipeRefreshLayout = (SwipeRefreshLayout) listViewContainer.findViewById(R.id.container);
		mSwipeRefreshLayout.setColorScheme(R.color.swipe_gray, R.color.swipe_green,R.color.swipe_gray, R.color.swipe_green);  
		mSwipeRefreshLayout.setOnRefreshListener(this);
	//	mSwipeRefreshLayout.setVisibility(View.GONE);
		
        viewLoading = listViewContainer.findViewById(android.R.id.empty);
		viewEmptyHint = listViewContainer.findViewById(R.id.emptyAgentHint);
		hisView = (ListView) listViewContainer.findViewById(android.R.id.list);
		hisView.setOnScrollListener(this);
		viewEmptyHint.setVisibility(View.GONE);
		
		
	  	
		adapter = new LiveHistoryListAdapter(getActivity());
		setListAdapter(adapter);
		onRefresh();
	

		return listViewContainer;
	}
	
	
	public void onRefresh(int action){
		action_refresh = action;
		User user = UserUtil.getUser(getActivity());
		
		if (!loading) {
	
			String url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_rest_histories);
			
		
			new RestServiceHistoryListCommand(getActivity(),user, url){
		
				protected void onPostExecute(java.util.List<StreamHistory> result) {
				
					hisstory = result;
					adapter.setData(hisstory);
					adapter.notifyDataSetChanged();
					
					int count = adapter.getCount();
					if(count > 0){
						mSwipeRefreshLayout.setVisibility(View.VISIBLE);
					}else{
						mSwipeRefreshLayout.setVisibility(View.GONE);
						viewLoading.setVisibility(View.GONE);
						viewEmptyHint.setVisibility(View.VISIBLE);
					}
				
				};
			}.execute();
			
		}	
	}
	
	public void onRefresh() {
		onRefresh(MotionEvent.ACTION_DOWN);
		mSwipeRefreshLayout.setRefreshing(false);
	}

	public void onScroll(AbsListView abs, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
	
			if (loading) {
	            if (totalItemCount > previousTotal) {
	                loading = false;
	                previousTotal = totalItemCount;
	            }
	        }
	        if (!loading & totalItemCount > 0 && (totalItemCount + visibleItemCount) <= (firstVisibleItem - visibleThreshold)) {
	            // load the next feeds using a background task,
	        	onRefresh();
	        	
	            loading = true;
	        }
	
			
		}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		
			Intent intent = new Intent(getActivity(), LiveHistoryPlayer.class);
			intent.putExtra(StreamHistory.class.getName(), adapter.getItem(position));
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			
	
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		
	}


}