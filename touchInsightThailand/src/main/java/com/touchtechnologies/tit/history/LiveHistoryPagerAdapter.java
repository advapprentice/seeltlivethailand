package com.touchtechnologies.tit.history;

import java.util.List;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.tit.R;

/**
 * Simple History adapter that create a simple image and title
 * @author Touch
 *
 */
public class LiveHistoryPagerAdapter extends PagerAdapter implements OnClickListener{
	private List<StreamHistory> his;
	private Context context;
	private int resLayout;
	Bitmap bitmap;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	
	public LiveHistoryPagerAdapter(Context context) {
		this.context = context;
		
	
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.NONE)
				.showImageForEmptyUri(R.drawable.bg_cctvdefault)
				.showImageOnFail(R.drawable.bg_cctvdefault)
				.showImageOnLoading(R.drawable.bg_cctvdefault)
				.cacheInMemory(true)
				.displayer(new FadeInBitmapDisplayer(500)) //fade in images
			  	.resetViewBeforeLoading(true).build();
				 imageLoader = ImageLoader.getInstance();  
	}
	public void setData(List<StreamHistory> arrcctv){
		this.his = arrcctv;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return his == null ? 0 : his.size();
	}
  //  @Override
  //  public float getPageWidth(int position) {
  //      return 0.8f;
 //   }
	@Override
	public boolean isViewFromObject(View view, Object object) {
		return (view == object);		
	}
	
	public StreamHistory getItem(int position) {
		return his.get(position);
	}


	public long getItemId(int position) {
		return position;
	}
	
	  
		@Override
		public Object instantiateItem(ViewGroup collection, int position) {
			
//			imageLoader.displayImage(feed.medias.get(position).getThumbURL(),view, options);
			View convertView = LayoutInflater.from(context).inflate(R.layout.item_history_pager, collection, false);
			convertView.setTag(his.get(position));
		
			ImageView imageView = (ImageView)convertView.findViewById(R.id.imageViewMedia);
			TextView textView  = (TextView)convertView.findViewById(R.id.texttitlelive);
			View viewdisplays = (View)convertView.findViewById(R.id.viewVideos);
			View viewimage = (View)convertView.findViewById(R.id.Viewimage);
			Button btnplay = (Button)convertView.findViewById(R.id.btnPlayvideo);
			
	//		Toast.makeText(context, "���ͺ"+getItem(position).getUrl(), Toast.LENGTH_LONG).show();
			textView.setText(getItem(position).getTitle());
			
			viewdisplays.setOnClickListener(this);
			btnplay.setOnClickListener(this);
			viewdisplays.setTag(position);
			btnplay.setTag(position);
			
			imageLoader.displayImage(getItem(position).getSnapshots(), imageView, options);
			
	//		viewimage.setBackgroundColor(Color.argb(180, position * 50, position * 107, position * 181));
			
			collection.addView(convertView,0);
			return convertView;
	}
	
	class ViewHolder{
		ImageView imageView;
		TextView textView;
	}
	
	@Override
	public void destroyItem(View collection, int position, Object view) {
		((ViewPager) collection).removeView((View)view);
		 
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.viewVideos:
   		case R.id.btnPlayvideo:
	
			 try {
				  	Intent intent = new Intent(context, LiveHistoryPlayer.class);
					intent.putExtra(StreamHistory.class.getName(), his.get((Integer)v.getTag()));
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					context.startActivity(intent);
							
			} catch (ActivityNotFoundException activityException) {
			        Log.e("dialing", "Call failed", activityException);
		    }
					 
		       
			break;
		}
	}
}
