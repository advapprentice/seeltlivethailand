package com.touchtechnologies.tit.history;

import java.util.List;

import android.content.Context;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.touchtechnologies.animate.gridview.ChattyListView;
import com.touchtechnologies.animate.progress.RotateLoading;
import com.touchtechnologies.command.insightthailand.RestServiceHistoryListCommand;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.dataobject.reporter.LiveStreamChannel;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.cctvitlive.LiveStreamFragment2;
import com.touchtechnologies.tit.cctvitlive.LiveStreamListener;
import com.touchtechnologies.tit.live.category.ScrollTabHolderFragment;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;
import com.touchtechnologies.wifi.WIFIMod;

public class LiveHistoryGridviewFragment extends ScrollTabHolderFragment implements OnScrollListener, OnRefreshListener, LiveStreamListener{
	private static final String ARG_POSITION = "position";
	private static int FIRST = 0;
	private SwipeRefreshLayout mSwipeRefreshLayout;
	private View viewLoading, viewEmptyHint,viewnotlive;
	private LiveHistoryGridviewAdapter adapter;
	private ListView listView ;
	private String[] fillter_menu_string = {"new","topview"};
	private String fillter = "new";
	private Context context;
	private ImageView fillter_menu;
	private List<StreamHistory> hisstory;
	protected int action_refresh;
	private boolean loading = false;
	private int previousTotal = 0;
	private int visibleThreshold = 5;
	private  View view_fillter;
	private String url;
	public static final String KEY_CATEGORY_TYPE = "type";
	private int mPosition;
	private int liveSize;
	private View placeHolderView,viewDot;
	int page = 0;
	public boolean liveEmpty;
	public static Fragment newInstance(int position) {
		LiveHistoryGridviewFragment fragment = new LiveHistoryGridviewFragment();
		Bundle b = new Bundle();
		b.putInt(ARG_POSITION, position);
		fragment.setArguments(b);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View listViewContainer = inflater.inflate(R.layout.fragment_history_gridview, container, false);
		mPosition = getArguments().getInt(ARG_POSITION);
		liveSize = getArguments().getInt("liveSize",0);

	//	Toast.makeText(getActivity(), "size " + liveSize, Toast.LENGTH_LONG).show();
		MainApplication.getInstance().trackScreenView("LiveStream_History");

		viewEmptyHint = (View)listViewContainer.findViewById(R.id.emptyAgentHint);
		fillter_menu = (ImageView)listViewContainer.findViewById(R.id.filter_video_history);
		view_fillter = (View)listViewContainer.findViewById(R.id.view_videofillter);

		if(!WIFIMod.isNetworkConnected(getActivity())){
			viewEmptyHint.setVisibility(View.VISIBLE);
		}else{
			viewEmptyHint.setVisibility(View.GONE);
		}
		adapter = new LiveHistoryGridviewAdapter(getActivity());


		mSwipeRefreshLayout = (SwipeRefreshLayout) listViewContainer.findViewById(R.id.container);
		mSwipeRefreshLayout.setOnRefreshListener(this);
	//	mSwipeRefreshLayout.setVisibility(View.GONE);

		viewDot = (View)listViewContainer.findViewById(R.id.viewLoading);
		viewDot.setVisibility(View.VISIBLE);


		viewnotlive =(View) listViewContainer.findViewById(R.id.viewnotlive);
		viewnotlive.setVisibility(View.GONE);

		listView = (ListView) listViewContainer.findViewById(R.id.grid_view);


		placeHolderView = (View)inflater.inflate(R.layout.frame_layout, listView, false);
		placeHolderView.setBackgroundColor(0xFFFFFFFF);
		listView.addHeaderView(placeHolderView);

		placeHolderView.setPadding(0, getResources().getDimensionPixelSize(liveSize == 0 ? R.dimen.header_height_frame_null : R.dimen.header_height_frame), 0, 0);
		viewDot.setPadding(0, getResources().getDimensionPixelSize(	liveSize == 0 ? R.dimen.pading_loading_null : R.dimen.pading_loading_not_null), 0, 0);

		listView.setAdapter(adapter);

	//    gridView.setOnItemClickListener(myOnItemClickListener);


		listView.setOnScrollListener(new OnScroll());
		if(LiveStreamFragment2.NEEDS_PROXY){//in my moto phone(android 2.1),setOnScrollListener do not work well
			listView.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (mScrollTabHolder != null)
						mScrollTabHolder.onScroll(listView, 0, 0, 0, mPosition);
					return false;
				}
			});
		}


		adapter.notifyDataSetChanged();
		onRefresh();

		fillter_menu.bringToFront();
		view_fillter.bringToFront();


		return listViewContainer;
	}

	public ListView getGridView() {
		return listView;
	}

	@Override
	public void ready(List<LiveStreamingChannel> list) {
	try {
		placeHolderView.setPadding(0, getResources().getDimensionPixelSize(list.size() == 0 ? R.dimen.header_height_frame_null : R.dimen.header_height_frame), 0, 0);
	}catch (Exception e){
		e.printStackTrace();
	}
	}

    @Override
    public void onResume() {
        super.onResume();
		try{
			Fragment fragment = getParentFragment();
			if(fragment != null){
				fragment.onResume();
				((LiveStreamFragment2)fragment).getlistlivestream();
			}
		}catch (Exception e){
			Log.i("Error",":"+e.getMessage());
		}

           //  fragment.onResume();

    }


	@Override
	public void onStart() {
		super.onStart();
		onRefresh();
	}

	public void onRefresh(int action){

		action_refresh = action;
		User user = UserUtil.getUser(getActivity());
		url = ResourceUtil.getServiceUrl(getActivity())+getResources().getString(R.string.app_service_rest_histories)+"?filterLimit="+10+"&filtersPage="+1;
		Log.i("POST : Url =",""+url);

	//	Toast.makeText(getActivity(), "Refresh:" + url, Toast.LENGTH_SHORT).show();

		if (!loading) {
			loading = true;
			String urlwithfilter;

			if(fillter == fillter_menu_string[0]){
				 urlwithfilter = url;
			}else{
				urlwithfilter = "?filterOrder[watchedCountPeriod]=desc";
			}
			Log.d(LiveHistoryGridviewFragment.class.getSimpleName(), "onRefresh: "+urlwithfilter);
			
			new RestServiceHistoryListCommand(getActivity(),user, urlwithfilter){
		
				protected void onPostExecute(java.util.List<StreamHistory> result) {
					hisstory = result;
					
					if (hisstory.size()==0) {
						viewnotlive.setVisibility(View.VISIBLE);
					} else {
						viewnotlive.setVisibility(View.GONE);
					}
									
					adapter.setData(hisstory);
				//	pager.setCurrentItem(1);
					adapter.notifyDataSetChanged();
					viewDot.setVisibility(View.GONE);

					fillter_menu.bringToFront();
					fillter_menu.setOnClickListener(onClickListener);

					loading = false;
				}
			}.execute();

			page = 1;
		}	
	}
 /*
	OnItemClickListener myOnItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			 try {
				  	Intent intent = new Intent(getActivity(), LiveHistoryPlayer.class);
					intent.putExtra(StreamHistory.class.getName(),adapter.getItem(position));
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);

			} catch (ActivityNotFoundException activityException) {
			        Log.e("dialing", "Call failed", activityException);
		    }

		}
	};

*/

	@Override
	public void onRefresh() {
		onRefresh(MotionEvent.ACTION_DOWN);
		mSwipeRefreshLayout.setRefreshing(false);

	}


	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		
	}

	public void onScroll(AbsListView abs, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//
//		if (loading) {
//            if (totalItemCount > previousTotal) {
//                loading = false;
//                previousTotal = totalItemCount;
//
//            }
//        }
//
//
//		if (!loading & totalItemCount > 0 && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
//			// load the next feeds using a background task,
//			Object creatorContext = getActivity();
//			if(creatorContext instanceof OnRefreshListener){
//				((OnRefreshListener) creatorContext).onRefresh();
//			}
//
//			loading = true;
//		}

		/*
		if (!loading & totalItemCount > 0 && (totalItemCount + visibleItemCount) <= (firstVisibleItem - visibleThreshold)) {
            // load the next feeds using a background task,
			Toast.makeText(getActivity(),"To 5555",Toast.LENGTH_LONG).show();
        	onRefresh();

            loading = true;
        }
		*/
		
	}

	private View.OnClickListener onClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Log.d("eq","eq");
			PopupMenu popupMenu = new PopupMenu(getActivity(),fillter_menu);
			popupMenu.getMenuInflater().inflate(R.menu.fillter_video_popup,popupMenu.getMenu());
			popupMenu.setOnMenuItemClickListener(PopupclickListener);
			popupMenu.show();
		}
	};

	private PopupMenu.OnMenuItemClickListener PopupclickListener = new PopupMenu.OnMenuItemClickListener() {
		@Override
		public boolean onMenuItemClick(MenuItem item) {
			switch (item.getItemId()){
				case R.id.Mostview:
					fillter = fillter_menu_string[1];
					onRefresh();

					break;
				case R.id.newest_video:
					fillter = fillter_menu_string[0];
					onRefresh();
					break;
			}
			return false;
		}
	};

	@Override
	public void adjustScroll(int scrollHeight) {
		if (scrollHeight == 0 && listView.getFirstVisiblePosition() >= 1) {
			return;
		}

		listView.setSelectionFromTop(1, scrollHeight);
	}


	public class OnScroll implements AbsListView.OnScrollListener {

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {

		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

			if (mScrollTabHolder != null) {
				mScrollTabHolder.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount, mPosition);

			}
			if (totalItemCount > previousTotal) {
				//loading = false;
				previousTotal = totalItemCount;
			}
			if(listView.getAdapter().getCount()>2) {
				if (!loading && (listView.getLastVisiblePosition() >= listView.getAdapter().getCount() - visibleThreshold)) {
					loadMore();
				}
			}
		}

	}


	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount, int pagePosition) {

	}


	public void loadMore(){
		page+=1;

		User user = UserUtil.getUser(getActivity());
		url = ResourceUtil.getServiceUrl(getActivity())+getResources().getString(R.string.app_service_rest_histories)+"?filterLimit="+10+"&filtersPage="+page;

		Log.i("POST : Url =",""+url);
		if (!loading) {
			loading = true;
		//	Toast.makeText(getActivity(), "Load more " + url, Toast.LENGTH_SHORT).show();

			Log.d(LiveHistoryGridviewFragment.class.getSimpleName(), "onRefresh: "+url);

			new RestServiceHistoryListCommand(getActivity(),user, url){

				protected void onPostExecute(java.util.List<StreamHistory> result) {
					hisstory = result;

					adapter.addData(hisstory);
					//	pager.setCurrentItem(1);
					adapter.notifyDataSetChanged();

					loading = false;
				}
			}.execute();


		}
	}

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1234){
           Bundle bundle= data.getBundleExtra(StreamHistory.class.getName());
            StreamHistory mStreamHistory = (StreamHistory) bundle.getSerializable(StreamHistory.class.getSimpleName());
            hisstory.add(requestCode,mStreamHistory);
            adapter.setData(hisstory);
            adapter.notifyDataSetChanged();
        }
    }
}

