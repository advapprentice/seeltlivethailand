package com.touchtechnologies.tit.history;


import java.util.List;
import java.util.Timer;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.tit.R;

public class LiveHistoryListAdapter extends BaseAdapter {
	private Context context;
	private Runnable runnable;
	protected ImageLoader imageLoader;
	protected DisplayImageOptions options;
	Timer t;
	private List<StreamHistory> arrcctv;
	private StreamHistory cctvs;
	String url;
	public LiveHistoryListAdapter(Context context) {
		this.context = context;
		

		imageLoader = ImageLoader.getInstance();
		
		
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.NONE)
		.showImageForEmptyUri(R.drawable.bg_noimage)
		.showImageOnFail(R.drawable.bg_noimage)
		.showImageOnLoading(R.drawable.bg_loading)
		.cacheInMemory(true)
		.displayer(new FadeInBitmapDisplayer(300)) //fade in images
	  	.resetViewBeforeLoading(true)
	  	.build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
		.memoryCache(new WeakMemoryCache())
		.threadPriority(Thread.NORM_PRIORITY)
        .denyCacheImageMultipleSizesInMemory()
        .build();
 	     	  
 	    imageLoader = ImageLoader.getInstance();
		if(!imageLoader.isInited()){
			imageLoader.init(config);	
		}
		
	}
	public void setData(List<StreamHistory> arrcctv){
		this.arrcctv = arrcctv;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return arrcctv == null?0:arrcctv.size();
	}
	
	@Override
	public StreamHistory getItem(int position) {
		return arrcctv.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	public void remove(StreamHistory his){
		boolean success = arrcctv.remove(his);
		if(success){
			notifyDataSetChanged();
		}
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		
		if(convertView == null){
			convertView = LayoutInflater.from(context).inflate(R.layout.item_mobilelive_list_history, parent, false);
			
			viewHolder = new ViewHolder();
			
		
			viewHolder.imglive = (ImageView)convertView.findViewById(R.id.imgcctv);
			viewHolder.tvlocation = (TextView)convertView.findViewById(R.id.txtlocation);
			viewHolder.tvcreatedate = (TextView)convertView.findViewById(R.id.txtcreatedate);
			viewHolder.tvupdate = (TextView)convertView.findViewById(R.id.txtupdate);
		
			viewHolder.txtstart = (TextView)convertView.findViewById(R.id.txtstart);
			viewHolder.txtfinish = (TextView)convertView.findViewById(R.id.txtfinish);
			

			convertView.setTag(viewHolder);
			
			
		}else{
			viewHolder = (ViewHolder)convertView.getTag();
		}
		cctvs = getItem(position);
		viewHolder.tvlocation.setText(getItem(position).getTitle());
			
		String createdate = getItem(position).getStartStreamingDatetime();
		String create = createdate.substring(0, 10);
		viewHolder.tvcreatedate.setText(create);
		
		String update = getItem(position).getFinishStreamingDatetime();
		String updatedate = update.substring(0, 10);
		viewHolder.tvupdate.setText(updatedate);
		
		String start = getItem(position).getStartStreamingDatetime();
		String timecreate = start.substring(11, 19);
		viewHolder.txtstart.setText(timecreate);
					
		String finish = getItem(position).getFinishStreamingDatetime();
		String timeupdate = finish.substring(11, 19);
		viewHolder.txtfinish.setText(timeupdate);
		
		
		
		
	  	imageLoader.displayImage(getItem(position).getSnapshots(),viewHolder.imglive, options);
		Log.i("CCTVActivity", "reloaded");

	
		return convertView;
	}



	class ViewHolder{
		ImageView imglive;
		TextView tvlocation;
		TextView tvcreatedate;
		TextView tvupdate;
		TextView txtstart;
		TextView txtfinish;
		View Viewvideoschedule;
		View topic;
		
	}
	
	
	
}