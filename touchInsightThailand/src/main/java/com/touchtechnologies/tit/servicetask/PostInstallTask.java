package com.touchtechnologies.tit.servicetask;

import org.apache.http.HttpStatus;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.reporter.PostInstallCommand;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.util.ResourceUtil;

public class PostInstallTask extends AsyncTask<Void, Void, Void> {
	final String PREF_POSTINSTALL = "postInstall";
	private Context context; 

	public PostInstallTask(Context context) {
		this.context = context;
	}
		
	@Override
	protected Void doInBackground(Void... params) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConfig.PREFERENCE_POSTINSTALL, Activity.MODE_PRIVATE);
		boolean postInstallSuccess = sharedPreferences.getBoolean(PREF_POSTINSTALL, false);
		
		//if no post install flag found, post it.
		if(!postInstallSuccess){
			//run post install every time when an application start
		    Command command = new PostInstallCommand(context);
		    
			ServiceConnector srConnector = new ServiceConnector(ResourceUtil.getServiceUrl(context));			
			ServiceResponse serviceResponse = srConnector.doAsynPost(command, true);
			
			try{
				int code = serviceResponse.getCode();
				if(code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK){
					Log.i(AppConfig.LOG, "Post install success");
					sharedPreferences.edit().putBoolean(PREF_POSTINSTALL, true).commit();
				}else{
					Log.i(AppConfig.LOG, "Post install failed");
				}
			}catch(Exception e){
				Log.e(AppConfig.LOG, "doInBackground error", e);
			}
		}
		return null;
	}
	
}