package com.touchtechnologies.tit.servicetask;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;

import com.touchtechnologies.command.reporter.UpdateUserLocationCommand;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

/**
 * Update location 
 * @author MEe
 *
 */
public class LocationUpdateTask extends AsyncTask<Location, Void, ServiceResponse> {
	private Context context;
	
	public LocationUpdateTask(FragmentActivity context) {
		this.context = context;
	}	
	
	@Override
	protected ServiceResponse doInBackground(Location... params) {
		
		UpdateUserLocationCommand command = new UpdateUserLocationCommand(context);
		
		Location location = params[0];
		User user = UserUtil.getUser(context);
		user.setLatitude(location.getLatitude());
		user.setLongitude(location.getLongitude());
	//	command.setUser(user);
		
		ServiceConnector srConnector = new ServiceConnector(ResourceUtil.getServiceUrl(context));
		ServiceResponse serviceResponse = srConnector.doAsynPost(command, true);

		return serviceResponse;
	}
}