package com.touchtechnologies.tit.servicetask;

import org.apache.http.HttpStatus;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.touchtechnologies.app.CommonServiceFragmentDialog;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.util.ResourceUtil;

public class CommonServiceTask extends AsyncTask<Command, Void, ServiceResponse> {
	protected FragmentActivity context;
	protected CommonServiceFragmentDialog dialog;
	
	/**
	 * Create a common service task without dialog.
	 * @param fragment
	 */
	public CommonServiceTask(FragmentActivity context){
		this.context = context;
	}
	
	/**
	 * Create a common service task with dialog
	 * @param fragment
	 * @param title
	 * @param message
	 * @param button
	 */
	public CommonServiceTask(FragmentActivity context, String title, String message,String button) {
		this.context = context;
		dialog = new CommonServiceFragmentDialog();
		
		Bundle bundle = new Bundle();
		if(title != null){
			bundle.putString("title", title);
		}
		if(message != null){
			bundle.putString("message", message);
		}
		if(button != null){
			bundle.putString("button", button);
		}
		dialog.setArguments(bundle);		
		dialog.setAsyncTask(this);
	}
		
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if(dialog != null){
			dialog.show(context.getSupportFragmentManager(), "commonservice");
			
		}
	}
	
	@Override
	protected ServiceResponse doInBackground(Command... params) {
		ServiceConnector srConnector = new ServiceConnector(ResourceUtil.getServiceUrl(context));
		ServiceResponse serviceResponse = srConnector.doAsynPost(params[0], true);

		return serviceResponse;
	}
	
	@Override
	protected void onPostExecute(ServiceResponse result) {
		Log.i(AppConfig.LOG, "responses received:" + result.getCode());
		if(result.getCode() == HttpStatus.SC_BAD_GATEWAY){
			AlertDialog alertDialog = new AlertDialog.Builder(context).create();
			alertDialog.setTitle(context.getText(R.string.error_no_bed_register));
			alertDialog.setMessage(context.getText(R.string.error_no_Wifi));
			
			alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,context.getText(R.string.up_Ok), new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
//					context.finish();
					dialog.dismiss();
				}
			});
			alertDialog.show();
		}
	}


}