package com.touchtechnologies.tit.notification;

/**
 * Created by TouchICS on 6/9/2016.
 */


import org.json.JSONObject;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.touchtechnologies.command.insightthailand.RestServiceLiveStreamPropertieCommand;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamConnection;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamingChannel;
import com.touchtechnologies.dataobject.insightthailand.Notification;
import com.touchtechnologies.dataobject.insightthailand.NotificationOption;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.dataobject.reporter.Feed;
import com.touchtechnologies.json.JSONUtil;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.cctvitlive.CCTVMenuMainActivity;
import com.touchtechnologies.tit.live.liststreaming.LiveOnAirPlayerActivity;
import com.touchtechnologies.tit.setting.SettingActivity;
import com.touchtechnologies.tit.setting.SwitchServerActivity;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

import java.util.Random;

public class ReporterIntentService extends IntentService {
    public static int NOTIFICATION_ID = 1;
    public static final String KEY_TYPE_LIVE = "Streamlive";

    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    private MediaPlayer mMediaPlayer;
    protected Context context;
    private User myUser;
//    MessageEventListner messageEventListner;
    public ReporterIntentService() {
        super("ReporterIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i("", "onHandleIntent");
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);
        myUser = UserUtil.getUser(getApplicationContext());
//        messageEventListner.messageEvent("onHandleIntent " + messageType);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
//            	sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
//                sendNotification("Deleted messages on server: " + extras.toString());
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // This loop represents the service doing some work.
//                for (int i=0; i<5; i++) {
//                    Log.i(AppConfig.LOG, "Working... " + (i+1)
//                            + "/5 @ " + SystemClock.elapsedRealtime());
//                    try {
//                        Thread.sleep(5000);
//                    } catch (InterruptedException e) {
//                    }
//                }
//                Log.i(AppConfig.LOG, "Completed work @ " + SystemClock.elapsedRealtime());
                // Post notification of received message.
                String message = extras.getString("message", "{}");
                Log.i("message", "============Notification==========: " + message);
                try{
                    JSONObject jsonObject = new JSONObject(message);
                    Log.i("Json", "============Notification: " + jsonObject);

                    if(!jsonObject.isNull("text")& !jsonObject.isNull("options")){

                        Notification noti = new Notification(jsonObject);

                        sendNotification(noti);
                    }
                }catch(Exception e){
                    Log.e("", "parsing push message error", e);
                }
                Log.d("", "Received:" + message);
                Log.i("", "Received: " + message);
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        ReporterBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(Notification msg) {
//    	messageEventListner.messageEvent(msg);

        Log.d("Json", "============Notification: " + msg.getText());
        Log.i("\tType", ": " +msg.getNotificationOption().getType());
    	/*
    	 * Do not push notification if user is not logged in.
    	 * Google always pushing message if server request to send to a valid registered ID
    	 */
        if(!UserUtil.isLoggedIn(getBaseContext())){
            return;
        }

        mNotificationManager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = null;
        Intent intent ;
        LiveStreamingChannel liveStream = null;


        intent = new Intent(getApplicationContext(), CCTVMenuMainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Notification.class.getName(), msg);
        contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);




        String noticID =""+msg.getNotificationOption().getId();
        NOTIFICATION_ID = noticID.hashCode();

        NotificationCompat.Builder mBuilder =  new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher_logo)
                        .setContentTitle("See it Live Thailand")
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(msg.getText()))
                        .setContentText(msg.getText());

        mBuilder.setContentIntent(contentIntent);
        mBuilder.setAutoCancel(true);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

        //notify message back to activity
        Intent i = new Intent("tit.notifyMessage");
        i.putExtra("message", msg.toString());
        sendBroadcast(i);
        setsound();

    }


    ///////// effect sond
    private void setsound() {

    	boolean checkSound = SwitchServerActivity.getCheckSound(getApplicationContext());
    	boolean checkVibrate = SwitchServerActivity.getCheckVibrate(getApplicationContext());

		if (checkSound == true) {
			mMediaPlayer = MediaPlayer.create(ReporterIntentService.this,R.raw.wiffpop);
			mMediaPlayer.start();
		} else if (checkSound == false){
			mMediaPlayer = MediaPlayer.create(ReporterIntentService.this,R.raw.wiffpop);
			mMediaPlayer.reset();

		}
		if(checkVibrate == true){
			Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
		    v.vibrate(1200);
		}else if(checkVibrate == false){
			Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
		    v.vibrate(0);
		}



    }


    private static Feed getFeedFromPush(JSONObject json){
        Feed feed = new Feed();
        try{
            feed.setId(JSONUtil.getString(json, "feedID"));
            feed.setTypeKey(JSONUtil.getString(json, "feedType"));
            feed.setTitle(JSONUtil.getString(json, "feedTitle"));
            feed.setStatus(JSONUtil.getString(json, "feedStatus"));
        }catch(Exception e){
            Log.e("", e.getMessage());
        }
        return feed;
    }
}