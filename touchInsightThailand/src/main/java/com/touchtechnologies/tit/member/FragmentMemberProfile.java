package com.touchtechnologies.tit.member;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ScrollView;

import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.delegate.AbsListViewDelegate;
import com.touchtechnologies.delegate.ScrollViewDelegate;
import com.touchtechnologies.fragment.BaseViewPagerFragment;
import com.touchtechnologies.tit.R;

/**
 * Created by TouchICS on 2/15/2016.
 */
public class FragmentMemberProfile extends BaseViewPagerFragment {
    private User user;
    private EditText edittextFirstName,edittextLastName,edittextBirthDate;
    private ScrollView mScrollView;
    private ScrollViewDelegate mScrollViewDelegate = new ScrollViewDelegate();


    public static FragmentMemberProfile newInstance(int position,User user){
        FragmentMemberProfile memberProfile = new FragmentMemberProfile();
        Bundle mBundle = new Bundle();
        mBundle.putInt(BUNDLE_FRAGMENT_INDEX,position);
        mBundle.putSerializable(User.class.getSimpleName(),user);
        memberProfile.setArguments(mBundle);
        return memberProfile;
    }



    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootview = inflater.inflate(R.layout.item_member_profile, container, false);

        user = (User)getArguments().getSerializable(User.class.getSimpleName());

        mScrollView = (ScrollView)rootview.findViewById(R.id.scrollView);
        edittextFirstName = (EditText)rootview.findViewById(R.id.editTextFirstName);
        edittextLastName = (EditText)rootview.findViewById(R.id.editTextLastName);
        edittextBirthDate = (EditText)rootview.findViewById(R.id.editTextBirthDate);

        edittextFirstName.setText(user.getFirstName());
        edittextLastName.setText(user.getLastName());
        edittextBirthDate.setText(user.getBirthDate());

        return rootview;
    }

    @Override
    public boolean isViewBeingDragged(MotionEvent event) {
        return mScrollViewDelegate.isViewBeingDragged(event,mScrollView);
    }
}