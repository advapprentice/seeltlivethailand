package com.touchtechnologies.tit.member;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.touchtechnologies.dataobject.insightthailand.User;

/**
 * Created by TouchICS on 2/15/2016.
 */
public class MemberProfileAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 2;
    // Tab Titles
    private String tabtitles[] = new String[] {"Live History","Profile"};
    Context context;
    User user;

    public MemberProfileAdapter(FragmentManager fm,User user) {
        super(fm);
        this.user = user;
    }

    @Override
    public Fragment getItem(int position) {
          Fragment fragment;
//        Bundle bundle = new Bundle();
//        bundle.putSerializable(User.class.getSimpleName(), user);

        switch (position) {

            // Open FragmentTab1.java
            case 0:
//                fragment = new MemberLiveStream();
//                fragment.setArguments(bundle);
                fragment = MemberLiveStream.newInstance(position,user);
                return fragment;

            // Open FragmentTab2.java
            case 1:
                fragment = FragmentMemberProfile.newInstance(position,user);
                return fragment;

        }
        return null;
    }

    @Override
    public int getCount() {

        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabtitles[position];
    }



}
