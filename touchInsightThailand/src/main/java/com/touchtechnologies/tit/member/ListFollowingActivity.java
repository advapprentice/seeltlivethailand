package com.touchtechnologies.tit.member;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.touchtechnologies.animate.progress.DotsTextView;
import com.touchtechnologies.command.insightthailand.RestServiceFollowingListCommand;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

import java.util.List;

/**
 * Created by TouchICS on 3/14/2016.
 */
public class ListFollowingActivity extends SettingActionbarActivity {
    public static String KEY_FOLLOWING = "following";
    public static String KEY_FOLLOWER = "follower";
    public static String KEY = "key";
    public static String KEY_TITLE = "key_title";
    private ListView listview;
    private  ListFollowingAdapter adapter;
    private List<User> arrUser;
    private String data ,title;
    private User user;
    private View viewFollowing,viewFollower,viewDot;
    TextView textDotLoading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following);



        user = (User)  getIntent().getSerializableExtra(User.class.getName());
        data = getIntent().getExtras().getString(KEY);
        title = getIntent().getExtras().getString(KEY_TITLE);
        mTitle = title;


        adapter = new ListFollowingAdapter(this);
        listview = (ListView)findViewById(R.id.list);
        viewFollowing =(View)findViewById(R.id.viewFollowing);
        viewFollower =(View)findViewById(R.id.viewFollower);
        viewDot = (View)findViewById(R.id.viewDot);


        listview.setAdapter(adapter);

        listLiveStreamMember();

    }

    public void listLiveStreamMember() {
        String url;
        User userlocal = UserUtil.getUser(this);
        url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_member_history)+user.getId()+"/"+data;
        Log.d(AppConfig.LOG,"url"+ url);

        new RestServiceFollowingListCommand(this,userlocal,url){

            protected void onPostExecute(java.util.List<User> result) {

                if(result.size()==0){
                    if(data.equals(KEY_FOLLOWING)){
                        viewFollowing.setVisibility(View.VISIBLE);
                        viewFollower.setVisibility(View.GONE);
                    }else if(data.equals(KEY_FOLLOWER)){
                        viewFollowing.setVisibility(View.GONE);
                        viewFollower.setVisibility(View.VISIBLE);
                    }

                }
                arrUser = result;

                adapter.setData(arrUser);
                adapter.notifyDataSetChanged();
                viewDot.setVisibility(View.GONE);
            }
        }.execute();

    }

}
