package com.touchtechnologies.tit.member;

import android.graphics.drawable.ClipDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import com.touchtechnologies.animate.progress.RotateLoading;
import com.touchtechnologies.command.insightthailand.RestServiceHistoryListCommand;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.delegate.AbsListViewDelegate;
import com.touchtechnologies.fragment.BaseViewPagerFragment;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.history.LiveHistoryGridviewAdapter;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.widget.ProgressBar;

import java.util.List;

/**
 * Created by TouchICS on 2/15/2016.
 */

public class MemberLiveStream extends BaseViewPagerFragment {
    private View viewLoading, viewEmptyHint,viewnotlive;
    private MemberLiveStreamAdapter adapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RotateLoading rotateLoading;
    private GridView  gridView ;
    private List<StreamHistory> arrHistorieslive;
    private  User  user;
    private AbsListViewDelegate mAbsListViewDelegate = new AbsListViewDelegate();
    ProgressBar pg;

    public static MemberLiveStream newInstance(int position,User user){
        MemberLiveStream memberLiveStream = new MemberLiveStream();
        Bundle mBundle = new Bundle();
        mBundle.putSerializable(User.class.getSimpleName(),user);
        mBundle.putInt(BUNDLE_FRAGMENT_INDEX,position);
        memberLiveStream.setArguments(mBundle);
        return memberLiveStream;
    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootview = inflater.inflate(R.layout.fragment_member_live_history_gridview, container, false);

        user = (User)getArguments().getSerializable(User.class.getSimpleName());
        viewEmptyHint = (View)rootview.findViewById(R.id.emptyAgentHint);

        adapter = new MemberLiveStreamAdapter(getActivity());
        viewnotlive = (View) rootview.findViewById(R.id.viewnotlive);
        viewnotlive.setVisibility(View.GONE);

        gridView = (GridView)rootview.findViewById(R.id.grid_view);
        gridView.setAdapter(adapter);

        pg = (ProgressBar)rootview.findViewById(R.id.progressBar);

        pg.getIndeterminateDrawable().setColorFilter(0xFF568ae8, android.graphics.PorterDuff.Mode.MULTIPLY);

        //    gridView.setOnItemClickListener(myOnItemClickListener);

        listLiveStreamMember();
        return rootview;
    }


    public void listLiveStreamMember() {
        String url;
        User userlocal = UserUtil.getUser(getActivity());
        url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_rest_member_history)+user.getId()+"/stream";
        new RestServiceHistoryListCommand(getActivity(),userlocal, url){

            protected void onPostExecute(java.util.List<StreamHistory> result) {
                arrHistorieslive = result;
                if (arrHistorieslive.size()==0) {
                    viewnotlive.setVisibility(View.VISIBLE);
                } else {
                    viewnotlive.setVisibility(View.GONE);
                }
                pg.setVisibility(View.GONE);
                adapter.setData(arrHistorieslive);
                adapter.notifyDataSetChanged();
            }
        }.execute();


    }

    @Override
    public boolean isViewBeingDragged(MotionEvent event) {
        return mAbsListViewDelegate.isViewBeingDragged(event, gridView);
    }
}
