package com.touchtechnologies.tit.member;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.opensource.pagerindicator.CircleBitmapDisplayer;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.LoveCommand;
import com.touchtechnologies.command.insightthailand.LoveUndoCommand;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.live.mylivestream.MyliveGridviewActivity;
import com.touchtechnologies.tit.login.LoginsActivity;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TouchICS on 3/14/2016.
 */
public class ListFollowingAdapter extends BaseAdapter implements View.OnClickListener{
    private Context context;
    private Runnable runnable;
    protected ImageLoader imageLoader;
    protected DisplayImageOptions options;
    private String liveId;
    private List<User> arrusers;
    private User user;
    private User localUser;

    String url;

    public ListFollowingAdapter(Context context) {
        this.context = context;

        imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPoolSize(3)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCacheSize(4194304)
                .defaultDisplayImageOptions(options).build();

        imageLoader.init(config);
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .imageScaleType(ImageScaleType.NONE)
                .showImageForEmptyUri(R.drawable.ic_new_imgaccount)
                .showImageOnFail(R.drawable.ic_new_imgaccount)
                .cacheOnDisc(true)
                .cacheInMemory(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new FadeInBitmapDisplayer(1000))
                .displayer(new CircleBitmapDisplayer())
                .build();
        localUser = UserUtil.getUser(context);
    }

    public void setData(List<User> user){
        this.arrusers = user;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return arrusers == null ? 0 : arrusers.size();
    }
    @Override
    public boolean hasStableIds() {
        return true;
    }


    public User getItem(int position) {
        return arrusers.get(position);
    }


    public long getItemId(int position) {
        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_following, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imgProfile = (ImageView)convertView.findViewById(R.id.imgProfile);
            viewHolder.txtProfile = (TextView)convertView.findViewById(R.id.txtName);
            viewHolder.btnFollow = (Button)convertView.findViewById(R.id.btnFollow);
            viewHolder.btnUnFollow = (Button)convertView.findViewById(R.id.btnUnFollow);

            viewHolder.btnFollow.setOnClickListener(this);
            viewHolder.btnUnFollow.setOnClickListener(this);
            viewHolder.imgProfile.setOnClickListener(this);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
            //		viewHolder.topic.setTag(getItem(position));
        }
        user = getItem(position);

        try {
            viewHolder.txtProfile.setText(getItem(position).getFirstName());

            if (user.isFollowed()){
                viewHolder.btnFollow.setVisibility(View.GONE);
                viewHolder.btnUnFollow.setVisibility(View.VISIBLE);

            }else {

                if (getItem(position).getId().equals(localUser.getId())){
                    viewHolder.btnFollow.setVisibility(View.GONE);
                    viewHolder.btnUnFollow.setVisibility(View.GONE);
                }else {
                    viewHolder.btnFollow.setVisibility(View.VISIBLE);
                    viewHolder.btnUnFollow.setVisibility(View.GONE);
                }
            }
        }catch (Exception e){
            Log.d("Error", "Null" + e.getMessage());
        }
        imageLoader.displayImage(getItem(position).getAvatar(), viewHolder.imgProfile, options);
        //	viewHolder.viewimage.setBackgroundColor(Color.argb(180, position * 50, position * 107, position * 181));
        viewHolder.btnFollow.setTag(user);
        viewHolder.btnUnFollow.setTag(user);
        viewHolder.imgProfile.setTag(user);
        viewHolder.btnFollow.setTag(R.id.btnFollow, viewHolder);
        viewHolder.btnUnFollow.setTag(R.id.btnUnFollow, viewHolder);
        return convertView;
    }


    class ViewHolder{
        Button btnFollow,btnUnFollow;
        ImageView imgProfile;
        TextView txtProfile;
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        User user = (User)v.getTag();

        ViewHolder viewHolder;
        switch (v.getId()){
            case R.id.btnFollow :
                if(UserUtil.isLoggedIn(context)) {
                    user.setIsFollowed(true);
                    isFollowing(user.getId());
                    viewHolder = (ViewHolder)v.getTag(R.id.btnFollow);
                    viewHolder.btnFollow.setVisibility(View.GONE);
                    viewHolder.btnUnFollow.setVisibility(View.VISIBLE);
                }else{
                    intent = new Intent(context, LoginsActivity.class);
                    context.startActivity(intent);
                }

            break;
            case R.id.btnUnFollow :
                if(UserUtil.isLoggedIn(context)) {
                    user.setIsFollowed(false);
                    UnFollowing(user.getId());
                    viewHolder = (ViewHolder)v.getTag(R.id.btnUnFollow);
                    viewHolder.btnFollow.setVisibility(View.VISIBLE);
                    viewHolder.btnUnFollow.setVisibility(View.GONE);
                }else{
                    intent = new Intent(context, LoginsActivity.class);
                    context.startActivity(intent);
                }
                break;
            case R.id.imgProfile:

                String idUser = localUser.getId();
                if(user.getId().equals(idUser)){
                    if(UserUtil.isLoggedIn(context)) {
                        intent = new Intent(context, MyliveGridviewActivity.class);
                        context.startActivity(intent);
                    }else{
                        intent = new Intent(context, LoginsActivity.class);
                        context.startActivity(intent);
                    }
                }else{
                    intent = new Intent(context, MemberProfileActivity.class);
                    intent.putExtra(User.class.getName(),user);
                    ((Activity) context).startActivityForResult(intent, 1);
                    //	context.startActivity(intent);
                }
                break;

        }

    }

    public void isFollowing(String his){
        liveId = his;
        LoveCommand command = new LoveCommand(context);
        new Following((FragmentActivity)context).execute(command);

    }

    public void UnFollowing(String his){
        liveId = his;
        User localuser = UserUtil.getUser(context);
        String url = ResourceUtil.getServiceUrl(context)+context.getString(R.string.app_service_update_user)+localuser.getId()+"/following/"+liveId;
        Log.d("Source", "URL :"+url);
        new LoveUndoCommand(context, localuser,url) {

            protected void onPostExecute(JSONObject result) {

                try {
                    if(result!=null){
                        int status = result.getInt("status");
                        String message = result.getString("message");
                        Log.d("message Response",":"+message);
                        notifyDataSetChanged();
                    }else{
                        Toast.makeText(context, "Fail! No Internet Connection", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.getStackTrace();

                }

            };
        }.execute();

    }


    class Following extends CommonServiceTask {

        public Following(FragmentActivity context) {
            super(context);
        }


        @Override
        protected ServiceResponse doInBackground(Command... params) {
            User localuser = UserUtil.getUser(context);
            String url = ResourceUtil.getServiceUrl(context)+context.getString(R.string.app_service_update_user)+localuser.getId()+"/following/"+liveId;

            Log.d("Source", "="); Log.d("Source", "URL :"+url);
            ServiceConnector srConnector = new ServiceConnector(url);

            List<NameValuePair> headers = new ArrayList<NameValuePair>();
            headers.add(new BasicNameValuePair("Content-Type", "application/json"));
            headers.add(new BasicNameValuePair("X-TIT-ACCESS-TOKEN", localuser.getToken()));

            ServiceResponse serviceResponse = srConnector.doAsynPost(params[0], true, headers);
            Log.d("headers", "URL :"+headers);

            try {
                int code = serviceResponse.getCode();
                if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
                    JSONObject objuser = new JSONObject(serviceResponse.getContent().toString());

                }

            } catch (Exception e) {

                serviceResponse.setMessage(e.getMessage());
                Log.e(AppConfig.LOG, "doInBackground error", e);
            }

            return serviceResponse;
        }



        @Override
        protected void onPostExecute(ServiceResponse result) {
            super.onPostExecute(result);

            try {
                if (result.getCode() == ServiceResponse.SUCCESS|| result.getCode() == HttpStatus.SC_OK) {
                    notifyDataSetChanged();
                    context.setResult(Activity.RESULT_OK);

                } else {
                    Toast.makeText(context, "Update Fail"+"!\r\n" + result.getMessage(),Toast.LENGTH_LONG).show();

                }
            } catch (Exception e) {
                Toast.makeText(context, "ERROR!\r\n" + e.getMessage(),Toast.LENGTH_LONG).show();
                Log.e(AppConfig.LOG, "process response error", e);
            }
        }
    }


}