package com.touchtechnologies.tit.member;

/**
 * Created by TouchICS on 2/17/2016.
 */


import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.LoveCommand;
import com.touchtechnologies.command.insightthailand.LoveUndoCommand;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.history.LiveHistoryPlayer;
import com.touchtechnologies.tit.login.LoginsActivity;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class MemberLiveStreamAdapter extends BaseAdapter implements View.OnClickListener{
    private Context context;
    private Runnable runnable;
    protected ImageLoader imageLoader;
    protected DisplayImageOptions options;
    private String liveId;
    private Timer t;
    private List<StreamHistory> arrHistorieslive;
    private StreamHistory history;

    String url;
    public MemberLiveStreamAdapter(Context context) {
        this.context = context;


        imageLoader = ImageLoader.getInstance();

        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .showImageForEmptyUri(R.drawable.bg_cctvdefault)
                .showImageOnFail(R.drawable.bg_cctvdefault)
                .showImageOnLoading(R.drawable.bg_cctvdefault)
                .cacheInMemory(true)
                .handler(new Handler())
                        //	.displayer(new FadeInBitmapDisplayer(500)) //fade in images
                        //	.resetViewBeforeLoading(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)

                .denyCacheImageMultipleSizesInMemory()
                .threadPoolSize(5)
                .threadPriority(Thread.NORM_PRIORITY - 2) // default
                .tasksProcessingOrder(QueueProcessingType.FIFO) // default
                .denyCacheImageMultipleSizesInMemory()
                .imageDownloader(new BaseImageDownloader(context)) // default
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                .writeDebugLogs()
                .build();

        // Initialize ImageLoader with configuration.


        imageLoader.init(config);
        imageLoader = ImageLoader.getInstance();


    }
    public void setData(List<StreamHistory> arrcctv){
        this.arrHistorieslive = arrcctv;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return arrHistorieslive == null ? 0 : arrHistorieslive.size();
    }
    @Override
    public boolean hasStableIds() {
        return true;
    }


    public StreamHistory getItem(int position) {
        return arrHistorieslive.get(position);
    }


    public long getItemId(int position) {
        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_member_live_history, parent, false);


            viewHolder = new ViewHolder();


            viewHolder.imageView = (ImageView)convertView.findViewById(R.id.imageViewMedia);
            viewHolder.textView  = (TextView)convertView.findViewById(R.id.texttitlelive);
            viewHolder.viewimage = (View)convertView.findViewById(R.id.Viewimage);
            viewHolder.textCount = (TextView)convertView.findViewById(R.id.txtCount);
            viewHolder.viewVideo = (View)convertView.findViewById(R.id.viewVideos);
            viewHolder.imgLove = (ImageView)convertView.findViewById(R.id.imgLove);
            viewHolder.imgUnLove = (ImageView)convertView.findViewById(R.id.imgUnlove);

            viewHolder.textviewLove = (TextView)convertView.findViewById(R.id.textlove);

            viewHolder.imgLove.setOnClickListener(this);
            viewHolder.imgUnLove.setOnClickListener(this);
            viewHolder.viewVideo.setOnClickListener(this);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
            //		viewHolder.topic.setTag(getItem(position));

        }
        history = getItem(position);

        try {
            viewHolder.textView.setText(getItem(position).getTitle());
            viewHolder.textCount.setText(getItem(position).getWatchedCount());
            viewHolder.textviewLove.setText("" + getItem(position).getLove());


            if (getItem(position).isLove()){
                viewHolder.imgLove.setVisibility(View.GONE);
                viewHolder.imgUnLove.setVisibility(View.VISIBLE);
            }else {
                viewHolder.imgLove.setVisibility(View.VISIBLE);
                viewHolder.imgUnLove.setVisibility(View.GONE);
            }




        }catch (Exception e){
            Log.d("Error", "Null" + e.getMessage());
        }

        imageLoader.displayImage(getItem(position).getSnapshots(), viewHolder.imageView, options);

        //	viewHolder.viewimage.setBackgroundColor(Color.argb(180, position * 50, position * 107, position * 181));
        viewHolder.viewVideo.setTag(arrHistorieslive.get(position));
        viewHolder.imgLove.setTag(history);
        viewHolder.imgUnLove.setTag(history);
        viewHolder.imgLove.setTag(R.id.imgLove, viewHolder);
        viewHolder.imgUnLove.setTag(R.id.imgUnlove, viewHolder);

        return convertView;
    }


    class ViewHolder{
        ImageView imageView,imgLove,imgUnLove;
        TextView textView;
        TextView textCount;
        View viewdisplays;
        View viewVideo;
        View viewimage;
        TextView textviewLove;
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        StreamHistory myhistory = (StreamHistory)v.getTag();
        User user = myhistory.getUser();

        ViewHolder viewHolder;
        switch (v.getId()){
            case R.id.viewVideos :
                intent = new Intent(context, LiveHistoryPlayer.class);
                intent.putExtra(StreamHistory.class.getName(),myhistory);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);

                break;
            case R.id.imgLove :
                if(UserUtil.isLoggedIn(context)) {
                    myhistory.setIsLove(true);
                    myhistory.setLove(myhistory.getLove() + 1);
                    isLove(myhistory.getId());
                    viewHolder = (ViewHolder)v.getTag(R.id.imgLove);
                    viewHolder.imgLove.setVisibility(View.GONE);
                    viewHolder.imgUnLove.setVisibility(View.VISIBLE);
                }else{
                    intent = new Intent(context, LoginsActivity.class);
                    context.startActivity(intent);

                }
                break;
            case R.id.imgUnlove :
                if(UserUtil.isLoggedIn(context)) {
                    myhistory.setIsLove(false);
                    myhistory.setLove(myhistory.getLove()-1);
                    isUnLove(myhistory.getId());
                    viewHolder = (ViewHolder)v.getTag(R.id.imgUnlove);
                    viewHolder.imgLove.setVisibility(View.VISIBLE);
                    viewHolder.imgUnLove.setVisibility(View.GONE);
                }else{
                    intent = new Intent(context, LoginsActivity.class);
                    context.startActivity(intent);

                }
                break;


        }

    }

    public void isLove(String his){
        liveId = his;

        LoveCommand command = new LoveCommand(context);
        new Love((FragmentActivity)context).execute(command);

    }
    public void isUnLove(String his){
        liveId = his;
        User user = UserUtil.getUser(context);
        String url = ResourceUtil.getServiceUrl(context)+context.getString(R.string.app_service_love)+liveId+"/loves";
        Log.d("Source", "URL :"+url);
        new LoveUndoCommand(context, user,url) {

            protected void onPostExecute(JSONObject result) {

                try {
                    if(result!=null){
                        int status = result.getInt("status");
                        String message = result.getString("message");
                        Log.d("message Response",":"+message);
                        notifyDataSetChanged();
                    }else{
                        Toast.makeText(context,"Fail! No Internet Connection",Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.getStackTrace();

                }

            };
        }.execute();

    }


    class Love extends CommonServiceTask {

        public Love(FragmentActivity context) {
            super(context);
        }


        @Override
        protected ServiceResponse doInBackground(Command... params) {

            String url = ResourceUtil.getServiceUrl(context)+context.getString(R.string.app_service_love)+liveId+"/loves";
            User user = UserUtil.getUser(context);
            Log.d("Source", "="); Log.d("Source", "URL :"+url);
            ServiceConnector srConnector = new ServiceConnector(url);

            List<NameValuePair> headers = new ArrayList<NameValuePair>();
            headers.add(new BasicNameValuePair("Content-Type", "application/json"));
            headers.add(new BasicNameValuePair("X-TIT-ACCESS-TOKEN", user.getToken()));

            ServiceResponse serviceResponse = srConnector.doAsynPost(params[0], true, headers);
            Log.d("headers", "URL :"+headers);

            try {
                int code = serviceResponse.getCode();
                if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
                    JSONObject objuser = new JSONObject(serviceResponse.getContent().toString());

                }

            } catch (Exception e) {

                serviceResponse.setMessage(e.getMessage());
                Log.e(AppConfig.LOG, "doInBackground error", e);
            }

            return serviceResponse;
        }



        @Override
        protected void onPostExecute(ServiceResponse result) {
            super.onPostExecute(result);

            try {
                if (result.getCode() == ServiceResponse.SUCCESS|| result.getCode() == HttpStatus.SC_OK) {
                    notifyDataSetChanged();
                    context.setResult(Activity.RESULT_OK);

                } else {
                    Toast.makeText(context, "Update Fail"+"!\r\n" + result.getMessage(),Toast.LENGTH_LONG).show();

                }
            } catch (Exception e) {
                Toast.makeText(context, "ERROR!\r\n" + e.getMessage(),Toast.LENGTH_LONG).show();
                Log.e(AppConfig.LOG, "process response error", e);
            }
        }
    }


}