package com.touchtechnologies.tit.member;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.SparseArrayCompat;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.opensource.pagerindicator.CircleBitmapDisplayer;
import com.touchtechnologies.animate.pager.PagerSlidingTabStrip;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.LoveCommand;
import com.touchtechnologies.command.insightthailand.LoveUndoCommand;
import com.touchtechnologies.command.insightthailand.RestServiceHistoryListCommand;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamConnection;
import com.touchtechnologies.dataobject.insightthailand.StreamHistory;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.live.LiveCam;
import com.touchtechnologies.tit.login.LoginsActivity;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;
import com.touchtechnologies.tools.ScrollableFragmentListener;
import com.touchtechnologies.tools.ScrollableListener;
import com.touchtechnologies.tools.ViewPagerHeaderHelper;
import com.touchtechnologies.widget.SlidingTabLayout;
import com.touchtechnologies.widget.TouchCallbackLayout;

import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TouchICS on 2/15/2016.
 */
public class MemberProfileActivity extends SettingActionbarActivity implements View.OnClickListener,TouchCallbackLayout.TouchEventListener,
        ScrollableFragmentListener,ViewPagerHeaderHelper.OnViewPagerTouchListener{
    int red = 0xFF6495ED;
    int orance = 0xFFf7871f;
    private MemberProfileAdapter adapter;
    private User user ;
    private TextView txtName,txtFollower,txtFollowing;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private ImageView imgProfile ;
    private Button btnFollow,btnUnFollow;
    private View viewFollowing,viewFollower;
    private String TAG = MemberProfileActivity.class.getSimpleName();
    private static final long  DEFAULT_DURATION = 300L;
    private static final float DEFAULT_DAMPING  = 1.5f;
    private SparseArrayCompat<ScrollableListener> mScrollableListenerArrays = new SparseArrayCompat<>();
    private ViewPager             mViewPager;
    private View                  mHeaderLayoutView;
    private ViewPagerHeaderHelper mViewPagerHeaderHelper;

    private int mTouchSlop;
    private int mTabHeight;
    private int mHeaderHeight;

    private Interpolator mInterpolator = new DecelerateInterpolator();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member);
        imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPoolSize(3)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCacheSize(4194304)
                .defaultDisplayImageOptions(options).build();

        imageLoader.init(config);
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .imageScaleType(ImageScaleType.NONE)
                .showImageForEmptyUri(R.drawable.ic_new_imgaccount)
                .showImageOnFail(R.drawable.ic_new_imgaccount)
                .cacheOnDisc(true)
                .cacheInMemory(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new FadeInBitmapDisplayer(1000))
                .displayer(new CircleBitmapDisplayer())
                .build();

        user = (User)  getIntent().getSerializableExtra(User.class.getName());


        mTitle = user.getFirstName();

        mViewPager = (ViewPager)findViewById(R.id.pagerhotel);

        txtName = (TextView)findViewById(R.id.txtName);
        txtFollower = (TextView)findViewById(R.id.txtFollower);
        txtFollowing  =(TextView)findViewById(R.id.txtFollowing);
        imgProfile =(ImageView)findViewById(R.id.imgProfile);
        btnFollow =(Button)findViewById(R.id.btnFollow);
        btnUnFollow =(Button)findViewById(R.id.btnUnFollow);
        viewFollowing = (View)findViewById(R.id.viewFollowing);
        viewFollower = (View)findViewById(R.id.viewFollower);


        txtName.setText(user.getFirstName());
        txtFollower.setText("" + user.getFollower());
        txtFollowing.setText(""+user.getFollowing());

        imageLoader.displayImage(user.getAvatar(), imgProfile, options);

        if (user.isFollowed()){
            btnFollow.setVisibility(View.GONE);
            btnUnFollow.setVisibility(View.VISIBLE);
        }else {
            btnFollow.setVisibility(View.VISIBLE);
            btnUnFollow.setVisibility(View.GONE);
        }

        mTouchSlop = ViewConfiguration.get(this).getScaledTouchSlop();
        mTabHeight = getResources().getDimensionPixelSize(R.dimen.tabs_height);
        mHeaderHeight = getResources().getDimensionPixelSize(R.dimen.viewpager_header_height);

        mViewPagerHeaderHelper = new ViewPagerHeaderHelper(this, this);

        TouchCallbackLayout touchCallbackLayout = (TouchCallbackLayout) findViewById(R.id.layout_touchcallback);
        touchCallbackLayout.setTouchEventListener(this);

        mHeaderLayoutView = findViewById(R.id.header_activitymember);

        PagerSlidingTabStrip slidingTabLayout = (PagerSlidingTabStrip) findViewById(R.id.tabs);


        adapter = new MemberProfileAdapter(getSupportFragmentManager(),user);
        // Set the ViewPagerAdapter into ViewPager
        mViewPager.setAdapter(adapter);
        slidingTabLayout.setShouldExpand(true);
        slidingTabLayout.setIndicatorHeight(5);
        slidingTabLayout.setViewPager(mViewPager);


//        PagerTabStrip pagerTabStrip = (PagerTabStrip)findViewById(R.id.PagerTabStriphotel);
//        pagerTabStrip.setDrawFullUnderline(false);
//        pagerTabStrip.setTabIndicatorColor(getResources().getColor(R.color.transparent));
        ViewCompat.setTranslationY(mViewPager, mHeaderHeight);
        btnFollow.setOnClickListener(this);
        btnUnFollow.setOnClickListener(this);
        viewFollowing.setOnClickListener(this);
        viewFollower.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        Intent intent ;
        switch (v.getId()){
            case R.id.viewFollowing:
                if(UserUtil.isLoggedIn(this)){
                    intent = new Intent(this,ListFollowingActivity.class);
                    intent.putExtra(User.class.getName(),user);
                    intent.putExtra("key","following");
                    intent.putExtra("key_title","Following");
                    startActivity(intent);
                }else{
                    startActivity(new Intent(this, LoginsActivity.class));

                }


                break;
            case R.id.viewFollower:
                if(UserUtil.isLoggedIn(this)){
                    intent = new Intent(this,ListFollowingActivity.class);
                    intent.putExtra(User.class.getName(), user);
                    intent.putExtra("key", "follower");
                    intent.putExtra("key_title", "Follower");
                    startActivity(intent);
                }else{
                    startActivity(new Intent(this, LoginsActivity.class));

                }

                break;
            case R.id.btnFollow:
                if(UserUtil.isLoggedIn(this)){
                    btnFollow.setVisibility(View.GONE);
                    btnUnFollow.setVisibility(View.VISIBLE);
                    txtFollower.setText("" + user.setFollower(user.getFollower() + 1));
                    isFollowing();
                }else{
                    startActivity(new Intent(this, LoginsActivity.class));

                }

                break;
            case R.id.btnUnFollow:
                if(UserUtil.isLoggedIn(this)){
                    btnFollow.setVisibility(View.VISIBLE);
                    btnUnFollow.setVisibility(View.GONE);
                    txtFollower.setText("" + user.setFollower(user.getFollower()-1));
                    UnFollowing();
                }else{
                    startActivity(new Intent(this, LoginsActivity.class));

                }

                break;
        }
    }

    public void isFollowing(){

        LoveCommand command = new LoveCommand(this);
        new Following((FragmentActivity)this).execute(command);

    }

    public void UnFollowing(){

        User userlocal = UserUtil.getUser(this);
        String url = ResourceUtil.getServiceUrl(this) + getString(R.string.app_service_update_user)+userlocal.getId()+"/following/"+user.getId();
        Log.d("TTTTTTTTT", "URL :" +url);
        new LoveUndoCommand(this, userlocal,url) {

            protected void onPostExecute(JSONObject result) {

                try {
                    if(result!=null){
                        int status = result.getInt("status");
                        String message = result.getString("message");
                        Log.d("message Response", ":" + message);


                    }else{
                        Toast.makeText(getApplicationContext(), "Fail! No Internet Connection", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.getStackTrace();

                }

            };
        }.execute();

    }
    @Override
    public boolean onLayoutInterceptTouchEvent(MotionEvent event) {

        return mViewPagerHeaderHelper.onLayoutInterceptTouchEvent(event,
                mTabHeight + mHeaderHeight);
    }

    @Override
    public boolean onLayoutTouchEvent(MotionEvent event) {
        return mViewPagerHeaderHelper.onLayoutTouchEvent(event);
    }

    @Override public boolean isViewBeingDragged(MotionEvent event) {
        return mScrollableListenerArrays.valueAt(mViewPager.getCurrentItem()).isViewBeingDragged(event);
    }

    @Override public void onMoveStarted(float y) {

    }

    @Override public void onMove(float y, float yDx) {
        float headerTranslationY = ViewCompat.getTranslationY(mHeaderLayoutView) + yDx;
        if (headerTranslationY >= 0) { // pull end
            headerExpand(0L);

            //Log.d("kaede", "pull end");
            if(countPullEnd>=1){
                if (countPullEnd==1){
                    downtime= SystemClock.uptimeMillis();
                    simulateTouchEvent(mViewPager,downtime, SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 250, y+mHeaderHeight);
                }
                simulateTouchEvent(mViewPager,downtime, SystemClock.uptimeMillis(), MotionEvent.ACTION_MOVE, 250, y+mHeaderHeight);
            }
            countPullEnd++;

        } else if (headerTranslationY <= -mHeaderHeight) { // push end
            headerFold(0L);

            //Log.d("kaede", "push end");
            //Log.d("kaede", "kaede onMove y="+y+",yDx="+yDx+",headerTranslationY="+headerTranslationY);
            if(countPushEnd>=1){
                if (countPushEnd==1){
                    downtime=SystemClock.uptimeMillis();
                    simulateTouchEvent(mViewPager,downtime, SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 250, y+mHeaderHeight);
                }
                simulateTouchEvent(mViewPager,downtime, SystemClock.uptimeMillis(), MotionEvent.ACTION_MOVE, 250, y+mHeaderHeight);
            }
            countPushEnd++;

        } else {

            //Log.d("kaede", "ing");
        	/*if(!isHasDispatchDown3){
        	simulateTouchEvent(mViewPager,SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_CANCEL, 250, y+mHeaderHeight);
        	isHasDispatchDown3=true;
        	}*/

            ViewCompat.animate(mHeaderLayoutView)
                    .translationY(headerTranslationY)
                    .setDuration(0)
                    .start();
            ViewCompat.animate(mViewPager)
                    .translationY(headerTranslationY + mHeaderHeight)
                    .setDuration(0)
                    .start();
        }
    }

    long downtime=-1;
    int countPushEnd=0,countPullEnd=0;
    private void simulateTouchEvent(View dispatcher, long downTime, long eventTime, int action, float x, float y) {
        MotionEvent motionEvent = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), action, x, y, 0);
        try {
            dispatcher.dispatchTouchEvent(motionEvent);
        } catch (Throwable e) {
            Log.e(TAG, "simulateTouchEvent error: " + e.toString());
        } finally {
            motionEvent.recycle();
        }
    }

    @Override
    public void onMoveEnded(boolean isFling, float flingVelocityY) {

        //Log.d("kaede", "move end");
        countPushEnd = countPullEnd=0;

        float headerY = ViewCompat.getTranslationY(mHeaderLayoutView); // 0到负数
        if (headerY == 0 || headerY == -mHeaderHeight) {
            return;
        }

        if (mViewPagerHeaderHelper.getInitialMotionY() - mViewPagerHeaderHelper.getLastMotionY()
                < -mTouchSlop) {  // pull > mTouchSlop = expand
            headerExpand(headerMoveDuration(true, headerY, isFling, flingVelocityY));
        } else if (mViewPagerHeaderHelper.getInitialMotionY()
                - mViewPagerHeaderHelper.getLastMotionY()
                > mTouchSlop) { // push > mTouchSlop = fold
            headerFold(headerMoveDuration(false, headerY, isFling, flingVelocityY));
        } else {
            if (headerY > -mHeaderHeight / 2f) {  // headerY > header/2 = expand
                headerExpand(headerMoveDuration(true, headerY, isFling, flingVelocityY));
            } else { // headerY < header/2= fold
                headerFold(headerMoveDuration(false, headerY, isFling, flingVelocityY));
            }
        }
    }


    private long headerMoveDuration(boolean isExpand, float currentHeaderY, boolean isFling,
                                    float velocityY) {

        long defaultDuration = DEFAULT_DURATION;

        if (isFling) {

            float distance = isExpand ? Math.abs(mHeaderHeight) - Math.abs(currentHeaderY) : Math.abs(currentHeaderY);

            velocityY = Math.abs(velocityY) / 1000;

            defaultDuration = (long) (distance / velocityY * DEFAULT_DAMPING);

            defaultDuration = defaultDuration > DEFAULT_DURATION ? DEFAULT_DURATION : defaultDuration;
        }

        return defaultDuration;
    }

    private void headerFold(long duration) {
        ViewCompat.animate(mHeaderLayoutView).translationY(-mHeaderHeight).setDuration(duration).setInterpolator(mInterpolator).start();

        ViewCompat.animate(mViewPager).translationY(0).setDuration(duration).setInterpolator(mInterpolator).start();

        mViewPagerHeaderHelper.setHeaderExpand(false);
    }

    private void headerExpand(long duration) {
        ViewCompat.animate(mHeaderLayoutView).translationY(0).setDuration(duration).setInterpolator(mInterpolator).start();

        ViewCompat.animate(mViewPager).translationY(mHeaderHeight).setDuration(duration).setInterpolator(mInterpolator).start();

        mViewPagerHeaderHelper.setHeaderExpand(true);
    }

    @Override
    public void onFragmentAttached(ScrollableListener listener, int position) {
        mScrollableListenerArrays.put(position, listener);
    }

    @Override
    public void onFragmentDetached(ScrollableListener listener, int position) {
        mScrollableListenerArrays.remove(position);
    }


    class Following extends CommonServiceTask {

        public Following(FragmentActivity context) {
            super(context);
        }


        @Override
        protected ServiceResponse doInBackground(Command... params) {
            User localuser = UserUtil.getUser(context);
            String url = ResourceUtil.getServiceUrl(context)+context.getString(R.string.app_service_update_user)+localuser.getId()+"/following/"+user.getId();

            Log.d("Source", "="); Log.d("Source", "URL :"+url);
            ServiceConnector srConnector = new ServiceConnector(url);

            List<NameValuePair> headers = new ArrayList<NameValuePair>();
            headers.add(new BasicNameValuePair("Content-Type", "application/json"));
            headers.add(new BasicNameValuePair("X-TIT-ACCESS-TOKEN", localuser.getToken()));

            ServiceResponse serviceResponse = srConnector.doAsynPost(params[0], true, headers);
            Log.d("headers", "URL :"+headers);

            try {
                int code = serviceResponse.getCode();
                if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
                    JSONObject objuser = new JSONObject(serviceResponse.getContent().toString());

                }

            } catch (Exception e) {

                serviceResponse.setMessage(e.getMessage());
                Log.e(AppConfig.LOG, "doInBackground error", e);
            }

            return serviceResponse;
        }



        @Override
        protected void onPostExecute(ServiceResponse result) {
            super.onPostExecute(result);

            try {
                if (result.getCode() == ServiceResponse.SUCCESS|| result.getCode() == HttpStatus.SC_OK) {
                   context.setResult(Activity.RESULT_OK);

                } else {
                    Toast.makeText(context, "Fail"+"!\r\n" + result.getMessage(),Toast.LENGTH_LONG).show();

                }
            } catch (Exception e) {
                Toast.makeText(context, "ERROR!\r\n" + e.getMessage(),Toast.LENGTH_LONG).show();
                Log.e(AppConfig.LOG, "process response error", e);
            }
        }
    }




}
