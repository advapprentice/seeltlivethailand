package com.touchtechnologies.tit.roi;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.util.LruCache;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.opensource.pagerindicator.CirclePageIndicator;
import com.touchtechnologies.command.insightthailand.RestCategoryServiceHotelListCommand;
import com.touchtechnologies.command.insightthailand.RestServiceHotelListCommand;
import com.touchtechnologies.command.insightthailand.RestServiceROIListCommand;
import com.touchtechnologies.dataobject.Category;
import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.tit.POIAdapter;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.hotel.HotelActivity;
import com.touchtechnologies.tit.lib.animator.DepthPageTransformer;
import com.touchtechnologies.tit.util.LruDaoCache;
import com.touchtechnologies.tit.util.ResourceUtil;

public class ROIPagerFragment extends Fragment implements OnItemClickListener,
		OnClickListener {
	private List<ROI> rois;
	private POIAdapter adapter;
	private ROICategoriesAdapter categoryAdapter;
	ViewPager mViewPager, rViewPager;
	private int positionRoiSelected;

	PagerAdapter pagerAdapter;
	LruCache<String, String> lruCache;
	GridView gridView;
	CirclePageIndicator circlepager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		lruCache = LruDaoCache.getInstance();

		categoryAdapter = new ROICategoriesAdapter(getActivity(), this);

		String url = ResourceUtil.getServiceUrl(getActivity())
				+ getResources().getString(R.string.app_service_rest_rois);

		new RestServiceROIListCommand(getActivity(), url) {

			/**
			 * List roi from cached or get roi from the remote server if a
			 * cached does not exist.
			 */
			@Override
			protected List<ROI> doInBackground(Void... params) {
				List<ROI> rois = null;
				String jsonCached = lruCache.get("rois");
				// TODO remove
				jsonCached = null;
				if (jsonCached != null) {
					rois = new ArrayList<ROI>();
					try {
						// parsing response to json
						JSONArray jArray = new JSONArray(jsonCached);

						// parsing json element to ROI object
						for (int i = 0; i < jArray.length(); i++) {
							rois.add(new ROI(jArray.getJSONObject(i)));
						}
					} catch (Exception e) {
						Log.e(this.getClass().getSimpleName(), e.getMessage(),
								e);
					}

				} else {
					// return remote roi
					rois = super.doInBackground(params);
				}

				return rois;
			}

			protected void onPostExecute(java.util.List<ROI> result) {
				rois = result;
				adapter.notifyDataSetChanged();

				if (result.size() > 0) {
					categoryAdapter.setData(result.get(0));
					categoryAdapter.notifyDataSetChanged();
					pagerAdapter.notifyDataSetChanged();

					roichangpager(rois.get(0));

					// do caching in a thread
					Thread th = new Thread() {
						public void run() {
							JSONArray jsonArray = new JSONArray();
							for (ROI roi : rois) {
								jsonArray.put(roi.toString());
							}
							lruCache.put("rois", jsonArray.toString());
						};
					};
					th.start();
				}
			};
		}.execute();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_rois_pager, container,
				false);
		//
		// PagerTabStrip pagerTabStrip =
		// (PagerTabStrip)view.findViewById(R.id.pager_title_strip);
		// pagerTabStrip.setDrawFullUnderline(true);
		// pagerTabStrip.setTabIndicatorColor(Color.parseColor("#fefefe"));
		//
		pagerAdapter = new ROIAdapter(getFragmentManager());

		mViewPager = (ViewPager) view.findViewById(R.id.pager);
		mViewPager.setAdapter(pagerAdapter);
		mViewPager.setPageTransformer(true, new DepthPageTransformer());
		mViewPager.setOnPageChangeListener(new PageChangeListner());

		// // Categories
		rViewPager = (ViewPager) view.findViewById(R.id.cpager);
		rViewPager.setAdapter(categoryAdapter);
		rViewPager.setClipChildren(false);
		rViewPager.setOffscreenPageLimit(categoryAdapter.getCount());
		rViewPager.setPageMargin(15);
		rViewPager.setCurrentItem(1);

		// // circlepager
		circlepager = (CirclePageIndicator) view.findViewById(R.id.indicator);
		circlepager.setViewPager(mViewPager);
		circlepager.setOnPageChangeListener(new PageChangeListner());

		final float density = getResources().getDisplayMetrics().density;

		
		
			circlepager.setRadius(5 * density);
		//	circlepager.setPageColor(0xFFCFCFCF);
		//	circlepager.setFillColor(0xFF3399FF);
			circlepager.setStrokeColor(0xFFCFCFCF);
			circlepager.setStrokeWidth(1 * density);
			
		
		
		
//		
//		circlepager.setRadius(6 * density);
//		circlepager.setPageColor(0xFFFFFFFF);
//		circlepager.setFillColor(0xFF3399FF);
//		circlepager.setStrokeColor(0xFF3399FF);
//		circlepager.setStrokeWidth(1 * density);

		adapter = new POIAdapter(getActivity(), R.layout.item_grid_square);
		gridView = (GridView) view.findViewById(R.id.gridViewHotelPOIs);

		gridView.setAdapter(adapter);
		gridView.setOnItemClickListener(this);

		return view;
	}

	@Override
	public void onClick(View v) {
		Category category = null;
		if (v.getTag() != null) {
			category = (Category) v.getTag();

			if (category.getKeyname() != null) {
				final String url = ResourceUtil.getServiceUrl(getActivity())
						+ getResources().getString(
								R.string.app_service_rest_pois);
				new RestCategoryServiceHotelListCommand(getActivity(),
						rois.get(positionRoiSelected), category, url) {

					protected void onPostExecute(java.util.List<Hotel> result) {
						adapter.setData(result);
						Log.d("PageChanged", "selected" + url);

						// Toast.makeText(getActivity(), result.size() +
						// " hotel has been return!",
						// Toast.LENGTH_SHORT).show();
					};
				}.execute();
			} else {
				roichangpager(rois.get(positionRoiSelected));// ��ͧ�����ҡѺ 0
																// ��ͧ��ҡѺ
																// position

			}

		}

	}

	// @Override
	// public void onAttach(Activity activity) {
	// super.onAttach(activity);
	// ((MainActivity) activity).onSectionAttached(roi ==
	// null?getString(R.string.app_name):roi.getName());
	// }

	/**
	 * A class use to create a fragment to put in each pager's screen
	 * 
	 * @author MEe
	 * 
	 */
	class ROIAdapter extends FragmentStatePagerAdapter {
		public ROIAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public int getCount() {
			return rois == null ? 0 : rois.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return rois.get(position).getName();
		}

		/**
		 * @see StringArray business_manager_pager
		 * 
		 */
		@Override
		public Fragment getItem(int position) {
			Fragment fragment = new ROIFragment(rois.get(position));

			return fragment;

		}
	}

	class PageChangeListner implements ViewPager.OnPageChangeListener {
		@Override
		public void onPageScrolled(int position, float positionOffset,
				int positionOffsetPixels) {

		}

		@Override
		public void onPageScrollStateChanged(int state) {
			Log.d("PageChanged", "state=" + state);
		}

		@Override
		public void onPageSelected(int position) {
			positionRoiSelected = position;
			// Toast.makeText(getActivity(),
			// "status = "+rois.get(position).getCategoryAt(position).getKeyname(),
			// Toast.LENGTH_LONG).show();

			categoryAdapter.setData(rois.get(position));
			rViewPager.setAdapter(categoryAdapter);

			// categoryAdapter.setData(rois.get(position));

			// Toast.makeText(getActivity(), "Cat count=" +
			// rois.get(position).sizeCategory(), Toast.LENGTH_SHORT).show();
			roichangpager(rois.get(position));

			Log.d("PageChanged", "selected -------------> " + position);
		}
	}

	public void roichangpager(ROI tab) {

		final String url = ResourceUtil.getServiceUrl(getActivity())
				+ getResources().getString(R.string.app_service_rest_pois);
		new RestServiceHotelListCommand(getActivity(), tab, url) {

			protected void onPostExecute(java.util.List<Hotel> result) {
				// adapter.setData(result.get(0));
				adapter.setData(result);
				Log.d("PageChanged", "selected" + url);

			};
		}.execute();

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		Intent intent = new Intent(getActivity(), HotelActivity.class);
		intent.putExtra(Hotel.class.getName(), adapter.getItem(position));

		startActivity(intent);
	}

}
