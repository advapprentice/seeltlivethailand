package com.touchtechnologies.tit.roi;


import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.Category;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.tit.R;

/**
 * Simple ROI adapter that create a simple image and title
 * @author Touch
 *
 */
public class ROICategoriesAdapter extends PagerAdapter {
	private ROI roi;
	private Context context;
	private int resLayout;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	int defaultItemBackground;

	TextView textcategories;
	ImageView imageView;
	CCTV lcctv;
	Bitmap bitmap;
	
	OnClickListener onClickListener;
	
	public ROICategoriesAdapter(Context context, OnClickListener onClickListener) {
		this.context = context;
		this.onClickListener = onClickListener;
	
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.NONE)
				.showImageForEmptyUri(R.drawable.bg_noimage)
				.showImageOnFail(R.drawable.bg_noimage)
				.cacheInMemory(true)
				.displayer(new FadeInBitmapDisplayer(200)) //fade in images
			  	.resetViewBeforeLoading(true).build();
				 imageLoader = ImageLoader.getInstance();  
				 
				 
	}

	public void setData(ROI roi){
		this.roi = roi;
		notifyDataSetChanged();
	}
	
	public int getCount() {
		return roi == null ? 0 : roi.sizeCategory();
	}
	
	public boolean isViewFromObject(View view, Object object) {
		ViewPager vp;
		
		return (view ==  object);		
	}
		
	
	public Category getItem(int position) {
		return roi.getCategoryAt(position);
	}


	public long getItemId(int position) {
		return position;
	}
	@Override
	public float getPageWidth(int position) {
	
	return (0.23f);
	}

	public Object instantiateItem(ViewGroup collection, int position) {
		View convertView = LayoutInflater.from(context).inflate(R.layout.item_categories, collection, false);
		convertView.setOnClickListener(onClickListener);
	
		convertView.setTag(roi.getCategoryAt(position));
		
		imageView = (ImageView)convertView.findViewById(R.id.imgcategories);
		textcategories  = (TextView)convertView.findViewById(R.id.textcategories);
		

		View viewcctv = (View)convertView.findViewById(R.id.viewcategories);
		viewcctv.setTag(position);
	
	
		textcategories.setText(roi.getCategoryAt(position).getCategoryname());
		
		String mountain = context.getString(R.string.image_categories)+"Mountain_over_40x.png";
		String resort = context.getString(R.string.image_categories)+"Hotel_over_40x.png";
		String beach = context.getString(R.string.image_categories)+"Beach_over_40x.png";
		String zoo = context.getString(R.string.image_categories)+"Zoo_Aguaria_over_40x.png";
		String monuments = context.getString(R.string.image_categories)+"Mounment_Temple_over_40x.png";
		String musuems = context.getString(R.string.image_categories)+"Museum_Art_Gallery_over_40x.png";
		String wedding = context.getString(R.string.image_categories)+"Wedding_Place_over_40x.png";
		String cities = context.getString(R.string.image_categories)+"City_Shopping_over_40x.png";
		String addAll = context.getString(R.string.image_categories)+"Category_All.png";
		String bikeroute = context.getString(R.string.image_categories)+"Bike_Route_over_40x.png";
		String fourmore = context.getString(R.string.image_categories)+"four-more_over_40x.png";
		
		
		
		if(roi.getCategoryAt(position).getId().equals("1")){
			imageLoader.displayImage(mountain, imageView, options);
		}else if(roi.getCategoryAt(position).getId().equals("2")){
			imageLoader.displayImage(beach, imageView, options);
		}else if(roi.getCategoryAt(position).getId().equals("3")){
			imageLoader.displayImage(resort, imageView, options);
		}else if(roi.getCategoryAt(position).getId().equals("4")){
		   imageLoader.displayImage(zoo, imageView, options);
		}else if(roi.getCategoryAt(position).getId().equals("5")){
			imageLoader.displayImage(monuments, imageView, options);
		}else if(roi.getCategoryAt(position).getId().equals("6")){
			imageLoader.displayImage(musuems, imageView, options);
		}else if(roi.getCategoryAt(position).getId().equals("9")){
			imageLoader.displayImage(wedding, imageView, options);
		}else if(roi.getCategoryAt(position).getId().equals("8")){
			imageLoader.displayImage(cities, imageView, options);
		}else if(roi.getCategoryAt(position).getId().equals("0")){
			imageLoader.displayImage(addAll, imageView, options);
		}else if(roi.getCategoryAt(position).getId().equals("13")){
			imageLoader.displayImage(bikeroute, imageView, options);
		}else if(roi.getCategoryAt(position).getId().equals("14")){
			imageLoader.displayImage(fourmore, imageView, options);
		}
		
		
	//	convertView.setBackgroundColor(Color.argb(255, position * 50, position * 10, position * 50));
		
//		imageLoader.displayImage(getItem(position).getUrl()+"&seed="+Math.random(), imageView, options);
		collection.addView(convertView,0);
		return convertView;
	}
	
	
	class ViewHolder{
		ImageView imageView;
		TextView textView;
	}
	
	@Override
	public void destroyItem(View collection, int position, Object view) {
		((ViewPager) collection).removeView((View)view);
		 
	}

 
}
