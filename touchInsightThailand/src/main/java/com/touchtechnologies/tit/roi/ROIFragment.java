package com.touchtechnologies.tit.roi;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.touchtechnologies.dataobject.ROI;
import com.touchtechnologies.tit.MainActivity;
import com.touchtechnologies.tit.R;
@SuppressLint("ValidFragment")
public class ROIFragment extends Fragment{
	private ROI roi;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private ImageView imageView;
	private TextView txtroi;
	public ROIFragment(){
		
	}
	
	public ROIFragment(ROI roi) {
		this.roi = roi;
		
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
				.showImageForEmptyUri(R.drawable.empty_cover)
				.showImageOnFail(R.drawable.empty_cover)
				.build();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(ROI.class.getName(), roi);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = null;
	
		try{
			if(roi == null & savedInstanceState != null){
				roi = (ROI)savedInstanceState.getSerializable(ROI.class.getName());
			}
			
			view = inflater.inflate(R.layout.fragment_roi, container, false);
			imageView = (ImageView)view.findViewById(R.id.imageViewROICover);
			txtroi =(TextView)view.findViewById(R.id.txtrois);
			
			txtroi.setText(roi.getName());
			imageLoader.displayImage(roi.getImageUrlCover(), imageView, options);
			
			
			
		}catch(NullPointerException ne){
			//can be removed if not crashed happend in POI info page
			Activity ac = getActivity();
			((MainActivity)ac).onNavigationDrawerItemSelected(0);
			
		}
		
		return view;
	}
}
