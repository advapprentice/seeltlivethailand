package com.touchtechnologies.tit;
import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;


public class LocationAwareFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener,
		LocationListener {

	/** A request location update interval */
	static final int UPDATE_INTERVAL = 30000;
	private static final int REQUEST_FINE_LOCATION=0;
	protected GoogleApiClient mGoogleApiClient;

	protected boolean mRequestingLocationUpdates = true;
	private boolean isLocationfused;
	protected Location mLastLocation;
	private LocationRequest mLocationRequest;
	protected Location location;

	@Override
	public void onStart() {
		super.onStart();
		loadPermissions(Manifest.permission.ACCESS_FINE_LOCATION,REQUEST_FINE_LOCATION);

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (mGoogleApiClient == null) {
			mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
					.addConnectionCallbacks(this)
					.addOnConnectionFailedListener(this)
					.addApi(LocationServices.API)
					.build();

		}
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		return null;
	}
	@Override
	public void onConnected(Bundle bundle) {

		mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

		if (mRequestingLocationUpdates) {
			startLocationUpdates();
		}
		location = mLastLocation;
	}
	@Override
	public void onResume() {
		super.onResume();

		if (mGoogleApiClient.isConnected() && !mRequestingLocationUpdates) {
			startLocationUpdates();
		}

	}
	protected void startLocationUpdates() {
		createLocationRequest();

		LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
		isLocationfused =true;
	}


	protected void createLocationRequest() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		mLocationRequest.setFastestInterval(UPDATE_INTERVAL/2);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
	}

	protected void stopLocationUpdates() {
		if(isLocationfused == true) {
			LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
			isLocationfused =false;
		}else {
			return;
		}
	}


	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {

	}
	
	@Override
	public void onDestroy() {

		super.onDestroy();
	}
	

	@Override
	public void onLocationChanged(Location location) {
		this.location = location;
		mLastLocation = location;
	}



	@Override
	public void onConnectionSuspended(int i) {

	}

	@Override
	public void onPause(){
		super.onPause();
		stopLocationUpdates();
	}

/**
	 * Called by Location Services when the request to connect the client
	 * finishes successfully.
	 */

private void loadPermissions(String perm,int requestCode) {
	if (ContextCompat.checkSelfPermission(getActivity(), perm) != PackageManager.PERMISSION_GRANTED) {
		if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), perm)) {
			ActivityCompat.requestPermissions(getActivity(), new String[]{perm},requestCode);
		}
	}else {
		mGoogleApiClient.connect();
	}
}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		//super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		switch (requestCode) {
			case REQUEST_FINE_LOCATION: {
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					// granted
					mGoogleApiClient.connect();
				}
				else{
					// no granted
					mLastLocation = null;
				}
				return;
			}


		}
	}
}
