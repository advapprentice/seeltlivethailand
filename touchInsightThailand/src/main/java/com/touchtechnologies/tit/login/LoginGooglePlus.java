package com.touchtechnologies.tit.login;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.touchtechnologies.command.insightthailand.UserExistCommand;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.UserExistAsynTask;
import com.touchtechnologies.tit.util.UserUtil;

public class LoginGooglePlus extends FragmentActivity implements ConnectionCallbacks, OnConnectionFailedListener{
	
	private static final int DIALOG_GET_GOOGLE_PLAY_SERVICES = 1;

    private static final int REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES = 2;
	// A magic number we will use to know that our sign-in error
	// resolution activity has completed.
	private static final int OUR_REQUEST_CODE = 49404;

	// The core Google+ client.
//	private PlusClient mPlusClient;

	// A flag to stop multiple dialogues appearing for the user.
	private boolean mResolveOnFail;

	// We can store the connection result from a failed connect()
	// attempt in order to make the application feel a bit more
	// responsive for the user.
	private ConnectionResult mConnectionResult;

	// A progress dialog to display when the user is connecting in
	// case there is a delay in any of the dialogs being ready.
	private ProgressDialog mConnectionProgressDialog;
	
	//-------------
	/* Track whether the sign-in button has been clicked so that we know to resolve
	 * all issues preventing sign-in without waiting.
	 */
	private boolean mSignInClicked;

	
	/* Request code used to invoke sign in user interactions. */
	private static final int RC_SIGN_IN = 0;

	  /* Client used to interact with Google APIs. */
	private GoogleApiClient mGoogleApiClient;

	  /* A flag indicating that a PendingIntent is in progress and prevents
	   * us from starting further intents.
	   */
	private boolean mIntentInProgress;
	//--------------------------

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// We pass through this for all three arguments, specifying the:
		// 1. Context
		// 2. Object to call onConnected and onDisconnected on
		// 3. Object to call onConnectionFailed on
		
		mGoogleApiClient = new GoogleApiClient.Builder(this)
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(Plus.API)
        .addScope(Plus.SCOPE_PLUS_LOGIN)
        .addScope(Plus.SCOPE_PLUS_PROFILE)
        .build();
		
		
		// We use mResolveOnFail as a flag to say whether we should trigger
		// the resolution of a connectionFailed ConnectionResult.
		mResolveOnFail = false;
		
		mSignInClicked = true;
		
		// Configure the ProgressDialog that will be shown if there is a
		// delay in presenting the user with the next sign in step.
		mConnectionProgressDialog = new ProgressDialog(this);
		mConnectionProgressDialog.setMessage(getString(R.string.wait));
		mConnectionProgressDialog.show();
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.v("LoginGooglePlus", "Start");
		// Every time we start we want to try to connect. If it
		// succeeds we'll get an onConnected() callback. If it
		// fails we'll get onConnectionFailed(), with a result!
		mGoogleApiClient.connect();
//		mPlusClient.connect();
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.v("LoginGooglePlus", "Stop");
		// It can be a little costly to keep the connection open
		// to Google Play Services, so each time our activity is
		// stopped we should disconnect.
		mGoogleApiClient.disconnect();
//		mPlusClient.disconnect();
	}
	
	@Override
	public void onConnectionSuspended(int cause) {
		mGoogleApiClient.connect();
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Log.v("LoginGooglePlus", "ConnectionFailed");
		if (!mIntentInProgress) {
		    // Store the ConnectionResult so that we can use it later when the user clicks
		    // 'sign-in'.
		    mConnectionResult = result;

		    if (mSignInClicked) {
		      // The user has already clicked 'sign-in' so we attempt to resolve all
		      // errors until the user is signed in, or they cancel.
		      resolveSignInError();
		    }
		  }
	
//		if (result.hasResolution()) {
//			mConnectionResult = result;
//			if (mResolveOnFail) {
//			
////				startResolution();
//			resolveSignInError();
//			}
//		}
		
		
	}
	
	

	@Override
	public void onConnected(Bundle bundle) {
		// Yay! We can get the oAuth 2.0 access token we are using.
		Log.v("LoginGooglePlus", "Google+ connected. Yay!");

		// Turn off the flag, so if the user signs out they'll have to
		// tap to sign in again.
		mResolveOnFail = false;

		// Hide the progress dialog if its showing.
		mConnectionProgressDialog.dismiss();
		
		
		//start main activity
        User user = new User();
//        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
		    Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
		    String personName = currentPerson.getDisplayName();
		    String personGooglePlusProfile = currentPerson.getUrl();
		    
		    Log.i("LoginGoogle+", "Profile " + personName + " " + personGooglePlusProfile );
	  
		    user.setId(currentPerson.getId());
		    user.setEmail(currentPerson.getId());
		    user.setSignupSource(User.SIGNUP_SOURCE_GOOGLEPLUS);
		    
		    UserUtil.persist(this, user);
		    
		    escalateToUserExist();
//        }
//	
//		escalateToUserExist();
		// Retrieve the oAuth 2.0 access token.
//		final Context context = this.getApplicationContext();
//		AsyncTask task = new AsyncTask() {
//			@Override
//			protected Object doInBackground(Object... params) {
//				String scope = "oauth2:" + Scopes.PLUS_LOGIN;
//				try {
//					// We can retrieve the token to check via
//					// tokeninfo or to pass to a service-side
//					// application.
//					String token = GoogleAuthUtil.getToken(context,
//							mPlusClient.getAccountName(), scope);
//				} catch (UserRecoverableAuthException e) {
//					// This error is recoverable, so we could fix this
//					// by displaying the intent to the user.
//					e.printStackTrace();
//				} catch (IOException e) {
//					e.printStackTrace();
//				} catch (GoogleAuthException e) {
//					e.printStackTrace();
//				}
//				return null;
//			}
//		};
//		task.execute((Void) null);
	}
	

	protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
		Log.v("LoginGooglePlus", "ActivityResult: " + requestCode);
		
		if(mConnectionProgressDialog.isShowing()){
        	mConnectionProgressDialog.dismiss();
        }
		
		if (requestCode == RC_SIGN_IN
                || requestCode == REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES) {
            mIntentInProgress = false; //Previous resolution intent no longer in progress.

            if (responseCode == RESULT_OK) {
                if (!mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting()) {
                    // Resolved a recoverable error, now try connect() again.
                    mGoogleApiClient.connect();
                }
                
            } else {
            	finish();
            	
                mSignInClicked = false; // No longer in the middle of resolving sign-in errors.
                if (responseCode == RESULT_CANCELED) {
                	Toast.makeText(this, "User canceled", Toast.LENGTH_LONG).show();
//                    mSignInStatus.setText(getString(R.string.signed_out_status));
                } else {
                	Toast.makeText(this, "Error " + responseCode, Toast.LENGTH_LONG).show();
//                    mSignInStatus.setText(getString(R.string.sign_in_error_status));
                }
            }
        }
	}
	
	/**
	 * Start UserExistAsynTask
	 */
	void escalateToUserExist(){
		//escalate to check use exist flow
	    UserExistCommand command = new UserExistCommand(this);
	    command.setUser(UserUtil.getUser(this));
	    
	    new UserExistAsynTask(this).execute(command);
	}

//	@Override
//	public void onClick(View view) {
//		switch (view.getId()) {
//		case R.id.btn_sign_in:
//			Log.v(TAG, "Tapped sign in");
//			if (!mPlusClient.isConnected()) {
//				// Show the dialog as we are now signing in.
//				mConnectionProgressDialog.show();
//				// Make sure that we will start the resolution (e.g. fire the
//				// intent and pop up a dialog for the user) for any errors
//				// that come in.
//				mResolveOnFail = true;
//				// We should always have a connection result ready to resolve,
//				// so we can start that process.
//				if (mConnectionResult != null) {
//					startResolution();
//				} else {
//					// If we don't have one though, we can start connect in
//					// order to retrieve one.
//					mPlusClient.connect();
//				}
//			}
//			break;
//		case R.id.btn_sign_out:
//			Log.v(TAG, "Tapped sign out");
//			// We only want to sign out if we're connected.
//			if (mPlusClient.isConnected()) {
//				// Clear the default account in order to allow the user
//				// to potentially choose a different account from the
//				// account chooser.
//				mPlusClient.clearDefaultAccount();
//
//				// Disconnect from Google Play Services, then reconnect in
//				// order to restart the process from scratch.
//				mPlusClient.disconnect();
//				mPlusClient.connect();
//
//				// Hide the sign out buttons, show the sign in button.
//				findViewById(R.id.btn_sign_in).setVisibility(View.VISIBLE);
//				findViewById(R.id.btn_sign_out)
//						.setVisibility(View.INVISIBLE);
//				findViewById(R.id.btn_revoke_access).setVisibility(
//						View.INVISIBLE);
//			}
//			break;
//		case R.id.btn_revoke_access:
//			Log.v(TAG, "Tapped disconnect");
//			if (mPlusClient.isConnected()) {
//				// Clear the default account as in the Sign Out.
//				mPlusClient.clearDefaultAccount();
//
//				// Go away and revoke access to this entire application.
//				// This will call back to onAccessRevoked when it is
//				// complete as it needs to go away to the Google
//				// authentication servers to revoke all token.
//				mPlusClient.revokeAccessAndDisconnect(this);
//			}
//			break;
//		default:
//			// Unknown id.
//		}
//	}

//	@Override
//	public void onAccessRevoked(ConnectionResult status) {
//		// mPlusClient is now disconnected and access has been revoked.
//		// We should now delete any data we need to comply with the
//		// developer properties. To reset ourselves to the original state,
//		// we should now connect again. We don't have to disconnect as that
//		// happens as part of the call.
//		mPlusClient.connect();
//
//		// Hide the sign out buttons, show the sign in button.
//		findViewById(R.id.btn_sign_in).setVisibility(View.VISIBLE);
//		findViewById(R.id.btn_sign_out).setVisibility(View.INVISIBLE);
//		findViewById(R.id.btn_revoke_access).setVisibility(View.INVISIBLE);
//	}
	
	/* A helper method to resolve the current ConnectionResult error. */
	private void resolveSignInError() {
		
			if (mConnectionResult.hasResolution()) {
				try {
					mIntentInProgress = true;
					startIntentSenderForResult(mConnectionResult.getResolution().getIntentSender(),
					          RC_SIGN_IN, null, 0, 0, 0);
				} catch (SendIntentException e) {
					mIntentInProgress = false;
					mGoogleApiClient.connect();
				}
			}
	}
	
	@Override
    protected Dialog onCreateDialog(int id) {
        if (id != DIALOG_GET_GOOGLE_PLAY_SERVICES) {
            return super.onCreateDialog(id);
        }

        int available = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (available == ConnectionResult.SUCCESS) {
            return null;
        }
        if (GooglePlayServicesUtil.isUserRecoverableError(available)) {
            return GooglePlayServicesUtil.getErrorDialog(
                    available, this, REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES);
        }
        return new AlertDialog.Builder(this)
                .setMessage("Oops!")
                .setCancelable(true)
                .create();
    }
		

			
//	/**
//	 * A helper method to flip the mResolveOnFail flag and start the resolution
//	 * of the ConnenctionResult from the failed connect() call.
//	 */
//	private void startResolution() {
//		try {
//			// Don't start another resolution now until we have a
//			// result from the activity we're about to start.
//			mResolveOnFail = false;
//			// If we can resolve the error, then call start resolution
//			// and pass it an integer tag we can use to track. This means
//			// that when we get the onActivityResult callback we'll know
//			// its from being started here.
//			mConnectionResult.startResolutionForResult(this, OUR_REQUEST_CODE);
//		} catch (SendIntentException e) {
//			// Any problems, just try to connect() again so we get a new
//			// ConnectionResult.
//			mGoogleApiClient.connect();
//		}
//	}
		
		
//		if(mConnectionResult == null){
//			mGoogleApiClient.connect();
//			
//		}else if (mConnectionResult.hasResolution()) {
//	    try {
//	      mIntentInProgress = true;
//	      startIntentSenderForResult(mConnectionResult.getResolution().getIntentSender(),
//	          RC_SIGN_IN, null, 0, 0, 0);
//	    } catch (SendIntentException e) {
//	      // The intent was canceled before it was sent.  Return to the default
//	      // state and attempt to connect to get an updated ConnectionResult.
//	      mIntentInProgress = false;
//	      mGoogleApiClient.connect();
//	    }
//	  }
	
//	/**
//	 * 
//	 * @author Touch
//	 */
//	class LoginExistTask extends CommonServiceTask {
//
//		public LoginExistTask(FragmentActivity context) {
//			super(context);
//		}
//		
//		
//		@Override
//		protected ServiceResponse doInBackground(Command... params) {
//			ServiceConnector srConnector = new ServiceConnector(ResourceUtil.getServiceUrl(context) + getString(R.string.app_service_login));
//			ServiceResponse serviceResponse = srConnector.doAsynPost(params[0],  true);
//
//			try {
//				int code = serviceResponse.getCode();
//				if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
//					JSONObject json = new JSONObject(serviceResponse.getContent().toString());
//					Log.e(AppConfig.LOG, "doPost login : "+json);
//					
//					//persist user session
//					User user = new User(json.getJSONObject("data"));
//					UserUtil.persist(context, user);
//					
//				}else{
//					Log.e(AppConfig.LOG, "doPost login failed :"+ code + serviceResponse.getMessage());
//				}
//				
//			} catch (Exception e) {
//				serviceResponse.setCode(500);
//				serviceResponse.setMessage(e.getMessage());
//				Log.e(AppConfig.LOG, "doInBackground error", e);
//			}
//
//			return serviceResponse;
//		}
//
//		@Override
//		protected void onPostExecute(ServiceResponse result) {
//			super.onPostExecute(result);
//
//			try {
//				if (result.getCode() == ServiceResponse.SUCCESS|| result.getCode() == HttpStatus.SC_OK) {
//					
//					JSONObject json = new JSONObject(result.getContent().toString());
//					
//					//persist user session
//					User user = new User(json.getJSONObject("data"));
//					UserUtil.persist(context, user);
//					
//					//launch main activity
//					Intent intent = new Intent(context, MainActivity.class);
//					startActivity(intent);
//					
//					context.setResult(Activity.RESULT_OK);
//					context.finish();
//					
//				} else {
//					
//					AlertDialog alertDialog = new Builder(LoginGooglePlus.this).create();
//					alertDialog.setTitle(result.getMessage());
//					alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
//						
//						@Override
//						public void onClick(DialogInterface dialog, int which) {
//							dialog.dismiss();
//						}
//					});
//					alertDialog.show();
//					
//				}
//			} catch (Exception e) {
//				Toast.makeText(context, "ERROR!\r\n" + e.getMessage(),
//						Toast.LENGTH_LONG).show();
//				Log.e(AppConfig.LOG, "process response error", e);
//			}
//		}
//	 }	
	
}
