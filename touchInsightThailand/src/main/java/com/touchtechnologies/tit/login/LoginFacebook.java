package com.touchtechnologies.tit.login;

import java.util.ArrayList;
import java.util.Arrays;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.telecom.Call;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;


import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.internal.GraphUtil;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.touchtechnologies.command.insightthailand.UserExistCommand;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.json.JSONUtil;
import com.touchtechnologies.tit.UserExistAsynTask;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginFacebook extends FragmentActivity {

	Context context;
    private CallbackManager callbackManager;
    private ProfileTracker profileTracker;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

            FacebookSdk.sdkInitialize(this);

        callbackManager = CallbackManager.Factory.create();
		String id = ResourceUtil.getFacebookID(getApplicationContext());
		Log.d(LoginFacebook.class.getSimpleName(), "onCreate: Facebook =" + id);
      LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
//                GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
//                    @Override
//                    public void onCompleted(JSONObject me, GraphResponse response) {
//                        if (response.getError() != null) {
//                            // handle error
//                        } else {
//                            String email = me.optString("email");
//                            String id = me.optString("id");
//							LoginManager.getInstance().logOut();
//							User titUser = UserUtil.getUser(LoginFacebook.this);
//							titUser.s
//                            // send email and id to your web server
//                        }
//                    }
//                }).executeAsync();
				User user = UserUtil.getUser(LoginFacebook.this);
				user.setSignupSource(User.SIGNUP_SOURCE_FACEBOOK);
				user.setToken(loginResult.getAccessToken().getToken());
				UserUtil.persist(LoginFacebook.this,user);
				callProflie(loginResult.getAccessToken());
				AccessToken.setCurrentAccessToken(loginResult.getAccessToken());
				//escalateToUserExist();
            }

            @Override
            public void onCancel() {
				Log.d("cancel", "onCancel: ");
				LoginManager.getInstance().logOut();
				//LoginManager.getInstance().logInWithReadPermissions(LoginFacebook.this, Arrays.asList("public_profile"));
                finish();

			}

            @Override
            public void onError(FacebookException error) {
				if (error instanceof FacebookAuthorizationException) {
					if (AccessToken.getCurrentAccessToken() != null) {
						LoginManager.getInstance().logOut();
					}
				}
				Log.d("error", "onError: "+error);

            }
        });
		LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));



	}

//	private void login(User user) {
//		user = UserUtil.getUser(LoginFacebook.this);
//		LoginCommand command = new LoginCommand(getApplicationContext());
//		command.setUser(user);
//
//		// execute login task
//		new LoginTask(this).execute(command);
//
//	}

	/**
	 * Start UserExistAsynTask
	 */
	void escalateToUserExist() {
		// escalate to check use exist flow
		UserExistCommand command = new UserExistCommand(this);
		command.setUser(UserUtil.getUser(this));

		new UserExistAsynTask(this).execute(command);
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

	}

	@Override
	public void onPause() {
		super.onPause();

	}
	public void callProflie(AccessToken accessToken){
		GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
			@Override
			public void onCompleted(JSONObject object, GraphResponse response) {
		//		Toast.makeText(LoginFacebook.this,object.toString(),Toast.LENGTH_LONG).show();
				User user = UserUtil.getUser(LoginFacebook.this);
				if(!object.isNull("id")){
					user.setId(JSONUtil.getString(object,"id"));
					UserUtil.persist(LoginFacebook.this,user);
				}


				escalateToUserExist();
			}
		});
		Bundle parameters = new Bundle();
		parameters.putString("fields","id,name,link,email");
		request.setParameters(parameters);
		request.executeAsync();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {

			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
