package com.touchtechnologies.tit.login;

import org.apache.http.HttpStatus;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.ForgotPasswordCommand;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.regex.PatternMatcher;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.util.ResourceUtil;

public class ForgotPasswordActivity extends SettingActionbarActivity implements OnClickListener{
	private EditText forgotpassword;
	private Button btnsubmit;
	private User user ;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forgot_password);
		mTitle = getString(R.string.forgot_password) ;

		MainApplication.getInstance().trackScreenView("Menu_ForgotPass");

		forgotpassword = (EditText)findViewById(R.id.edittextEmail);
		btnsubmit =(Button)findViewById(R.id.btnsubmitforgot);
		btnsubmit.setOnClickListener(this);
	
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnsubmitforgot:
			
			
			forgotPassword();
			break;

		}
		
	}


	User PutStreamConnection() {
		User stremconnect = new User();

		stremconnect.setEmail(forgotpassword.getText().toString());
		
		
		return stremconnect;
	}
	
	private void forgotPassword() {
	   	String error = null;
	 	if (PatternMatcher.isEmpty(forgotpassword)) {
     		error = getString(R.string.error_no_email);

	    }else if (!PatternMatcher.isEmpty(forgotpassword)
				&& !PatternMatcher.isValidEmail(forgotpassword.getText()
						.toString())) {
			error = getString(R.string.error_invalid_email);

		}
	    
				// display error for an empty input
			AlertDialog alertDialog = new Builder(this).create();
			if (error != null) {
				alertDialog.setTitle(error);
				alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				alertDialog.show();
				return;
			}


		ForgotPasswordCommand getCommand = new ForgotPasswordCommand(this);
		getCommand.setUser(PutStreamConnection());
		new ForgotServiceTask(this).execute(getCommand);

}

class ForgotServiceTask extends CommonServiceTask {

	public ForgotServiceTask(FragmentActivity context) {
		super(context, context.getString(R.string.email_sending), null, context.getString(R.string.btn_cancel));
		
	}
	
	
	@Override
	protected ServiceResponse doInBackground(Command... params) {
		ServiceConnector srConnector = new ServiceConnector(ResourceUtil.getServiceUrl(context)+getString(R.string.app_service_forgot_password));
		ServiceResponse serviceResponse = srConnector.doAsynPost(params[0],  false);
	
		
		return serviceResponse;
	}

	@Override
	protected void onPostExecute(ServiceResponse result) {
		super.onPostExecute(result);
		
		dialog.dismiss();
		try {
			if (result.getCode() == ServiceResponse.SUCCESS|| result.getCode() == HttpStatus.SC_OK) {
				context.setResult(Activity.RESULT_OK);
			    confirm();


			} else {
				Toast.makeText(context, getText(R.string.up_not_connectstream)+"!\r\n" + result.getMessage(),
						Toast.LENGTH_LONG).show();

			}
		} catch (Exception e) {
			Toast.makeText(context, "ERROR!\r\n" + e.getMessage(),
					Toast.LENGTH_LONG).show();
			Log.e(AppConfig.LOG, "process response error", e);
		}

	
}	
}

	private void confirm() {
		AlertDialog alertDialog = new Builder(this).create();
		
			alertDialog.setTitle("Message");
			alertDialog.setMessage("Thank you please check Email inbox");
			alertDialog.setIcon(R.drawable.ic_email_profile);
			alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					finish();
					dialog.dismiss();
				}
			});
			alertDialog.show();

	}

}

