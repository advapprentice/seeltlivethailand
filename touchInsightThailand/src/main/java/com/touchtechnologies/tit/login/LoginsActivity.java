package com.touchtechnologies.tit.login;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.touchtechnologies.command.insightthailand.LoginCommand;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.regex.PatternMatcher;
import com.touchtechnologies.tit.LoginAsynTask;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.util.UserUtil;
import com.touchtechnologies.wifi.WIFIMod;

public class LoginsActivity extends SettingActionbarActivity implements OnClickListener {

	int Orange = 0xFFFF9900;
	public static boolean mRunning = false;
	private EditText editTextUsername;
	private EditText editTextPassword;
	private View viewforgotpassword;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_logins);

		MainApplication.getInstance().trackScreenView("Menu_Login");

		mTitle = "Login";
		findViewById(R.id.buttonSigninFacebook).setOnClickListener(this);
		findViewById(R.id.buttonSigninGooglePlus).setOnClickListener(this);
		findViewById(R.id.btnlogin).setOnClickListener(this);
		findViewById(R.id.viewregister).setOnClickListener(this);

		editTextUsername = (EditText) findViewById(R.id.editTextUsername);
		editTextPassword = (EditText) findViewById(R.id.editTextPassword);
		viewforgotpassword = (View)findViewById(R.id.view_forgot_password);
		viewforgotpassword.setOnClickListener(this);
		
		User user = UserUtil.getUser(this);
		if(user != null){
			if(PatternMatcher.isValidEmail(user.getEmail())){
				editTextUsername.setText(user.getEmail());
			}
		}
	}

	// @Override
	// public void onConnected(Bundle connectionHint) {
	// mSignInClicked = false;
	// Toast.makeText(this, "User is connected!", Toast.LENGTH_SHORT).show();
	//
	// if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
	// Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
	// String personName = currentPerson.getDisplayName();
	// String personGooglePlusProfile = currentPerson.getUrl();
	// Log.i("LoginGoogle+", "Profile " + personName + " " +
	// personGooglePlusProfile );
	// }
	//
	// User user = new User();
	// user.setSignupSource(User.SIGNUP_SOURCE_GOOGLEPLUS);
	//
	// Intent intent = new Intent(this, UserProfileActivity.class);
	// intent.putExtra(User.class.getName(), user);
	// startActivity(intent);
	//
	// finish();
	//
	// }

	// public void onConnectionFailed(ConnectionResult result) {
	// if (!mIntentInProgress) {
	// // Store the ConnectionResult so that we can use it later when the
	// // user clicks
	// // 'sign-in'.
	// mConnectionResult = result;
	//
	// if (mSignInClicked) {
	// // The user has already clicked 'sign-in' so we attempt to
	// // resolve all
	// // errors until the user is signed in, or they cancel.
	// resolveSignInError();
	// }
	// }
	// }

	// @Override
	// public void onConnectionSuspended(int cause) {
	// mGoogleApiClient.connect();
	// }

	/**
	 * Check network connection status and display alert if not connect.
	 * 
	 * @return true if connected to Internet, false otherwise
	 */
	boolean isNetworkConnected() {
		boolean connected = WIFIMod.isNetworkConnected(getApplicationContext());
		if (!connected) {
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			// alertDialog.setTitle(this.getText(
			// R.string.error_no_bed_register));
			alertDialog.setMessage(this.getText(R.string.error_no_Wifi));

			alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,
					this.getText(R.string.up_Ok),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// context.finish();
							dialog.dismiss();
						}
					});
			alertDialog.show();
		}

		return connected;
	}

	@Override
	public void onClick(View v) {
		if (!isNetworkConnected()) {
			return;
		}
		String dimensionValue = "Android";
		Tracker t;
		Intent intent;
		switch (v.getId()) {
		case R.id.btnlogin:
			if (isValidLogin()) {
				// create user object from input
				User user = new User();
				user.setEmail(editTextUsername.getText().toString().trim());
				user.setPassword(editTextPassword.getText().toString().trim());

				// compose command
				LoginCommand command = new LoginCommand(this);
				command.setUser(user);

				// execute login task
				new LoginAsynTask(this).execute(command);
			}

			break;
		case R.id.buttonSigninFacebook:

			MainApplication.getInstance().trackScreenView("Menu_LoginFB");
			MainApplication.getInstance().trackEvent("Menu_LoginFB", "Menu_LoginFB", "Send event Menu_LoginFB");

			intent = new Intent(this, LoginFacebook.class);
			startActivity(intent);
			break;
		case R.id.viewregister:

			MainApplication.getInstance().trackScreenView("Menu_CreateAccount");
			MainApplication.getInstance().trackEvent("Menu_CreateAccount", "Menu_CreateAccount", "Send event Menu_CreateAccount");

			intent = new Intent(this, RegistersActivity.class);
			startActivity(intent);
			break;
		case R.id.view_forgot_password:

			intent = new Intent(this, ForgotPasswordActivity.class);
			startActivity(intent);
			break;
			
		case R.id.buttonSigninGooglePlus:
			intent = new Intent(this, LoginGooglePlus.class);
			startActivity(intent);

			//
			// if(!mGoogleApiClient.isConnecting() &&
			// !mGoogleApiClient.isConnected()) {
			// mGoogleApiClient.connect();
			//
			// int available =
			// GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
			// if (available != ConnectionResult.SUCCESS) {
			// showDialog(DIALOG_GET_GOOGLE_PLAY_SERVICES);
			// return;
			// }
			//
			// mSignInClicked = true;
			// // mSignInStatus.setText(getString(R.string.signing_in_status));
			// resolveSignInError();
			// }
			break;
		}
	}

	/**
	 * Check is a login field is valid. Such as it should not empty and filled
	 * all login fields. This method will show alert dialog if a fields value is
	 * not valid.
	 * 
	 * @return true if valid
	 */
	boolean isValidLogin() {
		boolean valid = true;

		String errorMessage = null;
		if (PatternMatcher.isEmpty(editTextUsername)) {
			errorMessage = getString(R.string.error_no_username);

		} else if (PatternMatcher.isEmpty(editTextPassword)) {
			errorMessage = getString(R.string.error_empty_password);

		}

		// display error for an empty input
		AlertDialog alertDialog = new Builder(this).create();
		if (errorMessage != null) {
			valid = false;
			alertDialog.setTitle(errorMessage);
			alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			alertDialog.show();
		}

		return valid;
	}

	@Override
	protected void onStart() {
		super.onStart();
		mRunning =true;
	}

	@Override
	protected void onStop() {
		super.onStop();
		mRunning =false;
	}
}
