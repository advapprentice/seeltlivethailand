package com.touchtechnologies.tit.login;

import java.util.Calendar;

import org.apache.http.HttpStatus;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.RegisterCommand;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.regex.PatternMatcher;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.Termofservice;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;
public class RegistersActivity extends SettingActionbarActivity implements OnClickListener {
	
	Calendar dateandtime;
	Bitmap bitmap;

	private TextView tvDisplayDate,tvtermofservice;
	private DatePicker dpResult;
	private Button btnChangeDate;

	private int year ;
	private int month;
	private int day;
	
	private User user;

	static final int DATE_DIALOG_ID = 999;
	EditText editTextEmail, editTextPassword, editTextconfirmPassword,editTextFirstName,editTextLastName,editTextBirtdate;
	Button btnregister;
	View date,viewback;

	ProgressDialog dialogs;
	CheckBox checkcondition;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mTitle = getText(R.string.up_title_register);		
		setContentView(R.layout.activity_registers);

		user = UserUtil.getUser(this);

		MainApplication.getInstance().trackScreenView("Menu_CreateAccount_Register");

		editTextEmail = (EditText)findViewById(R.id.email);
		editTextPassword = (EditText)findViewById(R.id.password);
		editTextconfirmPassword = (EditText)findViewById(R.id.confirmpassword);
		editTextFirstName = (EditText)findViewById(R.id.name);
		editTextLastName = (EditText)findViewById(R.id.lastname);
		viewback = (View)findViewById(R.id.viewback);
		checkcondition = (CheckBox) findViewById(R.id.checkcondition);
		findViewById(R.id.txtterm).setOnClickListener(this);
		
		
		btnregister = (Button)findViewById(R.id.btnlogin);
		
		btnregister.setOnClickListener(this);
		viewback.setOnClickListener(this);
		
		editTextBirtdate = (EditText) findViewById(R.id.birthdate);
		date = (View)findViewById(R.id.viewdate);
		dpResult = (DatePicker) findViewById(R.id.datePicker1);
		dpResult.setVisibility(View.GONE);
		dpResult.setMaxDate(System.currentTimeMillis());
		if(savedInstanceState != null){
		User user =	(User)savedInstanceState.getSerializable("save");
		setusertocurrentview(user);
		}
		
		addListenerOnButton();
	}
	
	
	/// set calandar birth date
	
	public void setCurrentDateOnView() {
		final Calendar c = Calendar.getInstance();
		
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);
		// set current date into textview

		dpResult.init(year, month, day, null);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		setCurrentDateOnView();
		
		if(user == null){
			return;
		}
		
		editTextEmail.setText(user.getEmail()==null?"":user.getEmail());
		editTextFirstName.setText(user.getFirstName()==null?"":user.getFirstName());
		editTextLastName.setText(user.getLastName()==null?"":user.getLastName());
		
	}

	public void addListenerOnButton() {

		// btnChangeDate = (Button) findViewById(R.id.btnChangeDate);
		editTextBirtdate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				showDialog(DATE_DIALOG_ID);

			}

		});
		date.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				showDialog(DATE_DIALOG_ID);

			}

		});

	}

	@Override
	protected Dialog onCreateDialog(int id) {

		
		switch (id) {
		case DATE_DIALOG_ID:
			
			// set date picker as current date
			DatePickerDialog datePickerDialog = new DatePickerDialog(this, datePickerListener, year, month, day);
			datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
			
			return datePickerDialog;
		}
		return null;
	}
	
	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			// set selected date into textview
			editTextBirtdate.setText(new StringBuilder().append(year).append("-")
					.append(month + 1).append("-").append(day).append(" "));

			// set selected date into datepicker also
			dpResult.init(year, month, day, null);

		}
	};
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
		
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.btnlogin:
			registerImpl();
			break;
		case R.id.viewback:
			finish();
			break;
		case R.id.txtterm:

			MainApplication.getInstance().trackScreenView("Menu_CreateAccount_ViewTerm");
			MainApplication.getInstance().trackEvent("Menu_CreateAccount_ViewTerm", "Menu_CreateAccount_ViewTerm", "Send eventMenu_CreateAccount_ViewTerm");
			Intent intent = new Intent(RegistersActivity.this,Termofservice.class);
			startActivity(intent);
			break;
		}
		
		
	}
	
	User getUserFromInput() {
		User user = this.user == null?new User():this.user;
		
		user.setEmail(editTextEmail.getText().toString().trim());
		user.setPassword(editTextPassword.getText().toString().trim());
		user.setPasswordconfrim(editTextconfirmPassword.getText().toString().trim());
		user.setFirstName(editTextFirstName.getText().toString().trim());
		user.setLastName(editTextLastName.getText().toString().trim());
		user.setBirthDate(editTextBirtdate.getText().toString().trim());
		if(user.getSignupSource() == null){
			user.setSignupSource(User.SIGNUP_SOURCE_SYSTEM);
		}
	
		return user;
	}
	 // validating password with retype password
    private boolean isValiduser(String lengths) {
        if (lengths != null && lengths.length() > 1) {
            return true;
        }
        return false;
    }
    
    
    public boolean isSpecialCharacter(String s){
        return s.matches("([\\&&[^#,@!+?;:'/%*&]])*");
      }
    
    public boolean isSpecialCharactername(String s){
        return s.matches("([\\w&&[^#]])*");
      }
    
    public boolean isSpecialnumber(String s){
        return s.matches("([\\w&&[^0-9]])*");	
 	 }
    
	 public boolean isSpecialUsernameTH (String s){
	       return s.matches("([\\w&&["+R.string.error_not_language_thai+"]])*");	
	}
//
//    public boolean isIntersection(String s){
//        return s.matches("([\\w&&[^0-9\-\d]])*");
//      }
//  
	/**
	 * Separated method for clean-read purpose. Register button click handler
	 */
	void registerImpl() {
		String error = null;
		// validate empty inputs
		
		String password = editTextPassword.getText().toString().trim();
		String confirmPassword = editTextconfirmPassword.getText().toString().trim();
		String firstname = editTextFirstName.getText().toString().trim();
     	String lastname = editTextLastName.getText().toString().trim();
	 
     	
     	if (PatternMatcher.isEmpty(editTextEmail)) {
     		error = getString(R.string.error_no_email);

	    }else if (!PatternMatcher.isEmpty(editTextEmail)
				&& !PatternMatcher.isValidEmail(editTextEmail.getText()
						.toString())) {
			error = getString(R.string.error_invalid_email);

		}else if (!PatternMatcher.isValidLength(editTextPassword,6)) {

			error = getString(R.string.error_no_password);

		} else if (PatternMatcher.isEmpty(editTextconfirmPassword)) {
			error = getString(R.string.error_no_confirm_password);
		
		}else if(!password.equals(confirmPassword)){
			error = getString(R.string.error_password_not_match);
			
		}else if (PatternMatcher.isEmpty(editTextFirstName)) {
			error = getString(R.string.error_no_firstname);
//			
		}else if (!isSpecialCharactername(firstname)) {
		    error = getString(R.string.error_noupchar_firstname);
////			
		}else if (!isValiduser(firstname)) {
	        error = getString(R.string.error_noup_firstname);
	        	
		}else if (!isSpecialnumber(firstname)) {
			error = getString(R.string.error_noupnumber_firstname);   	
	        	
//		        	
		} else if (PatternMatcher.isEmpty(editTextLastName)) {
			error = getString(R.string.error_no_lastname);
			
		}else if (!isSpecialCharactername(lastname)) {
        	error = getString(R.string.error_noupchar_lastname);	
			
					
		}else if (!isValiduser(lastname)) {
        	error = getString(R.string.error_noup_lastname);
        	
		}else if (!isSpecialnumber(lastname)) {
			error = getString(R.string.error_noupnumber_lastname);
		
//		}else if(PatternMatcher.isEmpty(editTextBirtdate)){
//			error = getString(R.string.error_no_date_of_birth);
//		
		}else if(!checkcondition.isChecked()){
			
			error = getString(R.string.error_not_checkterm);
		}
     	
     	
		
			// display error for an empty input
		AlertDialog alertDialog = new Builder(this).create();
		if (error != null) {
			alertDialog.setTitle(error);
			alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			alertDialog.show();
			return;
		}
		

	RegisterCommand command = new RegisterCommand(this);
	command.setUser(getUserFromInput());

	new RegisterTask(this).execute(command);
}


class RegisterTask extends CommonServiceTask {

	public RegisterTask(FragmentActivity context) {
		super(context);
		dialogs = ProgressDialog.show(RegistersActivity.this, "", 
                "Please wait...", true);
	}
	
	
	@Override
	protected ServiceResponse doInBackground(Command... params) {
		RegisterCommand command = (RegisterCommand)params[0];
		String url = ResourceUtil.getServiceUrl(context)+getString(R.string.app_service_register);
		User user = command.getUser();
		
		Log.d("Source", "="+user);
		
		switch(user.getSignupSource()){
		case User.SIGNUP_SOURCE_FACEBOOK:
			url += "/Facebook";
			break;
		case User.SIGNUP_SOURCE_GOOGLEPLUS:
			url += "/Google";
			break;
		}
		
		ServiceConnector srConnector = new ServiceConnector(url);
		ServiceResponse serviceResponse = srConnector.doPost(params[0],  true);
		try {
			int code = serviceResponse.getCode();
			if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
				
//				JSONObject json = new JSONObject(serviceResponse.getContent().toString());
//				user = new User(json.getJSONObject(null));
//				Log.e(AppConfig.LOG, "doPost :"+json);
				UserUtil.persist(context, user);
			
			}
		} catch (Exception e) {
			serviceResponse.setCode(HttpStatus.SC_EXPECTATION_FAILED);
			serviceResponse.setMessage(e.getMessage());
			Log.e(AppConfig.LOG, "doInBackground error", e);
		}

		return serviceResponse;
	}

	@Override
	protected void onPostExecute(ServiceResponse result) {
		super.onPostExecute(result);

	
		dialogs.dismiss();
		try {
			if (result.getCode() == ServiceResponse.SUCCESS || result.getCode() == HttpStatus.SC_OK) {
				

				context.setResult(Activity.RESULT_OK);
				context.finish();

			} else {
				Toast.makeText(context, result.getMessage(),Toast.LENGTH_LONG).show();
				comFirmregister(result);
			}
		} catch (Exception e) {
			Toast.makeText(context, "ERROR!\r\n" + e.getMessage(),Toast.LENGTH_LONG).show();
			Log.e(AppConfig.LOG, "process response error", e);
		}
	}
 }
	
private void setusertocurrentview(User user){
	if(!user.getBirthDate().isEmpty()){
		editTextBirtdate.setText(user.getBirthDate());
	}
	if(!user.getFirstName().isEmpty()){
		editTextFirstName.setText(user.getFirstName());
	}
	if(!user.getLastName().isEmpty()){
		editTextLastName.setText(user.getLastName());
	}
	if(!user.getEmail().isEmpty()){
		editTextEmail.setText(user.getEmail());
	}
	
}

private void comFirmregister(ServiceResponse result) {
	int status = result.getCode();
	AlertDialog alertDialog = new Builder(this).create();
	if(status==5){

		alertDialog.setTitle(result.getMessage());
		alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
			
				dialog.dismiss();
			}
		});
		alertDialog.show();
		return;
		
	}else{
		alertDialog.setTitle(result.getMessage());
		alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent  intent = new Intent(getApplicationContext(), LoginsActivity.class);
//				startActivity(intent);
				if(LoginsActivity.mRunning){
					startActivity(intent);
					finish();
				}
				else {
					intent =null;
					finish();
				}

				dialog.dismiss();
			}
		});
		alertDialog.show();
		return;
	}

	

}



@Override
protected void onSaveInstanceState(Bundle outState) {
	User user =  getUserFromInput();
	outState.putSerializable("save", user);
	super.onSaveInstanceState(outState);
}



}