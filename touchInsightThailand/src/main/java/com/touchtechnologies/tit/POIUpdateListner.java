package com.touchtechnologies.tit;

import com.touchtechnologies.dataobject.Hospitality;

public interface POIUpdateListner {
	void poiUpdated(Hospitality poi);
}
