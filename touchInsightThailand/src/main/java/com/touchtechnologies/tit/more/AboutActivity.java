package com.touchtechnologies.tit.more;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;

public class AboutActivity extends SettingActionbarActivity implements OnClickListener{
	private TextView txtversion,txtMore,txtAboutfull;
	private ProgressBar progress;
	private View  viewSendmail,viewWebsite,viewDevelop,viewHide,viewAboutFull;
	 @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_more_about);
		
		mTitle ="About";
		
		txtversion =(TextView)findViewById(R.id.txtversion);
		
		try{
			String version = "Version " + getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			txtversion.setText(version);
		}catch(Exception e){
			Log.e("Message :", e.getMessage());
		}
		
		txtAboutfull =(TextView)findViewById(R.id.txtAboutfull);
		txtMore =(TextView)findViewById(R.id.txtMore);
		viewHide = (View)findViewById(R.id.viewHide);
		viewAboutFull = (View)findViewById(R.id.viewAboutFull);
		
		
		viewSendmail = (View)findViewById(R.id.viewSendMail);
		viewWebsite = (View)findViewById(R.id.viewWebsite);
		viewDevelop = (View)findViewById(R.id.viewDevelop);
		viewSendmail.setOnClickListener(this);
		viewWebsite.setOnClickListener(this);
		viewDevelop.setOnClickListener(this);
		txtMore.setOnClickListener(this);
		viewAboutFull.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		Intent intent ;
		switch (v.getId()) {
		case R.id.viewSendMail:
			
			intent = new Intent(Intent.ACTION_SEND);
			intent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.email_support_touch)});      
			intent.putExtra(Intent.EXTRA_SUBJECT, "");
			intent.putExtra(Intent.EXTRA_TEXT, "");
			intent.setType("plain/text");
			startActivity(Intent.createChooser(intent, "Email Sending Option :"));
			
			break;
		case R.id.viewWebsite:
	        intent = new Intent(Intent.ACTION_VIEW,Uri.parse(getString(R.string.web_seeitlivethailand)));      
			startActivity(intent);
			
			break;
		case R.id.viewDevelop:
			intent = new Intent(Intent.ACTION_VIEW,Uri.parse(getString(R.string.web_touchtechnologies)));      
			startActivity(intent);
			break;
		case R.id.txtMore:
			viewAboutFull.setVisibility(View.VISIBLE);
			viewHide.setVisibility(View.GONE);
			break;
		case R.id.viewAboutFull:
			viewHide.setVisibility(View.VISIBLE);
			viewAboutFull.setVisibility(View.GONE);
			break;
			
			
		}
		
	}

	
}
