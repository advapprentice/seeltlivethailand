package com.touchtechnologies.tit.more;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.login.LoginManager;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.opensource.pagerindicator.CircleBitmapDisplayer;
import com.touchtechnologies.animate.dialog.MaterialDialog;
import com.touchtechnologies.command.insightthailand.RestServiceUserCommand;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.cctvitlive.LiveStreamPagerAdapter;
import com.touchtechnologies.tit.checkin.CheckInActivity;
import com.touchtechnologies.tit.hotline.Hotline_activity;
import com.touchtechnologies.tit.live.mylivestream.MyliveGridviewActivity;
import com.touchtechnologies.tit.login.LoginsActivity;
import com.touchtechnologies.tit.member.ListFollowingActivity;
import com.touchtechnologies.tit.search.MySingleton;
import com.touchtechnologies.tit.setting.MyProfileActivity;
import com.touchtechnologies.tit.setting.SettingActivity;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class FragmentMore extends Fragment implements OnClickListener{
	
	private View viewshareapp,viewmyaccount,viewlivestream,viewnotofication,viewcheckin,
			     viewfanpage,viewabout,viewlogin,viewnotlogin;
	private View  viewFollower,viewFollowing;
	private TextView txtname,txtFollower,txtFollowing;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private ImageView imageProfile;

	private final int request_userupdate = 2002;
	private MaterialDialog mMaterialDialog;
	Button loning,logout;
	User user ,getProfile;
	Context context;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		 
		imageLoader = ImageLoader.getInstance();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
	        .threadPoolSize(3)
			.threadPriority(Thread.NORM_PRIORITY - 2)
			.denyCacheImageMultipleSizesInMemory()
            .memoryCacheSize(4194304)
            .defaultDisplayImageOptions(options).build();
		
		imageLoader.init(config);
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.imageScaleType(ImageScaleType.NONE)
				.showImageForEmptyUri(R.drawable.ic_new_imgaccount)
				.showImageOnFail(R.drawable.ic_new_imgaccount)
				.showImageOnLoading(R.drawable.ic_new_imgaccount)
				.cacheInMemory(false)
				.displayer(new FadeInBitmapDisplayer(100)) 
			    .displayer(new CircleBitmapDisplayer())
				.build();
		  imageLoader = ImageLoader.getInstance();
		

	      user = UserUtil.getUser(getActivity());
		  View rootview = inflater.inflate(R.layout.fragment_mores, container, false);

		  MainApplication.getInstance().trackScreenView("Menu_Setting");



		  imageProfile=(ImageView)rootview.findViewById(R.id.imageProfile);
		  
		  txtname =(TextView)rootview.findViewById(R.id.txtusernmae);
		  txtFollower =(TextView)rootview.findViewById(R.id.txtFollower);
		  txtFollowing =(TextView)rootview.findViewById(R.id.txtFollowing);

		  viewlogin =(View)rootview.findViewById(R.id.view_login);
		  viewnotlogin =(View)rootview.findViewById(R.id.view_not_login);
		  viewmyaccount =(View)rootview.findViewById(R.id.view_myaccount);
		  viewlivestream =(View)rootview.findViewById(R.id.view_mylivestream);
		  viewnotofication =(View)rootview.findViewById(R.id.view_not_login);
		  viewcheckin =(View)rootview.findViewById(R.id.view_checkin);
		  viewfanpage =(View)rootview.findViewById(R.id.view_fanpage);
		  viewabout =(View)rootview.findViewById(R.id.view_setting);
		  viewshareapp =(View)rootview.findViewById(R.id.view_share);

		  viewFollower =(View)rootview.findViewById(R.id.viewFollower);
		  viewFollowing =(View)rootview.findViewById(R.id.viewFollowing);

		  loning =(Button)rootview.findViewById(R.id.btn_login);
		  logout =(Button)rootview.findViewById(R.id.buttonlogout);
		  
			rootview.findViewById(R.id.view_callservice).setOnClickListener(this);
			Matrix m = imageProfile.getImageMatrix();
			float imageHeight = 100;
			float imageWidth = 100;
			RectF drawableRect = new RectF(0, 0, imageWidth, imageHeight);
			RectF viewRect = new RectF(0, 0, imageProfile.getWidth(), imageProfile.getHeight());
			m.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.CENTER);
			
			imageProfile.setImageMatrix(m);
		  
		  
		  
		  if(UserUtil.isLoggedIn(getActivity())){
			imageLoader.displayImage(user.getAvatar(), imageProfile, options);
			viewlogin.setVisibility(View.VISIBLE);
			viewnotlogin.setVisibility(View.GONE);
		  }else{
			viewlogin.setVisibility(View.GONE);
			viewnotlogin.setVisibility(View.VISIBLE);
		  }
		  txtname.setText(user.getFirstName());
		
		  
		  logout.setOnClickListener(this);
		  viewmyaccount.setOnClickListener(this);
		  viewlivestream.setOnClickListener(this);
		  viewnotofication.setOnClickListener(this);
		  viewcheckin.setOnClickListener(this);
		  viewfanpage.setOnClickListener(this);
		  viewabout.setOnClickListener(this);
		  loning.setOnClickListener(this);
		  viewshareapp.setOnClickListener(this);

		  viewFollower.setOnClickListener(this);
		  viewFollowing.setOnClickListener(this);

		  updateFollow();


		return rootview;
	}
	public void updateFollow(){
		String url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_rest_user);
		new RestServiceUserCommand(getActivity(), user,url){

			protected void onPostExecute(User result ) {

				getProfile  = new User();
				getProfile	= result;
				txtFollowing.setText(""+getProfile.getFollowing());
				txtFollower.setText("" + getProfile.getFollower());


			}
		}.execute();


	}

	public void notifyDatasetChanged(){
		updateFollow();
//		Toast.makeText(getActivity(), "Update live stream list", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onResume() {
		super.onResume();
		user = UserUtil.getUser(getActivity());
		if(UserUtil.isLoggedIn(getActivity())){
			txtname.setText(user.getFirstName() );
			imageLoader.displayImage(user.getAvatar(), imageProfile, options);
			viewlogin.setVisibility(View.VISIBLE);
			viewnotlogin.setVisibility(View.GONE);
		}else{
			viewlogin.setVisibility(View.GONE);
			viewnotlogin.setVisibility(View.VISIBLE);
		}



	}


	@Override
	public void onClick(View v) {

		Intent intent;

		switch (v.getId()) {
		case R.id.viewFollower:
			intent = new Intent(getActivity(),ListFollowingActivity.class);
			intent.putExtra(User.class.getName(),user);
			intent.putExtra("key", "follower");
			intent.putExtra("key_title", "Followers");
			getActivity().startActivityForResult(intent, 5);

			break;
		case R.id.viewFollowing:
			intent = new Intent(getActivity(),ListFollowingActivity.class);
			intent.putExtra(User.class.getName(),user);
			intent.putExtra("key","following");
			intent.putExtra("key_title", "Following");
			getActivity().startActivityForResult(intent,5);

			break;
		case R.id.view_myaccount:

			MainApplication.getInstance().trackScreenView("Menu_Account");
			MainApplication.getInstance().trackEvent("Menu_Account", "Menu_Account", "Send event Menu_Account");

			  if(UserUtil.isLoggedIn(getActivity())){
					intent = new Intent(getActivity(), MyProfileActivity.class);
					startActivityForResult(intent,request_userupdate);

			  }else{
					intent = new Intent(getActivity(), LoginsActivity.class);
					startActivity(intent);
			  }


			break;
		case R.id.view_callservice:
			intent = new Intent(getActivity(), Hotline_activity.class);
			startActivity(intent);
			break;
		case R.id.view_setting:
			intent = new Intent(getActivity(), SettingActivity.class);
			startActivity(intent);
			
			break;
		case R.id.view_checkin:

			MainApplication.getInstance().trackScreenView("Menu_MyDestination");
			MainApplication.getInstance().trackEvent("Menu_MyDestination", "Menu_MyDestination", "Send event Menu_MyDestination");

			if (UserUtil.isLoggedIn(getActivity())) {
				intent = new Intent(getActivity(), CheckInActivity.class);
				startActivity(intent);
			} else {
				intent = new Intent(getActivity(), LoginsActivity.class);
				startActivity(intent);
			}
			break;
		case R.id.view_share:

			MainApplication.getInstance().trackScreenView("Menu_ShareSIL");
			MainApplication.getInstance().trackEvent("Menu_ShareSIL", "Menu_ShareSIL", "Send event Menu_ShareSIL");

			try {
				intent = new Intent();
				intent.setAction(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_TEXT, "http://play.google.com/store/apps/details?id=com.touchtechnologies.tit");
				startActivity(Intent.createChooser(intent, "Share with"));
			} catch (Exception e) {
				Log.d("ERROR", "Error");
			}
			break;
		case R.id.btn_login:

			MainApplication.getInstance().trackScreenView("Menu");
			MainApplication.getInstance().trackEvent("Menu", "Menu", "Send event Menu");

			intent = new Intent(getActivity(),LoginsActivity.class);
			startActivity(intent);
			
			
			break;
		case R.id.view_fanpage:

			MainApplication.getInstance().trackScreenView("Menu_ShareFP");
			MainApplication.getInstance().trackEvent("Menu_ShareFP", "Menu_ShareFP", "Send event Menu_ShareFP");

			try {
				Intent ifb = new Intent (Intent.ACTION_VIEW);
				String uri="fb://page/185175235001102";
				Uri.parse(uri);
				ifb.setData(Uri.parse(uri));
				startActivity(ifb);
			
			} catch (android.content.ActivityNotFoundException anfe) {
			    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/seeitlivethailand?fref=ts")));
			}
			
			break;
		case R.id.view_mylivestream:

			MainApplication.getInstance().trackScreenView("Menu_MyLiveStreaming");
			MainApplication.getInstance().trackEvent("Menu_MyLiveStreaming", "Menu_MyLiveStreaming", "Send event Menu_MyLiveStreaming");

			  if(UserUtil.isLoggedIn(getActivity())){
				  intent = new Intent(getActivity(),MyliveGridviewActivity.class);
				  startActivity(intent);
			  }else{
					intent = new Intent(getActivity(), LoginsActivity.class);
					startActivity(intent);
			  }
			
			break;
			
			
			
			
		case R.id.buttonlogout:

			MainApplication.getInstance().trackScreenView("Menu_Logout");
			MainApplication.getInstance().trackEvent("Menu_Logout", "Menu_Logout", "Send event Menu_Logout");
			mMaterialDialog = new MaterialDialog(getActivity());
			mMaterialDialog.setTitle("Log out")
					.setMessage("Are you sure you want to log out?")
							//mMaterialDialog.setBackgroundResource(R.drawable.background);
					.setPositiveButton("OK", new View.OnClickListener() {
						@Override public void onClick(View v) {
							mMaterialDialog.dismiss();
							gologout();
						}
					})
					.setNegativeButton("CANCEL",
							new View.OnClickListener() {
								@Override public void onClick(View v) {
									mMaterialDialog.dismiss();
									//	Toast.makeText(getActivity(),"Cancel", Toast.LENGTH_LONG).show();
								}
							})
					.setCanceledOnTouchOutside(true)
					.setOnDismissListener(
							new DialogInterface.OnDismissListener() {
								@Override
								public void onDismiss(DialogInterface dialog) {
									//	Toast.makeText(getActivity(),"onDismiss",Toast.LENGTH_SHORT).show();
								}
							})
					.show();

			
			break;

			default:
				break;
			
	
		}
	
	}

	private void commingsoon() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
	    alertDialogBuilder.setMessage("comming soon...");
	//    alertDialogBuilder.setIcon(R.drawable.icon_lucne);
	    alertDialogBuilder.setTitle("comming soon...");
	    alertDialogBuilder.setPositiveButton("Ok",
	        new DialogInterface.OnClickListener() {

	        @Override
	        public void onClick(DialogInterface dialog, int arg1) {
	        	dialog.dismiss();
	        }
	    });
	  

	    AlertDialog alertDialog = alertDialogBuilder.create();
	    alertDialog.show();
		
		

	}
	
	private void gologout(){
		
		final ProgressDialog dialogs = ProgressDialog.show(getActivity(), "",	"Please wait...", true);
		if(UserUtil.isLoggedIn(getActivity())){

		String url = ResourceUtil.getServiceUrl(getActivity())+getString(R.string.app_service_logout);
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Method.POST,url, null, new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject arg0) {
				Intent  intent;
				Log.d("", arg0.toString());
				UserUtil.clear(getActivity());
				LoginManager.getInstance().logOut();
				intent = new Intent(getActivity(),LoginsActivity.class);
				startActivity(intent);
			//	getActivity().finish();
				dialogs.dismiss();
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				Log.d("qweqwe", arg0.toString());
				
			}
		}){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String>  params = new HashMap<String, String>();  
				params.put("X-TIT-ACCESS-TOKEN", user.getToken());
				return params;
			}

			@Override
			public String getBodyContentType() {
				return "appication/json";
			}
			
			
			
		};
		MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
		
		}
	}
		
//	private void getuserupdate(){
//
//	}



	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		//super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == 2002  && requestCode == getActivity().RESULT_OK){
			onResume();

		}


	}
}
