package com.touchtechnologies.tit.setting;


import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.opensource.pagerindicator.CircleBitmapDisplayer;
import com.touchtechnologies.animate.button.CircularProgressButton;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.RegisterCommand;
import com.touchtechnologies.command.insightthailand.RestServiceUserCommand;
import com.touchtechnologies.command.insightthailand.UploadPhotoCommand;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.graphics.BitmapUtil;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

public class MyProfileActivity extends SettingActionbarActivity implements OnClickListener{
	static final int DATE_DIALOG_ID = 999;
	static final int REQUEST_CODE_PICK = 765;
	static final int REQUEST_CODE_CAMERA = 75;
	private View profile,date,viewEditPhoto;
	private User user;
	private ImageView imgProfile;
	private EditText name, email,birthday,lastname,zipcode,address;
	private DatePicker dpResult;
	private int year ;
	private int month;
	private int day;
	private Spinner spinnerSex;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private CircularProgressButton circularButton;
	private String imgPath;
	private Spinner mSpinner;
	private ArrayAdapter<String> adaptercountry;
	ProgressDialog  progress;
	Bitmap bitmap;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);

		MainApplication.getInstance().trackScreenView("Menu_Account_Update");

		imageLoader = ImageLoader.getInstance();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
				.threadPoolSize(3)
				.threadPriority(Thread.NORM_PRIORITY - 2)
				.denyCacheImageMultipleSizesInMemory()
				.memoryCacheSize(4194304)
				.memoryCache(new WeakMemoryCache())
				.defaultDisplayImageOptions(options).build();

		imageLoader.init(config);
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.imageScaleType(ImageScaleType.NONE)
				.showImageForEmptyUri(R.drawable.ic_new_imgaccount)
				.showImageOnFail(R.drawable.ic_new_imgaccount)
			//	.showImageOnLoading(R.drawable.bg_loading_circle)
				.cacheInMemory(true)
				.displayer(new FadeInBitmapDisplayer(1000))
						//	.displayer(new RoundedBitmapDisplayer((int) 125.5f))
				.displayer(new CircleBitmapDisplayer())
				.build();


		user = UserUtil.getUser(this);
		mTitle = getText(R.string.up_title_profile);
		name = (EditText) findViewById(R.id.editname);
		email = (EditText) findViewById(R.id.editemail);
		birthday = (EditText) findViewById(R.id.editbirtdate);
		lastname = (EditText) findViewById(R.id.editTextLastname);
		viewEditPhoto = (View) findViewById(R.id.viewEditPhoto);
		imgProfile= (ImageView) findViewById(R.id.imgprofile);
		zipcode = (EditText)findViewById(R.id.edit_Zipcode);
		address = (EditText)findViewById(R.id.edit_address);
		circularButton = (CircularProgressButton) findViewById(R.id.btnUpdate);

		circularButton.setOnClickListener(this);
		viewEditPhoto.setOnClickListener(this);


		date = (View) findViewById(R.id.viewdate);
		dpResult = (DatePicker) findViewById(R.id.datePicker1);
		dpResult.setVisibility(View.GONE);
		dpResult.setMaxDate(System.currentTimeMillis());


		Matrix m = imgProfile.getImageMatrix();
		float imageHeight = 100;
		float imageWidth = 100;
		RectF drawableRect = new RectF(0, 0, imageWidth, imageHeight);
		RectF viewRect = new RectF(0, 0, imgProfile.getWidth(), imgProfile.getHeight());
		m.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.CENTER);

		imgProfile.setImageMatrix(m);
		// sex spinner
		mSpinner = (Spinner) findViewById(R.id.spinner_country);
		spinnerSex = (Spinner) findViewById(R.id.spinnerSex);
		ArrayAdapter<String> adapterSex = new ArrayAdapter<String>(this,
				R.layout.item_simple_text, R.id.textViewSpinnerItem,
				getResources().getStringArray(R.array.sex));
		spinnerSex.setAdapter(adapterSex);
		adaptercountry = new ArrayAdapter<>(this, R.layout.item_simple_text,R.id.textViewSpinnerItem,
				getResources().getStringArray(R.array.array_country));
		mSpinner.setAdapter(adaptercountry);

		findViewById(R.id.btnUpdate).setOnClickListener(this);
		String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_user);
		new RestServiceUserCommand(this, user,url){

			protected void onPostExecute(User result ) {

				User user = result;
				userUpdate(user);
			}
		}.execute();
		addListenerOnButton() ;

	}

	@Override
	public void onClick(View v) {
		Intent intent;

		switch (v.getId()) {

			case R.id.btnUpdate:
/*			if (UserUtil.isLoggedIn(getApplicationContext())) {
				intent = new Intent(getApplicationContext(), CheckInActivity.class);
				startActivity(intent);
			} else {
				intent = new Intent(getApplicationContext(), LoginsActivity.class);
				startActivity(intent);
			}
*/

				if (circularButton.getProgress() == 0) {
					if(imgPath!=null && imgPath != ""){
						upDatedata();
						upDatePhoto();
					}else {
						upDatedata();
					}
					circularButton.setIndeterminateProgressMode(true);
				} else {
					circularButton.setProgress(0);
				}



				break;
			case R.id.viewEditPhoto:

				final AlertDialog alertDialog = new Builder(this).create();
				View view = getLayoutInflater().inflate(
						R.layout.dialog_picture_chooser, null, false);
				alertDialog.setView(view);

				view.findViewById(R.id.cancel).setOnClickListener(
						new OnClickListener() {

							@Override
							public void onClick(View v) {
								alertDialog.dismiss();

							}
						});

				view.findViewById(R.id.viewPictGallery).setOnClickListener(
						new OnClickListener() {

							@Override
							public void onClick(View v) {
								Intent intent = new Intent(Intent.ACTION_PICK);
								intent.setType("image/*");
								startActivityForResult(intent, REQUEST_CODE_PICK);
								alertDialog.cancel();
							}
						});
				view.findViewById(R.id.viewPictCamera).setOnClickListener(
						new OnClickListener() {

							@Override
							public void onClick(View v) {

								Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
								startActivityForResult(intent, REQUEST_CODE_CAMERA);
								alertDialog.cancel();
							}
						});

				alertDialog.show();


				break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
		if(resultCode == RESULT_OK ) {
			Uri uri = data.getData();
			final Uri uri2 = data.getData();
			try{
				//get image info
//					Matrix matrix = new Matrix();
//					matrix.
				int outHeigth = 400;
				int outWidth  = (int)( outHeigth / BitmapUtil.getAspectRatio(getApplicationContext(), uri));


				bitmap = BitmapUtil.getBitmap(this, uri, outWidth, outHeigth);
				String[] filePathColumn = {MediaStore.Images.Media.DATA};

				Cursor cursor = getContentResolver().query(
						uri, filePathColumn, null, null, null);
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				imgPath	= cursor.getString(columnIndex);
				ExifInterface ei = new ExifInterface(imgPath);
				int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

				switch(orientation) {
					case ExifInterface.ORIENTATION_ROTATE_90:
						bitmap = RotateBitmap(bitmap, 0);
						Savebitmaptoimagepath(bitmap, imgPath);
						break;
					case ExifInterface.ORIENTATION_ROTATE_180:
						bitmap =   RotateBitmap(bitmap, 0);
						Savebitmaptoimagepath(bitmap, imgPath);
						break;
					case ExifInterface.ORIENTATION_ROTATE_270:
						bitmap =   RotateBitmap(bitmap, 0);
						Savebitmaptoimagepath(bitmap, imgPath);
						break;

				}
				cursor.close();
				//imgPath = data.toString();


			}catch(Exception e){
				Log.e(AppConfig.LOG, "get bitmap error", e);
			}
			if (resultCode == RESULT_OK && (requestCode == REQUEST_CODE_PICK)){

				imgProfile.post(new Runnable() {

					@Override
					public void run() {
						//imgProfile.setImageBitmap(bitmap);


						imageLoader.displayImage(uri2.toString(), imgProfile, options);



					}
				});

			}else {

				imgProfile.post(new Runnable() {

					@Override
					public void run() {


						//imgProfile.setImageBitmap(bitmap);
						imageLoader.displayImage(uri2.toString(), imgProfile, options);
						//    Canvas canvas = new Canvas(bitMap);
					}

				});
			}
		}
	}

	private void userUpdate(User user) {



		if(user == null){
			return;
		}

		Log.d("Test","TEST"+user.getFirstName());
		name.setText(user.getFirstName());
		email.setText(user.getEmail()== null?"":user.getEmail());
		if(user.getBirthDate() != null){
			if(!user.getBirthDate().isEmpty()){
				birthday.setText(user.getBirthDate());
			}else {
				setCurrentDateOnView();
			}
		}else {
			setCurrentDateOnView();
		}
		lastname.setText(user.getLastName()== null?"":user.getLastName());
		spinnerSex.setSelection(User.GENDER_MALE.equals(user.getGender())?1:2);
		zipcode.setText(user.getZipcode()==null?"":user.getZipcode());
		address.setText(user.getAddress()==null?"":user.getAddress());
		imageLoader.displayImage(user.getAvatar(), imgProfile, options);
		String compareValue =null;
		if(user.getCountry() != null){
			compareValue = user.getCountry();
			if (!compareValue.equals(null)) {
				int spinnerPosition = adaptercountry.getPosition(compareValue);
				mSpinner.setSelection(spinnerPosition);
			}
		}else{
			mSpinner.setSelection(0);
		}




	}

	public void setCurrentDateOnView() {
		final Calendar c = Calendar.getInstance(Locale.US);

		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);
		// set current date into textview
		if(user.getBirthDate() == null)
			birthday.setText(new StringBuilder().append(year-10).append("-").append(month + 1).append("-").append(day).append(" "));
		dpResult.init(year, month, day, null);
	}

	public void addListenerOnButton() {

		// btnChangeDate = (Button) findViewById(R.id.btnChangeDate);
		birthday.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				showDialog(DATE_DIALOG_ID);

			}

		});
		date.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				showDialog(DATE_DIALOG_ID);

			}

		});

	}

	@Override
	protected Dialog onCreateDialog(int id) {


		switch (id) {
			case DATE_DIALOG_ID:

				// set date picker as current date
				DatePickerDialog datePickerDialog = new DatePickerDialog(this, datePickerListener, year, month, day);
				datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

				return datePickerDialog;
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {


		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
							  int selectedMonth, int selectedDay) {
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			// set selected date into textview
			birthday.setText(new StringBuilder().append(year).append("-")
					.append(month + 1).append("-").append(day).append(" "));

			// set selected date into datepicker also
			dpResult.init(year, month, day, null);

		}
	};


	User getUserFromInput() {
		User user = this.user == null?new User():this.user;
		switch (spinnerSex.getSelectedItemPosition()) {
			case 1:
				user.setGender(User.GENDER_MALE);
				break;
			case 2:
				user.setGender(User.GENDER_FEMALE);
				break;
		}
		String country  = mSpinner.getSelectedItem().toString().trim();
		user.setEmail(email.getText().toString().trim());
		user.setFirstName(name.getText().toString().trim());
		user.setLastName(lastname.getText().toString().trim());
		user.setBirthDate(birthday.getText().toString().trim());
		if(!adaptercountry.getItem(0).equals(country))
			user.setCountry(country);
		user.setAddress(address.getText().toString().trim());
		user.setZipcode(zipcode.getText().toString().trim());
		return user;
	}

	User getUploadPic() {
		User userProfile = this.user == null?new User():this.user;
		user.setAvatar(imgPath);

		return userProfile;
	}

	private void upDatedata() {
		RegisterCommand command = new RegisterCommand(this);
		command.setUser(getUserFromInput());
		new RegisterTask(this).execute(command);


	}

	private void upDatePhoto() {

		UploadPhotoCommand command2 = new UploadPhotoCommand(this);
		command2.setUser(getUploadPic());
		new uploadpropic(this).execute(command2);

	}

	class RegisterTask extends CommonServiceTask {

		public RegisterTask(FragmentActivity context) {
			super(context);
			progress = ProgressDialog.show(MyProfileActivity.this, "","Please wait...", true);
		}


		@SuppressWarnings("deprecation")
		@Override
		protected ServiceResponse doInBackground(Command... params) {
			User us = UserUtil.getUser(getApplicationContext());
			RegisterCommand command = (RegisterCommand)params[0];
			String url = ResourceUtil.getServiceUrl(context)+getString(R.string.app_service_update_user)+user.getId();
			User users = command.getUser();
			Log.d("Source", "="+users); Log.d("Source", "URL :"+url);
			ServiceConnector srConnector = new ServiceConnector(url);

			List<NameValuePair> headers = new ArrayList<NameValuePair>();
			headers.add(new BasicNameValuePair("Content-Type", "application/json"));
			headers.add(new BasicNameValuePair("X-TIT-ACCESS-TOKEN", us.getToken()));

			ServiceResponse serviceResponse = srConnector.doAsynPut(params[0],true,headers);
			Log.d("headers", "URL :"+headers);

			try {
				int code = serviceResponse.getCode();
				if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
					JSONObject objuser = new JSONObject(serviceResponse.getContent().toString());

					//user = new User(objuser);
					//Log.d("USER", "URL :"+user);

					//UserUtil.persist(getApplicationContext(), user);
					SharedPreferences sharedPreferences =  context.getSharedPreferences(AppConfig.PREFERENCE_USER, Context.MODE_PRIVATE);
					//sharedPreferences.edit().putString(User.class.getName(), user.toString()).commit();
					try {
						String userJson = sharedPreferences.getString(User.class.getName(),  null);
						JSONObject obj = new JSONObject(userJson);
						obj.remove("first_name");
						obj.put("first_name", objuser.get("first_name").toString());
						obj.remove("last_name");
						obj.put("last_name", objuser.get("last_name").toString());
						obj.remove("birth_date");
						obj.put("birth_date", objuser.get("birth_date").toString());
						obj.remove("gender");
						obj.put("gender",objuser.get("gender").toString());
						obj.remove("country");
						obj.put("country", objuser.get("country").toString());
						obj.remove("zipcode");
						obj.put("zipcode", objuser.get("zipcode").toString());
						obj.remove("address");
						obj.put("address", objuser.get("address").toString());
						user = new User(obj);
						UserUtil.persist(context, user);
						// Log.d("USER", "URL :"+user);

					} catch (Throwable t) {
						Log.e("fixjson", "canconvert");
					}


				}
			} catch (Exception e) {

				serviceResponse.setMessage(e.getMessage());
				Log.e(AppConfig.LOG, "doInBackground error", e);
			}

			return serviceResponse;
		}



		@Override
		protected void onPostExecute(ServiceResponse result) {
			super.onPostExecute(result);

			progress.dismiss();
			try {
				if (result.getCode() == ServiceResponse.SUCCESS|| result.getCode() == HttpStatus.SC_OK) {
					//	Toast.makeText(context, "Success", Toast.LENGTH_LONG).show();
					simulateSuccessProgress(circularButton);
					context.setResult(Activity.RESULT_OK);


				} else {
					simulateErrorProgress(circularButton);
					AlertDialog.Builder mBuilder = new Builder(MyProfileActivity.this)
							.setTitle("Update Fail")
							.setMessage(result.getMessage())
							.setPositiveButton("OK", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.dismiss();
						}
					});
					AlertDialog mAlertDialog = mBuilder.create();
					mAlertDialog.show();

				}
			} catch (Exception e) {
				Toast.makeText(context, "ERROR!\r\n" + e.getMessage(),Toast.LENGTH_LONG).show();
				Log.e(AppConfig.LOG, "process response error", e);
			}
		}
	}

	class uploadpropic extends CommonServiceTask{

		public uploadpropic(FragmentActivity context) {
			super(context);
			// TODO Auto-generated constructor stub
		}
		@SuppressWarnings("deprecation")
		@Override
		protected ServiceResponse doInBackground(Command... params) {
			// TODO Auto-generated method stub
			UploadPhotoCommand command = (UploadPhotoCommand)params[0];
			String url = ResourceUtil.getServiceUrl(context)+getString(R.string.app_service_update_user)+user.getId()+"/profile";
			User userphoto = UserUtil.getUser(getApplicationContext());

			//Log.d("Response", "="+users); Log.d("Source", "URL :"+url);
			ServiceConnector srConnector = new ServiceConnector(url);
			List<NameValuePair> headers = new ArrayList<NameValuePair>();
			headers.add(new BasicNameValuePair("X-TIT-ACCESS-TOKEN", userphoto.getToken()));


			//String str = user.getAvatar();
			ServiceResponse serviceResponse = srConnector.doUploadpic(command, headers);
			Log.d("headers", "URL :"+headers);

			try {
				int code = serviceResponse.getCode();
				if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
					JSONObject objuser = new JSONObject(serviceResponse.getContent().toString());
					SharedPreferences sharedPreferences =  context.getSharedPreferences(AppConfig.PREFERENCE_USER, Context.MODE_PRIVATE);
					String userJson = sharedPreferences.getString(User.class.getName(),  null);
					JSONObject obj = new JSONObject(userJson);
					obj.remove("profile_picture");
					obj.put("profile_picture", objuser.get("profile_picture").toString());
					user = new User(obj);
					UserUtil.persist(context,user);
					Log.d("USER", "URL :"+user);

				}
			} catch (Exception e) {

				serviceResponse.setMessage(e.getMessage());
				Log.e(AppConfig.LOG, "doInBackground error", e);
			}

			return serviceResponse;
		}

		@Override
		protected void onPostExecute(ServiceResponse result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progress.dismiss();
			try {
				if (result.getCode() == ServiceResponse.SUCCESS|| result.getCode() == HttpStatus.SC_OK) {
					//	Toast.makeText(context, "Success", Toast.LENGTH_LONG).show();
					simulateSuccessProgress(circularButton);
					context.setResult(Activity.RESULT_OK);


				} else {
					Toast.makeText(context,"Fail"+"!\r\n" + result.getMessage(),
							Toast.LENGTH_LONG).show();
					simulateErrorProgress(circularButton);
				}
			} catch (Exception e) {
				Toast.makeText(context, "ERROR!\r\n" + e.getMessage(),Toast.LENGTH_LONG).show();
				Log.e(AppConfig.LOG, "process response error", e);
			}
		}

	}

	private void simulateSuccessProgress(final CircularProgressButton button) {
		ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 100);
		widthAnimation.setDuration(1500);
		widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
		widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				Integer value = (Integer) animation.getAnimatedValue();
				button.setProgress(value);
			}
		});
		widthAnimation.start();
	}

	private void simulateErrorProgress(final CircularProgressButton button) {
		ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 99);
		widthAnimation.setDuration(1500);
		widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
		widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				Integer value = (Integer) animation.getAnimatedValue();
				button.setProgress(value);
				if (value == 99) {
					button.setProgress(-1);
				}
			}
		});
		widthAnimation.start();
	}

	public  Bitmap RotateBitmap(Bitmap source, float angle)
	{
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
	}

	public void Savebitmaptoimagepath(Bitmap bitmap,String imgpath){
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(imgPath);
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
			// PNG is a lossless format, the compression factor (100) is ignored
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		//return super.onOptionsItemSelected(item);
		if (item.getItemId() == android.R.id.home) {
			Intent intent = new Intent();
			intent.putExtra(MyProfileActivity.class.getSimpleName(),user);
			setResult(RESULT_OK,intent);
			finish();
			return true;
		}
		return true;
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent();
		intent.putExtra(MyProfileActivity.class.getSimpleName(),user);
		setResult(RESULT_OK,intent);
		finish();
	}
}

