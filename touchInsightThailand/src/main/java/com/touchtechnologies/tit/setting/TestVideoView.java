package com.touchtechnologies.tit.setting;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

import com.touchtechnologies.animate.videoview.TextureVideoView;
import com.touchtechnologies.tit.R;

import net.majorkernelpanic.streaming.video.VideoStream;


/**
 * Created by TouchICS on 3/7/2016.
 */
public class TestVideoView extends Activity  {
    private MediaPlayer mediaPlayer;
    private SurfaceHolder vidHolder;
    private SurfaceView vidSurface;
    private TextureVideoView mVideoView1,mVideoView2,mVideoView3,mVideoView4;
    String vidAddress = "rtsp://192.168.8.111:1935/vod/sample.mp4";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.item_video_view);
        Uri uri = Uri.parse(vidAddress); //Declare your url here.

        mVideoView1  = (TextureVideoView)findViewById(R.id.myVideo1);
        mVideoView1.setVideoURI(uri);
        mVideoView1.requestFocus();

        mVideoView2  = (TextureVideoView)findViewById(R.id.myVideo2);
        mVideoView2.setVideoURI(uri);
        mVideoView2.requestFocus();

        mVideoView3  = (TextureVideoView)findViewById(R.id.videoView3);
        mVideoView3.setVideoURI(uri);
        mVideoView3.requestFocus();

        mVideoView4  = (TextureVideoView)findViewById(R.id.videoView4);
        mVideoView4.setVideoURI(uri);
        mVideoView4.requestFocus();
        initVideoView();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        mVideoView1.stopPlayback();
        mVideoView2.stopPlayback();
        mVideoView3.stopPlayback();
        mVideoView4.stopPlayback();

    }

    private void initVideoView() {
        mVideoView1.setVideoPath(vidAddress);

        mVideoView2.setVideoPath(vidAddress);

        mVideoView3.setVideoPath(vidAddress);

        mVideoView4.setVideoPath(vidAddress);

        startVideoPlayback();
        startVideoAnimation();
    }

    private void startVideoPlayback() {
        // "forces" anti-aliasing - but increases time for taking frames - so keep it disabled
        // mVideoView.setScaleX(1.00001f);
        mVideoView1.start();
        mVideoView2.start();
        mVideoView3.start();
        mVideoView4.start();
    }

    private void startVideoAnimation() {

        mVideoView1.setRotation(0);
        mVideoView2.setRotation(270);
        mVideoView3.setRotation(90);
        mVideoView4.setRotation(180);

    }


}
