package com.touchtechnologies.tit.setting;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.touchtechnologies.app.LibConfig;
import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.BaseInsightCommand;
import com.touchtechnologies.command.insightthailand.RequestLivestreamCommand;
import com.touchtechnologies.dataobject.insightthailand.LiveStreamConnection;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.telephony.TelephonyInfo;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.more.AboutActivity;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;
import com.touchtechnologies.wifi.WIFIMod;

import net.majorkernelpanic.streaming.video.VideoStream;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SettingActivity extends SettingActionbarActivity implements OnClickListener {

	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API.
	 * See https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	private int clickcount = 0 ;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);

		mTitle = getString(R.string.menu_tit_setting);

		findViewById(R.id.view_about).setOnClickListener(this);
		findViewById(R.id.view_notify).setOnClickListener(this);
		findViewById(R.id.view_language).setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {
			case R.id.view_about:

				MainApplication.getInstance().trackScreenView("Menu_Setting_About");
				MainApplication.getInstance().trackEvent("Menu_Setting_About", "Menu_Setting_About", "Send event Menu_Setting_About");

				intent = new Intent(this, AboutActivity.class);
				startActivity(intent);

				break;
			case R.id.view_notify:
				intent = new Intent(this, SwitchServerActivity.class);
				startActivity(intent);

				break;

			case R.id.view_language:

//				intent = new Intent(this, TestVideoView.class);
//				startActivity(intent);
				if(clickcount == 5){
					intent = new Intent(this, StaffAct.class);
					startActivity(intent);
				}else {
					clickcount++;
				}

				break;

		}

	}
}
/*
	JSONObject PutStreamConnection() throws JSONException {


		JSONObject questionnaire = new JSONObject();

		questionnaire.put("appName", "APIEmulator");
		questionnaire.put("appVersion", "2.1");
		questionnaire.put("OS", "iOS");
		questionnaire.put("deviceID", "UDID22AD6G1D3AA1S24");
		questionnaire.put("client", "emulator");
		questionnaire.put("data", "1XUFl4VXyc4zbVmIRbG/4x3aqjFTS4z6ckYxazPnOhpijCm5QJi0ouqtQlfrvYYUnUT6VTBzQkzW2GGmXb2LFHSTFhQdCx+5x3+nyRLHdhVQPRfn6uAmmuP52TYYP8P4Xr7mo7rYpwb3cyqZbGjARElgWQW+LByMwnYbWpng1jVtD6vo1nJcW6uU4WSVPjPbnnSF6HK0a/oo9or2tNCLl2QSHuLJ6gxDWjktGEwnVf3l9YiIxs3KXNUCuqgs+IPcBoqhlWDN6/T5CnUkZVpc3UAo2k5ATpAyeDoft8OFHfSE/UV9pByietN30XqCRXcKuAWw3OcwojlarBxWDVb30A==");
		questionnaire.put("command", "2970");
		questionnaire.put("OSVersion", "7.1");
		questionnaire.put("language", "EN");
		questionnaire.put("mask", "amtRbXFxQkc=");



		return questionnaire;
	}

	public void postman() {

		PostCommand getCommand = new PostCommand(getApplicationContext());

		try {
			getCommand.setProtocol(PutStreamConnection());
		} catch (JSONException e) {
			e.printStackTrace();
		}

		new GetStreamServiceTask(this).execute(getCommand);

	}

	class GetStreamServiceTask extends CommonServiceTask {
		String message;
		public GetStreamServiceTask(FragmentActivity context) {
			super(context);
		}

		@Override
		protected ServiceResponse doInBackground(Command... params) {
			ServiceConnector srConnector = new ServiceConnector("http://203.151.189.172/api/2.2/interface/streaming/src/default/from/data-mobile");
			ServiceResponse serviceResponse = srConnector.doAsynPost(params[0],  false);

			try {
				int code = serviceResponse.getCode();
				if (code == ServiceResponse.SUCCESS || code == HttpStatus.SC_OK) {
					JSONObject json = new JSONObject(serviceResponse.getContent().toString());


				}
			} catch (Exception e) {
				MainApplication.getInstance().trackException(e);
				Log.e("TIT", "doInBackground error", e);
			}

			return serviceResponse;
		}

		@Override
		protected void onPostExecute(ServiceResponse result) {
			super.onPostExecute(result);

			try {
				switch(result.getCode()){
					case ServiceResponse.SUCCESS:
					case HttpStatus.SC_OK:
						context.setResult(Activity.RESULT_OK);

						break;
					case 2://restore token failed
						//TODO logout

						break;
					default:
						message = result.getMessage();
						String ms =	message+"\r\n"+"Please retry start livestream again.";

				}
			} catch (Exception e) {
				MainApplication.getInstance().trackException(e);

				Log.e("TIT", "process response error", e);

				MainApplication.getInstance().trackScreenView("Live_Fail");
				MainApplication.getInstance().trackEvent("Live_Fail", "Live_Fail", "Send event Live_Fail");

				message = e.getMessage();

			}
		}


	}

	public class PostCommand extends BaseInsightCommand {
		private JSONObject json = new JSONObject();

		public PostCommand(Context context) {
			super(context, "");
		}

		public void setProtocol(JSONObject protocol) {
			try{

				json = protocol;

			}catch(Exception e){
				Log.e(LibConfig.LOGTAG, "add feed type error");
			}

		}
		@Override
		public String toString() {

			Log.d("POST","POST JSON:"+json.toString());
			return json.toString();

		}

	}

	}

*/