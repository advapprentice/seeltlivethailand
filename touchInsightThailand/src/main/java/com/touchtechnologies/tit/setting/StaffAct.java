package com.touchtechnologies.tit.setting;

import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;

import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.util.ResourceUtil;

/**
 * Created by TOUCH on 17/5/2559.
 */
public class StaffAct extends SettingActionbarActivity {
    private RadioGroup radioGroup,radioGroup2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff);
        mTitle = "Stting LiveStream";
        radioGroup = (RadioGroup) findViewById(R.id.radiogroup1);
        radioGroup.setOnCheckedChangeListener(resolutioncheck);
        Switch livetime = (Switch) findViewById(R.id.switch1);
        if (ResourceUtil.getLivetime(this) == 0){
            livetime.setChecked(true);
        }else {
            livetime.setChecked(false);
        }

        radioGroup2 = (RadioGroup) findViewById(R.id.radiogroup2);
        radioGroup2.setOnCheckedChangeListener(framerate);

        livetime.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ResourceUtil.setLivetime(StaffAct.this,0);
                }else {
                    ResourceUtil.setLivetime(StaffAct.this,5);
                }
            }
        });
      int fps=  ResourceUtil.getFramerate(this);
        switch (fps){
            case 10:
                ((RadioButton)findViewById(R.id.radioButton8)).setChecked(true);
                break;
            case 15:
                ((RadioButton)findViewById(R.id.radioButton9)).setChecked(true);
                break;
            case 20:
                ((RadioButton)findViewById(R.id.radioButton10)).setChecked(true);
                break;
            case 25:
                ((RadioButton)findViewById(R.id.radioButton11)).setChecked(true);
                break;
            case 30:
                ((RadioButton)findViewById(R.id.radioButton12)).setChecked(true);
                break;
            default:
                ((RadioButton)findViewById(R.id.radioButton8)).setChecked(true);
                break;
        }
        int resolution = ResourceUtil.getResolution(this);
        switch (resolution){
            case 1:
                ((RadioButton)findViewById(R.id.radioButton1)).setChecked(true);
                break;
            case 2:
                ((RadioButton)findViewById(R.id.radioButton2)).setChecked(true);
                break;
            case 3:
                ((RadioButton)findViewById(R.id.radioButton3)).setChecked(true);
                break;
            case 4:
                ((RadioButton)findViewById(R.id.radioButton4)).setChecked(true);
                break;
            case 5:
                ((RadioButton)findViewById(R.id.radioButton5)).setChecked(true);
                break;
            case 6:
                ((RadioButton)findViewById(R.id.radioButton6)).setChecked(true);
                break;
            case 7:
                ((RadioButton)findViewById(R.id.radioButton7)).setChecked(true);
                break;
            default:
                ((RadioButton)findViewById(R.id.radioButton6)).setChecked(true);
                break;
        }

    }

    @Override
    public void restoreActionBar() {
        super.restoreActionBar();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void getPermission() {
        super.getPermission();
    }

    RadioGroup.OnCheckedChangeListener resolutioncheck = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId){
                case R.id.radioButton1:
                    ResourceUtil.setResolution(StaffAct.this,1);
                    break;
                case R.id.radioButton2:
                    ResourceUtil.setResolution(StaffAct.this,2);
                    break;
                case R.id.radioButton3:
                    ResourceUtil.setResolution(StaffAct.this,3);
                    break;
                case R.id.radioButton4:
                    ResourceUtil.setResolution(StaffAct.this,4);
                    break;
                case R.id.radioButton5:
                    ResourceUtil.setResolution(StaffAct.this,5);
                    break;
                case R.id.radioButton6:
                    ResourceUtil.setResolution(StaffAct.this,6);
                    break;
                case R.id.radioButton7:
                    ResourceUtil.setResolution(StaffAct.this,7);
                    break;
                default:
                    ResourceUtil.setResolution(StaffAct.this,6);
                    break;

            }

        }
    };
    RadioGroup.OnCheckedChangeListener framerate = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId){
                case R.id.radioButton8:
                    ResourceUtil.setFramerate(StaffAct.this,10);
                    break;
                case R.id.radioButton9:
                    ResourceUtil.setFramerate(StaffAct.this,15);
                    break;
                case R.id.radioButton10:
                    ResourceUtil.setFramerate(StaffAct.this,20);
                    break;
                case R.id.radioButton11:
                    ResourceUtil.setFramerate(StaffAct.this,25);
                    break;
                case R.id.radioButton12:
                    ResourceUtil.setFramerate(StaffAct.this,30);
                    break;
            }
        }
    };

}
