package com.touchtechnologies.tit.setting;

import android.app.ActionBar;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.MenuItem;

import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.util.PushRegisterUtil;
import com.touchtechnologies.tit.util.UserUtil;

public class SwitchServerActivity extends PreferenceActivity  implements OnSharedPreferenceChangeListener {
	Context context;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getActionBar();
	    actionBar.setIcon(R.drawable.ic_settingalert); 
	    actionBar.setDisplayShowTitleEnabled(true); 
		getActionBar().setDisplayHomeAsUpEnabled(true);
	//	getActionBar().setDisplayShowHomeEnabled(false);
	    getActionBar().setTitle(R.string.menu_tit_setting);
	    
	    addPreferencesFromResource(R.xml.preference_setting);
		  Context context = getApplicationContext();
	      SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
	      prefs.registerOnSharedPreferenceChangeListener(this);
	  //    ListPreference dataPref = (ListPreference) findPreference("date");
	      
	      
	}

	@Override
	protected void onResume() {
		super.onResume();
	
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}
	 
    public static boolean getCheckSound(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switchsound", true);
     
    }
    public static boolean getCheckVibrate(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switchvibrate", true);
     
    }

	public static String getSelectItemServer(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context).getString("pref_mode","no selection");
	
	}

    public static String getEditTextSetting(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("keyEditText", "Default");
    }

   
    public static boolean getCkeckPettyScreenSetting(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("keyCkeckPettyScreen", true);
    }

	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
	//	Toast.makeText(this, sharedPreferences.getString(key, "NO VALUE"), Toast.LENGTH_LONG).show();
		if(key.equals("pref_mode")){			
			PushRegisterUtil.clear(this);
			UserUtil.clear(this);
		}
	}
    
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home){
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
}
