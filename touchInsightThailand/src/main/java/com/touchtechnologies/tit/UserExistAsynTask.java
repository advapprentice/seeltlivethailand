package com.touchtechnologies.tit;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.touchtechnologies.command.Command;
import com.touchtechnologies.command.insightthailand.LoginCommand;
import com.touchtechnologies.command.insightthailand.LoginSocialCommand;
import com.touchtechnologies.command.insightthailand.UserExistCommand;
import com.touchtechnologies.dataobject.insightthailand.User;
import com.touchtechnologies.net.ServiceConnector;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.login.RegistersActivity;
import com.touchtechnologies.tit.servicetask.CommonServiceTask;
import com.touchtechnologies.tit.util.ResourceUtil;
import com.touchtechnologies.tit.util.UserUtil;

/**
 * A task use to check is a current user with a given source (fb, g+) exist.
 * If exist then start a MainActivity otherwise start RegisterActivity.
 * This must be call after a fb or g+ login success.
 * @author Touch
 *
 */
public class UserExistAsynTask extends CommonServiceTask{

	public UserExistAsynTask(FragmentActivity context) {
		super(context, context.getString(R.string.wait), null,null);
	}
	
	@Override
	protected ServiceResponse doInBackground(Command... params) {
		Command command = null;
		if(params == null || params.length == 0){
			command = new UserExistCommand(context);
		}else{
			command = params[0];
		}
		
		//get user info posting
		User user = UserUtil.getUser(context);
		Log.d("UserExistAsynTask", "check user " + user.toString());
		
		ServiceConnector srConnector = new ServiceConnector(ResourceUtil.getServiceUrl(context) 
				+ context.getString(R.string.app_service_user_exist) 
				+ user.getSignupSource());
		
		ServiceResponse serviceResponse = srConnector.doAsynPost(command,  false);
		
		return serviceResponse;
	}
	
	@Override
	protected void onPostExecute(ServiceResponse result) {
		if(dialog != null){
			try{
				dialog.dismiss();
			}catch(Exception e){
			}
		}
		
		Intent intent;
		switch (result.getCode()) {
		case 0://exist, -> login
			// compose command
			LoginCommand command = new LoginSocialCommand(context);
			command.setUser(UserUtil.getUser(context));
			
			new LoginAsynTask(context).execute(command);
			break;
		case 404://not found	
			intent = new Intent(context, RegistersActivity.class);
			context.startActivity(intent);
			context.finish();
			break;
		default:
			AlertDialog alertDialog = new Builder(context).create();
			alertDialog.setTitle(result.getMessage());
			alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					context.finish();
				}
			});
			alertDialog.show();
			
			break;
		}
	}
}
