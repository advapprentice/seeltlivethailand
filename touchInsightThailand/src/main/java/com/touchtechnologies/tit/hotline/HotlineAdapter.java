package com.touchtechnologies.tit.hotline;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.touchtechnologies.animate.dialog.MaterialDialog;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.dataobject.HotlinePhone;

import java.util.List;

/**
 * Created by TOUCH on 21/3/2559.
 */


public class HotlineAdapter extends BaseExpandableListAdapter implements View.OnClickListener{

    private Context context;
    private List<HotlinePhone> mHotlinePhones;
    private final LayoutInflater mLayoutInflater;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    public HotlineAdapter(Context context,List<HotlinePhone> mHotlinePhones) {
        this.context = context;
        this.mHotlinePhones = mHotlinePhones;
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.NONE)
                .resetViewBeforeLoading(true)
                .build();
        mLayoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getGroupCount() {
        return mHotlinePhones.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mHotlinePhones.get(groupPosition).getHotlines().size();
    }

    @Override
    public HotlinePhone getGroup(int groupPosition) {
        return mHotlinePhones.get(groupPosition);
    }

    @Override
    public HotlinePhone.Hotline getChild(int groupPosition, int childPosition) {
        return mHotlinePhones.get(groupPosition).getHotlines().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {


        ExpandableListView mExpandableListView = (ExpandableListView) parent;
        mExpandableListView.expandGroup(groupPosition);

        if(convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.hotline_list_group_item, parent, false);
        }

        TextView text = (TextView) convertView.findViewById(R.id.hotline_list_group_item_text);
//		text.setText(mGroups[groupPosition]);
        text.setText(getGroup(groupPosition).getName() == null ? "" : getGroup(groupPosition).getName() );


        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = new ViewHolder();
        if(convertView == null){
            convertView = mLayoutInflater.inflate(R.layout.hotline_expand_item,parent,false);
            viewHolder.image_logo = (ImageView) convertView.findViewById(R.id.image_logo_hotline);
            viewHolder.hotlinename = (TextView) convertView.findViewById(R.id.textView_Hotline_name);
            viewHolder.phonenum = (TextView) convertView.findViewById(R.id.textView_Hotline_Phone);
            viewHolder.btn_call = (ImageView) convertView.findViewById(R.id.hotline_call);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        HotlinePhone.Hotline mHotline = getChild(groupPosition,childPosition);
        imageLoader.displayImage(mHotline.getImage(), viewHolder.image_logo,options);
        viewHolder.phonenum.setText(mHotline.getPhonenum());
        viewHolder.btn_call.setTag(mHotline.getPhonenum());
        viewHolder.hotlinename.setText(mHotline.getName());
        viewHolder.btn_call.setOnClickListener(this);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.hotline_call:
                setDialog((String) v.getTag());

                break;
        }
    }


    public void setDialog(final String tell){
        final MaterialDialog mMaterialDialog = new MaterialDialog(context);
        mMaterialDialog.setTitle("Call now")
                .setMessage(tell)
                .setPositiveButton("Call", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:" + tell));
                            context.startActivity(callIntent);
                        } catch (ActivityNotFoundException activityException) {
                            Log.e("dialing", "Call failed", activityException);
                        }
                        mMaterialDialog.dismiss();
                    }
                }).setNegativeButton("Cancle", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMaterialDialog.dismiss();
            }
        }).setCanceledOnTouchOutside(true).show();
    }

    protected class  ViewHolder{
        TextView hotlinename;
        TextView phonenum;
        ImageView image_logo;
        ImageView btn_call;
    }

}