package com.touchtechnologies.tit.hotline;


import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.dataobject.HotlinePhone;
import com.touchtechnologies.tit.util.DataCacheUtill;
import com.touchtechnologies.tit.util.ResourceUtil;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class Hotline_activity extends SettingActionbarActivity {
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotline);
        mTitle = "Helpful Contact";
        fragmentManager = getSupportFragmentManager();


        final HotlineExpandFragment mHotlineExpandFragment = new HotlineExpandFragment().newInstance();



        fragmentManager.beginTransaction().replace(R.id.container,mHotlineExpandFragment).commit();


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

      //  getMenuInflater().inflate(R.menu.helpful_search,menu);
        return true;
    }




}
