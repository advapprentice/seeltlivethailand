package com.touchtechnologies.tit.hotline;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;

import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.dataobject.HotlinePhone;
import com.touchtechnologies.tit.hotel.HotelPagerFragment;
import com.touchtechnologies.tit.lib.animator.FloatingGroupExpandableListView;
import com.touchtechnologies.tit.lib.animator.WrapperExpandableListAdapter;
import com.touchtechnologies.tit.util.DataCacheUtill;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class HotlineExpandFragment extends Fragment {

    private List<HotlinePhone> mHotlinePhones;
    private List<HotlinePhone.Hotline> mHotlines = new ArrayList<>();
    private FloatingGroupExpandableListView list;
    private ListView listView;
    private EditText editText;
    private HotlineAdapter hotlineAdapter;
    private  Listsearch_adapter mListsearch_adapter;

    public HotlineExpandFragment() {
        // Required empty public constructor
    }


    public static HotlineExpandFragment newInstance() {
        HotlineExpandFragment fragment = new HotlineExpandFragment();
        return fragment;
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getSerializable(ARG_PARAM1);
//            //mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_hotline_expand, container, false);
            list = (FloatingGroupExpandableListView) view.findViewById(R.id.Hotline_list_expand);
            listView = (ListView) view.findViewById(R.id.listView_hotline);
            editText = (EditText) view.findViewById(R.id.editText_hotline_search);
            list.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard(v);
                    return false;
                }
            });
            listView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard(v);
                    return false;
                }
            });
            view.findViewById(R.id.view_rela_hot).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard(v);
                    return false;
                }
            });

            list.setFocusable(true);
        try {
             mHotlinePhones = DataCacheUtill.getHotlinePhones(getActivity());
            if (mHotlines != null) {


            hotlineAdapter = new HotlineAdapter(getActivity(), mHotlinePhones);
            for (HotlinePhone mHotlinePhone: mHotlinePhones) {
                mHotlines.addAll(mHotlinePhone.getHotlines());
            }
             mListsearch_adapter = new Listsearch_adapter(getActivity(),mHotlines);

        final WrapperExpandableListAdapter wrapperAdapter = new WrapperExpandableListAdapter(hotlineAdapter);
        list.setAdapter(wrapperAdapter);
        listView.setAdapter(mListsearch_adapter);
//        listView.addHeaderView();
//        if(editText.isClickable()){
//            listView.setVisibility(View.VISIBLE);
//            list.setVisibility(View.INVISIBLE);
//        }else {
//            listView.setVisibility(View.INVISIBLE);
//            list.setVisibility(View.VISIBLE);
//        }
        listView.setVisibility(View.INVISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        editText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search, 0, 0, 0);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mListsearch_adapter.getFilter().filter(s);
                if(s.length() > 0){
                    listView.setVisibility(View.VISIBLE);
                    list.setVisibility(View.INVISIBLE);
                }else {
                    listView.setVisibility(View.INVISIBLE);
                    list.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

    protected void hideKeyboard(View view)
    {
        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
