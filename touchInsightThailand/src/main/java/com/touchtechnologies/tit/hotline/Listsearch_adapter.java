package com.touchtechnologies.tit.hotline;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.dataobject.HotlinePhone;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by TOUCH on 22/3/2559.
 */
public class Listsearch_adapter extends BaseAdapter implements Filterable{
    Context mContext;
    List<HotlinePhone.Hotline> mHotline;
    List<HotlinePhone.Hotline> search;
    String searchString;
    LayoutInflater mLayoutInflater;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    public Listsearch_adapter(Context mContext,List<HotlinePhone.Hotline> mHotline) {
        this.mContext = mContext;
        this.mHotline = mHotline;
        search = mHotline;
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.NONE)
                .resetViewBeforeLoading(true)
                .build();
        mLayoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public int getCount() {
        return mHotline.size();
    }

    @Override
    public Object getItem(int position) {
        return mHotline.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();
        if(convertView == null){
            convertView = mLayoutInflater.inflate(R.layout.hotline_expand_item,parent,false);
            viewHolder.image_logo = (ImageView) convertView.findViewById(R.id.image_logo_hotline);
            viewHolder.hotlinename = (TextView) convertView.findViewById(R.id.textView_Hotline_name);
            viewHolder.phonenum = (TextView) convertView.findViewById(R.id.textView_Hotline_Phone);
            viewHolder.btn_call = (ImageView) convertView.findViewById(R.id.hotline_call);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        HotlinePhone.Hotline mHotline = (HotlinePhone.Hotline) getItem(position);
        imageLoader.displayImage(mHotline.getImage(), viewHolder.image_logo,options);
        viewHolder.phonenum.setText(mHotline.getPhonenum());
        viewHolder.btn_call.setTag(mHotline.getPhonenum());
        viewHolder.hotlinename.setText(mHotline.getName());
        viewHolder.hotlinename.setTextColor(Color.parseColor("#787878"));
        String country = mHotline.getName().toLowerCase(Locale.getDefault());
        if(searchString != null) {
            if (country.contains(searchString)) {
                Log.e("test", country + " contains: " + searchString);
                int startPos = country.indexOf(searchString);
                int endPos = startPos + searchString.length();

                Spannable spanText = Spannable.Factory.getInstance().newSpannable(viewHolder.hotlinename.getText()); // <- EDITED: Use the original string, as `country` has been converted to lowercase.
                spanText.setSpan(new ForegroundColorSpan(Color.BLACK), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                viewHolder.hotlinename.setText(spanText, TextView.BufferType.SPANNABLE);
            }
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {

        Filter mFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<HotlinePhone.Hotline> FilteredArrayNames = new ArrayList<HotlinePhone.Hotline>();

                // perform your search here using the searchConstraint String.

                constraint = constraint.toString().toLowerCase().trim();
                if (constraint != null && constraint.length() > 0) {
                    searchString = (String)constraint;
                    for (int i = 0; i < search.size(); i++) {
                        String dataNames = search.get(i).getName().toString().trim();
                        if (dataNames.toLowerCase().contains(constraint.toString())) {
                            FilteredArrayNames.add(search.get(i));
                        }
                    }
                    results.count = FilteredArrayNames.size();
                    results.values = FilteredArrayNames;
                   // Log.e("VALUES", results.values.toString());
                }else {
                    results.count = search.size();
                    results.values = search;
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mHotline = (List<HotlinePhone.Hotline>) results.values;
                notifyDataSetChanged();

            }
        };

        return mFilter;
    }

    protected class ViewHolder{
        TextView hotlinename;
        TextView phonenum;
        ImageView image_logo;
        ImageView btn_call;
    }

}
