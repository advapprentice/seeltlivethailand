package com.touchtechnologies.tit.nearby;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.touchtechnologies.command.insightthailand.RestServiceNearbyPoiListCommand;
import com.touchtechnologies.dataobject.Hospitality;
import com.touchtechnologies.json.JSONUtil;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.util.DataCacheUtill;
import com.touchtechnologies.tit.util.GPSTrackers;
import com.touchtechnologies.tit.util.ResourceUtil;

//import org.apache.http.entity.ContentType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by TouchICS on 8/31/2016.
 */
public class NearByMapFragment extends Fragment implements View.OnClickListener,GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener,AdapterView.OnItemClickListener, OnMapReadyCallback {
    private GoogleMap googleMap;
    private GoogleApiClient client;
    private double latitude;
    private double longitude;
    private int mapZoomLevel = 14;
    private NearbyPoiInfoMapAdapter adapterInfo;
    private HashMap<Marker, Hospitality> hMarkers;
    protected Location location;
    private List<Hospitality> arrPois = new ArrayList<Hospitality>();

    private Hospitality poi = null;
    private View viewAll,viewAttaction,viewRestuarent,viewHotel;
    private String strFillter;
    private String url;
    private ImageView imgAll,imgAttraction,imgRestaurant,imgHotel;
    private boolean isPressed = false;
    private AutoCompleteTextView autocomplete;
    private SearchAdapter dataAdapter;
    private View viewSearch;
    private View viewDelete;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final	View rootview = inflater.inflate(R.layout.activity_nearby, container, false);


        hMarkers = new HashMap<>();
        adapterInfo = new NearbyPoiInfoMapAdapter(getActivity());
        adapterInfo.setMarker(hMarkers);

        GPSTrackers   mGPS = new GPSTrackers(getActivity());
        if (!mGPS.canGetLocation()) {
            mGPS.showSettingsAlert();
        } else {
            mGPS.getLocation();
            latitude = mGPS.getLatitude();
            longitude = mGPS.getLongitude();
            Log.i("Lat =>",":"+latitude+"\nLong =>"+longitude);
        }

        viewAll = (View)rootview.findViewById(R.id.viewAll);
        viewAttaction = (View)rootview.findViewById(R.id.viewAttactoin);
        viewRestuarent = (View)rootview.findViewById(R.id.viewRestaurent);
        viewHotel = (View)rootview.findViewById(R.id.viewHotel);
        imgAll =(ImageView) rootview.findViewById(R.id.imgAll);
        imgHotel =(ImageView) rootview.findViewById(R.id.imgHotel);
        imgAttraction =(ImageView) rootview.findViewById(R.id.imgAttaction);
        imgRestaurant =(ImageView) rootview.findViewById(R.id.imgRestuarent);
        viewSearch =(View)rootview.findViewById(R.id.viewSearch);
        viewDelete =(View)rootview.findViewById(R.id.viewDelete);
        rootview.findViewById(R.id.viewSocial).setOnClickListener(this);

        autocomplete =(AutoCompleteTextView)rootview.findViewById(R.id.autoCompleteTextView1);


        dataAdapter = new SearchAdapter(getActivity());
        autocomplete.setThreshold(1);
        autocomplete.setAdapter(dataAdapter);
        autocomplete.setOnItemClickListener(this);
        autocomplete.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length() != 0){
                    viewDelete.setVisibility(View.VISIBLE);
                    viewSearch.setVisibility(View.GONE);
                }else {
                    viewSearch.setVisibility(View.VISIBLE);
                    viewDelete.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });




        viewAll.setOnClickListener(this);
        viewAttaction.setOnClickListener(this);
        viewRestuarent.setOnClickListener(this);
        viewHotel.setOnClickListener(this);
        viewDelete.setOnClickListener(this);

        strFillter = "all";


        listLiveNearby(strFillter);
        return rootview;
    }
    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.viewAll:
                listLiveNearby("all");
                imgAll.setImageResource(R.drawable.new_all_2);
                imgHotel.setImageResource(R.drawable.new_hotel_1);
                imgAttraction.setImageResource(R.drawable.new_att_1);
                imgRestaurant.setImageResource(R.drawable.res_1);


                break;
            case R.id.viewHotel:

                listLiveNearby("hotel");
                imgAll.setImageResource(R.drawable.new_all_1);
                imgHotel.setImageResource(R.drawable.new_hotel_2);
                imgAttraction.setImageResource(R.drawable.new_att_1);
                imgRestaurant.setImageResource(R.drawable.res_1);

                break;
            case R.id.viewAttactoin:

                listLiveNearby("attraction");
                imgAll.setImageResource(R.drawable.new_all_1);
                imgHotel.setImageResource(R.drawable.new_hotel_1);
                imgAttraction.setImageResource(R.drawable.new_att_2);
                imgRestaurant.setImageResource(R.drawable.res_1);

                break;
            case R.id.viewRestaurent:

                listLiveNearby("restaurant");
                imgAll.setImageResource(R.drawable.new_all_1);
                imgHotel.setImageResource(R.drawable.new_hotel_1);
                imgAttraction.setImageResource(R.drawable.new_att_1);
                imgRestaurant.setImageResource(R.drawable.res_2);
                break;
            case R.id.viewSocial:
                startActivity(new Intent(getActivity(),FacebookNearbyActivity.class));
                break;
            case R.id.viewDelete:

                autocomplete.setText("");
                break;

        }

    }

    public void listLiveNearby(String fillter){
        strFillter = fillter;

        if (strFillter.equals("all")) {
            url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_NearByMe) + latitude + "/" + longitude + "/10";
            Log.i("Url", ":" + url);
            new RestServiceNearbyPoiListCommand(getActivity(), url) {

                protected void onPostExecute(java.util.List<Hospitality> result) {
                    arrPois = result;

                    onUpdatemap(arrPois, poi);
                    dataAdapter.setData(arrPois);

                }
            }.execute();
        }
//          else if(strFillter.equals(("face"))){
//       // getnearbyFacebook();
//
//        }
        else{
                url = ResourceUtil.getServiceUrl(getActivity()) + getResources().getString(R.string.app_service_NearByMe)+latitude+"/"+longitude+"/10"
                    +"?filters[poi][provider_type_keyname][operator]==&filters[poi][provider_type_keyname][value]="+strFillter;
            Log.i("Url",":"+url);
            new RestServiceNearbyPoiListCommand(getActivity(),url){

                protected void onPostExecute(java.util.List<Hospitality> result) {
                    arrPois = result;

                    onUpdatemap(arrPois,poi);
                    dataAdapter.setData(arrPois);

                }
            }.execute();
        }



    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Hospitality selection = dataAdapter.getItem(position);
      //  String selection = (String)  arrPois.get(parent.getSelectedItemPosition()).getId();
       // String selection = (String) arrPois.get(position).getName();
        Toast.makeText(getActivity(),"Select "+ position +" " + selection.getName(),Toast.LENGTH_SHORT).show();
        onUpdatemap(arrPois,selection);
    }


    void onUpdatemap(List<Hospitality> list, Hospitality hospi) {
        if(googleMap == null){
            return;
        }
        googleMap.clear();
        if (googleMap == null) {
            return ;
        }

        try {
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            for (Hospitality poi : list) {

                    MarkerOptions markerOptions = new MarkerOptions()
                            .title(poi.getName())
                            .snippet(poi.getDistance() + " Km")
                            .position(new LatLng(poi.getLatitude(), poi.getLongitude()));
                    if (poi.getProviderTypeKeyname().equals("hotel")) {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.hotel));
                    } else if (poi.getProviderTypeKeyname().equals("attraction")) {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.attraction));
                    } else if (poi.getProviderTypeKeyname().equals("restaurant")) {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.restaurant));
                    }


                    Marker tp = googleMap.addMarker(markerOptions);
                /*
                    Marker tp = googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(poi.getLatitude(), poi.getLongitude()))
                            .title(poi.getName())
                            .snippet(poi.getDistance()+" Km"));
                        //    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_live_nearby)));
                */
                    hMarkers.put(tp, poi);
                }

        if(hospi!=null) {
            MarkerOptions markerOptions = new MarkerOptions()
                    .title(hospi.getName())
                    .snippet(hospi.getDistance() + " Km")
                    .position(new LatLng(hospi.getLatitude(), hospi.getLongitude()));
            if (hospi.getProviderTypeKeyname().equals("hotel")) {
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.hotel_hover));
            } else if (hospi.getProviderTypeKeyname().equals("attraction")) {
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.attraction_hover));
            } else if (hospi.getProviderTypeKeyname().equals("restaurant")) {
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.restaurant_hover));
            }

            Marker tp2 = googleMap.addMarker(markerOptions);
            hMarkers.put(tp2, hospi);
            tp2.showInfoWindow();
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(hospi.getLatitude() ,hospi.getLongitude()),mapZoomLevel));

        }



        } catch (Exception e) {
            Log.e("map", "Map marker " + e.getMessage());
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        SupportMapFragment  MapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        MapFragment.getMapAsync(this);
            googleMap.setMyLocationEnabled(true);
            googleMap.setOnInfoWindowClickListener(this);
            googleMap.setOnMarkerClickListener(this);
            googleMap.setInfoWindowAdapter(adapterInfo);

            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude ,longitude),mapZoomLevel));

            Log.i("Lat/long",":"+latitude+"/"+longitude);


    }

    public void setLocation(Location location) {
        this.location = location;

        if (googleMap != null) {
            // move map to last know location if possible when started
            LatLng myLastLatLon = new LatLng(location.getLatitude(), location.getLongitude());
            CameraUpdate camUpdate = CameraUpdateFactory.newLatLngZoom(myLastLatLon, mapZoomLevel);
            googleMap.animateCamera(camUpdate);
        }
    }

    public void onLocationChanged(Location location) {
        this.location = location;

        if(client.isConnected()){
            client.disconnect();
        }

    }


    @Override
    public void onInfoWindowClick(Marker marker) {
        Hospitality pois = hMarkers.get(marker);
        Intent intent = new Intent(getActivity(),NearByDetailActivity.class);
        intent.putExtra(Hospitality.class.getName(), pois);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }

    @Override
    public void onMapClick(LatLng latLng) {
        MarkerOptions mark = new MarkerOptions();

        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mark.position(latLng);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Hospitality his = hMarkers.get(marker);

        onUpdatemap(arrPois,his);
        //  Toast.makeText(getActivity(),""+his.getName(),Toast.LENGTH_SHORT).show();

        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMapv) {
        googleMap = googleMapv;
    }

//         public void getnearbyFacebook(){
//         String url = "https://graph.facebook.com/search?" +
//                 "type=place&center=13.752468%2C100.566107&distance=1000&" +
//                 "access_token=EAAX0NmD7gWABAFFx51sZCReS3iOvFtZA9xFHyZBSXZCI2mHYRJrjFofwOAeOE7Y61uxi" +
//                 "uXnnkZAdVS9PPjsikZCusaFYUsnQclTIY6zgzXFIhRdtgfNgDZBxOZCVTauUDKmMNT9tQIu2kzUFG5vy" +
//                 "PC7AKiD8CIlbd0QZD&q=Hotel&fields=name,fan_count,talking_about_count,location" +
//                 ",checkins,category,category_list,picture.height(500)";
//         JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
//             @Override
//             public void onResponse(JSONObject jsonObject) {
//                 arrPois.clear();
//                 Log.d(NearByMapFragment.class.getSimpleName(), "onResponse: "+jsonObject.toString());
//                 try {
//                     JSONArray  jsonArray = jsonObject.getJSONArray("data");
//                     if(jsonArray != null){
//                         for(int i=0;jsonArray.length()> i;i++){
//                             JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//                             Hospitality hospitality = new Hospitality();
//                            Facebookplace facebookplace = new Facebookplace(jsonObject1,latitude,longitude);
//                             hospitality.setLogo(facebookplace.getImage().getUri());
//                             hospitality.setName(facebookplace.getName());
//                             hospitality.setLatitude(facebookplace.getLat());
//                             hospitality.setLongitude(facebookplace.getLon());
//                             hospitality.setProviderTypeKeyname("hotel");
//                             hospitality.setDistance(facebookplace.getDistance());
//                             arrPois.add(hospitality);
//
//                         }
//                         onUpdatemap(arrPois,null);
//                     }
//                 } catch (JSONException e) {
//                     e.printStackTrace();
//                 }
//
//
//
//             }
//         }, new Response.ErrorListener() {
//             @Override
//             public void onErrorResponse(VolleyError volleyError) {
//
//             }
//         });
//         MainApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//
//
//
//     }

}
