package com.touchtechnologies.tit.nearby;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.touchtechnologies.dataobject.Hospitality;
import com.touchtechnologies.tit.POIUpdateListner;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;


/**
 * Created by TouchICS on 9/1/2016.
 */
public class NearByDetailActivity extends SettingActionbarActivity implements View.OnClickListener {
    private Hospitality pois;
    private TextView txtAddress, txtPhone, txtWebsite, txtEmail, txtFacebook, txtAbout, txtDescription;
    private Button btnCallNow;
    private ImageView imgGetDirection, imgCover, imgGellary;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private POIUpdateListner poiUpdateListeners;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_detail);

        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.NONE)
                .showImageForEmptyUri(R.drawable.bg_cctvdefault)
                .showImageOnFail(R.drawable.bg_cctvdefault)
                .showImageOnLoading(R.drawable.bg_cctvdefault)
                .cacheInMemory(true)
                .displayer(new FadeInBitmapDisplayer(500)) //fade in images
                .resetViewBeforeLoading(true).build();
        imageLoader = ImageLoader.getInstance();


        pois = (Hospitality) getIntent().getSerializableExtra(Hospitality.class.getName());

        mTitle = pois.getName();

        txtAddress = (TextView) findViewById(R.id.txtAddress);
        txtPhone = (TextView) findViewById(R.id.txtPhone);
        txtWebsite = (TextView) findViewById(R.id.txtWebsite);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        txtFacebook = (TextView) findViewById(R.id.txtFacebook);
        txtAbout = (TextView) findViewById(R.id.txtAbout);
        txtDescription = (TextView) findViewById(R.id.txtDescription);
        btnCallNow = (Button) findViewById(R.id.btnCallNow);
        imgGetDirection = (ImageView) findViewById(R.id.imgGetDirection);
        imgCover = (ImageView) findViewById(R.id.imgCover);
        imgGellary = (ImageView) findViewById(R.id.imgGellary);

        imgGellary.setOnClickListener(this);
        btnCallNow.setOnClickListener(this);
        imgGetDirection.setOnClickListener(this);
        btnCallNow.setVisibility(View.GONE);

        if(pois.getPhone().equals("")){
            btnCallNow.setVisibility(View.GONE);
            txtPhone.setText("-");
        }else{
            btnCallNow.setVisibility(View.VISIBLE);
            txtPhone.setText(pois.getPhone());
        }
        if (pois.getWebUrl().equals("")){
            txtWebsite.setText("-");
        }else {
            txtWebsite.setText(pois.getWebUrl());
        }
        if (pois.getEmail().equals("")){
            txtEmail.setText("-");
        }else{
            txtEmail.setText(pois.getEmail());
        }
        if (pois.getDisplayAddress().equals("")){
            txtAddress.setText("-");
        }else {
            txtAddress.setText(pois.getDisplayAddress());
        }
        if (pois.getDescription().equals("")){
            txtDescription.setText("-");
        }else {
            txtDescription.setText(pois.getDescription());
        }




        txtFacebook.setText(pois.getWongnaiurl());
        txtAbout.setText("About " + pois.getName());
        imageLoader.displayImage(pois.getLogoCover(), imgCover, options);



    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCallNow:
                if (pois.getPhone() != null) {
                    checkPermissions();
                }

                break;
            case R.id.imgGetDirection:
                Uri gmmIntentUri = Uri.parse("google.navigation:" + pois.getLatitude() + ","
                        + pois.getLongitude()).buildUpon()
                        .appendQueryParameter("q", pois.getName()).build();
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);

                break;
            case R.id.imgGellary:
                Intent intent = new Intent(NearByDetailActivity.this, PoisGellaryActivity.class);
                intent.putExtra(Hospitality.class.getName(), pois);
                startActivity(intent);
           //     getDetailPois(pois);
                break;
        }

    }


    private void checkPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE}, 1);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + pois.getPhone()));
                    startActivity(callIntent);
                } else {

                }
                return;
            }


        }
    }

}
