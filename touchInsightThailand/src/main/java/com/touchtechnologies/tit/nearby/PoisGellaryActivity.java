package com.touchtechnologies.tit.nearby;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.touchtechnologies.command.insightthailand.RestServiceGetPoisCommand;
import com.touchtechnologies.dataobject.CCTV;
import com.touchtechnologies.dataobject.Hospitality;
import com.touchtechnologies.dataobject.insightthailand.PoiGallery;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.SettingActionbarActivity;
import com.touchtechnologies.tit.cctv.CCTVDetailActivity;
import com.touchtechnologies.tit.util.ResourceUtil;


import java.util.List;

/**
 * Created by TouchICS on 9/1/2016.
 */
public class PoisGellaryActivity extends SettingActionbarActivity implements AdapterView.OnItemClickListener {
   private Hospitality pois;
   private  ListView list;
   private PoisGellaryAdapter adapter;
   private View viewEmpty;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_list);

        pois = (Hospitality) getIntent().getSerializableExtra(Hospitality.class.getName());
        mTitle = pois.getName();


        list = (ListView) findViewById(R.id.list);
        viewEmpty =(View)findViewById(R.id.viewEmpty) ;
        viewEmpty.setVisibility(View.GONE);

        adapter = new PoisGellaryAdapter(this);
        list.setAdapter(adapter);
        list.setOnItemClickListener(this);
        getDetailPois();

    }

    public void getDetailPois() {

        String url = ResourceUtil.getServiceUrl(this) + getResources().getString(R.string.app_service_rest_poi) + pois.getProviderTypeKeyname() + "/" + pois.getIdHotel();
        new RestServiceGetPoisCommand(this, url) {

            protected void onPostExecute(Hospitality result) {
                pois = result;
                try{
                    if(pois.sizeGallery()==0){
                        viewEmpty.setVisibility(View.VISIBLE);
                    }else{
                        viewEmpty.setVisibility(View.GONE);
                    }
                    Log.i("POI Size",":"+pois.sizeGallery());

                    List<PoiGallery> poi = pois.getPoiGallery();
                    adapter.setData(poi);
                }catch (Exception e){
                    Log.i("Error",":"+e.getMessage());
                }


            }


        }.execute();


    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Intent intent = new Intent(this, PoiGalleryFullScreenActivity.class);
        intent.putExtra(PoiGallery.class.getName(), adapter.getItem(position));
        startActivity(intent);
    }

}
