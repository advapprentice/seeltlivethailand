package com.touchtechnologies.tit.nearby;

import com.touchtechnologies.dataobject.DataItem;
import com.touchtechnologies.dataobject.Image;
import com.touchtechnologies.json.JSONUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

/**
 * Created by surachet on 5/10/2559.
 */

public class Facebookplace extends DataItem implements Comparable<Facebookplace>{
    private double lat;
    private double lon;
    private int like;
    private int checkin;
    private String city;
    private String message;

    public Facebookplace(JSONObject jsonObject,double lat,double logi) throws JSONException {
        if(jsonObject != null){
            if(!jsonObject.isNull("id")){
                setId(JSONUtil.getString(jsonObject,"id"));

            }
            if(!jsonObject.isNull("name")){
                setName(JSONUtil.getString(jsonObject,"name"));
            }
            if(!jsonObject.isNull("location")){
                JSONObject jsonObject1 = jsonObject.getJSONObject("location");
                if(jsonObject1 != null){
                    if(!jsonObject1.isNull("latitude")&&!jsonObject1.isNull("longitude")){
                        setLat(jsonObject1.getDouble("latitude"));
                        setLon(jsonObject1.getDouble("longitude"));
                        setDistance(distance(lat,logi,jsonObject1.getDouble("latitude"),jsonObject1.getDouble("longitude")));
                    }
                    if(!jsonObject1.isNull("city")){
                        setCity(JSONUtil.getString(jsonObject1,"city"));
                    }
                }
            }
            if(!jsonObject.isNull("picture")){
                    setImage(new Image(jsonObject.getJSONObject("picture").getJSONObject("data").getString("url")));
            }
            if(!jsonObject.isNull("fan_count")){
                setLike(JSONUtil.getInt(jsonObject,"fan_count"));
            }
            if(!jsonObject.isNull("checkins")){
                setCheckin(JSONUtil.getInt(jsonObject,"checkins"));
            }
            if(!jsonObject.isNull("message")){
                setMessage(JSONUtil.getString(jsonObject,"message"));
            }



        }

    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public int getCheckin() {
        return checkin;
    }

    public void setCheckin(int checkin) {
        this.checkin = checkin;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    private String distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    @Override
    public int compareTo(Facebookplace another) {
        return another.getCheckin() - checkin;
    }
}
