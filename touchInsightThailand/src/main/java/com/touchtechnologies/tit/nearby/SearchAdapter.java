package com.touchtechnologies.tit.nearby;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.touchtechnologies.dataobject.Hospitality;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.dataobject.HotlinePhone;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;

/**
 * Created by TouchICS on 9/16/2016.
 */
public class SearchAdapter extends BaseAdapter implements Filterable {
    private static final int MAX_RESULTS = 10;
    private Context context;
    private Runnable runnable;
    protected ImageLoader imageLoader;
    protected DisplayImageOptions options;
    Timer t;
    private List<Hospitality> arrpoi = new ArrayList<Hospitality>();;
    List<Hospitality> search;
    private Hospitality poi;
    String result;
    String searchString;
    public SearchAdapter(Context context) {
        this.context = context;


        imageLoader = ImageLoader.getInstance();


        options = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.NONE)
                .showImageForEmptyUri(R.drawable.bg_cctvdefault)
                .showImageOnFail(R.drawable.bg_cctvdefault)
                //	.showImageOnLoading(R.drawable.bg_loading)
                .cacheInMemory(true)
                .displayer(new FadeInBitmapDisplayer(300)) //fade in images
                .resetViewBeforeLoading(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .memoryCache(new WeakMemoryCache())
                .threadPriority(Thread.NORM_PRIORITY)
                .denyCacheImageMultipleSizesInMemory()
                .build();

        imageLoader = ImageLoader.getInstance();
        if(!imageLoader.isInited()){
            imageLoader.init(config);
        }


    }

    public void setData(List<Hospitality> arrcctv){
        this.arrpoi = arrcctv;
        this.search = arrcctv;
       notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return arrpoi == null?0: arrpoi.size();
    }

    @Override
    public Hospitality getItem(int position) {
        return arrpoi.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_search_poi, parent, false);

            viewHolder = new ViewHolder();


            viewHolder.imgPoi = (ImageView)convertView.findViewById(R.id.imgPoi);
            viewHolder.txtNamePoi = (TextView)convertView.findViewById(R.id.txtNamePoi);

            convertView.setTag(viewHolder);


        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }
        poi = getItem(position);
        viewHolder.txtNamePoi.setText(getItem(position).getName());
        imageLoader.displayImage(getItem(position).getLogo(),viewHolder.imgPoi, options);

        return convertView;
    }

    @Override
    public Filter getFilter() {

        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((Hospitality) (resultValue)).getName();

            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                ArrayList<Hospitality> suggestions =new ArrayList<Hospitality>();
                for (Hospitality poi : search) {
                    if (poi.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(poi);
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {

                return new FilterResults();
            }
       }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            arrpoi = (List<Hospitality>) results.values;
            notifyDataSetChanged();

        }

    };



    class ViewHolder{
        ImageView imgPoi;
        TextView txtNamePoi;


    }



}