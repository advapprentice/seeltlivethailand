package com.touchtechnologies.tit.nearby;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Touch on 10/6/2016.
 */

public class FacebookDetail extends Activity implements View.OnClickListener {
    private Facebookplace fb;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
  //  private ActivityDetailFackbookBinding binding;
    private ListView list;
    private ImageView imgCover,imgExit;
    private TextView txtName,txtDistance,txtLike,txtCheckin,txtCity;
    private FacebookDetailTagedAdapter adapter;
    private List<Facebookplace> arrFb = new ArrayList<Facebookplace>();
    private View viewEmpty;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.item_list_empty);


        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.NONE)
                .showImageForEmptyUri(R.drawable.bg_cctvdefault)
                .showImageOnFail(R.drawable.bg_cctvdefault)
                .showImageOnLoading(R.drawable.bg_cctvdefault)
                .cacheInMemory(true)
                .displayer(new FadeInBitmapDisplayer(500)) //fade in images
                .resetViewBeforeLoading(true).build();
        imageLoader = ImageLoader.getInstance();

        fb = (Facebookplace) getIntent().getSerializableExtra(Facebookplace.class.getName());

        adapter = new FacebookDetailTagedAdapter(this);

        list = (ListView)findViewById(R.id.list);
        viewEmpty = (View)findViewById(R.id.viewEmpty);


        LayoutInflater myinflater = getLayoutInflater();
        ViewGroup myHeader = (ViewGroup)myinflater.inflate(R.layout.activity_detail_fackbook, list, false);

        imgExit =(ImageView)myHeader.findViewById(R.id.imgExit);
        imgCover =(ImageView)myHeader.findViewById(R.id.imgCover);
        txtName = (TextView)myHeader.findViewById(R.id.txtName);
        txtCity = (TextView)myHeader.findViewById(R.id.txtCity);
        txtDistance =(TextView)myHeader.findViewById(R.id.txtDistane);
        txtLike = (TextView)myHeader.findViewById(R.id.txtLike);
        txtCheckin = (TextView)myHeader.findViewById(R.id.txtCheckin);

        txtName.setText(fb.getName());
        txtDistance.setText(""+fb.getDistance()+" km");
        txtLike.setText(""+fb.getLike());
        txtCheckin.setText(""+fb.getCheckin());
        txtCity.setText(fb.getCity());


        imageLoader.displayImage(String.valueOf(fb.getImage().getUri()), imgCover, options);

        list.addHeaderView(myHeader, null, false);

        list.setAdapter(adapter);

        imgExit.setOnClickListener(this);

        getListTage();

    }

    public void getListTage(){

        String	url ="https://graph.facebook.com/" +fb.getId() +"/" +
                "tagged?access_token=EAAX0NmD7gWABAFFx51sZCReS3iOvFtZA9xFHyZBSXZCI2mHYRJrjFofwOAeOE7Y61uxiuXnnkZAdVS9PPjsikZCusaFYU" +
                "snQclTIY6zgzXFIhRdtgfNgDZBxOZCVTauUDKmMNT9tQIu2kzUFG5vyPC7AKiD8CIlbd0QZD&fields=message,created_time,id,name";

        Log.i("========Json==========","Response"+fb.getId()+"\n:"+url);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject obj) {

                 Log.i("Json","Response"+obj.toString());

                try {
                    JSONArray jsonArray = obj.getJSONArray("data");

                    if(jsonArray.length()==0){
                        viewEmpty.setVisibility(View.VISIBLE);
                    }else {
                        viewEmpty.setVisibility(View.GONE);
                    }

                    if(jsonArray != null){
                        for(int i=0;jsonArray.length()> i;i++){

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            Facebookplace facebookplace = new Facebookplace(jsonObject1,fb.getLat(),fb.getLon());
                            arrFb.add(facebookplace);

                        }
                         adapter.setData(arrFb);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });

        ((MainApplication) getApplicationContext()).addToRequestQueue(jsonRequest);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgExit:
                finish();
                break;
        }

    }
}
