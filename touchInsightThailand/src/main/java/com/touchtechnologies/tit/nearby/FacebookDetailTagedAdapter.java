package com.touchtechnologies.tit.nearby;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.opensource.pagerindicator.CircleBitmapDisplayer;
import com.touchtechnologies.dataobject.Hospitality;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import com.touchtechnologies.dataobject.insightthailand.UserFacebook;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Touch on 10/6/2016.
 */

public class FacebookDetailTagedAdapter extends BaseAdapter {
    private static final int MAX_RESULTS = 10;
    private Context context;
    private Runnable runnable;
    protected ImageLoader imageLoader;
    protected DisplayImageOptions options;
    Timer t;
    private List<Facebookplace> arrpoi = new ArrayList<Facebookplace>();
    ;
    List<Hospitality> search;
    private Hospitality poi;
    String result;
    String searchString;
    UserFacebook user;
    public FacebookDetailTagedAdapter(Context context) {
        this.context = context;

        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.NONE)
                .showImageForEmptyUri(R.drawable.bg_circle)
                .showImageOnFail(R.drawable.bg_circle)
                .cacheInMemory(true)
                .displayer(new CircleBitmapDisplayer())
                .resetViewBeforeLoading(true).build();
        imageLoader = ImageLoader.getInstance();


    }

    public void setData(List<Facebookplace> arrcctv) {
        this.arrpoi = arrcctv;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return arrpoi == null ? 0 : arrpoi.size();
    }

    @Override
    public Facebookplace getItem(int position) {
        return arrpoi.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_facebook_tage, parent, false);

            viewHolder = new ViewHolder();

            viewHolder.txtComment = (TextView) convertView.findViewById(R.id.txtComment);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.txtName);
            viewHolder.imgUser = (ImageView)convertView.findViewById(R.id.imgUser);


            convertView.setTag(viewHolder);


        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Log.i(":","Message"+getItem(position).getMessage());
       // viewHolder.txtName.setText(getItem(position).getName());
        viewHolder.txtComment.setText(getItem(position).getMessage());

        String idFb = getItem(position).getId();

        String id = idFb.substring(0,15);

        Log.i("id",":"+id);


        String	url ="https://graph.facebook.com/" + id +
                "?access_token=EAAX0NmD7gWABAFFx51sZCReS3iOvFtZA9xFHyZBSXZCI2mHYRJrjFofwOAeOE7Y61uxiuXnnkZAdVS9PPjsikZCusaFYUsnQclTIY6zgzXFIhRdtgfNgDZBxOZCVTauUDKmMNT9tQIu2kzUFG5vyPC7AKiD8CIlbd0QZD&fields=name,id,picture.height(200)";

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject obj) {

                Log.i("Json","Response"+obj.toString());

                try {
                   user = new UserFacebook(obj);

                    viewHolder.txtName.setText(user.getName());
                    imageLoader.displayImage(user.getUrlPicture(), viewHolder.imgUser, options);
                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });

        ((MainApplication) getApplicationContext()).addToRequestQueue(jsonRequest);

     //   imageLoader.displayImage(getItem(position).setName(user.getUrlPicture()), viewHolder.imgUser, options);



        return convertView;
    }


    class ViewHolder {
        ImageView imgUser;
        TextView txtName,txtComment;


    }
}
