package com.touchtechnologies.tit.nearby;

import android.app.ActionBar;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.touchtechnologies.dataobject.Hospitality;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.checkin.CheckInFragment;
import com.touchtechnologies.tit.checkin.Fragmentmapcheckin;
import com.touchtechnologies.tit.util.GPSTrackers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FacebookNearbyActivity extends FragmentActivity {
    int page = 0;
    private GoogleMap googleMap;
    private GoogleApiClient client;
    private double latitude;
    private double longitude;
    private int mapZoomLevel = 14;
    private View viewSearch;
    private View viewDelete;
    private AutoCompleteTextView autocomplete;
    private SearchAdapter dataAdapter;
    private NearbyPoiInfoMapAdapter adapterInfo;
    private HashMap<Marker, Hospitality> hMarkers;
    protected Location location;
    private List<Hospitality> arrPois = new ArrayList<Hospitality>();
    private ArrayList<Facebookplace> facebookplaces = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_nearby);


        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        //actionBar.setBackgroundDrawable(new ColorDrawable(0xFF1275AE));
        actionBar.setBackgroundDrawable(new ColorDrawable(0xFF000000));
        actionBar.setTitle(Html.fromHtml("<font color='#FFFFFF'>"+"Social near by"+"</font>"));
        actionBar.setIcon( new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        actionBar.setDisplayUseLogoEnabled(false);


        Fragment fragment;
        FragmentManager manager = getSupportFragmentManager();
        fragment = FacebookNearbyFragment.newInstance();
        manager.beginTransaction().replace(R.id.framelayout_fragment, fragment).commit();
        GPSTrackers mGPS = new GPSTrackers(this);
        if (!mGPS.canGetLocation()) {
            mGPS.showSettingsAlert();
        } else {
            mGPS.getLocation();
            latitude = mGPS.getLatitude();
            longitude = mGPS.getLongitude();
            Log.i("Lat =>",":"+latitude+"\nLong =>"+longitude);
        }
//        hMarkers = new HashMap<>();
//        adapterInfo = new NearbyPoiInfoMapAdapter(this);
//        adapterInfo.setMarker(hMarkers);
//
//        GPSTrackers mGPS = new GPSTrackers(this);
//        if (!mGPS.canGetLocation()) {
//            mGPS.showSettingsAlert();
//        } else {
//            mGPS.getLocation();
//            latitude = mGPS.getLatitude();
//            longitude = mGPS.getLongitude();
//            Log.i("Lat =>",":"+latitude+"\nLong =>"+longitude);
//        }
//        viewSearch =findViewById(R.id.viewSearch);
//        viewDelete =findViewById(R.id.viewDelete);
//
//        autocomplete =(AutoCompleteTextView)findViewById(R.id.autoCompleteTextView1);
//
//
//        dataAdapter = new SearchAdapter(this);
//        autocomplete.setThreshold(1);
//        autocomplete.setAdapter(dataAdapter);
//        autocomplete.setOnItemClickListener(this);
//        autocomplete.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                if(s.length() != 0){
//                    viewDelete.setVisibility(View.VISIBLE);
//                    viewSearch.setVisibility(View.GONE);
//                }else {
//                    viewSearch.setVisibility(View.VISIBLE);
//                    viewDelete.setVisibility(View.GONE);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//
//        viewDelete.setOnClickListener(this);
//        getnearbyFacebook();
        getnearbyFacebook();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(page == 0){
			getMenuInflater().inflate(R.menu.fragmentmapcheckin, menu);
		}else {
			getMenuInflater().inflate(R.menu.termofservice, menu);
		}
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Fragment fragment;
        FragmentManager manager = getSupportFragmentManager();
        if(item.getItemId() == R.id.icmapcheckin){
            fragment = FacebookNearListFragment.newInstance(facebookplaces);
            findViewById(R.id.fragmentCheckInFacbook).setVisibility(View.GONE);
            findViewById(R.id.framelayout_fragment).setVisibility(View.VISIBLE);
            //findViewById(R.id.icmapcheckin).setVisibility(View.GONE);
            manager.beginTransaction().replace(R.id.framelayout_fragment, fragment).commit();

            page =1;
            invalidateOptionsMenu();
            //onPrepareOptionsMenu(null);
        }else if(item.getItemId() == R.id.icmapcheckin2){
            fragment = FacebookNearbyFragment.newInstance();
            findViewById(R.id.fragmentCheckInFacbook).setVisibility(View.VISIBLE);
            findViewById(R.id.framelayout_fragment).setVisibility(View.GONE);
            //findViewById(R.id.icmapcheckin).setVisibility(View.VISIBLE);
            manager.beginTransaction().replace(R.id.fragmentCheckInFacbook, fragment).commit();
            page =0;
            //onPrepareOptionsMenu(null);
            invalidateOptionsMenu();
        }else if(item.getItemId() == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public void getnearbyFacebook(){
        String url = "https://graph.facebook.com/search?" +
                "type=place&center="+latitude+","+longitude+"&distance=1000&" +
                "access_token=EAAX0NmD7gWABAFFx51sZCReS3iOvFtZA9xFHyZBSXZCI2mHYRJrjFofwOAeOE7Y61uxi" +
                "uXnnkZAdVS9PPjsikZCusaFYUsnQclTIY6zgzXFIhRdtgfNgDZBxOZCVTauUDKmMNT9tQIu2kzUFG5vy" +
                "PC7AKiD8CIlbd0QZD&fields=name,fan_count,talking_about_count,location" +
                ",checkins,category,category_list,picture.height(500)";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                arrPois.clear();
                Log.d(NearByMapFragment.class.getSimpleName(), "onResponse: "+jsonObject.toString());
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    if(jsonArray != null){
                        for(int i=0;jsonArray.length()> i;i++){
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            Hospitality hospitality = new Hospitality();
                            Facebookplace facebookplace = new Facebookplace(jsonObject1,latitude,longitude);
                            hospitality.setLogo(facebookplace.getImage().getUri());
                            hospitality.setName(facebookplace.getName());
                            hospitality.setLatitude(facebookplace.getLat());
                            hospitality.setLongitude(facebookplace.getLon());
                            hospitality.setProviderTypeKeyname("hotel");
                            hospitality.setDistance(facebookplace.getDistance());
                            arrPois.add(hospitality);
                            facebookplaces.add(facebookplace);
                        }
                       // onUpdatemap(arrPois,null);
                       // dataAdapter.setData(arrPois);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        MainApplication.getInstance().addToRequestQueue(jsonObjectRequest);



    }







}
