package com.touchtechnologies.tit.nearby;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.touchtechnologies.dataobject.CCTVHighlight;
import com.touchtechnologies.dataobject.Hospitality;
import com.touchtechnologies.dataobject.Image;
import com.touchtechnologies.dataobject.ImageGallery;
import com.touchtechnologies.dataobject.insightthailand.PoiGallery;
import com.touchtechnologies.tit.GalleryAdapter;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.cctvitlive.history.CCTVHightlighFullScreenActivity;

import java.util.List;
import java.util.Timer;

/**
 * Created by TouchICS on 9/1/2016.
 */
public class PoisGellaryAdapter extends BaseAdapter {
    private Context context;
    private Runnable runnable;
    protected ImageLoader imageLoader;
    protected DisplayImageOptions options;
    Timer t;
    private List<Hospitality> arrcctv;
    private Hospitality pois;
    private List<PoiGallery> gallery;

    String url;
    public PoisGellaryAdapter(Context context) {
        this.context = context;
        imageLoader = ImageLoader.getInstance();

        options = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.NONE)
                .showImageForEmptyUri(R.drawable.bg_cctvdefault)
                .showImageOnFail(R.drawable.bg_cctvdefault)
                .showImageOnLoading(R.drawable.bg_cctvdefault)
                .cacheInMemory(true)
                .displayer(new FadeInBitmapDisplayer(100)) //fade in images
                .resetViewBeforeLoading(true)
                .build();


        imageLoader = ImageLoader.getInstance();


    }
    public void setData(List<PoiGallery> gallery){
        this.gallery =  gallery;

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return gallery == null ? 0 : gallery.size();
    }

    @Override
    public PoiGallery getItem(int position) {
        return gallery.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_poi_nearby, parent, false);
             viewHolder = new ViewHolder();

            viewHolder.imglive =(ImageView)convertView.findViewById(R.id.imgDisplay);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }


        imageLoader.displayImage(getItem(position).getThumbnail(), viewHolder.imglive, options);


        return convertView;
    }



    class ViewHolder{
        ImageView imgshare;
        ImageView imglive;
        View viewHighlight;

    }



}