package com.touchtechnologies.tit.nearby;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
//import com.nostra13.universalimageloader.core.assist.FailReason;
//import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.touchtechnologies.dataobject.Hospitality;
import com.touchtechnologies.dataobject.Image;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.databinding.ItemListNearbyfacebookBinding;
import com.touchtechnologies.tit.util.GPSTrackers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;



public class FacebookNearListFragment extends Fragment implements AdapterView.OnItemClickListener,View.OnClickListener {

    ArrayList<Facebookplace> facebookplaces;
    ArrayList<Hospitality> arrpoi;
    ArrayList<Facebookplace> facebookplaces2;
    FacebooknearbyListAdapter facebooknearbyListAdapter;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private double latitude,longitude;
    private ImageView imageViewhotel,imageViewres,imageViewshop,imageViewGas,imageViewAll
            ,imageViewPolice,imageViewBuffet,imageViewCoffe,imageViewParking;
    private HashMap<Hospitality,Facebookplace> hospitalityHashMap;
   // private SearchAdapter dataAdapter;
    private AutoCompleteTextView autocomplete;
    private String searchString;
    private ArrayList<Facebookplace> search;
    private View viewSearch;
    private View viewDelete;
    public FacebookNearListFragment() {

    }


    public static FacebookNearListFragment newInstance(ArrayList<Facebookplace> facebookplaces) {
        FacebookNearListFragment fragment = new FacebookNearListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("face",facebookplaces);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        facebookplaces = new ArrayList<>();
        facebooknearbyListAdapter = new FacebooknearbyListAdapter();
        if(getArguments() != null){
            facebookplaces = (ArrayList<Facebookplace>) getArguments().getSerializable("face");
            if(facebookplaces == null){
               getnearbyFacebook("");
            }else if(facebookplaces.size() ==0 ) {
                getnearbyFacebook("");
            }
        }else {
            getnearbyFacebook("");
        }


//
        facebookplaces2 = new ArrayList<>();
        arrpoi = new ArrayList<>();
        search = new ArrayList<>();
        hospitalityHashMap = new HashMap<>();
        GPSTrackers mGPS = new GPSTrackers(getActivity());
        if (!mGPS.canGetLocation()) {
            mGPS.showSettingsAlert();
        } else {
            mGPS.getLocation();
            latitude = mGPS.getLatitude();
            longitude = mGPS.getLongitude();
            Log.i("Lat =>",":"+latitude+"\nLong =>"+longitude);
        }
        imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .threadPoolSize(3)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCacheSize(4194304)
                .memoryCache(new WeakMemoryCache())
                .defaultDisplayImageOptions(options).build();

        imageLoader.init(config);
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .imageScaleType(ImageScaleType.NONE)
                .showImageForEmptyUri(R.drawable.ic_new_imgaccount)
                .showImageOnFail(R.drawable.ic_new_imgaccount)
                .cacheInMemory(true)
                .displayer(new FadeInBitmapDisplayer(1000))
              //  .displayer(new CircleBitmapDisplayer())
                .build();
            setHasOptionsMenu(true);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      View rootview =   inflater.inflate(R.layout.fragment_facebook_near_list, container, false);
        ListView listView = (ListView) rootview.findViewById(R.id.list_view_near_facebook);
        listView.setAdapter(facebooknearbyListAdapter);
        listView.setOnItemClickListener(this);
        listView.setEmptyView(rootview.findViewById(R.id.emptylistview));
        viewSearch = rootview.findViewById(R.id.viewSearch);
        viewDelete = rootview.findViewById(R.id.viewDelete);
        autocomplete = (AutoCompleteTextView)rootview.findViewById(R.id.autoCompleteTextView1);
        rootview.findViewById(R.id.viewHotel).setOnClickListener(this);
        rootview.findViewById(R.id.viewRestaurent).setOnClickListener(this);
        rootview.findViewById(R.id.viewShopingmall).setOnClickListener(this);
        rootview.findViewById(R.id.viewCoffe).setOnClickListener(this);
        rootview.findViewById(R.id.viewBuffet).setOnClickListener(this);
        rootview.findViewById(R.id.viewAll).setOnClickListener(this);
        rootview.findViewById(R.id.viewGasStation).setOnClickListener(this);
        rootview.findViewById(R.id.viewPolicStation).setOnClickListener(this);
        rootview.findViewById(R.id.viewParking).setOnClickListener(this);
        imageViewAll = (ImageView) rootview.findViewById(R.id.imgAll);
        imageViewBuffet = (ImageView) rootview.findViewById(R.id.imgBuffet);
        imageViewCoffe =(ImageView) rootview.findViewById(R.id.imgCoffe);
        imageViewGas = (ImageView) rootview.findViewById(R.id.imgGasStation);
        imageViewhotel = (ImageView) rootview.findViewById(R.id.imgHotel);
        imageViewParking = (ImageView) rootview.findViewById(R.id.imgParking);
        imageViewPolice = (ImageView) rootview.findViewById(R.id.imgPolicStaion);
        imageViewshop = (ImageView) rootview.findViewById(R.id.imgShopingmall);
        imageViewres = (ImageView) rootview.findViewById(R.id.imgRestuarent);
//        dataAdapter = new SearchAdapter(getActivity());
//        autocomplete.setThreshold(1);
//        autocomplete.setAdapter(dataAdapter);
//        autocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Facebookplace selection = hospitalityHashMap.get(dataAdapter.getItem(position));
//                Intent intent  = new Intent(getActivity(),FacebookDetail.class);
//                intent.putExtra(Facebookplace.class.getName(), selection);
//                startActivity(intent);
//            }
//        });
        autocomplete.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                facebooknearbyListAdapter.getFilter().filter(s);
                if(s.length() != 0){
                    viewDelete.setVisibility(View.VISIBLE);
                    viewSearch.setVisibility(View.GONE);
                }else {
                    viewSearch.setVisibility(View.VISIBLE);
                    viewDelete.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 0){
                    facebookplaces.addAll(facebookplaces2);
                    facebooknearbyListAdapter.notifyDataSetChanged();
                }
            }
        });
        viewDelete.setOnClickListener(this);

        return rootview;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mostcheckin:
                Collections.sort(facebookplaces);
                facebooknearbyListAdapter.notifyDataSetChanged();
                break;
            case R.id.distance:
                sortbydistance();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sortbydistance(){
        Collections.sort(facebookplaces, new Comparator<Facebookplace>() {
            @Override
            public int compare(Facebookplace lhs, Facebookplace rhs) {
                Double a = Double.parseDouble(rhs.getDistance());
                Double b = Double.parseDouble(lhs.getDistance());
                return b.compareTo(a);
            }
        });
        facebooknearbyListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    public void getnearbyFacebook(String q){
        String url = "https://graph.facebook.com/search?" +
                "type=place&center="+latitude+","+longitude+"&distance=1000&" +
                "access_token=EAAX0NmD7gWABAFFx51sZCReS3iOvFtZA9xFHyZBSXZCI2mHYRJrjFofwOAeOE7Y61uxi" +
                "uXnnkZAdVS9PPjsikZCusaFYUsnQclTIY6zgzXFIhRdtgfNgDZBxOZCVTauUDKmMNT9tQIu2kzUFG5vy" +
                "PC7AKiD8CIlbd0QZD&q="+q+"&fields=name,fan_count,talking_about_count,location" +
                ",checkins,category,category_list,picture.height(500)";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                arrpoi.clear();
               // facebookplaces.clear();
                if(facebookplaces != null){
                    facebookplaces.clear();
                }
                Log.d(NearByMapFragment.class.getSimpleName(), "onResponse: "+jsonObject.toString());
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    if(jsonArray != null){
                        for(int i=0;jsonArray.length()> i;i++){
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            Hospitality hospitality = new Hospitality();
                            Facebookplace facebookplace = new Facebookplace(jsonObject1,latitude,longitude);
                            hospitality.setLogo(facebookplace.getImage().getUri());
                            hospitality.setName(facebookplace.getName());
                            hospitality.setLatitude(facebookplace.getLat());
                            hospitality.setLongitude(facebookplace.getLon());
                            hospitality.setProviderTypeKeyname("hotel");
                            hospitality.setDistance(facebookplace.getDistance());
                            arrpoi.add(hospitality);
                            facebookplaces.add(facebookplace);
                            hospitalityHashMap.put(hospitality,facebookplace);
                        }
                        Collections.sort(facebookplaces);
                        facebooknearbyListAdapter.notifyDataSetChanged();
                        search.addAll(facebookplaces);
                        facebookplaces2.addAll(facebookplaces);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        MainApplication.getInstance().addToRequestQueue(jsonObjectRequest);



    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        Facebookplace selection = facebooknearbyListAdapter.getItem(position);
        //  String selection = (String)  arrPois.get(parent.getSelectedItemPosition()).getId();
        // String selection = (String) arrPois.get(position).getName();
     //   Toast.makeText(getActivity(),"Select "+ position +" " + selection.getName(),Toast.LENGTH_SHORT).show();
        Intent intent  = new Intent(getActivity(),FacebookDetail.class);
        intent.putExtra(Facebookplace.class.getName(), selection);
        startActivity(intent);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewAll:
                imageViewres.setImageResource(R.drawable.f_restaurant);
                imageViewAll.setImageResource(R.drawable.all_hover);
                imageViewshop.setImageResource(R.drawable.shoppingmall);
                imageViewPolice.setImageResource(R.drawable.police_station);
                imageViewParking.setImageResource(R.drawable.parking);
                imageViewBuffet.setImageResource(R.drawable.buffet);
                imageViewCoffe.setImageResource(R.drawable.coffee);
                imageViewGas.setImageResource(R.drawable.gas_station);
                imageViewhotel.setImageResource(R.drawable.f_hotel);
                getnearbyFacebook("");
                break;
            case R.id.viewHotel:
                imageViewres.setImageResource(R.drawable.f_restaurant);
                imageViewAll.setImageResource(R.drawable.all);
                imageViewshop.setImageResource(R.drawable.shoppingmall);
                imageViewPolice.setImageResource(R.drawable.police_station);
                imageViewParking.setImageResource(R.drawable.parking);
                imageViewBuffet.setImageResource(R.drawable.buffet);
                imageViewCoffe.setImageResource(R.drawable.coffee);
                imageViewGas.setImageResource(R.drawable.gas_station);
                imageViewhotel.setImageResource(R.drawable.f_hotel_hover);
                getnearbyFacebook("Hotel");
                break;
            case R.id.viewRestaurent:
                imageViewres.setImageResource(R.drawable.f_restaurant_hover);
                imageViewAll.setImageResource(R.drawable.all);
                imageViewshop.setImageResource(R.drawable.shoppingmall);
                imageViewPolice.setImageResource(R.drawable.police_station);
                imageViewParking.setImageResource(R.drawable.parking);
                imageViewBuffet.setImageResource(R.drawable.buffet);
                imageViewCoffe.setImageResource(R.drawable.coffee);
                imageViewGas.setImageResource(R.drawable.gas_station);
                imageViewhotel.setImageResource(R.drawable.f_hotel);
                getnearbyFacebook("Restaurent");
                break;
            case R.id.viewCoffe:
                imageViewres.setImageResource(R.drawable.f_restaurant);
                imageViewAll.setImageResource(R.drawable.all);
                imageViewshop.setImageResource(R.drawable.shoppingmall);
                imageViewPolice.setImageResource(R.drawable.police_station);
                imageViewParking.setImageResource(R.drawable.parking);
                imageViewBuffet.setImageResource(R.drawable.buffet);
                imageViewCoffe.setImageResource(R.drawable.coffee_hover);
                imageViewGas.setImageResource(R.drawable.gas_station);
                imageViewhotel.setImageResource(R.drawable.f_hotel);
                getnearbyFacebook("Coffee");
                break;
            case R.id.viewShopingmall:
                imageViewres.setImageResource(R.drawable.f_restaurant);
                imageViewAll.setImageResource(R.drawable.all);
                imageViewshop.setImageResource(R.drawable.shoppingmall_hover);
                imageViewPolice.setImageResource(R.drawable.police_station);
                imageViewParking.setImageResource(R.drawable.parking);
                imageViewBuffet.setImageResource(R.drawable.buffet);
                imageViewCoffe.setImageResource(R.drawable.coffee);
                imageViewGas.setImageResource(R.drawable.gas_station);
                imageViewhotel.setImageResource(R.drawable.f_hotel);
                getnearbyFacebook("ShoppingMall");
                break;
            case R.id.viewParking:
                imageViewres.setImageResource(R.drawable.f_restaurant);
                imageViewAll.setImageResource(R.drawable.all);
                imageViewshop.setImageResource(R.drawable.shoppingmall);
                imageViewPolice.setImageResource(R.drawable.police_station);
                imageViewParking.setImageResource(R.drawable.parking_hover);
                imageViewBuffet.setImageResource(R.drawable.buffet);
                imageViewCoffe.setImageResource(R.drawable.coffee);
                imageViewGas.setImageResource(R.drawable.gas_station);
                imageViewhotel.setImageResource(R.drawable.f_hotel);
                getnearbyFacebook("Parking");
                break;
            case R.id.viewPolicStation:
                imageViewres.setImageResource(R.drawable.f_restaurant);
                imageViewAll.setImageResource(R.drawable.all);
                imageViewshop.setImageResource(R.drawable.shoppingmall);
                imageViewPolice.setImageResource(R.drawable.police_station_hover);
                imageViewParking.setImageResource(R.drawable.parking);
                imageViewBuffet.setImageResource(R.drawable.buffet);
                imageViewCoffe.setImageResource(R.drawable.coffee);
                imageViewGas.setImageResource(R.drawable.gas_station);
                imageViewhotel.setImageResource(R.drawable.f_hotel);
                getnearbyFacebook("PoliceStation");
                break;
            case R.id.viewBuffet:
                imageViewres.setImageResource(R.drawable.f_restaurant);
                imageViewAll.setImageResource(R.drawable.all);
                imageViewshop.setImageResource(R.drawable.shoppingmall);
                imageViewPolice.setImageResource(R.drawable.police_station);
                imageViewParking.setImageResource(R.drawable.parking);
                imageViewBuffet.setImageResource(R.drawable.buffet_hover);
                imageViewCoffe.setImageResource(R.drawable.coffee);
                imageViewGas.setImageResource(R.drawable.gas_station);
                imageViewhotel.setImageResource(R.drawable.f_hotel);
                getnearbyFacebook("Buffet");
                break;
            case R.id.viewGasStation:
                imageViewres.setImageResource(R.drawable.f_restaurant);
                imageViewAll.setImageResource(R.drawable.all);
                imageViewshop.setImageResource(R.drawable.shoppingmall);
                imageViewPolice.setImageResource(R.drawable.police_station);
                imageViewParking.setImageResource(R.drawable.parking);
                imageViewBuffet.setImageResource(R.drawable.buffet);
                imageViewCoffe.setImageResource(R.drawable.coffee);
                imageViewGas.setImageResource(R.drawable.gas_station_hover);
                imageViewhotel.setImageResource(R.drawable.f_hotel);
                getnearbyFacebook("GasStation");
                break;
            case R.id.viewDelete:

                autocomplete.setText("");
                break;
        }

    }

    private class FacebooknearbyListAdapter extends BaseAdapter implements Filterable{
        ItemListNearbyfacebookBinding binding;

        @Override
        public int getCount() {
            if(facebookplaces == null)
                return 0;
            else
                return facebookplaces.size();
        }

        @Override
        public Facebookplace getItem(int position) {
            return facebookplaces.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                binding = DataBindingUtil.inflate(inflater,R.layout.item_list_nearbyfacebook,parent,false);
                convertView=binding.getRoot();

            }else {
                binding = (ItemListNearbyfacebookBinding) convertView.getTag();
            }
            imageLoader.displayImage(facebookplaces.get(position).getImage().getUri(),binding.imgPlace,options);
            binding.Name.setText(facebookplaces.get(position).getName());
            binding.Location.setText(facebookplaces.get(position).getCity());
            binding.txtDistance.setText(facebookplaces.get(position).getDistance());
            if(facebookplaces.get(position).getLike() > 1000 && facebookplaces.get(position).getLike() < 1000000){
                binding.txtLikeCount.setText((facebookplaces.get(position).getLike()/1000)+"k");
            }else if(facebookplaces.get(position).getLike() > 1000000){
                binding.txtLikeCount.setText((facebookplaces.get(position).getLike()/1000000)+"m");
            }else {
                binding.txtLikeCount.setText(facebookplaces.get(position).getLike()+"");
            }
            if(facebookplaces.get(position).getCheckin() > 1000 && facebookplaces.get(position).getCheckin() < 1000000){
                binding.txtCheckinCount.setText((facebookplaces.get(position).getCheckin()/1000)+"k");
            }else if(facebookplaces.get(position).getCheckin() > 1000000){
                binding.txtCheckinCount.setText((facebookplaces.get(position).getCheckin()/1000000)+"m");
            }else {
                binding.txtCheckinCount.setText(facebookplaces.get(position).getCheckin()+"");
            }


            convertView.setTag(binding);
            return convertView;
        }

        @Override
        public Filter getFilter() {
            Filter mFilter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults results = new FilterResults();
                    ArrayList<Facebookplace> FilteredArrayNames = new ArrayList<Facebookplace>();

                    // perform your search here using the searchConstraint String.

                    constraint = constraint.toString().toLowerCase().trim();
                    if (constraint != null && constraint.length() > 0) {
                        searchString = (String)constraint;
                        for (int i = 0; i < search.size(); i++) {
                            String dataNames = search.get(i).getName().toString().trim();
                            if (dataNames.toLowerCase().contains(constraint.toString())) {
                                FilteredArrayNames.add(search.get(i));
                            }
                        }
                        results.count = FilteredArrayNames.size();
                        results.values = FilteredArrayNames;
                        // Log.e("VALUES", results.values.toString());
                    }else {
                        results.count = search.size();
                        results.values = search;
                    }
                    return results;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    facebookplaces = (ArrayList<Facebookplace>) results.values;
                    notifyDataSetChanged();

                }
            };

            return mFilter;
        }
    }



}
