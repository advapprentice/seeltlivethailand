package com.touchtechnologies.tit.nearby;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.touchtechnologies.dataobject.Hospitality;
import com.touchtechnologies.dataobject.Image;
import com.touchtechnologies.tit.MainApplication;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.databinding.ItemFacenearMapInfoBinding;
import com.touchtechnologies.tit.util.GPSTrackers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class FacebookNearbyFragment extends Fragment implements View.OnClickListener,GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener,AdapterView.OnItemClickListener, OnMapReadyCallback {

    private DisplayImageOptions options;
    Facebookplace his;
//    Marker marker;
    private HashMap<Marker, View> hViews;
    private  ImageLoader imageLoader;
    private GoogleMap googleMap;
    private GoogleApiClient client;
    private double latitude;
    private double longitude;
    private int mapZoomLevel = 14;
    private FacebooknearInfomapadapter adapterInfo;
    private HashMap<Marker, Facebookplace> hMarkers;
    private HashMap<Hospitality,Facebookplace> hospitalityHashMap;
    protected Location location;
    private List<Hospitality> arrPois = new ArrayList<Hospitality>();
    private ArrayList<Facebookplace> facebookplaces = new ArrayList<>();
    private View viewSearch;
    private View viewDelete;
    private AutoCompleteTextView autocomplete;
    private SearchAdapter dataAdapter;
    View rootview;
    private LayoutInflater inflater;
    private ImageView imageViewhotel,imageViewres,imageViewshop,imageViewGas,imageViewAll
                        ,imageViewPolice,imageViewBuffet,imageViewCoffe,imageViewParking;

    public FacebookNearbyFragment() {
        // Required empty public constructor
    }


    public static FacebookNearbyFragment newInstance() {
        FacebookNearbyFragment fragment = new FacebookNearbyFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hMarkers = new HashMap<>();
        hospitalityHashMap = new HashMap<>();
        adapterInfo = new FacebooknearInfomapadapter();

        GPSTrackers mGPS = new GPSTrackers(getActivity());
        if (!mGPS.canGetLocation()) {
            mGPS.showSettingsAlert();
        } else {
            mGPS.getLocation();
            latitude = mGPS.getLatitude();
            longitude = mGPS.getLongitude();
            Log.i("Lat =>",":"+latitude+"\nLong =>"+longitude);
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        SupportMapFragment MapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        MapFragment.getMapAsync(this);
        googleMap.setMyLocationEnabled(true);
        googleMap.setOnInfoWindowClickListener(this);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setInfoWindowAdapter(adapterInfo);

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude ,longitude),mapZoomLevel));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this. inflater = inflater;
        rootview= inflater.inflate(R.layout.fragment_facebook_nearby, container, false);
        viewSearch = rootview.findViewById(R.id.viewSearch);
        viewDelete = rootview.findViewById(R.id.viewDelete);

        autocomplete = (AutoCompleteTextView)rootview.findViewById(R.id.autoCompleteTextView1);




        dataAdapter = new SearchAdapter(getActivity());
        autocomplete.setThreshold(1);
        autocomplete.setAdapter(dataAdapter);
        autocomplete.setOnItemClickListener(this);
        autocomplete.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length() != 0){
                    viewDelete.setVisibility(View.VISIBLE);
                    viewSearch.setVisibility(View.GONE);
                }else {
                    viewSearch.setVisibility(View.VISIBLE);
                    viewDelete.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        viewDelete.setOnClickListener(this);

        rootview.findViewById(R.id.viewHotel).setOnClickListener(this);
        rootview.findViewById(R.id.viewRestaurent).setOnClickListener(this);
        rootview.findViewById(R.id.viewShopingmall).setOnClickListener(this);
        rootview.findViewById(R.id.viewCoffe).setOnClickListener(this);
        rootview.findViewById(R.id.viewBuffet).setOnClickListener(this);
        rootview.findViewById(R.id.viewAll).setOnClickListener(this);
        rootview.findViewById(R.id.viewGasStation).setOnClickListener(this);
        rootview.findViewById(R.id.viewPolicStation).setOnClickListener(this);
        rootview.findViewById(R.id.viewParking).setOnClickListener(this);
        imageViewAll = (ImageView) rootview.findViewById(R.id.imgAll);
        imageViewBuffet = (ImageView) rootview.findViewById(R.id.imgBuffet);
        imageViewCoffe =(ImageView) rootview.findViewById(R.id.imgCoffe);
        imageViewGas = (ImageView) rootview.findViewById(R.id.imgGasStation);
        imageViewhotel = (ImageView) rootview.findViewById(R.id.imgHotel);
        imageViewParking = (ImageView) rootview.findViewById(R.id.imgParking);
        imageViewPolice = (ImageView) rootview.findViewById(R.id.imgPolicStaion);
        imageViewshop = (ImageView) rootview.findViewById(R.id.imgShopingmall);
        imageViewres = (ImageView) rootview.findViewById(R.id.imgRestuarent);

        getnearbyFacebook("");
        return rootview;
    }


    void onUpdatemap(List<Facebookplace> list, Facebookplace hospi) {

        googleMap.clear();
        if (googleMap == null) {
            return ;
        }

        try {
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            for (Facebookplace poi : list) {

                MarkerOptions markerOptions = new MarkerOptions()
                        .title(poi.getName())
                        .snippet(poi.getDistance() + " Km")
                        .position(new LatLng(poi.getLat(), poi.getLon()));
//                if (poi.getProviderTypeKeyname().equals("hotel")) {
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.f_pin));
//                } else if (poi.getProviderTypeKeyname().equals("attraction")) {
//                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.new_pin_att1));
//                } else if (poi.getProviderTypeKeyname().equals("restaurant")) {
//                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.new_pin_res1));
//                }


                Marker tp = googleMap.addMarker(markerOptions);
                hMarkers.put(tp, poi);
            }

            if(hospi!=null) {
                MarkerOptions markerOptions = new MarkerOptions()
                        .title(hospi.getName())
                        .snippet(hospi.getDistance() + " Km")
                        .position(new LatLng(hospi.getLat(), hospi.getLon()));
//                if (hospi.getProviderTypeKeyname().equals("hotel")) {
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.f_pin_hover));
//                } else if (hospi.getProviderTypeKeyname().equals("attraction")) {
//                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.new_pin_att2));
//                } else if (hospi.getProviderTypeKeyname().equals("restaurant")) {
//                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.new_pin_res2));
//                }

                Marker tp2 = googleMap.addMarker(markerOptions);
                hMarkers.put(tp2, hospi);
                tp2.showInfoWindow();
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(hospi.getLat() ,hospi.getLon()),mapZoomLevel));

            }



        } catch (Exception e) {
            Log.e("map", "Map marker " + e.getMessage());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void getnearbyFacebook(String q){
        String url = "https://graph.facebook.com/search?" +
                "type=place&center="+latitude+","+longitude+"&distance=1000&" +
                "access_token=EAAX0NmD7gWABAFFx51sZCReS3iOvFtZA9xFHyZBSXZCI2mHYRJrjFofwOAeOE7Y61uxi" +
                "uXnnkZAdVS9PPjsikZCusaFYUsnQclTIY6zgzXFIhRdtgfNgDZBxOZCVTauUDKmMNT9tQIu2kzUFG5vy" +
                "PC7AKiD8CIlbd0QZD&q=" + q +
                "&fields=name,fan_count,talking_about_count,location" +
                ",checkins,category,category_list,picture.height(500)";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                arrPois.clear();
                facebookplaces.clear();
                Log.d(NearByMapFragment.class.getSimpleName(), "onResponse: "+jsonObject.toString());
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    if(jsonArray != null){
                        for(int i=0;jsonArray.length()> i;i++){
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            Hospitality hospitality = new Hospitality();
                            Facebookplace facebookplace = new Facebookplace(jsonObject1,latitude,longitude);
                            hospitality.setLogo(facebookplace.getImage().getUri());
                            hospitality.setName(facebookplace.getName());
                            hospitality.setLatitude(facebookplace.getLat());
                            hospitality.setLongitude(facebookplace.getLon());
                            hospitality.setProviderTypeKeyname("hotel");
                            hospitality.setDistance(facebookplace.getDistance());
                            arrPois.add(hospitality);
                            facebookplaces.add(facebookplace);
                            hospitalityHashMap.put(hospitality,facebookplace);

                        }
                        onUpdatemap(facebookplaces,null);
                        dataAdapter.setData(arrPois);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        MainApplication.getInstance().addToRequestQueue(jsonObjectRequest);



    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewAll:
                imageViewres.setImageResource(R.drawable.f_restaurant);
                imageViewAll.setImageResource(R.drawable.all_hover);
                imageViewshop.setImageResource(R.drawable.shoppingmall);
                imageViewPolice.setImageResource(R.drawable.police_station);
                imageViewParking.setImageResource(R.drawable.parking);
                imageViewBuffet.setImageResource(R.drawable.buffet);
                imageViewCoffe.setImageResource(R.drawable.coffee);
                imageViewGas.setImageResource(R.drawable.gas_station);
                imageViewhotel.setImageResource(R.drawable.f_hotel);
                getnearbyFacebook("");
                break;
            case R.id.viewHotel:
                imageViewres.setImageResource(R.drawable.f_restaurant);
                imageViewAll.setImageResource(R.drawable.all);
                imageViewshop.setImageResource(R.drawable.shoppingmall);
                imageViewPolice.setImageResource(R.drawable.police_station);
                imageViewParking.setImageResource(R.drawable.parking);
                imageViewBuffet.setImageResource(R.drawable.buffet);
                imageViewCoffe.setImageResource(R.drawable.coffee);
                imageViewGas.setImageResource(R.drawable.gas_station);
                imageViewhotel.setImageResource(R.drawable.f_hotel_hover);
                getnearbyFacebook("Hotel");
                break;
            case R.id.viewRestaurent:
                imageViewres.setImageResource(R.drawable.f_restaurant_hover);
                imageViewAll.setImageResource(R.drawable.all);
                imageViewshop.setImageResource(R.drawable.shoppingmall);
                imageViewPolice.setImageResource(R.drawable.police_station);
                imageViewParking.setImageResource(R.drawable.parking);
                imageViewBuffet.setImageResource(R.drawable.buffet);
                imageViewCoffe.setImageResource(R.drawable.coffee);
                imageViewGas.setImageResource(R.drawable.gas_station);
                imageViewhotel.setImageResource(R.drawable.f_hotel);
                getnearbyFacebook("Restaurent");
                break;
            case R.id.viewCoffe:
                imageViewres.setImageResource(R.drawable.f_restaurant);
                imageViewAll.setImageResource(R.drawable.all);
                imageViewshop.setImageResource(R.drawable.shoppingmall);
                imageViewPolice.setImageResource(R.drawable.police_station);
                imageViewParking.setImageResource(R.drawable.parking);
                imageViewBuffet.setImageResource(R.drawable.buffet);
                imageViewCoffe.setImageResource(R.drawable.coffee_hover);
                imageViewGas.setImageResource(R.drawable.gas_station);
                imageViewhotel.setImageResource(R.drawable.f_hotel);
                getnearbyFacebook("Coffee");
                break;
            case R.id.viewShopingmall:
                imageViewres.setImageResource(R.drawable.f_restaurant);
                imageViewAll.setImageResource(R.drawable.all);
                imageViewshop.setImageResource(R.drawable.shoppingmall_hover);
                imageViewPolice.setImageResource(R.drawable.police_station);
                imageViewParking.setImageResource(R.drawable.parking);
                imageViewBuffet.setImageResource(R.drawable.buffet);
                imageViewCoffe.setImageResource(R.drawable.coffee);
                imageViewGas.setImageResource(R.drawable.gas_station);
                imageViewhotel.setImageResource(R.drawable.f_hotel);
                getnearbyFacebook("ShoppingMall");
                break;
            case R.id.viewParking:
                imageViewres.setImageResource(R.drawable.f_restaurant);
                imageViewAll.setImageResource(R.drawable.all);
                imageViewshop.setImageResource(R.drawable.shoppingmall);
                imageViewPolice.setImageResource(R.drawable.police_station);
                imageViewParking.setImageResource(R.drawable.parking_hover);
                imageViewBuffet.setImageResource(R.drawable.buffet);
                imageViewCoffe.setImageResource(R.drawable.coffee);
                imageViewGas.setImageResource(R.drawable.gas_station);
                imageViewhotel.setImageResource(R.drawable.f_hotel);
                getnearbyFacebook("Parking");
                break;
            case R.id.viewPolicStation:
                imageViewres.setImageResource(R.drawable.f_restaurant);
                imageViewAll.setImageResource(R.drawable.all);
                imageViewshop.setImageResource(R.drawable.shoppingmall);
                imageViewPolice.setImageResource(R.drawable.police_station_hover);
                imageViewParking.setImageResource(R.drawable.parking);
                imageViewBuffet.setImageResource(R.drawable.buffet);
                imageViewCoffe.setImageResource(R.drawable.coffee);
                imageViewGas.setImageResource(R.drawable.gas_station);
                imageViewhotel.setImageResource(R.drawable.f_hotel);
                getnearbyFacebook("PoliceStation");
                break;
            case R.id.viewBuffet:
                imageViewres.setImageResource(R.drawable.f_restaurant);
                imageViewAll.setImageResource(R.drawable.all);
                imageViewshop.setImageResource(R.drawable.shoppingmall);
                imageViewPolice.setImageResource(R.drawable.police_station);
                imageViewParking.setImageResource(R.drawable.parking);
                imageViewBuffet.setImageResource(R.drawable.buffet_hover);
                imageViewCoffe.setImageResource(R.drawable.coffee);
                imageViewGas.setImageResource(R.drawable.gas_station);
                imageViewhotel.setImageResource(R.drawable.f_hotel);
                getnearbyFacebook("Buffet");
                break;
            case R.id.viewGasStation:
                imageViewres.setImageResource(R.drawable.f_restaurant);
                imageViewAll.setImageResource(R.drawable.all);
                imageViewshop.setImageResource(R.drawable.shoppingmall);
                imageViewPolice.setImageResource(R.drawable.police_station);
                imageViewParking.setImageResource(R.drawable.parking);
                imageViewBuffet.setImageResource(R.drawable.buffet);
                imageViewCoffe.setImageResource(R.drawable.coffee);
                imageViewGas.setImageResource(R.drawable.gas_station_hover);
                imageViewhotel.setImageResource(R.drawable.f_hotel);
                getnearbyFacebook("GasStation");
                break;
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Facebookplace selection = hospitalityHashMap.get(dataAdapter.getItem(position));
                Intent intent  = new Intent(getActivity(),FacebookDetail.class);
                intent.putExtra(Facebookplace.class.getName(), selection);
                startActivity(intent);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        //// TODO: 6/10/2559  goto detail
        Facebookplace selection = hMarkers.get(marker);
        Intent intent  = new Intent(getActivity(),FacebookDetail.class);
        intent.putExtra(Facebookplace.class.getName(), selection);
        startActivity(intent);
    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
         his = hMarkers.get(marker);
//        this.marker =marker;

        onUpdatemap(facebookplaces,his);
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMapv) {
        googleMap = googleMapv;
    }

    private  class FacebooknearInfomapadapter implements  GoogleMap.InfoWindowAdapter{

        Facebookplace facebookplace;
        public FacebooknearInfomapadapter() {
            hViews = new HashMap<Marker, View>();
            imageLoader = ImageLoader.getInstance();
            //hViews = new HashMap<Marker, View>();

            options = new DisplayImageOptions.Builder()
                    .cacheInMemory(true).cacheOnDisc(true)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .imageScaleType(ImageScaleType.NONE)
                    .showImageForEmptyUri(R.drawable.empty_cover)
                    .showImageOnLoading(R.drawable.bg_cctvdefault)
                    .build();
        }

        @Override
        public View getInfoWindow(Marker marker) {

            return  null;
        }

        @Override
        public View getInfoContents(final Marker marker) {
            View popup = hViews.get(marker);
            facebookplace = hMarkers.get(marker);
            ItemFacenearMapInfoBinding binding;
            if(popup == null){

                binding = DataBindingUtil.inflate(inflater,R.layout.item_facenear_map_info,null,false);
                popup = binding.getRoot();
                popup.setTag(binding);
                hViews.put(marker,popup);
                String url = facebookplace.getImage().getUri();
                imageLoader.displayImage(url, binding.infoAvatar, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String s, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String s, View view, FailReason failReason) {

                    }

                    @Override
                    public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                        marker.showInfoWindow();
                    }

                    @Override
                    public void onLoadingCancelled(String s, View view) {

                    }
                });
                binding.title.setText(facebookplace.getName());
                binding.address.setText(facebookplace.getCity());
                binding.txtDistance.setText(facebookplace.getDistance());
                if(facebookplace.getLike() > 1000 && facebookplace.getLike() < 1000000){
                    binding.txtLike.setText((facebookplace.getLike()/1000)+"k");
                }else if(facebookplace.getLike() > 1000000){
                    binding.txtLike.setText((facebookplace.getLike()/1000000)+"m");
                }else {
                    binding.txtLike.setText(facebookplace.getLike()+"");
                }
                if(facebookplace.getCheckin() > 1000 && facebookplace.getCheckin() < 1000000){
                    binding.txtCheckin.setText((facebookplace.getCheckin()/1000)+"k");
                }else if(facebookplace.getCheckin() > 1000000){
                    binding.txtCheckin.setText((facebookplace.getCheckin()/1000000)+"m");
                }else {
                    binding.txtCheckin.setText(facebookplace.getCheckin()+"");
                }
            }


            return (popup);


//            return null;
        }
    }



}
