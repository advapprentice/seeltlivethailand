package com.touchtechnologies.tit.indoor;

import java.util.List;
import java.util.Timer;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.dataobject.Tenant;

public class ShopByCategoryAdapter extends BaseAdapter {
	private Context context;
	private Runnable runnable;
	protected ImageLoader imageLoader;
	protected DisplayImageOptions options;
	Timer t;
	private List<Tenant> arritem;
	String url;
	public ShopByCategoryAdapter(Context context) {
		this.context = context;
		imageLoader = ImageLoader.getInstance();
			
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.NONE)
		.showImageForEmptyUri(R.drawable.bg_noimage)
		.showImageOnFail(R.drawable.bg_noimage)
	//	.showImageOnLoading(R.drawable.bg_loading)
		.cacheInMemory(true)
		.displayer(new FadeInBitmapDisplayer(300)) //fade in images
	  	.resetViewBeforeLoading(true)
	  	.build();

 	     	  
 	    imageLoader = ImageLoader.getInstance();
	
		
	}
	public void setData(List<Tenant> arritem){
		this.arritem =  arritem;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return arritem == null?0:arritem.size();
	}
	
	@Override
	public Tenant getItem(int position) {
		return arritem.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}

	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		
		if(convertView == null){
			convertView = LayoutInflater.from(context).inflate(R.layout.item_indoor_category, parent, false);
			
			viewHolder = new ViewHolder();
			viewHolder.tvname =(TextView)convertView.findViewById(R.id.textViewname);
			viewHolder. imglive =(ImageView)convertView.findViewById(R.id.imgtypecategory);
			viewHolder.tvtype =(TextView)convertView.findViewById(R.id.textViewtype);
			viewHolder.tvfloor =(TextView)convertView.findViewById(R.id.textViewfloor);
			viewHolder.tvunit =(TextView)convertView.findViewById(R.id.textViewunit);
			convertView.setTag(viewHolder);
		}else{
			viewHolder = (ViewHolder)convertView.getTag();
		}
      		viewHolder.tvname.setText(getItem(position).getName());
      		viewHolder.tvtype.setText(getItem(position).getType());
      		viewHolder.tvfloor.setText(getItem(position).getFloor());
      		viewHolder.tvunit.setText("Unit "+getItem(position).getUnit());
      		imageLoader.displayImage(getItem(position).getLogo(),viewHolder.imglive, options);	
	
		return convertView;
	}



	class ViewHolder{
		ImageView imglive;
		TextView tvtype;
		TextView tvname;
		TextView tvfloor;
		TextView tvunit;
		View Viewvideoschedule;
		View topic;
		
	}
	
	
}