package com.touchtechnologies.tit.indoor;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.touchtechnologies.tit.MainActivity;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.dataobject.Tenant;

public class ShopCategoryFragment extends ListFragment {
	private ListView category;
	private ShopCategoryAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View listViewContainer = inflater.inflate(
				R.layout.fragment_list_shop_indoor, container, false);

		List<Tenant> image_details = GetResults();
		category = (ListView) listViewContainer.findViewById(R.id.list);

		adapter = new ShopCategoryAdapter(getActivity(), image_details);
		setListAdapter(adapter);

		return listViewContainer;
	}

	private List<Tenant> GetResults() {
		List<Tenant> results = new ArrayList<Tenant>();

		Tenant item = new Tenant();
		item.setType("Food");
		item.setId(1);
		results.add(item);

		item = new Tenant();
		item.setType("Mobile phone & ITServices");
		item.setId(2);
		results.add(item);

		item = new Tenant();
		item.setType("Banking");
		item.setId(3);
		results.add(item);

		item = new Tenant();

		item.setType("Fashion");
		item.setId(4);
		results.add(item);

		item = new Tenant();
		item.setType("Health & Beauty");
		item.setId(5);
		results.add(item);

		item = new Tenant();

		item.setType("Services");
		item.setId(6);
		results.add(item);

		return results;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		
			Intent intent = new Intent(getActivity(), ShopByCategoryActivity.class);
			
			intent.putExtra(Tenant.class.getName(),adapter.getItem(position));
			intent.putExtra("index", position);
			
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivityForResult(intent, MainActivity.REQUEST_MAP);
	
	}
}
