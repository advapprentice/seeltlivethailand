package com.touchtechnologies.tit.indoor;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.hardware.SensorEvent;
import android.location.Location;
import android.net.wifi.ScanResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.touchtechnologies.location.FingerprintSensorListener;
import com.touchtechnologies.location.MicroLocationManager;
import com.touchtechnologies.location.dataobject.FingerPrint;
import com.touchtechnologies.location.dataobject.Position;
import com.touchtechnologies.tit.MainActivity;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.dataobject.Tenant;
@SuppressLint("ValidFragment")
public class IndoorMapFragment extends Fragment implements OnClickListener,
		FingerprintSensorListener, OnMapReadyCallback {

	private int currentFloor = 0;

	private MicroLocationManager mclManager;
	Vector<Position> positionsTest;
	GroundOverlayOptions newarkMap;
	private static Handler msgHandler;
	int currentIndex = 0;
	private GoogleMap googleMap;
	
	private List<Tenant> lTenants;
	private HashMap<LatLng, Marker> hMapMarkers;
	
	public IndoorMapFragment(List<Tenant> lTenants){
		this.lTenants = lTenants;
	}
	
	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);

		getActivity().getWindow().addFlags(
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		hMapMarkers = new HashMap<>();
		
		//get micro location access
		mclManager = MicroLocationManager.getInstance(getActivity());
		
		positionsTest = new Vector<>();
		Vector<FingerPrint> vFps = mclManager.getFingerPrints();
		for(FingerPrint fp: vFps){
			positionsTest.add(fp.getPosition());
		}
		
//		new Timer().schedule(new TimerTask() {
//			
//			@Override
//			public void run() {
//				new AsyncTask<Void, Void, Void>() {
//					public Void doInBackground(Void... params) {
//						return null;
//					};
//					
//					protected void onPostExecute(Void result) {
//						if(currentIndex >= lTenants.size() - 1){
//							currentIndex = 0;
//						}
//						updateMap(lTenants.get(currentIndex++).getLatLng());
//					};
//				}.execute();
//			}
//		}, 0, 1500);
	}
	
	public void setData(List<Tenant> lTenants){
		this.lTenants = lTenants;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_indoor_map, container,false);
		view.findViewById(R.id.imgevent).setOnClickListener(this);

	//	mclManager = MicroLocationManager.getInstance(getActivity());

		msgHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == MSG_UPDATE_POSITION) {
					// TODO
				}
			}
		};

		loadMap(1);

		return view;

	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mclManager.requestLocationUpdates(this);
		} catch (Exception e) {
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		try {
			mclManager.unregisterReceiver();
		} catch (Exception e) {
		}
	}

	@Override
	public void onClick(View v) {
//		if(v.getId() == R.id.imgevent){
//			((MainActivity)getActivity()).onIndoorToggleMapSelected(false);
//		}
	}

	private void loadMap(int floor) {
		if (currentFloor != floor) {
			currentFloor = floor;

//			13.7331356,100.5301254
			LatLng zooLatLng = new LatLng(13.7331046,100.5295339);

			SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
					.findFragmentById(R.id.map);

			mapFragment.getMapAsync(this);
			googleMap.getUiSettings().setZoomControlsEnabled(false);
			googleMap.animateCamera(
					CameraUpdateFactory.newLatLngZoom(zooLatLng, 18), 5, null);

			int resource = 0;
			switch(floor){
			case 1:
				//resource = R.drawable.floor1;
				break;
			case 2:
				//resource = R.drawable.floor2;
				break;
			}
			 newarkMap = new GroundOverlayOptions()
			 .anchor(0.2f, 0.3f)
			 .image(BitmapDescriptorFactory.fromResource(resource))
			 .position(zooLatLng, 280f, 90f)
			 .bearing(6);
			 googleMap.addGroundOverlay(newarkMap);
			 
			 updateMarker(lTenants);
			 
			 System.gc();
		}
	}
	
	public void updateMarker(List<Tenant> lTenant){
		if(lTenant == null || lTenant.isEmpty()) return;
		
		SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
				.findFragmentById(R.id.map);

		mapFragment.getMapAsync(this);

		hMapMarkers.clear();
		for(Tenant tenant: lTenant){
			LatLng ll = tenant.getLatLng();
//			Log.d(IndoorMapFragment.class.getSimpleName(), "tenant location " + ll.latitude + "," + ll.longitude);
			
			if(ll.latitude > 0 && ll.longitude > 0){
				Marker mk = googleMap.addMarker(new MarkerOptions()
		        .position(ll)
		        .title(tenant.getName()));
				
				hMapMarkers.put(ll, mk);
			}
		}
		
//		for(int i=0; i<lTenants.size() - 1; i++){
//			Polyline line = googleMap.addPolyline(new PolylineOptions()
//		     .add(lTenants.get(i).getLatLng(), lTenants.get(i + 1).getLatLng())
//		     .width(5)
//		     .color(Color.RED));
//			googleMap.addPolyline(line);
//		}
	}
	

	@Override
	public void onLocationChanged(Location location) {
		if (location instanceof Position) {
			
			Message msg = new Message();
			msg.what = MSG_UPDATE_POSITION;
			msg.obj = location;

			msgHandler.sendMessage(msg);

			updateMap((Position) location);

			// Toast.makeText(getApplicationContext(), "location update " +
			// location., Toast.LENGTH_SHORT);
		}
	}
	
	void updateMap(Position position){
		if(lTenants == null) return;
		
		for(Tenant tenant: lTenants){
			if(tenant.getUnit().startsWith(position.getName())){
				updateMap(tenant.getLatLng());
				
				Toast.makeText(getActivity(),
						 "Position changed " + tenant.getName(),
						 Toast.LENGTH_LONG).show();
				break;
			}
		}
	}

	void updateMap(LatLng ll) {
		try{
			if(ll.latitude == 0 || ll.longitude == 0) return;
			
			SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
					.findFragmentById(R.id.map);
			
			mapFragment.getMapAsync(this);
			googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ll, 21), 1000, null);
			
			//pop infowindows
			Iterator<LatLng> lls = hMapMarkers.keySet().iterator();
			while(lls.hasNext()){
				LatLng llTmp = lls.next();
				if(llTmp.latitude != ll.latitude && 
						llTmp.longitude != llTmp.longitude){
					hMapMarkers.get(llTmp).hideInfoWindow();
					
				}else{
					hMapMarkers.get(llTmp).showInfoWindow();
				}
			}
			
			
		}catch(Exception e){
			
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO

	}

	@Override
	public void onScanFingerprintCompleted(List<FingerPrint> lFingerprint) {
	}

	@Override
	public void onScanCompleted(CellLocation cellLocation,
			List<NeighboringCellInfo> lNeighborCellInfos) {
		// TODO

	}

	@Override
	public void onScanCompleted(List<ScanResult> lScanResults) {
		// TODO
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO

	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// super.onCreateOptionsMenu(menu);
	//
	// menu.add(1, 1, 1, "Locate me!");
	//
	// return true;
	// }

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void onDestroy() {
		newarkMap.image(null);
		newarkMap = null;
		System.gc();
		
		super.onDestroy();
	}

	@Override
	public void onMapReady(GoogleMap googleMapv) {
		googleMap = googleMapv;
	}
}