package com.touchtechnologies.tit.indoor;

import java.util.List;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.touchtechnologies.tit.MainActivity;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.dataobject.Tenant;

public class IndoorListFragment extends Fragment implements OnClickListener{
	SimpleFragmentStatePagerAdapter adapter;
	ViewPager mPager;
	
	private List<Tenant> lTenants;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_indoor_list, container, false);
		view.findViewById(R.id.img_map).setOnClickListener(this);
		
		adapter = new SimpleFragmentStatePagerAdapter(getChildFragmentManager());
		mPager = (ViewPager)view.findViewById(R.id.pager);
		mPager.setAdapter(adapter);
		
		return view;
	}
	
	public void setData(List<Tenant> lTenants){
		this.lTenants = lTenants;
	}
	
	@Override
	public void onClick(View v) {
//		if(v.getId() == R.id.img_map){
//			((MainActivity)getActivity()).onIndoorToggleMapSelected(true);
//		}
	}
	
	static class SimpleFragmentStatePagerAdapter extends FragmentStatePagerAdapter{
		
		public SimpleFragmentStatePagerAdapter(FragmentManager fm) {
            super(fm);
        }
		
		@Override
		public int getCount() {
			return 1;
		}
		
		@Override
		public Fragment getItem(int position) {
			switch(position){
		//	case 0: return new PromotionFragment();
			case 0: return new ShopCategoryFragment();
			}
			return null;
		}
		
		@Override
		public CharSequence getPageTitle(int position) {
		//	return position == 0?"PROMOTION":"SHOP";
			return "SHOP";
		}
	}
}
