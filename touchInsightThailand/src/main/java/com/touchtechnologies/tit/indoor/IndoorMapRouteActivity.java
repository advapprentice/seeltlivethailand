package com.touchtechnologies.tit.indoor;

import java.util.ArrayList;
import java.util.List;

import android.content.res.AssetManager;
import android.graphics.Color;
import android.hardware.SensorEvent;
import android.location.Location;
import android.net.wifi.ScanResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.touchtechnologies.location.FingerprintSensorListener;
import com.touchtechnologies.location.MicroLocationManager;
import com.touchtechnologies.location.dataobject.FingerPrint;
import com.touchtechnologies.location.dataobject.Position;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.dataobject.Tenant;
import com.touchtechnologies.tit.util.TenantUtil;

public class IndoorMapRouteActivity extends FragmentActivity implements
		FingerprintSensorListener, OnMapReadyCallback {

	private MicroLocationManager mclManager;
	GroundOverlayOptions newarkMap;
	private static Handler msgHandler;
	int currentIndex = 0;
	GoogleMap googleMap;

	private List<Tenant> lTenants;
	private Tenant tenant;
	private boolean alreadyMatched = false;

	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setContentView(R.layout.fragment_indoor_map_route);

		tenant = (Tenant) getIntent().getSerializableExtra(
				Tenant.class.getName());

		// get micro location access
		mclManager = MicroLocationManager.getInstance(this);

		// mclManager = MicroLocationManager.getInstance(getActivity());

		msgHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == MSG_UPDATE_POSITION) {
					// TODO
				}
			}
		};

		AssetManager am = getAssets();
		try {
			lTenants = TenantUtil.getTenantFromAsset(am);
		} catch (Exception e) {
			Log.e(IndoorMapFragment.class.getSimpleName(),
					e.getMessage());
		}
		
		loadMap();
		updateMarker();
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mclManager.requestLocationUpdates(this);
		} catch (Exception e) {
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		try {
			mclManager.unregisterReceiver();
		} catch (Exception e) {
		}
	}

	private void loadMap() {

		// 13.7331356,100.5301254
		LatLng zooLatLng = new LatLng(13.7331046, 100.5295339);

		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);

		mapFragment.getMapAsync(this);
		googleMap.getUiSettings().setZoomControlsEnabled(false);
		googleMap.animateCamera(
				CameraUpdateFactory.newLatLngZoom(zooLatLng, 18), 5, null);

		int resource = 0;//R.drawable.floor1;

		newarkMap = new GroundOverlayOptions().anchor(0.2f, 0.3f)
				.image(BitmapDescriptorFactory.fromResource(resource))
				.position(zooLatLng, 280f, 90f).bearing(6);
		googleMap.addGroundOverlay(newarkMap);

		System.gc();
	}

	public void updateMarker() {

		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);

		LatLng ll = tenant.getLatLng();

		if (ll.latitude > 0 && ll.longitude > 0) {
			MarkerOptions mp = new MarkerOptions().position(ll).title(
					tenant.getName());
			mp.draggable(true);

			Marker mk = googleMap.addMarker(mp);
			mk.showInfoWindow();

			googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(tenant.getLatLng(), 19), 1000, null);
			
		} else {
			Toast.makeText(this, "No location for " + tenant.getName(),
					Toast.LENGTH_LONG).show();
		}

		googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

			@Override
			public void onMapClick(LatLng ll) {
				Log.d(IndoorMapRouteActivity.class.getSimpleName(), ll.latitude
						+ "," + ll.longitude);
			}
		});
	}

	@Override
	public void onLocationChanged(Location location) {
		if (location instanceof Position) {

			Message msg = new Message();
			msg.what = MSG_UPDATE_POSITION;
			msg.obj = location;

			msgHandler.sendMessage(msg);

			updateMap((Position) location);

			// Toast.makeText(getApplicationContext(), "location update " +
			// location., Toast.LENGTH_SHORT);
		}
	}

	/**
	 * Update my location
	 * 
	 * @param position
	 */
	void updateMap(Position position) {
		if(lTenants == null || alreadyMatched) return;
		
		Tenant tenantMatch = null;
		for(Tenant t: lTenants){
			if(t.getUnit().startsWith(position.getName())){
				tenantMatch = t;
				break;
			}
		}
		if(tenantMatch == null) return;
		
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);

		mapFragment.getMapAsync(this);
		
		// paint poly from current to tenent
		// draw path
		//List<Node> lNodes = NodeUtil.getNodes();
		// List<LatLng> lLatLong =
		// NodeUtil.getNodesBetween(lNodes.get(0).getLatLng(),
		// lNodes.get(1).getLatLng());

		List<LatLng> lLatLong = new ArrayList<>();
		lLatLong.add(tenant.getLatLng());
		lLatLong.add(tenantMatch.getLatLng());
		
		googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(tenantMatch.getLatLng(), 20), 1000, null);
		
		for (int i = 0; i < lLatLong.size() - 1; i++) {
			Polyline line = googleMap.addPolyline(new PolylineOptions()
					.add(lLatLong.get(i), lLatLong.get(i + 1)).width(5)
					.color(Color.RED));
		}
		
		alreadyMatched = true;
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO

	}

	@Override
	public void onScanFingerprintCompleted(List<FingerPrint> lFingerprint) {
	}

	@Override
	public void onScanCompleted(CellLocation cellLocation,
			List<NeighboringCellInfo> lNeighborCellInfos) {
		// TODO

	}

	@Override
	public void onScanCompleted(List<ScanResult> lScanResults) {
		// TODO
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO

	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// super.onCreateOptionsMenu(menu);
	//
	// menu.add(1, 1, 1, "Locate me!");
	//
	// return true;
	// }

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDestroy() {
		newarkMap.image(null);
		newarkMap = null;
		System.gc();

		super.onDestroy();
	}

	@Override
	public void onMapReady(GoogleMap googleMapv) {
		googleMap = googleMapv;
	}
}