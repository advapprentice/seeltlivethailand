package com.touchtechnologies.tit.indoor;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.touchtechnologies.tit.MainActivity;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.dataobject.Tenant;
import com.touchtechnologies.tit.search.SearchActionbarActivity;
import com.touchtechnologies.tit.util.TenantUtil;

public class ShopByCategoryActivity extends SearchActionbarActivity
	implements OnItemClickListener{
	private ImageView imgcategory ;
	private TextView txtcategoryname,txtcategoryname2;
	private ListView listbycategory;
	private ShopByCategoryAdapter adapter;
	private Integer[] imgid = {
//			R.drawable.indoor_food,
////			R.drawable.indoor_mobile,
//			R.drawable.indoor_banking,
//			R.drawable.indoor_fashion,
//			R.drawable.indoor_healthy,
//			R.drawable.indoor_service
			};

	String categoryName;
	List<Tenant> lShop;
	private int categoryIndex;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_shop_by_category);
		
		try{
			lShop = TenantUtil.getTenantFromAsset(getAssets());
		}catch(Exception e){
		}
		
		Tenant shop = (Tenant)getIntent().getSerializableExtra(Tenant.class.getName());
		categoryIndex = getIntent().getIntExtra("index", 0);
		categoryName = shop.getType();
		
		String catTmp;
		for(int i=lShop.size()-1; i>=0; i--){
			catTmp = lShop.get(i).getType();
			if(!catTmp.equals(categoryName)){
				lShop.remove(i);
			}
		}
		
		imgcategory = (ImageView)findViewById(R.id.imageViewCategory);
		txtcategoryname =(TextView)findViewById(R.id.textViewCategoryName);
		txtcategoryname2 =(TextView)findViewById(R.id.textViewCategoryName2);
		
		listbycategory = (ListView)findViewById(R.id.listViewShopByCagetory);
		listbycategory.setOnItemClickListener(this);
		
		adapter = new ShopByCategoryAdapter(this);
		adapter.setData(lShop);
		
		listbycategory.setAdapter(adapter);
		
		imgcategory.setImageResource(imgid[categoryIndex]);
		txtcategoryname.setText(categoryName);
		txtcategoryname2.setText(categoryName);
	};
		
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Intent intent = new Intent();
		intent.putExtra(Tenant.class.getName(), lShop.get(position));
		setResult(MainActivity.RESULT_REQUEST_MAP, intent);
		
		finish();
	}
}
