package com.touchtechnologies.tit.indoor;


import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.dataobject.Tenant;


public class ShopCategoryAdapter extends BaseAdapter {
	private static List<Tenant> itemDetailsrrayList;
	private Integer[] imgid = {
//			R.drawable.indoor_food,
//			R.drawable.indoor_mobile,
//			R.drawable.indoor_banking,
//			R.drawable.indoor_fashion,
//			R.drawable.indoor_healthy,
//			R.drawable.indoor_service
			};
	private LayoutInflater l_Inflater;
	
	public ShopCategoryAdapter(Context context, List<Tenant> results) {
		itemDetailsrrayList = results;
		l_Inflater = LayoutInflater.from(context);
	}
	
	public int getCount() {
		return itemDetailsrrayList.size();
	}

	public Tenant getItem(int position) {
		return itemDetailsrrayList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}
	
	

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = l_Inflater.inflate(R.layout.item_indoor_shop, null);
			holder = new ViewHolder();
			holder.txt_itemName = (TextView) convertView.findViewById(R.id.texttype);
			holder.itemImage = (ImageView) convertView.findViewById(R.id.imgtype);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.txt_itemName.setText(itemDetailsrrayList.get(position).getType());
	
		holder.itemImage.setImageResource(imgid[itemDetailsrrayList.get(position).getId()-1]);

		return convertView;
	}

	static class ViewHolder {
		TextView txt_itemName;
		ImageView itemImage;
	}
}	