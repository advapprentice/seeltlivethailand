package com.touchtechnologies.tit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.FacebookSdk;
import com.touchtechnologies.tit.cctvitlive.CCTVMenuMainActivity;

/**
 * A class use to launch a sign up activity or a main menu which depends on
 * user login status.
 * 1. Launch Login activity if user is not logged in.
 * 2. Launch Main activity if user is logged in. 
 * @author Touch
 *
 */
public class LauncherActivity  extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FacebookSdk.sdkInitialize(getApplicationContext());
			startActivity(new Intent(this, CCTVMenuMainActivity.class));
			finish();
	}

}
