package com.touchtechnologies.tit;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.touchtechnologies.dataobject.Image;
import com.touchtechnologies.dataobject.ImageGallery;

public class GalleryAdapter extends PagerAdapter {
	private ImageGallery gallery;
	private Context context;
	private int resLayout;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;

	public GalleryAdapter(Context context, int resLayout) {
		this.context = context;
		this.resLayout = resLayout;

		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
				.showImageForEmptyUri(R.drawable.empty_cover)
				.showImageOnFail(R.drawable.empty_cover).build();
	}

	public void setData(ImageGallery gallery) {
		this.gallery = gallery;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return gallery == null ? 0 : gallery.size();
	}

//	@Override
//	public float getPageWidth(int position) {
//		return 0.93f;
//	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return (view == object);
	}

	public Image getItem(int position) {
		return gallery.getImageAt(position);
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public Object instantiateItem(ViewGroup collection, int position) {
		View convertView = LayoutInflater.from(context).inflate(resLayout,
				collection, false);
		convertView.setTag(gallery.getImageAt(position));

		ImageView imageView = (ImageView) convertView.findViewById(R.id.imageViewGalleryPreview);

		imageLoader.displayImage(getItem(position).getThumbnail(), imageView,options);
		collection.addView(convertView, 0);
		return convertView;
	}

	@Override
	public void destroyItem(View collection, int position, Object view) {
		((ViewPager) collection).removeView((View) view);

	}
}