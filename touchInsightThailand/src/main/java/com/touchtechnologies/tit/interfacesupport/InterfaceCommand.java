package com.touchtechnologies.tit.interfacesupport;

/**
 * Created by TouchICS on 5/31/2016.
 */
public interface InterfaceCommand {
    void getCommand();

}