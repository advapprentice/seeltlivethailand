package com.touchtechnologies.tit;


import android.annotation.SuppressLint;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

@SuppressLint("ValidFragment")
public class GalleryFragment extends Fragment{
	private GalleryAdapter adapter;
	
	public GalleryFragment(GalleryAdapter adapter) {
		this.adapter = adapter;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_gallery, container, false);
		ViewPager viewPager = (ViewPager)view.findViewById(R.id.pager);
		viewPager.setAdapter(adapter);
		
		
		Point point = new Point();
	  	getActivity().getWindowManager().getDefaultDisplay().getSize(point);
	  	viewPager.getLayoutParams().height = (int)((point.x * 2.5)/4);

		
		return view;
	}
}
