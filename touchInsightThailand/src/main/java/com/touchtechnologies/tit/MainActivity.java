package com.touchtechnologies.tit;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.touchtechnologies.app.TransparentDialog;
import com.touchtechnologies.command.wifi.WiFiCommand;
import com.touchtechnologies.dataobject.WiFiAccount;
import com.touchtechnologies.dataobject.WiFiClient;
import com.touchtechnologies.net.ServiceResponse;
import com.touchtechnologies.tit.dataobject.Tenant;
import com.touchtechnologies.tit.indoor.IndoorListFragment;
import com.touchtechnologies.tit.indoor.IndoorMapFragment;
import com.touchtechnologies.tit.indoor.IndoorMapRouteActivity;
import com.touchtechnologies.tit.util.TenantUtil;
import com.touchtechnologies.tit.util.UserUtil;
import com.touchtechnologies.wifi.WIFIMod;
import com.touchtechnologies.wifi.WiFiServiceAsyncTask;

public class MainActivity extends FragmentActivity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks {
	static int REQUEST_WIFI_SETING = 76;
	public static int RESULT_REQUEST_MAP = 37;
	public static int REQUEST_MAP = 7;
	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	protected CharSequence mTitle;

	//private BroadcastReceiver broadReceiver;
	//private WifiManager wManager;
	//private IntentFilter intentFilter;

	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;

//	private List<Tenant> lTenants;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));

		// / Login Success
		// User user = UserUtil.getUser(this);
		// Toast.makeText(this, "test User"+user.getToken(),Toast.LENGTH_SHORT
		// ).show();

		// read tenant from asset to array list
//		if (lTenants == null) {
//			new AsyncTask<Void, Void, List<Tenant>>() {
//				public List<Tenant> doInBackground(Void... params) {
//					List<Tenant> lT = new ArrayList<>();
//
//					AssetManager am = getAssets();
//					try {
//						lT = TenantUtil.getTenantFromAsset(am);
//					} catch (Exception e) {
//						Log.e(IndoorMapFragment.class.getSimpleName(),
//								e.getMessage());
//					}
//
//					return lT;
//				};
//
//				@Override
//				protected void onPostExecute(List<Tenant> result) {
//					lTenants = result;
//				}
//			}.execute();
//		}
//		
//		String model = Build.MODEL;
	

//		scanForICSWiFi();
	}

//	public void scanForICSWiFi() {
//		wManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
//		broadReceiver = new BroadcastReceiver() {
//			@Override
//			public void onReceive(Context context, Intent intent) {
//				List<ScanResult> lScanResults = wManager.getScanResults();
//				for (ScanResult sr : lScanResults) {
//					Log.d(MainActivity.class.getSimpleName(),
//							"scan wifi found " + sr.SSID);
//				}
//				
//				onScanCompleted(lScanResults);
//				
//				unregisterReceiver(broadReceiver);
//			}
//
//		};
//
//		intentFilter = new IntentFilter();
//		intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
//
//		// register receiver for WiFi
//		registerReceiver(broadReceiver, intentFilter);
//
//		wManager.startScan();
//
//	}

	void openWiFiSetting() {
		startActivityForResult(new Intent(
				android.provider.Settings.ACTION_WIFI_SETTINGS),
				REQUEST_WIFI_SETING);
	}

	
	@Override
	protected void onActivityResult(int req, int result, Intent data) {
		if (req == REQUEST_WIFI_SETING) {

			if (WIFIMod.isConnectedToSSIDs(this,
					getResources().getStringArray(R.array.wifi_ssid))) {
				checkOnlineStatusToLogin();

			} else {
//				showDialogAskToConnectWiFi();

			}
		}else if(result == RESULT_REQUEST_MAP){
			Intent intent = new Intent(this, IndoorMapRouteActivity.class);
			Tenant tenant = (Tenant)data.getSerializableExtra(Tenant.class.getName());
			intent.putExtra(Tenant.class.getName(), tenant);
			
			startActivity(intent);
		}
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {

		switch (position) {
		case 0:
//			getSupportFragmentManager().beginTransaction()
//					.replace(R.id.container, new CCTVMenuMainActivity())
//					.commit();
			break;
		case 1:
//			checkOnlineStatusToAlert();
			
			break;
		}
	}
	
	public void onIndoorToggleMapSelected(Tenant tenant){
		
	}

//	public void onIndoorToggleMapSelected(boolean map) {
//		if (map) {
//			indoorMapSelectedActive();
//
//		} else {
//			IndoorListFragment fragment = new IndoorListFragment();
//			getSupportFragmentManager().beginTransaction()
//					.replace(R.id.container, fragment).commit();
//
//			fragment.setData(lTenants);
//		}
//	}

//	void showDialogAskToConnectWiFi() {
//		final TransparentDialog td = new TransparentDialog(this,
//				R.layout.fragment_wifi_service_dialog);
//
//		// go to indoor map/ services
//		td.findViewById(R.id.textViewWiFiServiceSkip).setOnClickListener(
//				new View.OnClickListener() {
//
//					@Override
//					public void onClick(View v) {
//						indoorMapSelectedActive();
//						
//						td.dismiss();
//						System.gc();
//					}
//				});
//
//		// connect wifi service
//		td.findViewById(R.id.textViewWiFiServiceConnect).setOnClickListener(
//				new View.OnClickListener() {
//
//					@Override
//					public void onClick(View v) {
//						td.dismiss();
//
//						String[] ssids = getResources().getStringArray(
//								R.array.wifi_ssid);
//
//						if (WIFIMod
//								.isConnectedToSSIDs(MainActivity.this, ssids)) {
//
//							WiFiAccount wifiAccount = UserUtil
//									.getWiFiAccount(getApplicationContext());
//							if (wifiAccount == null) {
//								// add account
//								showDialogAddAccount();
//
//							} else {
//								// check online status, -> connect WiFi
//								checkOnlineStatusToLogin();
//							}
//
//						} else {
//							openWiFiSetting();
//						}
//
//					}
//				});
//
//		td.show();
//	}

	void showDialogAddAccount() {
		final TransparentDialog td = new TransparentDialog(this,
				R.layout.fragment_wifi_account_add);

		final EditText textUsername = (EditText) td
				.findViewById(R.id.editTextWiFiUsername);
		final EditText textPassword = (EditText) td
				.findViewById(R.id.editTextWiFiPassword);

		// cancel
		td.findViewById(R.id.textViewWiFiAccAdd).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						td.dismiss();
					}
				});

		// save account
		td.findViewById(R.id.textViewWiFiAccSave).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						td.dismiss();

						WiFiAccount wc = new WiFiAccount();
						wc.setUsername(textUsername.getText().toString().trim());
						wc.setPassword(textPassword.getText().toString().trim());

						UserUtil.setWiFiAccount(MainActivity.this, wc);

						checkOnlineStatusToLogin();
					}
				});

		td.show();
	}
	
//	void checkOnlineStatusToAlert() {
//		String[] ssids = getResources().getStringArray(
//				R.array.wifi_ssid);
//		indoorMapSelectedActive();
//		
//		if (!WIFIMod
//				.isConnectedToSSIDs(MainActivity.this, ssids)){
//			
//		}else{
//			String gatewayAddr = WIFIMod.getGWAddress(this);
//			String url = "http://" + gatewayAddr + "/api/interface/ics";
//	
//			WiFiCommand command = new WiFiCommand(this, "010100");
//	
//			WiFiClient wc = new WiFiClient();
//			wc.setIp(WIFIMod.getLocalAddress(this));
//			wc.setAccount(UserUtil.getWiFiAccount(this));
//	
//			command.setWiFiClient(wc);
//	
//			WiFiServiceAsyncTask wifiTask = new WiFiServiceAsyncTask(url) {
//				@Override
//				protected void onPostExecute(ServiceResponse result) {
//	
//					ServiceResponse serviceResponse = result;
//					if (serviceResponse == null)
//						return;
//	
//					try {
//						JSONObject json = new JSONObject(serviceResponse
//								.getContent().toString());
//						if (!json.isNull("status")) {
//							String onlineStatus = json.getString("status");
//							if ("offline".equals(onlineStatus)) {
////								showDialogAskToConnectWiFi();
//	
//							} else if ("online".equals(onlineStatus)) {
//								
//								Toast.makeText(MainActivity.this,
//										"Your are already logged in",
//										Toast.LENGTH_LONG).show();
//								
//								
//								
//							}
//						}
//					} catch (Exception e) {
//						Log.e(MainActivity.class.getSimpleName(),
//								"check in line status error", e);
//					}
//				}
//			};
//			wifiTask.execute(command);
//		}
//	}

	void checkOnlineStatusToLogin() {
		String gatewayAddr = WIFIMod.getGWAddress(this);
		String url = "http://" + gatewayAddr + "/api/interface/ics";

		WiFiCommand command = new WiFiCommand(this, "010100");

		WiFiClient wc = new WiFiClient();
		wc.setIp(WIFIMod.getLocalAddress(this));
		wc.setAccount(UserUtil.getWiFiAccount(this));

		command.setWiFiClient(wc);

		WiFiServiceAsyncTask wifiTask = new WiFiServiceAsyncTask(url) {
			@Override
			protected void onPostExecute(ServiceResponse result) {

				ServiceResponse serviceResponse = result;
				if (serviceResponse == null)
					return;

				try {
					JSONObject json = new JSONObject(serviceResponse
							.getContent().toString());
					if (!json.isNull("status")) {
						String onlineStatus = json.getString("status");
						if ("offline".equals(onlineStatus)) {
							wifiLoginImpl();

						} else if ("online".equals(onlineStatus)) {
							Toast.makeText(MainActivity.this,
									"Your are already logged in",
									Toast.LENGTH_LONG).show();
						}
					}
				} catch (Exception e) {
					Log.e(MainActivity.class.getSimpleName(),
							"check in line status error", e);
				}
			}
		};
		wifiTask.execute(command);
	}

	void wifiLoginImpl() {
		String gatewayAddr = WIFIMod.getGWAddress(this);
		
		String url = "http://10.30.0.1/api/interface/ics";

		WiFiCommand command = new WiFiCommand(this, "010200");

		WiFiClient wc = new WiFiClient();
		wc.setIp(WIFIMod.getLocalAddress(this));
		wc.setAccount(UserUtil.getWiFiAccount(this));

		command.setWiFiClient(wc);

		WiFiServiceAsyncTask wifiTask = new WiFiServiceAsyncTask(url) {
			@Override
			protected void onPostExecute(ServiceResponse result) {

				ServiceResponse serviceResponse = result;
				try {
					JSONObject json = new JSONObject(
							serviceResponse.getMessage());

					System.gc();
				} catch (Exception e) {
					Log.e(MainActivity.class.getSimpleName(),
							"WiFi login error", e);
				}
			}
		};
		wifiTask.execute(command);

//		indoorMapSelectedActive();
	}

	/**
	 * Update title callback
	 * 
	 * @param title
	 */
	public void onSectionAttached(String title) {
		mTitle = title;
	}
	
//	void indoorMapSelectedActive(){
//		IndoorMapFragment fragment = new IndoorMapFragment(lTenants);
//		// show indoor map
//		getSupportFragmentManager().beginTransaction()
//				.replace(R.id.container, fragment).commit();
//
//		fragment.setData(lTenants);
//	}

	public void restoreActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.search) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			getMenuInflater().inflate(R.menu.main, menu);
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	public void onScanCompleted(List<ScanResult> lScanResults) {

		boolean alertFreeWiFiFound = false;
		String wifiSSIDs[] = getResources().getStringArray(R.array.wifi_ssid);

		boolean connected = WIFIMod.isConnectedToSSIDs(MainActivity.this,
				wifiSSIDs);

		if (!connected) {
			for (ScanResult sr : lScanResults) {
				String ssidScan = sr.SSID.toLowerCase();
				for (String ssid : wifiSSIDs) {
					if (ssidScan.equals(ssid)) {
						alertFreeWiFiFound = true;
						break;
					}
				}
			}
		}

		if (alertFreeWiFiFound) {
			Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
			v.vibrate(1500);

//			showDialogAskToConnectWiFi();

		}
	}

}
