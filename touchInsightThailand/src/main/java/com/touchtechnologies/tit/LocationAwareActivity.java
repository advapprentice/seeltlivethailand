package com.touchtechnologies.tit;


		import android.Manifest;
		import android.content.pm.PackageManager;
		import android.location.Location;

		import android.os.Bundle;
		import android.support.annotation.NonNull;
		import android.support.v4.app.ActivityCompat;
		import android.support.v4.app.FragmentActivity;
		import android.support.v4.content.ContextCompat;

		import com.google.android.gms.common.ConnectionResult;
		import com.google.android.gms.common.api.GoogleApiClient;
		import com.google.android.gms.location.LocationListener;
		import com.google.android.gms.location.LocationRequest;
		import com.google.android.gms.location.LocationServices;


public class LocationAwareActivity extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

	/** A request location update interval */
	static final int UPDATE_INTERVAL = 5000;
	protected GoogleApiClient mGoogleApiClient;
	protected Location location;
	protected boolean mRequestingLocationUpdates = true;
	private Location mLastLocation;
	private LocationRequest mLocationRequest;
	private static final int REQUEST_FINE_LOCATION=0;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (mGoogleApiClient == null) {
			mGoogleApiClient = new GoogleApiClient.Builder(this)
					.addConnectionCallbacks(this)
					.addOnConnectionFailedListener(this)
					.addApi(LocationServices.API)
					.build();

		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mGoogleApiClient.isConnected() && !mRequestingLocationUpdates) {
			startLocationUpdates();
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		loadPermissions(Manifest.permission.ACCESS_FINE_LOCATION,REQUEST_FINE_LOCATION);

	}

	@Override
	protected void onStop() {
		mGoogleApiClient.disconnect();
		super.onStop();
	}

	@Override
	public void onLocationChanged(Location location) {
		this.location = location;
	}

	@Override
	public void onConnected(Bundle bundle) {

		mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		if (mRequestingLocationUpdates) {
			startLocationUpdates();
		}
		location = mLastLocation;
	}

	@Override
	public void onConnectionSuspended(int i) {

	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {

	}

	protected void createLocationRequest() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL*2);
		mLocationRequest.setFastestInterval(UPDATE_INTERVAL);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
	}

	protected void stopLocationUpdates() {
		LocationServices.FusedLocationApi.removeLocationUpdates(
				mGoogleApiClient, this);
	}

	protected void startLocationUpdates() {
		createLocationRequest();
		LocationServices.FusedLocationApi.requestLocationUpdates(
				mGoogleApiClient, mLocationRequest, this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		stopLocationUpdates();
	}
	private void loadPermissions(String perm,int requestCode) {
		if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
			if (!ActivityCompat.shouldShowRequestPermissionRationale(this, perm)) {
				ActivityCompat.requestPermissions(this, new String[]{perm},requestCode);
			}
		}else {
			mGoogleApiClient.connect();
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		switch (requestCode) {
			case REQUEST_FINE_LOCATION: {
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					// granted
					mGoogleApiClient.connect();
				}
				else{
					// no granted
					mLastLocation = null;
				}
				return;
			}

		}
	}

}
