package com.touchtechnologies.tit.banners;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.touchtechnologies.tit.R;

public class ViewStatisticAdapter extends PagerAdapter {

	private Context context;
	private List<JSONObject> listViewStatistics;

	private ImageLoader imageLoader;
	private DisplayImageOptions options;

	public ViewStatisticAdapter(Context context) {
		this.context = context;
		imageLoader = ImageLoader.getInstance();

		options = new DisplayImageOptions.Builder().cacheOnDisc(true)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
				.cacheInMemory(true)
				.resetViewBeforeLoading(false).build();
	}
	
	void setData(List<JSONObject> listViewStatistics){
		this.listViewStatistics = listViewStatistics;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return listViewStatistics == null?1:listViewStatistics.size();

	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return (view == object);
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		View view = LayoutInflater.from(context).inflate(R.layout.fragment_view_statistic, container, false);
		ImageView imgView = (ImageView)view.findViewById(R.id.imageViewBanners);
		TextView txtTitle = (TextView)view.findViewById(R.id.textViewCountTitle);
		TextView txtCount = (TextView)view.findViewById(R.id.textViewCountNumber);
		
		JSONObject json = null;
		try{
			if(listViewStatistics == null || listViewStatistics.size() == 0){
				json = new JSONObject();
				json.put("title", "TOTAL VIEW");
				json.put("view", "0");
				json.put("imageUrl", "");
			}else{
				json = listViewStatistics.get(position);
			}
			
			String url = json.getString("imageUrl");
			imageLoader.displayImage(url, imgView, options);
			
			String title = json.getString("title");
			txtTitle.setText(title);
			
			int viewCount = json.getInt("view");
			txtCount.setText(viewCount == 0?"...":NumberFormat.getNumberInstance(Locale.UK).format(viewCount));
		
		}catch(Exception e){
			Log.e("SIT-STATISTIC", e.getMessage() + " " + json, e);
		}
		
		container.addView(view, 0);
		
		return view;
	}

	@Override
	public void destroyItem(View collection, int position, Object view) {
		((ViewPager) collection).removeView((View) view);

	}

}