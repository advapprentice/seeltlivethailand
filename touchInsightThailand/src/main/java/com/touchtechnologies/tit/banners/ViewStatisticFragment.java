package com.touchtechnologies.tit.banners;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.opensource.pagerindicator.CirclePageIndicator;
import com.touchtechnologies.tit.AppConfig;
import com.touchtechnologies.tit.R;
import com.touchtechnologies.tit.lib.animator.FullImagePagertransformer;
import com.touchtechnologies.tit.persistent.DataHelperAsyn;
import static com.touchtechnologies.tit.persistent.DataHelperAsyn.*;

/**
 * An entry point for view statistic fragment.
 * This fragment contains banner pager.
 * @author Touch
 *
 */
public class ViewStatisticFragment extends Fragment {
	
	private ViewStatisticAdapter adapter;
	
	private ViewPager viewPager;
	private View view;
	
	private CirclePageIndicator circlepager;
	
	private List<JSONObject> listViewStatistics = new ArrayList<>();
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_poi_banner, container, false);

		viewPager = (ViewPager) view.findViewById(R.id.pager);
		adapter = new ViewStatisticAdapter(getActivity());
		
		String cached = DataHelperAsyn.get(KEY_CACHED_STATISTIC);
		if(cached != null){
			listViewStatistics.addAll(DataHelperAsyn.parseStatictic(cached));
		}
		
		adapter.setData(listViewStatistics);
		
		viewPager.setAdapter(adapter);
		viewPager.setClipToPadding(false);

	//	viewPager.setPageTransformer(true, new FullImagePagertransformer());
					
		Point point = new Point();
		getActivity().getWindowManager().getDefaultDisplay().getSize(point);
		viewPager.getLayoutParams().height = (int)((point.x * 2)/4);

		
		circlepager = (CirclePageIndicator) view.findViewById(R.id.indicator);
		circlepager.setViewPager(viewPager);

		final float density = getResources().getDisplayMetrics().density;

		circlepager.setRadius(5 * density);
//		circlepager.setPageColor(0xFFCFCFCF);
		circlepager.setFillColor(0xFFFFFFFF);
		circlepager.setStrokeColor(0xFFFFFFFF);
		circlepager.setStrokeWidth(1 * density);
		
		return view;
	}
	
	public void setData(List<JSONObject> listViewStatistics){
		this.listViewStatistics = listViewStatistics;
		viewPager.setAdapter(null);
		
		adapter.setData(listViewStatistics);
		viewPager.setAdapter(adapter);
	}
}
