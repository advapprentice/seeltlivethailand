package com.touchtechnologies.tit.banners;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.opensource.pagerindicator.CirclePageIndicator;
import com.touchtechnologies.dataobject.Hotel;
import com.touchtechnologies.tit.R;

public class POIBannersFragment extends Fragment{
	private Hotel hotel;
	private Context context;
	private POIBannersAdapter adapter;
    private ViewPager viewPager;
    
    CirclePageIndicator circlepager;
    private int[] galImages;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_poi_banner ,container, false);
		
		
		galImages = new int[] {};
		
		 viewPager = (ViewPager)view.findViewById(R.id.pager);
		 adapter = new POIBannersAdapter(getActivity(),galImages);
	//	 adapter = new CCTVAdapter(getActivity());
		 
		 viewPager.setAdapter(adapter);
		 viewPager.setCurrentItem(0);
		 
		 viewPager.setClipToPadding(false);

		circlepager = (CirclePageIndicator) view.findViewById(R.id.indicator);
		circlepager.setViewPager(viewPager);

		final float density = getResources().getDisplayMetrics().density;


		circlepager.setRadius(5 * density);
	//	circlepager.setPageColor(0xFFCFCFCF);
	//	circlepager.setFillColor(0xFF3399FF);
		circlepager.setStrokeColor(0xFFCFCFCF);
		circlepager.setStrokeWidth(1 * density);
		
		
		
		return view;
	}

}
