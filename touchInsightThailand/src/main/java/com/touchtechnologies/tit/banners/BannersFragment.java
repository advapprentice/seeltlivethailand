package com.touchtechnologies.tit.banners;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.touchtechnologies.tit.R;

/**
 * Display banners and selectable to launch for a details
 * @author Touch
 *
 */
public class BannersFragment extends Fragment{

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_banners, container, false);
		return view;
	}
}
