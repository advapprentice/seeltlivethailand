package com.touchtechnologies.tit.databinding;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ItemListNearbyfacebookBinding extends ViewDataBinding {
  @NonNull
  public final TextView Location;

  @NonNull
  public final TextView Name;

  @NonNull
  public final ImageView imageView58;

  @NonNull
  public final ImageView imageView59;

  @NonNull
  public final ImageView imgPlace;

  @NonNull
  public final TextView textView43;

  @NonNull
  public final TextView txtCheckinCount;

  @NonNull
  public final TextView txtDistance;

  @NonNull
  public final TextView txtLikeCount;

  protected ItemListNearbyfacebookBinding(Object _bindingComponent, View _root,
      int _localFieldCount, TextView Location, TextView Name, ImageView imageView58,
      ImageView imageView59, ImageView imgPlace, TextView textView43, TextView txtCheckinCount,
      TextView txtDistance, TextView txtLikeCount) {
    super(_bindingComponent, _root, _localFieldCount);
    this.Location = Location;
    this.Name = Name;
    this.imageView58 = imageView58;
    this.imageView59 = imageView59;
    this.imgPlace = imgPlace;
    this.textView43 = textView43;
    this.txtCheckinCount = txtCheckinCount;
    this.txtDistance = txtDistance;
    this.txtLikeCount = txtLikeCount;
  }

  @NonNull
  public static ItemListNearbyfacebookBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.item_list_nearbyfacebook, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ItemListNearbyfacebookBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ItemListNearbyfacebookBinding>inflateInternal(inflater, com.touchtechnologies.tit.R.layout.item_list_nearbyfacebook, root, attachToRoot, component);
  }

  @NonNull
  public static ItemListNearbyfacebookBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.item_list_nearbyfacebook, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ItemListNearbyfacebookBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ItemListNearbyfacebookBinding>inflateInternal(inflater, com.touchtechnologies.tit.R.layout.item_list_nearbyfacebook, null, false, component);
  }

  public static ItemListNearbyfacebookBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ItemListNearbyfacebookBinding bind(@NonNull View view, @Nullable Object component) {
    return (ItemListNearbyfacebookBinding)bind(component, view, com.touchtechnologies.tit.R.layout.item_list_nearbyfacebook);
  }
}
