package com.touchtechnologies.tit.databinding;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ItemFacenearMapInfoBinding extends ViewDataBinding {
  @NonNull
  public final TextView address;

  @NonNull
  public final ImageView imageView56;

  @NonNull
  public final ImageView imageView57;

  @NonNull
  public final ImageView infoAvatar;

  @NonNull
  public final TextView textView38;

  @NonNull
  public final TextView title;

  @NonNull
  public final TextView txtCheckin;

  @NonNull
  public final TextView txtDistance;

  @NonNull
  public final TextView txtLike;

  protected ItemFacenearMapInfoBinding(Object _bindingComponent, View _root, int _localFieldCount,
      TextView address, ImageView imageView56, ImageView imageView57, ImageView infoAvatar,
      TextView textView38, TextView title, TextView txtCheckin, TextView txtDistance,
      TextView txtLike) {
    super(_bindingComponent, _root, _localFieldCount);
    this.address = address;
    this.imageView56 = imageView56;
    this.imageView57 = imageView57;
    this.infoAvatar = infoAvatar;
    this.textView38 = textView38;
    this.title = title;
    this.txtCheckin = txtCheckin;
    this.txtDistance = txtDistance;
    this.txtLike = txtLike;
  }

  @NonNull
  public static ItemFacenearMapInfoBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.item_facenear_map_info, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ItemFacenearMapInfoBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ItemFacenearMapInfoBinding>inflateInternal(inflater, com.touchtechnologies.tit.R.layout.item_facenear_map_info, root, attachToRoot, component);
  }

  @NonNull
  public static ItemFacenearMapInfoBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.item_facenear_map_info, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ItemFacenearMapInfoBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ItemFacenearMapInfoBinding>inflateInternal(inflater, com.touchtechnologies.tit.R.layout.item_facenear_map_info, null, false, component);
  }

  public static ItemFacenearMapInfoBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ItemFacenearMapInfoBinding bind(@NonNull View view, @Nullable Object component) {
    return (ItemFacenearMapInfoBinding)bind(component, view, com.touchtechnologies.tit.R.layout.item_facenear_map_info);
  }
}
